#!/bin/zsh

vimdiff <(cat $1 | sed -e 's/"o": [0-9]\+/"ooo": 1/g' -) <(cat $2 | sed -e 's/"o": [0-9]\+/"ooo": 1/g' -)

