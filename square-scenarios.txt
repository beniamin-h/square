1. Ally turns to be enemy - whole mission you support another player and once he is strong enough he attacks you
2. A traitor among us - someone told the enemy about our weaknesses (undefended cites etc.)
3. Vassalage to the Emperor - complete a few missions ordered by the Emperor
4. Common enemy - couple players fighting against other players
5. Seizure of power - overthrow the Emperor
6. Defend our lands
7. Chaos - couple players fighting each other
8. Defend metal supplies - must not lose couple metal tiles
9. Rocky islands or winter or far north - scenarios with a low food production
10. Free trait from campaign (reward for a mission)

Campaign opponent levels based on player level (not fixed)
