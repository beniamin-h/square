'use strict';

angular.module('square')

.factory('Math', function () {
  Math.log2 = Math.log2 || function (value) { return Math.log(value) / Math.LN2; };
  return Math;
})
.factory('Date', function () {
  return Date;
})
.factory('fs', function () {
  return require('fs');
});
