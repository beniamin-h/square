'use strict';

angular.module('square').factory('testingUtils', ['Joi', function (Joi) {

  return {
    copyObject(obj) {
      return Object.keys(obj).reduce(function (copy, key) {
        copy[key] = obj[key];
        return copy;
      }, {});
    },
    getEventListeners(eventEmitter, eventName) {
      var callbacks = [];
      eventEmitter.on.and.callFake(function (event, callback) {
        if (event === eventName) {
          callbacks.push(callback);
        }
        return eventEmitter;
      });
      return callbacks;
    },
    validateAgainstSchema(obj, schema, done) {
      Joi.validate(obj, schema, {
        presence: 'required', convert: false
      }, function (validationError) {
        if (validationError) {
          done.fail(validationError);
        } else {
          done();
        }
      });
    },
    _instance: this
  };
}]);
