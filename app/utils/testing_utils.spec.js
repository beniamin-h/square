'use strict';

describe('testingUtils', function() {
  beforeEach(module('square'));

  var testingUtils, Joi;

  beforeEach(inject(function (_testingUtils_, _Joi_) {
    testingUtils = _testingUtils_;
    Joi = _Joi_;
  }));

  describe('copyObject', function () {

    it('returns a new Object instance with copied properties', function () {
      var obj = {
        a: 0,
        b: null,
        3: {},
        0: [],
        5: false,
        f: this,
        g: [{}, { a: 8 }]
      };
      var result = testingUtils.copyObject(obj);
      expect(result).toEqual({
        a: 0,
        b: null,
        3: {},
        0: [],
        5: false,
        f: this,
        g: [{}, { a: 8 }]
      });
      expect(result).not.toBe(obj);
    });

  });

  describe('getEventListeners', function () {

    var fakeEventEmitter;
    var fakeEventName;
    var anotherFakeEventName;
    var fakeCallback1;
    var fakeCallback2;
    var fakeCallback3;

    beforeEach(function () {
      fakeEventEmitter = {
        on: {
          and: {
            callFake: jasmine.createSpy('callFake')
          }
        }
      };
      fakeEventName = jasmine.createSpy('eventName');
      anotherFakeEventName = jasmine.createSpy('anotherEventName');
      fakeCallback1 = jasmine.createSpy('callback 1');
      fakeCallback2 = jasmine.createSpy('callback 2');
      fakeCallback3 = jasmine.createSpy('callback 3');
    });

    it('calls callFake on the given `eventEmitter.on.and`', function () {
      testingUtils.getEventListeners(fakeEventEmitter, fakeEventName);
      expect(fakeEventEmitter.on.and.callFake).toHaveBeenCalledWith(jasmine.any(Function));
    });

    it('returns an array of callbacks passed to eventEmitter.on ' +
       'for the given event name', function () {
      fakeEventEmitter.on = jasmine.createSpy('eventEmitter.on');
      var result = testingUtils.getEventListeners(fakeEventEmitter, fakeEventName);
      fakeEventEmitter.on(fakeEventName, fakeCallback1);
      fakeEventEmitter.on(fakeEventName, fakeCallback2);
      fakeEventEmitter.on(fakeEventName, fakeCallback2);
      fakeEventEmitter.on(anotherFakeEventName, fakeCallback3);
      fakeEventEmitter.on(anotherFakeEventName, fakeCallback1);
      expect(result).toEqual([fakeCallback1, fakeCallback2, fakeCallback2]);
    });

  });

  describe('validateAgainstSchema', function () {

    let validationError;
    let fakeObj;
    let fakeSchema;
    let fakeDone;

    beforeEach(function () {
      fakeObj = jasmine.createSpy('obj');
      fakeSchema = jasmine.createSpy('schema');
      fakeDone = jasmine.createSpy('done');
      fakeDone.fail = jasmine.createSpy('done.fail');
      spyOn(Joi, 'validate').and.callFake(function (...args) {
        const callback = Array.from(args)[3];
        callback(validationError);
      });
    });

    it('calls Joi.validate', function () {
      testingUtils.validateAgainstSchema(fakeObj, fakeSchema, fakeDone);
      expect(Joi.validate).toHaveBeenCalledWith(
        fakeObj, fakeSchema, { presence: 'required', convert: false },
        jasmine.any(Function)
      );
    });

    it('calls done.fail if there is any validationError', function () {
      validationError = jasmine.createSpy('validationError');
      testingUtils.validateAgainstSchema(fakeObj, fakeSchema, fakeDone);
      expect(fakeDone.fail).toHaveBeenCalledWith(validationError);
    });

    it('calls done if there is no validationError', function () {
      validationError = null;
      testingUtils.validateAgainstSchema(fakeObj, fakeSchema, fakeDone);
      expect(fakeDone).toHaveBeenCalledWith();
    });

  });

});
