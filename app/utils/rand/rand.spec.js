'use strict';

describe('Rand', function() {
  beforeEach(module('square'));

  let Rand, md5, seedrandom, Math_, RandStatefulGenerator;

  beforeEach(function () {
    module(function ($provide) {
      $provide.value('md5', jasmine.createSpy('md5'));
      $provide.value('seedrandom', jasmine.createSpy('seedrandom'));
    });
  });

  beforeEach(inject(function (
      _Rand_, _md5_, _seedrandom_, _Math_, _RandStatefulGenerator_) {
    Rand = _Rand_;
    md5 = _md5_;
    seedrandom = _seedrandom_;
    Math_ = _Math_;
    RandStatefulGenerator = _RandStatefulGenerator_;
  }));

  let seedrandomResult;
  let md5Result;

  let rand;
  let fakeNamespace;
  let fakeGameSeed;

  beforeEach(function () {
    seedrandomResult = jasmine.createSpyObj('seedrandomResult',
      ['quick', 'double', 'int32']);
    seedrandom.and.returnValue(seedrandomResult);
    md5Result = jasmine.createSpy('md5');
    md5.and.returnValue(md5Result);
  });

  describe('constructor', function () {

    beforeEach(function () {
      fakeNamespace = jasmine.createSpy('namespace');
      fakeGameSeed = jasmine.createSpy('gameSeed');
    });

    it('calls md5 on the given namespace', function () {
      rand = new Rand(fakeNamespace, fakeGameSeed);
      expect(md5).toHaveBeenCalledWith(fakeNamespace);
    });

    it('calls md5 on the given gameSeed', function () {
      rand = new Rand(fakeNamespace, fakeGameSeed);
      expect(md5).toHaveBeenCalledWith(fakeGameSeed);
    });

    it('sets gameSeed to "" if not given', function () {
      rand = new Rand(fakeNamespace);
      expect(rand.gameSeed).toBe('');
    });

  });


  describe('_createUnpredictableSeed', function () {

    it('calls seedrandom', function () {
      Rand._createUnpredictableSeed();
      expect(seedrandom).toHaveBeenCalledWith(
        jasmine.any(String), { entropy: true });
    });

    it('returns seedrandom result', function () {
      const seedrandomResult = jasmine.createSpy('seedrandomResult');
      seedrandom.and.returnValue(seedrandomResult);
      const result = Rand._createUnpredictableSeed();
      expect(result).toBe(seedrandomResult);
    });

  });

  describe('getUnpredictableFloat', function () {

    beforeEach(function () {
      spyOn(Rand, '_createUnpredictableSeed').and.returnValue(seedrandomResult);
    });

    it('calls Rand._createUnpredictableSeed', function () {
      Rand.getUnpredictableFloat();
      expect(Rand._createUnpredictableSeed).toHaveBeenCalledWith();
    });

    it('calls seedrandom.quick', function () {
      Rand.getUnpredictableFloat();
      expect(seedrandomResult.quick).toHaveBeenCalledWith();
    });

    it('returns seedrandom.quick result', function () {
      const quickResult = jasmine.createSpy('quickResult');
      seedrandomResult.quick.and.returnValue(quickResult);
      const result = Rand.getUnpredictableFloat();
      expect(result).toBe(quickResult);
    });

  });

  describe('getUnpredictableDouble', function () {

    beforeEach(function () {
      spyOn(Rand, '_createUnpredictableSeed').and.returnValue(seedrandomResult);
    });

    it('calls Rand._createUnpredictableSeed', function () {
      Rand.getUnpredictableDouble();
      expect(Rand._createUnpredictableSeed).toHaveBeenCalledWith();
    });

    it('calls seedrandom.double', function () {
      Rand.getUnpredictableDouble();
      expect(seedrandomResult.double).toHaveBeenCalledWith();
    });

    it('returns seedrandom.double result', function () {
      const doubleResult = jasmine.createSpy('doubleResult');
      seedrandomResult.double.and.returnValue(doubleResult);
      const result = Rand.getUnpredictableDouble();
      expect(result).toBe(doubleResult);
    });

  });

  describe('getUnpredictableInt31Unsigned', function () {

    beforeEach(function () {
      spyOn(Rand, '_createUnpredictableSeed').and.returnValue(seedrandomResult);
      spyOn(Math_, 'abs');
    });

    it('calls Rand._createUnpredictableSeed', function () {
      Rand.getUnpredictableInt31Unsigned();
      expect(Rand._createUnpredictableSeed).toHaveBeenCalledWith();
    });

    it('calls seedrandom.int32', function () {
      Rand.getUnpredictableInt31Unsigned();
      expect(seedrandomResult.int32).toHaveBeenCalledWith();
    });

    it('calls Math.abs', function () {
      const int32Result = jasmine.createSpy('int32Result');
      seedrandomResult.int32.and.returnValue(int32Result);
      Rand.getUnpredictableInt31Unsigned();
      expect(Math_.abs).toHaveBeenCalledWith(int32Result);
    });

    it('returns Math.abs result', function () {
      const absResult = jasmine.createSpy('absResult');
      Math_.abs.and.returnValue(absResult);
      const result = Rand.getUnpredictableInt31Unsigned();
      expect(result).toBe(absResult);
    });

  });

  describe('getUnpredictableInt32Signed', function () {

    beforeEach(function () {
      spyOn(Rand, '_createUnpredictableSeed').and.returnValue(seedrandomResult);
    });

    it('calls Rand._createUnpredictableSeed', function () {
      Rand.getUnpredictableInt32Signed();
      expect(Rand._createUnpredictableSeed).toHaveBeenCalledWith();
    });

    it('calls seedrandom.int32', function () {
      Rand.getUnpredictableInt32Signed();
      expect(seedrandomResult.int32).toHaveBeenCalledWith();
    });

    it('returns seedrandom.int32 result', function () {
      const int32Result = jasmine.createSpy('int32Result');
      seedrandomResult.int32.and.returnValue(int32Result);
      const result = Rand.getUnpredictableInt32Signed();
      expect(result).toBe(int32Result);
    });

  });

  describe('method', function () {

    beforeEach(function () {
      fakeNamespace = jasmine.createSpy('namespace');
      fakeGameSeed = jasmine.createSpy('gameSeed');
      rand = new Rand(fakeNamespace, fakeGameSeed);
    });

    describe('_argsToHash', function () {

      let fakeElem1;
      let fakeElem2;

      beforeEach(function () {
        fakeElem1 = jasmine.createSpyObj('elem1', ['toString']);
        fakeElem2 = jasmine.createSpyObj('elem2', ['toString']);
      });

      it('throws an error if args is an empty array', function () {
        expect(() => rand._argsToHash([]))
          .toThrow(new Error('Invalid empty seed'));
      });

      it('calls toString on every args element', function () {
        rand._argsToHash([fakeElem1, fakeElem2, fakeElem1]);
        expect(fakeElem1.toString).toHaveBeenCalledWith();
        expect(fakeElem1.toString.calls.count()).toBe(2);
        expect(fakeElem2.toString).toHaveBeenCalledWith();
      });

      it('calls md5 on concatenated args elements\' toString results', function () {
        const fakeElem1toStringResult = 'result1xxx';
        const fakeElem2toStringResult = 'result2xxx';
        fakeElem1.toString.and.returnValue(fakeElem1toStringResult);
        fakeElem2.toString.and.returnValue(fakeElem2toStringResult);
        rand._argsToHash([fakeElem2, fakeElem1]);
        expect(md5).toHaveBeenCalledWith('result2xxx-result1xxx');
      });

      it('returns md5 result', function () {
        const result = rand._argsToHash([fakeElem1]);
        expect(result).toBe(md5Result);
      });

    });

    describe('_createSeed', function () {

      let fakeArgs;

      beforeEach(function () {
        fakeArgs = jasmine.createSpy('args');
        spyOn(rand, '_argsToHash');
      });

      it('calls seedrandom', function () {
        rand._createSeed(fakeArgs);
        expect(seedrandom).toHaveBeenCalledWith(jasmine.any(String));
      });

      it('calls _argsToHash', function () {
        rand._createSeed(fakeArgs);
        expect(rand._argsToHash).toHaveBeenCalledWith(fakeArgs);
      });

      it('returns seedrandom result', function () {
        const result = rand._createSeed(fakeArgs);
        expect(result).toBe(seedrandomResult);
      });

    });

    describe('getFloat', function () {

      beforeEach(function () {
        spyOn(rand, '_createSeed').and.returnValue(seedrandomResult);
      });

      it('calls _createSeed', function () {
        rand.getFloat();
        expect(rand._createSeed).toHaveBeenCalledWith([]);
      });

      it('calls seedrandom.quick', function () {
        rand.getFloat();
        expect(seedrandomResult.quick).toHaveBeenCalledWith();
      });

      it('transforms given arguments into an array', function () {
        spyOn(Array, 'from');
        rand.getFloat();
        expect(Array.from).toHaveBeenCalled();
      });

      it('returns seedrandom.quick result', function () {
        const quickResult = jasmine.createSpy('quickResult');
        seedrandomResult.quick.and.returnValue(quickResult);
        const result = rand.getFloat();
        expect(result).toBe(quickResult);
      });

    });

    describe('getDouble', function () {

      beforeEach(function () {
        spyOn(rand, '_createSeed').and.returnValue(seedrandomResult);
      });

      it('calls _createSeed', function () {
        rand.getDouble();
        expect(rand._createSeed).toHaveBeenCalledWith([]);
      });

      it('calls seedrandom.double', function () {
        rand.getDouble();
        expect(seedrandomResult.double).toHaveBeenCalledWith();
      });

      it('transforms given arguments into an array', function () {
        spyOn(Array, 'from');
        rand.getDouble();
        expect(Array.from).toHaveBeenCalled();
      });

      it('returns seedrandom.double result', function () {
        const doubleResult = jasmine.createSpy('doubleResult');
        seedrandomResult.double.and.returnValue(doubleResult);
        const result = rand.getDouble();
        expect(result).toBe(doubleResult);
      });

    });

    describe('getInt31Unsigned', function () {

      beforeEach(function () {
        spyOn(rand, '_createSeed').and.returnValue(seedrandomResult);
        spyOn(Math_, 'abs');
      });

      it('calls _createSeed', function () {
        rand.getInt31Unsigned();
        expect(rand._createSeed).toHaveBeenCalledWith([]);
      });

      it('calls seedrandom.int32', function () {
        rand.getInt31Unsigned();
        expect(seedrandomResult.int32).toHaveBeenCalledWith();
      });

      it('transforms given arguments into an array', function () {
        spyOn(Array, 'from');
        rand.getInt31Unsigned();
        expect(Array.from).toHaveBeenCalled();
      });

      it('calls Math.abs', function () {
        const int32Result = jasmine.createSpy('int32Result');
        seedrandomResult.int32.and.returnValue(int32Result);
        rand.getInt31Unsigned();
        expect(Math_.abs).toHaveBeenCalledWith(int32Result);
      });

      it('returns Math.abs result', function () {
        const absResult = jasmine.createSpy('absResult');
        Math_.abs.and.returnValue(absResult);
        const result = rand.getInt31Unsigned();
        expect(result).toBe(absResult);
      });

    });

    describe('getInt32Signed', function () {

      beforeEach(function () {
        spyOn(rand, '_createSeed').and.returnValue(seedrandomResult);
      });

      it('calls _createSeed', function () {
        rand.getInt32Signed();
        expect(rand._createSeed).toHaveBeenCalledWith([]);
      });

      it('calls seedrandom.int32', function () {
        rand.getInt32Signed();
        expect(seedrandomResult.int32).toHaveBeenCalledWith();
      });

      it('transforms given arguments into an array', function () {
        spyOn(Array, 'from');
        rand.getInt32Signed();
        expect(Array.from).toHaveBeenCalled();
      });

      it('returns seedrandom.int32 result', function () {
        const int32Result = jasmine.createSpy('int32Result');
        seedrandomResult.int32.and.returnValue(int32Result);
        const result = rand.getInt32Signed();
        expect(result).toBe(int32Result);
      });

    });

    describe('getGenerator', function () {

      let fakeArg1;
      let fakeArg2;

      beforeEach(function () {
        fakeArg1 = jasmine.createSpy('fakeArg1');
        fakeArg2 = jasmine.createSpy('fakeArg2');
        spyOn(rand, '_createSeed').and.returnValue(seedrandomResult);
      });
      
      it('calls _createSeed', function () {
        rand.getGenerator(fakeArg1, fakeArg2);
        expect(rand._createSeed).toHaveBeenCalledWith([fakeArg1, fakeArg2]);
      });

      it('returns an object with 4 methods', function () {
        const result = rand.getGenerator(fakeArg1, fakeArg2);
        expect(result).toEqual({
          getFloat: jasmine.any(Function),
          getDouble: jasmine.any(Function),
          getInt31Unsigned: jasmine.any(Function),
          getInt32Signed: jasmine.any(Function)
        });
      });

      describe('method getFloat', function () {

        it('calls seedrandom.quick', function () {
          rand.getGenerator(fakeArg1, fakeArg2).getFloat();
          expect(seedrandomResult.quick).toHaveBeenCalledWith();
        });

        it('returns seedrandom.quick result', function () {
          const quickResult = jasmine.createSpy('quickResult');
          seedrandomResult.quick.and.returnValue(quickResult);
          const result = rand.getGenerator(fakeArg1, fakeArg2).getFloat();
          expect(result).toBe(quickResult);
        });

      });

      describe('method getDouble', function () {

        it('calls seedrandom.double', function () {
          rand.getGenerator(fakeArg1, fakeArg2).getDouble();
          expect(seedrandomResult.double).toHaveBeenCalledWith();
        });

        it('returns seedrandom.double result', function () {
          const doubleResult = jasmine.createSpy('doubleResult');
          seedrandomResult.double.and.returnValue(doubleResult);
          const result = rand.getGenerator(fakeArg1, fakeArg2).getDouble();
          expect(result).toBe(doubleResult);
        });

      });

      describe('method getInt31Unsigned', function () {

        beforeEach(function () {
          spyOn(Math_, 'abs');
        });

        it('calls seedrandom.int32', function () {
          rand.getGenerator(fakeArg1, fakeArg2).getInt31Unsigned();
          expect(seedrandomResult.int32).toHaveBeenCalledWith();
        });

        it('calls Math.abs', function () {
          const int32Result = jasmine.createSpy('int32Result');
          seedrandomResult.int32.and.returnValue(int32Result);
          rand.getGenerator(fakeArg1, fakeArg2).getInt31Unsigned();
          expect(Math_.abs).toHaveBeenCalledWith(int32Result);
        });

        it('returns Math.abs result', function () {
          const absResult = jasmine.createSpy('absResult');
          Math_.abs.and.returnValue(absResult);
          const result = rand.getGenerator(fakeArg1, fakeArg2).getInt31Unsigned();
          expect(result).toBe(absResult);
        });

      });

      describe('method getInt32Signed', function () {

        it('calls seedrandom.int32', function () {
          rand.getGenerator(fakeArg1, fakeArg2).getInt31Unsigned();
          expect(seedrandomResult.int32).toHaveBeenCalledWith();
        });

        it('returns seedrandom.int32 result', function () {
          const int32Result = jasmine.createSpy('int32Result');
          seedrandomResult.int32.and.returnValue(int32Result);
          const result = rand.getGenerator(fakeArg1, fakeArg2).getInt32Signed();
          expect(result).toBe(int32Result);
        });

      });

    });

    describe('getStatefulGenerator', function () {

      let fakeArg1;
      let fakeArg2;

      beforeEach(function () {
        fakeArg1 = jasmine.createSpy('fakeArg1');
        fakeArg2 = jasmine.createSpy('fakeArg2');
        spyOn(rand, '_createSeed').and.returnValue(seedrandomResult);
      });

      it('throws an error if no seedArgs given', function () {
        expect(() => rand.getStatefulGenerator())
          .toThrow(new Error('Invalid empty seed'));
      });

      it('returns a new RandStatefulGenerator instance', function () {
        const result = rand.getStatefulGenerator(fakeArg1, fakeArg2);
        expect(result instanceof RandStatefulGenerator).toBeTrue();
      });

      it('calling getFloat causes counter incrementation', function () {
        const gen = rand.getStatefulGenerator(fakeArg1, fakeArg2);
        expect(gen.counter).toBe(0);
        gen.getFloat();
        expect(gen.counter).toBe(1);
        gen.getFloat();
        expect(gen.counter).toBe(2);
      });

      it('calling getDouble causes counter incrementation', function () {
        const gen = rand.getStatefulGenerator(fakeArg1, fakeArg2);
        expect(gen.counter).toBe(0);
        gen.getDouble();
        expect(gen.counter).toBe(1);
        gen.getDouble();
        expect(gen.counter).toBe(2);
      });

      it('calling getInt31Unsigned causes counter incrementation', function () {
        const gen = rand.getStatefulGenerator(fakeArg1, fakeArg2);
        expect(gen.counter).toBe(0);
        gen.getInt31Unsigned();
        expect(gen.counter).toBe(1);
        gen.getInt31Unsigned();
        expect(gen.counter).toBe(2);
      });

      it('calling getInt32Signed causes counter incrementation', function () {
        const gen = rand.getStatefulGenerator(fakeArg1, fakeArg2);
        expect(gen.counter).toBe(0);
        gen.getInt32Signed();
        expect(gen.counter).toBe(1);
        gen.getInt32Signed();
        expect(gen.counter).toBe(2);
      });

      describe('each method of the returned object calls _createSeed', function () {

        let gen;

        beforeEach(function () {
          gen = rand.getStatefulGenerator(fakeArg1, fakeArg2);
          expect(rand._createSeed).not.toHaveBeenCalled();
        });

        it('getInt32Signed', function () {
          gen.getInt32Signed();
          expect(rand._createSeed).toHaveBeenCalledWith([fakeArg1, fakeArg2, 0]);
        });

        it('getFloat', function () {
          gen.getFloat();
          expect(rand._createSeed).toHaveBeenCalledWith([fakeArg1, fakeArg2, 0]);
        });

        it('getInt32Signed', function () {
          gen.getInt32Signed();
          expect(rand._createSeed).toHaveBeenCalledWith([fakeArg1, fakeArg2, 0]);
        });

        it('getDouble', function () {
          gen.getDouble();
          expect(rand._createSeed).toHaveBeenCalledWith([fakeArg1, fakeArg2, 0]);
        });

      });

      it('each method of the returned object in subsequent calls ' +
         'calls _createSeed', function () {
        const gen = rand.getStatefulGenerator(fakeArg1, fakeArg2);
        expect(rand._createSeed).not.toHaveBeenCalled();
        gen.getInt32Signed();
        expect(rand._createSeed).toHaveBeenCalledWith([fakeArg1, fakeArg2, 0]);
        rand._createSeed.calls.reset();
        gen.getFloat();
        expect(rand._createSeed).toHaveBeenCalledWith([fakeArg1, fakeArg2, 1]);
        rand._createSeed.calls.reset();
        gen.getInt32Signed();
        expect(rand._createSeed).toHaveBeenCalledWith([fakeArg1, fakeArg2, 2]);
        rand._createSeed.calls.reset();
        gen.getDouble();
        expect(rand._createSeed).toHaveBeenCalledWith([fakeArg1, fakeArg2, 3]);
      });

      it('getFloat of the returned object ' +
         'calls seedrandom.quick', function () {
        const gen = rand.getStatefulGenerator(fakeArg1, fakeArg2);
        rand._createSeed.and.returnValue(seedrandomResult);
        gen.getFloat();
        expect(seedrandomResult.quick).toHaveBeenCalledWith();
      });

      it('getDouble of the returned object ' +
         'calls seedrandom.double', function () {
        const gen = rand.getStatefulGenerator(fakeArg1, fakeArg2);
        rand._createSeed.and.returnValue(seedrandomResult);
        gen.getDouble();
        expect(seedrandomResult.double).toHaveBeenCalledWith();
      });

      it('getInt31Unsigned of the returned object ' +
         'calls seedrandom.int32', function () {
        const gen = rand.getStatefulGenerator(fakeArg1, fakeArg2);
        rand._createSeed.and.returnValue(seedrandomResult);
        gen.getInt31Unsigned();
        expect(seedrandomResult.int32).toHaveBeenCalledWith();
      });

      it('getInt32Signed of the returned object ' +
         'calls seedrandom.int32', function () {
        const gen = rand.getStatefulGenerator(fakeArg1, fakeArg2);
        rand._createSeed.and.returnValue(seedrandomResult);
        gen.getInt32Signed();
        expect(seedrandomResult.int32).toHaveBeenCalledWith();
      });

    });

  });

});
