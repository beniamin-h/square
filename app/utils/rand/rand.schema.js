'use strict';

var _RegExp = require('nwglobal').RegExp;

angular.module('square')

.constant('md5RegExp', new _RegExp(/^[a-f0-9]{32}$/))

.factory('RandStatefulGeneratorSchema', ['Joi', 'RandSchema',
    function (Joi, RandSchema) {

  return Joi.object().keys({
    counter: Joi.number().natural(),
    rand: RandSchema,
    seedArgs: Joi.array().min(1),
    getFloat: Joi.func().arity(0),
    getDouble: Joi.func().arity(0),
    getInt31Unsigned: Joi.func().arity(0),
    getInt32Signed: Joi.func().arity(0)
  }).type(Object);

}])

.factory('RandGeneratorSchema', ['Joi',
    function (Joi) {

  return Joi.object().keys({
    getFloat: Joi.func().arity(0),
    getDouble: Joi.func().arity(0),
    getInt31Unsigned: Joi.func().arity(0),
    getInt32Signed: Joi.func().arity(0)
  }).type(Object);

}])

.factory('RandSchema', ['Joi', 'Rand', 'md5RegExp',
    function (Joi, Rand, md5RegExp) {

  return Joi.object().keys({
    namespace: Joi.string().regex(md5RegExp),
    gameSeed: [Joi.string().regex(md5RegExp), Joi.any().valid('')]
  }).type(Rand);

}]);
