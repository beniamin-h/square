'use strict';

angular.module('square')

.factory('RandStatefulGenerator', [function () {

  function RandStatefulGenerator(rand, seedArgs) {
    this.counter = 0;
    this.rand = rand;
    this.seedArgs = seedArgs;
  }

  RandStatefulGenerator.prototype.getFloat = function() {
    return this.rand._createSeed(this.seedArgs.concat(this.counter++)).quick();
  };

  RandStatefulGenerator.prototype.getDouble = function() {
    return this.rand._createSeed(this.seedArgs.concat(this.counter++)).double();
  };

  RandStatefulGenerator.prototype.getInt31Unsigned = function() {
    return Math.abs(
      this.rand._createSeed(this.seedArgs.concat(this.counter++)).int32()
    );
  };

  RandStatefulGenerator.prototype.getInt32Signed = function() {
    return this.rand._createSeed(this.seedArgs.concat(this.counter++)).int32();
  };

  return RandStatefulGenerator;
}])

.factory('Rand', ['Math', 'md5', 'seedrandom', 'RandStatefulGenerator',
                  function (Math, md5, seedrandom, RandStatefulGenerator) {

  var _secret = '4wGbJdxZ2L-Jq86HG0tg2';

  function Rand(namespace, gameSeed) {
    this.namespace = namespace ? md5(namespace) : '';
    this.gameSeed = gameSeed ? md5(gameSeed) : '';
  }

  Rand._createUnpredictableSeed = function () {
    return seedrandom(_secret + 'xyz', { entropy: true });
  };

  Rand.getUnpredictableFloat = function () {
    return Rand._createUnpredictableSeed().quick();
  };

  Rand.getUnpredictableDouble = function () {
    return Rand._createUnpredictableSeed().double();
  };

  Rand.getUnpredictableInt31Unsigned = function () {
    return Math.abs(Rand._createUnpredictableSeed().int32());
  };

  Rand.getUnpredictableInt32Signed = function () {
    return Rand._createUnpredictableSeed().int32();
  };

  Rand.prototype.namespace = '';
  Rand.prototype.gameSeed = '';

  Rand.prototype._argsToHash = function (args) {
    if (args.length === 0) {
      throw new Error('Invalid empty seed');
    }
    var strArgs = args.map(arg => arg.toString());
    return md5(strArgs.join('-'));
  };

  Rand.prototype._createSeed = function (args) {
    return seedrandom(
      _secret + 'z' +
      this.gameSeed + 'x' +
      this.namespace + 'y' +
      this._argsToHash(args)
    );
  };

  Rand.prototype.getFloat = function (/* ...seed */) {
    return this._createSeed(Array.from(arguments)).quick();
  };

  Rand.prototype.getDouble = function (/* ...seed */) {
    return this._createSeed(Array.from(arguments)).double();
  };

  Rand.prototype.getInt31Unsigned = function (/* ...seed */) {
    return Math.abs(this._createSeed(Array.from(arguments)).int32());
  };

  Rand.prototype.getInt32Signed = function (/* ...seed */) {
    return this._createSeed(Array.from(arguments)).int32();
  };

  Rand.prototype.getGenerator = function (/* ...seed */) {
    var rng = this._createSeed(Array.from(arguments));
    return {
      getFloat() { return rng.quick(); },
      getDouble() { return rng.double(); },
      getInt31Unsigned() { return Math.abs(rng.int32()); },
      getInt32Signed() { return rng.int32(); }
    };
  };

  Rand.prototype.getStatefulGenerator = function (...seedArgs) {
    if (seedArgs.length === 0) {
      throw new Error('Invalid empty seed');
    }
    return new RandStatefulGenerator(this, seedArgs);
  };

  return Rand;
}]);
