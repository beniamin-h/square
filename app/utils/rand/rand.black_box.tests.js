'use strict';

describe('Rand', function() {
  beforeEach(module('square'));

  let Rand, Block, Plain, Forest, ArrayUtils;

  beforeEach(inject(function (
      _Rand_, _Block_, _Plain_, _Forest_, _ArrayUtils_) {
    Rand = _Rand_;
    Block = _Block_;
    Plain = _Plain_;
    Forest = _Forest_;
    ArrayUtils = _ArrayUtils_;
  }));

  describe('getUnpredictableFloat', function () {

    it('returns a random float', function () {
      const result = Rand.getUnpredictableFloat();
      expect(result).toBeNumber();
      expect(result).toBeWithinRange(0, 1);
    });

    it('returns a different number each time', function () {
      expect(ArrayUtils.elementsAreDifferent([
        Rand.getUnpredictableFloat(),
        Rand.getUnpredictableFloat(),
        Rand.getUnpredictableFloat(),
        Rand.getUnpredictableFloat(),
      ])).toBeTrue();
    });

  });

  describe('getUnpredictableDouble', function () {

    it('returns a random float', function () {
      const result = Rand.getUnpredictableDouble();
      expect(result).toBeNumber();
      expect(result).toBeWithinRange(0, 1);
    });

    it('returns a different number each time', function () {
      expect(ArrayUtils.elementsAreDifferent([
        Rand.getUnpredictableDouble(),
        Rand.getUnpredictableDouble(),
        Rand.getUnpredictableDouble(),
        Rand.getUnpredictableDouble(),
      ])).toBeTrue();
    });

  });

  describe('getUnpredictableInt31Unsigned', function () {

    it('returns a random positive integer less than 2^31', function () {
      const result = Rand.getUnpredictableInt31Unsigned();
      expect(result).toBeNumber();
      expect(result).toBeWithinRange(0, Math.pow(2, 31));
    });

    describe('each time', function () {

      let results;

      beforeEach(function () {
        results = [];
        let i = 0;
        while(i++ < 1000) {
          results.push(Rand.getUnpredictableInt31Unsigned());
        }
      });

      it('returns a different number', function () {
        expect(ArrayUtils.elementsAreDifferent(results)).toBeTrue();
      });

      it('returns a number greater or equal to 0', function () {
        results.map((a) => expect(a).toBeGreaterThanOrEqual(0));
      });

      it('returns a number less than 2^31', function () {
        const limit = Math.pow(2, 31);
        results.map((a) => expect(a).toBeLessThan(limit));
      });

    });

  });

  describe('getUnpredictableInt32Signed', function () {

    it('returns a random integer less than 2^31 and greater than -2^31', function () {
      const result = Rand.getUnpredictableInt32Signed();
      expect(result).toBeNumber();
      expect(result).toBeWithinRange(-Math.pow(2, 31), Math.pow(2, 31));
    });

    describe('each time', function () {

      let results;

      beforeEach(function () {
        results = [];
        let i = 0;
        while(i++ < 1000) {
          results.push(Rand.getUnpredictableInt32Signed());
        }
      });

      it('returns a different number', function () {
        expect(ArrayUtils.elementsAreDifferent(results)).toBeTrue();
      });

      it('returns a number greater than -2^31', function () {
        const limit = -Math.pow(2, 31);
        results.map((a) => expect(a).toBeGreaterThan(limit));
      });

      it('returns a number less than 2^31', function () {
        const limit = Math.pow(2, 31);
        results.map((a) => expect(a).toBeLessThan(limit));
      });

    });

  });

  describe('_argsToHash', function () {

    it('returns different hash for different blocks', function () {
      const rand = new Rand('blocks');
      const blocks = [
        new Block(0, 0, { x: 0, y: 0 }, new Plain(), true),
        new Block(0, 1, { x: 0, y: 0 }, new Plain(), true),
        new Block(1, 0, { x: 0, y: 0 }, new Plain(), true),
        new Block(0, 0, { x: 1, y: 0 }, new Plain(), true),
        new Block(0, 0, { x: 0, y: 1 }, new Plain(), true),
        new Block(0, 0, { x: 0, y: 0 }, new Forest(), true),
        new Block(0, 0, { x: 0, y: 1 }, new Plain(), false),
      ];
      const results = blocks.map((block) => rand._argsToHash([block]));
      expect(ArrayUtils.elementsAreDifferent(results)).toBeTrue();
    });

    it('returns the same hash for the same blocks', function () {
      const rand = new Rand('blocks');
      const blocks = [
        new Block(0, 0, { x: 0, y: 0 }, new Plain(), true),
        new Block(0, 0, { x: 0, y: 0 }, new Plain(), true),
        new Block(0, 0, { x: 0, y: 0 }, new Plain(), true),
      ];
      const results = blocks.map((block) => rand._argsToHash([block]));
      expect(ArrayUtils.elementsAreEqual(results)).toBeTrue();
    });

  });

  describe('_createSeed', function () {

    it('returns the same seeded random numbers for the same ' +
       'gameSeed, namespace and the given args', function () {
      const fakeNamespace = 'fake-namespace';
      const fakeGameSeed = 435615646;
      const fakeArgs = ['x', 'y', 5, { toString() { return 'abc'; } }];
      const rand_1 = new Rand(fakeNamespace, fakeGameSeed);
      const rand_2 = new Rand(fakeNamespace, fakeGameSeed);
      expect(rand_1._createSeed(fakeArgs).quick())
        .toBe(rand_2._createSeed(fakeArgs).quick());
    });

    it('returns different seeded random numbers ' +
       'for different gameSeed', function () {
      const fakeNamespace = 'fake-namespace';
      const fakeGameSeed_1 = 435615646;
      const fakeGameSeed_2 = 77895159;
      const fakeArgs = ['x', 'y', 5, { toString() { return 'abc'; } }];
      const rand_1 = new Rand(fakeNamespace, fakeGameSeed_1);
      const rand_2 = new Rand(fakeNamespace, fakeGameSeed_2);
      expect(rand_1._createSeed(fakeArgs).quick())
        .not.toEqual(rand_2._createSeed(fakeArgs).quick());
    });

    it('returns different seeded random numbers ' +
       'for different namespace', function () {
      const fakeNamespace_1 = 'fake-namespace-1';
      const fakeNamespace_2 = 'fake-namespace-2';
      const fakeGameSeed = 435615646;
      const fakeArgs = ['x', 'y', 5, { toString() { return 'abc'; } }];
      const rand_1 = new Rand(fakeNamespace_1, fakeGameSeed);
      const rand_2 = new Rand(fakeNamespace_2, fakeGameSeed);
      expect(rand_1._createSeed(fakeArgs).quick())
        .not.toEqual(rand_2._createSeed(fakeArgs).quick());
    });

    it('returns different seeded random numbers ' +
       'for different given args', function () {
      const fakeNamespace = 'fake-namespace';
      const fakeGameSeed = 435615646;
      const fakeArgs_1 = ['x', 'y', 5, { toString() { return 'abc'; } }];
      const fakeArgs_2 = ['x', 'y', 6, { toString() { return 'abc'; } }];
      const rand_1 = new Rand(fakeNamespace, fakeGameSeed);
      const rand_2 = new Rand(fakeNamespace, fakeGameSeed);
      expect(rand_1._createSeed(fakeArgs_1).quick())
        .not.toEqual(rand_2._createSeed(fakeArgs_2).quick());
    });

    it('returns different seeded random numbers ' +
       'for different given args toString results', function () {
      const fakeNamespace = 'fake-namespace';
      const fakeGameSeed = 435615646;
      const fakeArgs_1 = ['x', 'y', 5, { toString() { return 'abc'; } }];
      const fakeArgs_2 = ['x', 'y', 5, { toString() { return 'abc z'; } }];
      const rand_1 = new Rand(fakeNamespace, fakeGameSeed);
      const rand_2 = new Rand(fakeNamespace, fakeGameSeed);
      expect(rand_1._createSeed(fakeArgs_1).quick())
        .not.toEqual(rand_2._createSeed(fakeArgs_2).quick());
    });

    it('returns the same seeded random numbers ' +
       'in subsequent calls', function () {
      const fakeNamespace = 'fake-namespace';
      const fakeGameSeed = 435615646;
      const fakeArgs = ['x', 'y', 5, { toString() { return 'abc'; } }];
      const rand = new Rand(fakeNamespace, fakeGameSeed);
      let i = 0;
      const results = [];
      while (i++ < 1000) {
        results.push(rand._createSeed(fakeArgs).quick());
      }
      expect(ArrayUtils.elementsAreEqual(results)).toBeTrue();
    });

    it('returns different seeded random numbers ' +
       'in subsequent calls on one _createSeed result', function () {
      const fakeNamespace = 'fake-namespace';
      const fakeGameSeed = 435615646;
      const fakeArgs = ['x', 'y', 5, { toString() { return 'abc'; } }];
      const rand = new Rand(fakeNamespace, fakeGameSeed);
      let i = 0;
      const results = [];
      const seed = rand._createSeed(fakeArgs);
      while (i++ < 1000) {
        results.push(seed.quick());
      }
      expect(ArrayUtils.elementsAreDifferent(results)).toBeTrue();
    });

  });

  describe('getFloat', function () {

    it('returns the same number for the same args', function () {
      const rand = new Rand('fake-ns');
      expect(rand.getFloat(1, 'z')).toBe(rand.getFloat(1, 'z'));
    });

    it('returns different numbers for different args', function () {
      const rand = new Rand('fake-ns');
      expect(rand.getFloat(1, 'z')).not.toEqual(rand.getFloat(1, 'y'));
    });

  });

  describe('getDouble', function () {

    it('returns the same number for the same args', function () {
      const rand = new Rand('fake-ns');
      expect(rand.getDouble(2, 'z')).toBe(rand.getDouble(2, 'z'));
    });

    it('returns different numbers for different args', function () {
      const rand = new Rand('fake-ns');
      expect(rand.getDouble(1, 'z')).not.toEqual(rand.getDouble(2, 'z'));
    });

  });

  describe('getInt31Unsigned', function () {

    it('returns the same number for the same args', function () {
      const rand = new Rand('fake-ns');
      expect(rand.getInt31Unsigned(0, 'z')).toBe(rand.getInt31Unsigned(0, 'z'));
    });

    it('returns different numbers for different args', function () {
      const rand = new Rand('fake-ns');
      expect(rand.getInt31Unsigned(0, 'z'))
        .not.toEqual(rand.getInt31Unsigned(0, 'a'));
    });

  });

  describe('getInt32Signed', function () {

    it('returns the same number for the same args', function () {
      const rand = new Rand('fake-ns');
      expect(rand.getInt32Signed(-1, 'z')).toBe(rand.getInt32Signed(-1, 'z'));
    });

    it('returns different numbers for different args', function () {
      const rand = new Rand('fake-ns');
      expect(rand.getInt32Signed(-1, 'x'))
        .not.toEqual(rand.getInt32Signed(-1, 'y'));
    });

  });

  describe('getGenerator', function () {

    describe('getFloat', function () {

      function getResults(gen_1, gen_2) {
        const results_1 = [];
        const results_2 = [];
        results_1.push(gen_1.getFloat());
        results_1.push(gen_1.getFloat());
        results_1.push(gen_1.getFloat());
        results_1.push(gen_1.getFloat());
        results_2.push(gen_2.getFloat());
        results_2.push(gen_2.getFloat());
        results_2.push(gen_2.getFloat());
        results_2.push(gen_2.getFloat());
        return { results_1, results_2 };
      }

      it('returns different numbers in subsequent calls ' +
         'on one generator', function () {
        const rand = new Rand('fake-ns-x');
        const gen = rand.getGenerator(5);
        let i = 0;
        const results = [];
        while (i++ < 1000) {
          results.push(gen.getFloat());
        }
        expect(ArrayUtils.elementsAreDifferent(results)).toBeTrue();
      });

      it('returns the same sequence of different numbers ' +
         'in subsequent calls on two generators with the same args', function () {
        const rand = new Rand('fake-ns-x');
        const gen_1 = rand.getGenerator('w');
        const gen_2 = rand.getGenerator('w');
        const { results_1, results_2 } = getResults(gen_1, gen_2);
        expect(ArrayUtils.elementsAreDifferent(results_1)).toBeTrue();
        expect(ArrayUtils.elementsAreDifferent(results_2)).toBeTrue();
        expect(results_1).toEqual(results_2);
      });

      it('returns different sequences of different numbers ' +
         'in subsequent calls on two generators with different args', function () {
        const rand = new Rand('fake-ns-x');
        const gen_1 = rand.getGenerator('w');
        const gen_2 = rand.getGenerator('z');
        const { results_1, results_2 } = getResults(gen_1, gen_2);
        expect(ArrayUtils.elementsAreDifferent(results_1)).toBeTrue();
        expect(ArrayUtils.elementsAreDifferent(results_2)).toBeTrue();
        expect(results_1).not.toEqual(results_2);
      });

      it('returns the same sequence of different numbers ' +
         'in subsequent calls on two generators with the same args ' +
         'created by two different Rand objects, ' +
         'but with the same namespace and gameSeed', function () {
        const rand_1 = new Rand('fake-ns-y', 91298341);
        const rand_2 = new Rand('fake-ns-y', 91298341);
        const gen_1 = rand_1.getGenerator('w');
        const gen_2 = rand_2.getGenerator('w');
        const { results_1, results_2 } = getResults(gen_1, gen_2);
        expect(ArrayUtils.elementsAreDifferent(results_1)).toBeTrue();
        expect(ArrayUtils.elementsAreDifferent(results_2)).toBeTrue();
        expect(results_1).toEqual(results_2);
      });

      it('returns different sequences of different numbers ' +
         'in subsequent calls on two generators with the same args ' +
         'created by two different Rand objects, ' +
         'with the same namespace, but different gameSeed', function () {
        const rand_1 = new Rand('fake-ns-y', 91298341);
        const rand_2 = new Rand('fake-ns-y', 89781697);
        const gen_1 = rand_1.getGenerator('w');
        const gen_2 = rand_2.getGenerator('w');
        const { results_1, results_2 } = getResults(gen_1, gen_2);
        expect(ArrayUtils.elementsAreDifferent(results_1)).toBeTrue();
        expect(ArrayUtils.elementsAreDifferent(results_2)).toBeTrue();
        expect(results_1).not.toEqual(results_2);
      });

      it('returns different sequences of different numbers ' +
         'in subsequent calls on two generators with the same args ' +
         'created by two different Rand objects, ' +
         'with the same gameSeed, but different namespace', function () {
        const rand_1 = new Rand('fake-ns-y', 91298341);
        const rand_2 = new Rand('fake-ns-z', 91298341);
        const gen_1 = rand_1.getGenerator('w');
        const gen_2 = rand_2.getGenerator('w');
        const { results_1, results_2 } = getResults(gen_1, gen_2);
        expect(ArrayUtils.elementsAreDifferent(results_1)).toBeTrue();
        expect(ArrayUtils.elementsAreDifferent(results_2)).toBeTrue();
        expect(results_1).not.toEqual(results_2);
      });

    });

  });

  describe('getStatefulGenerator', function () {

    describe('getInt31Unsigned', function () {

      it('returns sequence of different numbers ' +
         'in subsequent calls on one generator', function () {
        const rand = new Rand('fake-ns');
        const gen = rand.getStatefulGenerator('gen');
        const results = [];
        for (let i = 0; i < 100; i++) {
          results.push(gen.getInt31Unsigned());
        }
        expect(results.length).toBe(100);
        expect(ArrayUtils.elementsAreDifferent(results)).toBeTrue();
      });

      it('returns two the same sequences of different numbers ' +
         'in subsequent calls on two the same generators', function () {
        const rand = new Rand('fake-ns');
        const gen_1 = rand.getStatefulGenerator('gen');
        const gen_2 = rand.getStatefulGenerator('gen');
        const [results_1, results_2] = [[], []];
        for (let i = 0; i < 100; i++) {
          results_1.push(gen_1.getInt31Unsigned());
          results_2.push(gen_2.getInt31Unsigned());
        }
        expect(results_1.length).toBe(100);
        expect(results_2.length).toBe(100);
        expect(ArrayUtils.elementsAreDifferent(results_1)).toBeTrue();
        expect(ArrayUtils.elementsAreDifferent(results_2)).toBeTrue();
        expect(results_1).toEqual(results_2);
      });

      it('returns two different sequences of different numbers ' +
         'in subsequent calls on two different generators', function () {
        const rand = new Rand('fake-ns');
        const gen_1 = rand.getStatefulGenerator('gen 1');
        const gen_2 = rand.getStatefulGenerator('gen 2');
        const [results_1, results_2] = [[], []];
        for (let i = 0; i < 100; i++) {
          results_1.push(gen_1.getInt31Unsigned());
          results_2.push(gen_2.getInt31Unsigned());
        }
        expect(results_1.length).toBe(100);
        expect(results_2.length).toBe(100);
        expect(ArrayUtils.elementsAreDifferent(results_1)).toBeTrue();
        expect(ArrayUtils.elementsAreDifferent(results_2)).toBeTrue();
        expect(results_1).not.toEqual(results_2);
      });

      it('returns two the same sequences of different numbers ' +
         'in subsequent calls on two the same generators ' +
         'with the same state (counter value)', function () {
        const rand = new Rand('fake-ns');
        const gen_1 = rand.getStatefulGenerator('gen');
        const gen_2 = rand.getStatefulGenerator('gen');
        gen_1.counter = 511;
        gen_2.counter = 511;
        const [results_1, results_2] = [[], []];
        for (let i = 0; i < 100; i++) {
          results_1.push(gen_1.getInt31Unsigned());
          results_2.push(gen_2.getInt31Unsigned());
        }
        expect(results_1.length).toBe(100);
        expect(results_2.length).toBe(100);
        expect(ArrayUtils.elementsAreDifferent(results_1)).toBeTrue();
        expect(ArrayUtils.elementsAreDifferent(results_2)).toBeTrue();
        expect(results_1).toEqual(results_2);
      });

      it('returns two different sequences of different numbers ' +
         'in subsequent calls on two the same generators ' +
         'with different state (counter value)', function () {
        const rand = new Rand('fake-ns');
        const gen_1 = rand.getStatefulGenerator('gen');
        const gen_2 = rand.getStatefulGenerator('gen');
        gen_1.counter = 511;
        gen_2.counter = 512;
        const [results_1, results_2] = [[], []];
        for (let i = 0; i < 100; i++) {
          results_1.push(gen_1.getInt31Unsigned());
          results_2.push(gen_2.getInt31Unsigned());
        }
        expect(results_1.length).toBe(100);
        expect(results_2.length).toBe(100);
        expect(ArrayUtils.elementsAreDifferent(results_1)).toBeTrue();
        expect(ArrayUtils.elementsAreDifferent(results_2)).toBeTrue();
        expect(results_1).not.toEqual(results_2);
      });

    });

  });

});
