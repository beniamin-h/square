'use strict';

describe('RandSchema', function() {
  beforeEach(module('square'));

  let Joi, Rand, RandSchema, RandGeneratorSchema, testingUtils,
    RandStatefulGeneratorSchema;

  beforeEach(inject(function (
      _Joi_, _Rand_, _RandSchema_, _RandGeneratorSchema_, _testingUtils_,
      _RandStatefulGeneratorSchema_) {
    Joi = _Joi_;
    Rand = _Rand_;
    RandSchema = _RandSchema_;
    RandGeneratorSchema = _RandGeneratorSchema_;
    testingUtils = _testingUtils_;
    RandStatefulGeneratorSchema = _RandStatefulGeneratorSchema_;
  }));

  describe('a new instance', function () {

    it('validates against the rand schema', function (done) {
      const rand = new Rand('fake-ns', Rand.getUnpredictableInt31Unsigned());
      testingUtils.validateAgainstSchema(rand, RandSchema, done);
    });

  });

  describe('a new instance without gameSeed', function () {

    it('validates against the rand schema', function (done) {
      const rand = new Rand('fake-ns');
      testingUtils.validateAgainstSchema(rand, RandSchema, done);
    });

  });

  describe('Generator', function () {

    it('validates against the rand generator schema', function (done) {
      const rand = new Rand('fake-ns', Rand.getUnpredictableInt31Unsigned());
      const gen = rand.getGenerator('fake-seed');
      testingUtils.validateAgainstSchema(gen, RandGeneratorSchema, done);
    });

  });

  describe('StatefulGenerator', function () {

    it('validates against the rand stateful generator schema', function (done) {
      const rand = new Rand('fake-ns', Rand.getUnpredictableInt31Unsigned());
      const gen = rand.getStatefulGenerator('fake-seed-2');
      testingUtils.validateAgainstSchema(gen, RandStatefulGeneratorSchema, done);
    });

  });

});