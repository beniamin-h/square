/**
 * Created by benek on 12/28/14.
 */
'use strict';

angular.module('square').factory('ArrayUtils', ['Math', function (Math) {

  return {
    range(n, start) {
      start = start || 0;
      return Array.apply(null, Array(n)).map(function (_, i) { return i + start; });
    },
    shuffleInplace(o) {
      for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
      return o;
    },
    shuffle(input) {
      var idx = this.range(input.length);
      var output = [];
      while (idx.length) {
        output.push(input[idx.splice(Math.floor(Math.random() * idx.length), 1)[0]]);
      }
      return output;
    },
    sum(input) {
      if (!input.length) {
        return 0;
      }
      return input.reduce(function (a, b) { return a + b; });
    },
    removeElement(arr, elem, throwErrorNotFound) {
      throwErrorNotFound = typeof(throwErrorNotFound) === 'undefined' ? false : throwErrorNotFound;
      var index = arr.indexOf(elem);
      if (index > -1) {
        arr.splice(index, 1);
        return true;
      } else {
        if (throwErrorNotFound) {
          throw new Error('ArrayUtils.removeElement(): Element ' + elem + ' has been not found in array ' + arr);
        }
        return false;
      }
    },
    hasElement(arr, elem) {
      return arr.indexOf(elem) !== -1;
    },
    copy(arr) {
      return arr.slice();
    },
    elementsAreEqual(arr, strict/*=true*/) {
      var result = arr.reduce(function (a, b) {
        return (a === b || (typeof strict !== 'undefined' && strict !== true && a == b)) ?
               a : NaN;
      });
      return result === result;  // NaN !== NaN
    },
    elementsAreDifferent(arr) {
      return arr.length === new Set(arr).size;
    },
    elementsDifference(arrA, arrB) {
      const difference = this.copy(arrA);
      for (const elemB of arrB) {
        this.removeElement(difference, elemB);
      }
      return difference;
    },
    _instance: this
  };
}]);
