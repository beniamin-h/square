'use strict';

angular.module('square').factory('StringUtils', [function () {

  return {
    repeat: function (str, count) {
      return new Array(count + 1).join(str);
    },
    _instance: this
  };
}]);
