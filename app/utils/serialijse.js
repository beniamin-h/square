'use strict';

angular.module('serialijse', [])
  .factory('Serialijse', ['$window', function ($window) {
    if ($window.serialijse) {
      $window._thirdParty = $window._thirdParty || {};
      $window._thirdParty.serialijse = $window.serialijse;
      delete $window.serialijse;
    }
    var serialijse = $window._thirdParty.serialijse;
    return serialijse.Serialijse;
  }]);