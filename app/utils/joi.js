'use strict';

var Joi = require('joi-browser');

angular.module('square')
  .factory('Joi', [function () {

  return Joi.extend({
    base: Joi.number(),
    name: 'number',
    language: {
      natural: 'must be integer equal or greater than 0'
    },
    rules: [
      {
        name: 'natural',
        validate: function (params, value, state, options) {
          if (typeof value !== 'number' || isNaN(value) ||  // valid number
              parseFloat(value) !== parseInt(value, 10) ||  // integer
              value < 0) {  // not negative
            return this.createError('number.natural', { v: value }, state, options);
          }

          return value;
        }
      }
    ]
  });

}]);

