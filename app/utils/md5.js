'use strict';

const md5 = require('md5');

angular.module('square').factory('md5', [function () {
  return md5;
}]);

