if (!process.env.exceptionsCaught) {
  process.on('uncaughtException', function (err) {
    console.error('\033[01;31mUncaught exception:\033[0m ' + (err.stack || err.message || err));
  });
  process.env.exceptionsCaught = true;
}