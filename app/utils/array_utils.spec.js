'use strict';

describe('ArrayUtils', function() {
  beforeEach(module('square'));

  var ArrayUtils;
  var Math_;

  beforeEach(inject(function (_ArrayUtils_, _Math_) {
    ArrayUtils = _ArrayUtils_;
    Math_ = _Math_;
  }));

  describe('range', function () {

    it('returns an array of 5 following numbers for n = 5', function () {
      var result = ArrayUtils.range(5);
      expect(result).toEqual([0, 1, 2, 3, 4]);
    });

    it('returns an empty array for n = 0', function () {
      var result = ArrayUtils.range(0);
      expect(result).toEqual([]);
    });

    it('returns [2, 3, 4, 5] for n = 4 and start = 2', function () {
      var result = ArrayUtils.range(4, 2);
      expect(result).toEqual([2, 3, 4, 5]);
    });

  });

  describe('shuffleInplace', function () {

    beforeEach(function () {
      spyOn(Math_, 'random').and.returnValue(0.01);
    });

    it('changes the order of an input array', function () {
      var input = [2, 3, 4];
      ArrayUtils.shuffleInplace(input);
      expect(input).toEqual([3, 4, 2]);
    });

    it('returns the changed input array', function () {
      var input = [2, 3, 4];
      var result = ArrayUtils.shuffleInplace(input);
      expect(input).toEqual([3, 4, 2]);
      expect(result).toBe(input);
    });

  });

  describe('shuffle', function () {

    beforeEach(function () {
      spyOn(Math_, 'random').and.returnValue(0.99);
    });

    it('returns an array of changed order', function () {
      var input = [2, 3, 4];
      var result = ArrayUtils.shuffle(input);
      expect(result).toEqual([4, 3, 2]);
    });

    it('does not change the order of an input array', function () {
      var input = [2, 3, 4];
      ArrayUtils.shuffle(input);
      expect(input).toEqual([2, 3, 4]);
    });

    it('does not throw an error for an empty array', function () {
      var input = [];
      var result = null;
      expect(function () {
        result = ArrayUtils.shuffle(input);
      }).not.toThrowError();
      expect(result).toEqual([]);
    });

  });

  describe('sum', function () {

    it('returns 10 for [1, 2, 3, 4]', function () {
      var result = ArrayUtils.sum([1, 2, 3, 4]);
      expect(result).toBe(10);
    });

    it('returns 5 for [8, -3]', function () {
      var result = ArrayUtils.sum([8, -3]);
      expect(result).toBe(5);
    });

    it('returns 0 for [-1, 1]', function () {
      var result = ArrayUtils.sum([-1, 1]);
      expect(result).toBe(0);
    });

    it('returns 2 for [2]', function () {
      var result = ArrayUtils.sum([2]);
      expect(result).toBe(2);
    });

    it('returns 0 for []', function () {
      var result = ArrayUtils.sum([]);
      expect(result).toBe(0);
    });

    it('returns "2b" for [2, "b"]', function () {
      var result = ArrayUtils.sum([2, 'b']);
      expect(result).toBe('2b');
    });

  });

  describe('removeElement', function () {

    describe('removes the given element from the given array', function () {

      it('[2, 4, 5]', function () {
        var arr = [2, 4, 5];
        ArrayUtils.removeElement(arr, 4);
        expect(arr).toEqual([2, 5]);
      });

      it('[0, 4, 1]', function () {
        var arr = [0, 4, 1];
        ArrayUtils.removeElement(arr, 0);
        expect(arr).toEqual([4, 1]);
      });

      it('["a", null, -0, Infinity]', function () {
        var arr = ["a", null, -0, Infinity];
        ArrayUtils.removeElement(arr, 0);
        expect(arr).toEqual(["a", null, Infinity]);
        ArrayUtils.removeElement(arr, null);
        expect(arr).toEqual(["a", Infinity]);
        ArrayUtils.removeElement(arr, Infinity);
        expect(arr).toEqual(["a"]);
        ArrayUtils.removeElement(arr, "a");
        expect(arr).toEqual([]);
      });

      it('of objects', function () {
        var obj1 = { '': 0 };
        var obj2 = { '': 0 };
        var arr = [obj1, obj2];
        ArrayUtils.removeElement(arr, obj2);
        expect(arr).toEqual([obj1]);
      });

    });

    it('does not throw an error if element is not in the given array ' +
       'and throwErrorNotFound is falsy', function () {
      var arr = ['1', 2];
      expect(function () {
        ArrayUtils.removeElement(arr, 3);
      }).not.toThrowError();
    });

    it('throws an error if element is not in the given array ' +
       'and throwErrorNotFound is truthy', function () {
      var arr = ['1', 2, 3];
      expect(function () {
        ArrayUtils.removeElement(arr, 1, true);
      }).toThrowError();
    });

    it('returns false if the given element is not in the given array', function () {
      var arr = ['a', 'b', 'c'];
      var result = ArrayUtils.removeElement(arr, 'd');
      expect(result).toBeFalse();
    });

    it('returns true if the given element is in the given array', function () {
      var arr = ['a', 'b', 'c'];
      var result = ArrayUtils.removeElement(arr, 'a');
      expect(result).toBeTrue();
    });

  });

  describe('hasElement', function () {

    it('returns true if element is in array', function () {
      var result = ArrayUtils.hasElement(['a', 2, 'b'], 'a');
      expect(result).toBeTrue();
    });

    it('returns false if element is not in array', function () {
      var result = ArrayUtils.hasElement(['a', 2, 'b'], 'c');
      expect(result).toBeFalse();
    });

    it('works the same way as indexOf', function () {
      expect(ArrayUtils.hasElement(['a', 2, 'b'], 'A')).toBeFalse();
      expect(ArrayUtils.hasElement(['a', 2, 'b'], '2')).toBeFalse();
      expect(ArrayUtils.hasElement([{}, 2, 'b'], {})).toBeFalse();
      expect(ArrayUtils.hasElement([[], 2, 'b'], [])).toBeFalse();
      expect(ArrayUtils.hasElement([true, 2, 'b'], true)).toBeTrue();
      expect(ArrayUtils.hasElement([0, 2, 'b'], -0)).toBeTrue();
    });

  });

  describe('copy', function () {

    it('returns a new array with all elements copied from the given array', function () {
      var obj = {};
      var inputArray = ['a', 0, null, obj, '-2'];
      var result = ArrayUtils.copy(inputArray);
      expect(result).not.toBe(inputArray);
      expect(result).toEqual(inputArray);
      expect(result[0]).toBe('a');
      expect(result[1]).toBe(0);
      expect(result[2]).toBe(null);
      expect(result[3]).toBe(obj);
      expect(result[4]).toBe('-2');
      result[3] = '4';
      expect(inputArray[3]).toBe(obj);
    });

  });

  describe('elementsAreEqual', function () {

    it('returns true if the given array\'s elements are equal', function () {
      var arr = ['a', 'a', 'a'];
      var result = ArrayUtils.elementsAreEqual(arr);
      expect(result).toBeTrue();
    });

    it('returns false if the given array\'s elements are not equal', function () {
      var arr = ['a', 'b', 'z'];
      var result = ArrayUtils.elementsAreEqual(arr);
      expect(result).toBeFalse();
    });

    it('returns false if at least one element of the given array\'s is not equal', function () {
      var arr = ['a', 'b', 'b', 'b'];
      var result = ArrayUtils.elementsAreEqual(arr);
      expect(result).toBeFalse();
    });

    it('returns true for an array of one element', function () {
      var arr = ['a'];
      var result = ArrayUtils.elementsAreEqual(arr);
      expect(result).toBeTrue();
    });

    it('returns true for an array of one falsy element', function () {
      var arr = [0];
      var result = ArrayUtils.elementsAreEqual(arr);
      expect(result).toBeTrue();
    });

    it('returns true for an array of false elements', function () {
      var arr = [false, false, false];
      var result = ArrayUtils.elementsAreEqual(arr);
      expect(result).toBeTrue();
    });

    it('returns true for an array of logically equal elements ' +
       'if param strict is not true', function () {
      var arr = [1, '1', true];
      var result = ArrayUtils.elementsAreEqual(arr, false);
      expect(result).toBeTrue();
    });

    it('returns false for an array non-strictly equal elements ' +
       'if param strict is true', function () {
      var arr = [1, '1', true];
      var result = ArrayUtils.elementsAreEqual(arr, true);
      expect(result).toBeFalse();
    });

    it('returns false for an array non-strictly equal elements ' +
       'if param strict is not given', function () {
      var arr = [1, '1', true];
      var result = ArrayUtils.elementsAreEqual(arr);
      expect(result).toBeFalse();
    });

    it('returns true for an array of non-strictly equal falsy elements ' +
       'if param strict is false', function () {
      var arr = [false, 0, false];
      var result = ArrayUtils.elementsAreEqual(arr, false);
      expect(result).toBeTrue();
    });

  });

  describe('elementsAreDifferent', function () {

    describe('returns true', function () {

      it('for the given array: []', function () {
        const result = ArrayUtils.elementsAreDifferent([]);
        expect(result).toBeTrue();
      });

      it('for the given array: [true]', function () {
        const result = ArrayUtils.elementsAreDifferent([true]);
        expect(result).toBeTrue();
      });

      it('for the given array: [false]', function () {
        const result = ArrayUtils.elementsAreDifferent([false]);
        expect(result).toBeTrue();
      });

      it('for the given array: [true, false]', function () {
        const result = ArrayUtils.elementsAreDifferent([true, false]);
        expect(result).toBeTrue();
      });

      it('for the given array: [true, 1]', function () {
        const result = ArrayUtils.elementsAreDifferent([true, 1]);
        expect(result).toBeTrue();
      });

      it('for the given array: [true, {}, {}]', function () {
        const result = ArrayUtils.elementsAreDifferent([true, {}, {}]);
        expect(result).toBeTrue();
      });

      it('for the given array: ["a", "b"]', function () {
        const result = ArrayUtils.elementsAreDifferent(['a', 'b']);
        expect(result).toBeTrue();
      });

      it('for the given array: [1, 2]', function () {
        const result = ArrayUtils.elementsAreDifferent([1, 2]);
        expect(result).toBeTrue();
      });

      it('for the given array: [[], new Set(), {}]', function () {
        const result = ArrayUtils.elementsAreDifferent([[], new Set(), {}]);
        expect(result).toBeTrue();
      });

    });

    describe('returns false', function () {

      it('for the given array: [true, true]', function () {
        const result = ArrayUtils.elementsAreDifferent([true, true]);
        expect(result).toBeFalse();
      });

      it('for the given array: [true, true, true]', function () {
        const result = ArrayUtils.elementsAreDifferent([true, true, true]);
        expect(result).toBeFalse();
      });

      it('for the given array: [false, false]', function () {
        const result = ArrayUtils.elementsAreDifferent([false, false]);
        expect(result).toBeFalse();
      });

      it('for the given array: [0, 0]', function () {
        const result = ArrayUtils.elementsAreDifferent([0, 0]);
        expect(result).toBeFalse();
      });

      it('for the given array: [Infinity, Infinity]', function () {
        const result = ArrayUtils.elementsAreDifferent([Infinity, Infinity]);
        expect(result).toBeFalse();
      });

      it('for the given array: ["a", "a"]', function () {
        const result = ArrayUtils.elementsAreDifferent(["a", "a"]);
        expect(result).toBeFalse();
      });

      it('for the given array: ["asdf", "asdf"]', function () {
        const result = ArrayUtils.elementsAreDifferent(["asdf", "asdf"]);
        expect(result).toBeFalse();
      });

      it('for the given array: [elem, elem], where elem is {}', function () {
        const elem = {};
        const result = ArrayUtils.elementsAreDifferent([elem, elem]);
        expect(result).toBeFalse();
      });

      it('for the given array: [elem, elem, elem, elem], ' +
         'where elem is new Set()', function () {
        const elem = new Set();
        const result = ArrayUtils.elementsAreDifferent([elem, elem, elem, elem]);
        expect(result).toBeFalse();
      });

    });

  });

  describe('elementsDifference', function () {

    it('returns [A] for input: [A, B], [B]', function () {
      const result = ArrayUtils.elementsDifference(['A', 'B'], ['B']);
      expect(result).toEqual(['A']);
    });

    it('returns [B] for input: [A, B], [A]', function () {
      const result = ArrayUtils.elementsDifference(['A', 'B'], ['A']);
      expect(result).toEqual(['B']);
    });

    it('returns [A, B] for input: [A, B], []', function () {
      const result = ArrayUtils.elementsDifference(['A', 'B'], []);
      expect(result).toEqual(['A', 'B']);
    });

    it('returns [A, B] for input: [A, B], [C]', function () {
      const result = ArrayUtils.elementsDifference(['A', 'B'], ['C']);
      expect(result).toEqual(['A', 'B']);
    });

    it('returns [] for input: [A, B], [A, B]', function () {
      const result = ArrayUtils.elementsDifference(['A', 'B'], ['A', 'B']);
      expect(result).toEqual([]);
    });

    it('returns [] for input: [A], [A, B]', function () {
      const result = ArrayUtils.elementsDifference(['A'], ['A', 'B']);
      expect(result).toEqual([]);
    });

    it('returns [] for input: [B], [A, B]', function () {
      const result = ArrayUtils.elementsDifference(['B'], ['A', 'B']);
      expect(result).toEqual([]);
    });

    it('returns [] for input: [], [A, B]', function () {
      const result = ArrayUtils.elementsDifference([], ['A', 'B']);
      expect(result).toEqual([]);
    });

    it('returns [] for input: [], []', function () {
      const result = ArrayUtils.elementsDifference([], []);
      expect(result).toEqual([]);
    });

    it('returns [X, Y] for input: [X, Y, Z], [A, Z, C]', function () {
      const result = ArrayUtils.elementsDifference(['X', 'Y', 'Z'], ['A', 'Z', 'C']);
      expect(result).toEqual(['X', 'Y']);
    });

    it('does not change input arrays', function () {
      const [arrA, arrB] = [['X', 'Y', 'Z'], ['A', 'Z', 'C']];
      ArrayUtils.elementsDifference(arrA, arrB);
      expect(arrA).toEqual(['X', 'Y', 'Z']);
      expect(arrB).toEqual(['A', 'Z', 'C']);
    });

    it('does not return input arrays', function () {
      const [arrA, arrB] = [['X', 'Y', 'Z'], ['C']];
      const result = ArrayUtils.elementsDifference(arrA, arrB);
      expect(result).not.toBe(arrA);
      expect(result).not.toBe(arrB);
    });

  });

});
