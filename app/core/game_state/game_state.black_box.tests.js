'use strict';

describe('GameState', function () {
  beforeEach(module('square'));

  var GameState, Serialijse, fs,
    Water, Coalfield, WildHorses, GoldDeposit, SilverDeposit, GemstoneDeposit,
    Plain, Mountain, Hill, Forest,
    Player, HumanPlayer, AIPlayer, Storage,
    Map, Region, Block, StorageResourceShortage,
    playerProvider, storageProvider, mapProvider, AI, MapBlocks,
    Ticker, Rand, EventEmitter, turnTicker;

  beforeEach(inject(function (_GameState_, _Serialijse_, _fs_,
      _Water_, _Coalfield_, _WildHorses_, _GoldDeposit_, _SilverDeposit_, _GemstoneDeposit_,
      _Plain_, _Mountain_, _Hill_, _Forest_,
      _Player_, _HumanPlayer_, _AIPlayer_, _Storage_,
      _Map_, _Region_, _Block_, _StorageResourceShortage_,
      _playerProvider_, _storageProvider_, _mapProvider_, _AI_, _MapBlocks_,
      _Ticker_, _Rand_, _turnTicker_) {
    GameState = _GameState_;
    Serialijse = _Serialijse_;
    fs = _fs_;

    Water = _Water_;
    Coalfield = _Coalfield_;
    WildHorses = _WildHorses_;
    GoldDeposit = _GoldDeposit_;
    SilverDeposit = _SilverDeposit_;
    GemstoneDeposit = _GemstoneDeposit_;

    Plain = _Plain_;
    Mountain = _Mountain_;
    Hill = _Hill_;
    Forest = _Forest_;

    Player = _Player_;
    HumanPlayer = _HumanPlayer_;
    AIPlayer = _AIPlayer_;
    Storage = _Storage_;

    Map = _Map_;
    Region = _Region_;
    Block = _Block_;
    StorageResourceShortage = _StorageResourceShortage_;

    playerProvider = _playerProvider_;
    storageProvider = _storageProvider_;
    mapProvider = _mapProvider_;
    AI = _AI_;
    MapBlocks = _MapBlocks_;

    Ticker = _Ticker_;
    Rand = _Rand_;
    EventEmitter = require('events');
    turnTicker = _turnTicker_;
  }));

  describe('constructor', function () {

    var gs;

    beforeEach(function () {
      spyOn(Serialijse.prototype, 'declarePersistable').and.callThrough();
      expect(Serialijse.prototype.declarePersistable).not.toHaveBeenCalled();
      gs = new GameState();
      expect(gs).toHaveNonEmptyObject('serialijse');
      expect(gs.serialijse instanceof Serialijse).toBeTrue();
      expect(Serialijse.prototype.declarePersistable).toHaveBeenCalled();
    });

    function checkSerialization(obj, done) {
      gs.serialijse.serialize(obj, function (err, data) {
        if (err) {
          done.fail(err);
          return;
        }
        expect(data).toBeTruthy();
        done();
      });
    }

    it('declares Water persistable for serialization', function (done) {
      checkSerialization(new Water(), done);
    });

    it('declares Coalfield persistable for serialization', function (done) {
      checkSerialization(new Coalfield(), done);
    });

    it('declares WildHorses persistable for serialization', function (done) {
      checkSerialization(new WildHorses(), done);
    });

    it('declares GoldDeposit persistable for serialization', function (done) {
      checkSerialization(new GoldDeposit(), done);
    });

    it('declares SilverDeposit persistable for serialization', function (done) {
      checkSerialization(new SilverDeposit(), done);
    });

    it('declares GemstoneDeposit persistable for serialization', function (done) {
      checkSerialization(new GemstoneDeposit(), done);
    });

    it('declares Plain persistable for serialization', function (done) {
      checkSerialization(new Plain(), done);
    });

    it('declares Mountain persistable for serialization', function (done) {
      checkSerialization(new Mountain(), done);
    });

    it('declares Hill persistable for serialization', function (done) {
      checkSerialization(new Hill(), done);
    });

    it('declares Forest persistable for serialization', function (done) {
      checkSerialization(new Forest(), done);
    });

    it('declares Block persistable for serialization', function (done) {
      checkSerialization(new Block(), done);
    });

    it('declares Region persistable for serialization', function (done) {
      checkSerialization(new Region(), done);
    });

    it('declares Map persistable for serialization', function (done) {
      checkSerialization(new Map(), done);
    });

    it('declares AIPlayer persistable for serialization', function (done) {
      checkSerialization(new AIPlayer(), done);
    });

    it('declares HumanPlayer persistable for serialization', function (done) {
      checkSerialization(new HumanPlayer(), done);
    });

    it('declares Storage persistable for serialization', function (done) {
      checkSerialization(new Storage(), done);
    });

    it('declares AI persistable for serialization', function (done) {
      checkSerialization(new AI(), done);
    });

    it('declares MapBlocks persistable for serialization', function (done) {
      checkSerialization(new MapBlocks(), done);
    });

    it('declares StorageResourceShortage persistable for serialization', function (done) {
      checkSerialization(new StorageResourceShortage(), done);
    });

    it('declares Ticker persistable for serialization', function (done) {
      checkSerialization(new Ticker(), done);
    });

    it('declares Rand unserializable for serialization', function (done) {
      checkSerialization(new Rand('fake-ns'), done);
    });

    it('declares EventEmitter unserializable for serialization', function (done) {
      checkSerialization(new EventEmitter(), done);
    });

    it('does not declare GameState persistable for serialization', function (done) {
      gs.serialijse.serialize(new GameState(), function (err) {
        expect(err).toBeTruthy();
        done();
      });
    });
  });

  describe('load', function () {

    var fakeGameSave, fakeError;

    beforeEach(function () {
      spyOn(fs, 'readFile').and.callFake(function (filename, encoding, callback) {
        setTimeout(function () { callback(fakeError, fakeGameSave); }, 10);
      });
      spyOn(mapProvider, 'loadMap');
    });

    it('returns SyntaxError if game save is corrupted', function (done) {
      var gs = new GameState();
      fakeGameSave = '[[{"c":"Object","d":{}}],{"o":0}:::::';
      fakeError = null;
      gs.load('fakeFilename', function (err) {
        expect(err instanceof SyntaxError).toBeTrue();
        done();
      });
    });

    it('sets proper Region.blocksWidth', function (done) {
      var gs = new GameState();
      fakeGameSave = '[[{"c":"Object","d":{"regionBlocksWidth": 9996,"squares":""}}],{"o":0}]';
      fakeError = null;
      gs.load('fakeFilename', function (err) {
        if (err) {
          done.fail(err);
        } else {
          expect(Region.blocksWidth).toBe(9996);
          done();
        }
      });
    });

    it('sets proper Region.blocksHeight', function (done) {
      var gs = new GameState();
      fakeGameSave = '[[{"c":"Object","d":{"regionBlocksHeight": 654321,"squares":""}}],{"o":0}]';
      fakeError = null;
      gs.load('fakeFilename', function (err) {
        if (err) {
          done.fail(err);
        } else {
          expect(Region.blocksHeight).toBe(654321);
          done();
        }
      });
    });

    it('sets proper players', function (done) {
      var gs = new GameState();
      fakeGameSave = JSON.stringify([[
          {"c": "Object", "d": {"players": {"o": 1}, "squares": ""}},
          {"c": "Object", "d": {"player-human": {"o": 2},"player-AI-0": {"o": 3}}},
          {"c": "HumanPlayer", "d": {"playerId": "player-human"}},
          {"c": "AIPlayer", "d": {"playerId": "player-AI-0"}}
        ], {"o": 0}]);
      fakeError = null;
      expect(playerProvider.getAllPlayers()).toBeEmptyObject();
      gs.load('fakeFilename', function (err) {
        if (err) {
          done.fail(err);
          return;
        }
        var players = playerProvider.getAllPlayers();
        expect(players).toBeNonEmptyObject();
        expect(Object.keys(players)
          .map(function (playerId) {
            return players[playerId] instanceof Player;
          })
          .reduce(function (a, b) {
            return a && b;
          }))
          .toBeTrue();
        done();
      });
    });

    it('sets a proper map', function (done) {
      var gs = new GameState();
      fakeGameSave = JSON.stringify([[
        {"c": "Object", "d": {"players": {"o": 1}, "storages": {"o": 11}, "map": {"o": 4}, "squares": ""}},
        {"c": "Object", "d": {"player-human": {"o": 2}, "player-AI-0": {"o": 3}}},
        {"c": "HumanPlayer", "d": {"playerId": "player-human"}},
        {"c": "AIPlayer", "d": {"playerId": "player-AI-0"}},
        {"c": "Map", "d": {
          "regions": {"o": 5},
          "squares": {"o": 6},
          "improvedBlocksCounter": {"o": 7},
          "improvementNeighbors": {"o": 8},
          "affordableBlockCount": {"o": 9},
          "regionWidthPx": 7777,
          "regionHeightPx": 888,
          "borderRegionsCoords": {"o": 10},
          "initSizeRegions": 0,
          "players": {"o": 1}
        }},
        {"c": "Object", "d": {"fake-region": "fake-region-data"}},
        {"c": "Object", "d": {"fake-squares": "fake-squares-data"}},
        {"c": "Object", "d": {"fake-improvedBlocksCounter": "fake-improvedBlocksCounter-data"}},
        {"c": "Object", "d": {"fake-improvementNeighbors": "fake-improvementNeighbors-data"}},
        {"c": "Object", "d": {"fake-affordableBlockCount": "fake-affordableBlockCount-data"}},
        {"c": "Object", "d": {"fake-borderRegionsCoords": "fake-borderRegionsCoords-data"}},
        {"c": "Object", "d": {"player-human": {"o": 12}, "player-AI-0": {"o": 13}}},
        {"c": "Storage", "d": {"resources": {"c": "Object", "d": {}}, "owner": {"o": 2}}},
        {"c": "Storage", "d": {"resources": {"c": "Object", "d": {}}, "owner": {"o": 3}}}
      ], {"o": 0}]);
      fakeError = null;
      mapProvider.loadMap.and.callThrough();
      expect(mapProvider.getMap()).toBeNull();
      gs.load('fakeFilename', function (err) {
        if (err) {
          done.fail(err);
          return;
        }
        var map = mapProvider.getMap();
        expect(map instanceof Map).toBeTrue();
        expect(mapProvider.getRegionWidth()).toBe(7777);
        expect(mapProvider.getRegionHeight()).toBe(888);
        expect(map.regions).toHaveMember('fake-region');
        expect(map.regions['fake-region']).toBe('fake-region-data');
        expect(map.squares).toHaveMember('fake-squares');
        expect(map.squares['fake-squares']).toBe('fake-squares-data');
        expect(map.improvedBlocksCounter).toHaveMember('fake-improvedBlocksCounter');
        expect(map.improvedBlocksCounter['fake-improvedBlocksCounter']).toBe('fake-improvedBlocksCounter-data');
        expect(map.improvementNeighbors).toHaveMember('fake-improvementNeighbors');
        expect(map.improvementNeighbors['fake-improvementNeighbors']).toBe('fake-improvementNeighbors-data');
        expect(map.affordableBlockCount).toHaveMember('fake-affordableBlockCount');
        expect(map.affordableBlockCount['fake-affordableBlockCount']).toBe('fake-affordableBlockCount-data');
        expect(map.borderRegionsCoords).toHaveMember('fake-borderRegionsCoords');
        expect(map.borderRegionsCoords['fake-borderRegionsCoords']).toBe('fake-borderRegionsCoords-data');
        expect(map.initSizeRegions).toBe(0);
        expect(map.players).toBe(playerProvider.getAllPlayers());
        expect(map.players['player-human'].eventEmitter).toBeTruthy();
        expect(map.players['player-AI-0'].eventEmitter).toBeTruthy();
        expect(map.blockExhaustingThreshold).toBe(Map.prototype.blockExhaustingThreshold);
        expect(map.blockWidthPx).toBe(Map.prototype.blockWidthPx);
        expect(map.blockHeightPx).toBe(Map.prototype.blockHeightPx);
        done();
      });
    });

    it('sets proper storages', function (done) {
      var gs = new GameState();
      fakeGameSave = JSON.stringify([[
          {"c": "Object", "d": {"players": {"o": 1}, "storages": {"o": 4}, "squares": ""}},
          {"c": "Object", "d": {"player-human": {"o": 2}, "player-AI-0": {"o": 3}}},
          {"c": "HumanPlayer", "d": {"playerId": "player-human"}},
          {"c": "AIPlayer", "d": {"playerId": "player-AI-0"}},
          {"c": "Object", "d": {"player-human": {"o": 5}, "player-AI-0": {"o": 7}}},
          {"c": "Storage", "d": {"resources": {"o": 6}, "owner": {"o": 2}}},
          {"c": "Object", "d": {"wood": 8, "stone": 0, "metal": 44, "food": 2}},
          {"c": "Storage", "d": {"resources": {"o": 8}, "owner": {"o": 3}}},
          {"c": "Object", "d": {"wood": 0, "stone": 0, "metal": 0, "food": 0}}
        ], {"o": 0}]);
      fakeError = null;
      expect(storageProvider.getAllStorages()).toBeUndefined();
      gs.load('fakeFilename', function (err) {
        if (err) {
          done.fail(err);
          return;
        }
        var storages = storageProvider.getAllStorages();
        expect(storages).toBeNonEmptyObject();
        expect(Object.keys(storages)
          .map(function (playerId) {
            return storages[playerId] instanceof Storage
          })
          .reduce(function (a, b) {
            return a && b;
          }))
          .toBeTrue();
        expect(Object.keys(storages)
          .map(function (playerId) {
            return playerProvider.getById(playerId) instanceof Player;
          })
          .reduce(function (a, b) {
            return a && b;
          }))
          .toBeTrue();
        expect(storageProvider.getResources(playerProvider.getById('player-human')).wood).toBe(8);
        expect(storageProvider.getResources(playerProvider.getById('player-human')).food).toBe(2);
        expect(storageProvider.getResources(playerProvider.getById('player-human')).metal).toBe(44);
        expect(storageProvider.getResources(playerProvider.getById('player-human')).stone).toBe(0);
        expect(storageProvider.getResources(playerProvider.getById('player-AI-0')).wood).toBe(0);
        expect(storageProvider.getResources(playerProvider.getById('player-AI-0')).food).toBe(0);
        expect(storageProvider.getResources(playerProvider.getById('player-AI-0')).metal).toBe(0);
        expect(storageProvider.getResources(playerProvider.getById('player-AI-0')).stone).toBe(0);
        expect(storageProvider.getResources(playerProvider.getById('player-AI-0')).stone).toBe(0);
        expect(storages['player-human'].owner).toBe(playerProvider.getById('player-human'));
        expect(storages['player-AI-0'].owner).toBe(playerProvider.getById('player-AI-0'));
        expect(storages['player-human'].eventEmitter).toBeTruthy();
        expect(storages['player-AI-0'].eventEmitter).toBeTruthy();
        done();
      });
    });

    it('sets proper turnTicker', function (done) {
      const gs = new GameState();
      fakeGameSave = JSON.stringify([[
          {"c": "Object", "d": {"turnTicker": {"o": 1}, "squares": ""}},
          {"c": "Ticker", "d": {"tickStamp": 8}}
        ], {"o": 0}]);
      fakeError = null;
      expect(turnTicker.getTicker()).toBeUndefined();
      gs.load('fakeFilename', function (err) {
        if (err) {
          done.fail(err);
          return;
        }
        const ticker = turnTicker.getTicker();
        expect(ticker).toBeNonEmptyObject();
        expect(ticker.tickStamp).toBe(8);
        done();
      });
    });

  });

});