'use strict';

angular.module('square')

.factory('GameStateSchema', ['Joi', 'playerIdRegExp', 'StorageSchema', 'PlayerSchema', 'MapSchema',
                             'SquareFactorySchema', 'SquaresMapSchema', 'TickerSchema',
    function (Joi, playerIdRegExp, StorageSchema, PlayerSchema, MapSchema,
              SquareFactorySchema, SquaresMapSchema, TickerSchema) {

  return Joi.object().keys({
    regionBlocksWidth: Joi.number().positive(),
    regionBlocksHeight: Joi.number().positive(),
    storages: Joi.object({})
      .pattern(playerIdRegExp, StorageSchema),
    players: Joi.object({})
      .pattern(playerIdRegExp, PlayerSchema),
    squares: Joi.object({
      factory: SquareFactorySchema,
      map: SquaresMapSchema
    }),
    map: MapSchema,
    turnTicker: TickerSchema
  });

}]);
