'use strict';

var EventEmitter = require('events');

angular.module('square').factory('GameState', [
    'Serialijse', 'fs', 'Water', 'Coalfield', 'WildHorses', 'GoldDeposit', 'SilverDeposit', 'GemstoneDeposit',
    'Plain', 'Mountain', 'Hill', 'Forest', 'Block', 'Region', 'Map',
    'HumanPlayer', 'AIPlayer', 'Storage', 'mapProvider', 'storageProvider', 'playerProvider', 'AI', 'Square',
    'AISquaring', 'SquareFactory', 'SquaresMap', 'squareProvider', 'MapBlocks',
    'StorageResourceShortage', 'turnTicker', 'Ticker', 'Rand', 'RandStatefulGenerator',
    function (Serialijse, fs, Water, Coalfield, WildHorses, GoldDeposit, SilverDeposit, GemstoneDeposit,
              Plain, Mountain, Hill, Forest, Block, Region, Map,
              HumanPlayer, AIPlayer, Storage, mapProvider, storageProvider, playerProvider, AI, Square,
              AISquaring, SquareFactory, SquaresMap, squareProvider, MapBlocks,
              StorageResourceShortage, turnTicker, Ticker, Rand, RandStatefulGenerator) {

  function GameState() {
    this.serialijse = new Serialijse();
    this.serialijse.declarePersistable(Water);
    this.serialijse.declarePersistable(Coalfield);
    this.serialijse.declarePersistable(WildHorses);
    this.serialijse.declarePersistable(GoldDeposit);
    this.serialijse.declarePersistable(SilverDeposit);
    this.serialijse.declarePersistable(GemstoneDeposit);
    this.serialijse.declarePersistable(Plain);
    this.serialijse.declarePersistable(Mountain);
    this.serialijse.declarePersistable(Hill);
    this.serialijse.declarePersistable(Forest);
    this.serialijse.declarePersistable(Block);
    this.serialijse.declarePersistable(Region);
    this.serialijse.declarePersistable(Map);
    this.serialijse.declarePersistable(AIPlayer);
    this.serialijse.declarePersistable(HumanPlayer);
    this.serialijse.declarePersistable(Storage);
    this.serialijse.declarePersistable(AI);
    this.serialijse.declarePersistable(Square);
    this.serialijse.declarePersistable(Int8Array);
    this.serialijse.declarePersistable(AISquaring);
    this.serialijse.declarePersistable(SquareFactory);
    this.serialijse.declarePersistable(SquaresMap);
    this.serialijse.declarePersistable(MapBlocks);
    this.serialijse.declarePersistable(StorageResourceShortage);
    this.serialijse.declarePersistable(Ticker);
    this.serialijse.declarePersistable(RandStatefulGenerator);
    this.serialijse.declarePersistable(Rand);
    this.serialijse.declareUnserializable(EventEmitter);
  }

  GameState.prototype.serialijse = null;

  GameState.prototype.save = function (filename, callback) {
    var eventEmitter = new EventEmitter();
    eventEmitter.on('serializing', function (className) {
      //TODO: Progress bar
    });
    this.serialijse.serialize({
      map: mapProvider.getMap(),
      regionBlocksWidth: Region.blocksWidth,
      regionBlocksHeight: Region.blocksHeight,
      storages: storageProvider.getAllStorages(),
      players: playerProvider.getAllPlayers(),
      squares: {
        factory: squareProvider.getSquareFactory(),
        map: squareProvider.getSquaresMap()
      },
      turnTicker: turnTicker.getTicker()
    }, function (err, data) {
      if (!err) {
        fs.writeFile(filename, data, 'utf8', callback);
      } else {
        callback(err);
      }
    }, angular.toJson, eventEmitter);
  };

  GameState.prototype.load = function (filename, callback) {
    var that = this;
    fs.readFile(filename, 'utf8', function (readFileErr, data) {
      if (readFileErr) {
        callback(readFileErr);
        return;
      }
      try {
        var obj = that.serialijse.deserialize(data);
        Region.blocksWidth = obj.regionBlocksWidth;
        Region.blocksHeight = obj.regionBlocksHeight;
        playerProvider.load(obj.players);
        storageProvider.loadStorages(obj.storages);
        mapProvider.loadMap(obj.map);
        squareProvider.loadSquareFactory(obj.squares.factory);
        squareProvider.loadSquaresMap(obj.squares.map);
        turnTicker.load(obj.turnTicker);
      } catch (err) {
        callback(err);
        return;
      }
      callback();
    });
  };

  return GameState;
}]);