'use strict';

var async = require("async");

describe('GameStateSchema', function () {
  beforeEach(module('square'));

  var GameState, fs, Player, HumanPlayer, Storage,
    Map, Square, playerProvider, storageProvider, mapProvider,
    Joi, MapSchema, PlayerSchema, StorageSchema, GameStateSchema;

  beforeEach(inject(function (_GameState_, _fs_, _Player_, _HumanPlayer_, _Storage_,
                              _Map_, _Square_, _playerProvider_, _storageProvider_, _mapProvider_,
                              _Joi_, _MapSchema_, _PlayerSchema_, _StorageSchema_, _GameStateSchema_) {
    GameState = _GameState_;
    fs = _fs_;
    Player = _Player_;
    HumanPlayer = _HumanPlayer_;
    Storage = _Storage_;

    Map = _Map_;
    Square = _Square_;
    playerProvider = _playerProvider_;
    storageProvider = _storageProvider_;
    mapProvider = _mapProvider_;

    Joi = _Joi_;
    MapSchema = _MapSchema_;
    PlayerSchema = _PlayerSchema_;
    StorageSchema = _StorageSchema_;
    GameStateSchema = _GameStateSchema_;
  }));

  describe('load', function () {

    var fakeGameSave, fakeError;

    beforeEach(function () {
      spyOn(fs, 'readFile').and.callFake(function (filename, encoding, callback) {
        setTimeout(function () { callback(fakeError, fakeGameSave); }, 10);
      });
    });

    it('properly loads simple_map_3x3_5x5.json test fixture', function (done) {
      var gs = new GameState();
      fakeGameSave = readJSON('test_fixtures/simple_map_3x3_5x5.json');
      fakeError = null;
      expect(mapProvider.getMap()).toBeNull();
      expect(storageProvider.getAllStorages()).toBeUndefined();
      expect(playerProvider.getAllPlayers()).toBeEmptyObject();
      gs.load('fakeFilename', function () {
        var players = playerProvider.getAllPlayers();
        var storages = storageProvider.getAllStorages();
        var map = mapProvider.getMap();
        expect(typeof map).toBe('object');
        expect(typeof storages).toBe('object');
        expect(typeof players).toBe('object');
        expect(map instanceof Map).toBeTrue();
        expect(Object.keys(storages)
          .map(function (playerId) {
            return storages[playerId] instanceof Storage;
          })
          .reduce(function (a, b) {
            return a && b;
          }))
          .toBeTrue();
        expect(Object.keys(players)
          .map(function (playerId) {
            return players[playerId] instanceof Player;
          })
          .reduce(function (a, b) {
            return a && b;
          }))
          .toBeTrue();

        var humanPlayer = playerProvider.getById('player-human');
        var aiPlayer = playerProvider.getById('player-AI-0');

        async.parallel([function (callback) {
          Joi.validate(map, MapSchema,
              {presence: 'required', convert: false}, function (validation_error) {
            callback(validation_error);
          });
        }, function (callback) {
          Joi.validate(humanPlayer, PlayerSchema,
              {presence: 'required', convert: false}, function (validation_error) {
            callback(validation_error);
          });
        }, function (callback) {
          Joi.validate(aiPlayer, PlayerSchema,
              {presence: 'required', convert: false}, function (validation_error) {
            callback(validation_error);
          });
        }, function (callback) {
          Joi.validate(storages['player-human'], StorageSchema,
              {presence: 'required', convert: false}, function (validation_error) {
            callback(validation_error);
          });
        }, function (callback) {
          Joi.validate(storages['player-human'], StorageSchema,
              {presence: 'required', convert: false}, function (validation_error) {
            callback(validation_error);
          });
        }], function (err) {
          if (err) {
            done.fail(err);
          } else {
            done();
          }
        });
      });
    });

    describe('with simple_map_3x3_5x5.json test fixture loads a map valid against the map schema', function () {

      it('with one block improved', function (done) {
        var gs = new GameState();
        fakeGameSave = readJSON('test_fixtures/simple_map_3x3_5x5.json');
        fakeError = null;
        expect(mapProvider.getMap()).toBeNull();
        gs.load('fakeFilename', function () {
          var map = mapProvider.getMap();
          var player = playerProvider.getHuman();
          expect(map instanceof Map).toBeTrue();
          expect(player instanceof HumanPlayer).toBeTrue();
          var plainBlock = map.getBlock(1, 0, 0, 2);
          expect(plainBlock.level).toBe(0);
          var improved = map.tryToCaptureBlock(plainBlock, player);
          expect(improved).toBeTrue();
          expect(plainBlock.level).toBe(1);
          Joi.validate(map, MapSchema, {presence: 'required', convert: false}, function (validation_error) {
            if (validation_error) {
              done.fail(validation_error);
            } else {
              done();
            }
          });
        });
      });

      it('with three blocks improved', function (done) {
        var gs = new GameState();
        fakeGameSave = readJSON('test_fixtures/simple_map_3x3_5x5.json');
        fakeError = null;
        expect(mapProvider.getMap()).toBeNull();
        gs.load('fakeFilename', function () {
          var map = mapProvider.getMap();
          var player = playerProvider.getHuman();
          expect(map instanceof Map).toBeTrue();
          expect(player instanceof HumanPlayer).toBeTrue();
          var foodBlock = map.getBlock(1, 0, 0, 2);
          var metalBlock = map.getBlock(1, 0, 0, 1);
          var stoneBlock = map.getBlock(0, 0, 4, 1);
          expect([foodBlock, metalBlock, stoneBlock]
            .map(function levelEquals1(block) { return block.level === 0; })
            .reduce(function all(a, b) { return a && b; }))
            .toBeTrue();
          map.tryToCaptureBlock(foodBlock, player);
          map.tryToCaptureBlock(metalBlock, player);
          map.tryToCaptureBlock(stoneBlock, player);
          expect([foodBlock, metalBlock, stoneBlock]
            .map(function levelEquals1(block) { return block.level === 1; })
            .reduce(function all(a, b) { return a && b; }))
            .toBeTrue();
          expect(foodBlock !== metalBlock).toBeTrue();
          expect(foodBlock !== stoneBlock).toBeTrue();
          expect(metalBlock !== stoneBlock).toBeTrue();
          Joi.validate(map, MapSchema, {presence: 'required', convert: false}, function (validation_error) {
            if (validation_error) {
              done.fail(validation_error);
            } else {
              done();
            }
          });
        });
      });

      it('with one square', function (done) {
        var gs = new GameState();
        fakeGameSave = readJSON('test_fixtures/simple_map_3x3_5x5.json');
        fakeError = null;
        expect(mapProvider.getMap()).toBeNull();
        gs.load('fakeFilename', function () {
          var map = mapProvider.getMap();
          var player = playerProvider.getHuman();
          expect(map instanceof Map).toBeTrue();
          expect(player instanceof HumanPlayer).toBeTrue();
          var foodBlock = map.getBlock(1, 0, 0, 2);
          var metalBlock = map.getBlock(1, 0, 0, 1);
          var stoneBlock = map.getBlock(0, 0, 4, 1);
          var woodBlock = map.getBlock(0, 0, 4, 2);
          expect([foodBlock, metalBlock, stoneBlock, woodBlock]
            .map(function levelEquals1(block) { return block.level === 0; })
            .reduce(function all(a, b) { return a && b; }))
            .toBeTrue();
          expect([
            map.tryToCaptureBlock(foodBlock, player),
            map.tryToCaptureBlock(metalBlock, player),
            map.tryToCaptureBlock(stoneBlock, player),
            map.tryToCaptureBlock(woodBlock, player)
          ].reduce(function all(a, b) { return a && b; }))
            .toBeTrue();
          expect([foodBlock, metalBlock, stoneBlock]
            .map(function levelEquals1(block) { return block.level === 1; })
            .reduce(function all(a, b) { return a && b; }))
            .toBeTrue();
          expect(woodBlock.level).toBe(2);
          expect(map.squaresMap.getSquare(player, 2, 0, 0, 4, 1) instanceof Square).toBeTrue();
          Joi.validate(map, MapSchema, {presence: 'required', convert: false}, function (validation_error) {
            if (validation_error) {
              done.fail(validation_error);
            } else {
              done();
            }
          });
        });
      });

    });

  });

  describe('serialijse', function () {

    function runTest(fixtureName, done) {
      var gs = new GameState();
      var gameState = gs.serialijse.deserialize(readJSON('test_fixtures/' + fixtureName + '.json'));
      Joi.validate(gameState, GameStateSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    }

    it('deserializes fixture simple_map_3x3_5x5 ' +
       'valid against game state schema', function (done) {
      runTest('simple_map_3x3_5x5', done);
    });

    it('deserializes fixture simple_map_3x3_5x5_with_2_human_squares ' +
       'valid against game state schema', function (done) {
      runTest('simple_map_3x3_5x5_with_2_human_squares', done);
    });

    it('deserializes fixture simple_map_3x3_5x5_with_24_improved_blocks ' +
       'valid against game state schema', function (done) {
      runTest('simple_map_3x3_5x5_with_24_improved_blocks', done);
    });

    it('deserializes fixture simple_map_3x3_5x5_with_34_improved_blocks ' +
       'valid against game state schema', function (done) {
      runTest('simple_map_3x3_5x5_with_34_improved_blocks', done);
    });

    it('deserializes fixture simple_map_5x5_10x10 ' +
       'valid against game state schema', function (done) {
      runTest('simple_map_5x5_10x10', done);
    });

    it('deserializes fixture simple_map_3x3_5x5_with_2_human_squares ' +
       'valid against game state schema', function (done) {
      runTest('simple_map_3x3_5x5_with_2_human_squares', done);
    });

    it('deserializes fixture plenty_advanced_resources_map_11x11_5x5 ' +
       'valid against game state schema', function (done) {
      runTest('plenty_advanced_resources_map_11x11_5x5', done);
    });

    it('deserializes fixture easy_map_11x11_5x5 ' +
       'valid against game state schema', function (done) {
      runTest('easy_map_11x11_5x5', done);
    });

    it('deserializes fixture easy_map_11x11_5x5_with_multiple_squares ' +
       'valid against game state schema', function (done) {
      runTest('easy_map_11x11_5x5', done);
    });

  });

});