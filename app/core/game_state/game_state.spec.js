'use strict';

describe('GameState', function () {
  beforeEach(module('square'));

  let GameState, Serialijse, mapProvider, storageProvider, playerProvider,
    squareProvider, turnTicker, EventEmitter, fs;

  beforeEach(inject(function (
      _GameState_, _Serialijse_, _mapProvider_, _storageProvider_, _playerProvider_,
      _squareProvider_, _turnTicker_) {
    GameState = _GameState_;
    Serialijse = _Serialijse_;
    mapProvider = _mapProvider_;
    storageProvider = _storageProvider_;
    playerProvider = _playerProvider_;

    squareProvider = _squareProvider_;
    turnTicker = _turnTicker_;
    EventEmitter = require('events');
    fs = require('fs');
  }));

  let gameState;

  beforeEach(function () {
    spyOn(Serialijse.prototype, 'declarePersistable');
    spyOn(Serialijse.prototype, 'declareUnserializable');
    gameState = new GameState();
  });
  
  describe('constructor', function () {

    it('sets a new instance of Serialijse to `serialijse` prop', function () {
      expect(gameState.serialijse instanceof Serialijse).toBeTrue();
    });

    it('calls declarePersistable on serialijse', function () {
      expect(gameState.serialijse.declarePersistable.calls.count())
        .toBeGreaterThan(1);
    });

    it('calls declareUnserializable on serialijse', function () {
      expect(gameState.serialijse.declareUnserializable.calls.count())
        .toBe(1);
    });

  });

  describe('save', function () {

    let getMapResult;
    let getAllStoragesResult;
    let getAllPlayersResult;
    let getSquareFactoryResult;
    let getSquaresMapResult;
    let getTickerResult;

    beforeEach(function () {
      spyOn(gameState.serialijse, 'serialize');
      getMapResult = jasmine.createSpy('getMapResult');
      spyOn(mapProvider, 'getMap').and.returnValue(getMapResult);
      getAllStoragesResult = jasmine.createSpy('getAllStoragesResult');
      spyOn(storageProvider, 'getAllStorages').and.returnValue(getAllStoragesResult);
      getAllPlayersResult = jasmine.createSpy('getAllPlayersResult');
      spyOn(playerProvider, 'getAllPlayers').and.returnValue(getAllPlayersResult);
      getSquareFactoryResult = jasmine.createSpy('getSquareFactoryResult');
      spyOn(squareProvider, 'getSquareFactory').and.returnValue(getSquareFactoryResult);
      getSquaresMapResult = jasmine.createSpy('getSquaresMapResult');
      spyOn(squareProvider, 'getSquaresMap').and.returnValue(getSquaresMapResult);
      getTickerResult = jasmine.createSpy('getTickerResult');
      spyOn(turnTicker, 'getTicker').and.returnValue(getTickerResult);
      spyOn(fs, 'writeFile');
    });

    it('calls serialize on serialijse', function () {
      gameState.save();
      expect(gameState.serialijse.serialize).toHaveBeenCalledWith(
        jasmine.any(Object), jasmine.any(Function),
        angular.toJson, jasmine.any(EventEmitter)
      );
    });

    it('calls getMap on mapProvider and pass it results ' +
       'to `map` prop in serialized obj', function () {
      gameState.save();
      expect(mapProvider.getMap).toHaveBeenCalledWith();
      const serializedObj = gameState.serialijse.serialize.calls.argsFor(0)[0];
      expect(serializedObj).toEqual(jasmine.objectContaining({
        map: getMapResult
      }));
    });

    it('calls getAllStorages on storageProvider and pass it results ' +
       'to `storages` prop in serialized obj', function () {
      gameState.save();
      expect(storageProvider.getAllStorages).toHaveBeenCalledWith();
      const serializedObj = gameState.serialijse.serialize.calls.argsFor(0)[0];
      expect(serializedObj).toEqual(jasmine.objectContaining({
        storages: getAllStoragesResult
      }));
    });

    it('calls getAllPlayers on playerProvider and pass it results ' +
       'to `players` prop in serialized obj', function () {
      gameState.save();
      expect(playerProvider.getAllPlayers).toHaveBeenCalledWith();
      const serializedObj = gameState.serialijse.serialize.calls.argsFor(0)[0];
      expect(serializedObj).toEqual(jasmine.objectContaining({
        players: getAllPlayersResult
      }));
    });

    it('calls getSquareFactory on squareProvider and pass it results ' +
       'to `squares.factory` prop in serialized obj', function () {
      gameState.save();
      expect(squareProvider.getSquareFactory).toHaveBeenCalledWith();
      const serializedObj = gameState.serialijse.serialize.calls.argsFor(0)[0];
      expect(serializedObj.squares).toEqual(jasmine.objectContaining({
        factory: getSquareFactoryResult
      }));
    });

    it('calls getSquaresMap on squareProvider and pass it results ' +
       'to `squares.map` prop in serialized obj', function () {
      gameState.save();
      expect(squareProvider.getSquaresMap).toHaveBeenCalledWith();
      const serializedObj = gameState.serialijse.serialize.calls.argsFor(0)[0];
      expect(serializedObj.squares).toEqual(jasmine.objectContaining({
        map: getSquaresMapResult
      }));
    });

    it('calls getTicker on turnTicker and pass it results ' +
       'to `turnTicker` prop in serialized obj', function () {
      gameState.save();
      expect(turnTicker.getTicker).toHaveBeenCalledWith();
      const serializedObj = gameState.serialijse.serialize.calls.argsFor(0)[0];
      expect(serializedObj).toEqual(jasmine.objectContaining({
        turnTicker: getTickerResult
      }));
    });

    it('calls writeFile on fs if serialize callback ' +
       'does not get any error', function () {
      let serializeCallback = function () { };
      gameState.serialijse.serialize.and.callFake(function (obj, callback_) {
        serializeCallback = callback_;
      });
      const fakeCallback = jasmine.createSpy('callback');
      const fakeData = jasmine.createSpy('data');
      const fakeFilename = jasmine.createSpy('filename');
      const fakeError = null;
      gameState.save(fakeFilename, fakeCallback);
      serializeCallback(fakeError, fakeData);
      expect(fs.writeFile).toHaveBeenCalledWith(
        fakeFilename, fakeData, 'utf8', fakeCallback);
    });

    it('does not call writeFile on fs if serialize callback ' +
       'gets an error', function () {
      let serializeCallback = function () { };
      gameState.serialijse.serialize.and.callFake(function (obj, callback_) {
        serializeCallback = callback_;
      });
      const fakeCallback = jasmine.createSpy('callback');
      const fakeData = jasmine.createSpy('data');
      const fakeFilename = jasmine.createSpy('filename');
      const fakeError = jasmine.createSpy('error');
      gameState.save(fakeFilename, fakeCallback);
      serializeCallback(fakeError, fakeData);
      expect(fs.writeFile).not.toHaveBeenCalled();
      expect(fakeCallback).toHaveBeenCalledWith(fakeError);
    });

  });

  describe('load', function () {

    let readFileCallback;
    let fakeDeserializedObj;

    beforeEach(function () {
      readFileCallback = function () { };
      spyOn(fs, 'readFile').and.callFake(function (...args) {
        readFileCallback = Array.from(args)[2];
      });
      fakeDeserializedObj = jasmine.createSpyObj('deserializedObj',
        ['players', 'storages', 'map', 'squares', 'turnTicker']);
      fakeDeserializedObj.squares = jasmine.createSpyObj('deserializedObj.squares',
        ['factory', 'map']);
      spyOn(gameState.serialijse, 'deserialize')
        .and.returnValue(fakeDeserializedObj);
      spyOn(playerProvider, 'load');
      spyOn(storageProvider, 'loadStorages');
      spyOn(mapProvider, 'loadMap');
      spyOn(squareProvider, 'loadSquareFactory');
      spyOn(squareProvider, 'loadSquaresMap');
      spyOn(turnTicker, 'load');
    });

    it('calls readFile on fs', function () {
      const fakeFilename = jasmine.createSpy('filename');
      gameState.load(fakeFilename);
      expect(fs.readFile).toHaveBeenCalledWith(
        fakeFilename, 'utf8', jasmine.any(Function));
    });

    describe('calls the given callback', function () {

      let fakeCallback;

      beforeEach(function () {
        fakeCallback = jasmine.createSpy('callback');
      });

      it('with readFileErr if there is any', function () {
        const fakeReadFileErr = jasmine.createSpy('readFileErr');
        gameState.load('', fakeCallback);
        readFileCallback(fakeReadFileErr);
        expect(fakeCallback).toHaveBeenCalledWith(fakeReadFileErr);
        expect(fakeCallback.calls.count()).toBe(1);
      });

      it('with an unexpected error if any occurs', function () {
        const fakeError = new Error('fake error');
        gameState.serialijse.deserialize.and.callFake(function () {
          throw fakeError;
        });
        gameState.load('', fakeCallback);
        readFileCallback();
        expect(fakeCallback).toHaveBeenCalledWith(fakeError);
        expect(fakeCallback.calls.count()).toBe(1);
      });

      it('without params if no error occurs', function () {
        gameState.load('', fakeCallback);
        readFileCallback();
        expect(fakeCallback).toHaveBeenCalledWith();
        expect(fakeCallback.calls.count()).toBe(1);
      });

    });

    describe('readFile callback', function () {

      let fakeCallback;

      beforeEach(function () {
        fakeCallback = jasmine.createSpy('callback');
      });

      it('calls deserialize on serialijse', function () {
        gameState.load('', fakeCallback);
        const fakeData = jasmine.createSpy('data');
        readFileCallback(null, fakeData);
        expect(gameState.serialijse.deserialize).toHaveBeenCalledWith(fakeData);
      });

      it('calls load on playerProvider', function () {
        gameState.load('', fakeCallback);
        readFileCallback();
        expect(playerProvider.load).toHaveBeenCalledWith(
          fakeDeserializedObj.players);
      });

      it('calls loadStorages on storageProvider', function () {
        gameState.load('', fakeCallback);
        readFileCallback();
        expect(storageProvider.loadStorages).toHaveBeenCalledWith(
          fakeDeserializedObj.storages);
      });

      it('calls loadMap on mapProvider', function () {
        gameState.load('', fakeCallback);
        readFileCallback();
        expect(mapProvider.loadMap).toHaveBeenCalledWith(
          fakeDeserializedObj.map);
      });

      it('calls loadSquareFactory on squareProvider', function () {
        gameState.load('', fakeCallback);
        readFileCallback();
        expect(squareProvider.loadSquareFactory).toHaveBeenCalledWith(
          fakeDeserializedObj.squares.factory);
      });

      it('calls loadSquaresMap on squareProvider', function () {
        gameState.load('', fakeCallback);
        readFileCallback();
        expect(squareProvider.loadSquaresMap).toHaveBeenCalledWith(
          fakeDeserializedObj.squares.map);
      });

      it('calls load on turnTicker', function () {
        gameState.load('', fakeCallback);
        readFileCallback();
        expect(turnTicker.load).toHaveBeenCalledWith(
          fakeDeserializedObj.turnTicker);
      });

    });

  });

});