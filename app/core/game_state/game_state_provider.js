'use strict';

angular.module('square').factory('gameStateProvider', ['GameState', function (GameState) {

  var that = this;
  that.gameState = new GameState();

  return {
    load: function (filename, callback) {
      that.gameState.load(filename, callback);
    },
    save: function (filename, callback) {
      that.gameState.save(filename, callback);
    },
    _instance: this
  };
}]);
