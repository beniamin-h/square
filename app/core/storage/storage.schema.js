'use strict';

var _RegExp = require('nwglobal').RegExp;
var EventEmitter = require('events');

angular.module('square')

.constant('storageResourcesRegExp',
  new _RegExp(/^(food|stone|wood|metal|clay|coal|horses|boards|cutStone|steel|brick)$/))

.factory('StorageResourcesSchema', ['Joi', 'storageResourcesRegExp',
    function (Joi, storageResourcesRegExp) {

  return Joi.object({})
    .pattern(storageResourcesRegExp, Joi.number().integer()
  );

}])

.factory('StorageSchema', ['Joi', 'Storage', 'PlayerSchema', 'StorageResourcesSchema',
                           'StorageResourceShortage',
    function (Joi, Storage, PlayerSchema, StorageResourcesSchema,
              StorageResourceShortage) {

  return Joi.object().keys({
    owner: PlayerSchema,
    resources: StorageResourcesSchema,
    gold: Joi.number().integer(),
    goldIncome: Joi.number().integer(),
    eventEmitter: [
      Joi.object().type(EventEmitter),
      Joi.any().valid(null)
    ],
    resourceShortage: Joi.object().type(StorageResourceShortage).keys({
      storage: Joi.object().type(Storage)
    })
  }).type(Storage);

}]);
