'use strict';

describe('StorageResourceShortage', function() {
  beforeEach(module('square'));

  var StorageResourceShortage;

  beforeEach(inject(function (_StorageResourceShortage_) {
    StorageResourceShortage = _StorageResourceShortage_;
  }));

  var resourceShortage;

  describe('setConsumptionReductionMethodsReversed', function () {

    it('sets consumptionReductionMethodsReversed to an array ' +
       'of consumptionReductionMethods reversed', function () {
      StorageResourceShortage.consumptionReductionMethods = ['a', 'b', 'c', 2, 'z'];
      StorageResourceShortage.setConsumptionReductionMethodsReversed();
      expect(StorageResourceShortage.consumptionReductionMethodsReversed)
        .toEqual(['z', 2, 'c', 'b', 'a']);
    });

    it('does not change the original consumptionReductionMethods array', function () {
      StorageResourceShortage.consumptionReductionMethods = ['1', '5', 'c', 'a'];
      StorageResourceShortage.setConsumptionReductionMethodsReversed();
      expect(StorageResourceShortage.consumptionReductionMethods)
        .toEqual(['1', '5', 'c', 'a']);
    });

  });

  describe('checkResourceAmounts', function () {

    beforeEach(function () {
      var storage = {
        resources: {
          wood: -1,
          food: 0,
          metal: 22,
          boards: -10,
          stone: -999999
        }
      };
      resourceShortage = new StorageResourceShortage(storage);
      spyOn(resourceShortage, 'tryToReduceConsumption');
      spyOn(resourceShortage, 'revertConsumptionReduction');
    });

    it('calls tryToReduceConsumption for each resource with negative amount', function () {
      resourceShortage.checkResourceAmounts();
      expect(resourceShortage.tryToReduceConsumption.calls.allArgs())
        .toEqual(jasmine.arrayContaining([
          ['wood', null], ['boards', null], ['stone', null]
        ]));
    });

    it('calls revertConsumptionReduction for each resource with positive amount', function () {
      resourceShortage.checkResourceAmounts();
      expect(resourceShortage.revertConsumptionReduction.calls.allArgs())
        .toEqual([['metal', null]]);
    });

  });

  describe('getNextReductionMethod', function () {

    beforeEach(function () {
      resourceShortage = new StorageResourceShortage();
      StorageResourceShortage.consumptionReductionMethods = ['a', 'b', 'c'];
      StorageResourceShortage.setConsumptionReductionMethodsReversed();
    });

    it('returns the first reduction method if currentMethod is null ' +
       'and reversed is falsy', function () {
      var result = resourceShortage.getNextReductionMethod(null);
      expect(result).toBe('a');
    });

    it('returns the last reduction method if currentMethod is null ' +
       'and reversed is truthy', function () {
      var result = resourceShortage.getNextReductionMethod(null, true);
      expect(result).toBe('c');
    });

    it('returns `b` for last reduction method `a`, reversed = false ' +
       'and consumptionReductionMethods = [`a`, `b`, `c`]', function () {
      var result = resourceShortage.getNextReductionMethod('a', false);
      expect(result).toBe('b');
    });

    it('returns `c` for last reduction method `b`, reversed = false ' +
       'and consumptionReductionMethods = [`a`, `b`, `c`]', function () {
      var result = resourceShortage.getNextReductionMethod('b', false);
      expect(result).toBe('c');
    });

    it('returns null for last reduction method `c`, reversed = false ' +
       'and consumptionReductionMethods = [`a`, `b`, `c`]', function () {
      var result = resourceShortage.getNextReductionMethod('c', false);
      expect(result).toBe(null);
    });

    it('returns `b` for last reduction method `c`, reversed = true ' +
       'and consumptionReductionMethods = [`a`, `b`, `c`]', function () {
      var result = resourceShortage.getNextReductionMethod('c', true);
      expect(result).toBe('b');
    });

    it('returns `a` for last reduction method `b`, reversed = true ' +
       'and consumptionReductionMethods = [`a`, `b`, `c`]', function () {
      var result = resourceShortage.getNextReductionMethod('b', true);
      expect(result).toBe('a');
    });

    it('returns null for last reduction method `a`, reversed = true ' +
       'and consumptionReductionMethods = [`a`, `b`, `c`]', function () {
      var result = resourceShortage.getNextReductionMethod('a', true);
      expect(result).toBe(null);
    });

    it('throws an error if last reduction method ' +
       'is not in consumptionReductionMethods', function () {
      expect(function () {
        resourceShortage.getNextReductionMethod('z');
      }).toThrow(new Error('StorageResourceShortage.getNextReductionMethod: ' +
        'Invalid method given: z'));
    });

  });

  describe('tryToReduceConsumption', function () {

    var lastTriedMethodSpy;
    var resNameSpy;

    beforeEach(function () {
      var storage = jasmine.createSpyObj('storage', ['eventEmitter']);
      storage.eventEmitter = jasmine.createSpyObj('eventEmitter', ['emit']);
      resourceShortage = new StorageResourceShortage(storage);
      spyOn(resourceShortage, 'getNextReductionMethod');
      lastTriedMethodSpy = jasmine.createSpy('lastTriedMethod');
      resNameSpy = jasmine.createSpy('resName');
    });

    it('calls getNextReductionMethod', function () {
      resourceShortage.tryToReduceConsumption(resNameSpy, lastTriedMethodSpy);
      expect(resourceShortage.getNextReductionMethod).toHaveBeenCalledWith(
        lastTriedMethodSpy, false);
    });

    it('calls emit on storage eventEmitter ' +
       'if getNextReductionMethod returns not null result', function () {
      resourceShortage.getNextReductionMethod.and.returnValue('fakeMethod');
      resourceShortage.tryToReduceConsumption(resNameSpy, lastTriedMethodSpy);
      expect(resourceShortage.storage.eventEmitter.emit).toHaveBeenCalledWith(
        'fakeMethodResourceShortage', resNameSpy);
    });

  });

  describe('revertConsumptionReduction', function () {

    var lastTriedMethodSpy;
    var resNameSpy;

    beforeEach(function () {
      var storage = jasmine.createSpyObj('storage', ['eventEmitter']);
      storage.eventEmitter = jasmine.createSpyObj('eventEmitter', ['emit']);
      resourceShortage = new StorageResourceShortage(storage);
      spyOn(resourceShortage, 'getNextReductionMethod');
      lastTriedMethodSpy = jasmine.createSpy('lastTriedMethod');
      resNameSpy = jasmine.createSpy('resName');
    });

    it('calls getNextReductionMethod', function () {
      resourceShortage.revertConsumptionReduction(resNameSpy, lastTriedMethodSpy);
      expect(resourceShortage.getNextReductionMethod).toHaveBeenCalledWith(
        lastTriedMethodSpy, true);
    });

    it('calls emit on storage eventEmitter ' +
       'if getNextReductionMethod returns not null result', function () {
      resourceShortage.getNextReductionMethod.and.returnValue('anotherFakeMethod');
      resourceShortage.revertConsumptionReduction(resNameSpy, lastTriedMethodSpy);
      expect(resourceShortage.storage.eventEmitter.emit).toHaveBeenCalledWith(
        'anotherFakeMethodResourceShortageEnd', resNameSpy);
    });

  });

});