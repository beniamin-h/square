'use strict';

describe('StorageSchema', function() {
  beforeEach(module('square'));

  var Storage, AIPlayer, StorageSchema, Joi,
    storageProvider, playerProvider;

  beforeEach(inject(function (_Storage_, _AIPlayer_, _StorageSchema_, _Joi_,
      _storageProvider_, _playerProvider_) {
    Storage = _Storage_;
    AIPlayer = _AIPlayer_;
    StorageSchema = _StorageSchema_;
    Joi = _Joi_;

    storageProvider = _storageProvider_;
    playerProvider = _playerProvider_;
  }));

  describe('a raw new instance', function () {

    it('validates against the storage schema', function (done) {
      var fakeOwner = new AIPlayer('fake-player-id', {x: 0, y: -1});
      fakeOwner.setEventEmitter();
      var storage = new Storage(fakeOwner);
      storage.setResourceShortageHandler();
      Joi.validate(storage, StorageSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

  });

  describe('a new instance created by a storage provider', function () {

    it('validates against the storage schema', function (done) {
      var human = playerProvider.addHuman();
      playerProvider.addAI();
      storageProvider.init(playerProvider.getAllPlayers());
      Joi.validate(
        storageProvider.getStorage(human),
        StorageSchema,
        {presence: 'required', convert: false},
        function (validation_error) {
          if (validation_error) {
            done.fail(validation_error);
          } else {
            done();
          }
        }
      );
    });

  });

});