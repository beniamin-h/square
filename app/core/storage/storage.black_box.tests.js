'use strict';

describe('Storage', function() {
  beforeEach(module('square'));

  var initProvider, storageProvider, playerProvider,
    mapProvider, Plain, Mountain, Hill, Forest, SilverDeposit, GoldDeposit;
  var storage, player, map;

  beforeEach(inject(function (_initProvider_, _storageProvider_, _playerProvider_,
      _mapProvider_, _Plain_, _Mountain_, _Hill_, _Forest_, _SilverDeposit_, _GoldDeposit_) {
    initProvider = _initProvider_;
    storageProvider = _storageProvider_;
    playerProvider = _playerProvider_;

    mapProvider = _mapProvider_;
    Plain = _Plain_;
    Mountain = _Mountain_;
    Hill = _Hill_;
    Forest = _Forest_;
    SilverDeposit = _SilverDeposit_;
    GoldDeposit = _GoldDeposit_;
  }));

  beforeEach(function () {
    initProvider.initNewGame();
    player = playerProvider.getHuman();
    storage = storageProvider.getStorage(player);
    map = mapProvider.getMap();
  });

  describe('processStorage', function () {

    beforeEach(function () {
      storage.put({ food: 100, wood: 100, stone: 100, metal: 100 });
      storage.goldIncome = 100;
      map.changeBlockKind(map.getBlock(0, 0, 0, 0), new Plain(), true);
      map.changeBlockKind(map.getBlock(0, 0, 1, 0), new Mountain(), true);
      map.changeBlockKind(map.getBlock(0, 0, 0, 1), new Hill(), false);
      map.changeBlockKind(map.getBlock(0, 0, 1, 1), new Forest(), false);
      map.changeBlockKind(map.getBlock(0, 0, 2, 0), new Plain(), false);
      map.changeBlockKind(map.getBlock(0, 0, 2, 1), new SilverDeposit(), false);
      map.captureBlock(map.getBlock(0, 0, 0, 0), player);
      map.captureBlock(map.getBlock(0, 0, 1, 0), player);
      map.captureBlock(map.getBlock(0, 0, 0, 1), player);
      map.captureBlock(map.getBlock(0, 0, 1, 1), player);
      map.captureBlock(map.getBlock(0, 0, 2, 0), player);
      map.captureBlock(map.getBlock(0, 0, 2, 1), player);
      expect(map.getBlock(0, 0, 1, 1).level).toBe(2);
      expect(storage.resources).toEqual(jasmine.objectContaining({
        food: 101,
        wood: 101,
        stone: 100,
        metal: 101
      }));
      expect(storage.goldIncome).toBe(110);
    });

    it('disables blocks consuming missing resources: wood', function () {
      storage.take({ wood: 500 }, true);
      expect(storage.resources).toEqual(jasmine.objectContaining({
        wood: 101 - 500,
        food: 101
      }));
      expect(map.getBlock(0, 0, 0, 0).disabled).toBeFalse();
      expect(map.getBlock(0, 0, 2, 0).disabled).toBeFalse();
      storage.processStorage();
      expect(map.getBlock(0, 0, 1, 0).disabled).toBeFalse();
      expect(map.getBlock(0, 0, 1, 1).disabled).toBeFalse();
      expect(map.getBlock(0, 0, 2, 1).disabled).toBeFalse();
      expect([map.getBlock(0, 0, 0, 0).disabled,
              map.getBlock(0, 0, 2, 0).disabled]).toContain(true);
      expect([map.getBlock(0, 0, 0, 0).disabled,
              map.getBlock(0, 0, 2, 0).disabled]).toContain(false);
      storage.processStorage();
      expect(map.getBlock(0, 0, 0, 0).disabled).toBeTrue();
      expect(map.getBlock(0, 0, 2, 0).disabled).toBeTrue();
      storage.processStorage();
      expect(map.getBlock(0, 0, 0, 0).disabled).toBeTrue();
      expect(map.getBlock(0, 0, 2, 0).disabled).toBeTrue();
      expect(map.getBlock(0, 0, 1, 0).disabled).toBeFalse();
      expect(map.getBlock(0, 0, 1, 1).disabled).toBeFalse();
      expect(map.getBlock(0, 0, 2, 1).disabled).toBeFalse();
      expect(storage.goldIncome).toBe(110);
      expect(storage.resources).toEqual(jasmine.objectContaining({
        wood: 101 - 500 + 2,
        food: 101 - 3
      }));
    });

    it('disables blocks consuming missing resources: food', function () {
      storage.take({ food: 500 }, true);
      expect(storage.resources).toEqual(jasmine.objectContaining({
        food: 101 - 500,
        metal: 101
      }));
      expect(storage.goldIncome).toBe(110);
      expect(map.getBlock(0, 0, 1, 0).disabled).toBeFalse();
      expect(map.getBlock(0, 0, 2, 1).disabled).toBeFalse();
      storage.processStorage();
      expect(map.getBlock(0, 0, 0, 0).disabled).toBeFalse();
      expect(map.getBlock(0, 0, 1, 1).disabled).toBeFalse();
      expect(map.getBlock(0, 0, 2, 0).disabled).toBeFalse();
      expect([map.getBlock(0, 0, 1, 0).disabled,
              map.getBlock(0, 0, 2, 1).disabled]).toContain(true);
      expect([map.getBlock(0, 0, 1, 0).disabled,
              map.getBlock(0, 0, 2, 1).disabled]).toContain(false);
      storage.processStorage();
      expect(map.getBlock(0, 0, 1, 0).disabled).toBeTrue();
      expect(map.getBlock(0, 0, 2, 1).disabled).toBeTrue();
      storage.processStorage();
      expect(map.getBlock(0, 0, 1, 0).disabled).toBeTrue();
      expect(map.getBlock(0, 0, 2, 1).disabled).toBeTrue();
      expect(map.getBlock(0, 0, 0, 0).disabled).toBeFalse();
      expect(map.getBlock(0, 0, 1, 1).disabled).toBeFalse();
      expect(map.getBlock(0, 0, 2, 0).disabled).toBeFalse();
      expect(storage.goldIncome).toBe(101);
      expect(storage.resources).toEqual(jasmine.objectContaining({
        food: 101 - 500 + 2,
        metal: 101 - 2
      }));
    });

    it('disables blocks consuming missing resources: stone and metal', function () {
      storage.take({ stone: 500, metal: 300 }, true);
      expect(storage.resources).toEqual(jasmine.objectContaining({
        wood: 101,
        stone: 100 - 500,
        metal: 101 - 300
      }));
      expect(map.getBlock(0, 0, 1, 1).disabled).toBeFalse();
      expect(map.getBlock(0, 0, 0, 1).disabled).toBeFalse();
      storage.processStorage();
      expect(map.getBlock(0, 0, 1, 1).disabled).toBeTrue();
      expect(map.getBlock(0, 0, 0, 1).disabled).toBeTrue();
      expect(map.getBlock(0, 0, 0, 0).disabled).toBeFalse();
      expect(map.getBlock(0, 0, 1, 0).disabled).toBeFalse();
      expect(map.getBlock(0, 0, 2, 0).disabled).toBeFalse();
      storage.processStorage();
      expect(map.getBlock(0, 0, 1, 1).disabled).toBeTrue();
      expect(map.getBlock(0, 0, 0, 1).disabled).toBeTrue();
      expect(map.getBlock(0, 0, 0, 0).disabled).toBeFalse();
      expect(map.getBlock(0, 0, 1, 0).disabled).toBeFalse();
      expect(map.getBlock(0, 0, 2, 0).disabled).toBeFalse();
      expect(storage.goldIncome).toBe(110);
      expect(storage.resources).toEqual(jasmine.objectContaining({
        wood: 101 - 2,
        stone: 100 - 500,
        metal: 101 - 300 + 1
      }));
    });

    it('disables blocks consuming missing resources ' +
       'as long the resource amount level is negative', function () {
      storage.take({ food: 102 }, true);
      expect(storage.resources).toEqual(jasmine.objectContaining({
        food: -1
      }));
      expect(storage.goldIncome).toBe(110);
      expect(map.getBlock(0, 0, 1, 0).disabled).toBeFalse();
      expect(map.getBlock(0, 0, 2, 1).disabled).toBeFalse();
      storage.processStorage();
      expect(storage.resources.food).toBe(0);
      expect(map.getBlock(0, 0, 0, 0).disabled).toBeFalse();
      expect(map.getBlock(0, 0, 1, 1).disabled).toBeFalse();
      expect(map.getBlock(0, 0, 2, 0).disabled).toBeFalse();
      var mountainDisabled = map.getBlock(0, 0, 1, 0).disabled;
      var silverDepositDisabled = map.getBlock(0, 0, 2, 1).disabled;
      expect([mountainDisabled, silverDepositDisabled]).toContain(true);
      expect([mountainDisabled, silverDepositDisabled]).toContain(false);
      storage.processStorage();
      storage.processStorage();
      storage.processStorage();
      expect(storage.resources.food).toBe(0);
      var mountainDisabled_2 = map.getBlock(0, 0, 1, 0).disabled;
      var silverDepositDisabled_2 = map.getBlock(0, 0, 2, 1).disabled;
      expect(mountainDisabled_2 === mountainDisabled).toBeTrue();
      expect(silverDepositDisabled_2 === silverDepositDisabled).toBeTrue();
    });

    it('changes square outcome by blocks consuming missing resources: food', function () {
      map.changeBlockKind(map.getBlock(0, 0, 3, 0), new Plain(), false);
      map.changeBlockKind(map.getBlock(0, 0, 3, 1), new GoldDeposit(), false);
      map.captureBlock(map.getBlock(0, 0, 3, 0), player);
      map.captureBlock(map.getBlock(0, 0, 3, 1), player);
      expect(map.getBlock(0, 0, 3, 1).level).toBe(2);
      var square_1 = map.getBlock(0, 0, 0, 0).square;
      var square_2 = map.getBlock(0, 0, 3, 0).square;
      expect(storage.goldIncome).toBe(135);
      expect(square_1).toBeObject();
      expect(square_2).toBeObject();
      expect(square_1.goldIncomeBlocks).toBe(0);
      expect(square_2.goldIncomeBlocks).toBe(33);
      expect(square_1.blockResourceProduction.metal).toBe(2);
      storage.take({ food: 500 }, true);
      storage.processStorage();
      storage.processStorage();
      expect(storage.goldIncome).toBeLessThan(135);
      expect(square_2.goldIncomeBlocks).toBeLessThan(33);
      storage.processStorage();
      expect(square_2.goldIncomeBlocks).toBe(0);
      expect(storage.goldIncome).toBe(102);
      expect(square_1.blockResourceProduction.metal).toBeUndefined();
    });

    it('enables blocks disabled due to resource shortage ' +
       'when the resource amount become positive: wood', function () {
      storage.take({ wood: 500 }, true);
      expect(map.getBlock(0, 0, 0, 0).disabled).toBeFalse();
      expect(map.getBlock(0, 0, 2, 0).disabled).toBeFalse();
      storage.processStorage();
      storage.processStorage();
      expect(map.getBlock(0, 0, 0, 0).disabled).toBeTrue();
      expect(map.getBlock(0, 0, 2, 0).disabled).toBeTrue();
      storage.put({ wood: 500 });
      storage.processStorage();
      storage.processStorage();
      expect(map.getBlock(0, 0, 0, 0).disabled).toBeFalse();
      expect(map.getBlock(0, 0, 2, 0).disabled).toBeFalse();
    });

    it('enables blocks disabled due to resource shortage ' +
       'when the resource amount become positive: metal', function () {
      storage.take({ food: 101, wood: 101, stone: 100, metal: 102 }, true);
      expect(storage.resources).toEqual(jasmine.objectContaining({
        food: 0,
        wood: 0,
        stone: 0,
        metal: -1
      }));
      storage.processStorage();
      expect(map.getBlock(0, 0, 0, 1).disabled).toBeTrue();
      expect(storage.resources).toEqual(jasmine.objectContaining({
        stone: -1,
        metal: 0
      }));
      storage.processStorage();
      expect(map.getBlock(0, 0, 1, 1).disabled).toBeTrue();
      expect(storage.resources).toEqual(jasmine.objectContaining({
        wood: -2,
        stone: 0
      }));
      storage.put({ wood: 2 });
      expect(storage.resources).toEqual(jasmine.objectContaining({
        food: 0,
        wood: 0,
        stone: 0,
        metal: 0
      }));
      storage.processStorage();
      expect(map.getBlock(0, 0, 1, 1).disabled).toBeTrue();
      expect(map.getBlock(0, 0, 0, 1).disabled).toBeTrue();
      storage.put({ metal: 1 });
      storage.processStorage();
      expect(map.getBlock(0, 0, 1, 1).disabled).toBeTrue();
      expect(map.getBlock(0, 0, 0, 1).disabled).toBeFalse();
      expect(storage.resources).toEqual(jasmine.objectContaining({
        food: 0,
        wood: 0,
        stone: 1,
        metal: 0
      }));
      storage.processStorage();
      expect(map.getBlock(0, 0, 1, 1).disabled).toBeFalse();
      expect(map.getBlock(0, 0, 0, 1).disabled).toBeFalse();
      expect(storage.resources).toEqual(jasmine.objectContaining({
        food: 0,
        wood: 2,
        stone: 0,
        metal: 0
      }));
    });

    it('enables proper blocks disabled due to resource shortage ' +
       'when its kind has been changed in the meantime: stone', function () {
      storage.take({ stone: 500 }, true);
      storage.processStorage();
      expect(map.getBlock(0, 0, 1, 1).disabled).toBeTrue();
      map.changeBlockKind(map.getBlock(0, 0, 1, 1), new Plain(), false);
      storage.take({ wood: 500 }, true);
      storage.put({ stone: 500 });
      storage.processStorage();
      expect(map.getBlock(0, 0, 1, 1).disabled).toBeTrue();
      storage.put({ wood: 500 });
      storage.processStorage();
      storage.processStorage();
      expect(map.getBlock(0, 0, 1, 1).disabled).toBeFalse();
    });

    it('enables proper blocks disabled due to resource shortage ' +
       'when its kind has been changed in the meantime: metal', function () {
      storage.take({ metal: 500 }, true);
      expect(storage.resources.metal).toBe(101 - 500);
      storage.processStorage();
      expect(map.getBlock(0, 0, 0, 1).disabled).toBeTrue();
      expect(storage.resources.metal).toBe(101 - 500 + 1);
      map.changeBlockKind(map.getBlock(0, 0, 0, 1), new Mountain(), true);
      expect(storage.resources.metal).toBe(101 - 500 + 1);
      storage.processStorage();
      expect(map.getBlock(0, 0, 1, 1).disabled).toBeFalse();
      expect(storage.resources.metal).toBe(101 - 500 + 1 + 2);
    });

  });

});