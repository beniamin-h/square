'use strict';

var EventEmitter = require('events');

angular.module('square').factory('Storage', ['StorageResourceShortage',
    function (StorageResourceShortage) {

  function Storage(player) {
    this.resources = {
      wood: 0,
      stone: 0,
      metal: 0,
      food: 0,

      clay: 0,
      coal: 0,
      horses: 0,
      boards: 0,
      cutStone: 0,
      steel: 0,
      brick: 0
    };
    this.gold = 0;
    this.goldIncome = 0;
    if (player) {
      this.owner = player;
    }
  }

  Storage.basicResources = Storage.prototype.basicResources = [
    'wood', 'stone', 'metal', 'food'
  ];

  Storage.advancedResources = Storage.prototype.advancedResources = [
    'clay', 'coal', 'horses', 'boards', 'cutStone', 'steel', 'brick'
  ];

  Storage.prototype.resources = null;
  Storage.prototype.gold = 0;
  Storage.prototype.goldIncome = 0;
  Storage.prototype.owner = null;
  Storage.prototype.eventEmitter = null;
  Storage.prototype.resourceShortage = null;

  Storage.prototype.setEventEmitter = function () {
    this.eventEmitter = new EventEmitter();
    this.eventEmitter.setMaxListeners(1 << 20);  // 2^20
  };

  Storage.prototype.setEventListeners = function () {
    var that = this;
    this.owner.eventEmitter.on('processingStorage', function () {
      that.processStorage();
    });
  };

  Storage.prototype.setResourceShortageHandler = function () {
    this.resourceShortage = new StorageResourceShortage(this);
  };

  Storage.prototype.setStartingStorage = function () {
    this.put({ wood: 1 });
  };

  Storage.prototype.processStorage = function () {
    this.resourceShortage.checkResourceAmounts();
    this.putGold(this.goldIncome);
  };

  Storage.prototype.has = function (resources) {
    for (var resName in resources) {
      var amount = resources[resName];
      if (this.resources[resName] < amount) {
        return false;
      }
    }
    return true;
  };

  Storage.prototype.hasGold = function (amount) {
    return this.gold >= amount;
  };

  Storage.prototype.take = function (resources, forced) {
    var has = this.has(resources);
    if (!has && !forced) {
      return false;
    }
    for (var resName in resources) {
      var amount = resources[resName];
      this.resources[resName] -= amount;
    }
    return has;
  };

  Storage.prototype.takeGold = function (amount, forced) {
    var has = this.hasGold(amount);
    if (!has && !forced) {
      return false;
    }
    this.gold -= amount;
    return has;
  };

  Storage.prototype.put = function (resources) {
    for (var resName_ in resources) {
      var amount = resources[resName_];
      this.resources[resName_] += amount;
    }
    this.eventEmitter.emit('put', resources);
  };

  Storage.prototype.putGold = function (amount) {
    this.gold += amount;
  };

  Storage.prototype.changeGoldIncome = function (change) {
    this.goldIncome += change;
  };

  return Storage;
}]);