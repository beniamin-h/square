'use strict';

describe('storageProvider', function() {
  beforeEach(module('square'));

  var storageProvider, Storage;

  beforeEach(inject(function (_storageProvider_, _Storage_) {
    storageProvider = _storageProvider_;
    Storage = _Storage_;
  }));

  describe('init', function () {

    var fakePlayers;

    beforeEach(function () {
      fakePlayers = {
        'fake-player-id': jasmine.createSpyObj('fake-player', ['eventEmitter']),
        'another-fake-player-id': jasmine.createSpyObj('another-fake-player', ['eventEmitter'])
      };
      fakePlayers['fake-player-id'].eventEmitter =
        jasmine.createSpyObj('fake-player-eventEmitter', ['on']);
      fakePlayers['another-fake-player-id'].eventEmitter =
        jasmine.createSpyObj('another-fake-player-eventEmitter', ['on']);
      storageProvider._instance.storages = jasmine.createSpy('storages');
      spyOn(Storage.prototype, 'setEventListeners');
      spyOn(Storage.prototype, 'setEventEmitter');
      spyOn(Storage.prototype, 'setResourceShortageHandler');
      spyOn(Storage.prototype, 'setStartingStorage');
    });

    it('sets new Storage instances to `storages` prop for each given player', function () {
      storageProvider.init(fakePlayers);
      expect(storageProvider._instance.storages).toEqual({
        'fake-player-id': jasmine.any(Storage),
        'another-fake-player-id': jasmine.any(Storage)
      });
      expect(storageProvider._instance.storages['fake-player-id'])
        .not.toEqual(storageProvider._instance.storages['another-fake-player-id']);
    });

    it('calls setEventEmitter for each new storage', function () {
      storageProvider.init(fakePlayers);
      expect(Storage.prototype.setEventEmitter.calls.count()).toEqual(2);
    });

    it('calls setEventListeners for each new storage', function () {
      storageProvider.init(fakePlayers);
      expect(Storage.prototype.setEventListeners.calls.count()).toEqual(2);
    });

    it('calls setResourceShortageHandler for each new storage', function () {
      storageProvider.init(fakePlayers);
      expect(Storage.prototype.setResourceShortageHandler.calls.count()).toEqual(2);
    });

    it('calls setStartingStorage for each new storage', function () {
      fakePlayers['yet-another-fake-player-id'] = jasmine.createSpyObj('fake-player', ['eventEmitter']);
      fakePlayers['yet-another-fake-player-id'].eventEmitter =
        jasmine.createSpyObj('yet-another-fake-player-id', ['on']);
      storageProvider.init(fakePlayers);
      expect(Storage.prototype.setStartingStorage.calls.count()).toEqual(3);
    });

  });

  describe('getAllStorages', function () {

    var storagesSpy;

    beforeEach(function () {
      storagesSpy = jasmine.createSpy('storages');
      storageProvider._instance.storages = storagesSpy;
    });

    it('returns `storages` prop', function () {
      var result = storageProvider.getAllStorages();
      expect(result).toBe(storagesSpy);
    });

  });

  describe('getResources', function () {

    var fakePlayer;
    var fakeStorage;

    beforeEach(function () {
      fakePlayer = jasmine.createSpyObj('player', ['playerId']);
      fakePlayer.playerId = 'fake-player-id-1';
      fakeStorage = jasmine.createSpyObj('Storage', ['resources']);
      storageProvider._instance.storages = {
        'fake-player-id-1': fakeStorage,
        'another-fake-player-id-1': jasmine.createSpyObj('anotherStorage', ['resources'])
      };
    });

    it('returns resources of the given player storage', function () {
      var result = storageProvider.getResources(fakePlayer);
      expect(result).toBe(fakeStorage.resources);
    });

  });

  describe('getStorage', function () {

    var fakePlayer;
    var fakeStorage;

    beforeEach(function () {
      fakePlayer = jasmine.createSpyObj('player', ['playerId']);
      fakePlayer.playerId = 'fake-player-id-1';
      fakeStorage = jasmine.createSpy('Storage');
      storageProvider._instance.storages = {
        'fake-player-id-1': fakeStorage,
        'another-fake-player-id-1': jasmine.createSpy('anotherStorage')
      };
    });

    it('returns storage of the given player', function () {
      var result = storageProvider.getStorage(fakePlayer);
      expect(result).toBe(fakeStorage);
    });

  });

  describe('getGold', function () {

    var fakePlayer;
    var fakeStorage;

    beforeEach(function () {
      fakePlayer = jasmine.createSpyObj('player', ['playerId']);
      fakePlayer.playerId = 'fake-player-id-x';
      fakeStorage = jasmine.createSpyObj('Storage', ['gold']);
      storageProvider._instance.storages = {
        'fake-player-id-x': fakeStorage,
        'another-fake-player-id-x': jasmine.createSpyObj('anotherStorage', ['gold'])
      };
    });

    it('returns gold of the given player', function () {
      var result = storageProvider.getGold(fakePlayer);
      expect(result).toBe(fakeStorage.gold);
    });

  });

  describe('getGoldIncome', function () {

    var fakePlayer;
    var fakeStorage;

    beforeEach(function () {
      fakePlayer = jasmine.createSpyObj('player', ['playerId']);
      fakePlayer.playerId = 'fake-player-id-x';
      fakeStorage = jasmine.createSpyObj('Storage', ['goldIncome']);
      storageProvider._instance.storages = {
        'fake-player-id-x': fakeStorage,
        'another-fake-player-id-x': jasmine.createSpyObj('anotherStorage', ['goldIncome'])
      };
    });

    it('returns gold income of the given player', function () {
      var result = storageProvider.getGoldIncome(fakePlayer);
      expect(result).toBe(fakeStorage.goldIncome);
    });

  });

  describe('loadStorages', function () {

    var fakeStorages;

    function createFakeStorage(id) {
      return jasmine.createSpyObj(id, ['setEventEmitter', 'setEventListeners']);
    }

    beforeEach(function () {
      storageProvider._instance.storages = jasmine.createSpy('storages');
      fakeStorages = [createFakeStorage('storage 1'),
                      createFakeStorage('storage 2')];
    });

    it('sets given storages to `storages` prop', function () {
      storageProvider.loadStorages(fakeStorages);
      expect(storageProvider._instance.storages).toBe(fakeStorages);
    });

    it('sets `storages` prop to {} if given storages are falsy', function () {
      storageProvider.loadStorages(null);
      expect(storageProvider._instance.storages).toEqual({});
    });

    it('calls setEventEmitter for each given storage', function () {
      storageProvider.loadStorages(fakeStorages);
      expect(fakeStorages[0].setEventEmitter).toHaveBeenCalledWith();
      expect(fakeStorages[1].setEventEmitter).toHaveBeenCalledWith();
    });

    it('calls setEventListeners for each given storage', function () {
      storageProvider.loadStorages(fakeStorages);
      expect(fakeStorages[0].setEventListeners).toHaveBeenCalledWith();
      expect(fakeStorages[1].setEventListeners).toHaveBeenCalledWith();
    });

  });

});