'use strict';

describe('Storage', function() {
  beforeEach(module('square'));

  var Storage, StorageResourceShortage;

  beforeEach(inject(function (_Storage_, _StorageResourceShortage_) {
    Storage = _Storage_;
    StorageResourceShortage = _StorageResourceShortage_;
  }));

  describe('constructor', function () {

    var fakeOwner;

    beforeEach(function () {
      fakeOwner = jasmine.createSpy('owner');
    });

    it('initializes resources', function () {
      var storage = new Storage();
      expect(storage.resources).toBeNonEmptyObject();
    });

    it('sets proper owner', function () {
      var storage = new Storage(fakeOwner);
      expect(storage.owner).toBe(fakeOwner);
    });

  });

  describe('setEventListeners', function () {

    var storage;
    var fakeOwner;

    beforeEach(function () {
      fakeOwner = jasmine.createSpyObj('owner', ['eventEmitter']);
      fakeOwner.eventEmitter = jasmine.createSpyObj('eventEmitter', ['on']);
      storage = new Storage(fakeOwner);
      spyOn(storage, 'processStorage');
    });

    it('add listener on "processingStorage" event on storage owner', function () {
      storage.setEventListeners();
      expect(storage.owner.eventEmitter.on)
        .toHaveBeenCalledWith('processingStorage', jasmine.any(Function));
    });

    it('calls processStorage on "processingStorage" event on storage owner', function () {
      var processingStorageCallback = function () { };
      fakeOwner.eventEmitter.on.and.callFake(function (event, callback) {
        if (event === 'processingStorage') {
          processingStorageCallback = callback;
        }
      });
      storage.setEventListeners();
      processingStorageCallback();
      expect(storage.processStorage).toHaveBeenCalledWith();
    });

  });

  describe('setResourceShortageHandler', function () {

    var storage;

    beforeEach(function () {
      storage = new Storage();
      spyOn(storage, 'resourceShortage');
    });

    it('sets a new instance of StorageResourceShortage to `resourceShortage` prop', function () {
      storage.setResourceShortageHandler();
      expect(storage.resourceShortage instanceof StorageResourceShortage).toBeTrue();
    });

  });

  describe('setStartingStorage', function () {

    it('adds 1 wood to the storage', function () {
      var storage = new Storage();
      storage.setEventEmitter();
      expect(storage.resources).toEqual({
        wood: 0,
        stone: 0,
        metal: 0,
        food: 0,
        clay: 0,
        coal: 0,
        horses: 0,
        boards: 0,
        cutStone: 0,
        steel: 0,
        brick: 0
      });
      storage.setStartingStorage();
      expect(storage.resources).toEqual({
        wood: 1,
        stone: 0,
        metal: 0,
        food: 0,
        clay: 0,
        coal: 0,
        horses: 0,
        boards: 0,
        cutStone: 0,
        steel: 0,
        brick: 0
      });
    });

  });

  describe('processStorage', function () {

    var storage;

    beforeEach(function () {
      storage = new Storage();
      storage.resourceShortage = jasmine.createSpyObj('resourceShortage',
        ['checkResourceAmounts']);
      spyOn(storage, 'putGold');
      spyOn(storage, 'goldIncome');
    });

    it('calls checkResourceAmounts on resourceShortage', function () {
      storage.processStorage();
      expect(storage.resourceShortage.checkResourceAmounts).toHaveBeenCalledWith();
    });

    it('calls putGold', function () {
      storage.processStorage();
      expect(storage.putGold).toHaveBeenCalledWith(storage.goldIncome);
    });

  });

  describe('has', function () {

    it('returns true if there are requested resources', function () {
      var storage = new Storage();
      storage.resources.wood = 4;
      expect(storage.has({wood: 4})).toBeTrue();
    });

    it('returns true if there are all requested resources', function () {
      var storage = new Storage();
      storage.resources.wood = 4;
      storage.resources.food = 5;
      expect(storage.has({wood: 4, food: 3})).toBeTrue();
    });

    it('returns false if there are not requested resources', function () {
      var storage = new Storage();
      storage.resources.stone = 1;
      expect(storage.has({stone: 3})).toBeFalse();
    });

    it('returns false if there are not any requested resources', function () {
      var storage = new Storage();
      storage.resources.wood = 3;
      storage.resources.metal = 1;
      expect(storage.has({wood: 3, metal: 3})).toBeFalse();
    });

  });

  describe('take', function () {

    it('returns true if there are requested resources', function () {
      var storage = new Storage();
      storage.resources.wood = 4;
      expect(storage.take({wood: 4})).toBeTrue();
    });

    it('returns true if there are requested resources even if it is forced', function () {
      var storage = new Storage();
      storage.resources.wood = 4;
      expect(storage.take({wood: 4}, true)).toBeTrue();
    });

    it('returns false if there are not requested resources', function () {
      var storage = new Storage();
      storage.resources.stone = 1;
      expect(storage.take({stone: 3})).toBeFalse();
    });

    it('returns false if any resource is not sufficient', function () {
      var storage = new Storage();
      storage.resources.stone = 10;
      storage.resources.metal = 10;
      expect(storage.take({stone: 3, metal: 11})).toBeFalse();
    });

    it('returns false if any resource is not sufficient even if it is forced', function () {
      var storage = new Storage();
      storage.resources.stone = 10;
      storage.resources.metal = 10;
      expect(storage.take({stone: 3, metal: 11}, true)).toBeFalse();
    });

    it('takes requested resources from the storage', function () {
      var storage = new Storage();
      storage.resources.stone = 5;
      storage.take({stone: 3});
      expect(storage.resources.stone).toBe(2);
    });

    it('takes multiple requested resources from the storage', function () {
      var storage = new Storage();
      storage.resources.stone = 5;
      storage.resources.wood = 4;
      storage.take({stone: 3, wood: 1});
      expect(storage.resources.stone).toBe(2);
      expect(storage.resources.wood).toBe(3);
    });

    it('does not take requested resources from the storage if not sufficient', function () {
      var storage = new Storage();
      storage.resources.food = 5;
      storage.take({food: 6});
      expect(storage.resources.food).toBe(5);
    });

    it('does not take requested resources from the storage if not sufficient unless it is forced', function () {
      var storage = new Storage();
      storage.resources.food = 5;
      storage.take({food: 6}, true);
      expect(storage.resources.food).toBe(-1);
    });

    it('does not take requested resources from the storage if any is not sufficient', function () {
      var storage = new Storage();
      storage.resources.food = 15;
      storage.resources.metal = 1;
      storage.take({food: 6, metal: 3});
      expect(storage.resources.food).toBe(15);
      expect(storage.resources.metal).toBe(1);
    });

    it('does not take requested resources from the storage if any is not sufficient unless it is forced', function () {
      var storage = new Storage();
      storage.resources.food = 15;
      storage.resources.metal = 1;
      storage.take({food: 6, metal: 3}, true);
      expect(storage.resources.food).toBe(9);
      expect(storage.resources.metal).toBe(-2);
    });

    it('can take all requested resources from the storage', function () {
      var storage = new Storage();
      storage.resources.stone = 19;
      expect(storage.take({stone: 19})).toBeTrue();
      expect(storage.resources.stone).toBe(0);
    });

  });

  describe('put', function () {

    it('adds given resources to the storage', function () {
      var storage = new Storage();
      storage.setEventEmitter();
      storage.resources.wood = 4;
      storage.put({wood: 4});
      expect(storage.resources.wood).toBe(8);
    });

    it('adds multiple given resources to the storage', function () {
      var storage = new Storage();
      storage.resources.wood = 7;
      storage.resources.stone = 1;
      storage.setEventEmitter();
      storage.put({wood: 4, stone: 5});
      expect(storage.resources.wood).toBe(11);
      expect(storage.resources.stone).toBe(6);
    });

  });

  describe('hasGold', function () {

    it('returns true if there is requested amount of gold', function () {
      var storage = new Storage();
      storage.gold = 40;
      expect(storage.hasGold(39)).toBeTrue();
    });

    it('returns false if there is not requested amount of gold', function () {
      var storage = new Storage();
      storage.gold = 40;
      expect(storage.hasGold(41)).toBeFalse();
    });

  });

  describe('takeGold', function () {

    it('returns true if is request amount of gold', function () {
      var storage = new Storage();
      storage.gold = 50;
      expect(storage.takeGold(1)).toBeTrue();
    });

    it('returns true if is request amount of gold even if it is forced', function () {
      var storage = new Storage();
      storage.gold = 50;
      expect(storage.takeGold(1, true)).toBeTrue();
    });

    it('returns false if there is not request amount of gold', function () {
      var storage = new Storage();
      storage.gold = 50;
      expect(storage.takeGold(100)).toBeFalse();
    });

    it('returns false if there is not request amount of gold even if it is forced', function () {
      var storage = new Storage();
      storage.gold = 50;
      expect(storage.takeGold(100, true)).toBeFalse();
    });

    it('takes requested gold amount from the storage', function () {
      var storage = new Storage();
      storage.gold = 150;
      storage.takeGold(69);
      expect(storage.gold).toBe(81);
    });

    it('does not take requested amount of gold from the storage if not sufficient', function () {
      var storage = new Storage();
      storage.gold = 150;
      storage.takeGold(169);
      expect(storage.gold).toBe(150);
    });

    it('does not take requested amount of gold from the storage if not sufficient unless it is forced', function () {
      var storage = new Storage();
      storage.gold = 150;
      storage.takeGold(169, true);
      expect(storage.gold).toBe(-19);
    });

  });

  describe('putGold', function () {

    it('adds given gold amount to the storage', function () {
      var storage = new Storage();
      storage.gold = 4;
      storage.putGold(4);
      expect(storage.gold).toBe(8);
    });

  });

  describe('changeGoldIncome', function () {

    it('adds given positive number to goldIncome', function () {
      var storage = new Storage();
      storage.goldIncome = 2;
      storage.changeGoldIncome(5);
      expect(storage.goldIncome).toBe(7);
    });

    it('subtracts given negative amount from goldIncome', function () {
      var storage = new Storage();
      storage.goldIncome = 8;
      storage.changeGoldIncome(-11);
      expect(storage.goldIncome).toBe(-3);
    });

  });

});