'use strict';

angular.module('square').factory('storageProvider', ['Storage',
    function (Storage) {

  var that = this;

  return {
    basicResources: Storage.basicResources,
    advancedResources: Storage.advancedResources,
    init: function (players) {
      that.storages = {};
      Object.keys(players).forEach(function (playerId) {
        that.storages[playerId] = new Storage(players[playerId]);
        that.storages[playerId].setEventListeners();
        that.storages[playerId].setEventEmitter();
        that.storages[playerId].setResourceShortageHandler();
        that.storages[playerId].setStartingStorage();
      });
    },
    getAllStorages: function () {
      return that.storages;
    },
    getResources: function (player) {
      return that.storages[player.playerId].resources;
    },
    getStorage: function (player) {
      return that.storages[player.playerId];
    },
    getGold: function (player) {
      return that.storages[player.playerId].gold;
    },
    getGoldIncome: function (player) {
      return that.storages[player.playerId].goldIncome;
    },
    loadStorages: function (storages) {
      that.storages = storages || {};
      Object.keys(that.storages).forEach(function (playerId) {
        that.storages[playerId].setEventListeners();
        that.storages[playerId].setEventEmitter();
      });
    },
    _instance: this
  };

}]);
