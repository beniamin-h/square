'use strict';

angular.module('square').factory('StorageResourceShortage', [
    function () {

  function StorageResourceShortage(storage) {
    this.storage = storage;
  }

  StorageResourceShortage.consumptionReductionMethods = [
    // 'market', 'construction', 'recruitment', 'building',
    'block'
    //'unit'
  ];

  StorageResourceShortage.setConsumptionReductionMethodsReversed = function () {
    StorageResourceShortage.consumptionReductionMethodsReversed =
      StorageResourceShortage.consumptionReductionMethods.slice().reverse();
  };

  StorageResourceShortage.setConsumptionReductionMethodsReversed();

  StorageResourceShortage.prototype.storage = null;

  StorageResourceShortage.prototype.checkResourceAmounts = function () {
    Object.keys(this.storage.resources).forEach(function (resName) {
      if (this.storage.resources[resName] < 0) {
        this.tryToReduceConsumption(resName, null);
      } else if (this.storage.resources[resName] > 0) {
        this.revertConsumptionReduction(resName, null);
      }
    }, this);
  };

  StorageResourceShortage.prototype.getNextReductionMethod = function (currentMethod, reversed) {
    var methodSequence = reversed ?
                         StorageResourceShortage.consumptionReductionMethodsReversed :
                         StorageResourceShortage.consumptionReductionMethods;
    if (currentMethod === null) {
      return methodSequence[0];
    }
    var currentMethodIdx = methodSequence.indexOf(currentMethod);
    if (currentMethodIdx === -1) {
      throw new Error('StorageResourceShortage.getNextReductionMethod: ' +
        'Invalid method given: ' + currentMethod);
    }
    if (currentMethodIdx === methodSequence.length - 1) {
      return null;
    }
    return methodSequence[currentMethodIdx + 1];
  };

  StorageResourceShortage.prototype.tryToReduceConsumption = function (resName, lastTriedMethod) {
    var nextMethod = this.getNextReductionMethod(lastTriedMethod, false);
    if (nextMethod !== null) {
      this.storage.eventEmitter.emit(nextMethod + 'ResourceShortage', resName);
    }
  };

  StorageResourceShortage.prototype.revertConsumptionReduction = function (resName, lastTriedMethod) {
    var nextMethod = this.getNextReductionMethod(lastTriedMethod, true);
    if (nextMethod !== null) {
      this.storage.eventEmitter.emit(nextMethod + 'ResourceShortageEnd', resName);
    }
  };

  return StorageResourceShortage;
}]);