'use strict';

describe('mapDirections', function() {
  beforeEach(module('square'));

  var mapDirections;
  
  beforeEach(inject(function (_mapDirections_) {
    mapDirections = _mapDirections_;
  }));

  describe('isNonDiagonal', function () {

    it('returns true if the given direction is not diagonal', function () {
      expect(mapDirections.isNonDiagonal('up')).toBeTrue();
      expect(mapDirections.isNonDiagonal('right')).toBeTrue();
      expect(mapDirections.isNonDiagonal('down')).toBeTrue();
      expect(mapDirections.isNonDiagonal('left')).toBeTrue();
    });

    it('returns false if the given direction is diagonal', function () {
      expect(mapDirections.isNonDiagonal('rightUp')).toBeFalse();
      expect(mapDirections.isNonDiagonal('rightDown')).toBeFalse();
      expect(mapDirections.isNonDiagonal('leftDown')).toBeFalse();
      expect(mapDirections.isNonDiagonal('leftUp')).toBeFalse();
    });

  });

  describe('getNextNonDiagonal', function () {

    it('returns `down` for given `right` direction', function () {
      expect(mapDirections.getNextNonDiagonal('right')).toBe('down');
    });

    it('returns `down` for given `rightDown` direction', function () {
      expect(mapDirections.getNextNonDiagonal('rightDown')).toBe('down');
    });

    it('returns `left` for given `down` direction', function () {
      expect(mapDirections.getNextNonDiagonal('down')).toBe('left');
    });

    it('returns `left` for given `leftDown` direction', function () {
      expect(mapDirections.getNextNonDiagonal('leftDown')).toBe('left');
    });

    it('returns `up` for given `left` direction', function () {
      expect(mapDirections.getNextNonDiagonal('left')).toBe('up');
    });

    it('returns `up` for given `leftUp` direction', function () {
      expect(mapDirections.getNextNonDiagonal('leftUp')).toBe('up');
    });

    it('returns `right` for given `up` direction', function () {
      expect(mapDirections.getNextNonDiagonal('up')).toBe('right');
    });

    it('returns `right` for given `rightUp` direction', function () {
      expect(mapDirections.getNextNonDiagonal('rightUp')).toBe('right');
    });

  });

  describe('getNextDiagonal', function () {

    it('returns `rightDown` for given `right` direction', function () {
      expect(mapDirections.getNextDiagonal('right')).toBe('rightDown');
    });

    it('returns `leftDown` for given `rightDown` direction', function () {
      expect(mapDirections.getNextDiagonal('rightDown')).toBe('leftDown');
    });

    it('returns `leftDown` for given `down` direction', function () {
      expect(mapDirections.getNextDiagonal('down')).toBe('leftDown');
    });

    it('returns `leftUp` for given `leftDown` direction', function () {
      expect(mapDirections.getNextDiagonal('leftDown')).toBe('leftUp');
    });

    it('returns `leftUp` for given `left` direction', function () {
      expect(mapDirections.getNextDiagonal('left')).toBe('leftUp');
    });

    it('returns `rightUp` for given `leftUp` direction', function () {
      expect(mapDirections.getNextDiagonal('leftUp')).toBe('rightUp');
    });

    it('returns `rightUp` for given `up` direction', function () {
      expect(mapDirections.getNextDiagonal('up')).toBe('rightUp');
    });

    it('returns `rightDown` for given `rightUp` direction', function () {
        expect(mapDirections.getNextDiagonal('rightUp')).toBe('rightDown');
    });

  });

  describe('getPrevNonDiagonal', function () {

    it('returns `up` for given `right` direction', function () {
      expect(mapDirections.getPrevNonDiagonal('right')).toBe('up');
    });

    it('returns `right` for given `rightDown` direction', function () {
      expect(mapDirections.getPrevNonDiagonal('rightDown')).toBe('right');
    });

    it('returns `right` for given `down` direction', function () {
      expect(mapDirections.getPrevNonDiagonal('down')).toBe('right');
    });

    it('returns `down` for given `leftDown` direction', function () {
      expect(mapDirections.getPrevNonDiagonal('leftDown')).toBe('down');
    });

    it('returns `down` for given `left` direction', function () {
      expect(mapDirections.getPrevNonDiagonal('left')).toBe('down');
    });

    it('returns `left` for given `leftUp` direction', function () {
      expect(mapDirections.getPrevNonDiagonal('leftUp')).toBe('left');
    });

    it('returns `left` for given `up` direction', function () {
      expect(mapDirections.getPrevNonDiagonal('up')).toBe('left');
    });

    it('returns `up` for given `rightUp` direction', function () {
      expect(mapDirections.getPrevNonDiagonal('rightUp')).toBe('up');
    });

  });

  describe('getPrevDiagonal', function () {

    it('returns `rightUp` for given `right` direction', function () {
      expect(mapDirections.getPrevDiagonal('right')).toBe('rightUp');
    });

    it('returns `rightUp` for given `rightDown` direction', function () {
      expect(mapDirections.getPrevDiagonal('rightDown')).toBe('rightUp');
    });

    it('returns `rightDown` for given `down` direction', function () {
      expect(mapDirections.getPrevDiagonal('down')).toBe('rightDown');
    });

    it('returns `rightDown` for given `leftDown` direction', function () {
      expect(mapDirections.getPrevDiagonal('leftDown')).toBe('rightDown');
    });

    it('returns `leftDown` for given `left` direction', function () {
      expect(mapDirections.getPrevDiagonal('left')).toBe('leftDown');
    });

    it('returns `leftDown` for given `leftUp` direction', function () {
      expect(mapDirections.getPrevDiagonal('leftUp')).toBe('leftDown');
    });

    it('returns `leftUp` for given `up` direction', function () {
      expect(mapDirections.getPrevDiagonal('up')).toBe('leftUp');
    });

    it('returns `leftUp` for given `rightUp` direction', function () {
        expect(mapDirections.getPrevDiagonal('rightUp')).toBe('leftUp');
    });

  });
  
});