'use strict';

describe('MapBlocksSchema', function() {
  beforeEach(module('square'));

  var MapBlocksSchema, MapBlocks, improvableBlockKindRegExp,
    Joi;

  beforeEach(inject(function (_MapBlocksSchema_, _MapBlocks_, _improvableBlockKindRegExp_,
                              _Joi_) {
    MapBlocksSchema = _MapBlocksSchema_;
    MapBlocks = _MapBlocks_;
    improvableBlockKindRegExp = _improvableBlockKindRegExp_;

    Joi = _Joi_;
  }));

  describe('MapBlocks', function () {

    it('validates against its schema', function (done) {
      Joi.validate(new MapBlocks(), MapBlocksSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });


    it('blockKinds consists only improvable block kinds', function (done) {
      Joi.validate(MapBlocks.blockKinds, Joi.array().items(
        Joi.string().regex(improvableBlockKindRegExp)
      ), {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

  });

});