'use strict';

angular.module('square').factory('MapBlocksSchema', [
    'Joi', 'playerIdRegExp', 'improvableBlockKindRegExp', 'BlockSchema',
    'MapBlocks',
    function (Joi, playerIdRegExp, improvableBlockKindRegExp, BlockSchema,
              MapBlocks) {

  return Joi.object().keys({
    blocksByPlayerAndKind: Joi.object({})
      .pattern(playerIdRegExp, Joi.object({})
        .pattern(improvableBlockKindRegExp, Joi.array().items(
          BlockSchema
        ))
      )
  }).type(MapBlocks);

}]);