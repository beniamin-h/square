'use strict';

angular.module('square').factory('MapBlocks', ['ArrayUtils',
    function (ArrayUtils) {

  function MapBlocks() {
    this.blocksByPlayerAndKind = {};
  }

  MapBlocks.blockKinds = ['plain', 'forest', 'hill', 'mountain',
                          'coalfield', 'wildHorses',
                          'silverDeposit', 'goldDeposit', 'gemstoneDeposit'];

  MapBlocks.prototype.blocksByPlayerAndKind = null;

  MapBlocks.prototype.setPlayers = function (players) {
    Object.keys(players).forEach(function (playerId) {
      this.blocksByPlayerAndKind[playerId] = MapBlocks.blockKinds
        .reduce(function (dict, kind) {
          dict[kind] = [];
          return dict;
        }, {});
    }, this);
  };

  MapBlocks.prototype.addOwnedBlock = function (block) {
    this.blocksByPlayerAndKind[block.ownedBy.playerId][block.kind.toString()]
      .push(block);
  };

  MapBlocks.prototype.removeOwnedBlock = function (block, player) {
    ArrayUtils.removeElement(
      this.blocksByPlayerAndKind[player.playerId][block.kind.toString()],
      block
    );
  };

  MapBlocks.prototype.changeOwnedBlockKind = function (block, oldKind, newKind) {
    ArrayUtils.removeElement(
      this.blocksByPlayerAndKind[block.ownedBy.playerId][oldKind.toString()],
      block
    );
    this.blocksByPlayerAndKind[block.ownedBy.playerId][newKind.toString()]
      .push(block);
  };

  MapBlocks.prototype.getBlocksByPlayerAndKind = function (player, kind) {
    return this.blocksByPlayerAndKind[player.playerId][kind];
  };

  return MapBlocks;

}]);