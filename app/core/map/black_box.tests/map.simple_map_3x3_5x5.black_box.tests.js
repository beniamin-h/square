'use strict';

describe('Map:simple_map_3x3_5x5', function() {
  beforeEach(module('square'));

  var Map, Region, Block, Square, storageProvider, Serialijse,
    Water, Coalfield, WildHorses, GoldDeposit, SilverDeposit, GemstoneDeposit,
    Plain, Mountain, Hill, Forest, playerProvider, HumanPlayer, AIPlayer,
    Storage, AI, AISquaring, gameStateProvider,
    mapProvider, SquareFactory, SquaresMap, squareProvider, EventEmitter,
    MapBlocks, StorageResourceShortage, Ticker, Rand, RandStatefulGenerator;

  beforeEach(inject(function (_Map_, _Region_, _Block_, _Square_, _storageProvider_, _Serialijse_,
      _Water_, _Coalfield_, _WildHorses_, _GoldDeposit_, _SilverDeposit_, _GemstoneDeposit_,
      _Plain_, _Mountain_, _Hill_, _Forest_, _playerProvider_, _HumanPlayer_, _AIPlayer_,
      _Storage_, _AI_, _AISquaring_, _gameStateProvider_,
      _mapProvider_, _SquareFactory_, _SquaresMap_, _squareProvider_,
      _MapBlocks_, _StorageResourceShortage_, _Ticker_, _Rand_, _RandStatefulGenerator_) {
    Map = _Map_;
    Region = _Region_;
    Block = _Block_;
    Square = _Square_;
    storageProvider = _storageProvider_;
    Serialijse = _Serialijse_;

    Water = _Water_;
    Coalfield = _Coalfield_;
    WildHorses = _WildHorses_;
    GoldDeposit = _GoldDeposit_;
    SilverDeposit = _SilverDeposit_;
    GemstoneDeposit = _GemstoneDeposit_;

    Plain = _Plain_;
    Mountain = _Mountain_;
    Hill = _Hill_;
    Forest = _Forest_;
    playerProvider = _playerProvider_;
    HumanPlayer = _HumanPlayer_;
    AIPlayer = _AIPlayer_;

    Storage = _Storage_;
    AI = _AI_;
    AISquaring = _AISquaring_;
    gameStateProvider = _gameStateProvider_;

    mapProvider = _mapProvider_;
    SquareFactory = _SquareFactory_;
    SquaresMap = _SquaresMap_;
    squareProvider = _squareProvider_;

    EventEmitter = require('events');
    MapBlocks = _MapBlocks_;
    StorageResourceShortage = _StorageResourceShortage_;
    Ticker = _Ticker_;
    Rand = _Rand_;
    RandStatefulGenerator = _RandStatefulGenerator_;
  }));

  function getBlocksBaseAiValues(blocks) {
    return blocks.map(function (block) {
      return block.baseAiValue;
    });
  }

  function getBlocksAiValuesForPlayer(blocks, playerId) {
    return blocks.map(function (block) {
      return block.aiValueForPlayer[playerId];
    });
  }

  it('loads Serialijse', function () {
    expect(Serialijse).toBeFunction();
  });

  describe('simple_map_3x3_5x5 fixture', function () {
    var map;
    var fixture;
    var _Map, _Region, _Block, _storageProvider, _playerProvider,
      _HumanPlayer, _AIPlayer, _mapProvider, _squareProvider;
    var serialijse;
    var simple_map_3x3_5x5_fixtureString;
    var _map, _player, _storage;

    beforeAll(function () {
      serialijse = new Serialijse();
      serialijse.declarePersistable(Water);
      serialijse.declarePersistable(Coalfield);
      serialijse.declarePersistable(WildHorses);
      serialijse.declarePersistable(GoldDeposit);
      serialijse.declarePersistable(SilverDeposit);
      serialijse.declarePersistable(GemstoneDeposit);
      serialijse.declarePersistable(Plain);
      serialijse.declarePersistable(Mountain);
      serialijse.declarePersistable(Hill);
      serialijse.declarePersistable(Forest);
      serialijse.declarePersistable(Block);
      serialijse.declarePersistable(Region);
      serialijse.declarePersistable(Map);
      serialijse.declarePersistable(HumanPlayer);
      serialijse.declarePersistable(AIPlayer);
      serialijse.declarePersistable(Storage);
      serialijse.declarePersistable(AI);
      serialijse.declarePersistable(Int8Array);
      serialijse.declarePersistable(AISquaring);
      serialijse.declarePersistable(SquareFactory);
      serialijse.declarePersistable(SquaresMap);
      serialijse.declarePersistable(MapBlocks);
      serialijse.declarePersistable(StorageResourceShortage);
      serialijse.declarePersistable(Ticker);
      serialijse.declarePersistable(RandStatefulGenerator);
      serialijse.declarePersistable(Rand);
      serialijse.declareUnserializable(EventEmitter);
      _Map = Map;
      _Region = Region;
      _Block = Block;
      _storageProvider = storageProvider;
      _playerProvider = playerProvider;
      _HumanPlayer = HumanPlayer;
      _AIPlayer = AIPlayer;
      _mapProvider = mapProvider;
      _squareProvider = squareProvider;
      simple_map_3x3_5x5_fixtureString = readJSON('test_fixtures/simple_map_3x3_5x5.json');
    });

    function loadGame() {
      fixture = serialijse.deserialize(simple_map_3x3_5x5_fixtureString);
      map = fixture.map;
      _map = map;
      _playerProvider.load(fixture.players);
      _storageProvider.loadStorages(fixture.storages);
      _mapProvider.loadMap(map);
      _squareProvider.loadSquareFactory(fixture.squares.factory);
      _squareProvider.loadSquaresMap(fixture.squares.map);
      _player = _playerProvider.getHuman();
      _storage = _storageProvider.getStorage(_player);
    }

    beforeEach(function () {
      loadGame();
    });

    it('deserialize itself correctly', function () {
      var playersCount = 2;
      expect(map instanceof _Map).toBeTrue();
      expect(typeof map.players).toBe('object');
      expect(Object.keys(map.players).length).toBe(playersCount);
      expect(map.players['player-human'] instanceof _HumanPlayer).toBeTrue();
      expect(map.players['player-AI-0'] instanceof _AIPlayer).toBeTrue();
      var humanPlayer = map.players['player-human'];
      var aiPlayer = map.players['player-AI-0'];
      expect(typeof map.initRegions).toBe('function');
      expect(typeof map.squaresMap.squares).toBe('object');
      expect(Object.keys(map.squaresMap.squares).length).toBe(playersCount);
      expect(typeof map.borderRegionsCoords).toBe('object');
      expect(Object.keys(map.borderRegionsCoords).length).toBe(4);
      expect(map.blockWidthPx).toBe(50);
      expect(map.blockHeightPx).toBe(50);
      expect(map.regionWidthPx).toBe(250);
      expect(map.regionHeightPx).toBe(250);
      expect(map.initSizeRegions).toBe(3);
      expect(typeof map.affordableBlockCount).toBe('object');
      expect(Object.keys(map.affordableBlockCount).length).toBe(playersCount);
      expect(map.affordableBlockCount[humanPlayer.playerId]).toBe(0);
      expect(map.affordableBlockCount[aiPlayer.playerId]).toBe(0);
      expect(typeof map.improvementNeighbors).toBe('object');
      expect(Object.keys(map.improvementNeighbors).length).toBe(playersCount);
      expect(typeof map.regions).toBe('object');
      expect(Object.keys(map.regions).length).toBe(3);
      expect(Object.keys(map.regions[0]).length).toBe(3);
      var region = map.getRegionByXY(1, 0);
      expect(region instanceof _Region).toBeTrue();
      expect(Array.isArray(region.blocks)).toBeTrue();
      expect(region.blocks.length).toBe(5);
      expect(Array.isArray(region.blocks[0])).toBeTrue();
      expect(region.blocks[0].length).toBe(5);
      expect(region.blocks[0][0] instanceof _Block).toBeTrue();
    });

    describe('updateNeighborBlocksAffordability', function () {

      it('sets proper blocks proper affordability', function () {
        _Region.blocksWidth = fixture.regionBlocksWidth;
        _Region.blocksHeight = fixture.regionBlocksHeight;
        var player = _playerProvider.getHuman();
        var anotherPlayer = _playerProvider.getById('player-AI-0');
        var allBlocksNonAffordable = !map.getRegionByXY(0, 0).blocks
          .map(function (blockRow) {
            return blockRow.map(function (block) {
              return block.affordableForPlayer(player);
            }).reduce(function (a, b) {
              return a || b;
            });
          })
          .reduce(function (a, b) {
            return a || b;
          });
        expect(allBlocksNonAffordable).toBeTrue();
        spyOn(map, 'updateNeighborBlocksAffordability').and.callThrough();
        expect(map.affordableBlockCount[player.playerId]).toBe(0);
        var foodBlock = map.getBlock(0, 0, 1, 3);
        var neighborMetalBlock = map.getBlock(0, 0, 0, 3);
        var neighborStoneBlock = map.getBlock(0, 0, 1, 4);
        var neighborWoodBlock = map.getBlock(0, 0, -1, 3);
        var neighborRichWoodBlock = map.getBlock(0, 0, 1, 5);
        var anotherNeighborMetalBlock = map.getBlock(0, 0, 0, 4);
        var anotherMetalBlock = map.getBlock(0, 0, 2, 5);
        expect(neighborMetalBlock.affordableForPlayer(player)).toBeFalse();
        expect(neighborStoneBlock.affordableForPlayer(player)).toBeFalse();
        expect(neighborWoodBlock.affordableForPlayer(player)).toBeFalse();
        expect(neighborRichWoodBlock.affordableForPlayer(player)).toBeFalse();
        expect(anotherNeighborMetalBlock.affordableForPlayer(player)).toBeFalse();
        expect(anotherMetalBlock.affordableForPlayer(player)).toBeFalse();
        expect(neighborMetalBlock.affordableForPlayer(anotherPlayer)).toBeFalse();
        expect(anotherMetalBlock.affordableForPlayer(anotherPlayer)).toBeFalse();
        map.tryToCaptureBlock(foodBlock, player);
        expect(map.updateNeighborBlocksAffordability).toHaveBeenCalled();
        expect(map.affordableBlockCount[player.playerId]).toBe(1);
        expect(neighborMetalBlock.affordableForPlayer(player)).toBeTrue();
        expect(neighborMetalBlock.affordableForPlayer(anotherPlayer)).toBeFalse();
        map.tryToCaptureBlock(neighborMetalBlock, player);
        expect(map.updateNeighborBlocksAffordability).toHaveBeenCalled();
        expect(map.affordableBlockCount[player.playerId]).toBe(1);
        expect(neighborStoneBlock.affordableForPlayer(player)).toBeTrue();
        map.tryToCaptureBlock(neighborStoneBlock, player);
        expect(map.updateNeighborBlocksAffordability).toHaveBeenCalled();
        expect(map.affordableBlockCount[player.playerId]).toBe(2);
        expect(neighborWoodBlock.affordableForPlayer(player)).toBeTrue();
        expect(neighborWoodBlock.affordableForPlayer(anotherPlayer)).toBeFalse();
        expect(neighborRichWoodBlock.affordableForPlayer(player)).toBeTrue();
        expect(neighborRichWoodBlock.affordableForPlayer(anotherPlayer)).toBeFalse();
        expect(anotherNeighborMetalBlock.affordableForPlayer(player)).toBeFalse();
        expect(anotherMetalBlock.affordableForPlayer(player)).toBeFalse();
      });

    });

    describe('tryToCaptureBlock', function () {

      it('improves block to a higher level', function () {
        var foodBlock = _map.getBlock(1, 0, 0, 2);
        expect(foodBlock.level).toBe(0);
        var result = _map.tryToCaptureBlock(foodBlock, _player);
        expect(foodBlock.level).toBe(1);
        expect(result).toBeTrue();
      });

      it('improves another block to a higher level', function () {
        var foodBlock = _map.getBlock(1, 0, 0, 2);
        var metalBlock = _map.getBlock(1, 0, 0, 1);
        _map.tryToCaptureBlock(foodBlock, _player);
        var result = _map.tryToCaptureBlock(metalBlock, _player);
        expect(result).toBeTrue();
        expect(metalBlock.level).toBe(1);
      });

      it('improves block to the level of a created square', function () {
        var foodBlock = _map.getBlock(1, 0, 0, 2);
        var metalBlock = _map.getBlock(1, 0, 0, 1);
        var stoneBlock = _map.getBlock(0, 0, 4, 1);
        var woodBlock = _map.getBlock(0, 0, 4, 2);
        _map.tryToCaptureBlock(foodBlock, _player);
        _map.tryToCaptureBlock(metalBlock, _player);
        _map.tryToCaptureBlock(stoneBlock, _player);
        var result = _map.tryToCaptureBlock(woodBlock, _player);
        expect(result).toBeTrue();
        expect(foodBlock.level).toBe(1);
        expect(metalBlock.level).toBe(1);
        expect(stoneBlock.level).toBe(1);
        expect(woodBlock.level).toBe(2);
      });

      it('cannot improve the same block twice', function () {
        var foodBlock = _map.getBlock(1, 0, 0, 2);
        expect(foodBlock.level).toBe(0);
        var result = _map.tryToCaptureBlock(foodBlock, _player);
        expect(foodBlock.level).toBe(1);
        expect(result).toBeTrue();
        var result_2 = _map.tryToCaptureBlock(foodBlock, _player);
        expect(foodBlock.level).toBe(1);
        expect(result_2).toBeFalse();
      });

      it('does not improve block to a higher level if it does not have a neighbor improved ' +
         'and it is not the first improvement', function () {
        var foodBlock = _map.getBlock(1, 0, 0, 2);
        var result = _map.tryToCaptureBlock(foodBlock, _player);
        expect(foodBlock.level).toBe(1);
        expect(result).toBeTrue();
        var metalBlock = _map.getBlock(0, 0, 3, 4);
        var resultMetal = _map.tryToCaptureBlock(metalBlock, _player);
        expect(metalBlock.level).toBe(0);
        expect(resultMetal).toBeFalse();
      });

      it('does not improve block to a higher level if it has one neighbor improved by another player ' +
         'and it is not the first improvement', function () {
        var anotherPlayer = _playerProvider.getById('player-AI-0');
        var foodBlock = _map.getBlock(1, 0, 0, 2);
        var result = _map.tryToCaptureBlock(foodBlock, anotherPlayer);
        expect(foodBlock.level).toBe(1);
        expect(result).toBeTrue();
        var metalBlock = _map.getBlock(1, 0, 0, 1);
        var resultMetal = _map.tryToCaptureBlock(metalBlock, _player);
        expect(metalBlock.level).toBe(0);
        expect(resultMetal).toBeFalse();
      });

      it('cannot improve exhausted block', function () {
        var foodBlock = _map.getBlock(1, 0, 0, 2);
        foodBlock.exhausted = true;
        expect(foodBlock.level).toBe(0);
        var result = _map.tryToCaptureBlock(foodBlock, _player);
        expect(foodBlock.level).toBe(0);
        expect(result).toBeFalse();
      });

      it('cannot improve non-improvable block', function () {
        var waterBlock = _map.getBlock(0, 0, 4, 4);
        expect(waterBlock.level).toBe(0);
        var result = _map.tryToCaptureBlock(waterBlock, _player);
        expect(waterBlock.level).toBe(0);
        expect(result).toBeFalse();
      });

      it('cannot improve a higher level block', function () {
        var foodBlock = _map.getBlock(1, 0, 0, 2);
        foodBlock.level = 8;
        expect(foodBlock.level).toBe(8);
        var result = _map.tryToCaptureBlock(foodBlock, _player);
        expect(foodBlock.level).toBe(8);
        expect(result).toBeFalse();
      });

      it('does not improve a block if not sufficient resources in a player storage', function () {
        var foodBlock = _map.getBlock(1, 0, 0, 2);
        var taken = _storage.take({wood: 1});
        expect(taken).toBeTrue();
        expect(foodBlock.level).toBe(0);
        var result = _map.tryToCaptureBlock(foodBlock, _player);
        expect(foodBlock.level).toBe(0);
        expect(result).toBeFalse();
      });

      it('updates neighbor blocks affordability', function () {
        var foodBlock = _map.getBlock(0, 0, 1, 3);
        var metalBlock = _map.getBlock(0, 0, 0, 3);
        expect(foodBlock.level).toBe(0);
        spyOn(_map, 'updateNeighborBlocksAffordability').and.callThrough();
        var result = _map.tryToCaptureBlock(foodBlock, _player);
        expect(foodBlock.level).toBe(1);
        expect(result).toBeTrue();
        expect(_map.updateNeighborBlocksAffordability).toHaveBeenCalled();
        expect(metalBlock.affordableForPlayer(_player)).toBeTrue();
      });

      it('changes block ownership', function () {
        var foodBlock = _map.getBlock(0, 0, 1, 3);
        expect(foodBlock.level).toBe(0);
        expect(foodBlock.ownedBy).toBeNull();
        var result = _map.tryToCaptureBlock(foodBlock, _player);
        expect(foodBlock.level).toBe(1);
        expect(result).toBeTrue();
        expect(foodBlock.ownedBy).toBe(_player);
      });

      it('puts resource gain into the player storage', function () {
        var foodBlock = _map.getBlock(0, 0, 1, 3);
        expect(foodBlock.level).toBe(0);
        expect(_storage.has({wood: 1}, _player)).toBeTrue();
        expect(_storage.has({food: 1}, _player)).toBeFalse();
        var result = _map.tryToCaptureBlock(foodBlock, _player);
        expect(foodBlock.level).toBe(1);
        expect(result).toBeTrue();
        expect(_storage.has({food: 1}, _player)).toBeTrue();
      });

      it('changes player gold income by the gold gain', function () {
        var foodBlock = _map.getBlock(0, 0, 1, 3);
        expect(foodBlock.level).toBe(0);
        _storage.goldIncome = 0;
        foodBlock.kind.baseGoldIncome = 10;
        var result = _map.tryToCaptureBlock(foodBlock, _player);
        expect(result).toBeTrue();
        expect(_storage.goldIncome).toBe(10);
      });

      it('takes resource cost from the player storage', function () {
        var anotherPlayer = _playerProvider.getById('player-AI-0');
        var foodBlock = _map.getBlock(0, 0, 1, 3);
        expect(foodBlock.level).toBe(0);
        expect(_storage.has({wood: 1}, _player)).toBeTrue();
        var anotherPlayerStorage = _storageProvider.getStorage(anotherPlayer);
        expect(anotherPlayerStorage.has({wood: 1})).toBeTrue();
        var result = _map.tryToCaptureBlock(foodBlock, _player);
        expect(foodBlock.level).toBe(1);
        expect(result).toBeTrue();
        expect(_storage.has({wood: 1})).toBeFalse();
        expect(anotherPlayerStorage.has({wood: 1})).toBeTrue();
      });

      it('notify neighbor blocks about improvement', function () {
        var foodBlock = _map.getBlock(0, 0, 1, 3);
        expect(foodBlock.level).toBe(0);
        spyOn(_map, 'neighborImproved').and.callThrough();
        var result = _map.tryToCaptureBlock(foodBlock, _player);
        expect(foodBlock.level).toBe(1);
        expect(result).toBeTrue();
        expect(_map.neighborImproved).toHaveBeenCalled();
        var neighbors = [_map.getBlock(0, 0, 0, 3), _map.getBlock(0, 0, 2, 3),
          _map.getBlock(0, 0, 1, 2), _map.getBlock(0, 0, 1, 4)];
        expect(neighbors.map(function (neighbor) { return neighbor.hasImprovedNeighbor(_player); })
          .reduce(function (a, b) { return a && b; })).toBeTrue();
        var nonNeighbors = [_map.getBlock(0, 0, 1, 3), _map.getBlock(0, 0, 2, 2),
          _map.getBlock(0, 0, 5, 2), _map.getBlock(0, 0, 0, 2)];
        expect(nonNeighbors.map(function (neighbor) { return neighbor.hasImprovedNeighbor(_player); })
          .reduce(function (a, b) { return a || b; })).toBeFalse();
      });

      it('increments improved blocks counter', function () {
        var foodBlock = _map.getBlock(0, 0, 1, 3);
        expect(_map.improvedBlocksCounter[_player.playerId]).toBe(0);
        _map.tryToCaptureBlock(foodBlock, _player);
        expect(_map.improvedBlocksCounter[_player.playerId]).toBe(1);
      });

      it('updates neighborhood improvedNeighbors', function () {
        var block = _map.getBlock(0, 0, 1, 3);
        expect(_map.getBlock(0, 0, 1, 4).improvedNeighbors.left).toBeUndefined();
        expect(_map.getBlock(0, 0, 1, 2).improvedNeighbors.down).toBeUndefined();
        expect(_map.getBlock(0, 0, 1, -1).improvedNeighbors.rightDown).toBeUndefined();
        expect(_map.getBlock(0, 0, 1, 1).improvedNeighbors.rightUp).toBeUndefined();
        _map.tryToCaptureBlock(block, _player);
        expect(_map.getBlock(0, 0, 2, 3).improvedNeighbors.left).toBe(block);
        expect(_map.getBlock(0, 0, 1, 2).improvedNeighbors.down).toBe(block);
        expect(_map.getBlock(0, 0, 0, 2).improvedNeighbors.rightDown).toBe(block);
        expect(_map.getBlock(0, 0, 0, 4).improvedNeighbors.rightUp).toBe(block);
      });

      it('updates neighborhood ai values for Human player', function () {
        var block = _map.getBlock(0, 0, 1, 3);
        _player = _playerProvider.getHuman();
        spyOn(_map, 'updateBlockAiValuesAfterImproving').and.callThrough();
        _map.tryToCaptureBlock(block, _player);
        expect(_map.updateBlockAiValuesAfterImproving).toHaveBeenCalledWith(block, _player);
      });

      it('updates neighborhood ai values for AI player', function () {
        var block = _map.getBlock(0, 0, 1, 3);
        _player = _playerProvider.getFirstAI();
        spyOn(_map, 'updateBlockAiValuesAfterImproving').and.callThrough();
        _map.tryToCaptureBlock(block, _player);
        expect(_map.updateBlockAiValuesAfterImproving).toHaveBeenCalledWith(block, _player);
      });

      it('causes mutation of the same blocks for the same gameSeed', function () {
        function makeTest() {
          const blocks = [
            _map.getBlock(0, 0, 1, 3),
            _map.getBlock(0, 0, 0, 3),
            _map.getBlock(0, 0, 1, 4),
            _map.getBlock(0, 1, 1, 0),
          ];
          _player = _playerProvider.getFirstAI();
          spyOn(_map, 'getBlockMutationProbability').and.returnValue(0.25);
          spyOn(_map, 'mutateBlockKind').and.callThrough();
          for (const block of blocks) {
            expect(_map.mutateBlockKind).not.toHaveBeenCalled();
            _map.tryToCaptureBlock(block, _player);
          }
          expect(_map.mutateBlockKind).toHaveBeenCalledWith(blocks[3]);
        }
        makeTest();
        loadGame();
        makeTest();
      });

      it('causes mutation of different blocks for the different gameSeed', function () {
        function makeTest() {
          const blocks = [
            _map.getBlock(0, 0, 1, 3),
            _map.getBlock(0, 0, 0, 3),
            _map.getBlock(0, 0, 1, 4),
            _map.getBlock(0, 1, 1, 0),
          ];
          _player = _playerProvider.getFirstAI();
          spyOn(_map, 'getBlockMutationProbability').and.returnValue(0.75);
          spyOn(_map, 'mutateBlockKind').and.callThrough();
          for (const block of blocks) {
            _map.tryToCaptureBlock(block, _player);
          }
        }
        _map.setRandomness(792156799, _map.mapSeed);
        makeTest();
        const oldCalls = _map.mutateBlockKind.calls.allArgs();
        _map.mutateBlockKind.calls.reset();
        loadGame();
        _map.setRandomness(1452352345, _map.mapSeed);
        makeTest();
        const newCalls = _map.mutateBlockKind.calls.allArgs();
        expect(oldCalls[0].toString()).not.toEqual(newCalls[0].toString());
      });

    });

    describe('isFirstImprovement', function () {

      it('returns false after improving block', function () {
        var foodBlock = _map.getBlock(0, 0, 1, 3);
        expect(_map.isFirstImprovement(_player)).toBeTrue();
        _map.tryToCaptureBlock(foodBlock, _player);
        expect(_map.isFirstImprovement(_player)).toBeFalse();
      });

      it('returns true after another player block improvement', function () {
        var anotherPlayer = _playerProvider.getById('player-AI-0');
        var foodBlock = _map.getBlock(0, 0, 1, 3);
        expect(_map.isFirstImprovement(_player)).toBeTrue();
        _map.tryToCaptureBlock(foodBlock, anotherPlayer);
        expect(_map.isFirstImprovement(_player)).toBeTrue();
      });

    });

  });

  describe('simple_map_3x3_5x5 loaded by gameStateProvider', function () {

    var map;

    beforeEach(function (done) {
      gameStateProvider.load(process.env.PWD + '/test_fixtures/simple_map_3x3_5x5.json', function (err) {
        if (err) {
          done.fail(err);
          return;
        }
        map = mapProvider.getMap();
        done();
      });
    });

    describe('changeBlockKind', function () {

      it('changes the given block kind', function () {
        var forestBlock = map.getBlock(1, 0, 1, 1);
        expect(forestBlock.kind instanceof Forest).toBeTrue();
        var newKind = new Hill();
        map.changeBlockKind(forestBlock, newKind, false);
        expect(forestBlock.kind).toBe(newKind);
      });

      it('changes the given block richness', function () {
        var forestBlock = map.getBlock(1, 0, 1, 1);
        expect(forestBlock.rich).toBeFalse();
        map.changeBlockKind(forestBlock, new Forest(), true);
        expect(forestBlock.rich).toBeTrue();
      });

      it('updates the given block and its neighbor blocks baseAiValue and aiValueForPlayer properties', function () {
        var forestBlock = map.getBlock(1, 0, 1, 1);
        expect(forestBlock.kind instanceof Forest).toBeTrue();
        var neighborBlocks = map.getBlocks(1, 0, -1, -1, 5, 5);
        var aiPlayerId = playerProvider.getFirstAI().playerId;
        var baseAiValues = getBlocksBaseAiValues(neighborBlocks);
        var aiValuesForPlayer = getBlocksAiValuesForPlayer(neighborBlocks, aiPlayerId);
        map.changeBlockKind(forestBlock, new Mountain());
        expect(forestBlock.kind instanceof Mountain).toBeTrue();
        var updatedBaseAiValues = getBlocksBaseAiValues(neighborBlocks);
        var updatedAiValuesForPlayer = getBlocksAiValuesForPlayer(neighborBlocks, aiPlayerId);
        for (var i = 0; i < 5 * 5; i++) {
          expect(updatedBaseAiValues[i]).toBeGreaterThan(baseAiValues[i]);
          expect(updatedAiValuesForPlayer[i]).toBeGreaterThan(aiValuesForPlayer[i]);
        }
      });

      it('calls _updateNeighborBlocksNeighborsByKind', function () {
        var forestBlock = map.getBlock(1, 0, 1, 1);
        expect(map.getBlock(1, 0, 1, 2).neighborsByKind['hill']).not.toContain(forestBlock);
        expect(map.getBlock(1, 0, 1, 2).neighborsByKind['forest']).toContain(forestBlock);
        spyOn(map, '_updateNeighborBlocksNeighborsByKind').and.callThrough();
        map.changeBlockKind(forestBlock, new Hill());
        expect(forestBlock.kind instanceof Hill).toBeTrue();
        expect(map._updateNeighborBlocksNeighborsByKind).toHaveBeenCalledWith(forestBlock);
        expect(map.getBlock(1, 0, 1, 2).neighborsByKind['hill']).toContain(forestBlock);
        expect(map.getBlock(1, 0, 1, 2).neighborsByKind['forest']).not.toContain(forestBlock);
      });

      it('calls updateNeighborBlocksAffordability for each player', function () {
        var forestBlock = map.getBlock(1, 0, 1, 1);
        spyOn(map, 'updateBlocksAffordability');
        map.changeBlockKind(forestBlock, new Forest());
        expect(map.updateBlocksAffordability).toHaveBeenCalledWith();
      });

    });

    describe('_updateNeighborBlocksNeighborsByKind', function () {

      it('updates neighborsByKind property of 4 neighbor blocks', function () {
        var mountainBlock = map.getBlock(0, 1, 2, 2);
        map.getBlockAdjacentNeighbors(mountainBlock).forEach(function (neighbor) {
          neighbor.neighborsByKind = {};
        });
        map._updateNeighborBlocksNeighborsByKind(mountainBlock);
        map.getBlockAdjacentNeighbors(mountainBlock).forEach(function (neighbor) {
          expect(neighbor.neighborsByKind['mountain']).toContain(mountainBlock);
        });
        expect(map.getBlock(0, 1, 2, 3).neighborsByKind['mountain']).toContain(mountainBlock);
        expect(map.getBlock(0, 1, 2, 1).neighborsByKind['mountain']).toContain(mountainBlock);
      });

      it('calls resetNeighborsByKind for 4 neighbor blocks', function () {
        var mountainBlock = map.getBlock(0, 1, 2, 2);
        spyOn(Block.prototype, 'resetNeighborsByKind').and.callThrough();
        map._updateNeighborBlocksNeighborsByKind(mountainBlock);
        map.getBlockAdjacentNeighbors(mountainBlock).forEach(function (neighbor) {
          expect(neighbor.resetNeighborsByKind).toHaveBeenCalled();
        });
      });

    });

  });

});