'use strict';

describe('Map:easy_map_11x11_5x5', function() {
  beforeEach(module('square'));

  let gameStateProvider, mapProvider, Rand;

  beforeEach(inject(function (_gameStateProvider_, _mapProvider_, _Rand_) {
    gameStateProvider = _gameStateProvider_;
    mapProvider = _mapProvider_;
    Rand = _Rand_;
  }));

  describe('easy_map_11x11_5x5 loaded by gameStateProvider', function () {

    let map;
    
    function loadGame(onError, onSuccess) {
      gameStateProvider.load(process.env.PWD + 
          '/test_fixtures/easy_map_11x11_5x5.json', function (err) {
        if (err) {
          onError(err);
          return;
        }
        map = mapProvider.getMap();
        onSuccess();
      });
    }

    beforeEach(function (done) {
      loadGame(done.fail, done);
    });

    describe('autonomousBlockKindMutation', function () {

      function tryToMutate(limit) {
        map.baseAutonomousBlockMutationProbability = 0.05;
        let counter = 0;
        while (map.mutateBlockKind.calls.count() < 5 &&
               counter++ < limit) {
          map.autonomousBlockKindMutation();
        }
        return counter;
      }

      function getMutatedBlocks() {
        return map.mutateBlockKind.calls.allArgs()
          .map((args) => args[0].toString());
      }

      it('changes the same blocks for the same gameSeed', function (done) {
        const limit = 2000;
        spyOn(map, 'mutateBlockKind').and.callThrough();
        const firstTriesCount = tryToMutate(limit);
        expect(firstTriesCount).toBeLessThan(limit);
        const mutatedBlocks = getMutatedBlocks();
        loadGame(done.fail, function () {
          spyOn(map, 'mutateBlockKind').and.callThrough();
          map.setRandomness(map.gameSeed, map.mapSeed);
          const secondTriesCount = tryToMutate(limit);
          expect(secondTriesCount).toBeLessThan(limit);
          expect(firstTriesCount).toBe(secondTriesCount);
          expect(mutatedBlocks).toEqual(getMutatedBlocks());
          done();
        });
      });

      it('changes different blocks for the different gameSeed', function (done) {
        const limit = 2000;
        spyOn(map, 'mutateBlockKind').and.callThrough();
        const firstTriesCount = tryToMutate(limit);
        expect(firstTriesCount).toBeLessThan(limit);
        const mutatedBlocks = getMutatedBlocks();
        loadGame(done.fail, function () {
          spyOn(map, 'mutateBlockKind').and.callThrough();
          map.setRandomness(Rand.getUnpredictableInt31Unsigned(), map.mapSeed);
          const secondTriesCount = tryToMutate(limit);
          expect(secondTriesCount).toBeLessThan(limit);
          expect(mutatedBlocks).not.toEqual(getMutatedBlocks());
          done();
        });
      });

    });
    
  });

});