'use strict';

describe('Map:init_provider', function() {
  beforeEach(module('square'));

  var Region, Block, storageProvider,
    Coalfield, WildHorses, GoldDeposit,
    Plain, Mountain, Hill, Forest, playerProvider,
    mapProvider, initProvider, Rand;

  beforeEach(inject(function (_Region_, _Block_, _storageProvider_,
      _Coalfield_, _WildHorses_, _GoldDeposit_,
      _Plain_, _Mountain_, _Hill_, _Forest_, _playerProvider_,
      _mapProvider_, _initProvider_, _Rand_) {
    Region = _Region_;
    Block = _Block_;
    storageProvider = _storageProvider_;

    Coalfield = _Coalfield_;
    WildHorses = _WildHorses_;
    GoldDeposit = _GoldDeposit_;

    Plain = _Plain_;
    Mountain = _Mountain_;
    Hill = _Hill_;
    Forest = _Forest_;
    playerProvider = _playerProvider_;

    mapProvider = _mapProvider_;
    initProvider = _initProvider_;
    Rand = _Rand_;
  }));

  function getBlocksBaseAiValues(blocks) {
    return blocks.map(function (block) {
      return block.baseAiValue;
    });
  }

  function getBlocksTmpAiValues(blocks) {
    return blocks.map(function (block) {
      return block.tmpAiValue;
    });
  }

  describe('created by initProvider', function () {

    var map;

    beforeEach(function () {
      initProvider.initNewGame({ mapSize: 5 });
      map = mapProvider.getMap();
    });

    describe('_setNewRegionsBlocksNeighborsAndAiValues', function () {

      function ValueStorage() {
        this.neighborsByKind = [];
        this.neighborsByPosition = [];
        this.baseAiValues = [];
        this.aiValuesForPlayer = [];
      }

      function setValuesToCompare(valueStorage) {
        map.getBlocks(-1, -1, 0, 0, Region.blocksWidth * 3, Region.blocksHeight * 3)
          .forEach(function (block) {
            valueStorage.neighborsByKind.push(block.neighborsByKind);
            valueStorage.neighborsByPosition.push(block.neighborsByPosition);
            valueStorage.baseAiValues.push(block.baseAiValue);
            valueStorage.aiValuesForPlayer.push(block.aiValueForPlayer);
          });
      }

      it('works the same way as initAllBlocksNeighborsAndTmpAiValues ' +
         'with initAllBlocksBaseAiValues does', function () {
        var region = map.regions[0][0];
        var oldValueStorage = new ValueStorage();
        setValuesToCompare(oldValueStorage);
        map.resetNeighborsByKindForAllBlocks();
        region.blocks.forEach(function (row) {
          row.forEach(function (block) {
            block.aiValueForPlayer = {};
          });
        });
        map._setNewRegionsBlocksNeighborsAndAiValues(new Set([region]));
        var newValueStorage = new ValueStorage();
        setValuesToCompare(newValueStorage);
        expect(newValueStorage.neighborsByKind.length).toBe(Region.blocksWidth * 3 * Region.blocksHeight * 3);
        expect(newValueStorage.neighborsByPosition.length).toBe(Region.blocksWidth * 3 * Region.blocksHeight * 3);
        expect(newValueStorage.baseAiValues.length).toBe(Region.blocksWidth * 3 * Region.blocksHeight * 3);
        expect(newValueStorage.aiValuesForPlayer.length).toBe(Region.blocksWidth * 3 * Region.blocksHeight * 3);
        expect(newValueStorage.neighborsByKind).toEqual(oldValueStorage.neighborsByKind);
        expect(newValueStorage.neighborsByPosition).toEqual(oldValueStorage.neighborsByPosition);
        expect(newValueStorage.baseAiValues).toEqual(oldValueStorage.baseAiValues);
        expect(newValueStorage.aiValuesForPlayer).toEqual(oldValueStorage.aiValuesForPlayer);
      });

    });

    describe('getRegionByXY', function () {

      it('returns a proper region', function () {
        expect(map.getRegionByXY(1, 0)).toBe(map.regions[0][1]);
        expect(map.getRegionByXY(0, -1)).toBe(map.regions[-1][0]);
        expect(map.getRegionByXY(1, 0).x).toBe(1);
        expect(map.getRegionByXY(1, 0).y).toBe(0);
        expect(map.getRegionByXY(0, -1).x).toBe(0);
        expect(map.getRegionByXY(0, -1).y).toBe(-1);
      });

      it('returns null if map is fixed size and coords are beyond regions borders', function () {
        expect(map.getRegionByXY(12, 0)).toBeNull();
        expect(map.getRegionByXY(0, -1)).toBe(map.regions[-1][0]);
        expect(map.getRegionByXY(1, -9)).toBeNull();
        expect(map.getRegionByXY(-100, -100)).toBeNull();
      });

    });

    describe('getBlockAdjacentNeighbors', function () {

      it('returns proper blocks', function () {
        var blocks = map.getBlockAdjacentNeighbors(map.getBlock(0, 1, 2, 1));
        expect(blocks).toBeArray();
        expect(blocks.length).toBe(4);
        expect(blocks[0]).toBe(map.getBlock(0, 1, 2, 0));
        expect(blocks[1]).toBe(map.getBlock(0, 1, 3, 1));
        expect(blocks[2]).toBe(map.getBlock(0, 1, 2, 2));
        expect(blocks[3]).toBe(map.getBlock(0, 1, 1, 1));
      });

    });

    describe('getBlock', function () {

      it('returns Block instance', function () {
        expect(map.getBlock(0, 0, 1, 0) instanceof Block).toBeTrue();
      });

      it('returns null for coords beyond regions borders on a fixed size map', function () {
        expect(map.getBlock(1, 2, 1, 2000)).toBeNull();
        expect(map.getBlock(10, 1, 1, 2)).toBeNull();
        expect(map.getBlock(-11, 1, -11, 0)).toBeNull();
        expect(map.getBlock(1, 12, 1, 0)).toBeNull();
      });

    });

    describe('getBlocks', function () {

      it('returns an array of Block instances', function () {
        expect(map.getBlocks(0, 0, 1, 0, 1, 3)).toBeArrayOfObjects();
        expect(map.getBlocks(0, 0, 1, 0, 1, 3).reduce(function (prev, curr) {
          return prev && (curr instanceof Block);
        }, true)).toBeTrue();
      });

      it('returns an array of proper blocks', function () {
        var blocks = map.getBlocks(0, 0, 5, 4, 1, 4);
        expect(blocks[0].x).toBe(0);
        expect(blocks[0].y).toBe(4);
        expect(blocks[1].x).toBe(0);
        expect(blocks[1].y).toBe(0);
        expect(blocks[2].x).toBe(0);
        expect(blocks[2].y).toBe(1);
        expect(blocks[3].x).toBe(0);
        expect(blocks[3].y).toBe(2);
        expect(blocks.length).toBe(4);
      });

      it('returns an array of proper blocks #2', function () {
        var blocks = map.getBlocks(1, -1, -8, 2, 2, 3);
        expect(blocks[0].x).toBe(2);
        expect(blocks[0].y).toBe(2);
        expect(blocks[1].x).toBe(3);
        expect(blocks[1].y).toBe(2);
        expect(blocks[2].x).toBe(2);
        expect(blocks[2].y).toBe(3);
        expect(blocks[3].x).toBe(3);
        expect(blocks[3].y).toBe(3);
        expect(blocks[4].x).toBe(2);
        expect(blocks[4].y).toBe(4);
        expect(blocks[5].x).toBe(3);
        expect(blocks[5].y).toBe(4);
        expect(blocks.length).toBe(6);
      });

      it('returns an array of proper blocks on a fixed size map', function () {
        var blocks = map.getBlocks(2, 2, 3, 2, 5, 5);
        expect(blocks.length).toBe(6);
        expect(blocks[0].x).toBe(3);
        expect(blocks[0].y).toBe(2);
        expect(blocks[1].x).toBe(4);
        expect(blocks[1].y).toBe(2);
        expect(blocks[2].x).toBe(3);
        expect(blocks[2].y).toBe(3);
        expect(blocks[3].x).toBe(4);
        expect(blocks[3].y).toBe(3);
        expect(blocks[4].x).toBe(3);
        expect(blocks[4].y).toBe(4);
        expect(blocks[5].x).toBe(4);
        expect(blocks[5].y).toBe(4);
      });

    });

    describe('neighborImproved', function () {

      it('adds block neighborhood to improvementNeighbors', function () {
        var regionX = 1;
        var regionY = -1;
        var x = 0;
        var y = 1;
        var block = map.getBlock(regionX, regionY, x, y);
        map.neighborImproved(block, playerProvider.getHuman());
        expect(map.improvementNeighbors[playerProvider.getHuman().playerId]).toContain(map.getBlock(regionX, regionY, x + 1, y));
        expect(map.improvementNeighbors[playerProvider.getHuman().playerId]).toContain(map.getBlock(regionX, regionY, x, y + 1));
        expect(map.improvementNeighbors[playerProvider.getHuman().playerId]).toContain(map.getBlock(regionX, regionY, x - 1, y));
        expect(map.improvementNeighbors[playerProvider.getHuman().playerId]).toContain(map.getBlock(regionX, regionY, x, y - 1));
      });

      it('adds only the closest block neighborhood to improvementNeighbors', function () {
        var regionX = 1;
        var regionY = -1;
        var x = 3;
        var y = 1;
        var block = map.getBlock(regionX, regionY, x, y);
        map.neighborImproved(block, playerProvider.getHuman());
        expect(map.improvementNeighbors[playerProvider.getHuman().playerId]).not.toContain(map.getBlock(regionX, regionY, x + 1, y - 1));
        expect(map.improvementNeighbors[playerProvider.getHuman().playerId]).not.toContain(map.getBlock(regionX, regionY, x + 1, y + 1));
        expect(map.improvementNeighbors[playerProvider.getHuman().playerId]).not.toContain(map.getBlock(regionX, regionY, x - 1, y + 1));
        expect(map.improvementNeighbors[playerProvider.getHuman().playerId]).not.toContain(map.getBlock(regionX, regionY, x - 1, y - 1));
      });

      it('must not add passed block to improvementNeighbors', function () {
        var regionX = 1;
        var regionY = -1;
        var x = 0;
        var y = 1;
        var block = map.getBlock(regionX, regionY, x, y);
        map.neighborImproved(block, playerProvider.getHuman());
        expect(map.improvementNeighbors[playerProvider.getHuman().playerId]).not.toContain(block);
      });

      it('must remove passed block from improvementNeighbors if already contains', function () {
        var regionX = 1;
        var regionY = -1;
        var x = 0;
        var y = 1;
        var block = map.getBlock(regionX, regionY, x, y);
        var upBlock = map.getBlock(regionX, regionY, x, y - 1);
        map.neighborImproved(upBlock, playerProvider.getHuman());
        expect(map.improvementNeighbors[playerProvider.getHuman().playerId]).toContain(block);
        map.neighborImproved(block, playerProvider.getHuman());
        expect(map.improvementNeighbors[playerProvider.getHuman().playerId]).not.toContain(block);
      });

      it('must not add block to improvementNeighbors twice', function () {
        var regionX = 0;
        var regionY = 0;
        var x = 10;
        var y = 1;
        var block = map.getBlock(regionX, regionY, x, y);
        var anotherBlock = map.getBlock(regionX, regionY, x + 2, y);
        var commonNeighbor = map.getBlock(regionX, regionY, x + 1, y);
        map.neighborImproved(block, playerProvider.getHuman());
        expect(map.improvementNeighbors[playerProvider.getHuman().playerId]).toContain(commonNeighbor);
        map.neighborImproved(anotherBlock, playerProvider.getHuman());
        expect(map.improvementNeighbors[playerProvider.getHuman().playerId]).toContain(commonNeighbor);
        expect(map.improvementNeighbors[playerProvider.getHuman().playerId]
          .filter(function (block_) { return block_=== commonNeighbor; }).length)
          .toBe(1);
      });

      it('must not add already improved blocks to improvementNeighbors', function () {
        var regionX = 0;
        var regionY = 0;
        var x = 7;
        var y = 1;
        var block = map.getBlock(regionX, regionY, x, y);
        var anotherBlock = map.getBlock(regionX, regionY, x + 1, y);
        block.level = 1;
        map.neighborImproved(block, playerProvider.getHuman());
        expect(map.improvementNeighbors[playerProvider.getHuman().playerId]).toContain(anotherBlock);
        anotherBlock.level = 1;
        map.neighborImproved(anotherBlock, playerProvider.getHuman());
        expect(map.improvementNeighbors[playerProvider.getHuman().playerId]).not.toContain(block);
      });

      it('adds only existing blocks on a fixed size map', function () {
        var regionX = 2;
        var regionY = -2;
        var x = 4;
        var y = 0;
        map.fixedSize = true;
        var block = map.getBlock(regionX, regionY, x, y);
        block.level = 1;
        map.neighborImproved(block, playerProvider.getHuman());
        expect(map.improvementNeighbors[playerProvider.getHuman().playerId].length).toBe(2);
        expect(map.improvementNeighbors[playerProvider.getHuman().playerId]).toContain(map.getBlock(regionX, regionY, x, y + 1));
        expect(map.improvementNeighbors[playerProvider.getHuman().playerId]).toContain(map.getBlock(regionX, regionY, x - 1, y));
      });

    });

    describe('updateNeighborBlocksAffordability', function () {

      it('updates affordableBlockCount properly', function () {
        var regionX = 0;
        var regionY = 0;
        var x = 2;
        var y = 1;
        var player = playerProvider.getHuman();
        var storage = storageProvider.getStorage(player);
        storage.has = function () { return true; };
        var block1 = map.getBlock(regionX, regionY, x, y);
        var block2 = map.getBlock(regionX, regionY, x + 1, y);
        block1.exhausted = block2.exhausted = false;
        block1.kind.improvable = block2.kind.improvable = true;
        map.improvementNeighbors = {};
        map.improvementNeighbors[player.playerId] = [block1, block2];
        map.updateNeighborBlocksAffordability(player);
        expect(map.affordableBlockCount[player.playerId]).toBe(2);
      });

      it('updates affordableBlockCount for a proper player', function () {
        var regionX = 0;
        var regionY = 0;
        var x = 2;
        var y = 1;
        var player = playerProvider.getHuman();
        var anotherPlayer = playerProvider.getFirstAI();
        var storage = storageProvider.getStorage(player);
        storage.has = function () { return true; };
        var anotherPlayerStorage = storageProvider.getStorage(anotherPlayer);
        anotherPlayerStorage.has = function () { return true; };
        var block1 = map.getBlock(regionX, regionY, x, y);
        var block2 = map.getBlock(regionX, regionY, x + 1, y);
        block1.exhausted = block2.exhausted = false;
        block1.kind.improvable = block2.kind.improvable = true;
        map.improvementNeighbors = {};
        map.improvementNeighbors[player.playerId] = [block1, block2];
        map.improvementNeighbors[anotherPlayer.playerId] = [block2];
        map.updateNeighborBlocksAffordability(anotherPlayer);
        expect(map.affordableBlockCount[player.playerId]).toBe(0);
        expect(map.affordableBlockCount[anotherPlayer.playerId]).toBe(1);
        map.updateNeighborBlocksAffordability(player);
        expect(map.affordableBlockCount[player.playerId]).toBe(2);
      });

    });

    describe('captureBlock', function () {

      function changeAndCaptureBlock(regionX, regionY) {
        var player = playerProvider.getHuman();
        var block = map.getBlock(regionX, regionY, 1, 2);
        map.changeBlockKind(block, new Plain(), false);
        map.captureBlock(block, player);
      }

      describe('adds new regions upon capturing the first block in a border region', function () {

        var oldBorderRegionsCoords;

        function checkIsValidRegion(region) {
          expect(region instanceof Region).toBeTrue();
          expect(region.blocks.length).toBe(Region.blocksHeight);
        }

        beforeEach(function () {
          oldBorderRegionsCoords = {
            top: map.borderRegionsCoords.top,
            right: map.borderRegionsCoords.right,
            bottom: map.borderRegionsCoords.bottom,
            left: map.borderRegionsCoords.left
          };
        });

        it('for the top most region', function () {
          changeAndCaptureBlock(0, map.borderRegionsCoords.top);
          expect(map.borderRegionsCoords.top).toBe(oldBorderRegionsCoords.top - 1);
          expect(map.borderRegionsCoords.right).toBe(oldBorderRegionsCoords.right);
          expect(map.borderRegionsCoords.bottom).toBe(oldBorderRegionsCoords.bottom);
          expect(map.borderRegionsCoords.left).toBe(oldBorderRegionsCoords.left);
          [map.getRegionByXY(-1, map.borderRegionsCoords.top),
           map.getRegionByXY(0, map.borderRegionsCoords.top),
           map.getRegionByXY(1, map.borderRegionsCoords.top)]
            .forEach(checkIsValidRegion);
        });

        it('for the rightTop most region', function () {
          changeAndCaptureBlock(map.borderRegionsCoords.right, map.borderRegionsCoords.top);
          expect(map.borderRegionsCoords.top).toBe(oldBorderRegionsCoords.top - 1);
          expect(map.borderRegionsCoords.right).toBe(oldBorderRegionsCoords.right + 1);
          expect(map.borderRegionsCoords.bottom).toBe(oldBorderRegionsCoords.bottom);
          expect(map.borderRegionsCoords.left).toBe(oldBorderRegionsCoords.left);
          [map.getRegionByXY(map.borderRegionsCoords.right - 2, map.borderRegionsCoords.top),
           map.getRegionByXY(map.borderRegionsCoords.right - 1, map.borderRegionsCoords.top),
           map.getRegionByXY(map.borderRegionsCoords.right, map.borderRegionsCoords.top),
           map.getRegionByXY(map.borderRegionsCoords.right, map.borderRegionsCoords.top + 1),
           map.getRegionByXY(map.borderRegionsCoords.right, map.borderRegionsCoords.top + 2)]
            .forEach(checkIsValidRegion);
        });

        it('for the right most region', function () {
          changeAndCaptureBlock(map.borderRegionsCoords.right, 0);
          expect(map.borderRegionsCoords.top).toBe(oldBorderRegionsCoords.top);
          expect(map.borderRegionsCoords.right).toBe(oldBorderRegionsCoords.right + 1);
          expect(map.borderRegionsCoords.bottom).toBe(oldBorderRegionsCoords.bottom);
          expect(map.borderRegionsCoords.left).toBe(oldBorderRegionsCoords.left);
          [map.getRegionByXY(map.borderRegionsCoords.right, -1),
           map.getRegionByXY(map.borderRegionsCoords.right, 0),
           map.getRegionByXY(map.borderRegionsCoords.right, 1)]
            .forEach(checkIsValidRegion);
        });

        it('for the rightBottom most region', function () {
          changeAndCaptureBlock(map.borderRegionsCoords.right, map.borderRegionsCoords.bottom);
          expect(map.borderRegionsCoords.top).toBe(oldBorderRegionsCoords.top);
          expect(map.borderRegionsCoords.right).toBe(oldBorderRegionsCoords.right + 1);
          expect(map.borderRegionsCoords.bottom).toBe(oldBorderRegionsCoords.bottom + 1);
          expect(map.borderRegionsCoords.left).toBe(oldBorderRegionsCoords.left);
          [map.getRegionByXY(map.borderRegionsCoords.right - 2, map.borderRegionsCoords.bottom),
           map.getRegionByXY(map.borderRegionsCoords.right - 1, map.borderRegionsCoords.bottom),
           map.getRegionByXY(map.borderRegionsCoords.right, map.borderRegionsCoords.bottom),
           map.getRegionByXY(map.borderRegionsCoords.right, map.borderRegionsCoords.bottom - 1),
           map.getRegionByXY(map.borderRegionsCoords.right, map.borderRegionsCoords.bottom - 2)]
            .forEach(checkIsValidRegion);
        });

        it('for the bottom most region', function () {
          changeAndCaptureBlock(0, map.borderRegionsCoords.bottom);
          expect(map.borderRegionsCoords.top).toBe(oldBorderRegionsCoords.top);
          expect(map.borderRegionsCoords.right).toBe(oldBorderRegionsCoords.right);
          expect(map.borderRegionsCoords.bottom).toBe(oldBorderRegionsCoords.bottom + 1);
          expect(map.borderRegionsCoords.left).toBe(oldBorderRegionsCoords.left);
          [map.getRegionByXY(-1, map.borderRegionsCoords.bottom),
           map.getRegionByXY(0, map.borderRegionsCoords.bottom),
           map.getRegionByXY(1, map.borderRegionsCoords.bottom)]
            .forEach(checkIsValidRegion);
        });

        it('for the leftBottom most region', function () {
          changeAndCaptureBlock(map.borderRegionsCoords.left, map.borderRegionsCoords.bottom);
          expect(map.borderRegionsCoords.top).toBe(oldBorderRegionsCoords.top);
          expect(map.borderRegionsCoords.right).toBe(oldBorderRegionsCoords.right);
          expect(map.borderRegionsCoords.bottom).toBe(oldBorderRegionsCoords.bottom + 1);
          expect(map.borderRegionsCoords.left).toBe(oldBorderRegionsCoords.left - 1);
          [map.getRegionByXY(map.borderRegionsCoords.left + 2, map.borderRegionsCoords.bottom),
           map.getRegionByXY(map.borderRegionsCoords.left + 1, map.borderRegionsCoords.bottom),
           map.getRegionByXY(map.borderRegionsCoords.left, map.borderRegionsCoords.bottom),
           map.getRegionByXY(map.borderRegionsCoords.left, map.borderRegionsCoords.bottom - 1),
           map.getRegionByXY(map.borderRegionsCoords.left, map.borderRegionsCoords.bottom - 2)]
            .forEach(checkIsValidRegion);
        });

        it('for the left most region', function () {
          changeAndCaptureBlock(map.borderRegionsCoords.left, 0);
          expect(map.borderRegionsCoords.top).toBe(oldBorderRegionsCoords.top);
          expect(map.borderRegionsCoords.right).toBe(oldBorderRegionsCoords.right);
          expect(map.borderRegionsCoords.bottom).toBe(oldBorderRegionsCoords.bottom);
          expect(map.borderRegionsCoords.left).toBe(oldBorderRegionsCoords.left - 1);
          [map.getRegionByXY(map.borderRegionsCoords.left, -1),
           map.getRegionByXY(map.borderRegionsCoords.left, 0),
           map.getRegionByXY(map.borderRegionsCoords.left, 1)]
            .forEach(checkIsValidRegion);
        });

        it('for the leftTop most region', function () {
          changeAndCaptureBlock(map.borderRegionsCoords.left, map.borderRegionsCoords.top);
          expect(map.borderRegionsCoords.top).toBe(oldBorderRegionsCoords.top - 1);
          expect(map.borderRegionsCoords.right).toBe(oldBorderRegionsCoords.right);
          expect(map.borderRegionsCoords.bottom).toBe(oldBorderRegionsCoords.bottom);
          expect(map.borderRegionsCoords.left).toBe(oldBorderRegionsCoords.left - 1);
          [map.getRegionByXY(map.borderRegionsCoords.left + 2, map.borderRegionsCoords.top),
           map.getRegionByXY(map.borderRegionsCoords.left + 1, map.borderRegionsCoords.top),
           map.getRegionByXY(map.borderRegionsCoords.left, map.borderRegionsCoords.top),
           map.getRegionByXY(map.borderRegionsCoords.left, map.borderRegionsCoords.top + 1),
           map.getRegionByXY(map.borderRegionsCoords.left, map.borderRegionsCoords.top + 2)]
            .forEach(checkIsValidRegion);
        });

      });

      describe('sets newly added regions\' blocks\' neighbors properly', function () {

        function checkBlocksNeighbors(region, minNeighborsByPositionLen) {
          region.blocks.forEach(function (row) {
            row.forEach(function (block) {
              expect(Object.keys(block.neighborsByKind).length)
                .toBeGreaterThanOrEqualTo(1);
              expect(Object.keys(block.neighborsByPosition).length)
                .toBeGreaterThanOrEqualTo(minNeighborsByPositionLen);
            });
          });
        }

        it('for the leftTop most region', function () {
          changeAndCaptureBlock(map.borderRegionsCoords.left, map.borderRegionsCoords.top);
          [map.getRegionByXY(map.borderRegionsCoords.left + 2, map.borderRegionsCoords.top),
           map.getRegionByXY(map.borderRegionsCoords.left + 1, map.borderRegionsCoords.top),
           map.getRegionByXY(map.borderRegionsCoords.left, map.borderRegionsCoords.top),
           map.getRegionByXY(map.borderRegionsCoords.left, map.borderRegionsCoords.top + 1),
           map.getRegionByXY(map.borderRegionsCoords.left, map.borderRegionsCoords.top + 2)]
            .forEach(function (region) {
              checkBlocksNeighbors(region, 2);
            });
          [map.getRegionByXY(map.borderRegionsCoords.left + 1, map.borderRegionsCoords.top + 1),
           map.getRegionByXY(map.borderRegionsCoords.left + 1, map.borderRegionsCoords.top + 2),
           map.getRegionByXY(map.borderRegionsCoords.left + 2, map.borderRegionsCoords.top + 1)]
            .forEach(function (region) {
              checkBlocksNeighbors(region, 4);
            });
        });

        it('for the right most region', function () {
          changeAndCaptureBlock(map.borderRegionsCoords.right, 0);
          [map.getRegionByXY(map.borderRegionsCoords.right, -1),
           map.getRegionByXY(map.borderRegionsCoords.right, 0),
           map.getRegionByXY(map.borderRegionsCoords.right, 1)]
            .forEach(function (region) {
              checkBlocksNeighbors(region, 2);
            });
          [map.getRegionByXY(map.borderRegionsCoords.right - 1, -1),
           map.getRegionByXY(map.borderRegionsCoords.right - 1, 0),
           map.getRegionByXY(map.borderRegionsCoords.right - 1, 1)]
            .forEach(function (region) {
              checkBlocksNeighbors(region, 4);
            });
        });

        it('for the bottom most region', function () {
          changeAndCaptureBlock(0, map.borderRegionsCoords.bottom);
          [map.getRegionByXY(-1, map.borderRegionsCoords.bottom),
           map.getRegionByXY(0, map.borderRegionsCoords.bottom),
           map.getRegionByXY(1, map.borderRegionsCoords.bottom)]
            .forEach(function (region) {
              checkBlocksNeighbors(region, 2);
            });
          [map.getRegionByXY(-1, map.borderRegionsCoords.bottom - 1),
           map.getRegionByXY(0, map.borderRegionsCoords.bottom - 1),
           map.getRegionByXY(1, map.borderRegionsCoords.bottom - 1)]
            .forEach(function (region) {
              checkBlocksNeighbors(region, 4);
            });
        });

      });

      describe('sets newly added regions\' AiValues properly', function () {

        beforeEach(function () {
          Block.prototype.baseAiValue = null;
        });

        function checkAiValues(region) {
          region.blocks.forEach(function (row) {
            row.forEach(function (block) {
              expect(block.baseAiValue).not.toBeNull();
              expect(block.baseAiValue).toBeWholeNumber();
              expect(block.aiValueForPlayer).toBeObject();
              expect(block.aiValueForPlayer['player-AI-0']).toBeWholeNumber();
              expect(block.aiValueForPlayer['player-AI-0']).toBeWholeNumber();
              expect(block.aiValuesForPlayerMissions['player-AI-0']).toBeObject();
            });
          });
        }

        it('for the rightBottom most region', function () {
          changeAndCaptureBlock(map.borderRegionsCoords.right, map.borderRegionsCoords.bottom);
          [map.getRegionByXY(map.borderRegionsCoords.right, map.borderRegionsCoords.bottom - 1),
           map.getRegionByXY(map.borderRegionsCoords.right, map.borderRegionsCoords.bottom),
           map.getRegionByXY(map.borderRegionsCoords.right - 1, map.borderRegionsCoords.bottom)]
            .forEach(checkAiValues);
        });

        it('for the left most region', function () {
          changeAndCaptureBlock(map.borderRegionsCoords.right, 0);
          [map.getRegionByXY(map.borderRegionsCoords.left, -1),
           map.getRegionByXY(map.borderRegionsCoords.left, 0),
           map.getRegionByXY(map.borderRegionsCoords.left, 1)]
            .forEach(checkAiValues);
        });

        it('for the top most region', function () {
          changeAndCaptureBlock(0, map.borderRegionsCoords.top);
          [map.getRegionByXY(-1, map.borderRegionsCoords.top),
           map.getRegionByXY(0, map.borderRegionsCoords.top),
           map.getRegionByXY(1, map.borderRegionsCoords.top)]
            .forEach(checkAiValues);
        });

      });

    });

    describe('initAllBlocksNeighborsAndTmpAiValues', function () {

      it('gives the same result called many times', function () {
        var allBlocks = map.getBlocks(-2, -2, 0, 0, 25, 25);
        expect(allBlocks.length).toBe(25 * 25);
        var updatedBaseAiValues = getBlocksBaseAiValues(allBlocks);
        var updatedTmpAiValues = getBlocksTmpAiValues(allBlocks);
        map.initAllBlocksNeighborsAndTmpAiValues();
        map.initAllBlocksBaseAiValues();
        map.initAllBlocksNeighborsAndTmpAiValues();
        map.initAllBlocksBaseAiValues();
        map.initAllBlocksNeighborsAndTmpAiValues();
        map.initAllBlocksBaseAiValues();
        map.initAllBlocksNeighborsAndTmpAiValues();
        map.initAllBlocksBaseAiValues();
        expect(updatedBaseAiValues).toEqual(getBlocksBaseAiValues(allBlocks));
        expect(updatedTmpAiValues).toEqual(getBlocksTmpAiValues(allBlocks));
      });

      describe('sets baseAiValue', function () {

        var allBlocks;

        beforeEach(function () {
          allBlocks = map.getBlocks(-2, -2, 0, 0, 25, 25);
          expect(allBlocks.length).toBe(25 * 25);
        });

        it('between 24 and 512 for map of mountain blocks', function () {
          allBlocks.forEach(function (block) {
            block.kind = new Mountain();
            block.rich = true;
          });
          map.initAllBlocksNeighborsAndTmpAiValues();
          map.initAllBlocksBaseAiValues();
          expect(allBlocks.reduce(function (maxValue, block) {
            maxValue = maxValue > block.baseAiValue ? maxValue : block.baseAiValue;
            return maxValue;
          }, 0)).toBeWithinRange(24, 512);
        });

        it('between 48 and 512 for map of wild horses blocks', function () {
          allBlocks.forEach(function (block) {
            block.kind = new WildHorses();
            block.rich = false;
          });
          map.initAllBlocksNeighborsAndTmpAiValues();
          map.initAllBlocksBaseAiValues();
          expect(allBlocks.reduce(function (maxValue, block) {
            maxValue = maxValue > block.baseAiValue ? maxValue : block.baseAiValue;
            return maxValue;
          }, 0)).toBeWithinRange(48, 512);
        });

      });

    });

    describe('getBlockAdjacentNeighbors', function () {

      it('returns proper blocks', function () {
        var blocks = map.getBlockAdjacentNeighbors(map.getBlock(0, 1, 2, 1));
        expect(blocks).toBeArray();
        expect(blocks.length).toBe(4);
        expect(blocks[0]).toBe(map.getBlock(0, 1, 2, 0));
        expect(blocks[1]).toBe(map.getBlock(0, 1, 3, 1));
        expect(blocks[2]).toBe(map.getBlock(0, 1, 2, 2));
        expect(blocks[3]).toBe(map.getBlock(0, 1, 1, 1));
      });

      it('returns proper blocks for a border block on fixed size map', function () {
        var blocks = map.getBlockAdjacentNeighbors(map.getBlock(2, 2, 4, 4));
        expect(blocks).toBeArray();
        expect(blocks.length).toBe(2);
        expect(blocks[0]).toBe(map.getBlock(2, 2, 4, 3));
        expect(blocks[1]).toBe(map.getBlock(2, 2, 3, 4));
      });

    });

    describe('setNeighborhoodImprovedNeighbor', function () {

      it('sets proper improvedNeighbors on proper blocks', function () {
        var block = map.getBlock(0, 0, 1, 2);
        expect(map.getBlock(0, 0, 2, 2).improvedNeighbors.left).toBeUndefined();
        expect(map.getBlock(0, 0, 2, 2).improvedNeighbors.left).toBeUndefined();
        expect(map.getBlock(0, 0, 0, 2).improvedNeighbors.right).toBeUndefined();
        expect(map.getBlock(0, 0, 1, 1).improvedNeighbors.down).toBeUndefined();
        expect(map.getBlock(0, 0, 1, 3).improvedNeighbors.up).toBeUndefined();
        expect(map.getBlock(0, 0, 2, 3).improvedNeighbors.leftUp).toBeUndefined();
        expect(map.getBlock(0, 0, 0, 1).improvedNeighbors.rightDown).toBeUndefined();
        expect(map.getBlock(0, 0, 2, 1).improvedNeighbors.leftDown).toBeUndefined();
        expect(map.getBlock(0, 0, 0, 3).improvedNeighbors.rightUp).toBeUndefined();
        map.setNeighborhoodImprovedNeighbor(block);
        expect(map.getBlock(0, 0, 2, 2).improvedNeighbors.left).toBe(block);
        expect(map.getBlock(0, 0, 0, 2).improvedNeighbors.right).toBe(block);
        expect(map.getBlock(0, 0, 1, 1).improvedNeighbors.down).toBe(block);
        expect(map.getBlock(0, 0, 1, 3).improvedNeighbors.up).toBe(block);
        expect(map.getBlock(0, 0, 2, 3).improvedNeighbors.leftUp).toBe(block);
        expect(map.getBlock(0, 0, 0, 1).improvedNeighbors.rightDown).toBe(block);
        expect(map.getBlock(0, 0, 2, 1).improvedNeighbors.leftDown).toBe(block);
        expect(map.getBlock(0, 0, 0, 3).improvedNeighbors.rightUp).toBe(block);
      });

    });

    describe('setBlockAiValueWithSideEffects', function () {

      it('does not change surroundings tmpAiValue or baseAiValue if block kind baseAiValue has not been changed', function () {
        var regionX = 2;
        var regionY = -1;
        var x = 2;
        var y = 1;
        var block1 = map.getBlock(regionX, regionY, x, y);
        var block2 = map.getBlock(regionX, regionY, x + 1, y);
        var block3 = map.getBlock(regionX, regionY, x, y - 1);
        var block4 = map.getBlock(regionX, regionY, x - 1, y + 2);
        var block5 = map.getBlock(regionX, regionY, x - 2, y - 2);
        var initTmpAiValues = getBlocksTmpAiValues([block1, block2, block3, block4, block5]);
        var initBaseAiValues = getBlocksBaseAiValues([block1, block2, block3, block4, block5]);
        var block1BaseAiValue = block1.kind.baseAiValue;
        map.setBlockAiValueWithSideEffects(block1, block1BaseAiValue);
        expect(getBlocksBaseAiValues([block1, block2, block3, block4, block5])).toEqual(initBaseAiValues);
        expect(getBlocksTmpAiValues([block1, block2, block3, block4, block5])).toEqual(initTmpAiValues);
      });

      it('changes surroundings tmpAiValue or baseAiValue ' +
         'if block kind baseAiValue has been changed', function () {
        var regionX = 1;
        var regionY = 2;
        var x = 4;
        var y = 4;
        var block1 = map.getBlock(regionX, regionY, x, y);
        var surroundingBlocks = map.getBlocks(regionX, regionY, x - 2, y - 2, 5, 5);
        expect(surroundingBlocks.length).toBe(15);
        var initTmpAiValues = getBlocksTmpAiValues(surroundingBlocks);
        var initBaseAiValues = getBlocksBaseAiValues(surroundingBlocks);
        map.setBlockAiValueWithSideEffects(block1, block1.kind.baseAiValue + 8);
        expect(getBlocksBaseAiValues(surroundingBlocks)).not.toEqual(initBaseAiValues);
        expect(getBlocksTmpAiValues(surroundingBlocks)).not.toEqual(initTmpAiValues);
      });

      it('works the same way as initAllBlocksNeighborsAndTmpAiValues does', function () {
        var regionX = -2;
        var regionY = 2;
        var x = 1;
        var y = 4;
        var block1 = map.getBlock(regionX, regionY, x, y);
        var surroundingBlocks = map.getBlocks(regionX, regionY, x - 2, y - 2, 5, 5);
        expect(surroundingBlocks.length).toBe(12);
        map.setBlockAiValueWithSideEffects(block1, block1.kind.baseAiValue + 8);
        var updatedBaseAiValues = getBlocksBaseAiValues(surroundingBlocks);
        var updatedTmpAiValues = getBlocksTmpAiValues(surroundingBlocks);
        map.initAllBlocksNeighborsAndTmpAiValues();
        map.initAllBlocksBaseAiValues();
        expect(getBlocksBaseAiValues(surroundingBlocks)).toEqual(updatedBaseAiValues);
        expect(getBlocksTmpAiValues(surroundingBlocks)).toEqual(updatedTmpAiValues);
      });

      it('gives the same result called multiple times', function () {
        var regionX = 0;
        var regionY = 1;
        var x = 0;
        var y = 0;
        var block1 = map.getBlock(regionX, regionY, x, y);
        var surroundingBlocks = map.getBlocks(regionX, regionY, x - 2, y - 2, 5, 5);
        expect(surroundingBlocks.length).toBe(25);
        map.setBlockAiValueWithSideEffects(block1, 10);
        var updatedBaseAiValues = getBlocksBaseAiValues(surroundingBlocks);
        var updatedTmpAiValues = getBlocksTmpAiValues(surroundingBlocks);
        map.setBlockAiValueWithSideEffects(block1, 10);
        map.setBlockAiValueWithSideEffects(block1, 10);
        map.setBlockAiValueWithSideEffects(block1, 10);
        expect(getBlocksBaseAiValues(surroundingBlocks)).toEqual(updatedBaseAiValues);
        expect(getBlocksTmpAiValues(surroundingBlocks)).toEqual(updatedTmpAiValues);
      });

    });

    describe('resetNeighborsByKindForAllBlocks', function () {

      it('sets empty neighborsByKind for all blocks in the map', function () {
        var checkCounter = 0;
        map.resetNeighborsByKindForAllBlocks();
        for (var regionY in map.regions) {
          for (var regionX in map.regions[regionY]) {
            map.regions[regionY][regionX].blocks.forEach(function (row) {
              row.forEach(function (block) {
                expect(block.neighborsByKind).toEqual({
                  plain: [],
                  forest: [],
                  hill: [],
                  mountain: [],
                  coalfield: [],
                  wildHorses: [],
                  goldDeposit: [],
                  silverDeposit: [],
                  gemstoneDeposit: []
                });
                checkCounter++;
              });
            });
          }
        }
        expect(checkCounter).toBe(5 * 5 * 5 * 5);
      });

    });

    describe('updateOwnedBlockProduction', function () {

      var storage;

      beforeEach(function () {
        var player = playerProvider.getHuman();
        map.changeBlockKind(map.getBlock(0, 0, 0, 0), new Plain(), false);  // food: 1
        map.changeBlockKind(map.getBlock(0, 0, 1, 0), new Mountain(), false);  // metal: 1
        map.changeBlockKind(map.getBlock(0, 0, 0, 1), new Hill(), true);  // stone: 2
        map.changeBlockKind(map.getBlock(0, 0, 1, 1), new Forest(), true);  // stone: 1, wood: 4
        map.changeBlockKind(map.getBlock(0, 0, 2, 1), new Plain(), true);  // stone: 1, wood: 3, food: 2
        map.changeBlockKind(map.getBlock(0, 0, 2, 0), new GoldDeposit(), false);  // stone: 1, wood: 3, food: 1
        map.captureBlock(map.getBlock(0, 0, 0, 0), player);
        map.captureBlock(map.getBlock(0, 0, 1, 0), player);
        map.captureBlock(map.getBlock(0, 0, 0, 1), player);
        map.captureBlock(map.getBlock(0, 0, 1, 1), player);
        map.captureBlock(map.getBlock(0, 0, 2, 1), player);
        map.captureBlock(map.getBlock(0, 0, 2, 0), player);
        storage = storageProvider.getStorage(player);
        expect(storage.resources).toEqual(jasmine.objectContaining({
          food: 1,
          wood: 3,
          stone: 1,
          metal: 0
        }));
        expect(storage.goldIncome).toBe(13);
      });

      describe('takes previous resource production from the storage', function () {

        it('for an old rich and new non-rich block', function () {
          var hillBlock = map.getBlock(0, 0, 0, 1);
          map.updateOwnedBlockProduction(
            hillBlock, hillBlock.kind, hillBlock.rich, new GoldDeposit(), false);
          expect(storage.resources).toEqual(jasmine.objectContaining({
            stone: -1
          }));
        });

        it('for an old non-rich and new rich block', function () {
          var plainBlock = map.getBlock(0, 0, 0, 0);
          map.updateOwnedBlockProduction(
            plainBlock, plainBlock.kind, plainBlock.rich, new Hill(), true);
          expect(storage.resources).toEqual(jasmine.objectContaining({
            food: 0
          }));
        });

        it('for an old level-2 rich block', function () {
          var forestBlock = map.getBlock(0, 0, 1, 1);
          map.updateOwnedBlockProduction(
            forestBlock, forestBlock.kind, forestBlock.rich, new Hill(), false);
          expect(storage.resources).toEqual(jasmine.objectContaining({
            wood: -1
          }));
        });

      });

      it('takes previous gold income from the storage', function () {
        var goldBlock = map.getBlock(0, 0, 2, 0);
        map.updateOwnedBlockProduction(
          goldBlock, goldBlock.kind, goldBlock.rich, new Forest(), true);
        expect(storage.goldIncome).toBe(1);
      });

      it('puts new resource production to the storage', function () {
        var forestBlock = map.getBlock(0, 0, 1, 1);
        map.updateOwnedBlockProduction(
          forestBlock, forestBlock.kind, forestBlock.rich, new Mountain(), false);
        expect(storage.resources).toEqual(jasmine.objectContaining({
          metal: 1
        }));
      });

      it('puts new gold income to the storage', function () {
        var mountainBlock = map.getBlock(0, 0, 1, 0);
        map.updateOwnedBlockProduction(
          mountainBlock, mountainBlock.kind, mountainBlock.rich, new GoldDeposit(), false);
        expect(storage.goldIncome).toBe(25);
      });

      it('takes new resource cost from the storage', function () {
        var forestBlock = map.getBlock(0, 0, 1, 1);
        map.updateOwnedBlockProduction(
          forestBlock, forestBlock.kind, forestBlock.rich, new Mountain(), false);
        expect(storage.resources).toEqual(jasmine.objectContaining({
          food: 0
        }));
      });

      it('puts previous resource cost to the storage', function () {
        var plainBlock = map.getBlock(0, 0, 0, 0);
        map.updateOwnedBlockProduction(
          plainBlock, plainBlock.kind, plainBlock.rich, new Hill(), false);
        expect(storage.resources).toEqual(jasmine.objectContaining({
          wood: 4
        }));
      });

      it('calc a new storage in the right way', function () {
        var hillBlock = map.getBlock(0, 0, 0, 1);
        map.updateOwnedBlockProduction(
          hillBlock, hillBlock.kind, hillBlock.rich, new Forest(), false);
        expect(storage.resources).toEqual(jasmine.objectContaining({
          food: 1,
          wood: 4,
          stone: -2,
          metal: 1
        }));
        var mountainBlock = map.getBlock(0, 0, 1, 0);
        map.updateOwnedBlockProduction(
          mountainBlock, mountainBlock.kind, mountainBlock.rich, new Plain(), true);
        expect(storage.resources).toEqual(jasmine.objectContaining({
          food: 4,
          wood: 3,
          stone: -2,
          metal: 0
        }));
        var goldBlock = map.getBlock(0, 0, 2, 0);
        map.updateOwnedBlockProduction(
          goldBlock, goldBlock.kind, goldBlock.rich, new Coalfield(), true);
        expect(storage.resources).toEqual(jasmine.objectContaining({
          coal: 4
        }));
        expect(storage.goldIncome).toBe(1);
      });

    });

    describe('mutateBlockKind', function () {

      function prepareTest() {
        let block, oldKind, oldRich, mutatedKind, mutatedRich;
        let result = false;
        let i = -1;
        while (result !== true && i < 45) {
          i = i + 1;
          block = map.getBlock(0, 0, i % 15, Math.floor(i / 5));
          oldKind = block.kind;
          oldRich = block.rich;
          result = map.mutateBlockKind(block) &&
            block.kind.toString() !== 'water';
        }
        expect(result).toBeTrue();
        expect(block).toBeObject();
        mutatedKind = block.kind;
        mutatedRich = block.rich;
        map.changeBlockKind(block, oldKind, oldRich);
        block.mutated = false;
        return { block, mutatedKind, mutatedRich };
      }

      it('changes the given block kind twice into the same kind ' +
         'for the same gameSeed', function () {
        const { block, mutatedKind, mutatedRich } = prepareTest();
        map.setRandomness(map.gameSeed, map.mapSeed);
        const result = map.mutateBlockKind(block);
        expect(result).toBeTrue();
        expect(block.kind.toString()).toBe(mutatedKind.toString());
        expect(block.rich).toBe(mutatedRich);
      });

      it('changes the given block kind into a different kind ' +
         'for the different gameSeed', function () {
        const originalMutations = [];
        const newMutations = [];
        for (let i = 0; i < 20; ++i) {
          const { block, mutatedKind, mutatedRich } = prepareTest();
          map.setRandomness(Rand.getUnpredictableInt31Unsigned(), map.mapSeed);
          const result = map.mutateBlockKind(block);
          expect(result).toBeTrue();
          originalMutations.push(mutatedKind.toString());
          originalMutations.push(mutatedRich);
          newMutations.push(block.kind.toString());
          newMutations.push(block.rich);
        }
        expect(originalMutations).not.toEqual(newMutations);
      });

    });

  });

});