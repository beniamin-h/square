'use strict';

describe('Map:raw_instance', function() {
  beforeEach(module('square'));

  let Map, Region, storageProvider, playerProvider, Rand;

  beforeEach(inject(function (
      _Map_, _Region_, _storageProvider_, _playerProvider_, _Rand_) {
    Map = _Map_;
    Region = _Region_;
    storageProvider = _storageProvider_;
    playerProvider = _playerProvider_;
    Rand = _Rand_;
  }));

  describe('a new raw instance', function () {

    let map;

    beforeEach(function () {
      map = new Map();
      map.setRandomness('fake-game-seed', Rand.getUnpredictableInt31Unsigned());
    });

    describe('constructor', function () {

      it('does not initializes regions', function () {
        expect(map.regions).toBeEmptyObject();
      });

    });

    describe('initRegions', function () {

      it('sets up proper borderRegionsCoords', function () {
        map.initSizeRegions = 15;
        map.initRegions({ populateWithBlocks: false });
        expect(map.borderRegionsCoords.top).toBe(-7);
        expect(map.borderRegionsCoords.right).toBe(7);
        expect(map.borderRegionsCoords.bottom).toBe(7);
        expect(map.borderRegionsCoords.left).toBe(-7);
      });


      it('sets up proper borderRegionsCoords for the given mapSize', function () {
        map.initSizeRegions = 15;
        map.initRegions({ populateWithBlocks: false, mapSize: 5 });
        expect(map.borderRegionsCoords.top).toBe(-2);
        expect(map.borderRegionsCoords.right).toBe(2);
        expect(map.borderRegionsCoords.bottom).toBe(2);
        expect(map.borderRegionsCoords.left).toBe(-2);
      });

      it('initializes regions with blocks', function () {
        map.initRegions();
        expect(typeof map.regions).toBe('object');
        expect(Object.keys(map.regions).length).toBeGreaterThan(0);
        expect(Object.keys(map.regions[0]).length).toBeGreaterThan(0);
        expect(map.regions[0][0] instanceof Region).toBeTrue(0);
        expect(Object.keys(map.regions[0][0].blocks).length).toBeGreaterThan(0);
      });

      it('initializes regions without blocks if populateWithBlocks=false passed to its parameters', function () {
        map.initRegions({ populateWithBlocks: false });
        expect(typeof map.regions).toBe('object');
        expect(Object.keys(map.regions).length).toBeGreaterThan(0);
        expect(Object.keys(map.regions[0]).length).toBeGreaterThan(0);
        expect(map.regions[0][0] instanceof Region).toBeTrue(0);
        expect(Object.keys(map.regions[0][0].blocks).length).toBe(0);
      });

    });

    describe('generateRegion', function () {

      function getBlocksStrings(blocks) {
        return blocks.map((row) => row.map((block) => block.toString()));
      }

      it('generates a region with blocks if populateWithBlocks=true passed to its parameters', function () {
        map.generateRegion(1, 5, true);
        expect(map.regions[5][1] instanceof Region).toBeTrue();
        expect(map.regions[5][1].blocks.length).toBeGreaterThan(0);
      });

      it('generates a region without blocks if populateWithBlocks=false passed to its parameters', function () {
        map.generateRegion(2, 3, false);
        expect(map.regions[3][2] instanceof Region).toBeTrue();
        expect(map.regions[3][2].blocks.length).toBe(0);
      });

      it('generates a region with the same blocks for the same mapSeed', function () {
        const oldRegion = map.generateRegion(-2, 3, true);
        const oldBlocksStrings = getBlocksStrings(oldRegion.blocks);
        map.setRandomness(map.gameSeed, map.mapSeed);
        const newRegion = map.generateRegion(-2, 3, true);
        const newBlocksStrings = getBlocksStrings(newRegion.blocks);
        expect(oldBlocksStrings).toEqual(newBlocksStrings);
      });

      it('generates a region with the same blocks for different gameSeed', function () {
        const oldRegion = map.generateRegion(-2, 3, true);
        const oldBlocksStrings = getBlocksStrings(oldRegion.blocks);
        const oldGameSeed = map.gameSeed;
        map.setRandomness(Rand.getUnpredictableInt31Unsigned(), map.mapSeed);
        expect(oldGameSeed).not.toEqual(map.gameSeed);
        const newRegion = map.generateRegion(-2, 3, true);
        const newBlocksStrings = getBlocksStrings(newRegion.blocks);
        expect(oldBlocksStrings).toEqual(newBlocksStrings);
      });

      it('generates a region with different blocks for different mapSeed', function () {
        const oldRegion = map.generateRegion(5, 0, true);
        const oldBlocksStrings = getBlocksStrings(oldRegion.blocks);
        const oldMapSeed = map.mapSeed;
        map.setRandomness(map.gameSeed, Rand.getUnpredictableInt31Unsigned());
        expect(oldMapSeed).not.toEqual(map.mapSeed);
        const newRegion = map.generateRegion(5, 0, true);
        const newBlocksStrings = getBlocksStrings(newRegion.blocks);
        expect(oldBlocksStrings).not.toEqual(newBlocksStrings);
      });

    });

    describe('setPlayers', function () {

      it('sets given players', function () {
        expect(map.players).toBeNull();
        const player = playerProvider.addHuman();
        const anotherPlayer = playerProvider.addAI();
        storageProvider.init(playerProvider.getAllPlayers());
        map.initAllBlocksNeighborsAndTmpAiValues();
        map.initAllBlocksBaseAiValues();
        map.setPlayers(playerProvider.getAllPlayers());
        expect(map.players).not.toBeNull();
        expect(typeof map.players).toBe('object');
        expect(Object.keys(map.players).length).toBe(2);
        expect(map.players[player.playerId]).toBe(player);
        expect(map.players[anotherPlayer.playerId]).toBe(anotherPlayer);
      });

      it('sets multiplayered improvedBlocksCounter', function () {
        expect(map.improvedBlocksCounter).toBeEmptyObject();
        const player = playerProvider.addHuman();
        const anotherPlayer = playerProvider.addAI();
        storageProvider.init(playerProvider.getAllPlayers());
        map.initAllBlocksNeighborsAndTmpAiValues();
        map.initAllBlocksBaseAiValues();
        map.setPlayers(playerProvider.getAllPlayers());
        expect(map.improvedBlocksCounter).toBeNonEmptyObject();
        expect(map.improvedBlocksCounter).toHaveNumber(player.playerId);
        expect(map.improvedBlocksCounter[player.playerId]).toBe(0);
        expect(map.improvedBlocksCounter).toHaveNumber(anotherPlayer.playerId);
        expect(map.improvedBlocksCounter[anotherPlayer.playerId]).toBe(0);
      });

      it('sets multiplayered improvementNeighbors', function () {
        expect(map.improvementNeighbors).toBeEmptyObject();
        const player = playerProvider.addHuman();
        const anotherPlayer = playerProvider.addAI();
        storageProvider.init(playerProvider.getAllPlayers());
        map.initAllBlocksNeighborsAndTmpAiValues();
        map.initAllBlocksBaseAiValues();
        map.setPlayers(playerProvider.getAllPlayers());
        expect(map.improvementNeighbors).toBeNonEmptyObject();
        expect(map.improvementNeighbors).toHaveArray(player.playerId);
        expect(map.improvementNeighbors[player.playerId].length).toBe(0);
        expect(map.improvementNeighbors).toHaveArray(anotherPlayer.playerId);
        expect(map.improvementNeighbors[anotherPlayer.playerId].length).toBe(0);
      });

      it('sets multiplayered affordableBlockCount', function () {
        expect(map.affordableBlockCount).toBeEmptyObject();
        const player = playerProvider.addHuman();
        const anotherPlayer = playerProvider.addAI();
        storageProvider.init(playerProvider.getAllPlayers());
        map.initAllBlocksNeighborsAndTmpAiValues();
        map.initAllBlocksBaseAiValues();
        map.setPlayers(playerProvider.getAllPlayers());
        expect(map.affordableBlockCount).toBeNonEmptyObject();
        expect(map.affordableBlockCount).toHaveNumber(player.playerId);
        expect(map.affordableBlockCount[player.playerId]).toBe(0);
        expect(map.affordableBlockCount).toHaveNumber(anotherPlayer.playerId);
        expect(map.affordableBlockCount[anotherPlayer.playerId]).toBe(0);
      });

    });

  });

});