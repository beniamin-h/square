'use strict';

describe('Map:simple_map_3x3_5x5_with_24_improved_blocks', function() {
  beforeEach(module('square'));

  var Map, storageProvider, GoldDeposit, playerProvider, gameStateProvider,
    mapProvider, ArrayUtils, testingUtils, turnTicker;

  beforeEach(inject(function (
      _Map_, _storageProvider_, _GoldDeposit_, _playerProvider_, _gameStateProvider_,
      _mapProvider_, _ArrayUtils_, _testingUtils_, _turnTicker_) {
    Map = _Map_;
    storageProvider = _storageProvider_;
    GoldDeposit = _GoldDeposit_;
    playerProvider = _playerProvider_;
    gameStateProvider = _gameStateProvider_;

    mapProvider = _mapProvider_;
    ArrayUtils = _ArrayUtils_;
    testingUtils = _testingUtils_;
    turnTicker = _turnTicker_;
  }));

  describe('simple_map_3x3_5x5_with_24_improved_blocks loaded by gameStateProvider', function () {

    var map;
    var plainBlock;
    var hillBlock;
    var aiPlayer;
    var humanPlayer;

    beforeEach(function (done) {
      gameStateProvider.load(process.env.PWD + '/test_fixtures/simple_map_3x3_5x5_with_24_improved_blocks.json', function (err) {
        if (err) {
          done.fail(err);
          return;
        }
        map = mapProvider.getMap();
        aiPlayer = playerProvider.getFirstAI();
        humanPlayer = playerProvider.getHuman();
        plainBlock = map.getBlock(1, -1, 1, 4);
        hillBlock = map.getBlock(1, 0, 1, 0);
        expect(plainBlock.kind.toString()).toBe('plain');
        expect(plainBlock.level).toBe(1);
        expect(plainBlock.square).toBeObject();
        expect(plainBlock.ownedBy).toBe(aiPlayer);
        expect(hillBlock.kind.toString()).toBe('hill');
        expect(hillBlock.level).toBe(2);
        expect(hillBlock.square).toBeObject();
        expect(hillBlock.ownedBy).toBe(aiPlayer);
        done();
      });
    });

    describe('tryToDisclaimBlock', function () {

      it('changes the given block level to 0', function () {
        var result = map.tryToDisclaimBlock(plainBlock, aiPlayer);
        expect(result).toBeTrue();
        expect(plainBlock.level).toBe(0);
      });

      it('sets the given block owner to null', function () {
        var result = map.tryToDisclaimBlock(plainBlock, aiPlayer);
        expect(result).toBeTrue();
        expect(plainBlock.ownedBy).toBeNull();
      });

      it('destroys the square of the given block', function () {
        var square = plainBlock.square;
        var prevNumberOfSquaresLevel2 = aiPlayer.squaresByLevel['2'].length;
        var prevGoldIncome = storageProvider.getStorage(aiPlayer).goldIncome;
        var result = map.tryToDisclaimBlock(plainBlock, aiPlayer);
        expect(result).toBeTrue();
        expect(plainBlock.square).toBeNull();
        expect(map.squaresMap.getSquare(aiPlayer, 2,
          square.regionX, square.regionY, square.x, square.y)).toBeNull();
        expect(aiPlayer.squaresByLevel['2'].length).toBe(prevNumberOfSquaresLevel2 - 1);
        expect(storageProvider.getStorage(aiPlayer).goldIncome).toBe(prevGoldIncome - 1);
      });

      it('does not change the given block level ' +
         'if the given player disclaimCooldown is not 0', function () {
        aiPlayer.disclaimCooldown = 10;
        var result = map.tryToDisclaimBlock(plainBlock, aiPlayer);
        expect(result).toBeFalse();
        expect(plainBlock.level).toBe(1);
      });

      it('cannot disclaim the same block twice', function () {
        var result = map.tryToDisclaimBlock(plainBlock, aiPlayer);
        expect(result).toBeTrue();
        aiPlayer.disclaimCooldown = 0;
        var resultAgain = map.tryToDisclaimBlock(plainBlock, aiPlayer);
        expect(resultAgain).toBeFalse();
      });

      it('cannot disclaim the block of another player', function () {
        var anotherPlainBlock = map.getBlock(0, 1, -1, 0);
        map.captureBlock(map.getBlock(0, 1, 0, 0), humanPlayer);
        map.captureBlock(anotherPlainBlock, humanPlayer);
        var wrongPlayerResult = map.tryToDisclaimBlock(anotherPlainBlock, aiPlayer);
        expect(wrongPlayerResult).toBeFalse();
        var rightPlayerResult = map.tryToDisclaimBlock(anotherPlainBlock, humanPlayer);
        expect(rightPlayerResult).toBeTrue();
      });

      it('cannot disclaim the block of level = 0', function () {
        var blockLevel0 = map.getBlock(-1, -1, 0, 0);
        expect(blockLevel0.level).toBe(0);
        var result = map.tryToDisclaimBlock(blockLevel0, aiPlayer);
        expect(result).toBeFalse();
      });

      it('cannot disclaim the last block', function () {
        var lastBlock = map.getBlock(0, 1, 0, 0);
        map.captureBlock(lastBlock, humanPlayer);
        var result = map.tryToDisclaimBlock(lastBlock, humanPlayer);
        expect(result).toBeFalse();
      });

      it('can disclaim a disabled block', function () {
        plainBlock.disabled = true;
        var result = map.tryToDisclaimBlock(plainBlock, aiPlayer);
        expect(result).toBeTrue();
        expect(plainBlock.level).toBe(0);
      });

      describe('changes player\'s resource production', function () {

        it('for block level = 1', function () {
          storageProvider.getStorage(aiPlayer).take({ food: 1000 }, true);
          var resources = storageProvider.getStorage(aiPlayer).resources;
          var prevFood = resources.food;
          var prevWood = resources.wood;
          var result = map.tryToDisclaimBlock(plainBlock, aiPlayer);
          expect(result).toBeTrue();
          expect(resources.food).toBe(prevFood - 1);
          expect(resources.wood).toBe(prevWood + 1);
        });

        it('for block level = 2', function () {
          storageProvider.getStorage(aiPlayer).take({ stone: 1000 }, true);
          var resources = storageProvider.getStorage(aiPlayer).resources;
          var prevStone = resources.stone;
          var prevMetal = resources.metal;
          var result = map.tryToDisclaimBlock(hillBlock, aiPlayer);
          expect(result).toBeTrue();
          expect(resources.stone).toBe(prevStone - 2);
          expect(resources.metal).toBe(prevMetal + 1);
        });

      });

      describe('changes player\'s gold income', function () {

        it('for block level = 1', function () {
          map.changeBlockKind(plainBlock, new GoldDeposit());
          var prevGoldIncome = storageProvider.getStorage(aiPlayer).goldIncome;
          var result = map.tryToDisclaimBlock(plainBlock, aiPlayer);
          expect(result).toBeTrue();
          expect(storageProvider.getStorage(aiPlayer).goldIncome).toBe(prevGoldIncome - 13);
        });

        it('for block level = 2', function () {
          map.changeBlockKind(plainBlock, new GoldDeposit());
          map.changeOwnedBlockLevel(plainBlock, 2);
          var prevGoldIncome = storageProvider.getStorage(aiPlayer).goldIncome;
          var result = map.tryToDisclaimBlock(plainBlock, aiPlayer);
          expect(result).toBeTrue();
          expect(storageProvider.getStorage(aiPlayer).goldIncome).toBe(prevGoldIncome - 25);
        });

      });

      it('enables disabled block', function () {
        plainBlock.disabled = true;
        var result = map.tryToDisclaimBlock(plainBlock, aiPlayer);
        expect(result).toBeTrue();
        expect(plainBlock.disabled).toBeFalse();
      });

      describe('properly updates surrounding blocks\' affordability and improvability', function () {

        function expectImprovable(block) {
          expect(map.checkBlockImprovability(block, aiPlayer)).toBeTrue();
          return block;
        }

        function expectAffordable(block) {
          expect(block.affordableForPlayer(aiPlayer)).toBeTrue();
          return block;
        }

        function expectHasImprovedNeighbor(block) {
          expect(block.hasImprovedNeighbor(aiPlayer)).toBeTrue();
          return block;
        }

        function expectInMapImprovementNeighbors(block) {
          expect(map.improvementNeighbors[aiPlayer.playerId]).toContain(block);
          return block;
        }

        function expectNotImprovable(block) {
          expect(map.checkBlockImprovability(block, aiPlayer)).toBeFalse();
          return block;
        }

        function expectNotAffordable(block) {
          expect(block.affordableForPlayer(aiPlayer)).toBeFalse();
          return block;
        }

        function expectHasNotImprovedNeighbor(block) {
          expect(block.hasImprovedNeighbor(aiPlayer)).toBeFalse();
          return block;
        }

        function expectNotInMapImprovementNeighbors(block) {
          expect(map.improvementNeighbors[aiPlayer.playerId]).not.toContain(block);
          return block;
        }

        it('for block Block-r0x-1_3x3L1h', function () {
          var block = map.getBlock(0, -1, 3, 3);
          [ // neighboring blocks improvable & affordable for AIPlayer:
            map.getBlock(0, -1, 2, 3),
            map.getBlock(0, -1, 4, 4),
            map.getBlock(0, -1, 4, 2)]
              .map(expectImprovable)
              .map(expectAffordable)
              .map(expectHasImprovedNeighbor)
              .map(expectInMapImprovementNeighbors);
          [ // neighboring blocks not affordable for AIPlayer:
            map.getBlock(0, -1, 3, 2),
            map.getBlock(0, -1, 4, 1)]
              .map(expectNotImprovable)
              .map(expectNotAffordable)
              .map(expectHasImprovedNeighbor)
              .map(expectInMapImprovementNeighbors);
          [ // further blocks:
            map.getBlock(0, -1, 2, 2),
            map.getBlock(0, -1, 2, 4),
            map.getBlock(0, -1, 3, 1),
            map.getBlock(0, -1, 3, 0)]
              .map(expectNotImprovable)
              .map(expectNotAffordable)
              .map(expectHasNotImprovedNeighbor)
              .map(expectNotInMapImprovementNeighbors);
          [ // improved blocks
            map.getBlock(0, -1, 3, 3),
            map.getBlock(0, -1, 4, 3)]
              .map(expectNotImprovable)
              .map(expectNotAffordable)
              .map(expectNotInMapImprovementNeighbors);
          var result = map.tryToDisclaimBlock(block, aiPlayer);
          expect(result).toBeTrue();
          [ // neighboring blocks improvable & affordable for AIPlayer:
            map.getBlock(0, -1, 3, 3),
            map.getBlock(0, -1, 4, 4),
            map.getBlock(0, -1, 4, 2)]
              .map(expectImprovable)
              .map(expectAffordable)
              .map(expectHasImprovedNeighbor)
              .map(expectInMapImprovementNeighbors);
          [ // neighboring blocks not affordable for AIPlayer:
            map.getBlock(0, -1, 4, 1)]
              .map(expectNotImprovable)
              .map(expectNotAffordable)
              .map(expectHasImprovedNeighbor)
              .map(expectInMapImprovementNeighbors);
          [ // further blocks:
            map.getBlock(0, -1, 3, 2),
            map.getBlock(0, -1, 2, 3),
            map.getBlock(0, -1, 3, 4),
            map.getBlock(0, -1, 2, 2),
            map.getBlock(0, -1, 2, 4),
            map.getBlock(0, -1, 3, 1),
            map.getBlock(0, -1, 3, 0)]
              .map(expectNotImprovable)
              .map(expectNotAffordable)
              .map(expectHasNotImprovedNeighbor)
              .map(expectNotInMapImprovementNeighbors);
          [ // improved blocks
            map.getBlock(0, -1, 4, 3)]
              .map(expectNotImprovable)
              .map(expectNotAffordable)
              .map(expectNotInMapImprovementNeighbors);
        });

        it('for block Block-r0x0_4x2L1f', function () {
          var block = map.getBlock(0, 0, 4, 2);
          [ // neighboring blocks improvable & affordable for AIPlayer:
            map.getBlock(0, 0, 4, 3),
            map.getBlock(0, 0, 4, 0)]
              .map(expectImprovable)
              .map(expectAffordable)
              .map(expectHasImprovedNeighbor)
              .map(expectInMapImprovementNeighbors);
          [ // neighboring blocks not affordable for AIPlayer:
            map.getBlock(0, 0, 3, 1),
            map.getBlock(0, 0, 3, 2),
            map.getBlock(1, 0, 0, 3),
            map.getBlock(1, 0, 1, 3)]
              .map(expectNotImprovable)
              .map(expectNotAffordable)
              .map(expectHasImprovedNeighbor)
              .map(expectInMapImprovementNeighbors);
          [ // further blocks:
            map.getBlock(0, 0, 3, 0),
            map.getBlock(0, 0, 2, 1),
            map.getBlock(0, 0, 2, 2),
            map.getBlock(0, 0, 3, 3),
            map.getBlock(0, 0, 4, 4),
            map.getBlock(1, 0, 0, 4),
            map.getBlock(1, 0, 2, 3)]
              .map(expectNotImprovable)
              .map(expectNotAffordable)
              .map(expectHasNotImprovedNeighbor)
              .map(expectNotInMapImprovementNeighbors);
          [ // improved blocks,
            map.getBlock(0, 0, 4, 2),
            map.getBlock(0, 0, 4, 1),
            map.getBlock(1, 0, 0, 2),
            map.getBlock(1, 0, 0, 1)]
              .map(expectNotImprovable)
              .map(expectNotAffordable)
              .map(expectNotInMapImprovementNeighbors);
          var result = map.tryToDisclaimBlock(block, aiPlayer);
          expect(result).toBeTrue();
          [ // neighboring blocks improvable & affordable for AIPlayer:
            map.getBlock(0, 0, 4, 2),
            map.getBlock(0, 0, 4, 0),
            map.getBlock(0, 0, 3, 1),
            map.getBlock(1, 0, 0, 3),
            map.getBlock(1, 0, 1, 3),
            map.getBlock(1, 0, 2, 2),
            map.getBlock(1, -1, 3, 0)]
              .map(expectImprovable)
              .map(expectAffordable)
              .map(expectHasImprovedNeighbor)
              .map(expectInMapImprovementNeighbors);
          // no neighboring blocks not affordable for AIPlayer
          [ // further blocks:
            map.getBlock(0, 0, 3, 0),
            map.getBlock(0, 0, 2, 1),
            map.getBlock(0, 0, 2, 2),
            map.getBlock(0, 0, 3, 3),
            map.getBlock(0, 0, 4, 3),
            map.getBlock(0, 0, 4, 4),
            map.getBlock(1, 0, 0, 4),
            map.getBlock(1, 0, 2, 3)]
              .map(expectNotImprovable)
              .map(expectNotAffordable)
              .map(expectHasNotImprovedNeighbor)
              .map(expectNotInMapImprovementNeighbors);
          [ // improved blocks,
            map.getBlock(0, 0, 4, 1),
            map.getBlock(1, 0, 0, 2),
            map.getBlock(1, 0, 0, 1)]
              .map(expectNotImprovable)
              .map(expectNotAffordable)
              .map(expectNotInMapImprovementNeighbors);
        });

        it('for block Block-r1x0_0x2L1p', function () {
          var block = map.getBlock(1, 0, 0, 2);
          [ // neighboring blocks improvable & affordable for AIPlayer:
            map.getBlock(0, 0, 4, 3),
            map.getBlock(0, 0, 4, 0)]
              .map(expectImprovable)
              .map(expectAffordable)
              .map(expectHasImprovedNeighbor)
              .map(expectInMapImprovementNeighbors);
          [ // neighboring blocks not affordable for AIPlayer:
            map.getBlock(0, 0, 3, 1),
            map.getBlock(0, 0, 3, 2),
            map.getBlock(1, 0, 0, 3),
            map.getBlock(1, 0, 1, 3)]
              .map(expectNotImprovable)
              .map(expectNotAffordable)
              .map(expectHasImprovedNeighbor)
              .map(expectInMapImprovementNeighbors);
          [ // further blocks:
            map.getBlock(0, 0, 3, 0),
            map.getBlock(0, 0, 2, 1),
            map.getBlock(0, 0, 2, 2),
            map.getBlock(0, 0, 3, 3),
            map.getBlock(0, 0, 4, 4),
            map.getBlock(1, 0, 0, 4),
            map.getBlock(1, 0, 2, 3)]
              .map(expectNotImprovable)
              .map(expectNotAffordable)
              .map(expectHasNotImprovedNeighbor)
              .map(expectNotInMapImprovementNeighbors);
          [ // improved blocks,
            map.getBlock(1, 0, 0, 2),
            map.getBlock(1, 0, 1, 2),
            map.getBlock(0, 0, 4, 2),
            map.getBlock(0, 0, 4, 1),
            map.getBlock(1, 0, 0, 1),
            map.getBlock(1, 0, 1, 1)]
              .map(expectNotImprovable)
              .map(expectNotAffordable)
              .map(expectNotInMapImprovementNeighbors);
          var result = map.tryToDisclaimBlock(block, aiPlayer);
          expect(result).toBeTrue();
          [ // neighboring blocks improvable & affordable for AIPlayer:
            map.getBlock(0, 0, 4, 0),
            map.getBlock(1, 0, 2, 1),
            map.getBlock(1, -1, 2, 0)]
              .map(expectImprovable)
              .map(expectAffordable)
              .map(expectHasImprovedNeighbor)
              .map(expectInMapImprovementNeighbors);
          [ // neighboring blocks not affordable for AIPlayer:
            map.getBlock(0, 0, 3, 1),
            map.getBlock(0, 0, 3, 2),
            map.getBlock(0, 0, 4, 3),
            map.getBlock(1, 0, 1, 3)]
              .map(expectNotImprovable)
              .map(expectNotAffordable)
              .map(expectHasImprovedNeighbor)
              .map(expectInMapImprovementNeighbors);
          [ // further blocks:
            map.getBlock(1, 0, 0, 3),
            map.getBlock(0, 0, 3, 0),
            map.getBlock(0, 0, 2, 1),
            map.getBlock(0, 0, 2, 2),
            map.getBlock(0, 0, 3, 3),
            map.getBlock(0, 0, 4, 4),
            map.getBlock(1, 0, 0, 4),
            map.getBlock(1, 0, 2, 3)]
              .map(expectNotImprovable)
              .map(expectNotAffordable)
              .map(expectHasNotImprovedNeighbor)
              .map(expectNotInMapImprovementNeighbors);
          [ // improved blocks,
            map.getBlock(1, 0, 1, 2),
            map.getBlock(0, 0, 4, 2),
            map.getBlock(0, 0, 4, 1),
            map.getBlock(1, 0, 0, 1),
            map.getBlock(1, 0, 1, 1)]
              .map(expectNotImprovable)
              .map(expectNotAffordable)
              .map(expectNotInMapImprovementNeighbors);
        });

        it('for block Block-r1x0_0x2L1p', function () {
          var block = map.getBlock(1, 0, 0, 2);
          [ // neighboring blocks improvable & affordable for AIPlayer:
            map.getBlock(0, 0, 4, 3),
            map.getBlock(0, 0, 4, 0)]
              .map(expectImprovable)
              .map(expectAffordable)
              .map(expectHasImprovedNeighbor)
              .map(expectInMapImprovementNeighbors);
          [ // neighboring blocks not affordable for AIPlayer:
            map.getBlock(0, 0, 3, 1),
            map.getBlock(0, 0, 3, 2),
            map.getBlock(1, 0, 0, 3),
            map.getBlock(1, 0, 1, 3)]
              .map(expectNotImprovable)
              .map(expectNotAffordable)
              .map(expectHasImprovedNeighbor)
              .map(expectInMapImprovementNeighbors);
          [ // further blocks:
            map.getBlock(0, 0, 3, 0),
            map.getBlock(0, 0, 2, 1),
            map.getBlock(0, 0, 2, 2),
            map.getBlock(0, 0, 3, 3),
            map.getBlock(0, 0, 4, 4),
            map.getBlock(1, 0, 0, 4),
            map.getBlock(1, 0, 2, 3)]
              .map(expectNotImprovable)
              .map(expectNotAffordable)
              .map(expectHasNotImprovedNeighbor)
              .map(expectNotInMapImprovementNeighbors);
          [ // improved blocks,
            map.getBlock(1, 0, 0, 2),
            map.getBlock(1, 0, 1, 2),
            map.getBlock(0, 0, 4, 2),
            map.getBlock(0, 0, 4, 1),
            map.getBlock(1, 0, 0, 1),
            map.getBlock(1, 0, 1, 1)]
              .map(expectNotImprovable)
              .map(expectNotAffordable)
              .map(expectNotInMapImprovementNeighbors);
          var result = map.tryToDisclaimBlock(block, aiPlayer);
          expect(result).toBeTrue();
          [ // neighboring blocks improvable & affordable for AIPlayer:
            map.getBlock(0, 0, 4, 0),
            map.getBlock(1, 0, 2, 1),
            map.getBlock(1, -1, 2, 0)]
              .map(expectImprovable)
              .map(expectAffordable)
              .map(expectHasImprovedNeighbor)
              .map(expectInMapImprovementNeighbors);
          [ // neighboring blocks not affordable for AIPlayer:
            map.getBlock(0, 0, 3, 1),
            map.getBlock(0, 0, 3, 2),
            map.getBlock(0, 0, 4, 3),
            map.getBlock(1, 0, 1, 3)]
              .map(expectNotImprovable)
              .map(expectNotAffordable)
              .map(expectHasImprovedNeighbor)
              .map(expectInMapImprovementNeighbors);
          [ // further blocks:
            map.getBlock(1, 0, 0, 3),
            map.getBlock(0, 0, 3, 0),
            map.getBlock(0, 0, 2, 1),
            map.getBlock(0, 0, 2, 2),
            map.getBlock(0, 0, 3, 3),
            map.getBlock(0, 0, 4, 4),
            map.getBlock(1, 0, 0, 4),
            map.getBlock(1, 0, 2, 3)]
              .map(expectNotImprovable)
              .map(expectNotAffordable)
              .map(expectHasNotImprovedNeighbor)
              .map(expectNotInMapImprovementNeighbors);
          [ // improved blocks,
            map.getBlock(1, 0, 1, 2),
            map.getBlock(0, 0, 4, 2),
            map.getBlock(0, 0, 4, 1),
            map.getBlock(1, 0, 0, 1),
            map.getBlock(1, 0, 1, 1)]
              .map(expectNotImprovable)
              .map(expectNotAffordable)
              .map(expectNotInMapImprovementNeighbors);
        });

      });

      it('decreases improvedBlocksCounter', function () {
        var prevCounter = map.improvedBlocksCounter[aiPlayer.playerId];
        var result = map.tryToDisclaimBlock(plainBlock, aiPlayer);
        expect(result).toBeTrue();
        expect(map.improvedBlocksCounter[aiPlayer.playerId]).toBe(prevCounter - 1);
      });

      describe('changes the game to the state before block improving', function () {

        function* makeTest(block) {
          expect(block.level).toBe(0);
          expect(map.checkBlockImprovability(block, aiPlayer)).toBeTrue();
          var prevImprovementNeighbors = ArrayUtils.copy(map.improvementNeighbors[aiPlayer.playerId]);
          var prevAffordableForPlayers = ArrayUtils.copy(block._affordableForPlayers);
          var prevHasImprovedNeighborOfPlayers = ArrayUtils.copy(block._hasImprovedNeighborOfPlayers);
          var prevAffordableBlockCount = map.affordableBlockCount[aiPlayer.playerId];
          var prevImprovedNeighbors = testingUtils.copyObject(block.improvedNeighbors);
          var prevBaseAiValue = block.baseAiValue;
          var prevKindBaseAiValue = block.kind.baseAiValue;
          var prevAiValueForPlayer = block.aiValueForPlayer[aiPlayer.playerId];
          var prevAiValuesForPlayerMissions = testingUtils.copyObject(block.aiValuesForPlayerMissions);
          var storage = storageProvider.getStorage(aiPlayer);
          var prevResources = testingUtils.copyObject(storage.resources);
          var prevGoldIncome = storage.goldIncome;
          var prevBlocksByPlayerAndKind = ArrayUtils.copy(
            map.mapBlocks.blocksByPlayerAndKind[aiPlayer.playerId]['plain']);
          expect(prevBlocksByPlayerAndKind.length).toBeGreaterThan(0);

          yield null;

          expect(block.level).toBe(0);
          expect(map.checkBlockImprovability(block, aiPlayer)).toBeTrue();
          expect(map.improvementNeighbors[aiPlayer.playerId])
            .toEqual(jasmine.arrayContaining(prevImprovementNeighbors));
          expect(map.improvementNeighbors[aiPlayer.playerId].length)
            .toBe(prevImprovementNeighbors.length);
          expect(block._affordableForPlayers)
            .toEqual(jasmine.arrayContaining(prevAffordableForPlayers));
          expect(block._affordableForPlayers.length)
            .toBe(prevAffordableForPlayers.length);
          expect(block._hasImprovedNeighborOfPlayers)
            .toEqual(jasmine.arrayContaining(prevHasImprovedNeighborOfPlayers));
          expect(block._hasImprovedNeighborOfPlayers.length)
            .toBe(prevHasImprovedNeighborOfPlayers.length);
          expect(map.affordableBlockCount[aiPlayer.playerId])
            .toBe(prevAffordableBlockCount);
          expect(block.improvedNeighbors).toEqual(prevImprovedNeighbors);
          expect(block.baseAiValue).toBe(prevBaseAiValue);
          expect(block.kind.baseAiValue).toBe(prevKindBaseAiValue);
          expect(block.aiValueForPlayer[aiPlayer.playerId])
            .toBeWithinRange(prevAiValueForPlayer - 1, prevAiValueForPlayer + 1);
          expect(block.aiValuesForPlayerMissions).toEqual(prevAiValuesForPlayerMissions);
          expect(storage.resources).toEqual(prevResources);
          expect(storage.goldIncome).toBe(prevGoldIncome);
          expect(map.mapBlocks.blocksByPlayerAndKind[aiPlayer.playerId]['plain'])
            .toEqual(jasmine.arrayContaining(prevBlocksByPlayerAndKind));
          expect(map.mapBlocks.blocksByPlayerAndKind[aiPlayer.playerId]['plain'].length)
            .toBe(prevBlocksByPlayerAndKind.length);
        }

        it('for block Block-r0x0_4x3L0m', function () {
          var block = map.getBlock(0, 0, 4, 3);
          var test = makeTest(block);
          test.next();
          map.captureBlock(block, aiPlayer);
          var result = map.tryToDisclaimBlock(block, aiPlayer);
          expect(result).toBeTrue();
          test.next();
        });

        it('for block Block-r0x-1_4x2L0p', function () {
          var block = map.getBlock(0, -1, 4, 2);
          var test = makeTest(block);
          test.next();
          map.captureBlock(block, aiPlayer);
          var result = map.tryToDisclaimBlock(block, aiPlayer);
          expect(result).toBeTrue();
          test.next();
        });

        it('for block Block-r0x0_4x3L0m and Block-r0x0_3x2L0h', function () {
          var block_1 = map.getBlock(0, 0, 4, 3);
          var block_2 = map.getBlock(0, 0, 3, 2);
          var test_1 = makeTest(block_1);
          var test_2 = makeTest(block_2);
          test_1.next();
          map.captureBlock(block_1, aiPlayer);
          test_2.next();
          map.captureBlock(block_2, aiPlayer);
          var result_1 = map.tryToDisclaimBlock(block_2, aiPlayer);
          aiPlayer.disclaimCooldown = 0;
          expect(result_1).toBeTrue();
          test_2.next();
          var result_2 = map.tryToDisclaimBlock(block_1, aiPlayer);
          aiPlayer.disclaimCooldown = 0;
          expect(result_2).toBeTrue();
          test_1.next();
        });

        it('for block Block-r0x0_4x3L0m, Block-r0x0_3x2L0h and Block-r1x0_0x3L0f', function () {
          var block_1 = map.getBlock(0, 0, 4, 3);
          var block_2 = map.getBlock(0, 0, 3, 2);
          var block_3 = map.getBlock(1, 0, 0, 3);
          var test = makeTest(block_1);
          test.next();
          map.captureBlock(block_1, aiPlayer);
          map.captureBlock(block_2, aiPlayer);
          map.captureBlock(block_3, aiPlayer);
          expect(map.tryToDisclaimBlock(block_1, aiPlayer)).toBeTrue();
          aiPlayer.disclaimCooldown = 0;
          expect(map.tryToDisclaimBlock(block_2, aiPlayer)).toBeTrue();
          aiPlayer.disclaimCooldown = 0;
          expect(map.tryToDisclaimBlock(block_3, aiPlayer)).toBeTrue();
          aiPlayer.disclaimCooldown = 0;
          test.next();
        });

      });

      it('removes the given block from AI improvedBlocks list', function () {
        expect(aiPlayer.ai.improvedBlocks).toContain(plainBlock);
        var result = map.tryToDisclaimBlock(plainBlock, aiPlayer);
        expect(result).toBeTrue();
        expect(aiPlayer.ai.improvedBlocks).not.toContain(plainBlock);
      });

      it('creates new squares from the former square blocks', function () {
        var storage = storageProvider.getStorage(aiPlayer);
        storage.put({ metal: 100, stone: 100, wood: 100, food: 100 });
        expect(map.getBlock(0, 0, 4, 1).square).toBeNull();
        expect(map.tryToDisclaimBlock(map.getBlock(1, 0, 1, 2), aiPlayer)).toBeTrue();
        aiPlayer.disclaimCooldown = 0;
        expect(map.getBlock(0, 0, 4, 1).square).toBeObject();
        expect(map.tryToDisclaimBlock(map.getBlock(1, 0, 0, 2), aiPlayer)).toBeTrue();
        aiPlayer.disclaimCooldown = 0;
        expect(map.getBlock(0, 0, 4, 1).square).toBeNull();
        expect(map.tryToCaptureBlock(map.getBlock(1, 0, 2, 0), aiPlayer)).toBeTrue();
        expect(map.tryToCaptureBlock(map.getBlock(1, 0, 2, 1), aiPlayer)).toBeTrue();
        expect(map.tryToCaptureBlock(map.getBlock(0, 0, 4, 0), aiPlayer)).toBeTrue();
        expect(map.tryToDisclaimBlock(map.getBlock(1, -1, 0, 4), aiPlayer)).toBeTrue();
        aiPlayer.disclaimCooldown = 0;
        expect(map.getBlock(1, -1, 0, 4).square).toBeNull();
        expect(map.getBlock(1, 0, 1, 0).square).toBeObject();
        expect(map.getBlock(0, 0, 4, 1).square).toBeObject();
        expect(map.getBlock(1, -1, 1, 4).square).toBeObject();
        expect(map.getBlock(1, 0, 1, 1).square).toBeNull();
      });

    });

    describe('changeOwnedBlockLevel', function () {

      it('does not change the level of unimproved block', function () {
        var block = map.getBlock(1, -1, 3, 4);
        expect(block.level).toBe(0);
        expect(function () {
          map.changeOwnedBlockLevel(block, 2);
        }).toThrowError();
        expect(block.level).toBe(0);
      });

      it('does nothing if given newLevel is equal to given block level', function () {
        var block = map.getBlock(1, 0, 1, 0);
        expect(block.level).toBe(2);
        map.changeOwnedBlockLevel(block, 2);
        expect(block.level).toBe(2);
      });

      describe('adds resource production difference to the storage', function () {

        it('for block Block-r1x-1_0x4L1p', function () {
          var storage = storageProvider.getStorage(aiPlayer);
          var prevResources = testingUtils.copyObject(storage.resources);
          var block = map.getBlock(1, -1, 0, 4);
          expect(block.level).toBe(1);
          map.changeOwnedBlockLevel(block, 2);
          expect(block.level).toBe(2);
          expect(storage.resources.food).toBe(prevResources.food + 1);
        });

        it('for block Block-r1x0_1x0L2h', function () {
          var storage = storageProvider.getStorage(aiPlayer);
          var prevResources = testingUtils.copyObject(storage.resources);
          var block = map.getBlock(1, 0, 1, 0);
          expect(block.level).toBe(2);
          map.changeOwnedBlockLevel(block, 3);
          expect(block.level).toBe(3);
          expect(storage.resources.stone).toBe(prevResources.stone + 2);
        });

        it('for block Block-r1x-1_0x3L1mR', function () {
          var storage = storageProvider.getStorage(aiPlayer);
          var prevResources = testingUtils.copyObject(storage.resources);
          var block = map.getBlock(1, -1, 0, 3);  //rich
          expect(block.level).toBe(1);
          map.changeOwnedBlockLevel(block, 3);
          expect(block.level).toBe(3);
          expect(storage.resources.metal).toBe(prevResources.metal + 6);
        });

      });

      describe('subtracts resource production difference from the storage', function () {

        it('for block Block-r1x0_1x0L2h', function () {
          var storage = storageProvider.getStorage(aiPlayer);
          var prevResources = testingUtils.copyObject(storage.resources);
          var block = map.getBlock(1, 0, 1, 0);
          expect(block.level).toBe(2);
          map.changeOwnedBlockLevel(block, 1);
          expect(block.level).toBe(1);
          expect(storage.resources.stone).toBe(prevResources.stone - 1);
        });

        it('for block Block-r1x0_1x2L2f', function () {
          var storage = storageProvider.getStorage(aiPlayer);
          var prevResources = testingUtils.copyObject(storage.resources);
          var block = map.getBlock(1, 0, 1, 2);
          expect(block.level).toBe(2);
          map.changeOwnedBlockLevel(block, 1);
          expect(block.level).toBe(1);
          expect(storage.resources.wood).toBe(prevResources.wood - 1);
        });

      });

      it('adds gold income difference to the storage', function () {
        var storage = storageProvider.getStorage(aiPlayer);
        var block = map.getBlock(1, -1, 1, 4);
        map.changeBlockKind(block, new GoldDeposit(), false);
        expect(block.level).toBe(1);
        var prevGoldIncome = storage.goldIncome;
        map.changeOwnedBlockLevel(block, 3);
        expect(block.level).toBe(3);
        expect(storage.goldIncome).toBe(prevGoldIncome + 36);
      });

      it('subtracts gold income difference from the storage', function () {
        var storage = storageProvider.getStorage(aiPlayer);
        var block = map.getBlock(1, -1, 1, 4);
        map.changeBlockKind(block, new GoldDeposit(), false);
        expect(block.level).toBe(1);
        var prevGoldIncome = storage.goldIncome;
        map.changeOwnedBlockLevel(block, 3);
        expect(block.level).toBe(3);
        expect(storage.goldIncome).toBe(prevGoldIncome + 36);
        map.changeOwnedBlockLevel(block, 2);
        expect(block.level).toBe(2);
        expect(storage.goldIncome).toBe(prevGoldIncome + 12);
      });

    });

    describe('disableBlockDueToResourceShortage', function () {

      function disableBlock() {
        const disableResult = map.disableBlockDueToResourceShortage(
          'wood', playerProvider.getFirstAI());
        expect(disableResult).toBeTrue();
        const disabledBlocks = map._getBlocksByConsumedResourceAndPlayer(
          'wood', playerProvider.getFirstAI(), false);
        expect(disabledBlocks.length).toBe(1);
        const enableResult = map.enableBlockDisabledDueToResourceShortage(
          'wood', playerProvider.getFirstAI());
        expect(enableResult).toBeTrue();
        return disabledBlocks[0];
      }

      it('disables the same blocks for the same ' +
         'gameSeed, player, resource and turn', function () {
        const prevDisabledBlock = disableBlock();
        map.setRandomness(map.gameSeed, map.mapSeed);
        const nextDisabledBlock = disableBlock();
        expect(prevDisabledBlock).toBe(nextDisabledBlock);
      });

      it('disables different blocks for the same ' +
         'player, resource and turn, but a different gameSeed', function () {
        const prevDisabledBlock = disableBlock();
        map.setRandomness(1595975755, map.mapSeed);
        const nextDisabledBlock = disableBlock();
        expect(prevDisabledBlock).not.toBe(nextDisabledBlock);
      });

      it('disables different blocks for the same ' +
         'gameSeed, player and resource, but a different turn', function () {
        const prevDisabledBlock = disableBlock();
        map.setRandomness(map.gameSeed, map.mapSeed);
        turnTicker.tick();
        turnTicker.tick();
        const nextDisabledBlock = disableBlock();
        expect(prevDisabledBlock).not.toBe(nextDisabledBlock);
      });

    });

    describe('enableBlockDisabledDueToResourceShortage', function () {

      function enableBlock() {
        const disablingResult = map.disableBlockDueToResourceShortage(
            'wood', playerProvider.getFirstAI()) &&
          map.disableBlockDueToResourceShortage(
            'wood', playerProvider.getFirstAI()) &&
          map.disableBlockDueToResourceShortage(
            'wood', playerProvider.getFirstAI());
        expect(disablingResult).toBeTrue();
        const disabledBlocks = map._getBlocksByConsumedResourceAndPlayer(
          'wood', playerProvider.getFirstAI(), false);
        expect(disabledBlocks.length).toBe(3);
        const enableResult = map.enableBlockDisabledDueToResourceShortage(
          'wood', playerProvider.getFirstAI());
        expect(enableResult).toBeTrue();
        const newDisabledBlocks = map._getBlocksByConsumedResourceAndPlayer(
          'wood', playerProvider.getFirstAI(), false);
        const enabledBlocks = ArrayUtils.elementsDifference(
          disabledBlocks, newDisabledBlocks);
        expect(enabledBlocks.length).toBe(1);
        const enablingRestResult = map.enableBlockDisabledDueToResourceShortage(
            'wood', playerProvider.getFirstAI()) &&
          map.enableBlockDisabledDueToResourceShortage(
            'wood', playerProvider.getFirstAI());
        expect(enablingRestResult).toBeTrue();
        return enabledBlocks[0];
      }

      it('enables the same block for the same ' +
         'gameSeed, player, resource and turn', function () {
        const prevEnabledBlock = enableBlock();
        map.setRandomness(map.gameSeed, map.mapSeed);
        const nextEnabledBlock = enableBlock();
        expect(prevEnabledBlock).toBe(nextEnabledBlock);
      });

      it('enables different block for the same ' +
         'player, resource and turn, but different gameSeed', function () {
        const prevEnabledBlock = enableBlock();
        map.setRandomness(778915661, map.mapSeed);
        const nextEnabledBlock = enableBlock();
        expect(prevEnabledBlock).toBe(nextEnabledBlock);
      });

    });

  });

});