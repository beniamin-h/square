'use strict';

angular.module('square').factory('mapDirections', [
    function () {

  var prevNonDiagonalDirections = {
    up: 'left',
    rightUp: 'up',
    right: 'up',
    rightDown: 'right',
    down: 'right',
    leftDown: 'down',
    left: 'down',
    leftUp: 'left'
  };

  var prevDiagonalDirections = {
    up: 'leftUp',
    rightUp: 'leftUp',
    right: 'rightUp',
    rightDown: 'rightUp',
    down: 'rightDown',
    leftDown: 'rightDown',
    left: 'leftDown',
    leftUp: 'leftDown'
  };

  var nextNonDiagonalDirections = {
    up: 'right',
    rightUp: 'right',
    right: 'down',
    rightDown: 'down',
    down: 'left',
    leftDown: 'left',
    left: 'up',
    leftUp: 'up'
  };

  var nextDiagonalDirections = {
    up: 'rightUp',
    rightUp: 'rightDown',
    right: 'rightDown',
    rightDown: 'leftDown',
    down: 'leftDown',
    leftDown: 'leftUp',
    left: 'leftUp',
    leftUp: 'rightUp'
  };

  return {
    nonDiagonal: [
      'up', 'right', 'down', 'left'
    ],
    diagonal: [
      'rightUp', 'rightDown', 'leftDown', 'leftUp'
    ],
    directions8: [
      'up', 'rightUp', 'right', 'rightDown',
      'down', 'leftDown', 'left', 'leftUp'
    ],
    directionToCoord: {
      up: { x: 0, y: -1 },
      rightUp: { x: 1, y: -1 },
      right: { x: 1, y: 0 },
      rightDown: { x: 1, y: 1 },
      down: { x: 0, y: 1 },
      leftDown: { x: -1, y: 1 },
      left: { x: -1, y: 0 },
      leftUp: { x: -1, y: -1 }
    },
    noneDirectionCoord: {
      x: 0, y: 0
    },
    directionOpposites: {
      up: 'down',
      rightUp: 'leftDown',
      right: 'left',
      rightDown: 'leftUp',
      down: 'up',
      leftDown: 'rightUp',
      left: 'right',
      leftUp: 'rightDown'
    },
    isNonDiagonal: function (direction) {
      return this.nonDiagonal.indexOf(direction) > -1;
    },
    getNextNonDiagonal: function (direction) {
      return nextNonDiagonalDirections[direction];
    },
    getNextDiagonal: function (direction) {
      return nextDiagonalDirections[direction];
    },
    getPrevNonDiagonal: function (direction) {
      return prevNonDiagonalDirections[direction];
    },
    getPrevDiagonal: function (direction) {
      return prevDiagonalDirections[direction];
    }
  };

}]);