'use strict';

describe('MapBlocks', function() {
  beforeEach(module('square'));

  var MapBlocks, ArrayUtils;
  
  beforeEach(inject(function (_MapBlocks_, _ArrayUtils_) {
    MapBlocks = _MapBlocks_;
    ArrayUtils = _ArrayUtils_;
  }));

  var mapBlocks;

  describe('constructor', function () {

    it('sets blocksByPlayerAndKind to {}', function () {
      mapBlocks = new MapBlocks();
      expect(mapBlocks.blocksByPlayerAndKind).toEqual({});
    });

  });

  describe('setPlayers', function () {

    beforeEach(function () {
      mapBlocks = new MapBlocks();
    });

    it('sets blocksByPlayerAndKind for each player and kind', function () {
      mapBlocks.setPlayers({
        'fake-player-id-1': jasmine.createSpy('player-1'),
        'fake-player-id-2': jasmine.createSpy('player-2')
      });
      var blocksPlayer_1 = mapBlocks.blocksByPlayerAndKind['fake-player-id-1'];
      var blocksPlayer_2 = mapBlocks.blocksByPlayerAndKind['fake-player-id-2'];
      expect(blocksPlayer_1).toBeObject();
      expect(blocksPlayer_2).toBeObject();
      expect(Object.keys(blocksPlayer_1)).toEqual(MapBlocks.blockKinds);
      expect(Object.keys(blocksPlayer_2)).toEqual(MapBlocks.blockKinds);
      Object.keys(blocksPlayer_1).forEach(function (kind) {
        expect(blocksPlayer_1[kind]).toEqual([]);
      });
      Object.keys(blocksPlayer_2).forEach(function (kind) {
        expect(blocksPlayer_2[kind]).toEqual([]);
      });
    });

  });

  describe('addOwnedBlock', function () {

    var fakeBlock;

    beforeEach(function () {
      mapBlocks = new MapBlocks();
      mapBlocks.blocksByPlayerAndKind['fake-player-id'] = {
        fakeKind: [{}, {}], anotherFakeKind: [{}]
      };
      mapBlocks.blocksByPlayerAndKind['another-fake-player-id'] = {
        fakeKind: [{}], anotherFakeKind: []
      };
      fakeBlock = {
        ownedBy: { playerId: 'fake-player-id' },
        kind: {
          toString: function () { return 'anotherFakeKind'; }
        }
      };
    });

    it('adds the given block into the proper blocksByPlayerAndKind index', function () {
      mapBlocks.addOwnedBlock(fakeBlock);
      expect(mapBlocks.blocksByPlayerAndKind['fake-player-id'].fakeKind)
        .toEqual([{}, {}]);
      expect(mapBlocks.blocksByPlayerAndKind['fake-player-id'].anotherFakeKind)
        .toEqual([{}, fakeBlock]);
      expect(mapBlocks.blocksByPlayerAndKind['another-fake-player-id'].fakeKind)
        .toEqual([{}]);
      expect(mapBlocks.blocksByPlayerAndKind['another-fake-player-id'].anotherFakeKind)
        .toEqual([]);
    });

  });

  describe('removeOwnedBlock', function () {

    var fakeBlock;
    var fakePlayer;

    function createBlock(kindStr) {
      return {
        kind: {
          toString: function () { return kindStr; }
        }
      };
    }

    beforeEach(function () {
      fakeBlock = createBlock('anotherFakeKind');
      mapBlocks = new MapBlocks();
      mapBlocks.blocksByPlayerAndKind['fake-player-id'] = {
        fakeKind: [createBlock('fakeKind'), fakeBlock, createBlock('fakeKind')],
        anotherFakeKind: [createBlock('anotherFakeKind'), fakeBlock]
      };
      mapBlocks.blocksByPlayerAndKind['another-fake-player-id'] = {
        fakeKind: [createBlock('fakeKind')],
        anotherFakeKind: [createBlock('anotherFakeKind'),
                          createBlock('anotherFakeKind')]
      };
      fakePlayer = {
        playerId: 'fake-player-id'
      };
      spyOn(ArrayUtils, 'removeElement').and.callThrough();
    });

    it('calls ArrayUtils.removeElement', function () {
      mapBlocks.removeOwnedBlock(fakeBlock, fakePlayer);
      expect(ArrayUtils.removeElement).toHaveBeenCalledWith(
        mapBlocks.blocksByPlayerAndKind['fake-player-id'].anotherFakeKind,
        fakeBlock
      );
    });

    it('removes the given block from blocksByPlayerAndKind', function () {
      expect(mapBlocks.blocksByPlayerAndKind['fake-player-id'].anotherFakeKind.length)
        .toBe(2);
      expect(mapBlocks.blocksByPlayerAndKind['fake-player-id'].anotherFakeKind)
        .toContain(fakeBlock);
      mapBlocks.removeOwnedBlock(fakeBlock, fakePlayer);
      expect(mapBlocks.blocksByPlayerAndKind['fake-player-id'].anotherFakeKind.length)
        .toBe(1);
      expect(mapBlocks.blocksByPlayerAndKind['fake-player-id'].anotherFakeKind)
        .not.toContain(fakeBlock);
      expect(mapBlocks.blocksByPlayerAndKind['fake-player-id'].fakeKind.length)
        .toBe(3);
      expect(mapBlocks.blocksByPlayerAndKind['fake-player-id'].fakeKind)
        .toContain(fakeBlock);
      expect(mapBlocks.blocksByPlayerAndKind['another-fake-player-id'].fakeKind.length)
        .toBe(1);
      expect(mapBlocks.blocksByPlayerAndKind['another-fake-player-id'].anotherFakeKind.length)
        .toBe(2);
    });

  });

  describe('changeOwnedBlockKind', function () {

    var fakeBlock;
    var anotherFakeBlock;

    function createFakeBlock(ownerPlayerId) {
      var block = jasmine.createSpy('block');
      block.ownedBy = {
        playerId: ownerPlayerId
      };
      return block;
    }

    function createFakeKind(kindStr) {
      return {
        toString: function () {
          return kindStr;
        }
      };
    }

    beforeEach(function () {
      fakeBlock = createFakeBlock('fake-player-id');
      anotherFakeBlock = createFakeBlock('fake-player-id');
      mapBlocks = new MapBlocks();
      mapBlocks.blocksByPlayerAndKind = {
        'fake-player-id': {
          fakeKind: [fakeBlock,
                     anotherFakeBlock],
          anotherFakeKind: [anotherFakeBlock]
        },
        'another-fake-player-id': {
          fakeKind: [],
          anotherFakeKind: [createFakeBlock('fake-player-id')]
        }
      };
    });

    it('removes the given block from the old kind array', function () {
      expect(mapBlocks.blocksByPlayerAndKind['fake-player-id'].fakeKind)
        .toContain(fakeBlock);
      mapBlocks.changeOwnedBlockKind(
        fakeBlock,
        createFakeKind('fakeKind'),
        createFakeKind('anotherFakeKind'));
      expect(mapBlocks.blocksByPlayerAndKind['fake-player-id'].fakeKind)
        .not.toContain(fakeBlock);
      expect(mapBlocks.blocksByPlayerAndKind['fake-player-id'].fakeKind)
        .not.toContain(fakeBlock);
      expect(mapBlocks.blocksByPlayerAndKind['fake-player-id'].fakeKind)
        .toContain(anotherFakeBlock);
    });

    it('adds the given block to the new kind array', function () {
      expect(mapBlocks.blocksByPlayerAndKind['fake-player-id'].anotherFakeKind)
        .not.toContain(fakeBlock);
      mapBlocks.changeOwnedBlockKind(
        fakeBlock,
        createFakeKind('fakeKind'),
        createFakeKind('anotherFakeKind'));
      expect(mapBlocks.blocksByPlayerAndKind['fake-player-id'].anotherFakeKind)
        .toContain(fakeBlock);
      expect(mapBlocks.blocksByPlayerAndKind['fake-player-id'].anotherFakeKind)
        .toContain(anotherFakeBlock);
    });

  });

  describe('getBlocksByPlayerAndKind', function () {

    var mapBlocks;
    var fakePlayer;

    beforeEach(function () {
      mapBlocks = new MapBlocks();
      mapBlocks.blocksByPlayerAndKind['fake-player-id'] = {
        fakeKind: jasmine.createSpy('player 1 fakeKind'),
        anotherFakeKind: jasmine.createSpy('player 1 anotherFakeKind')
      };
      mapBlocks.blocksByPlayerAndKind['another-fake-player-id'] = {
        fakeKind: jasmine.createSpy('player 2 fakeKind'),
        anotherFakeKind: jasmine.createSpy('player 2 anotherFakeKind')
      };
      fakePlayer = { playerId: 'another-fake-player-id' };
    });

    it('returns proper blocks', function () {
      var result = mapBlocks.getBlocksByPlayerAndKind(fakePlayer, 'fakeKind');
      expect(result.and.identity()).toEqual('player 2 fakeKind');
    });

  });
  
});