'use strict';

var _RegExp = require('nwglobal').RegExp;
var EventEmitter = require('events');

angular.module('square')

.constant('coordRegExp', new _RegExp(/^\-?\d+$/))
.constant('map8DirectionRegExp', new _RegExp(/^(up|rightUp|right|rightDown|down|leftDown|left|leftUp)$/))
.constant('map4DirectionRegExp', new _RegExp(/^(up|right|down|left)$/))
.constant('mapPositionRegExp', new _RegExp(/^(top|right|bottom|left)$/))

.factory('MapSchema', ['Joi', 'Map', 'RegionSchema', 'BlockSchema', 'PlayerSchema',
    'playerIdRegExp', 'coordRegExp', 'SquaresMap', 'mapPositionRegExp',
    'MapBlocksSchema', 'RandSchema', 'RandStatefulGeneratorSchema',
    function (Joi, Map, RegionSchema, BlockSchema, PlayerSchema,
              playerIdRegExp, coordRegExp, SquaresMap, mapPositionRegExp,
              MapBlocksSchema, RandSchema, RandStatefulGeneratorSchema) {

  return Joi.object().keys({
    regions: Joi.object({})
      .pattern(coordRegExp, Joi.object({})
        .pattern(coordRegExp, RegionSchema)
      ),
    borderRegionsCoords: Joi.object({})
      .pattern(mapPositionRegExp, Joi.number().integer()),
    blockHeightPx: Joi.number().integer(),
    blockWidthPx: Joi.number().integer(),
    regionWidthPx: Joi.number().integer(),
    regionHeightPx: Joi.number().integer(),
    initSizeRegions: Joi.number().positive(),
    affordableBlockCount: Joi.object({})
      .pattern(playerIdRegExp, Joi.number().natural()),
    improvementNeighbors: Joi.object()
      .pattern(playerIdRegExp, Joi.array().items(
        BlockSchema
      )),
    improvedBlocksCounter: Joi.object()
      .pattern(playerIdRegExp, Joi.number().natural()),
    blockExhaustingThreshold: Joi.number().natural(),
    players: Joi.object({})
      .pattern(playerIdRegExp, PlayerSchema),
    aiPlayers: Joi.object({})
      .pattern(playerIdRegExp, PlayerSchema),
    fixedSize: Joi.boolean(),
    squaresMap: Joi.object().type(SquaresMap),
    windowSizePx: Joi.object().keys({
      width: Joi.number().positive(),
      height: Joi.number().positive()
    }),
    windowWidthRegions: Joi.number().positive(),
    windowHeightRegions: Joi.number().positive(),
    baseBlockMutationProbability: Joi.number().min(0).max(1.0),
    baseAutonomousBlockMutationProbability: Joi.number().min(0).max(1.0),
    maxBlockMutationProbability: Joi.number().min(0).max(1.0),
    eventEmitter: [
      Joi.object().type(EventEmitter),
      Joi.any().valid(null)
    ],
    mapDifficulty: Joi.number().min(0).max(1.0),
    mapBlocks: MapBlocksSchema,
    gameSeed: Joi.number().integer().positive(),
    mapSeed: Joi.number().integer().positive(),
    randGenerators: Joi.object().keys({
      blockKindAndRichness: RandSchema,
      captureBlockMutation: RandSchema,
      blockKindMutation: RandSchema,
      blockRichMutation: RandSchema,
      blockMutatedKind: RandSchema,
      autonomousBlockMutationGenerator: RandStatefulGeneratorSchema,
      autonomousBlockMutationCoordsGenerator: RandStatefulGeneratorSchema,
      disablingBlockDueToResourceShortage: RandSchema,
      enablingBlockDisabledDueToResourceShortage: RandSchema,
    })
  }).type(Map);

}]);