'use strict';

describe('mapProvider', function() {
  beforeEach(module('square'));

  var mapProvider, Map;
  
  beforeEach(inject(function (_mapProvider_, _Map_) {
    mapProvider = _mapProvider_;
    Map = _Map_;
  }));

  function getIdentities(arrOfSpies) {
    return arrOfSpies.map(function (spy) {
      return spy.and.identity();
    });
  }

  describe('getVisibleRegionsCoords', function () {

    beforeEach(function () {
      mapProvider._instance.map = {
        borderRegionsCoords: {
          top:  -Infinity,
          right: Infinity,
          bottom: Infinity,
          left: -Infinity
        }
      };
      mapProvider._instance.map.windowWidthRegions = 5;
      mapProvider._instance.map.windowHeightRegions = 5;
    });

    it('returns an obj = { minX: 0, maxX: 2, minY: -2, maxY: 0 } ' +
       'when given leftTopCornerRegionX = 1, leftTopCornerRegionY = -1, ' +
       'borderRegionsCoords.right = 2 and borderRegionsCoords.bottom = 0', function () {
      mapProvider._instance.map.borderRegionsCoords.right = 2;
      mapProvider._instance.map.borderRegionsCoords.bottom = 0;
      var result = mapProvider._instance.getVisibleRegionsCoords(1, -1);
      expect(result).toEqual({ minX: 0, maxX: 2, minY: -2, maxY: 0 });
    });

    it('returns an obj = { minX: -35, maxX: -35, minY: 51, maxY: 54 } ' +
       'when given leftTopCornerRegionX = -34, leftTopCornerRegionY = 52, ' +
       'borderRegionsCoords.right = -35 and borderRegionsCoords.bottom = 54', function () {
      mapProvider._instance.map.borderRegionsCoords.right = -35;
      mapProvider._instance.map.borderRegionsCoords.bottom = 54;
      var result = mapProvider._instance.getVisibleRegionsCoords(-34, 52);
      expect(result).toEqual({ minX: -35, maxX: -35, minY: 51, maxY: 54 });
    });

    it('returns an obj = { minX: 12, maxX: 18, minY: -1, maxY: 5 } ' +
       'when given leftTopCornerRegionX = 13, leftTopCornerRegionY = 0, ' +
       'borderRegionsCoords.right = 20 and borderRegionsCoords.bottom = 55', function () {
      mapProvider._instance.map.borderRegionsCoords.right = 20;
      mapProvider._instance.map.borderRegionsCoords.bottom = 55;
      var result = mapProvider._instance.getVisibleRegionsCoords(13, 0);
      expect(result).toEqual({ minX: 12, maxX: 18, minY: -1, maxY: 5 });
    });

    it('returns an obj = { minX: 13, maxX: 18, minY: -1, maxY: 5 } ' +
       'when given leftTopCornerRegionX = 13, leftTopCornerRegionY = 0, ' +
       'borderRegionsCoords.left = 13 and borderRegionsCoords.bottom = 55', function () {
      mapProvider._instance.map.borderRegionsCoords.left = 13;
      mapProvider._instance.map.borderRegionsCoords.bottom = 55;
      var result = mapProvider._instance.getVisibleRegionsCoords(13, 0);
      expect(result).toEqual({ minX: 13, maxX: 18, minY: -1, maxY: 5 });
    });

    it('returns an obj = { minX: 12, maxX: 18, minY: -1, maxY: 5 } ' +
       'when given leftTopCornerRegionX = 13, leftTopCornerRegionY = 0, ' +
       'borderRegionsCoords.left = 1 and borderRegionsCoords.bottom = 55', function () {
      mapProvider._instance.map.borderRegionsCoords.left = 1;
      mapProvider._instance.map.borderRegionsCoords.bottom = 55;
      var result = mapProvider._instance.getVisibleRegionsCoords(13, 0);
      expect(result).toEqual({ minX: 12, maxX: 18, minY: -1, maxY: 5 });
    });

    it('returns an obj = { minX: 12, maxX: 18, minY: 0, maxY: 5 } ' +
       'when given leftTopCornerRegionX = 13, leftTopCornerRegionY = 0, ' +
       'borderRegionsCoords.left = 1 and borderRegionsCoords.top = 0', function () {
      mapProvider._instance.map.borderRegionsCoords.left = 1;
      mapProvider._instance.map.borderRegionsCoords.top = 0;
      var result = mapProvider._instance.getVisibleRegionsCoords(13, 0);
      expect(result).toEqual({ minX: 12, maxX: 18, minY: 0, maxY: 5 });
    });

    it('returns an obj = { minX: 8, maxX: 9, minY: -2, maxY: -2 } ' +
       'when given leftTopCornerRegionX = 8, leftTopCornerRegionY = -2, ' +
       'borderRegionsCoords.left = 8, borderRegionsCoords.right = 9, ' +
       'borderRegionsCoords.bottom = -2 and borderRegionsCoords.top = -2', function () {
      mapProvider._instance.map.borderRegionsCoords.left = 8;
      mapProvider._instance.map.borderRegionsCoords.right = 9;
      mapProvider._instance.map.borderRegionsCoords.bottom = -2;
      mapProvider._instance.map.borderRegionsCoords.top = -2;
      var result = mapProvider._instance.getVisibleRegionsCoords(8, -2);
      expect(result).toEqual({ minX: 8, maxX: 9, minY: -2, maxY: -2 });
    });

  });

  describe('removeNotVisibleRegions', function () {

    var visibleRegions;

    function createRegion(x, y) {
      var fakeRegion = jasmine.createSpy('region ' + x + 'x' + y);
      fakeRegion.x = x;
      fakeRegion.y = y;
      visibleRegions.push(fakeRegion);
      return fakeRegion;
    }

    beforeEach(function () {
      visibleRegions = [];
      mapProvider._instance.map = {
        regions: {
          '-55': {
            '-2': createRegion(-2, -55),
            '-1': createRegion(-1, -55),
            '0': createRegion(0, -55),
            '1': createRegion(1, -55),
            '2': createRegion(2, -55)
          },
          '-54': {},
          '-53': {
            '0': createRegion(0, -53),
            '1': createRegion(1, -53)
          },
          '-52': {
            '-1': createRegion(-1, -52)
          },
          '-51': {}
        }
      };
    });

    describe('removes not visible regions', function () {

      it('for visibilityCoords: { minY: -53, maxY: -2, minX: 0, maxX: 2 }', function () {
        var visibilityCoords = { minY: -53, maxY: -2, minX: 0, maxX: 2 };
        mapProvider._instance.removeNotVisibleRegions(visibleRegions, visibilityCoords);
        expect(getIdentities(visibleRegions)).toEqual([
          'region 0x-53', 'region 1x-53'
        ]);
      });

      it('for visibilityCoords: { minY: -55, maxY: -54, minX: -3, maxX: 0 }', function () {
        var visibilityCoords = { minY: -55, maxY: -54, minX: -3, maxX: 0 };
        mapProvider._instance.removeNotVisibleRegions(visibleRegions, visibilityCoords);
        expect(getIdentities(visibleRegions)).toEqual([
          'region -2x-55', 'region -1x-55', 'region 0x-55'
        ]);
      });

      it('for visibilityCoords: { minY: -52, maxY: -52, minX: -3, maxX: 0 }', function () {
        var visibilityCoords = { minY: -52, maxY: -52, minX: -3, maxX: 0 };
        mapProvider._instance.removeNotVisibleRegions(visibleRegions, visibilityCoords);
        expect(getIdentities(visibleRegions)).toEqual([
          'region -1x-52'
        ]);
      });

      it('for visibilityCoords: { minY: -57, maxY: -51, minX: 0, maxX: 0 }', function () {
        var visibilityCoords = { minY: -57, maxY: -51, minX: 0, maxX: 0 };
        mapProvider._instance.removeNotVisibleRegions(visibleRegions, visibilityCoords);
        expect(getIdentities(visibleRegions)).toEqual([
          'region 0x-55', 'region 0x-53'
        ]);
      });

      it('for visibilityCoords: { minY: -51, maxY: -50, minX: -30, maxX: 30 }', function () {
        var visibilityCoords = { minY: -51, maxY: -50, minX: -30, maxX: 30 };
        mapProvider._instance.removeNotVisibleRegions(visibleRegions, visibilityCoords);
        expect(getIdentities(visibleRegions)).toEqual([]);
      });

    });

  });

  describe('addVisibleRegions', function () {

    var visibleRegions;

    beforeEach(function () {
      mapProvider._instance.map = {
        regions: {
          '-3': {},
          '-2': {
            '-1': jasmine.createSpy('region -1x-2'),
            '0': jasmine.createSpy('region 0x-2'),
            '1': jasmine.createSpy('region 1x-2'),
            '2': jasmine.createSpy('region 2x-2')
          },
          '-1': {
            '0': jasmine.createSpy('region 0x-1'),
            '1': jasmine.createSpy('region 1x-1'),
            '2': jasmine.createSpy('region 2x-1')
          },
          '0': {
            '-1': jasmine.createSpy('region -1x0')
          },
          '1': {}
        }
      };
      visibleRegions = [];
    });

    describe('adds regions to visibleRegions param according to', function () {

      it('visibilityCoords: { minY: -2, maxY: 0, minX: 0, maxX: 2 }', function () {
        var visibilityCoords = { minY: -2, maxY: 0, minX: 0, maxX: 2 };
        mapProvider._instance.addVisibleRegions(visibleRegions, visibilityCoords);
        expect(getIdentities(visibleRegions)).toEqual([
          'region 0x-2', 'region 1x-2', 'region 2x-2',
          'region 0x-1', 'region 1x-1', 'region 2x-1'
        ]);
      });

      it('visibilityCoords: { minY: -3, maxY: 0, minX: 0, maxX: 2 }', function () {
        var visibilityCoords = { minY: -3, maxY: 0, minX: 0, maxX: 2 };
        mapProvider._instance.addVisibleRegions(visibleRegions, visibilityCoords);
        expect(getIdentities(visibleRegions)).toEqual([
          'region 0x-2', 'region 1x-2', 'region 2x-2',
          'region 0x-1', 'region 1x-1', 'region 2x-1'
        ]);
      });

      it('visibilityCoords: { minY: -1, maxY: 1, minX: -1, maxX: 0 }', function () {
        var visibilityCoords = { minY: -1, maxY: 1, minX: -1, maxX: 0 };
        mapProvider._instance.addVisibleRegions(visibleRegions, visibilityCoords);
        expect(getIdentities(visibleRegions)).toEqual(['region 0x-1', 'region -1x0']);
      });

      it('visibilityCoords: { minY: -2, maxY: -2, minX: -5, maxX: 5 }', function () {
        var visibilityCoords = { minY: -2, maxY: -2, minX: -5, maxX: 5 };
        mapProvider._instance.addVisibleRegions(visibleRegions, visibilityCoords);
        expect(getIdentities(visibleRegions)).toEqual([
          'region -1x-2', 'region 0x-2', 'region 1x-2', 'region 2x-2'
        ]);
      });

      it('visibilityCoords: { minY: -3, maxY: -3, minX: 0, maxX: 2 }', function () {
        var visibilityCoords = { minY: -3, maxY: -3, minX: 0, maxX: 2 };
        mapProvider._instance.addVisibleRegions(visibleRegions, visibilityCoords);
        expect(getIdentities(visibleRegions)).toEqual([]);
      });

      it('visibilityCoords: { minY: -3, maxY: 0, minX: 8, maxX: 10 }', function () {
        var visibilityCoords = { minY: -3, maxY: 0, minX: 8, maxX: 10 };
        mapProvider._instance.addVisibleRegions(visibleRegions, visibilityCoords);
        expect(getIdentities(visibleRegions)).toEqual([]);
      });

      it('visibilityCoords: { minY: -3, maxY: 0, minX: -1, maxX: -1 }', function () {
        var visibilityCoords = { minY: -3, maxY: 0, minX: -1, maxX: -1 };
        mapProvider._instance.addVisibleRegions(visibleRegions, visibilityCoords);
        expect(getIdentities(visibleRegions)).toEqual(['region -1x-2', 'region -1x0']);
      });

    });

    it('does not add a region if it is already added to visibleRegions', function () {
      var visibilityCoords = { minY: -2, maxY: -2, minX: -5, maxX: 5 };
      visibleRegions.push(mapProvider._instance.map.regions['-2']['-1']);
      visibleRegions.push(mapProvider._instance.map.regions['-2']['1']);
      expect(getIdentities(visibleRegions)).toEqual([
        'region -1x-2', 'region 1x-2'
      ]);
      mapProvider._instance.addVisibleRegions(visibleRegions, visibilityCoords);
      expect(getIdentities(visibleRegions)).toEqual([
        'region -1x-2', 'region 1x-2', 'region 0x-2', 'region 2x-2'
      ]);
    });

  });

  describe('initMap', function () {

    let fakePlayers;
    let fakeMapDifficulty;
    let fakeGameSeed;
    let fakeMapSeed;
    let fakeMapSize;

    beforeEach(function () {
      spyOn(Map.prototype, 'initRegions');
      spyOn(Map.prototype, 'initAllBlocksNeighborsAndTmpAiValues');
      spyOn(Map.prototype, 'initAllBlocksBaseAiValues');
      spyOn(Map.prototype, 'setPlayers');
      spyOn(Map.prototype, 'setEventEmitter');
      spyOn(Map.prototype, 'setMapBlocks');
      spyOn(Map.prototype, 'setRandomness');
      mapProvider._instance.eventEmitter = jasmine.createSpyObj('eventEmitter', ['emit']);
      spyOn(mapProvider._instance, 'map');
      fakeGameSeed = jasmine.createSpy('gameSeed');
      fakeMapSeed = jasmine.createSpy('mapSeed');
      fakePlayers = jasmine.createSpy('players');
      fakeMapDifficulty = jasmine.createSpy('mapDifficulty');
      fakeMapSize = jasmine.createSpy('mapSize');
    });

    it('creates a new instance of Map and sets it to `map` prop', function () {
      mapProvider.initMap(fakeGameSeed, fakeMapSeed, fakePlayers,
                          fakeMapDifficulty, fakeMapSize);
      expect(mapProvider._instance.map instanceof Map).toBeTrue();
      expect(mapProvider._instance.map.mapDifficulty).toBe(fakeMapDifficulty);
    });

    it('calls setEventEmitter on map', function () {
      mapProvider.initMap(fakeGameSeed, fakeMapSeed, fakePlayers,
                          fakeMapDifficulty, fakeMapSize);
      expect(mapProvider._instance.map.setEventEmitter).toHaveBeenCalledWith();
    });

    it('calls setRandomness on map', function () {
      mapProvider.initMap(fakeGameSeed, fakeMapSeed, fakePlayers,
                          fakeMapDifficulty, fakeMapSize);
      expect(mapProvider._instance.map.setRandomness)
        .toHaveBeenCalledWith(fakeGameSeed, fakeMapSeed);
    });

    it('calls initRegions on map', function () {
      mapProvider.initMap(fakeGameSeed, fakeMapSeed, fakePlayers,
                          fakeMapDifficulty, fakeMapSize);
      expect(mapProvider._instance.map.initRegions)
        .toHaveBeenCalledWith({ mapSize: fakeMapSize });
    });

    it('calls initAllBlocksNeighborsAndTmpAiValues on map', function () {
      mapProvider.initMap(fakeGameSeed, fakeMapSeed, fakePlayers,
                          fakeMapDifficulty, fakeMapSize);
      expect(mapProvider._instance.map.initAllBlocksNeighborsAndTmpAiValues)
        .toHaveBeenCalledWith();
    });

    it('calls initAllBlocksBaseAiValues on map', function () {
      mapProvider.initMap(fakeGameSeed, fakeMapSeed, fakePlayers,
                          fakeMapDifficulty, fakeMapSize);
      expect(mapProvider._instance.map.initAllBlocksBaseAiValues).toHaveBeenCalledWith();
    });

    it('calls setPlayers on map', function () {
      mapProvider.initMap(fakeGameSeed, fakeMapSeed, fakePlayers,
                          fakeMapDifficulty, fakeMapSize);
      expect(mapProvider._instance.map.setPlayers).toHaveBeenCalledWith(fakePlayers);
    });

    it('calls setMapBlocks on map', function () {
      mapProvider.initMap(fakeGameSeed, fakeMapSeed, fakePlayers,
                          fakeMapDifficulty, fakeMapSize);
      expect(mapProvider._instance.map.setMapBlocks).toHaveBeenCalledWith(fakePlayers);
    });

    it('emits mapInited', function () {
      mapProvider.initMap(fakeGameSeed, fakeMapSeed, fakePlayers,
                          fakeMapDifficulty, fakeMapSize);
      expect(mapProvider._instance.eventEmitter.emit).toHaveBeenCalledWith('mapInited', mapProvider._instance.map);
    });

  });

  describe('loadMap', function () {

    var fakeMap;

    beforeEach(function () {
      mapProvider._instance.eventEmitter = jasmine.createSpyObj(
        'eventEmitter', ['emit']);
      spyOn(mapProvider._instance, 'map');
      fakeMap = jasmine.createSpyObj(
        'map', ['setEventEmitter', 'setPlayersEventListeners',
                'players', 'loadRandomness']);
    });

    it('sets the given map to `map` prop', function () {
      mapProvider.loadMap(fakeMap);
      expect(mapProvider._instance.map).toBe(fakeMap);
    });

    it('calls setEventEmitter on map', function () {
      mapProvider.loadMap(fakeMap);
      expect(fakeMap.setEventEmitter).toHaveBeenCalledWith();
    });

    it('calls setPlayersEventListeners on map', function () {
      mapProvider.loadMap(fakeMap);
      expect(fakeMap.setPlayersEventListeners).toHaveBeenCalledWith(fakeMap.players);
    });

    it('emits mapLoaded', function () {
      mapProvider.loadMap(fakeMap);
      expect(mapProvider._instance.eventEmitter.emit).toHaveBeenCalledWith('mapLoaded', fakeMap);
    });

  });

  describe('getMap', function () {

    var mapSpy;

    beforeEach(function () {
      mapSpy = spyOn(mapProvider._instance, 'map');
    });

    it('returns map', function () {
      var result = mapProvider.getMap();
      expect(result).toBeDefined();
      expect(result).toBe(mapSpy);
    });

  });

  describe('setSquaresMap', function () {

    var fakeSquaresMap;

    beforeEach(function () {
      mapProvider._instance.map = jasmine.createSpyObj('map', ['setSquaresMap']);
      fakeSquaresMap = jasmine.createSpy('squaresMap');
    });

    it('sets given squaresMap on map', function () {
      mapProvider.setSquaresMap(fakeSquaresMap);
      expect(mapProvider._instance.map.setSquaresMap).toHaveBeenCalledWith(fakeSquaresMap);
    });

  });

  describe('getRegionWidth', function () {

    beforeEach(function () {
      mapProvider._instance.map = jasmine.createSpyObj('map', ['regionWidthPx']);
    });
    
    it('returns map regionWidthPx', function () {
      var result = mapProvider.getRegionWidth();
      expect(result).toBe(mapProvider._instance.map.regionWidthPx);
    });

  });

  describe('getRegionHeight', function () {

    beforeEach(function () {
      mapProvider._instance.map = jasmine.createSpyObj('map', ['regionHeightPx']);
    });
    
    it('returns map regionHeightPx', function () {
      var result = mapProvider.getRegionHeight();
      expect(result).toBe(mapProvider._instance.map.regionHeightPx);
    });

  });

  describe('getBlockWidth', function () {

    beforeEach(function () {
      mapProvider._instance.map = jasmine.createSpyObj('map', ['blockWidthPx']);
    });

    it('returns map blockWidthPx', function () {
      var result = mapProvider.getBlockWidth();
      expect(result).toBe(mapProvider._instance.map.blockWidthPx);
    });

  });

  describe('getBlockHeight', function () {

    beforeEach(function () {
      mapProvider._instance.map = jasmine.createSpyObj('map', ['blockHeightPx']);
    });

    it('returns map blockHeightPx', function () {
      var result = mapProvider.getBlockHeight();
      expect(result).toBe(mapProvider._instance.map.blockHeightPx);
    });

  });

  describe('updateVisibleRegions', function () {

    var fakeLeftTopCornerRegionX;
    var fakeLeftTopCornerRegionY;
    var visibleRegions;
    var getVisibleRegionsCoordsResult;

    beforeEach(function () {
      getVisibleRegionsCoordsResult = jasmine.createSpy('getVisibleRegionsCoords');
      spyOn(mapProvider._instance, 'getVisibleRegionsCoords')
        .and.returnValue(getVisibleRegionsCoordsResult);
      fakeLeftTopCornerRegionX = jasmine.createSpy('leftTopCornerRegionX');
      fakeLeftTopCornerRegionY = jasmine.createSpy('leftTopCornerRegionY');
      visibleRegions = jasmine.createSpy('visibleRegions');
      spyOn(mapProvider._instance, 'removeNotVisibleRegions');
      spyOn(mapProvider._instance, 'addVisibleRegions');
    });

    it('calls getVisibleRegionsCoords', function () {
      mapProvider.updateVisibleRegions(
        visibleRegions, fakeLeftTopCornerRegionX, fakeLeftTopCornerRegionY);
      expect(mapProvider._instance.getVisibleRegionsCoords)
        .toHaveBeenCalledWith(fakeLeftTopCornerRegionX, fakeLeftTopCornerRegionY);
    });

    it('calls removeNotVisibleRegions', function () {
      mapProvider.updateVisibleRegions(
        visibleRegions, fakeLeftTopCornerRegionX, fakeLeftTopCornerRegionY);
      expect(mapProvider._instance.removeNotVisibleRegions)
        .toHaveBeenCalledWith(visibleRegions, getVisibleRegionsCoordsResult);
    });

    it('calls addVisibleRegions', function () {
      mapProvider.updateVisibleRegions(
        visibleRegions, fakeLeftTopCornerRegionX, fakeLeftTopCornerRegionY);
      expect(mapProvider._instance.addVisibleRegions)
        .toHaveBeenCalledWith(visibleRegions, getVisibleRegionsCoordsResult);
    });

  });

  describe('normalizeCoords', function () {

    beforeEach(function () {
        mapProvider._instance.map = jasmine.createSpyObj('map', ['normalizeCoords']);
    });

    it('calls normalizeCoords on map', function () {
      var fakeRegionX = jasmine.createSpy('regionX');
      var fakeRegionY = jasmine.createSpy('regionY');
      var fakeBlockX = jasmine.createSpy('blockX');
      var fakeBlockY = jasmine.createSpy('blockY');
      mapProvider.normalizeCoords(fakeRegionX, fakeRegionY, fakeBlockX, fakeBlockY);
      expect(mapProvider._instance.map.normalizeCoords)
        .toHaveBeenCalledWith(fakeRegionX, fakeRegionY, fakeBlockX, fakeBlockY);
    });

  });

  describe('getBlock', function () {

    beforeEach(function () {
        mapProvider._instance.map = jasmine.createSpyObj('map', ['getBlock']);
    });

    it('calls getBlock on map', function () {
      var fakeRegionX = jasmine.createSpy('regionX');
      var fakeRegionY = jasmine.createSpy('regionY');
      var fakeBlockX = jasmine.createSpy('blockX');
      var fakeBlockY = jasmine.createSpy('blockY');
      mapProvider.getBlock(fakeRegionX, fakeRegionY, fakeBlockX, fakeBlockY);
      expect(mapProvider._instance.map.getBlock)
        .toHaveBeenCalledWith(fakeRegionX, fakeRegionY, fakeBlockX, fakeBlockY);
    });

  });

  describe('getBlocks', function () {

    beforeEach(function () {
        mapProvider._instance.map = jasmine.createSpyObj('map', ['getBlocks']);
    });

    it('calls getBlocks on map', function () {
      var fakeRegionX = jasmine.createSpy('regionX');
      var fakeRegionY = jasmine.createSpy('regionY');
      var fakeBlockX = jasmine.createSpy('blockX');
      var fakeBlockY = jasmine.createSpy('blockY');
      var fakeWidth = jasmine.createSpy('width');
      var fakeHeight = jasmine.createSpy('height');
      mapProvider.getBlocks(fakeRegionX, fakeRegionY, fakeBlockX, fakeBlockY, fakeWidth, fakeHeight);
      expect(mapProvider._instance.map.getBlocks)
        .toHaveBeenCalledWith(fakeRegionX, fakeRegionY, fakeBlockX, fakeBlockY, fakeWidth, fakeHeight);
    });

  });

});