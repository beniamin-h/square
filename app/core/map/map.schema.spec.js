'use strict';

describe('MapSchema', function() {
  beforeEach(module('square'));

  var Region, mapProvider, storageProvider, initProvider,
    Plain, playerProvider, Joi, MapSchema, squareProvider;

  beforeEach(inject(function (_Region_, _mapProvider_, _storageProvider_, _initProvider_,
                              _Plain_, _playerProvider_, _Joi_, _MapSchema_, _squareProvider_) {
    Region = _Region_;
    mapProvider = _mapProvider_;
    storageProvider = _storageProvider_;
    initProvider = _initProvider_;

    Plain = _Plain_;
    playerProvider = _playerProvider_;
    Joi = _Joi_;
    MapSchema = _MapSchema_;
    squareProvider = _squareProvider_;
  }));

  describe('validates against its schema', function () {

    var player;
    var map;

    function getOneSample (array) {
      return array[Math.floor(array.length * Math.random())];
    }

    function getPlains (regionX, regionY) {
      return map.getBlocks(regionX, regionY, 0, 0, Region.blocksWidth, Region.blocksHeight)
        .filter(function (block) { return block.kind instanceof Plain; });
    }

    function validateMap(done) {
      Joi.validate(map, MapSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    }

    beforeEach(function () {
      initProvider.initNewGame();
      player = playerProvider.getHuman();
      map = mapProvider.getMap();
    });

    it('right after game initializing', function (done) {
      validateMap(done);
    });

    it('when it is a map with 1 block improved', function (done) {
      storageProvider.getStorage(player).put({wood: 100});
      var plainBlock = getOneSample(getPlains(0, 0));
      map.baseBlockMutationProbability = 0;
      map.tryToCaptureBlock(plainBlock, player);
      expect(plainBlock.level).toBe(1);
      validateMap(done);
    });

    it('when it is a map with 1 square', function (done) {
      squareProvider.getSquareFactory().saveSquare(
        squareProvider.getSquareFactory().createSquare(0, 0, 5, 4, 2, player));
      validateMap(done);
    });

    it('when it is a map with a newly generated region after a block capturing', function (done) {
      var oldBorderRegionsCoordsTop = map.borderRegionsCoords.top;
      var oldBorderRegionsCoordsLeft = map.borderRegionsCoords.left;
      storageProvider.getStorage(player).put({wood: 100});
      var plainBlock = getOneSample(getPlains(
        map.borderRegionsCoords.top, map.borderRegionsCoords.left));
      map.baseBlockMutationProbability = 0;
      map.tryToCaptureBlock(plainBlock, player);
      expect(plainBlock.level).toBe(1);
      expect(map.borderRegionsCoords.top).toBe(oldBorderRegionsCoordsTop - 1);
      expect(map.borderRegionsCoords.left).toBe(oldBorderRegionsCoordsLeft - 1);
      validateMap(done);
    });

  });
});