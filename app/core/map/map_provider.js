'use strict';

var EventEmitter = require('events');

angular.module('square').factory('mapProvider', ['Map', 'Math',
    function (Map, Math) {

  var that = this;
  that.eventEmitter = new EventEmitter();
  that.eventEmitter.setMaxListeners(1 << 20);  // 2^20
  that.map = null;

  that.getVisibleRegionsCoords = function (leftTopCornerRegionX, leftTopCornerRegionY) {
    return {
      minX: Math.max(leftTopCornerRegionX - 1, that.map.borderRegionsCoords.left),
      maxX: Math.min(leftTopCornerRegionX + that.map.windowWidthRegions, that.map.borderRegionsCoords.right),
      minY: Math.max(leftTopCornerRegionY - 1, that.map.borderRegionsCoords.top),
      maxY: Math.min(leftTopCornerRegionY + that.map.windowHeightRegions, that.map.borderRegionsCoords.bottom)
    };
  };

  that.removeNotVisibleRegions = function (visibleRegions, visibilityCoords) {
    var regionIndexesToRemove = [];
    visibleRegions.forEach(function (region, idx) {
      if (region.x < visibilityCoords.minX || region.x > visibilityCoords.maxX ||
          region.y < visibilityCoords.minY || region.y > visibilityCoords.maxY) {
        regionIndexesToRemove.push(idx);
      }
    });
    for (var i = regionIndexesToRemove.length - 1; i >= 0; i--) {
      visibleRegions.splice(regionIndexesToRemove[i], 1);
    }
  };

  that.addVisibleRegions = function (visibleRegions, visibilityCoords) {
    for (var y = visibilityCoords.minY; y <= visibilityCoords.maxY; y++) {
      for (var x = visibilityCoords.minX; x <= visibilityCoords.maxX; x++) {
        if (that.map.regions[y][x] && visibleRegions.indexOf(that.map.regions[y][x]) === -1) {
          visibleRegions.push(that.map.regions[y][x]);
        }
      }
    }
  };

  return {
    eventEmitter: that.eventEmitter,
    initMap: function (gameSeed, mapSeed, players, mapDifficulty, mapSize) {
      var map = new Map(mapDifficulty);
      map.setEventEmitter();
      map.setRandomness(gameSeed, mapSeed);
      map.initRegions({ mapSize });
      map.initAllBlocksNeighborsAndTmpAiValues();
      map.initAllBlocksBaseAiValues();
      map.setPlayers(players);
      map.setMapBlocks(players);
      that.map = map;
      that.eventEmitter.emit('mapInited', that.map);
    },
    loadMap: function (map) {
      that.map = map;
      that.map.setPlayersEventListeners(map.players);
      that.map.setEventEmitter();
      that.eventEmitter.emit('mapLoaded', that.map);
    },
    getMap: function () {
      return that.map;
    },
    setSquaresMap: function (squaresMap) {
      that.map.setSquaresMap(squaresMap);
    },
    getRegionWidth: function () {
      return that.map.regionWidthPx;
    },
    getRegionHeight: function () {
      return that.map.regionHeightPx;
    },
    getBlockWidth: function () {
      return that.map.blockWidthPx;
    },
    getBlockHeight: function () {
      return that.map.blockHeightPx;
    },
    updateVisibleRegions: function (visibleRegions, leftTopCornerRegionX, leftTopCornerRegionY) {
      var coords = that.getVisibleRegionsCoords(leftTopCornerRegionX, leftTopCornerRegionY);
      that.removeNotVisibleRegions(visibleRegions, coords);
      that.addVisibleRegions(visibleRegions, coords);
    },
    getVisibleSquares: function () {
      var squares = [];
      Object.keys(that.map.squaresMap.squares).forEach(function (playerId) {
        Object.keys(that.map.squaresMap.squares[playerId]).forEach(function (squareLevel) {
          var squaresByRegions = that.map.squaresMap.squares[playerId][squareLevel];
          Object.keys(squaresByRegions).forEach(function (regionY) {
            var squaresByRegionRow = that.map.squaresMap.squares[playerId][squareLevel][regionY];
            Object.keys(squaresByRegionRow).forEach(function (regionX) {
              var squaresByBlocks = that.map.squaresMap.squares[playerId][squareLevel][regionY][regionX];
              Object.keys(squaresByBlocks).forEach(function (blockY) {
                var squaresByBlockRow = that.map.squaresMap.squares[playerId][squareLevel][regionY][regionX][blockY];
                Object.keys(squaresByBlockRow).forEach(function (blockX) {
                  var square = squaresByBlockRow[blockX];
                  squares.push(square);
                });
              });
            });
          });
        });
      });
      return squares;
    },
    normalizeCoords: function (regionX, regionY, blockX, blockY) {
      return that.map.normalizeCoords(regionX, regionY, blockX, blockY);
    },
    getBlock: function (regionX, regionY, blockX, blockY) {
      return that.map.getBlock(regionX, regionY, blockX, blockY);
    },
    getBlocks: function (regionX, regionY, blockX, blockY, width, height) {
      return that.map.getBlocks(regionX, regionY, blockX, blockY, width, height);
    },
    _instance: this
  };
}]);
