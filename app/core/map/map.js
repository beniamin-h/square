'use strict';

var EventEmitter = require('events');

angular.module('square').factory('Map', [
      'Region', 'Square', 'storageProvider', 'AIPlayer', 'Math', 'ArrayUtils',
      'mapDirections', 'MapBlocks', 'Rand', 'turnTicker',
    function (Region, Square, storageProvider, AIPlayer, Math, ArrayUtils,
              mapDirections, MapBlocks, Rand, turnTicker) {

  function Map(mapDifficulty) {
    this.regions = {};
    this.improvedBlocksCounter = {};
    this.improvementNeighbors = {};
    this.affordableBlockCount = {};
    this.setRegionWidthHeightPx();
    this.setWindowWidthHeightRegions();
    this.borderRegionsCoords = {
      top: 0,
      right: 0,
      bottom: 0,
      left: 0
    };
    this.mapDifficulty = mapDifficulty === undefined ? 0.5 : mapDifficulty;  //TODO: test & remove from constructor
  }

  Map.prototype.regions = null;
  Map.prototype.borderRegionsCoords = null;
  Map.prototype.blockWidthPx = 50;
  Map.prototype.blockHeightPx = 50;
  Map.prototype.mapWindowWidthHeightDifferencePx = 200;  //TODO: remove it
  Map.prototype.regionWidthPx = 0; // Loaded on init
  Map.prototype.regionHeightPx = 0; // Loaded on init
  Map.prototype.initSizeRegions = 11;
  Map.prototype.affordableBlockCount = null;
  Map.prototype.improvementNeighbors = null;
  Map.prototype.improvedBlocksCounter = null;
  Map.prototype.blockExhaustingThreshold = 150;
  Map.prototype.players = null;
  Map.prototype.aiPlayers = null;
  Map.prototype.fixedSize = false;
  Map.prototype.squaresMap = null;
  Map.prototype.windowSizePx = {  //TODO: remove it
    width: 1000,
    height: 800
  };
  Map.prototype.windowWidthRegions = 0;  //TODO: remove it
  Map.prototype.windowHeightRegions = 0;  //TODO: remove it
  Map.prototype.baseBlockMutationProbability = 0.015;
  Map.prototype.baseAutonomousBlockMutationProbability = 0.005;
  Map.prototype.maxBlockMutationProbability = 0.06;
  Map.prototype.eventEmitter = null;
  Map.prototype.mapDifficulty = 0.0;
  Map.prototype.mapBlocks = null;
  Map.prototype.gameSeed = 0;
  Map.prototype.mapSeed = 0;
  Map.prototype.randGenerators = null;

  Map.prototype._setGameSeed = function (gameSeed) {
    this.gameSeed = gameSeed;
  };

  Map.prototype._setMapSeed = function (mapSeed) {
    if (mapSeed === null) {
      var rand = new Rand('Map seed', this.gameSeed);
      this.mapSeed = rand.getInt31Unsigned('Map seed');
    } else {
      this.mapSeed = mapSeed;
    }
  };

  Map.prototype._setRandGenerators = function () {
    this.randGenerators = {
      blockKindAndRichness: new Rand('Map.blockKindAndRichness'),
      captureBlockMutation: new Rand('Map.captureBlockMutation', this.gameSeed),
      blockKindMutation: new Rand('Map.blockKindMutation', this.gameSeed),
      blockRichMutation: new Rand('Map.blockRichMutation', this.gameSeed),
      blockMutatedKind: new Rand('Map.blockMutatedKind', this.gameSeed),
      autonomousBlockMutationGenerator:
        new Rand('Map.autonomousBlockMutation', this.gameSeed)
          .getStatefulGenerator('autonomousBlockMutationGenerator'),
      autonomousBlockMutationCoordsGenerator:
        new Rand('Map.autonomousBlockMutationCoords', this.gameSeed)
          .getStatefulGenerator('autonomousBlockMutationCoordsGenerator'),
      disablingBlockDueToResourceShortage:
        new Rand('Map.disablingBlockDueToResourceShortage', this.gameSeed),
      enablingBlockDisabledDueToResourceShortage:
        new Rand('Map.enablingBlockDisabledDueToResourceShortage', this.gameSeed)
    };
  };

  Map.prototype.setRandomness = function (gameSeed, mapSeed) {
    this._setGameSeed(gameSeed);
    this._setMapSeed(mapSeed);
    this._setRandGenerators();
  };

  Map.prototype.setMapBlocks = function (players) {
    this.mapBlocks = new MapBlocks();
    this.mapBlocks.setPlayers(players);
  };

  Map.prototype.setEventEmitter = function () {
    this.eventEmitter = new EventEmitter();
    this.eventEmitter.setMaxListeners(1 << 20);  // 2^20
  };

  Map.prototype.setSquaresMap = function (squaresMap) {
    this.squaresMap = squaresMap;
  };

  Map.prototype.setRegionWidthHeightPx = function () {
    this.regionWidthPx = this.blockWidthPx * Region.blocksWidth;
    this.regionHeightPx = this.blockHeightPx * Region.blocksHeight;
  };

  Map.prototype.setWindowWidthHeightRegions = function () {
    this.windowWidthRegions = Math.ceil(this.windowSizePx.width / this.regionWidthPx);
    this.windowHeightRegions = Math.ceil(this.windowSizePx.height / this.regionHeightPx);
  };

  Map.prototype.initRegions = function ({
      populateWithBlocks=true,
      mapSize=this.initSizeRegions
    } = {}) {
    populateWithBlocks = populateWithBlocks === undefined ? true : populateWithBlocks;
    var minY = parseInt(-Math.floor(mapSize / 2.0), 10);
    var maxY = parseInt(Math.floor(mapSize / 2.0), 10);
    var minX = parseInt(-Math.floor(mapSize / 2.0), 10);
    var maxX = parseInt(Math.floor(mapSize / 2.0), 10);
    for (var y = minY; y <= maxY; y++) {
      for (var x = minX; x <= maxX; x++) {
        this.generateRegion(x, y, populateWithBlocks);
      }
    }
  };

  Map.prototype.generateRegion = function (x, y, populateWithBlocks) {
    this.regions[y] = this.regions[y] || {};
    var region = new Region(x, y);
    if (populateWithBlocks) {
      region.populateWithBlocks(
        this.randGenerators.blockKindAndRichness.getGenerator(this.mapSeed, x, y),
        this.mapDifficulty
      );
    }
    this.regions[y][x] = region;
    this.borderRegionsCoords.top = Math.min(this.borderRegionsCoords.top, y);
    this.borderRegionsCoords.right = Math.max(this.borderRegionsCoords.right, x);
    this.borderRegionsCoords.bottom = Math.max(this.borderRegionsCoords.bottom, y);
    this.borderRegionsCoords.left = Math.min(this.borderRegionsCoords.left, x);
    return region;
  };

  Map.prototype.ensureAdjacentRegionsExistence = function (region) {
    var newRegions = new Set();
    mapDirections.directions8.forEach(function (direction) {
      var coords = mapDirections.directionToCoord[direction];
      if (!this.getRegionByXY(region.x + coords.x, region.y + coords.y)) {
        newRegions.add(this.generateRegion(region.x + coords.x, region.y + coords.y, true));
      }
    }, this);
    if (newRegions.size) {
      this._setNewRegionsBlocksNeighborsAndAiValues(newRegions);
    }
    return newRegions;
  };

  Map.prototype._setNewRegionsBlocksNeighborsAndAiValues = function (newRegions) {
    var newRegionsNeighbors = this._getRegionsNeighbors(newRegions);
    this._setRegionsBlocksNeighborsAndTmpAiValues(newRegions);
    this._resetRegionsBlocksNeighborsAndTmpAiValues(newRegionsNeighbors);
    this._setRegionsBlocksBaseAiValues(newRegions);
    this._updateRegionsBlocksBaseAiValue(newRegionsNeighbors);
  };

  Map.prototype._getRegionsNeighbors = function (regions) {
    var allNeighboringRegions = new Set();
    var that = this;
    regions.forEach(function (region) {
      that._getNeighboringRegions(region).forEach(function (neighboringRegion) {
        if (!regions.has(neighboringRegion)) {
          allNeighboringRegions.add(neighboringRegion);
        }
      });
    });
    return allNeighboringRegions;
  };

  Map.prototype._getNeighboringRegions = function (region) {
    var that = this;
    var neighboringRegions = new Set();
    mapDirections.directions8.forEach(function (direction) {
      var coords = mapDirections.directionToCoord[direction];
      var neighboringRegion = that.getRegionByXY(region.x + coords.x, region.y + coords.y);
      if (neighboringRegion) {
        neighboringRegions.add(neighboringRegion);
      }
    });
    return neighboringRegions;
  };

  Map.prototype._setRegionsBlocksNeighborsAndTmpAiValues = function (regions) {
    var that = this;
    regions.forEach(function (region) {
      region.blocks.forEach(function (row) {
        row.forEach(that.initBlockNeighborsAndTmpAiValue, that);
      });
    });
  };

  Map.prototype._resetRegionsBlocksNeighborsAndTmpAiValues = function (regions) {
    var that = this;
    regions.forEach(function (region) {
      region.blocks.forEach(function (row) {
        row.forEach(function (block) {
          block.resetNeighborsByKind();
          that.initBlockNeighborsAndTmpAiValue(block);
        });
      });
    });
  };

  Map.prototype._setRegionsBlocksBaseAiValues = function (regions) {
    var that = this;
    var aiPlayers = Object.values(that.aiPlayers);
    regions.forEach(function (region) {
      region.blocks.forEach(function (row) {
        row.forEach(function (block) {
          that.setBlockBaseAiValue(block);
          aiPlayers.forEach(function (aiPlayer) {
            that.setBlockAiValueForPlayer(block, aiPlayer);
          });
        });
      });
    });
  };

  Map.prototype._updateRegionsBlocksBaseAiValue = function (regions) {
    var that = this;
    regions.forEach(function (region) {
      region.blocks.forEach(function (row) {
        row.forEach(that.updateBlockBaseAiValue, that);
      });
    });
  };

  Map.prototype.getRegionByXY = function (x, y) {
    if (y < this.borderRegionsCoords.top || x > this.borderRegionsCoords.right ||
        y > this.borderRegionsCoords.bottom || x < this.borderRegionsCoords.left) {
      return null;
    }
    return this.regions[y][x] || null;
  };

  Map.prototype.getDistance = function (blockA, blockB) {
    var rDistX = blockA.region.x - blockB.region.x;
    var rDistY = blockA.region.y - blockB.region.y;
    var distX = blockA.x - blockB.x;
    var distY = blockA.y - blockB.y;
    return {
      x: -(rDistX * Region.blocksWidth + distX),
      y: -(rDistY * Region.blocksHeight + distY)
    };
  };

  Map.prototype.normalizeCoords = function (regionX, regionY, blockX, blockY) {
    regionX = parseInt(regionX);
    regionY = parseInt(regionY);
    if (blockX < 0) {
      regionX -= Math.ceil(-blockX / Region.blocksWidth);
      blockX = Region.blocksWidth - (-blockX % Region.blocksWidth);
    }
    if (blockY < 0) {
      regionY -= Math.ceil(-blockY / Region.blocksHeight);
      blockY = Region.blocksHeight - (-blockY % Region.blocksHeight);
    }
    if (blockX >= Region.blocksWidth) {
      regionX += Math.floor(blockX / Region.blocksWidth);
      blockX = blockX % Region.blocksWidth;
    }
    if (blockY >= Region.blocksHeight) {
      regionY += Math.floor(blockY / Region.blocksHeight);
      blockY = blockY % Region.blocksHeight;
    }

    return {
      regionX: regionX,
      regionY: regionY,
      blockX: blockX,
      blockY: blockY
    };
  };

  Map.prototype.getBlockAdjacentNeighbors = function (block) {
    return [
      this.getBlock(block.region.x, block.region.y, block.x, block.y - 1), // up
      this.getBlock(block.region.x, block.region.y, block.x + 1, block.y), // right
      this.getBlock(block.region.x, block.region.y, block.x, block.y + 1), // down
      this.getBlock(block.region.x, block.region.y, block.x - 1, block.y) // left
    ].filter(function (block) {
      return !!block;
    });
  };

  Map.prototype.getBlockAllNeighborsWithPositions = function (block) {
    var neighbors = {};
    for (var direction in mapDirections.directionToCoord) {
      var neighbor = this.getBlock(block.region.x, block.region.y,
        block.x + mapDirections.directionToCoord[direction].x,
        block.y + mapDirections.directionToCoord[direction].y);
      if (neighbor) {
        neighbors[direction] = neighbor;
      }
    }
    return neighbors;
  };

  Map.prototype.getBlock = function (regionX, regionY, blockX, blockY) {
    var coords = this.normalizeCoords(regionX, regionY, blockX, blockY);
    if (this.borderRegionsCoords.top > coords.regionY ||
        this.borderRegionsCoords.right < coords.regionX ||
        this.borderRegionsCoords.bottom < coords.regionY ||
        this.borderRegionsCoords.left > coords.regionX) {
      return null;
    }
    return this.regions[coords.regionY][coords.regionX] ?
           this.regions[coords.regionY][coords.regionX].blocks[coords.blockY][coords.blockX] :
           null;
  };

  Map.prototype.getBlocks = function (regionX, regionY, blockX, blockY, width, height) {
    var blocks = [];
    for (var y = 0; y < height; y++) {
      for (var x = 0; x < width; x++) {
        var block = this.getBlock(regionX, regionY, blockX + x, blockY + y);
        if (block) {
          blocks.push(block);
        }
      }
    }
    return blocks;
  };

  Map.prototype.setBlockHasImprovedNeighbor = function (block, player) {
    block.setHasImprovedNeighbor(player);
    if (block.level === 0 && this.improvementNeighbors[player.playerId].indexOf(block) === -1) {
      this.improvementNeighbors[player.playerId].push(block);
    }
  };

  Map.prototype.unsetBlockHasImprovedNeighbor = function (block, player) {
    block.unsetHasImprovedNeighbor(player);
    ArrayUtils.removeElement(this.improvementNeighbors[player.playerId], block);
  };

  Map.prototype.neighborImproved = function (improvedBlock, player) {
    for (const neighborBlock of this.getBlockAdjacentNeighbors(improvedBlock)) {
      this.setBlockHasImprovedNeighbor(neighborBlock, player);
    }
    this._unsetBlockImprovableForAllPlayers(improvedBlock);
  };

  Map.prototype._blockHasImprovedNeighbor = function (block, player) {
    for (const neighborBlock of this.getBlockAdjacentNeighbors(block)) {
      if (neighborBlock.level > 0 && neighborBlock.ownedBy === player) {
        return true;
      }
    }
    return false;
  };

  Map.prototype._updateBlockImprovability = function (block, player) {
    if (block.level === 0 && this._blockHasImprovedNeighbor(block, player)) {
      this.setBlockHasImprovedNeighbor(block, player);
      if (storageProvider.getStorage(player).has(block.kind.calcResourceCost())) {
        block.setAffordableForPlayer(player);
      } else {
        block.unsetAffordableForPlayer(player);
      }
    } else {
      this.unsetBlockHasImprovedNeighbor(block, player);
      block.unsetAffordableForPlayer(player);
    }
  };

  Map.prototype._unsetBlockImprovableForAllPlayers = function (block) {
    for (const player of Object.values(this.players)) {
      this.unsetBlockHasImprovedNeighbor(block, player);
      block.unsetAffordableForPlayer(player);
    }
  };

  Map.prototype._setBlockImprovable = function (block, player) {
    this.setBlockHasImprovedNeighbor(block, player);
    if (storageProvider.getStorage(player).has(block.kind.calcResourceCost())) {
      block.setAffordableForPlayer(player);
    }
  };

  Map.prototype.neighborUnimproved = function (unimprovedBlock, player) {
    this._unsetBlockImprovableForAllPlayers(unimprovedBlock);
    for (const neighborBlock of this.getBlockAdjacentNeighbors(unimprovedBlock)) {
      this._updateBlockImprovability(neighborBlock, player);
      if (neighborBlock.level > 0) {
        this._setBlockImprovable(unimprovedBlock, neighborBlock.ownedBy);
      }
    }
  };

  Map.prototype.updateNeighborBlocksAffordability = function (player) {
    var map = this;
    map.affordableBlockCount[player.playerId] = 0;
    this.improvementNeighbors[player.playerId].forEach(function (block) {
      if (!block.exhausted && block.kind.improvable && block.level === 0 &&
        storageProvider.getStorage(player).has(block.kind.calcResourceCost())) {
        block.setAffordableForPlayer(player);
        map.affordableBlockCount[player.playerId]++;
      } else {
        block.unsetAffordableForPlayer(player);
      }
    });
  };

  Map.prototype.updateBlocksAffordability = function () {
    var that = this;
    Object.keys(that.players).forEach(function (playerId) {
      that.updateNeighborBlocksAffordability(that.players[playerId]);
    });
  };

  Map.prototype.checkBlockImprovability = function (block, player) {
    return !block.exhausted && block.kind.improvable && block.level === 0 &&
      (block.hasImprovedNeighbor(player) || this.isFirstImprovement(player)) &&
      storageProvider.getStorage(player).has(block.kind.calcResourceCost());
  };

  Map.prototype.getBlockMutationProbability = function (block, player) {
    var distX = Math.abs(block.region.x - player.startingRegionCoords.x);
    var distY = Math.abs(block.region.y - player.startingRegionCoords.y);
    return Math.min(this.maxBlockMutationProbability, Math.max(
      this.baseBlockMutationProbability * (distX > 1 ? distX : 0),
      this.baseBlockMutationProbability * (distY > 1 ? distY : 0)
    ));
  };

  Map.prototype.captureBlock = function (block, player) {
    storageProvider.getStorage(player).take(block.kind.calcResourceCost());
    block.ownedBy = player;
    block.level = 1;
    this.squaresMap.checkSmallestSquare(block, player);
    storageProvider.getStorage(player).put(
      block.kind.calcResourceGain(block.level, block.rich));
    storageProvider.getStorage(player).changeGoldIncome(
      block.kind.calcGoldGain(block.level, block.rich));
    this.neighborImproved(block, player);
    this.updateBlocksAffordability();
    this.improvedBlocksCounter[player.playerId]++;
    this.setNeighborhoodImprovedNeighbor(block);
    this.updateBlockAiValuesAfterImproving(block, player);
    if (player.ai) {
      player.ai.setBlockImproved(block);
    }
    this.mapBlocks.addOwnedBlock(block);
    if (!this.fixedSize) {
      this.ensureAdjacentRegionsExistence(block.region);
    }
    //TODO: capture block's buildings ?
  };

  Map.prototype.tryToCaptureBlock = function (block, player) {
    if (this.checkBlockImprovability(block, player)) {
      var rand = this.randGenerators.captureBlockMutation.getFloat(block);
      if (!this.isFirstImprovement(player) &&
          rand < this.getBlockMutationProbability(block, player) &&
          this.mutateBlockKind(block)) {
        //TODO: check affordableBlockCount
        return false;
      }
      this.captureBlock(block, player);
      return true;
    }
    return false;
  };

  Map.prototype.checkBlockAbilityToDisclaim = function (block, player) {
    return player.disclaimCooldown === 0 &&
      block.ownedBy === player &&
      block.level !== 0 &&
      block.kind.improvable &&
      !this.isLastOwnedBlock(player);
  };

  Map.prototype.disclaimBlock = function (block) {
    var player = block.ownedBy;
    this._updateStorageByLostBlockOutcomes(block, player);
    block.disabled = false;
    var formerSquareBlocks = this._checkLostBlockSquare(block);
    this.mapBlocks.removeOwnedBlock(block, player);
    block.level = 0;
    block.ownedBy = null;
    this._checkForNewSmallestSquares(formerSquareBlocks, player);
    this.neighborUnimproved(block, player);
    this.updateBlocksAffordability();
    this.improvedBlocksCounter[player.playerId]--;
    this.unsetNeighborhoodImprovedNeighbor(block);
    this.updateBlockAiValuesAfterUnimproving(block, player);
    if (player.ai) {
      player.ai.unsetBlockImproved(block);
    }
    player.setNextDisclaimCooldown();
    //TODO: lose block's buildings
  };

  Map.prototype.tryToDisclaimBlock = function (block, player) {
    if (this.checkBlockAbilityToDisclaim(block, player)) {
      this.disclaimBlock(block);
      return true;
    }
    return false;
  };

  Map.prototype._updateStorageByLostBlockOutcomes = function (block, player) {
    if (!block.disabled) {
      var storage = storageProvider.getStorage(player);
      storage.take(block.kind.calcResourceGain(block.level, block.rich), true);
      storage.changeGoldIncome(-block.kind.calcGoldGain(block.level, block.rich));
      storage.put(block.kind.calcResourceCost());
    }
  };

  Map.prototype._checkLostBlockSquare = function (disclaimedBlock) {
    var formerSquareBlocks = [];
    if (disclaimedBlock.square) {
      formerSquareBlocks = disclaimedBlock.square.blocks
        .filter(block => block !== disclaimedBlock);
      this.squaresMap.removeBlockFromSquare(disclaimedBlock);
    }
    return formerSquareBlocks;
  };

  Map.prototype._checkForNewSmallestSquares = function (blocks, player) {
    for (const block of blocks) {
      this.squaresMap.checkSmallestSquare(block, player);
    }
  };

  Map.prototype.updateBlockAiValuesAfterImproving = function (block, player) {
    this.setBlockAiValueWithSideEffects(block, 0);
    if (player instanceof AIPlayer) {
      //TODO: use getBlockAdjacentNeighbors
      mapDirections.nonDiagonal.forEach(function (direction) {
        if (block.neighborsByPosition[direction] && block.neighborsByPosition[direction].level === 0) {
          var value = block.neighborsByPosition[direction].aiValueForPlayer[player.playerId];
          block.neighborsByPosition[direction].aiValueForPlayer[player.playerId] = Math.round(value * 1.5);
        }
      });
    }
  };

  Map.prototype.updateBlockAiValuesAfterUnimproving = function (block, player) {
    this.setBlockAiValueWithSideEffects(block, block.kind.constructor.prototype.baseAiValue);
    if (player.ai) {
      for (const neighbor of this.getBlockAdjacentNeighbors(block)) {
        if (neighbor.level === 0 && !neighbor.hasImprovedNeighbor(player)) {
          var value = neighbor.aiValueForPlayer[player.playerId];
          neighbor.aiValueForPlayer[player.playerId] = Math.round(value / 1.5);
        }
      }
    }
  };

  Map.prototype.setNeighborhoodImprovedNeighbor = function (block) {
    for (var direction in mapDirections.directionToCoord) {
      var neighbor = this.getBlock(block.region.x, block.region.y,
        block.x + mapDirections.directionToCoord[direction].x,
        block.y + mapDirections.directionToCoord[direction].y);
      if (neighbor) {
        neighbor.improvedNeighbors[mapDirections.directionOpposites[direction]] = block;
      }
    }
  };

  Map.prototype.unsetNeighborhoodImprovedNeighbor = function (block) {
    var neighbors = this.getBlockAllNeighborsWithPositions(block);
    for (const direction in neighbors) {
      let neighbor = neighbors[direction];
      delete neighbor.improvedNeighbors[mapDirections.directionOpposites[direction]];
    }
  };

  Map.prototype.isFirstImprovement = function (player) {
    return this.improvedBlocksCounter[player.playerId] === 0;
  };

  Map.prototype.isLastOwnedBlock = function (player) {
    return this.improvedBlocksCounter[player.playerId] === 1;
  };

  Map.prototype.checkScrollingBoundaries = function (scrollLeft, scrollTop, scrollDirection) {
    if (['up', 'right', 'down', 'left'].indexOf(scrollDirection) === -1) {
      throw new Error('Wrong scroll direction: ' + scrollDirection);
    }
    //TODO: scrolling offset buffers (depending on window size), different for fixedSize map
    if (scrollDirection == 'up') {
      if (-(parseFloat(scrollTop) / this.blockHeightPx / Region.blocksHeight) <= this.borderRegionsCoords.top) {
        return false;
      }
    }
    if (scrollDirection == 'right') {
      if (-(parseFloat(scrollLeft) / this.blockWidthPx / Region.blocksWidth) >= this.borderRegionsCoords.right - 1) {
        return false;
      }
    }
    if (scrollDirection == 'down') {
      if (-(parseFloat(scrollTop + this.mapWindowWidthHeightDifferencePx) /
          this.blockHeightPx / Region.blocksHeight) >= this.borderRegionsCoords.bottom - 1) {
        return false;
      }
    }
    if (scrollDirection == 'left') {
      if (-(parseFloat(scrollLeft) / this.blockWidthPx / Region.blocksWidth) <= this.borderRegionsCoords.left) {
        return false;
      }
    }
    return true;
  };

  Map.prototype.initAllBlocksNeighborsAndTmpAiValues = function () {
    var that = this;
    Object.keys(that.regions).forEach(function (regionY) {
      Object.keys(that.regions[regionY]).forEach(function (regionX) {
        var region = that.regions[regionY][regionX];
        region.blocks.forEach(function (row) {
          row.forEach(that.initBlockNeighborsAndTmpAiValue, that);
        });
      });
    });
  };

  Map.prototype.initBlockNeighborsAndTmpAiValue = function (block /*, idx, blockRow */) {
    block.tmpAiValue = block.kind.baseAiValue * 3 * (block.rich ? 2 : 1);
    this.getBlockAdjacentNeighbors(block).forEach(function (neighbor) {
      if (neighbor.kind.toString() != 'water') {
        block.neighborsByKind[neighbor.kind.toString()].push(neighbor);
      }
      block.tmpAiValue += neighbor.kind.baseAiValue * (neighbor.rich ? 2 : 1);
    });
    var neighbors = this.getBlockAllNeighborsWithPositions(block);
    Object.keys(neighbors).forEach(function (position) {
      block.neighborsByPosition[position] = neighbors[position];
      block.tmpAiValue += neighbors[position].kind.baseAiValue * (neighbors[position].rich ? 2 : 1);
    });
  };

  Map.prototype.initAllBlocksBaseAiValues = function () {
    var that = this;
    Object.keys(that.regions).forEach(function (regionY) {
      Object.keys(that.regions[regionY]).forEach(function (regionX) {
        var region = that.regions[regionY][regionX];
        region.blocks.forEach(function (row) {
          row.forEach(that.setBlockBaseAiValue, that);
        });
      });
    });
  };

  Map.prototype.updateBlockBaseAiValue = function (block /*, idx, blockRow */) {
    var valueChange = this.setBlockBaseAiValue(block);
    Object.keys(this.aiPlayers).forEach(function (playerId) {
      var initValue = block.aiValueForPlayer[playerId] || 1;
      block.aiValueForPlayer[playerId] = Math.round(initValue * valueChange);
    });
  };

  Map.prototype.setBlockBaseAiValue = function (block /*, idx, blockRow */) {
    var oldBaseAiValue = block.baseAiValue;
    block.baseAiValue = block.tmpAiValue * 3;
    this.getBlockAdjacentNeighbors(block).forEach(function (adjacentNeighbor) {
      block.baseAiValue += adjacentNeighbor.tmpAiValue * 2;
    });
    Object.keys(block.neighborsByPosition).forEach(function (position) {
      block.baseAiValue += block.neighborsByPosition[position].tmpAiValue * 2;
    });
    var surroundings = this.getBlocks(block.region.x, block.region.y, block.x - 2, block.y - 2, 5, 5);
    surroundings.forEach(function (neighbor) {
      block.baseAiValue += neighbor.tmpAiValue;
    });
    block.baseAiValue = Math.floor(block.baseAiValue / 25);
    return block.baseAiValue / (oldBaseAiValue || 1);
  };

  Map.prototype.setBlockAiValueWithSideEffects = function (block, newBaseAiValue) {
    block.kind.baseAiValue = newBaseAiValue;
    var that = this;
    var affectedNeighbors = that.getBlocks(block.region.x, block.region.y, block.x - 1, block.y - 1, 3, 3);
    affectedNeighbors.forEach(that.setBlockTmpAiValue, that);
    var affectedSurroundings = that.getBlocks(block.region.x, block.region.y, block.x - 3, block.y - 3, 7, 7);
    affectedSurroundings.forEach(that.updateBlockBaseAiValue, that);
  };

  Map.prototype.setBlockTmpAiValue = function (block /*, idx, blockRow */) {
    block.tmpAiValue = block.kind.baseAiValue * 3 * (block.rich ? 2 : 1);
    this.getBlockAdjacentNeighbors(block).forEach(function (neighbor) {
      block.tmpAiValue += neighbor.kind.baseAiValue * (neighbor.rich ? 2 : 1);
    });
    Object.keys(block.neighborsByPosition).forEach(function (position) {
      block.tmpAiValue += block.neighborsByPosition[position].kind.baseAiValue *
        (block.neighborsByPosition[position].rich ? 2 : 1);
    });
  };

  Map.prototype.setPlayers = function (players) {
    this.players = players;
    var that = this;
    Object.keys(that.players).forEach(function (playerId) {
      that.improvedBlocksCounter[playerId] = 0;
      that.improvementNeighbors[playerId] = [];
      that.affordableBlockCount[playerId] = 0;
    });
    this.setPlayersEventListeners(players);
    this.setAiPlayers(players);
  };

  Map.prototype.setPlayersEventListeners = function (players) {
    var that = this;
    Object.keys(players).forEach(function (playerId) {
      var player = players[playerId];
      player.eventEmitter
        .on('updatingBlocksAffordability', function () {
          that.updateNeighborBlocksAffordability(player);
        })
        .on('playerTurnBegin', function () {
          that.autonomousBlockKindMutation();
        });
      var storage = storageProvider.getStorage(player);
      storage.eventEmitter
        .on('blockResourceShortage', function (resName) {
          if (!that.disableBlockDueToResourceShortage(resName, player)) {
            storage.resourceShortage.tryToReduceConsumption(resName, 'block');
          }
        })
        .on('blockResourceShortageEnd', function (resName) {
          if (!that.enableBlockDisabledDueToResourceShortage(resName, player)) {
            storage.resourceShortage.revertConsumptionReduction(resName, 'block');
          }
        });
    });
  };

  Map.prototype.setAiPlayers = function (players) {
    this.aiPlayers = {};
    var that = this;
    Object.keys(players).forEach(function (playerId) {
      if (players[playerId] instanceof AIPlayer) {
        that.aiPlayers[playerId] = players[playerId];
        that.setAllBlocksAiValuesForPlayer(players[playerId]);
        players[playerId].ai.setMap(that);
      }
    });
  };

  Map.prototype.setAllBlocksAiValuesForPlayer = function (player) {
    var that = this;
    Object.keys(that.regions).forEach(function (regionY) {
      Object.keys(that.regions[regionY]).forEach(function (regionX) {
        var region = that.regions[regionY][regionX];
        region.blocks.forEach(function (row) {
          row.forEach(function (block) {
            that.setBlockAiValueForPlayer(block, player);
          });
        });
      });
    });
  };

  Map.prototype.setBlockAiValueForPlayer = function (block, player) {
    block.aiValueForPlayer[player.playerId] = block.baseAiValue;
    block.aiValuesForPlayerMissions[player.playerId] = {};
  };

  Map.prototype.resetNeighborsByKindForAllBlocks = function () {
    var that = this;
    Object.keys(that.regions).forEach(function (regionY) {
      Object.keys(that.regions[regionY]).forEach(function (regionX) {
        var region = that.regions[regionY][regionX];
        region.blocks.forEach(function (row) {
          row.forEach(function (block) {
            block.resetNeighborsByKind();
          });
        });
      });
    });
  };

  Map.prototype.resetAllBlocksAiValueForPlayer = function () {
    var that = this;
    Object.keys(that.regions).forEach(function (regionY) {
      Object.keys(that.regions[regionY]).forEach(function (regionX) {
        var region = that.regions[regionY][regionX];
        region.blocks.forEach(function (row) {
          row.forEach(function (block) {
            block.aiValueForPlayer = {};
          });
        });
      });
    });
  };

  Map.prototype.mutateBlockKind = function (block) {
    if (!block.isMutable()) {
      return false;
    }
    var newKind;
    var newRich;
    if (!block.canBeRich() ||
        this.randGenerators.blockKindMutation.getFloat(block) < 0.95) {
      //change richness & kind
      newKind = block.createMutatedKind(
        this.randGenerators.blockMutatedKind.getGenerator(block));
      newRich = block.canBeRich() &&
        this.randGenerators.blockRichMutation.getFloat(block) < 0.01;
    } else {
      //change only richness
      newKind = block.kind;
      newRich = true;
    }
    block.mutated = true;
    this.eventEmitter.emit('blockMutates', block);
    this.changeBlockKind(block, newKind, newRich);
    return true;
  };

  Map.prototype.changeBlockKind = function (block, newKind, newRich) {
    //TODO: notification to buildingsBuilder.getSquareResourceCount
    //TODO: notification to AISquaring.checkNeighborBlocks
    //TODO: notification to Square
    var that = this;
    var oldKind = block.kind;
    var oldRich = block.rich;
    newRich = newRich === undefined ? oldRich : newRich;
    block.changeKind(newKind, newRich);
    that.setBlockAiValueWithSideEffects(block, newKind.baseAiValue);
    that._updateNeighborBlocksNeighborsByKind(block);
    that.updateBlocksAffordability();
    if (block.level > 0 && (oldKind !== newKind || oldRich !== newRich)) {
      if (!block.disabled) {
        this.updateOwnedBlockProduction(block, oldKind, oldRich, newKind, newRich);
      }
      if (oldKind !== newKind) {
        this.mapBlocks.changeOwnedBlockKind(block, oldKind, newKind);
      }
    }
  };

  Map.prototype.updateOwnedBlockProduction = function (block, oldKind, oldRich, newKind, newRich) {
    //TODO: update square internal production if block level > 1
    var storage = storageProvider.getStorage(block.ownedBy);
    storage.take(oldKind.calcResourceGain(block.level, oldRich), true);
    storage.put(oldKind.calcResourceCost());
    storage.changeGoldIncome(- oldKind.calcGoldGain(block.level, oldRich));
    storage.put(newKind.calcResourceGain(1, newRich));
    storage.take(newKind.calcResourceCost(), true);
    storage.changeGoldIncome(newKind.calcGoldGain(block.level, newRich));
  };

  Map.prototype._updateNeighborBlocksNeighborsByKind = function (block) {
    var that = this;
    that.getBlockAdjacentNeighbors(block).forEach(function (neighbor) {
      neighbor.resetNeighborsByKind();
      that.getBlockAdjacentNeighbors(neighbor).forEach(function (farNeighbor) {
        if (farNeighbor.kind.toString() != 'water') {
          neighbor.neighborsByKind[farNeighbor.kind.toString()].push(farNeighbor);
        }
      });
    });
  };

  Map.prototype.getAutonomousBlockKindMutationCoords = function () {
    var borderRegions = this.borderRegionsCoords;
    var mapWidthRegions = borderRegions.right - borderRegions.left + 1;
    var mapHeightRegions = borderRegions.bottom - borderRegions.top + 1;
    var randGenerator = this.randGenerators.autonomousBlockMutationCoordsGenerator;
    return {
      regionX: Math.floor(randGenerator.getFloat() * mapWidthRegions) +
                 borderRegions.left,
      regionY: Math.floor(randGenerator.getFloat() * mapHeightRegions) +
                 borderRegions.top,
      blockX: Math.floor(randGenerator.getFloat() * Region.blocksWidth),
      blockY: Math.floor(randGenerator.getFloat() * Region.blocksHeight)
    };
  };

  Map.prototype.autonomousBlockKindMutation = function () {
    if (this.randGenerators.autonomousBlockMutationGenerator.getFloat() <
        this.baseAutonomousBlockMutationProbability) {
      var mutationCoords = this.getAutonomousBlockKindMutationCoords();
      if (mutationCoords.regionX >= -1 && mutationCoords.regionX <= 1 &&
          mutationCoords.regionY >= -1 && mutationCoords.regionY <= 1) {
        return;
      }
      var mutationBlock = this.getBlock(mutationCoords.regionX, mutationCoords.regionY,
        mutationCoords.blockX, mutationCoords.blockY);
      if (mutationBlock && mutationBlock.level === 0) {
        this.mutateBlockKind(mutationBlock);
      }
    }
  };

  Map.prototype._getBlocksByConsumedResourceAndPlayer = function (resName, player, enabled) {
    var resNameToBlockKinds = {
      wood: ['plain'],
      stone: ['forest'],
      metal: ['hill'],
      food: ['mountain','coalfield', 'wildHorses',
             'silverDeposit', 'goldDeposit', 'gemstoneDeposit']
    };
    if (Object.keys(resNameToBlockKinds).indexOf(resName) === -1) {
      return [];
    }
    var that = this;
    return resNameToBlockKinds[resName].reduce(function (blocks, kind) {
      var tmpBlocks = that.mapBlocks.getBlocksByPlayerAndKind(player, kind)
        .filter(function (block) {
          return block.disabled !== enabled;
        });
      return blocks.concat(tmpBlocks);
    }, []);
  };

  Map.prototype.disableBlockDueToResourceShortage = function (resName, player) {
    var matchingBlocks = this._getBlocksByConsumedResourceAndPlayer(resName, player, true);
    if (matchingBlocks.length === 0) {
      return false;
    }
    var rand = this.randGenerators.disablingBlockDueToResourceShortage.getFloat(
      player.playerId, resName, turnTicker.now());
    var block = matchingBlocks[Math.floor(matchingBlocks.length * rand)];
    var storage = storageProvider.getStorage(player);
    storage.put(block.kind.calcResourceCost());
    storage.take(block.kind.calcResourceGain(block.level, block.rich), true);
    storage.changeGoldIncome(-block.kind.calcGoldGain(block.level, block.rich));
    block.disabled = true;
    if (block.square) {
      block.square.calcResourceOutcomeTotal();
      block.square.calcGoldOutcomeTotal();
    }
    return true;
    //TODO: disabling turn + loosing tile after n turns
  };

  Map.prototype.enableBlockDisabledDueToResourceShortage = function (resName, player) {
    var matchingBlocks = this._getBlocksByConsumedResourceAndPlayer(resName, player, false);
    if (matchingBlocks.length === 0) {
      return false;
    }
    var rand = this.randGenerators.enablingBlockDisabledDueToResourceShortage.getFloat(
      player.playerId, resName, turnTicker.now());
    var block = matchingBlocks[Math.floor(matchingBlocks.length * rand)];
    var storage = storageProvider.getStorage(player);
    if (storage.take(block.kind.calcResourceCost())) {
      storage.put(block.kind.calcResourceGain(block.level, block.rich));
      storage.changeGoldIncome(block.kind.calcGoldGain(block.level, block.rich));
      block.disabled = false;
      if (block.square) {
        block.square.calcResourceOutcomeTotal();
        block.square.calcGoldOutcomeTotal();
      }
      return true;
    }
    return false;
  };

  Map.prototype._calcOwnedBlockResourceGainDiff = function (block, newLevel) {
    var currentResourceGain = block.kind.calcResourceGain(block.level, block.rich);
    var newResourceGain = block.kind.calcResourceGain(newLevel, block.rich);
    var resourceGainDiff = Object.keys(newResourceGain)
      .reduce(function (diff, resName) {
        diff[resName] = newResourceGain[resName] - (currentResourceGain[resName] || 0);
        return diff;
      }, {});
    Object.keys(currentResourceGain).forEach(function (resName) {
      if (resourceGainDiff[resName] === undefined) {
        resourceGainDiff[resName] = -currentResourceGain[resName];
      }
    });
    return resourceGainDiff;
  };

  Map.prototype.changeOwnedBlockLevel = function (block, newLevel) {
    if (block.level === 0) {
      throw new Error('Map.changeOwnedBlockLevel: ' +
        'cannot change the level of the block of level 0');
    }
    if (!block.ownedBy) {
      throw new Error('Map.changeOwnedBlockLevel: ' +
        'cannot change the level of a non-owned block');
    }
    if (newLevel === 0) {
      throw new Error('Map.changeOwnedBlockLevel: ' +
        'cannot set the block level to 0');
    }
    if (block.level === newLevel) {
      return;
    }
    var resourceGainDiff = this._calcOwnedBlockResourceGainDiff(block, newLevel);
    var currentGoldGain = block.kind.calcGoldGain(block.level, block.rich);
    var newGoldGain = block.kind.calcGoldGain(newLevel, block.rich);
    var storage = storageProvider.getStorage(block.ownedBy);
    storage.put(resourceGainDiff);
    storage.changeGoldIncome(newGoldGain - currentGoldGain);
    block.level = newLevel;
  };

  return Map;
}]);