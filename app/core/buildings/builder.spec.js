'use strict';

describe('buildingsBuilder', function() {
  beforeEach(module('square'));

  var buildingsBuilder, Block, Market, Forest, Mountain, Square, Hill,
    storageProvider, playerProvider, buildingClassRegister;
  var fakeStorage;

  beforeEach(inject(function (_buildingsBuilder_, _Block_, _Market_, _Forest_, _Mountain_, _Square_, _Hill_,
                              _storageProvider_, _playerProvider_, _buildingClassRegister_) {
    buildingsBuilder = _buildingsBuilder_;
    Block = _Block_;
    Market = _Market_;
    Forest = _Forest_;
    Mountain = _Mountain_;
    Hill = _Hill_;
    Square = _Square_;

    storageProvider = _storageProvider_;
    playerProvider = _playerProvider_;
    buildingClassRegister = _buildingClassRegister_;

    fakeStorage = {
      take: jasmine.createSpy('storage.take'),
      takeGold: jasmine.createSpy('storage.takeGold'),
      has: jasmine.createSpy('storage.has'),
      hasGold: jasmine.createSpy('storage.hasGold')
    };
    storageProvider.getStorage = jasmine.createSpy('getStorage')
      .and.returnValue(fakeStorage);
  }));

  describe('chooseBuildingBlock', function () {

    it('calls getSpecialBuildingPosition if it is set on buildingClass and returns its result', function () {
      var buildingClass = jasmine.createSpyObj('buildingClass', ['prototype', 'name']);
      var expectedResult = jasmine.createSpy('expectedResult');
      buildingClass.prototype.getSpecialBuildingPosition =
        jasmine.createSpy('getSpecialBuildingPosition').and.returnValue(expectedResult);

      var result = buildingsBuilder._instance.chooseBuildingBlock(jasmine.createSpy('square'), buildingClass);

      expect(buildingClass.prototype.getSpecialBuildingPosition).toHaveBeenCalled();
      expect(result).toBe(expectedResult);
    });

    it('calls chooseBuildingBlockByMainResource if there is any maxNumberPerResourceInSquare set', function () {
      var buildingClass = jasmine.createSpyObj('buildingClass', ['prototype', 'name']);
      var expectedResult = jasmine.createSpy('expectedResult');
      var notExpectedResult = jasmine.createSpy('notExpectedResult');
      buildingsBuilder._instance.getMaxNumberPerResourceInSquare =
        jasmine.createSpy('getMaxNumberPerResourceInSquare').and.returnValue({ metal: 2 });
      buildingsBuilder._instance.chooseBuildingBlockByNumberOfBuildingsInBlock =
        jasmine.createSpy('chooseBuildingBlockByNumberOfBuildingsInBlock').and.returnValue(notExpectedResult);
      buildingsBuilder._instance.chooseBuildingBlockByMainResource =
        jasmine.createSpy('chooseBuildingBlockByMainResource').and.returnValue(expectedResult);

      var result = buildingsBuilder._instance.chooseBuildingBlock(jasmine.createSpy('square'), buildingClass);

      expect(buildingsBuilder._instance.chooseBuildingBlockByMainResource).toHaveBeenCalled();
      expect(result).toBe(expectedResult);
      expect(buildingsBuilder._instance.chooseBuildingBlockByNumberOfBuildingsInBlock).not.toHaveBeenCalled();
      expect(result).not.toBe(notExpectedResult);
    });

    it('calls chooseBuildingBlockByNumberOfBuildingsInBlock if there is not any maxNumberPerResourceInSquare set', function () {
      var buildingClass = jasmine.createSpyObj('buildingClass', ['prototype', 'name']);
      var expectedResult = jasmine.createSpy('expectedResult');
      var notExpectedResult = jasmine.createSpy('notExpectedResult');
      buildingsBuilder._instance.getMaxNumberPerResourceInSquare =
        jasmine.createSpy('getMaxNumberPerResourceInSquare').and.returnValue({});
      buildingsBuilder._instance.chooseBuildingBlockByNumberOfBuildingsInBlock =
        jasmine.createSpy('chooseBuildingBlockByNumberOfBuildingsInBlock').and.returnValue(expectedResult);
      buildingsBuilder._instance.chooseBuildingBlockByMainResource =
        jasmine.createSpy('chooseBuildingBlockByMainResource').and.returnValue(notExpectedResult);

      var result = buildingsBuilder._instance.chooseBuildingBlock(jasmine.createSpy('square'), buildingClass);

      expect(buildingsBuilder._instance.chooseBuildingBlockByMainResource).not.toHaveBeenCalled();
      expect(result).not.toBe(notExpectedResult);
      expect(buildingsBuilder._instance.chooseBuildingBlockByNumberOfBuildingsInBlock).toHaveBeenCalled();
      expect(result).toBe(expectedResult);
    });

  });

  describe('chooseBuildingBlockByNumberOfBuildingsInBlock', function () {

    it('must not change square blocks', function () {
      var fakeSquare = {blocks: [new Block(0, 0), new Block(0, 1), new Block(1, 0), new Block(1, 1)]};
      var buildingClassName = 'fakeBuildingClassName';
      var oldSquareBlocks = fakeSquare.blocks.slice();

      buildingsBuilder._instance.chooseBuildingBlockByNumberOfBuildingsInBlock(
        fakeSquare, buildingClassName);

      expect(fakeSquare.blocks).toEqual(oldSquareBlocks);
    });

    describe('chooses the block with the least number of buildings', function () {

      it('for 2 blocks and 1 building class', function () {
        var block1 = new Block(0, 0);
        var block2 = new Block(0, 1);
        var fakeSquare = {blocks: [block1, block2]};
        function FakeBuilding1() { return null; }
        var buildingClassName = 'FakeBuilding1';
        block1.addBuilding(new FakeBuilding1());
        block2.addBuilding(new FakeBuilding1());
        block2.addBuilding(new FakeBuilding1());

        var result = buildingsBuilder._instance.chooseBuildingBlockByNumberOfBuildingsInBlock(
          fakeSquare, buildingClassName);

        expect(result).toBe(block1);
      });

      it('for 4 blocks and 2 building classes', function () {
        var block1 = new Block(0, 0);
        var block2 = new Block(0, 1);
        var block3 = new Block(1, 0);
        var block4 = new Block(1, 1);
        var fakeSquare = {blocks: [block1, block2, block3, block4]};
        function FakeBuilding1() { return null; }
        function FakeBuilding2() { return null; }
        var buildingClassName = 'FakeBuilding1';
        block1.addBuilding(new FakeBuilding1());
        block1.addBuilding(new FakeBuilding1());
        block1.addBuilding(new FakeBuilding2());
        block2.addBuilding(new FakeBuilding1());
        block2.addBuilding(new FakeBuilding2());
        block3.addBuilding(new FakeBuilding1());
        block4.addBuilding(new FakeBuilding2());

        var result = buildingsBuilder._instance.chooseBuildingBlockByNumberOfBuildingsInBlock(
          fakeSquare, buildingClassName);

        expect(result).toBe(block4);
      });

      it('for 3 blocks with the same number of buildings', function () {
        var block1 = new Block(0, 0);
        var block2 = new Block(0, 1);
        var block3 = new Block(1, 0);
        var block4 = new Block(1, 1);
        var fakeSquare = {blocks: [block1, block2, block3, block4]};
        function FakeBuilding1() { return null; }
        function FakeBuilding2() { return null; }
        var buildingClassName = 'FakeBuilding1';
        block1.addBuilding(new FakeBuilding1());
        block1.addBuilding(new FakeBuilding1());
        block1.addBuilding(new FakeBuilding2());
        block2.addBuilding(new FakeBuilding1());
        block2.addBuilding(new FakeBuilding2());
        block3.addBuilding(new FakeBuilding1());
        block3.addBuilding(new FakeBuilding1());
        block4.addBuilding(new FakeBuilding2());
        block4.addBuilding(new FakeBuilding2());

        var result = buildingsBuilder._instance.chooseBuildingBlockByNumberOfBuildingsInBlock(
          fakeSquare, buildingClassName);

        expect(result).toBe(block4);
      });

      it('for 3 blocks with exactly the same buildings returning one of them with a proper distribution', function () {
        var block1 = new Block(0, 0);
        var block2 = new Block(0, 1);
        var block3 = new Block(1, 0);
        var block4 = new Block(1, 1);
        var fakeSquare = {blocks: [block1, block2, block3, block4]};
        function FakeBuilding1() { return null; }
        function FakeBuilding2() { return null; }
        var buildingClassName = 'FakeBuilding1';
        block1.addBuilding(new FakeBuilding1());
        block1.addBuilding(new FakeBuilding1());
        block1.addBuilding(new FakeBuilding2());
        block2.addBuilding(new FakeBuilding1());
        block2.addBuilding(new FakeBuilding2());
        block3.addBuilding(new FakeBuilding1());
        block3.addBuilding(new FakeBuilding2());
        block4.addBuilding(new FakeBuilding1());
        block4.addBuilding(new FakeBuilding2());

        var result = buildingsBuilder._instance.chooseBuildingBlockByNumberOfBuildingsInBlock(
          fakeSquare, buildingClassName);

        expect([block2, block3, block4]).toContain(result);

        var results = {};
        results[block2.toString()] = 0;
        results[block3.toString()] = 0;
        results[block4.toString()] = 0;

        var sampleSize = 5000;
        for (var i = 0; i < sampleSize; i++) {
          var block = buildingsBuilder._instance.chooseBuildingBlockByNumberOfBuildingsInBlock(
            fakeSquare, buildingClassName);
          results[block.toString()]++;
        }

        expect(results[block2.toString()] / sampleSize).toBeWithinRange(0.3, 0.366);
        expect(results[block3.toString()] / sampleSize).toBeWithinRange(0.3, 0.366);
        expect(results[block4.toString()] / sampleSize).toBeWithinRange(0.3, 0.366);
      });

    });

  });

  describe('chooseBuildingBlockByMainResource', function () {

    it('returns metal block if building requires it', function () {
      var fakeRegion = {x: 0, y: 0};
      var forestBlock1 = new Block(0, 0, fakeRegion, new Forest());
      var metalBlock = new Block(0, 1, fakeRegion, new Mountain());
      var forestBlock2 = new Block(1, 0, fakeRegion, new Forest());
      var forestBlock3 = new Block(1, 1, fakeRegion, new Forest());
      var fakeSquare = {blocks: [forestBlock1, metalBlock, forestBlock2, forestBlock3]};
      var buildingClassName = 'fakeBuildingClassName';

      var result = buildingsBuilder._instance.chooseBuildingBlockByMainResource(
        fakeSquare, buildingClassName, { metal: 1 });

      expect(result).toEqual(metalBlock);
    });

    it('returns block with the least number of given class buildings', function () {
      var fakeRegion = {x: 0, y: 0};
      var forestBlock1 = new Block(0, 0, fakeRegion, new Forest());
      var metalBlock1 = new Block(0, 1, fakeRegion, new Mountain());
      var forestBlock2 = new Block(1, 0, fakeRegion, new Forest());
      var metalBlock2 = new Block(1, 1, fakeRegion, new Mountain());
      var fakeSquare = {blocks: [forestBlock1, metalBlock1, forestBlock2, metalBlock2]};
      function FakeBuilding1() { return null; }
      var buildingClassName = 'FakeBuilding1';
      metalBlock1.addBuilding(new FakeBuilding1());
      metalBlock1.addBuilding(new FakeBuilding1());
      metalBlock2.addBuilding(new FakeBuilding1());

      var result = buildingsBuilder._instance.chooseBuildingBlockByMainResource(
        fakeSquare, buildingClassName, { metal: 2 });

      expect(result).toEqual(metalBlock2);
    });

    it('returns a rich block if there are few suitable blocks with the same number of buildings', function () {
      var fakeRegion = {x: 0, y: 0};
      var forestBlock1 = new Block(0, 0, fakeRegion, new Forest());
      var richMetalBlock1 = new Block(0, 1, fakeRegion, new Mountain(), true);
      var forestBlock2 = new Block(1, 0, fakeRegion, new Forest());
      var metalBlock2 = new Block(1, 1, fakeRegion, new Mountain());
      var fakeSquare = {blocks: [forestBlock1, richMetalBlock1, forestBlock2, metalBlock2]};
      function FakeBuilding1() { return null; }
      var buildingClassName = 'FakeBuilding1';
      richMetalBlock1.addBuilding(new FakeBuilding1());
      metalBlock2.addBuilding(new FakeBuilding1());

      var result = buildingsBuilder._instance.chooseBuildingBlockByMainResource(
        fakeSquare, buildingClassName, { metal: 2 });

      expect(result).toBe(richMetalBlock1);
    });

  });

  describe('sortRichFirst', function () {

    it('sorts blocks putting rich ones on the beginning', function () {
      var fakeRegion = {x: 0, y: 0};
      var forestBlock = new Block(0, 0, fakeRegion, new Forest());
      var richMetalBlock = new Block(0, 1, fakeRegion, new Mountain(), true);
      var richForestBlock = new Block(1, 0, fakeRegion, new Forest(), true);
      var metalBlock = new Block(1, 1, fakeRegion, new Mountain());
      var blocks = [forestBlock, richMetalBlock, richForestBlock, metalBlock];

      buildingsBuilder._instance.sortRichFirst(blocks);
      expect([blocks[0], blocks[1]]).toContain(richMetalBlock);
      expect([blocks[0], blocks[1]]).toContain(richForestBlock);
      expect([blocks[2], blocks[3]]).not.toContain(richMetalBlock);
      expect([blocks[2], blocks[3]]).not.toContain(richForestBlock);
    });

  });

  describe('sortDescendingByEmptySlotCount', function () {

    it('puts blocks with the least number of buildings on the beginning', function () {
      var block1 = new Block(0, 0);
      var block2 = new Block(0, 1);
      var block3 = new Block(1, 0);
      var block4 = new Block(1, 1);
      var blocks = [block1, block2, block3, block4];
      function FakeBuilding1() { return null; }
      block1.addBuilding(new FakeBuilding1());
      block1.addBuilding(new FakeBuilding1());
      block1.addBuilding(new FakeBuilding1()); // 3 buildings
      block2.addBuilding(new FakeBuilding1());
      block2.addBuilding(new FakeBuilding1()); // 2 buildings
      block3.addBuilding(new FakeBuilding1()); // the least number = 1
      block4.addBuilding(new FakeBuilding1());
      block4.addBuilding(new FakeBuilding1());
      block4.addBuilding(new FakeBuilding1());
      block4.addBuilding(new FakeBuilding1()); // 4 buildings

      buildingsBuilder._instance.sortDescendingByEmptySlotCount(blocks, 'FakeBuilding1');
      expect(blocks[0]).toBe(block3);
      expect(blocks[1]).toBe(block2);
      expect(blocks[2]).toBe(block1);
      expect(blocks[3]).toBe(block4);
    });

  });

  describe('checkAdditionalRequirements', function () {

    it('calls buildingClass additionalRequirements', function () {
      function FakeBuildingClass() { return null; }
      FakeBuildingClass.prototype.additionalRequirements = {
        req1: jasmine.createSpy('req1').and.returnValue(true),
        req2: jasmine.createSpy('req2').and.returnValue(true),
        req3: jasmine.createSpy('req3').and.returnValue(true),
        req4: jasmine.createSpy('req4').and.returnValue(true)
      };
      var fakeSquare = jasmine.createSpy('fakeSquare');
      buildingsBuilder._instance.checkAdditionalRequirements(fakeSquare, FakeBuildingClass);
      expect(FakeBuildingClass.prototype.additionalRequirements.req1).toHaveBeenCalledWith(fakeSquare);
      expect(FakeBuildingClass.prototype.additionalRequirements.req2).toHaveBeenCalledWith(fakeSquare);
      expect(FakeBuildingClass.prototype.additionalRequirements.req3).toHaveBeenCalledWith(fakeSquare);
      expect(FakeBuildingClass.prototype.additionalRequirements.req4).toHaveBeenCalledWith(fakeSquare);
    });

    it('returns buildingClass additionalRequirements results conjuncted', function () {
      function FakeBuildingClass() { return null; }
      FakeBuildingClass.prototype.additionalRequirements = {
        req1: jasmine.createSpy('req1').and.returnValue(true),
        req2: jasmine.createSpy('req2').and.returnValue(true),
        req3: jasmine.createSpy('req3').and.returnValue(true),
        req4: jasmine.createSpy('req4').and.returnValue(true)
      };
      var fakeSquare = jasmine.createSpy('fakeSquare');
      var result = buildingsBuilder._instance.checkAdditionalRequirements(fakeSquare, FakeBuildingClass);
      expect(result).toBeTrue();

      FakeBuildingClass.prototype.additionalRequirements.req5 = jasmine.createSpy('req5').and.returnValue(false);
      var result2 = buildingsBuilder._instance.checkAdditionalRequirements(fakeSquare, FakeBuildingClass);
      expect(result2).toBeFalse();
    });

  });

  describe('checkRequiredTechnologies', function () {

    it('returns true if no technologies are required', function () {
      function FakeBuilding1() { return null; }
      FakeBuilding1.prototype.requiredTechnologies = [];
      var result = buildingsBuilder._instance.checkRequiredTechnologies(FakeBuilding1, []);
      expect(result).toBeTrue();
    });

    it('returns true if player possess one required technology', function () {
      var fakeTech = jasmine.createSpy('fakeTech');
      var anotherFakeTech = jasmine.createSpy('anotherFakeTech');
      function FakeBuilding1() { return null; }
      FakeBuilding1.prototype.requiredTechnologies = [fakeTech];
      var result = buildingsBuilder._instance.checkRequiredTechnologies(FakeBuilding1, [fakeTech, anotherFakeTech]);
      expect(result).toBeTrue();
    });

    it('returns true if player possesses all required technologies', function () {
      var fakeTech = jasmine.createSpy('fakeTech');
      var anotherFakeTech = jasmine.createSpy('anotherFakeTech');
      function FakeBuilding1() { return null; }
      FakeBuilding1.prototype.requiredTechnologies = [fakeTech, anotherFakeTech];
      var result = buildingsBuilder._instance.checkRequiredTechnologies(FakeBuilding1, [fakeTech, anotherFakeTech]);
      expect(result).toBeTrue();
    });

    it('returns false if player does not posses any of required technologies', function () {
      var fakeTech = jasmine.createSpy('fakeTech');
      var anotherFakeTech = jasmine.createSpy('anotherFakeTech');
      function FakeBuilding1() { return null; }
      FakeBuilding1.prototype.requiredTechnologies = [fakeTech, anotherFakeTech];
      var result = buildingsBuilder._instance.checkRequiredTechnologies(FakeBuilding1, []);
      expect(result).toBeFalse();
    });

    it('returns false if player does not posses one of required technologies', function () {
      var fakeTech = jasmine.createSpy('fakeTech');
      var anotherFakeTech = jasmine.createSpy('anotherFakeTech');
      function FakeBuilding1() { return null; }
      FakeBuilding1.prototype.requiredTechnologies = [fakeTech, anotherFakeTech];
      var result = buildingsBuilder._instance.checkRequiredTechnologies(FakeBuilding1, [anotherFakeTech]);
      expect(result).toBeFalse();
    });

  });

  describe('checkRequiredSquareLevel', function () {

    it('returns false if requiredSquareLevel is greater than given square level', function () {
      var square = { level: 2 };
      function FakeBuildingClass() { return null; }
      FakeBuildingClass.prototype.requiredSquareLevel = 3;
      var result = buildingsBuilder._instance.checkRequiredSquareLevel(square, FakeBuildingClass);
      expect(result).toBeFalse();
    });

    it('returns true if requiredSquareLevel is equal to given square level', function () {
      var square = { level: 4 };
      function FakeBuildingClass() { return null; }
      FakeBuildingClass.prototype.requiredSquareLevel = 4;
      var result = buildingsBuilder._instance.checkRequiredSquareLevel(square, FakeBuildingClass);
      expect(result).toBeTrue();
    });

    it('returns true if requiredSquareLevel is lesser than given square level', function () {
      var square = { level: 3 };
      function FakeBuildingClass() { return null; }
      FakeBuildingClass.prototype.requiredSquareLevel = 2;
      var result = buildingsBuilder._instance.checkRequiredSquareLevel(square, FakeBuildingClass);
      expect(result).toBeTrue();
    });

  });

  describe('checkMaxNumberPerSquareSize', function () {

    it('returns false if there are more buildings than maxNumberPerSquareSize defined in buildingClass', function () {
      var squareLevel = 2;
      var square = new Square(0, 0, 0, 0, [], squareLevel, {});
      function FakeBuilding1() { return null; }
      FakeBuilding1.prototype.maxNumberPerSquareSize = {
        2: 3, // limit for squareLevel = 3
        3: 40,
        4: 40,
        5: 40
      };
      square.addBuilding(new FakeBuilding1());
      square.addBuilding(new FakeBuilding1());
      square.addBuilding(new FakeBuilding1());
      square.addBuilding(new FakeBuilding1()); // 4 buildings added
      var result = buildingsBuilder._instance.checkMaxNumberPerSquareSize(square, FakeBuilding1);
      expect(result).toBeFalse();
    });

    it('returns false if square number of buildings is equal to maxNumberPerSquareSize defined in buildingClass', function () {
      var squareLevel = 2;
      var square = new Square(0, 0, 0, 0, [], squareLevel, {});
      function FakeBuilding1() { return null; }
      FakeBuilding1.prototype.maxNumberPerSquareSize = {
        2: 3, // limit for squareLevel = 3
        3: 40,
        4: 8,
        5: 40
      };
      square.addBuilding(new FakeBuilding1());
      square.addBuilding(new FakeBuilding1());
      square.addBuilding(new FakeBuilding1()); // 3 buildings added
      var result = buildingsBuilder._instance.checkMaxNumberPerSquareSize(square, FakeBuilding1);
      expect(result).toBeFalse();
    });

    it('returns true if square number of buildings is lesser than maxNumberPerSquareSize defined in buildingClass', function () {
      var squareLevel = 2;
      var square = new Square(0, 0, 0, 0, [], squareLevel, {});
      function FakeBuilding1() { return null; }
      FakeBuilding1.prototype.maxNumberPerSquareSize = {
        2: 3, // limit for squareLevel = 3
        3: 40,
        4: 1,
        5: 40
      };
      square.addBuilding(new FakeBuilding1());
      square.addBuilding(new FakeBuilding1()); // 2 buildings added
      var result = buildingsBuilder._instance.checkMaxNumberPerSquareSize(square, FakeBuilding1);
      expect(result).toBeTrue();
    });

  });

  describe('checkMaxNumberPerResourceInSquare', function () {

    it('returns false if building count is greater than resource count multiplied by max number per resource', function () {
      buildingsBuilder._instance.getMaxNumberPerResourceInSquare =
        jasmine.createSpy('getMaxNumberPerResourceInSquare').and.returnValue({
          metal: 4,
          wood: 2
          // there have to be at least:
          // 1 metal and 1 wood for 1 building
          // 1 metal and 1 wood for 2 buildings
          // 1 metal and 2 wood for 3 buildings
          // 1 metal and 2 wood for 4 buildings
          // 2 metal and 3 wood for 5 buildings
        });
      buildingsBuilder._instance.getSquareResourceCount =
        jasmine.createSpy('getSquareResourceCount').and.callFake(function (square, resName) {
          return resName === 'wood' ? 2 : 0;  // 2 woods in square, no metal
        });
      var fakeSquare = { getBuildingCountByClass: function () { return 3; }};  // 3 buildings in square
      /*
        resources:    limit:      result:
        wood: 2       wood: 2     3 < 2 * 2 - pass
        metal: 0      metal: 4    3 > 0 * 4 - not pass
       */
      var result = buildingsBuilder._instance.checkMaxNumberPerResourceInSquare(fakeSquare, function FakeBuilding() {});
      expect(result).toBeFalse();
    });

    it('returns false if building count is equal to resource count multiplied by max number per resource', function () {
      buildingsBuilder._instance.getMaxNumberPerResourceInSquare =
        jasmine.createSpy('getMaxNumberPerResourceInSquare').and.returnValue({
          metal: 2,
          wood: 1
        });
      buildingsBuilder._instance.getSquareResourceCount =
        jasmine.createSpy('getSquareResourceCount').and.callFake(function (square, resName) {
          return resName === 'wood' ? 2 : 1;  // 2 woods, 1 metal in square
        });
      /*
        resources:    limit:      result:
        wood: 2       wood: 1     2 == 2 * 1 - not pass
        metal: 1      metal: 2    2 == 1 * 2 - not pass
       */
      var fakeSquare = { getBuildingCountByClass: function () { return 2; }};  // 2 buildings in square
      var result = buildingsBuilder._instance.checkMaxNumberPerResourceInSquare(fakeSquare, function FakeBuilding() {});
      expect(result).toBeFalse();
    });

    it('returns true if building count is lesser than resource count multiplied by max number per resource', function () {
      buildingsBuilder._instance.getMaxNumberPerResourceInSquare =
        jasmine.createSpy('getMaxNumberPerResourceInSquare').and.returnValue({
          metal: 2,
          wood: 1
        });
      buildingsBuilder._instance.getSquareResourceCount =
        jasmine.createSpy('getSquareResourceCount').and.callFake(function (square, resName) {
          return resName === 'wood' ? 3 : 2;  // 3 woods, 2 metal in square
        });
      /*
        resources:    limit:      result:
        wood: 3       wood: 1     2 < 3 * 1 - pass
        metal: 2      metal: 2    2 < 2 * 2 - pass
       */
      var fakeSquare = { getBuildingCountByClass: function () { return 2; }};  // 2 buildings in square
      var result = buildingsBuilder._instance.checkMaxNumberPerResourceInSquare(fakeSquare, function FakeBuilding() {});
      expect(result).toBeTrue();
    });

  });

  describe('getSquareResourceCount', function () {

    it('returns number of blocks which main resource is the given resource', function () {
      var woodBlock1 = new Block(0, 0, {}, new Forest());
      var woodBlock2 = new Block(0, 1, {}, new Forest());
      var woodBlock3 = new Block(0, 2, {}, new Forest());
      var stoneBlock1 = new Block(1, 0, {}, new Hill());
      var stoneBlock2 = new Block(1, 1, {}, new Hill());
      var metalBlock1 = new Block(1, 2, {}, new Mountain());
      var metalBlock2 = new Block(2, 0, {}, new Mountain());
      var metalBlock3 = new Block(2, 1, {}, new Mountain());
      var metalBlock4 = new Block(2, 2, {}, new Mountain());

      var fakeSquare = { blocks: [woodBlock1, woodBlock2, woodBlock3,
                                  stoneBlock1, stoneBlock2,
                                  metalBlock1, metalBlock2, metalBlock3, metalBlock4] };

      var result = buildingsBuilder._instance.getSquareResourceCount(fakeSquare, 'stone');
      expect(result).toBe(2);
    });

  });

  describe('checkBuildRequirements', function () {

    beforeEach(function prepareSpies() {
      buildingsBuilder._instance.checkRequiredSquareLevel =
        jasmine.createSpy('checkRequiredSquareLevel').and.returnValue(true);
      buildingsBuilder._instance.checkMaxNumberPerSquareSize =
        jasmine.createSpy('checkMaxNumberPerSquareSize').and.returnValue(true);
      buildingsBuilder._instance.checkMaxNumberPerResourceInSquare =
        jasmine.createSpy('checkMaxNumberPerResourceInSquare').and.returnValue(true);
      buildingsBuilder._instance.checkAdditionalRequirements =
        jasmine.createSpy('checkAdditionalRequirements').and.returnValue(true);
      buildingsBuilder._instance.checkRequiredTechnologies =
        jasmine.createSpy('checkRequiredTechnologies').and.returnValue(true);
    });

    it('calls checkRequiredSquareLevel', function () {
      var fakeSquare = jasmine.createSpy('fakeSquare');
      var fakeBuildingClass = jasmine.createSpy('fakeBuildingClass');
      var fakePlayerTechnologies = jasmine.createSpy('fakePlayerTechnologies');

      buildingsBuilder.checkBuildRequirements(fakeSquare, fakeBuildingClass, fakePlayerTechnologies);
      expect(buildingsBuilder._instance.checkRequiredSquareLevel).toHaveBeenCalledWith(fakeSquare, fakeBuildingClass);
    });

    it('calls checkMaxNumberPerSquareSize', function () {
      var fakeSquare = jasmine.createSpy('fakeSquare');
      var fakeBuildingClass = jasmine.createSpy('fakeBuildingClass');
      var fakePlayerTechnologies = jasmine.createSpy('fakePlayerTechnologies');

      buildingsBuilder.checkBuildRequirements(fakeSquare, fakeBuildingClass, fakePlayerTechnologies);
      expect(buildingsBuilder._instance.checkMaxNumberPerSquareSize).toHaveBeenCalledWith(fakeSquare, fakeBuildingClass);
    });

    it('calls checkMaxNumberPerResourceInSquare', function () {
      var fakeSquare = jasmine.createSpy('fakeSquare');
      var fakeBuildingClass = jasmine.createSpy('fakeBuildingClass');
      var fakePlayerTechnologies = jasmine.createSpy('fakePlayerTechnologies');

      buildingsBuilder.checkBuildRequirements(fakeSquare, fakeBuildingClass, fakePlayerTechnologies);
      expect(buildingsBuilder._instance.checkMaxNumberPerResourceInSquare).toHaveBeenCalledWith(fakeSquare, fakeBuildingClass);
    });

    it('calls checkAdditionalRequirements', function () {
      var fakeSquare = jasmine.createSpy('fakeSquare');
      var fakeBuildingClass = jasmine.createSpy('fakeBuildingClass');
      var fakePlayerTechnologies = jasmine.createSpy('fakePlayerTechnologies');

      buildingsBuilder.checkBuildRequirements(fakeSquare, fakeBuildingClass, fakePlayerTechnologies);
      expect(buildingsBuilder._instance.checkAdditionalRequirements).toHaveBeenCalledWith(fakeSquare, fakeBuildingClass);
    });

    it('calls checkRequiredTechnologies', function () {
      var fakeSquare = jasmine.createSpy('fakeSquare');
      var fakeBuildingClass = jasmine.createSpy('fakeBuildingClass');
      var fakePlayerTechnologies = jasmine.createSpy('fakePlayerTechnologies');

      buildingsBuilder.checkBuildRequirements(fakeSquare, fakeBuildingClass, fakePlayerTechnologies);
      expect(buildingsBuilder._instance.checkRequiredTechnologies)
        .toHaveBeenCalledWith(fakeBuildingClass, fakePlayerTechnologies);
    });

    it('returns checks result conjuncted', function () {
      var fakeSquare = jasmine.createSpy('fakeSquare');
      var fakeBuildingClass = jasmine.createSpy('fakeBuildingClass');

      var result = buildingsBuilder.checkBuildRequirements(fakeSquare, fakeBuildingClass);
      expect(result).toBeTrue();

      buildingsBuilder._instance.checkMaxNumberPerResourceInSquare =
        jasmine.createSpy('checkMaxNumberPerResourceInSquareReturningFalse').and.returnValue(false);

      var result2 = buildingsBuilder.checkBuildRequirements(fakeSquare, fakeBuildingClass);
      expect(result2).toBeFalse();
    });

  });

  describe('build', function () {

    var fakeSquare;
    var fakeBlock;
    var FakeBuildingClass_;

    beforeEach(function () {
      FakeBuildingClass_ = function FakeBuildingClass() { return null; };
      FakeBuildingClass_.prototype.calcBuildDuration = jasmine.createSpy('FakeBuildingClass_.calcBuildDuration');
      FakeBuildingClass_.prototype.beginConstruction = jasmine.createSpy('FakeBuildingClass_.beginConstruction');
      fakeBlock = jasmine.createSpyObj('fakeBlock', ['addBuilding']);
      buildingsBuilder._instance.chooseBuildingBlock =
        jasmine.createSpy('buildingsBuilder.chooseBuildingBlock').and.returnValue(fakeBlock);
      fakeSquare = jasmine.createSpyObj('fakeSquare', ['player', 'addBuilding']);
      buildingsBuilder.checkBuildRequirements =
        jasmine.createSpy('checkBuildRequirements').and.returnValue(true);
      fakeStorage.has.and.returnValue(true);
      fakeStorage.hasGold.and.returnValue(true);
      buildingsBuilder.getBuildCost =
        jasmine.createSpy('getBuildCost').and.returnValue({
          resources: {  },
          gold: 0,
          duration: 0
        });
      playerProvider.getPlayerTechnologies =
        jasmine.createSpy('playerProvider.getPlayerTechnologies').and.returnValue([]);
    });

    it('returns false if checkBuildRequirements returns false', function () {
      buildingsBuilder.checkBuildRequirements =
        jasmine.createSpy('checkBuildRequirements').and.returnValue(false);

      var result = buildingsBuilder.build(fakeSquare, FakeBuildingClass_);
      expect(result).toBeFalse();
    });

    it('returns false if there is not enough resources in storage', function () {
      fakeStorage.has.and.returnValue(false);

      var result = buildingsBuilder.build(fakeSquare, FakeBuildingClass_);
      expect(result).toBeFalse();
    });

    it('returns false if there is not enough gold in storage', function () {
      fakeStorage.hasGold.and.returnValue(false);

      var result = buildingsBuilder.build(fakeSquare, FakeBuildingClass_);
      expect(result).toBeFalse();
    });

    it('returns true if build requirements are satisfied ' +
       'and there are enough resources and gold in storage', function () {
      var result = buildingsBuilder.build(fakeSquare, FakeBuildingClass_);
      expect(result).toBeTrue();
    });

    it('calls addBuilding on both block and square', function () {
      buildingsBuilder.build(fakeSquare, FakeBuildingClass_);
      expect(fakeBlock.addBuilding).toHaveBeenCalled();
      expect(fakeSquare.addBuilding).toHaveBeenCalled();
    });

    it('calls take and takeGold on storage', function () {
      buildingsBuilder.build(fakeSquare, FakeBuildingClass_);
      expect(fakeStorage.take).toHaveBeenCalled();
      expect(fakeStorage.takeGold).toHaveBeenCalled();
    });

    it('calls calcBuildDuration and beginConstruction on building', function () {
      expect(FakeBuildingClass_.prototype.calcBuildDuration).not.toHaveBeenCalled();
      expect(FakeBuildingClass_.prototype.beginConstruction).not.toHaveBeenCalled();
      buildingsBuilder.build(fakeSquare, FakeBuildingClass_);
      expect(FakeBuildingClass_.prototype.calcBuildDuration).toHaveBeenCalled();
      expect(FakeBuildingClass_.prototype.beginConstruction).toHaveBeenCalled();
    });

  });

  describe('getAvailableBuildingClasses', function () {

    beforeEach(function () {
      buildingClassRegister.buildingClasses = {};
      spyOn(buildingsBuilder._instance, 'checkRequiredSquareLevel').and.returnValue(true);
      spyOn(buildingsBuilder._instance, 'checkMaxNumberPerSquareSize').and.returnValue(true);
      spyOn(buildingsBuilder._instance, 'checkMaxNumberPerResourceInSquare').and.returnValue(true);
      spyOn(buildingsBuilder._instance, 'checkAdditionalRequirements').and.returnValue(true);
      spyOn(buildingsBuilder._instance, 'checkRequiredTechnologies').and.returnValue(true);
      spyOn(buildingsBuilder, 'getBuildCost').and.returnValue({});
      spyOn(playerProvider, 'getPlayerTechnologies').and.returnValue({});
      fakeStorage.has.and.returnValue(true);
      fakeStorage.hasGold.and.returnValue(true);
    });

    it('returns [] if no building is registered in buildingClassRegister', function () {
      var result = buildingsBuilder.getAvailableBuildingClasses(jasmine.createSpy('square'));
      expect(result).toEqual([]);
    });

    it('returns [] if one building class is registered in buildingClassRegister ' +
       'and all conditions except required square level are met', function () {
      function FakeBuilding() {}
      buildingClassRegister.buildingClasses = {'FakeBuilding': FakeBuilding};
      buildingsBuilder._instance.checkRequiredSquareLevel.and.returnValue(false);
      var result = buildingsBuilder.getAvailableBuildingClasses(jasmine.createSpy('square'));
      expect(result).toEqual([]);
    });

    it('returns array containing 1 building class if one building class is registered in buildingClassRegister ' +
       'and all conditions are met', function () {
      function FakeBuilding() {}
      buildingClassRegister.buildingClasses = {'FakeBuilding': FakeBuilding};
      var result = buildingsBuilder.getAvailableBuildingClasses(jasmine.createSpy('square'));
      expect(result).toBeArray();
      expect(result.length).toBe(1);
      expect(result[0]).toEqual(jasmine.objectContaining({
        buildingClass: FakeBuilding
      }));
    });

    it('returns array containing 1 building class with maxNumPerSize: false ' +
       'if one building class is registered in buildingClassRegister ' +
       'and all conditions except max number per square size are met', function () {
      function FakeBuilding() {}
      buildingClassRegister.buildingClasses = {'FakeBuilding': FakeBuilding};
      buildingsBuilder._instance.checkMaxNumberPerSquareSize.and.returnValue(false);
      var result = buildingsBuilder.getAvailableBuildingClasses(jasmine.createSpy('square'));
      expect(result).toBeArray();
      expect(result.length).toBe(1);
      expect(result[0]).toEqual(jasmine.objectContaining({
        buildingClass: FakeBuilding,
        conditionsSatisfied: {
          maxNumPerSize: false,
          maxNumPerRes: true,
          additionalReq: true,
          resCost: true,
          goldCost: true,
          techs: true
        }
      }));
    });

    it('returns array containing 1 building class with maxNumPerRes: false ' +
       'if one building class is registered in buildingClassRegister ' +
       'and all conditions except max number per resource in square are met', function () {
      function FakeBuilding() {}
      buildingClassRegister.buildingClasses = {'FakeBuilding': FakeBuilding};
      buildingsBuilder._instance.checkMaxNumberPerResourceInSquare.and.returnValue(false);
      var result = buildingsBuilder.getAvailableBuildingClasses(jasmine.createSpy('square'));
      expect(result).toBeArray();
      expect(result.length).toBe(1);
      expect(result[0]).toEqual(jasmine.objectContaining({
        buildingClass: FakeBuilding,
        conditionsSatisfied: {
          maxNumPerSize: true,
          maxNumPerRes: false,
          additionalReq: true,
          resCost: true,
          goldCost: true,
          techs: true
        }
      }));
    });

    it('returns array containing 1 building class with additionalReq: false ' +
       'if one building class is registered in buildingClassRegister ' +
       'and all conditions except additional requirements are met', function () {
      function FakeBuilding() {}
      buildingClassRegister.buildingClasses = {'FakeBuilding': FakeBuilding};
      buildingsBuilder._instance.checkAdditionalRequirements.and.returnValue(false);
      var result = buildingsBuilder.getAvailableBuildingClasses(jasmine.createSpy('square'));
      expect(result).toBeArray();
      expect(result.length).toBe(1);
      expect(result[0]).toEqual(jasmine.objectContaining({
        buildingClass: FakeBuilding,
        conditionsSatisfied: {
          maxNumPerSize: true,
          maxNumPerRes: true,
          additionalReq: false,
          resCost: true,
          goldCost: true,
          techs: true
        }
      }));
    });

    it('returns array containing 1 building class with resCost: false ' +
       'if one building class is registered in buildingClassRegister ' +
       'and all conditions are met but there is not enough resources in the storage', function () {
      function FakeBuilding() {}
      buildingClassRegister.buildingClasses = {'FakeBuilding': FakeBuilding};
      fakeStorage.has.and.returnValue(false);
      var result = buildingsBuilder.getAvailableBuildingClasses(jasmine.createSpy('square'));
      expect(result).toBeArray();
      expect(result.length).toBe(1);
      expect(result[0]).toEqual(jasmine.objectContaining({
        buildingClass: FakeBuilding,
        conditionsSatisfied: {
          maxNumPerSize: true,
          maxNumPerRes: true,
          additionalReq: true,
          resCost: false,
          goldCost: true,
          techs: true
        }
      }));
    });

    it('returns array containing 1 building class with goldCost: false ' +
       'if one building class is registered in buildingClassRegister ' +
       'and all conditions are met but there is not enough gold in the storage', function () {
      function FakeBuilding() {}
      buildingClassRegister.buildingClasses = {'FakeBuilding': FakeBuilding};
      fakeStorage.hasGold.and.returnValue(false);
      var result = buildingsBuilder.getAvailableBuildingClasses(jasmine.createSpy('square'));
      expect(result).toBeArray();
      expect(result.length).toBe(1);
      expect(result[0]).toEqual(jasmine.objectContaining({
        buildingClass: FakeBuilding,
        conditionsSatisfied: {
          maxNumPerSize: true,
          maxNumPerRes: true,
          additionalReq: true,
          resCost: true,
          goldCost: false,
          techs: true
        }
      }));
    });

    it('returns array containing 1 building class with techs: false ' +
       'if one building class is registered in buildingClassRegister ' +
       'and all conditions are met but there is not enough gold in the storage', function () {
      function FakeBuilding() {}
      buildingClassRegister.buildingClasses = {'FakeBuilding': FakeBuilding};
      buildingsBuilder._instance.checkRequiredTechnologies.and.returnValue(false);
      var result = buildingsBuilder.getAvailableBuildingClasses(jasmine.createSpy('square'));
      expect(result).toBeArray();
      expect(result.length).toBe(1);
      expect(result[0]).toEqual(jasmine.objectContaining({
        buildingClass: FakeBuilding,
        conditionsSatisfied: {
          maxNumPerSize: true,
          maxNumPerRes: true,
          additionalReq: true,
          resCost: true,
          goldCost: true,
          techs: false
        }
      }));
    });

    it('returns array containing 2 building classes with goldCost: false and maxNumPerRes: false ' +
       'if two building classes are registered in buildingClassRegister ' +
       'and all conditions except max number per resource in square are met ' +
       'and there is not enough gold in the storage', function () {
      function FakeBuilding1() {}
      function FakeBuilding2() {}
      buildingClassRegister.buildingClasses = {'FakeBuilding1': FakeBuilding1, 'FakeBuilding2': FakeBuilding2};
      buildingsBuilder._instance.checkMaxNumberPerResourceInSquare.and.returnValue(false);
      fakeStorage.hasGold.and.returnValue(false);
      var result = buildingsBuilder.getAvailableBuildingClasses(jasmine.createSpy('square'));
      expect(result).toBeArray();
      expect(result.length).toBe(2);
      expect(result[0]).toEqual(jasmine.objectContaining({
        buildingClass: FakeBuilding1,
        conditionsSatisfied: {
          maxNumPerSize: true,
          maxNumPerRes: false,
          additionalReq: true,
          resCost: true,
          goldCost: false,
          techs: true
        }
      }));
      expect(result[1]).toEqual(jasmine.objectContaining({
        buildingClass: FakeBuilding2,
        conditionsSatisfied: {
          maxNumPerSize: true,
          maxNumPerRes: false,
          additionalReq: true,
          resCost: true,
          goldCost: false,
          techs: true
        }
      }));
    });

    it('returns array containing 1 building class ' +
       'if two building classes are registered in buildingClassRegister ' +
       'but only one of them passes required square level check', function () {
      function FakeBuilding1() {}
      function FakeBuilding2() {}
      buildingClassRegister.buildingClasses = {'FakeBuilding1': FakeBuilding1, 'FakeBuilding2': FakeBuilding2};
      buildingsBuilder._instance.checkRequiredSquareLevel.and.callFake(function (square, buildingClass) {
        return buildingClass === FakeBuilding2;
      });
      var result = buildingsBuilder.getAvailableBuildingClasses(jasmine.createSpy('square'));
      expect(result).toBeArray();
      expect(result.length).toBe(1);
      expect(result[0]).toEqual(jasmine.objectContaining({
        buildingClass: FakeBuilding2
      }));
    });

  });

});