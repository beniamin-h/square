'use strict';

angular.module('square').factory('buildingsBuilder', ['storageProvider', 'ArrayUtils', 'playerProvider', 'buildingClassRegister',
    function (storageProvider, ArrayUtils, playerProvider, buildingClassRegister) {

  var that = this;

  that.getMaxNumberPerResourceInSquare = function (buildingClass) {
    return buildingClass.prototype.maxNumberPerResourceInSquare;  // may be affected by character skills or buildings etc.
  };

  that.getSquareResourceCount = function (square, resName) {
    return square.blocks.filter(function (block) {
      return block.kind.mainResource === resName;
    }).length;
  };

  that.checkMaxNumberPerResourceInSquare = function (square, buildingClass) {
    var maxNumberPerResourceInSquare = that.getMaxNumberPerResourceInSquare(buildingClass);
    for (var resName in maxNumberPerResourceInSquare) {
      var limitPerResource = maxNumberPerResourceInSquare[resName];
      var resourceCount = that.getSquareResourceCount(square, resName);
      if (resourceCount * limitPerResource <= square.getBuildingCountByClass(buildingClass.name)) {
        return false;
      }
    }
    return true;
  };

  that.checkMaxNumberPerSquareSize = function (square, buildingClass) {
    return buildingClass.prototype.maxNumberPerSquareSize[square.level] > square.getBuildingCountByClass(buildingClass.name);
  };

  that.checkRequiredSquareLevel = function (square, buildingClass) {
    return buildingClass.prototype.requiredSquareLevel <= square.level;
  };

  that.checkAdditionalRequirements = function (square, buildingClass) {
    for (var reqName in buildingClass.prototype.additionalRequirements) {
      if (!buildingClass.prototype.additionalRequirements[reqName](square)) {
        return false;
      }
    }
    return true;
  };

  that.checkRequiredTechnologies = function (buildingClass, playerTechnologies) {
    var result = true;
    buildingClass.prototype.requiredTechnologies.forEach(function (reqTech) {
      if (playerTechnologies.indexOf(reqTech) === -1) {
        result = false;
      }
    });
    return result;
  };

  that.sortDescendingByEmptySlotCount = function (blocks, buildingClassName) {
    blocks.sort(function (blockA, blockB) {
      var blockAValue = blockA.getBuildingCountByClass(buildingClassName);
      var blockBValue = blockB.getBuildingCountByClass(buildingClassName);
      return blockAValue - blockBValue;
    });
  };

  that.sortRichFirst = function (blocks) {
    blocks.sort(function richFirst(blockA, blockB) {
      var blockAValue = +blockA.rich;
      var blockBValue = +blockB.rich;
      return blockBValue - blockAValue;
    });
  };

  that._chooseResourceBlock = function (blocks, resName, buildingClassName, limitPerResource) {
    var suitableBlocks = blocks.filter(function resourceBlocks(block) {
        return block.kind.mainResource === resName;
      })
      .filter(function emptySlots(block) {
        return limitPerResource > block.getBuildingCountByClass(buildingClassName);
      });

    ArrayUtils.shuffleInplace(suitableBlocks);

    that.sortDescendingByEmptySlotCount(suitableBlocks, buildingClassName);
    that.sortRichFirst(suitableBlocks);

    if (!suitableBlocks.length) {
      throw new Error('No suitableBlocks in square - building should not be possible.');
    }

    return suitableBlocks[0];
  };

  that.chooseBuildingBlockByMainResource = function (square, buildingClassName, maxNumberPerResourceInSquare) {
    var suitableBlocks = [];
    for (var resName in maxNumberPerResourceInSquare) {
      suitableBlocks.push(that._chooseResourceBlock(
        square.blocks, resName, buildingClassName, maxNumberPerResourceInSquare[resName]));
    }

    if (!suitableBlocks.length) {
      throw new Error('No suitableBlocks in square - building should not be possible.');
    }

    if (suitableBlocks.length > 1) {
      ArrayUtils.shuffleInplace(suitableBlocks);
      that.sortRichFirst(suitableBlocks);
    }

    return suitableBlocks[0];
  };

  that.chooseBuildingBlockByNumberOfBuildingsInBlock = function (square, buildingClassName) {
    var blocks = square.blocks.slice();  // copy all blocks

    ArrayUtils.shuffleInplace(blocks);

    function emptierBlocksFirst(blockA, blockB) {
      var diff = blockA.getBuildingCountByClass(buildingClassName) - blockB.getBuildingCountByClass(buildingClassName);
      if (diff !== 0) {
        return diff;
      }
      return blockA.buildings.length - blockB.buildings.length;
    }

    blocks.sort(emptierBlocksFirst);

    return blocks[0];
  };

  that.chooseBuildingBlock = function (square, buildingClass) {
    if (buildingClass.prototype.getSpecialBuildingPosition) {
      return buildingClass.prototype.getSpecialBuildingPosition();
    }

    var maxNumberPerResourceInSquare = that.getMaxNumberPerResourceInSquare(buildingClass);
    if (Object.keys(maxNumberPerResourceInSquare).length) {
      return that.chooseBuildingBlockByMainResource(square, buildingClass.name, maxNumberPerResourceInSquare);
    }

    return that.chooseBuildingBlockByNumberOfBuildingsInBlock(square, buildingClass.name);
  };

  return {
    getBuildCost: function (buildingClass) {
      return {
        resources: buildingClass.prototype._baseResourceBuildCost,
        gold: buildingClass.prototype._baseGoldBuildCost,
        duration: buildingClass.prototype._baseBuildDuration
      };
    },
    checkBuildRequirements: function (square, buildingClass, playerTechnologies) {
      return that.checkRequiredSquareLevel(square, buildingClass) &&
        that.checkMaxNumberPerSquareSize(square, buildingClass) &&
        that.checkMaxNumberPerResourceInSquare(square, buildingClass) &&
        that.checkAdditionalRequirements(square, buildingClass) &&
        that.checkRequiredTechnologies(buildingClass, playerTechnologies);
    },
    build: function (square, buildingClass) {
      var cost = this.getBuildCost(buildingClass);
      if (this.checkBuildRequirements(
            square, buildingClass, playerProvider.getPlayerTechnologies(square.player)) &&
          storageProvider.getStorage(square.player).has(cost.resources) &&
          storageProvider.getStorage(square.player).hasGold(cost.gold)) {

        storageProvider.getStorage(square.player).take(cost.resources);
        storageProvider.getStorage(square.player).takeGold(cost.gold);

        var buildingBlock = that.chooseBuildingBlock(square, buildingClass);
        var building = new buildingClass(square, buildingBlock);
        buildingBlock.addBuilding(building);
        square.addBuilding(building);
        building.calcBuildDuration();
        building.beginConstruction();
        return true;
      }
      return false;
    },
    getAvailableBuildingClasses: function (square) {
      var availableBuildings = [];
      Object.keys(buildingClassRegister.buildingClasses).forEach(function (buildingClassName) {
        var buildingClass = buildingClassRegister.buildingClasses[buildingClassName];
        if (!that.checkRequiredSquareLevel(square, buildingClass)) {
          return;
        }
        var availableBuilding = {
          buildingClass: buildingClass,
          conditionsSatisfied: {
            maxNumPerSize: true,
            maxNumPerRes: true,
            additionalReq: true,
            resCost: true,
            goldCost: true,
            techs: true
          }
        };
        if (!that.checkMaxNumberPerSquareSize(square, buildingClass)) {
          availableBuilding.conditionsSatisfied.maxNumPerSize = false;
        }
        if (!that.checkMaxNumberPerResourceInSquare(square, buildingClass)) {
          availableBuilding.conditionsSatisfied.maxNumPerRes = false;
        }
        if (!that.checkAdditionalRequirements(square, buildingClass)) {
          availableBuilding.conditionsSatisfied.additionalReq = false;
        }
        var cost = this.getBuildCost(buildingClass);
        if (!storageProvider.getStorage(square.player).has(cost.resources)) {
          availableBuilding.conditionsSatisfied.resCost = false;
        }
        if (!storageProvider.getStorage(square.player).hasGold(cost.gold)) {
          availableBuilding.conditionsSatisfied.goldCost = false;
        }
        if (!that.checkRequiredTechnologies(buildingClass, playerProvider.getPlayerTechnologies(square.player))) {
          availableBuilding.conditionsSatisfied.techs = false;
        }
        availableBuildings.push(availableBuilding);
      }, this);
      return availableBuildings;
    },
    _instance: this
  };
}]);