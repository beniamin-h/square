'use strict';

describe('BuildingSchema', function () {
  beforeEach(module('square'));

  var Building, EventEmitter, storageProvider;
  var fakeStorage;

  beforeEach(inject(function (_Building_, _storageProvider_) {
    Building = _Building_;
    EventEmitter = require('events');
    storageProvider = _storageProvider_;

    fakeStorage = {
      take: jasmine.createSpy('storage.take'),
      takeGold: jasmine.createSpy('storage.takeGold'),
      has: jasmine.createSpy('storage.has'),
      hasGold: jasmine.createSpy('storage.hasGold'),
      put: jasmine.createSpy('storage.put'),
      changeGoldIncome: jasmine.createSpy('storage.changeGoldIncome')
    };
    storageProvider.getStorage = jasmine.createSpy('getStorage')
      .and.returnValue(fakeStorage);
  }));

  describe('constructor', function () {

    it('sets empty resourceProduction, resourceUpkeep and notSatisfiedStartConditions', function () {
      Building.prototype.resourceProduction = jasmine.createSpy('resourceProduction');
      Building.prototype.resourceUpkeep = jasmine.createSpy('resourceUpkeep');
      Building.prototype.notSatisfiedStartConditions = jasmine.createSpy('notSatisfiedStartConditions');
      var building = new Building();
      expect(building.resourceProduction).toEqual({});
      expect(building.resourceUpkeep).toEqual({});
      expect(building.notSatisfiedStartConditions).toEqual([]);
    });

  });

  describe('calcResourceProduction', function () {

    it('sets resourceProduction to {} if _baseResourceProduction is empty', function () {
      var building = new Building();
      building.resourceProduction = jasmine.createSpy('resourceProduction');
      building._baseResourceProduction = {};
      building.calcResourceProduction();
      expect(building.resourceProduction).toEqual({});
    });

    it('sets resourceProduction to {metal: 1} if _baseResourceProduction is {metal: 1}', function () {
      var building = new Building();
      building.resourceProduction = jasmine.createSpy('resourceProduction');
      building._baseResourceProduction = {metal: 1};
      building.calcResourceProduction();
      expect(building.resourceProduction).toEqual({metal: 1});
    });

    it('sets resourceProduction to {metal: 5, wood: 3, food: 1} ' +
       'if _baseResourceProduction is {metal: 5, wood: 3, food: 1}', function () {
      var building = new Building();
      building.resourceProduction = jasmine.createSpy('resourceProduction');
      building._baseResourceProduction = {metal: 5, wood: 3, food: 1};
      building.calcResourceProduction();
      expect(building.resourceProduction).toEqual({metal: 5, wood: 3, food: 1});
    });

  });

  describe('calcResourceUpkeep', function () {

    it('sets resourceUpkeep to {} if _baseResourceUpkeep is empty', function () {
      var building = new Building();
      building.resourceUpkeep = jasmine.createSpy('resourceUpkeep');
      building._baseResourceUpkeep = {};
      building.calcResourceUpkeep();
      expect(building.resourceUpkeep).toEqual({});
    });

    it('sets resourceUpkeep to {wood: 2} if _baseResourceUpkeep is {wood: 2}', function () {
      var building = new Building();
      building.resourceUpkeep = jasmine.createSpy('resourceUpkeep');
      building._baseResourceUpkeep = {wood: 2};
      building.calcResourceUpkeep();
      expect(building.resourceUpkeep).toEqual({wood: 2});
    });

    it('sets resourceUpkeep to {wood: 3, stone: 10} ' +
       'if _baseResourceUpkeep is {wood: 3, stone: 10}', function () {
      var building = new Building();
      building.resourceUpkeep = jasmine.createSpy('resourceUpkeep');
      building._baseResourceUpkeep = {wood: 3, stone: 10};
      building.calcResourceUpkeep();
      expect(building.resourceUpkeep).toEqual({wood: 3, stone: 10});
    });

  });

  describe('calcGoldIncome', function () {

    it('sets goldIncome to 0 if _baseGoldIncomePerSquareLevel is empty', function () {
      var building = new Building(null, { level: 2 });
      building.goldIncome = jasmine.createSpy('goldIncome');
      building._baseGoldIncomePerSquareLevel = {};
      building.calcGoldIncome();
      expect(building.goldIncome).toBe(0);
    });

    it('sets goldIncome to 0 if _baseGoldIncomePerSquareLevel does not contain square level', function () {
      var square = { level: 3 };
      var building = new Building(null, square);
      building.goldIncome = jasmine.createSpy('goldIncome');
      building._baseGoldIncomePerSquareLevel = { 2: 999, 4: 50 };
      building.calcGoldIncome();
      expect(building.goldIncome).toBe(0);
    });

    it('sets goldIncome to 5 if square is 2 and _baseGoldIncomePerSquareLevel is {2: 5}', function () {
      var square = { level: 2 };
      var building = new Building(null, square);
      building.goldIncome = jasmine.createSpy('goldIncome');
      building._baseGoldIncomePerSquareLevel = { 2: 5 };
      building.calcGoldIncome();
      expect(building.goldIncome).toBe(5);
    });

    it('sets goldIncome to 7 if square is 5 and _baseGoldIncomePerSquareLevel is {2: 1, 3: 2, 5: 7}', function () {
      var square = { level: 5 };
      var building = new Building(null, square);
      building.goldIncome = jasmine.createSpy('goldIncome');
      building._baseGoldIncomePerSquareLevel = { 2: 1, 3: 2, 5: 7 };
      building.calcGoldIncome();
      expect(building.goldIncome).toBe(7);
    });

  });

  describe('calcGoldUpkeep', function () {

    it('sets goldUpkeep to 0 if _baseGoldUpkeepPerSquareLevel is empty', function () {
      var building = new Building(null, { level: 3 });
      building.goldUpkeep = jasmine.createSpy('goldUpkeep');
      building._baseGoldUpkeepPerSquareLevel = {};
      building.calcGoldUpkeep();
      expect(building.goldUpkeep).toBe(0);
    });

    it('sets goldUpkeep to 0 if _baseGoldUpkeepPerSquareLevel does not contain square level', function () {
      var square = { level: 5 };
      var building = new Building(null, square);
      building.goldUpkeep = jasmine.createSpy('goldUpkeep');
      building._baseGoldUpkeepPerSquareLevel = { 2: 1, 4: 60 };
      building.calcGoldUpkeep();
      expect(building.goldUpkeep).toBe(0);
    });

    it('sets goldUpkeep to 8 if square is 3 and _baseGoldUpkeepPerSquareLevel is {3: 8}', function () {
      var square = { level: 3 };
      var building = new Building(null, square);
      building.goldUpkeep = jasmine.createSpy('goldUpkeep');
      building._baseGoldUpkeepPerSquareLevel = { 3: 8 };
      building.calcGoldUpkeep();
      expect(building.goldUpkeep).toBe(8);
    });

    it('sets goldUpkeep to 17 if square is 4 and _baseGoldUpkeepPerSquareLevel is {2: 1, 4: 17}', function () {
      var square = { level: 4 };
      var building = new Building(null, square);
      building.goldUpkeep = jasmine.createSpy('goldUpkeep');
      building._baseGoldUpkeepPerSquareLevel = { 2: 1, 4: 17 };
      building.calcGoldUpkeep();
      expect(building.goldUpkeep).toBe(17);
    });

  });

  describe('calcBuildDuration', function () {

    it('sets buildDuration to 5 if _baseBuildDuration is 5', function () {
      var building = new Building();
      building.buildDuration = jasmine.createSpy('buildDuration');
      building._baseBuildDuration = 5;
      building.calcBuildDuration();
      expect(building.buildDuration).toEqual(5);
    });

    it('sets buildDuration to 15 if _baseBuildDuration is 15', function () {
      var building = new Building();
      building.buildDuration = jasmine.createSpy('buildDuration');
      building._baseBuildDuration = 15;
      building.calcBuildDuration();
      expect(building.buildDuration).toEqual(15);
    });

  });

  describe('beginConstruction', function () {

    it('sets calling proceedConstruction on proceedingConstructions event', function () {
      var square = { player: { eventEmitter: new EventEmitter() }};
      var building = new Building(null, square);
      spyOn(building, 'proceedConstruction');
      building.beginConstruction();
      expect(building.proceedConstruction).not.toHaveBeenCalled();
      square.player.eventEmitter.emit('proceedingConstructions', square.player);
      expect(building.proceedConstruction).toHaveBeenCalled();
    });

  });

  describe('finishConstruction', function () {

    var building;

    beforeEach(function () {
      building = new Building();
      spyOn(building, 'startOperating');
      spyOn(building, 'tryToStartOperatingNextTime');
    });

    it('sets constructionProgress to 1.0', function () {
      building.constructionProgress = jasmine.createSpy('constructionProgress');
      building.finishConstruction();
      expect(building.constructionProgress).toBe(1.0);
    });

    it('calls startOperating', function () {
      building.finishConstruction();
      expect(building.startOperating).toHaveBeenCalledWith();
    });

    it('calls tryToStartOperatingNextTime if startOperating returns false', function () {
      building.startOperating.and.returnValue(false);
      building.finishConstruction();
      expect(building.tryToStartOperatingNextTime).toHaveBeenCalledWith();
    });

    it('does not call tryToStartOperatingNextTime if startOperating returns true', function () {
      building.startOperating.and.returnValue(true);
      building.finishConstruction();
      expect(building.tryToStartOperatingNextTime).not.toHaveBeenCalled();
    });

  });

  describe('tryToStartOperatingNextTime', function () {

    it('sets calling startOperating on resumingStoppedBuildings event', function () {
      var square = { player: { eventEmitter: new EventEmitter() }};
      var building = new Building(null, square);
      spyOn(building, 'startOperating');
      building.tryToStartOperatingNextTime();
      expect(building.startOperating).not.toHaveBeenCalled();
      square.player.eventEmitter.emit('resumingStoppedBuildings', square.player);
      expect(building.startOperating).toHaveBeenCalled();
    });

    it('unset calling startOperating on resumingStoppedBuildings event ' +
       'if startOperating returns true', function () {
      var square = { player: { eventEmitter: new EventEmitter() }};
      var building = new Building(null, square);
      spyOn(building, 'startOperating').and.returnValue(true);
      building.tryToStartOperatingNextTime();
      square.player.eventEmitter.emit('resumingStoppedBuildings', square.player);
      expect(building.startOperating).toHaveBeenCalled();
      building.startOperating.calls.reset();
      square.player.eventEmitter.emit('resumingStoppedBuildings', square.player);
      expect(building.startOperating).not.toHaveBeenCalled();
    });

    it('unset calling startOperating on resumingStoppedBuildings event ' +
       'if building is disabled even if startOperating returns false', function () {
      var square = { player: { eventEmitter: new EventEmitter() }};
      var building = new Building(null, square);
      spyOn(building, 'startOperating').and.returnValue(false);
      building.disabled = true;
      building.tryToStartOperatingNextTime();
      square.player.eventEmitter.emit('resumingStoppedBuildings', square.player);
      expect(building.startOperating).not.toHaveBeenCalled();
      building.startOperating.calls.reset();
      square.player.eventEmitter.emit('resumingStoppedBuildings', square.player);
      expect(building.startOperating).not.toHaveBeenCalled();
    });

    it('does not unset calling startOperating on resumingStoppedBuildings event ' +
       'if startOperating returns false and building is not disabled', function () {
      var square = { player: { eventEmitter: new EventEmitter() }};
      var building = new Building(null, square);
      spyOn(building, 'startOperating').and.returnValue(false);
      building.disabled = false;
      building.tryToStartOperatingNextTime();
      square.player.eventEmitter.emit('resumingStoppedBuildings', square.player);
      expect(building.startOperating).toHaveBeenCalled();
      building.startOperating.calls.reset();
      square.player.eventEmitter.emit('resumingStoppedBuildings', square.player);
      expect(building.startOperating).toHaveBeenCalled();
    });

  });

  describe('proceedConstruction', function () {

    var building;

    beforeEach(function () {
      building = new Building();
      spyOn(building, 'finishConstruction');
    });

    it('sets constructionProgress to 1.0 if buildDuration is 0', function () {
      building.buildDuration = 0;
      building.constructionProgress = jasmine.createSpy('constructionProgress');
      building.proceedConstruction();
      expect(building.constructionProgress).toBe(1.0);
    });

    it('returns true if buildDuration is 0', function () {
      building.buildDuration = 0;
      var result = building.proceedConstruction();
      expect(result).toBeTrue();
    });

    it('adds 0.25 to constructionProgress if buildDuration is 4', function () {
      building.buildDuration = 4;
      building.constructionProgress = 0.3;
      building.proceedConstruction();
      expect(building.constructionProgress).toBe(0.3 + 0.25);
    });

    it('adds 1/3 to constructionProgress if buildDuration is 3', function () {
      building.buildDuration = 3;
      building.constructionProgress = 0.1;
      building.proceedConstruction();
      expect(building.constructionProgress).toBe(0.1 + 1/3);
    });

    it('adds 1/99 to constructionProgress if buildDuration is 99', function () {
      building.buildDuration = 99;
      building.constructionProgress = 0.9;
      building.proceedConstruction();
      expect(building.constructionProgress).toBe(0.9 + 1/99);
    });

    it('calls finishConstruction if constructionProgress was 0.5 and buildDuration is 2', function () {
      building.buildDuration = 2;
      building.constructionProgress = 0.5;
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction if constructionProgress was 2/3 and buildDuration is 3', function () {
      building.buildDuration = 3;
      building.constructionProgress = 1/3 + 1/3;
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction if constructionProgress was 0.75 and buildDuration is 4', function () {
      building.buildDuration = 4;
      building.constructionProgress = 0.75;
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction if constructionProgress was 6/7 and buildDuration is 7', function () {
      building.buildDuration = 7;
      building.constructionProgress = 1/7 * 6;
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('does not call finishConstruction if constructionProgress was 0.49 and buildDuration is 2', function () {
      building.buildDuration = 2;
      building.constructionProgress = 0.49;
      building.proceedConstruction();
      expect(building.finishConstruction).not.toHaveBeenCalled();
    });

    it('does not call finishConstruction if constructionProgress was 1/3 and buildDuration is 3', function () {
      building.buildDuration = 3;
      building.constructionProgress = 1/3;
      building.proceedConstruction();
      expect(building.finishConstruction).not.toHaveBeenCalled();
    });

    it('does not call finishConstruction if constructionProgress was 0.65 and buildDuration is 3', function () {
      building.buildDuration = 3;
      building.constructionProgress = 0.65;
      building.proceedConstruction();
      expect(building.finishConstruction).not.toHaveBeenCalled();
    });

    it('does not call finishConstruction if constructionProgress was 24/33 and buildDuration is 4', function () {
      building.buildDuration = 4;
      building.constructionProgress = 1/33 * 24;
      building.proceedConstruction();
      expect(building.finishConstruction).not.toHaveBeenCalled();
    });

    it('does not call finishConstruction if constructionProgress was 5/6 and buildDuration is 7', function () {
      building.buildDuration = 7;
      building.constructionProgress = 1/6 * 5;
      building.proceedConstruction();
      expect(building.finishConstruction).not.toHaveBeenCalled();
    });

    it('calls finishConstruction once if constructionProgress has been called 2 times ' +
       'and buildDuration is 2', function () {
      building.buildDuration = 2;
      building.proceedConstruction();
      expect(building.finishConstruction).not.toHaveBeenCalled();
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction once if constructionProgress has been called 3 times ' +
       'and buildDuration is 3', function () {
      building.buildDuration = 3;
      building.proceedConstruction();
      expect(building.finishConstruction).not.toHaveBeenCalled();
      building.proceedConstruction();
      expect(building.finishConstruction).not.toHaveBeenCalled();
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction once if constructionProgress has been called 4 times ' +
       'and buildDuration is 4', function () {
      building.buildDuration = 4;
      for (var i = 0; i < 3; i++) {
        building.proceedConstruction();
        expect(building.finishConstruction).not.toHaveBeenCalled();
      }
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction once if constructionProgress has been called 6 times ' +
       'and buildDuration is 6', function () {
      building.buildDuration = 6;
      for (var i = 0; i < 5; i++) {
        building.proceedConstruction();
        expect(building.finishConstruction).not.toHaveBeenCalled();
      }
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction once if constructionProgress has been called 7 times ' +
       'and buildDuration is 7', function () {
      building.buildDuration = 7;
      for (var i = 0; i < 6; i++) {
        building.proceedConstruction();
        expect(building.finishConstruction).not.toHaveBeenCalled();
      }
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction once if constructionProgress has been called 17 times ' +
       'and buildDuration is 17', function () {
      building.buildDuration = 17;
      for (var i = 0; i < 16; i++) {
        building.proceedConstruction();
        expect(building.finishConstruction).not.toHaveBeenCalled();
      }
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction once if constructionProgress has been called 23 times ' +
       'and buildDuration is 23', function () {
      building.buildDuration = 23;
      for (var i = 0; i < 22; i++) {
        building.proceedConstruction();
        expect(building.finishConstruction).not.toHaveBeenCalled();
      }
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction once if constructionProgress has been called 40 times ' +
       'and buildDuration is 40', function () {
      building.buildDuration = 40;
      for (var i = 0; i < 39; i++) {
        building.proceedConstruction();
        expect(building.finishConstruction).not.toHaveBeenCalled();
      }
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction once if constructionProgress has been called 47 times ' +
       'and buildDuration is 47', function () {
      building.buildDuration = 47;
      for (var i = 0; i < 46; i++) {
        building.proceedConstruction();
        expect(building.finishConstruction).not.toHaveBeenCalled();
      }
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction once if constructionProgress has been called 93 times ' +
       'and buildDuration is 93', function () {
      building.buildDuration = 93;
      for (var i = 0; i < 92; i++) {
        building.proceedConstruction();
        expect(building.finishConstruction).not.toHaveBeenCalled();
      }
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction once if constructionProgress has been called 107 times ' +
       'and buildDuration is 107', function () {
      building.buildDuration = 107;
      for (var i = 0; i < 106; i++) {
        building.proceedConstruction();
        expect(building.finishConstruction).not.toHaveBeenCalled();
      }
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction once if constructionProgress has been called 113 times ' +
       'and buildDuration is 113', function () {
      building.buildDuration = 113;
      for (var i = 0; i < 112; i++) {
        building.proceedConstruction();
        expect(building.finishConstruction).not.toHaveBeenCalled();
      }
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction once if constructionProgress has been called 173 times ' +
       'and buildDuration is 173', function () {
      building.buildDuration = 173;
      for (var i = 0; i < 172; i++) {
        building.proceedConstruction();
        expect(building.finishConstruction).not.toHaveBeenCalled();
      }
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction once if constructionProgress has been called 187 times ' +
       'and buildDuration is 187', function () {
      building.buildDuration = 187;
      for (var i = 0; i < 186; i++) {
        building.proceedConstruction();
        expect(building.finishConstruction).not.toHaveBeenCalled();
      }
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction once if constructionProgress has been called 193 times ' +
       'and buildDuration is 193', function () {
      building.buildDuration = 193;
      for (var i = 0; i < 192; i++) {
        building.proceedConstruction();
        expect(building.finishConstruction).not.toHaveBeenCalled();
      }
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction once if constructionProgress has been called 200 times ' +
       'and buildDuration is 200', function () {
      building.buildDuration = 200;
      for (var i = 0; i < 199; i++) {
        building.proceedConstruction();
        expect(building.finishConstruction).not.toHaveBeenCalled();
      }
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction once if constructionProgress has been called 203 times ' +
       'and buildDuration is 203', function () {
      building.buildDuration = 203;
      for (var i = 0; i < 202; i++) {
        building.proceedConstruction();
        expect(building.finishConstruction).not.toHaveBeenCalled();
      }
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction once if constructionProgress has been called 300 times ' +
       'and buildDuration is 300', function () {
      building.buildDuration = 300;
      for (var i = 0; i < 299; i++) {
        building.proceedConstruction();
        expect(building.finishConstruction).not.toHaveBeenCalled();
      }
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction once if constructionProgress has been called 999 times ' +
       'and buildDuration is 999', function () {
      building.buildDuration = 999;
      for (var i = 0; i < 998; i++) {
        building.proceedConstruction();
        expect(building.finishConstruction).not.toHaveBeenCalled();
      }
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction once if constructionProgress has been called 1000 times ' +
       'and buildDuration is 1000', function () {
      building.buildDuration = 1000;
      for (var i = 0; i < 999; i++) {
        building.proceedConstruction();
        expect(building.finishConstruction).not.toHaveBeenCalled();
      }
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction once if constructionProgress has been called 1001 times ' +
       'and buildDuration is 1001', function () {
      building.buildDuration = 1001;
      for (var i = 0; i < 1000; i++) {
        building.proceedConstruction();
        expect(building.finishConstruction).not.toHaveBeenCalled();
      }
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction once if constructionProgress has been called 1002 times ' +
       'and buildDuration is 1002', function () {
      building.buildDuration = 1002;
      for (var i = 0; i < 1001; i++) {
        building.proceedConstruction();
        expect(building.finishConstruction).not.toHaveBeenCalled();
      }
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction once if constructionProgress has been called 1003 times ' +
       'and buildDuration is 1003', function () {
      building.buildDuration = 1003;
      for (var i = 0; i < 1002; i++) {
        building.proceedConstruction();
        expect(building.finishConstruction).not.toHaveBeenCalled();
      }
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

    it('calls finishConstruction once if constructionProgress has been called 1007 times ' +
       'and buildDuration is 1007', function () {
      building.buildDuration = 1007;
      for (var i = 0; i < 1006; i++) {
        building.proceedConstruction();
        expect(building.finishConstruction).not.toHaveBeenCalled();
      }
      building.proceedConstruction();
      expect(building.finishConstruction).toHaveBeenCalled();
    });

  });

  describe('turnOff', function () {

    it('sets building to disabled', function () {
      var building = new Building();
      spyOn(building, 'stopOperating');
      building.disabled = jasmine.createSpy('disabled');
      building.turnOff();
      expect(building.disabled).toBeTrue();
    });

    it('calls stopOperating', function () {
      var building = new Building();
      spyOn(building, 'stopOperating');
      building.turnOff();
      expect(building.stopOperating).toHaveBeenCalledWith();
    });

  });

  describe('turnOn', function () {

    var building;

    beforeEach(function () {
      building = new Building();
      spyOn(building, 'startOperating');
      spyOn(building, 'tryToStartOperatingNextTime');
    });

    it('sets building to enabled', function () {
      building.disabled = jasmine.createSpy('disabled');
      building.turnOn();
      expect(building.disabled).toBeFalse();
    });

    it('calls stopOperating', function () {
      building.turnOn();
      expect(building.startOperating).toHaveBeenCalledWith();
    });

    it('calls tryToStartOperatingNextTime if startOperating returns false', function () {
      building.startOperating.and.returnValue(false);
      building.turnOn();
      expect(building.tryToStartOperatingNextTime).toHaveBeenCalledWith();
    });

    it('does not call tryToStartOperatingNextTime if startOperating returns true', function () {
      building.startOperating.and.returnValue(true);
      building.turnOn();
      expect(building.tryToStartOperatingNextTime).not.toHaveBeenCalled();
    });

  });

  describe('stopOperating', function () {

    var square;
    var building;

    beforeEach(function () {
      square = jasmine.createSpyObj('square', ['calcResourceOutcomeTotal', 'calcGoldOutcomeTotal', 'player']);
      building = new Building(null, square);
      building.isOperating = true;
    });

    it('returns false if building is not operating', function () {
      building.isOperating = false;
      var result = building.stopOperating();
      expect(result).toBeFalse();
    });

    it('returns true if building is operating', function () {
      building.isOperating = true;
      var result = building.stopOperating();
      expect(result).toBeTrue();
    });

    it('sets isOperating to false if building is operating', function () {
      building.isOperating = true;
      building.stopOperating();
      expect(building.isOperating).toBeFalse();
    });

    it('does not change isOperating prop if building is not operating', function () {
      building.isOperating = false;
      building.stopOperating();
      expect(building.isOperating).toBeFalse();
    });

    it('calls take on storage', function () {
      var resourceProductionSpy = spyOn(building, 'resourceProduction');
      building.stopOperating();
      expect(storageProvider.getStorage).toHaveBeenCalledWith(square.player);
      expect(fakeStorage.take).toHaveBeenCalledWith(resourceProductionSpy);
    });

    it('calls changeGoldIncome on storage', function () {
      building.stopOperating();
      expect(storageProvider.getStorage).toHaveBeenCalledWith(square.player);
      expect(fakeStorage.changeGoldIncome).toHaveBeenCalledWith(jasmine.any(Number));
    });

    it('sets resourceProduction to none', function () {
      building.resourceProduction = jasmine.createSpy('resourceProduction');
      building.stopOperating();
      expect(building.resourceProduction).toEqual({});
    });

    it('sets goldIncome to 0', function () {
      building.goldIncome = jasmine.createSpy('goldIncome');
      building.stopOperating();
      expect(building.goldIncome).toEqual(0);
    });

    it('sets resourceUpkeep to none', function () {
      building.resourceUpkeep = jasmine.createSpy('resourceUpkeep');
      building.stopOperating();
      expect(building.resourceUpkeep).toEqual({});
    });

    it('sets goldUpkeep to 0', function () {
      building.goldUpkeep = jasmine.createSpy('goldUpkeep');
      building.stopOperating();
      expect(building.goldUpkeep).toEqual(0);
    });

    it('calls calcResourceOutcomeTotal on square', function () {
      building.stopOperating();
      expect(square.calcResourceOutcomeTotal).toHaveBeenCalledWith();
    });

    it('calls calcGoldOutcomeTotal on square', function () {
      building.stopOperating();
      expect(square.calcGoldOutcomeTotal).toHaveBeenCalledWith();
    });

    it('does not call take nor changeGoldIncome on storage if building is not operating', function () {
      building.isOperating = false;
      building.stopOperating();
      expect(fakeStorage.take).not.toHaveBeenCalled();
      expect(fakeStorage.changeGoldIncome).not.toHaveBeenCalled();
    });

    it('does not call calcResourceOutcomeTotal nor calcGoldOutcomeTotal on square ' +
       'if building is not operating', function () {
      building.isOperating = false;
      building.stopOperating();
      expect(square.calcResourceOutcomeTotal).not.toHaveBeenCalled();
      expect(square.calcGoldOutcomeTotal).not.toHaveBeenCalled();
    });

    it('does not change building props if building is not operating', function () {
      var resourceProductionSpy = jasmine.createSpy('resourceProduction');
      building.resourceProduction = resourceProductionSpy;
      var goldIncomeSpy = jasmine.createSpy('goldIncome');
      building.goldIncome = goldIncomeSpy;
      var resourceUpkeepSpy = jasmine.createSpy('resourceUpkeep');
      building.resourceUpkeep = resourceUpkeepSpy;
      var goldUpkeepSpy = jasmine.createSpy('goldUpkeep');
      building.goldUpkeep = goldUpkeepSpy;
      building.isOperating = false;
      building.stopOperating();
      expect(building.resourceProduction).toBe(resourceProductionSpy);
      expect(building.goldIncome).toBe(goldIncomeSpy);
      expect(building.resourceUpkeep).toBe(resourceUpkeepSpy);
      expect(building.goldUpkeep).toBe(goldUpkeepSpy);
    });

  });

  describe('startOperating', function () {

    var building;
    var square;

    beforeEach(function () {
      square = jasmine.createSpyObj('square', ['calcResourceOutcomeTotal', 'calcGoldOutcomeTotal', 'player']);
      building = new Building(null, square);
      spyOn(building, 'calcResourceProduction');
      spyOn(building, 'calcGoldIncome');
      spyOn(building, 'calcResourceUpkeep');
      spyOn(building, 'calcGoldUpkeep');
      building.disabled = false;
      building.isOperating = false;
      building.notSatisfiedStartConditions = [];
      fakeStorage.has.and.returnValue(true);
      fakeStorage.hasGold.and.returnValue(true);
    });

    it('returns true if building is enabled and not operating yet and ' +
       'player has upkeep resources and upkeep gold', function () {
      var result = building.startOperating();
      expect(result).toBeTrue();
    });

    it('returns false if building is disabled', function () {
      building.disabled = true;
      var result = building.startOperating();
      expect(result).toBeFalse();
    });

    it('returns false if building is already operating', function () {
      building.isOperating = true;
      var result = building.startOperating();
      expect(result).toBeFalse();
    });

    it('returns false if player does not have upkeep resources', function () {
      fakeStorage.has.and.returnValue(false);
      var result = building.startOperating();
      expect(result).toBeFalse();
    });

    it('returns false if player does not have upkeep gold', function () {
      fakeStorage.hasGold.and.returnValue(false);
      var result = building.startOperating();
      expect(result).toBeFalse();
    });

    it('adds enabled to notSatisfiedStartConditions if building is disabled', function () {
      building.disabled = true;
      building.startOperating();
      expect(building.notSatisfiedStartConditions).toEqual(['enabled']);
    });

    it('adds notOperating to notSatisfiedStartConditions if building is operating', function () {
      building.isOperating = true;
      building.startOperating();
      expect(building.notSatisfiedStartConditions).toEqual(['notOperating']);
    });

    it('adds hasResources to notSatisfiedStartConditions if player does not have upkeep resources', function () {
      fakeStorage.has.and.returnValue(false);
      building.startOperating();
      expect(building.notSatisfiedStartConditions).toEqual(['hasResources']);
    });

    it('adds hasGold to notSatisfiedStartConditions if player does not have upkeep gold', function () {
      fakeStorage.hasGold.and.returnValue(false);
      building.startOperating();
      expect(building.notSatisfiedStartConditions).toEqual(['hasGold']);
    });

    it('adds enabled, notOperating, hasResources and hasGold to notSatisfiedStartConditions ' +
       'if none of these conditions are met', function () {
      building.disabled = true;
      building.isOperating = true;
      fakeStorage.has.and.returnValue(false);
      fakeStorage.hasGold.and.returnValue(false);
      building.startOperating();
      expect(building.notSatisfiedStartConditions).toEqual(['enabled', 'notOperating', 'hasResources', 'hasGold']);
    });

    it('sets notSatisfiedStartConditions to [] if all start condition are met', function () {
      building.notSatisfiedStartConditions = jasmine.createSpy('notSatisfiedStartConditions');
      building.startOperating();
      expect(building.notSatisfiedStartConditions).toEqual([]);
    });

    it('sets isOperating to true if start condition are met', function () {
      building.startOperating();
      expect(building.isOperating).toBeTrue();
    });

    it('calls calcResourceProduction if start condition are met', function () {
      building.startOperating();
      expect(building.calcResourceProduction).toHaveBeenCalledWith();
    });

    it('calls calcGoldIncome if start condition are met', function () {
      building.startOperating();
      expect(building.calcGoldIncome).toHaveBeenCalledWith();
    });

    it('calls calcResourceUpkeep if start condition are met', function () {
      building.startOperating();
      expect(building.calcResourceUpkeep).toHaveBeenCalledWith();
    });

    it('calls calcGoldUpkeep if start condition are met', function () {
      building.startOperating();
      expect(building.calcGoldUpkeep).toHaveBeenCalledWith();
    });

    it('calls calcResourceOutcomeTotal on square if start condition are met', function () {
      building.startOperating();
      expect(square.calcResourceOutcomeTotal).toHaveBeenCalledWith();
    });

    it('calls calcGoldOutcomeTotal on square if start condition are met', function () {
      building.startOperating();
      expect(square.calcGoldOutcomeTotal).toHaveBeenCalledWith();
    });

    it('calls take on storage if start condition are met', function () {
      var resourceUpkeepSpy = jasmine.createSpy('resourceUpkeep');
      building.resourceUpkeep = resourceUpkeepSpy;
      building.startOperating();
      expect(storageProvider.getStorage).toHaveBeenCalledWith(square.player);
      expect(fakeStorage.take).toHaveBeenCalledWith(resourceUpkeepSpy);
    });

    it('calls put on storage if start condition are met', function () {
      var resourceProductionSpy = jasmine.createSpy('resourceProduction');
      building.resourceProduction = resourceProductionSpy;
      building.startOperating();
      expect(storageProvider.getStorage).toHaveBeenCalledWith(square.player);
      expect(fakeStorage.put).toHaveBeenCalledWith(resourceProductionSpy);
    });

    it('calls changeGoldIncome on storage if start condition are met', function () {
      building.startOperating();
      expect(storageProvider.getStorage).toHaveBeenCalledWith(square.player);
      expect(fakeStorage.changeGoldIncome).toHaveBeenCalledWith(jasmine.any(Number));
    });

  });

});