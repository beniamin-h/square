'use strict';

angular.module('square').factory('buildingClassRegister', ['Market', 'Tavern',
    function (Market, Tavern) {

  var that = this;
  that.buildingClasses = {};

  that.registerBuildingClass = function (buildingClass) {
    that.buildingClasses[buildingClass.name] = buildingClass;
  };

  that.registerBuildingClass(Market);
  that.registerBuildingClass(Tavern);

  return {
    buildingClasses: that.buildingClasses,
    getBuildingClass: function (buildingClassName) {
      return that.buildingClasses[buildingClassName];
    }
  };
}]);