'use strict';

var _RegExp = require('nwglobal').RegExp;

angular.module('square')

.constant('buildingStartConditionsRegExp',
  new _RegExp(/^(enabled|notOperating|hasResources|hasGold)$/))

.factory('BuildingSchema', ['Joi', 'BlockSchema', 'SquareSchema',
                            'storageResourcesRegExp', 'buildingStartConditionsRegExp',
    function (Joi, BlockSchema, SquareSchema,
              storageResourcesRegExp, buildingStartConditionsRegExp) {

  return Joi.object().keys({
    block: BlockSchema,
    square: SquareSchema,
    resourceProduction: Joi.object({})
      .pattern(storageResourcesRegExp, Joi.number().integer()),
    goldIncome: Joi.number().natural(),
    resourceUpkeep: Joi.object({})
      .pattern(storageResourcesRegExp, Joi.number().integer()),
    goldUpkeep: Joi.number().natural(),
    constructionProgress: Joi.number().min(0).max(1),
    buildDuration: Joi.number().natural(),
    isOperating: Joi.boolean(),
    disabled: Joi.boolean(),
    notSatisfiedStartConditions: Joi.array().items(
      Joi.string().regex(buildingStartConditionsRegExp)
    )
  });

}]);
