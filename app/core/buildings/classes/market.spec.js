'use strict';

describe('Market', function() {
  beforeEach(module('square'));

  var Market, buildingClassRegister;

  beforeEach(inject(function (_Market_, _buildingClassRegister_) {
    Market = _Market_;
    buildingClassRegister = _buildingClassRegister_;
  }));

  it('is registered in buildingClassRegister', function () {
    expect(buildingClassRegister.getBuildingClass('Market')).toBe(Market);
  });

});