'use strict';

angular.module('square').factory('Market', ['Building',
    function (Building) {

  function Market() {
    Building.apply(this, arguments);
  }

  Market.prototype = Object.create(Building.prototype);
  Market.prototype.constructor = Market;

  Market.prototype._baseResourceProduction = {};
  Market.prototype._baseGoldIncomePerSquareLevel = {
    2: 1,
    3: 1,
    4: 1,
    5: 1
  };
  Market.prototype._baseGoldUpkeepPerSquareLevel = {
    2: 0,
    3: 0,
    4: 0,
    5: 0
  };
  Market.prototype._baseResourceUpkeep = {};

  Market.prototype.displayName = 'Market square';
  Market.prototype._baseResourceBuildCost = { wood: 1 };
  Market.prototype._baseGoldBuildCost = 3;
  Market.prototype._baseBuildDuration = 4;

  Market.prototype.requiredSquareLevel = 2;
  Market.prototype.maxNumberPerSquareSize = {
    2: 1,
    3: 5,
    4: 21,
    5: 85
  };
  Market.prototype.baseAiValue = 10;

  return Market;
}]);