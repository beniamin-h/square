'use strict';

angular.module('square').factory('Tavern', ['Building',
    function (Building) {

  function Tavern() {
    Building.apply(this, arguments);
  }

  Tavern.prototype = Object.create(Building.prototype);
  Tavern.prototype.constructor = Tavern;

  Tavern.prototype._baseResourceProduction = {};
  Tavern.prototype._baseGoldIncomePerSquareLevel = {
    2: 1,
    3: 1,
    4: 1,
    5: 1
  };
  Tavern.prototype._baseGoldUpkeepPerSquareLevel = {
    2: 0,
    3: 0,
    4: 0,
    5: 0
  };
  Tavern.prototype._baseResourceUpkeep = {
    food: 1
  };

  Tavern.prototype.displayName = 'Tavern';
  Tavern.prototype._baseResourceBuildCost = { wood: 1 };
  Tavern.prototype._baseGoldBuildCost = 7;
  Tavern.prototype._baseBuildDuration = 6;

  Tavern.prototype.requiredSquareLevel = 2;
  Tavern.prototype.maxNumberPerSquareSize = {
    2: 1,
    3: 4,
    4: 16,
    5: 64
  };
  Tavern.prototype.baseAiValue = 5;

  return Tavern;
}]);