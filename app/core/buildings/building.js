'use strict';

angular.module('square').factory('Building', ['storageProvider',
    function (storageProvider) {

  function Building(block, square) {
    this.block = block;
    this.square = square;
    this.resourceProduction = {};
    this.resourceUpkeep = {};
    this.notSatisfiedStartConditions = [];
    //TODO: resume construction on game load
  }

  Building.prototype.block = null;
  Building.prototype.square = null;
  Building.prototype._baseResourceProduction = {};  // abstract, immutable
  Building.prototype._baseResourceUpkeep = {};  // abstract, immutable
  Building.prototype._baseGoldIncomePerSquareLevel = {};  // abstract, immutable
  Building.prototype._baseGoldUpkeepPerSquareLevel = {};  // abstract, immutable

  Building.prototype.resourceProduction = null;  // calculated, mutable
  Building.prototype.goldIncome = 0;  // calculated, mutable
  Building.prototype.resourceUpkeep = null;  // calculated, mutable
  Building.prototype.goldUpkeep = 0;  // calculated, mutable
  Building.prototype.constructionProgress = 0.0;  // calculated, mutable
  Building.prototype.buildDuration = 0;  // calculated, mutable
  Building.prototype.isOperating = false;  // calculated, mutable
  Building.prototype.disabled = false;  // calculated, mutable
  Building.prototype.notSatisfiedStartConditions = null;  // calculated, mutable

  Building.prototype.displayName = '';  // abstract, immutable
  Building.prototype._baseResourceBuildCost = {};  // abstract, immutable
  Building.prototype._baseGoldBuildCost = 0;  // abstract, immutable
  Building.prototype._baseBuildDuration = 0;  // abstract, immutable

  Building.prototype.requiredSquareLevel = 2;  // abstract, immutable
  Building.prototype.additionalRequirements = {};  // abstract, immutable
  Building.prototype.maxNumberPerSquareSize = {};  // abstract, immutable
  Building.prototype.maxNumberPerResourceInSquare = {};  // abstract, immutable
  Building.prototype.baseAiValue = 0;  // abstract, immutable
  Building.prototype.supplyRadius = 0;  // abstract, immutable
  Building.prototype.requiredTechnologies = [];  // abstract, immutable

  Building.prototype.getSpecialBuildingPosition = null;  // abstract, function

  Building.prototype.toString = function () {
    return this.constructor.name + 'Building-' +
      (this.block.region ? this.block.region.x + 'x' + this.block.region.y : '') + '_' +
      this.block.x + 'x' + this.block.y;
  };

  Building.prototype.calcResourceProduction = function () {
    this.resourceProduction = {};
    for (var resName in this._baseResourceProduction) {
      this.resourceProduction[resName] = this._baseResourceProduction[resName];
    }
  };

  Building.prototype.calcResourceUpkeep = function () {
    this.resourceUpkeep = {};
    for (var resName in this._baseResourceUpkeep) {
      this.resourceUpkeep[resName] = this._baseResourceUpkeep[resName];
    }
  };

  Building.prototype.calcGoldIncome = function () {
    this.goldIncome = this._baseGoldIncomePerSquareLevel[this.square.level] || 0;
  };

  Building.prototype.calcGoldUpkeep = function () {
    this.goldUpkeep = this._baseGoldUpkeepPerSquareLevel[this.square.level] || 0;
  };

  Building.prototype.calcBuildDuration = function () {
    this.buildDuration = this._baseBuildDuration;
  };

  Building.prototype.beginConstruction = function () {
    var that = this;
    that.square.player.eventEmitter.on('proceedingConstructions', function () {
      that.proceedConstruction();
    });
  };

  Building.prototype.finishConstruction = function () {
    //TODO: this.square.player.eventEmitter.on('playerTurnBegin' -- disconnect
    this.constructionProgress = 1.0;
    if (!this.startOperating()) {
      this.tryToStartOperatingNextTime();
    }
  };

  Building.prototype.tryToStartOperatingNextTime = function () {
    var that = this;
    var callback = function () {
      if (that.disabled || that.startOperating()) {
        that.square.player.eventEmitter.removeListener('resumingStoppedBuildings', callback);
      }
    };
    that.square.player.eventEmitter.on('resumingStoppedBuildings', callback);
  };

  Building.prototype.proceedConstruction = function () {
    if (this.buildDuration === 0) {
      this.constructionProgress = 1.0;
      return true;
    }
    this.constructionProgress += 1.0 / this.buildDuration;
    if (+(this.constructionProgress.toFixed(4)) >= 1.0) {
      this.finishConstruction();
      return true;
    }
    return false;
  };

  Building.prototype.turnOff = function () {
    this.disabled = true;
    this.stopOperating();
  };

  Building.prototype.turnOn = function () {
    this.disabled = false;
    if (!this.startOperating()) {
      this.tryToStartOperatingNextTime();
    }
  };

  Building.prototype.stopOperating = function () {
    if (this.isOperating) {
      this.isOperating = false;
      storageProvider.getStorage(this.square.player).take(this.resourceProduction);
      storageProvider.getStorage(this.square.player).changeGoldIncome(this.goldUpkeep - this.goldIncome);
      this.resourceProduction = {};
      this.goldIncome = 0;
      this.resourceUpkeep = {};
      this.goldUpkeep = 0;
      this.square.calcResourceOutcomeTotal();
      this.square.calcGoldOutcomeTotal();
      return true;
    }
    return false;
  };

  Building.prototype.startOperating = function () {
    var conditions = {
      enabled: !this.disabled,
      notOperating: !this.isOperating,
      hasResources: storageProvider.getStorage(this.square.player).has(this.resourceUpkeep),
      hasGold: storageProvider.getStorage(this.square.player).hasGold(this.goldUpkeep)
    };
    if (conditions.enabled && conditions.notOperating && conditions.hasResources && conditions.hasGold) {
      this.isOperating = true;
      this.calcResourceProduction();
      this.calcGoldIncome();
      this.calcResourceUpkeep();
      this.calcGoldUpkeep();
      this.square.calcResourceOutcomeTotal();
      this.square.calcGoldOutcomeTotal();
      storageProvider.getStorage(this.square.player).take(this.resourceUpkeep);
      storageProvider.getStorage(this.square.player).put(this.resourceProduction);
      storageProvider.getStorage(this.square.player).changeGoldIncome(this.goldIncome - this.goldUpkeep);
      this.notSatisfiedStartConditions = [];
      return true;
    }
    this.notSatisfiedStartConditions = Object.keys(conditions).filter(function (condition) {
      return !conditions[condition];
    });
    return false;
  };

  return Building;
}]);