'use strict';

describe('BuildingSchema', function () {
  beforeEach(module('square'));

  var Joi, Building, BuildingSchema, Block, Region, Square, AIPlayer,
    Market, Plain;

  beforeEach(inject(function (_Joi_, _Building_, _BuildingSchema_, _Block_, _Region_, _Square_, _AIPlayer_,
                              _Market_, _Plain_) {
    Joi = _Joi_;
    Building = _Building_;
    BuildingSchema = _BuildingSchema_;
    Block = _Block_;
    Region = _Region_;
    Square = _Square_;
    AIPlayer = _AIPlayer_;

    Market = _Market_;
    Plain = _Plain_;
  }));

  describe('a new Building instance', function () {

    it('validates against its schema', function (done) {
      var fakeIAPlayer = new AIPlayer('fake-player-ID', { x: 0, y: 0 });
      var building = new Building(new Block(0, 1, new Region(0, 0), new Plain()),
                                  new Square(0, 1, 0, 0, [], 2, fakeIAPlayer));
      Joi.validate(building, BuildingSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

  });

  describe('a new Market instance', function () {

    it('validates against its schema', function (done) {
      var fakeIAPlayer = new AIPlayer('fake-player-ID', { x: 0, y: 0 });
      var market = new Market(new Block(0, 1, new Region(0, 0), new Plain()),
                              new Square(0, 1, 0, 0, [], 2, fakeIAPlayer));
      Joi.validate(market, BuildingSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

  });

});