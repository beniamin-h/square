'use strict';

angular.module('square').factory('BlockKind', [
    function () {

  function BlockKind() {

  }

  BlockKind.prototype.cssClass = '';
  BlockKind.prototype.improvable = true;
  BlockKind.prototype.baseResourceCost = null;
  BlockKind.prototype.baseResourceGain = null;
  BlockKind.prototype.baseGoldIncome = 0;
  BlockKind.prototype.baseAiValue = 0;
  BlockKind.prototype.mainResource = '';
  BlockKind.prototype.mainResourceLevel = 0;

  BlockKind.prototype.calcResourceCost = function () {
    var cost = {};
    for (var resName in this.baseResourceCost) {
      cost[resName] = this.baseResourceCost[resName];
    }

    return cost;
  };

  BlockKind.prototype.calcResourceGain = function (level, isRich) {
    var gain = {};
    for (var resName in this.baseResourceGain) {
      gain[resName] = Math.pow(2, level - 1 + (+isRich)) * this.baseResourceGain[resName];
    }

    return gain;
  };

  BlockKind.prototype.calcGoldGain = function (level, isRich) {
    return Math.pow(2, level - 1 + (+isRich)) * this.baseGoldIncome;
  };

  BlockKind.prototype.toString = function () {
    return 'Base block kind';
  };

  return BlockKind;
}]);