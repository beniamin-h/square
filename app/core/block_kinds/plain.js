'use strict';

angular.module('square').factory('Plain', ['BlockKind',
    function (BlockKind) {

  function Plain() {
    BlockKind.apply(this, arguments);
  }

  Plain.prototype = Object.create(BlockKind.prototype);
  Plain.prototype.constructor = Plain;

  Plain.prototype.cssClass = 'plain';
  Plain.prototype.baseResourceCost = { wood: 1 };
  Plain.prototype.baseResourceGain = { food: 1 };
  Plain.prototype.baseAiValue = 1;
  Plain.prototype.mainResource = 'food';
  Plain.prototype.mainResourceLevel = 1;

  Plain.prototype.toString = function () {
    return 'plain';
  };

  return Plain;
}]);