'use strict';

angular.module('square').factory('Coalfield', ['BlockKind',
    function (BlockKind) {

  function Coalfield() {
    BlockKind.apply(this, arguments);
  }

  Coalfield.prototype = Object.create(BlockKind.prototype);
  Coalfield.prototype.constructor = Coalfield;

  Coalfield.prototype.cssClass = 'coalfield';
  Coalfield.prototype.baseResourceCost = { food: 1 };
  Coalfield.prototype.baseResourceGain = { coal: 2 };
  Coalfield.prototype.baseAiValue = 12;
  Coalfield.prototype.mainResource = 'coal';
  Coalfield.prototype.mainResourceLevel = 2;

  Coalfield.prototype.toString = function () {
    return 'coalfield';
  };

  return Coalfield;
}]);