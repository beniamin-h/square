'use strict';

describe('BlockKindSchema', function() {
  beforeEach(module('square'));

  var Forest, Hill, Mountain, Plain, Water,
    Coalfield, WildHorses, GoldDeposit, SilverDeposit, GemstoneDeposit,
    BlockKindSchema, Joi;

  beforeEach(inject(function (_Forest_, _Hill_, _Mountain_, _Plain_, _Water_,
      _Coalfield_, _WildHorses_, _GoldDeposit_, _SilverDeposit_, _GemstoneDeposit_,
      _BlockKindSchema_, _Joi_) {
    Forest = _Forest_;
    Hill = _Hill_;
    Mountain = _Mountain_;
    Plain = _Plain_;
    Water = _Water_;

    Coalfield = _Coalfield_;
    WildHorses = _WildHorses_;
    GoldDeposit = _GoldDeposit_;
    SilverDeposit = _SilverDeposit_;
    GemstoneDeposit = _GemstoneDeposit_;

    BlockKindSchema = _BlockKindSchema_;
    Joi = _Joi_;
  }));

  describe('Forest', function () {

    it('validates against the block kind schema', function (done) {
      var blockKind = new Forest();
      Joi.validate(blockKind, BlockKindSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

  });

  describe('Hill', function () {

    it('validates against the block kind schema', function (done) {
      var blockKind = new Hill();
      Joi.validate(blockKind, BlockKindSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

  });

  describe('Mountain', function () {

    it('validates against the block kind schema', function (done) {
      var blockKind = new Mountain();
      Joi.validate(blockKind, BlockKindSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

  });

  describe('Plain', function () {

    it('validates against the block kind schema', function (done) {
      var blockKind = new Plain();
      Joi.validate(blockKind, BlockKindSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

  });

  describe('Coalfield', function () {

    it('validates against the block kind schema', function (done) {
      var blockKind = new Coalfield();
      Joi.validate(blockKind, BlockKindSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

  });

  describe('WildHorses', function () {

    it('validates against the block kind schema', function (done) {
      var blockKind = new WildHorses();
      Joi.validate(blockKind, BlockKindSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

  });

  describe('GoldDeposit', function () {

    it('validates against the block kind schema', function (done) {
      var blockKind = new GoldDeposit();
      Joi.validate(blockKind, BlockKindSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

  });

  describe('SilverDeposit', function () {

    it('validates against the block kind schema', function (done) {
      var blockKind = new SilverDeposit();
      Joi.validate(blockKind, BlockKindSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

  });

  describe('GemstoneDeposit', function () {

    it('validates against the block kind schema', function (done) {
      var blockKind = new GemstoneDeposit();
      Joi.validate(blockKind, BlockKindSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

  });

  describe('Water', function () {

    it('validates against the block kind schema', function (done) {
      var blockKind = new Water();
      Joi.validate(blockKind, BlockKindSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

  });

});