'use strict';

angular.module('square').factory('Water', ['BlockKind',
    function (BlockKind) {

  function Water() {
    BlockKind.apply(this, arguments);
  }

  Water.prototype = Object.create(BlockKind.prototype);
  Water.prototype.constructor = Water;

  Water.prototype.cssClass = 'water';
  Water.prototype.improvable = false;
  Water.prototype.baseResourceCost = {  };
  Water.prototype.baseResourceGain = {  };
  Water.prototype.baseAiValue = -1;
  Water.prototype.mainResource = '';
  Water.prototype.mainResourceLevel = 0;
      
  Water.prototype.toString = function () {
    return 'water';
  };

  return Water;
}]);