'use strict';

describe('BlockKind', function() {
  beforeEach(module('square'));

  var BlockKind, Forest, Hill, Mountain, Plain,
    Coalfield, WildHorses, GoldDeposit, SilverDeposit, GemstoneDeposit,
    Water;

  beforeEach(inject(function (_BlockKind_, _Forest_, _Hill_, _Mountain_, _Plain_,
      _Coalfield_, _WildHorses_, _GoldDeposit_, _SilverDeposit_, _GemstoneDeposit_,
      _Water_) {
    BlockKind = _BlockKind_;
    Forest = _Forest_;
    Hill = _Hill_;
    Mountain = _Mountain_;
    Plain = _Plain_;

    Coalfield = _Coalfield_;
    WildHorses = _WildHorses_;
    GoldDeposit = _GoldDeposit_;
    SilverDeposit = _SilverDeposit_;
    GemstoneDeposit = _GemstoneDeposit_;

    Water = _Water_;
  }));

  describe('calcResourceCost properly', function () {

    var block;
    beforeEach(function () {
      block = new BlockKind();
      block.baseResourceCost = { stone: 1, wood: 2, food: 30, metal: 666 };
    });

    it('for level 0', function () {
      var cost = block.calcResourceCost();
      expect(cost.stone).toBe(1);
      expect(cost.wood).toBe(2);
      expect(cost.food).toBe(30);
      expect(cost.metal).toBe(666);
    });

  });

  describe('calcResourceGain properly', function () {

    var block;
    beforeEach(function () {
      block = new BlockKind();
      block.baseResourceGain = { stone: 999, wood: 4, metal: 2 };
    });

    it('for level 1, not rich', function () {
      var cost = block.calcResourceGain(1, false);
      expect(cost.stone).toBe(999);
      expect(cost.wood).toBe(4);
      expect(cost.metal).toBe(2);
    });

    it('for level 2, not rich', function () {
      var cost = block.calcResourceGain(2, false);
      expect(cost.stone).toBe(1998);
      expect(cost.wood).toBe(8);
      expect(cost.metal).toBe(4);
    });

    it('for level 5, not rich', function () {
      var cost = block.calcResourceGain(5, false);
      expect(cost.stone).toBe(16 * 999);
      expect(cost.wood).toBe(16 * 4);
      expect(cost.metal).toBe(16 * 2);
    });

    it('for level 1, rich', function () {
      var cost = block.calcResourceGain(1, true);
      expect(cost.stone).toBe(1998);
      expect(cost.wood).toBe(8);
      expect(cost.metal).toBe(4);
    });

    it('for level 5, rich', function () {
      var cost = block.calcResourceGain(5, true);
      expect(cost.stone).toBe(2 * 16 * 999);
      expect(cost.wood).toBe(2 * 16 * 4);
      expect(cost.metal).toBe(2 * 16 * 2);
    });
  });

  describe('calcGoldGain', function () {

    var block;

    beforeEach(function () {
      block = new BlockKind();
    });

    describe('for a not rich block', function () {

      it('level = 1 and baseGoldIncome = 10 returns 10', function () {
        block.baseGoldIncome = 10;
        var result = block.calcGoldGain(1, false);
        expect(result).toBe(10);
      });

      it('level = 2 and baseGoldIncome = 8 returns 16', function () {
        block.baseGoldIncome = 8;
        var result = block.calcGoldGain(2, false);
        expect(result).toBe(16);
      });

      it('level = 3 and baseGoldIncome = 100 returns 400', function () {
        block.baseGoldIncome = 100;
        var result = block.calcGoldGain(3, false);
        expect(result).toBe(400);
      });

      it('level = 4 and baseGoldIncome = 2 returns 16', function () {
        block.baseGoldIncome = 2;
        var result = block.calcGoldGain(4, false);
        expect(result).toBe(16);
      });

      it('level = 3 and baseGoldIncome = 1 returns 4', function () {
        block.baseGoldIncome = 1;
        var result = block.calcGoldGain(3, false);
        expect(result).toBe(4);
      });

    });

    describe('for a rich block', function () {

      it('level = 1 and baseGoldIncome = 10 returns 20', function () {
        block.baseGoldIncome = 10;
        var result = block.calcGoldGain(1, true);
        expect(result).toBe(20);
      });

      it('level = 2 and baseGoldIncome = 11 returns 44', function () {
        block.baseGoldIncome = 11;
        var result = block.calcGoldGain(2, true);
        expect(result).toBe(44);
      });

      it('level = 5 and baseGoldIncome = 2 returns 64', function () {
        block.baseGoldIncome = 2;
        var result = block.calcGoldGain(5, true);
        expect(result).toBe(64);
      });

    });

  });

  describe('Forest', function () {

    it('inherit from BlockKind', function () {
      expect(new Forest() instanceof BlockKind);
    });

    it('has props setup properly', function () {
      expect((new Forest()).cssClass).toBeTruthy();
      expect((new Forest()).baseResourceCost).toBeTruthy();
      expect((new Forest()).baseResourceGain).toBeTruthy();
      expect((new Forest()).mainResource).toBeTruthy();
      expect((new Forest()).toString()).toBeTruthy();
    });

  });

  describe('Hill', function () {

    it('inherit from BlockKind', function () {
      expect(new Hill() instanceof BlockKind);
    });

    it('has props setup properly', function () {
      expect((new Hill()).cssClass).toBeTruthy();
      expect((new Hill()).baseResourceCost).toBeTruthy();
      expect((new Hill()).baseResourceGain).toBeTruthy();
      expect((new Hill()).mainResource).toBeTruthy();
      expect((new Hill()).toString()).toBeTruthy();
    });

  });

  describe('Mountain', function () {

    it('inherit from BlockKind', function () {
      expect(new Mountain() instanceof BlockKind);
    });

    it('has props setup properly', function () {
      expect((new Mountain()).cssClass).toBeTruthy();
      expect((new Mountain()).baseResourceCost).toBeTruthy();
      expect((new Mountain()).baseResourceGain).toBeTruthy();
      expect((new Mountain()).mainResource).toBeTruthy();
      expect((new Mountain()).toString()).toBeTruthy();
    });

  });

  describe('Plain', function () {

    it('inherit from BlockKind', function () {
      expect(new Plain() instanceof BlockKind);
    });

    it('has props setup properly', function () {
      expect((new Plain()).cssClass).toBeTruthy();
      expect((new Plain()).baseResourceCost).toBeTruthy();
      expect((new Plain()).baseResourceGain).toBeTruthy();
      expect((new Plain()).mainResource).toBeTruthy();
      expect((new Plain()).toString()).toBeTruthy();
    });

  });

  describe('Coalfield', function () {

    it('inherits from BlockKind', function () {
      expect(new Coalfield() instanceof BlockKind);
    });

    it('has props setup properly', function () {
      expect((new Coalfield()).cssClass).toBeTruthy();
      expect((new Coalfield()).baseResourceCost).toBeTruthy();
      expect((new Coalfield()).baseResourceGain).toBeTruthy();
      expect((new Coalfield()).mainResource).toBeTruthy();
      expect((new Coalfield()).toString()).toBeTruthy();
    });

  });

  describe('WildHorses', function () {

    it('inherits from BlockKind', function () {
      expect(new WildHorses() instanceof BlockKind);
    });

    it('has props setup properly', function () {
      expect((new WildHorses()).cssClass).toBeTruthy();
      expect((new WildHorses()).baseResourceCost).toBeTruthy();
      expect((new WildHorses()).baseResourceGain).toBeTruthy();
      expect((new WildHorses()).mainResource).toBeTruthy();
      expect((new WildHorses()).toString()).toBeTruthy();
    });

  });

  describe('GoldDeposit', function () {

    it('inherits from BlockKind', function () {
      expect(new GoldDeposit() instanceof BlockKind);
    });

    it('has props setup properly', function () {
      expect((new GoldDeposit()).cssClass).toBeTruthy();
      expect((new GoldDeposit()).baseResourceCost).toBeTruthy();
      expect((new GoldDeposit()).baseResourceGain).toBeTruthy();
      expect((new GoldDeposit()).mainResource).toBeTruthy();
      expect((new GoldDeposit()).toString()).toBeTruthy();
    });

  });

  describe('SilverDeposit', function () {

    it('inherits from BlockKind', function () {
      expect(new SilverDeposit() instanceof BlockKind);
    });

    it('has props setup properly', function () {
      expect((new SilverDeposit()).cssClass).toBeTruthy();
      expect((new SilverDeposit()).baseResourceCost).toBeTruthy();
      expect((new SilverDeposit()).baseResourceGain).toBeTruthy();
      expect((new SilverDeposit()).mainResource).toBeTruthy();
      expect((new SilverDeposit()).toString()).toBeTruthy();
    });

  });

  describe('GemstoneDeposit', function () {

    it('inherits from BlockKind', function () {
      expect(new GemstoneDeposit() instanceof BlockKind);
    });

    it('has props setup properly', function () {
      expect((new GemstoneDeposit()).cssClass).toBeTruthy();
      expect((new GemstoneDeposit()).baseResourceCost).toBeTruthy();
      expect((new GemstoneDeposit()).baseResourceGain).toBeTruthy();
      expect((new GemstoneDeposit()).mainResource).toBeTruthy();
      expect((new GemstoneDeposit()).toString()).toBeTruthy();
    });

  });

  describe('Water', function () {

    it('inherit from BlockKind', function () {
      expect(new Water() instanceof BlockKind);
    });

    it('has props setup properly', function () {
      expect((new Water()).cssClass).toBeTruthy();
      expect((new Water()).baseResourceCost).toBeTruthy();
      expect((new Water()).baseResourceGain).toBeTruthy();
      expect((new Water()).mainResource).toBeFalsy();
      expect((new Water()).toString()).toBeTruthy();
    });

    it('is not improvable', function () {
      expect((new Water()).improvable).toBeFalsy();
    });

  });

});