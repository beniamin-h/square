'use strict';

angular.module('square').factory('SilverDeposit', ['BlockKind',
    function (BlockKind) {

  function SilverDeposit() {
    BlockKind.apply(this, arguments);
  }

  SilverDeposit.prototype = Object.create(BlockKind.prototype);
  SilverDeposit.prototype.constructor = SilverDeposit;

  SilverDeposit.prototype.cssClass = 'silver-deposit';
  SilverDeposit.prototype.baseResourceCost = { food: 1 };
  SilverDeposit.prototype.baseResourceGain = { };
  SilverDeposit.prototype.baseAiValue = 14;
  SilverDeposit.prototype.mainResource = 'silver';
  SilverDeposit.prototype.mainResourceLevel = 3;
  SilverDeposit.prototype.baseGoldIncome = 9;

  SilverDeposit.prototype.toString = function () {
    return 'silverDeposit';
  };

  return SilverDeposit;
}]);