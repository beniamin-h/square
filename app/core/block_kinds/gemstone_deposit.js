'use strict';

angular.module('square').factory('GemstoneDeposit', ['BlockKind',
    function (BlockKind) {

  function GemstoneDeposit() {
    BlockKind.apply(this, arguments);
  }

  GemstoneDeposit.prototype = Object.create(BlockKind.prototype);
  GemstoneDeposit.prototype.constructor = GemstoneDeposit;

  GemstoneDeposit.prototype.cssClass = 'gemstone-deposit';
  GemstoneDeposit.prototype.baseResourceCost = { food: 1 };
  GemstoneDeposit.prototype.baseResourceGain = { };
  GemstoneDeposit.prototype.baseAiValue = 16;
  GemstoneDeposit.prototype.mainResource = 'gemstone';
  GemstoneDeposit.prototype.mainResourceLevel = 3;
  GemstoneDeposit.prototype.baseGoldIncome = 16;

  GemstoneDeposit.prototype.toString = function () {
    return 'gemstoneDeposit';
  };

  return GemstoneDeposit;
}]);