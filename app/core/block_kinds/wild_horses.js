'use strict';

angular.module('square').factory('WildHorses', ['BlockKind',
    function (BlockKind) {

  function WildHorses() {
    BlockKind.apply(this, arguments);
  }

  WildHorses.prototype = Object.create(BlockKind.prototype);
  WildHorses.prototype.constructor = WildHorses;

  WildHorses.prototype.cssClass = 'wild-horses';
  WildHorses.prototype.baseResourceCost = { food: 1 };
  WildHorses.prototype.baseResourceGain = { horses: 2 };
  WildHorses.prototype.baseAiValue = 16;
  WildHorses.prototype.mainResource = 'horses';
  WildHorses.prototype.mainResourceLevel = 2;

  WildHorses.prototype.toString = function () {
    return 'wildHorses';
  };

  return WildHorses;
}]);