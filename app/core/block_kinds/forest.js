'use strict';

angular.module('square').factory('Forest', ['BlockKind',
    function (BlockKind) {

  function Forest() {
    BlockKind.apply(this, arguments);
  }

  Forest.prototype = Object.create(BlockKind.prototype);
  Forest.prototype.constructor = Forest;

  Forest.prototype.cssClass = 'forest';
  Forest.prototype.baseResourceCost = { stone: 1 };
  Forest.prototype.baseResourceGain = { wood: 1 };
  Forest.prototype.baseAiValue = 2;
  Forest.prototype.mainResource = 'wood';
  Forest.prototype.mainResourceLevel = 1;

  Forest.prototype.toString = function () {
    return 'forest';
  };

  return Forest;
}]);