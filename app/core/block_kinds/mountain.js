'use strict';

angular.module('square').factory('Mountain', ['BlockKind',
    function (BlockKind) {

  function Mountain() {
    BlockKind.apply(this, arguments);
  }

  Mountain.prototype = Object.create(BlockKind.prototype);
  Mountain.prototype.constructor = Mountain;

  Mountain.prototype.cssClass = 'mountain';
  Mountain.prototype.baseResourceCost = { food: 1 };
  Mountain.prototype.baseResourceGain = { metal: 1 };
  Mountain.prototype.baseAiValue = 8;
  Mountain.prototype.mainResource = 'metal';
  Mountain.prototype.mainResourceLevel = 1;

  Mountain.prototype.toString = function () {
    return 'mountain';
  };

  return Mountain;
}]);