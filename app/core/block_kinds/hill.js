'use strict';

angular.module('square').factory('Hill', ['BlockKind',
    function (BlockKind) {

  function Hill() {
    BlockKind.apply(this, arguments);
  }

  Hill.prototype = Object.create(BlockKind.prototype);
  Hill.prototype.constructor = Hill;

  Hill.prototype.cssClass = 'hill';
  Hill.prototype.baseResourceCost = { metal: 1 };
  Hill.prototype.baseResourceGain = { stone: 1 };
  Hill.prototype.baseAiValue = 4;
  Hill.prototype.mainResource = 'stone';
  Hill.prototype.mainResourceLevel = 1;

  Hill.prototype.toString = function () {
    return 'hill';
  };

  return Hill;
}]);