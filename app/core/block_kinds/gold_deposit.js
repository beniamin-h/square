'use strict';

angular.module('square').factory('GoldDeposit', ['BlockKind',
    function (BlockKind) {

  function GoldDeposit() {
    BlockKind.apply(this, arguments);
  }

  GoldDeposit.prototype = Object.create(BlockKind.prototype);
  GoldDeposit.prototype.constructor = GoldDeposit;

  GoldDeposit.prototype.cssClass = 'gold-deposit';
  GoldDeposit.prototype.baseResourceCost = { food: 1 };
  GoldDeposit.prototype.baseResourceGain = { };
  GoldDeposit.prototype.baseAiValue = 16;
  GoldDeposit.prototype.mainResource = 'gold';
  GoldDeposit.prototype.mainResourceLevel = 3;
  GoldDeposit.prototype.baseGoldIncome = 12;

  GoldDeposit.prototype.toString = function () {
    return 'goldDeposit';
  };

  return GoldDeposit;
}]);