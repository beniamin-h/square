'use strict';

angular.module('square')

.constant('mapResourcesRegExp',
  new _RegExp(/^(food|stone|wood|metal|coal|horses|gold|silver|gemstone)$/))

.factory('BlockKindSchema', ['Joi', 'BlockKind', 'StorageResourcesSchema', 'mapResourcesRegExp',
    function (Joi, BlockKind, StorageResourcesSchema, mapResourcesRegExp) {

  return Joi.object().keys({
    cssClass: Joi.string(),
    improvable: Joi.boolean(),
    baseResourceCost: StorageResourcesSchema,
    baseResourceGain: StorageResourcesSchema,
    baseGoldIncome: Joi.number().integer(),
    baseAiValue: Joi.number(),
    mainResource: [Joi.string().regex(mapResourcesRegExp), ''],
    mainResourceLevel: Joi.number().natural()
  }).type(BlockKind);

}]);
