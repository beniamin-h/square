'use strict';

describe('PlayerSchema', function() {
  beforeEach(module('square'));

  var AIPlayer, HumanPlayer, PlayerSchema, Joi, playerProvider;

  beforeEach(inject(function (
      _AIPlayer_, _HumanPlayer_, _PlayerSchema_, _Joi_, _playerProvider_) {
    AIPlayer = _AIPlayer_;
    HumanPlayer = _HumanPlayer_;
    PlayerSchema = _PlayerSchema_;
    Joi = _Joi_;
    playerProvider = _playerProvider_;
  }));

  function validateAgainstPlayerSchema(player, done) {
    Joi.validate(player, PlayerSchema, {presence: 'required', convert: false}, function (validation_error) {
      if (validation_error) {
        done.fail(validation_error);
      } else {
        done();
      }
    });
  }

  describe('AIPlayer', function () {

    it('validates against player schema', function (done) {
      var player = new AIPlayer('mocked-player-id', {x: 0, y: -1});
      validateAgainstPlayerSchema(player, done);
    });

  });

  describe('HumanPlayer', function () {

    it('validates against player schema', function (done) {
      var player = new HumanPlayer('mocked-player-id', {x: 0, y: -1});
      validateAgainstPlayerSchema(player, done);
    });

  });

  describe('Human player created by playerProvider', function () {

    it('validates against player schema', function (done) {
      var player = playerProvider.addHuman();
      validateAgainstPlayerSchema(player, done);
    });

  });

  describe('AI player created by playerProvider', function () {

    it('validates against player schema', function (done) {
      var player = playerProvider.addAI();
      validateAgainstPlayerSchema(player, done);
    });

  });

});