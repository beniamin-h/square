'use strict';

var _RegExp = require('nwglobal').RegExp;
var EventEmitter = require('events');

angular.module('square')

.constant('playerIdRegExp', new _RegExp(/^[\w\-]+$/))

.factory('PlayerSchema', ['Joi', 'Player', 'AISchema', 'playerIdRegExp',
                          'Technology', 'squareLevelRegExp', 'Square',
    function (Joi, Player, AISchema, playerIdRegExp,
              Technology, squareLevelRegExp, Square) {

  return Joi.object().keys({
    playerId: Joi.string().regex(playerIdRegExp),
    startingRegionCoords: Joi.object().keys({
      x: Joi.number().integer(),
      y: Joi.number().integer()
    }),
    ai: AISchema.optional(),
    squaresByLevel: Joi.object().keys({})
      .pattern(squareLevelRegExp, Joi.array().items(
        Joi.object().type(Square)
      )),
    technologies: Joi.array().items(
      Joi.object().type(Technology)
    ),
    eventEmitter: [
      Joi.object().type(EventEmitter),
      Joi.any().valid(null)
    ],
    onMoveDone: [
      Joi.func().arity(0),
      Joi.any().valid(null)
    ],
    disclaimCooldown: Joi.number().natural(),
    baseDisclaimCooldown: Joi.number().natural()
  }).type(Player);

}]);
