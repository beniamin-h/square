'use strict';

angular.module('square').factory('playerProvider', ['HumanPlayer', 'AIPlayer', function (HumanPlayer, AIPlayer) {

  var that = this;
  that.players = {};
  that.aiPlayersCounter = 0;
  that.humanPlayerId = 'player-human';

  that.boundingNumber = function (current, direction, max, min) {
    var next = current + direction;
    if (next === max || next === min) {
      direction = -direction;
    }
    return {
      next: next,
      direction: direction
    };
  };

  var startingRegionHelperArray = [];
  for (var i = 2, z = 2; i < 150; ) {
    for (var j = 0; j <= z - 2; j++) {
      startingRegionHelperArray.push(i + j);
    }
    i = i + j + z++;
  }

  that.getNextStartingRegionCoords = function () {
    var playersCount = Object.keys(that.players).length;
    if (!playersCount) {
      return {x: 0, y: 0};
    }
    var counter = 0;
    var xNum = {
      next: 0,
      direction: 1
    };
    var yNum = {
      next: 0,
      direction: -1
    };
    var max = 1;
    var min = -1;

    while (counter++ < playersCount) {
      if (startingRegionHelperArray.indexOf(counter) > -1) {
        xNum = this.boundingNumber(xNum.next, xNum.direction, max, min);
      } else {
        yNum = this.boundingNumber(yNum.next, yNum.direction, max, min);
      }
      if ((2 * max + 1) * (2 * max + 1) - 2 == counter) {
        max++;
        min--;
      }
    }

    return {
      x: xNum.next,
      y: yNum.next
    };
  };

  return {
    addHuman: function () {
      that.players[that.humanPlayerId] = new HumanPlayer(that.humanPlayerId, that.getNextStartingRegionCoords());
      that.players[that.humanPlayerId].setEventEmitter();
      that.players[that.humanPlayerId].setNextDisclaimCooldown();
      return that.players[that.humanPlayerId];
    },
    addAI: function () {
      var playerId = 'player-AI-' + that.aiPlayersCounter++;
      that.players[playerId] = new AIPlayer(playerId, that.getNextStartingRegionCoords());
      that.players[playerId].setEventEmitter();
      that.players[playerId].setNextDisclaimCooldown();
      return that.players[playerId];
    },
    getHuman: function () {
      return that.players[that.humanPlayerId];
    },
    getFirstAI: function () {
      return this.getById('player-AI-0');
    },
    getAllPlayers: function () {
      return that.players;
    },
    getAllPlayerIds: function () {
      return Object.keys(that.players);
    },
    getById: function (playerId) {
      return that.players[playerId];
    },
    load: function (players) {
      that.players = players || {};
      Object.keys(that.players).forEach(function (playerId) {
        that.players[playerId].setEventEmitter();
      });
      that.aiPlayersCounter = Object.keys(that.players)
        .filter(function (playerId) {
          return that.players[playerId] instanceof AIPlayer;
        }).length;
    },
    nextPlayer: function (player) {
      if (player instanceof HumanPlayer) {
        if (that.aiPlayersCounter) {
          return this.getById('player-AI-0');
        } else {
          return this.getById('player-human');
        }
      } else {
        var orderNum = parseInt(player.playerId.match(/^player\-AI\-(\d+)$/)[1], 10);
        if (++orderNum < that.aiPlayersCounter) {
          return this.getById('player-AI-' + orderNum);
        } else {
          return this.getById('player-human') || this.getById('player-AI-0');
        }
      }
    },
    allPlayersDefeated: function () {
      return this.getAllPlayerIds().reduce(function (result, playerId) {
        return result && that.players[playerId].defeated;
      }, true);
    },
    getPlayerTechnologies: function (player) {
      return that.players[player.playerId].technologies;
    },
    _instance: this
  };
}]);
