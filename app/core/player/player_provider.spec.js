'use strict';

describe('playerProvider', function() {
  beforeEach(module('square'));

  var playerProvider, gameStateProvider, HumanPlayer, AIPlayer;

  beforeEach(inject(function (
      _playerProvider_, _gameStateProvider_, _HumanPlayer_, _AIPlayer_) {
    playerProvider = _playerProvider_;
    gameStateProvider = _gameStateProvider_;
    HumanPlayer = _HumanPlayer_;
    AIPlayer = _AIPlayer_;
  }));

  describe('getNextStartingRegionCoords', function () {

    it('returns proper following values', function () {
      var coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(0);
      expect(coords.y).toBe(0);

      playerProvider.addHuman();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(0);
      expect(coords.y).toBe(-1);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(1);
      expect(coords.y).toBe(-1);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(1);
      expect(coords.y).toBe(0);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(1);
      expect(coords.y).toBe(1);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(0);
      expect(coords.y).toBe(1);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-1);
      expect(coords.y).toBe(1);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-1);
      expect(coords.y).toBe(0);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-1);
      expect(coords.y).toBe(-1);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-1);
      expect(coords.y).toBe(-2);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(0);
      expect(coords.y).toBe(-2);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(1);
      expect(coords.y).toBe(-2);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(2);
      expect(coords.y).toBe(-2);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(2);
      expect(coords.y).toBe(-1);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(2);
      expect(coords.y).toBe(0);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(2);
      expect(coords.y).toBe(1);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(2);
      expect(coords.y).toBe(2);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(1);
      expect(coords.y).toBe(2);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(0);
      expect(coords.y).toBe(2);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-1);
      expect(coords.y).toBe(2);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-2);
      expect(coords.y).toBe(2);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-2);
      expect(coords.y).toBe(1);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-2);
      expect(coords.y).toBe(0);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-2);
      expect(coords.y).toBe(-1);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-2);
      expect(coords.y).toBe(-2);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-2);
      expect(coords.y).toBe(-3);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-1);
      expect(coords.y).toBe(-3);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(0);
      expect(coords.y).toBe(-3);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(1);
      expect(coords.y).toBe(-3);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(2);
      expect(coords.y).toBe(-3);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(3);
      expect(coords.y).toBe(-3);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(3);
      expect(coords.y).toBe(-2);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(3);
      expect(coords.y).toBe(-1);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(3);
      expect(coords.y).toBe(0);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(3);
      expect(coords.y).toBe(1);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(3);
      expect(coords.y).toBe(2);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(3);
      expect(coords.y).toBe(3);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(2);
      expect(coords.y).toBe(3);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(1);
      expect(coords.y).toBe(3);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(0);
      expect(coords.y).toBe(3);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-1);
      expect(coords.y).toBe(3);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-2);
      expect(coords.y).toBe(3);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-3);
      expect(coords.y).toBe(3);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-3);
      expect(coords.y).toBe(2);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-3);
      expect(coords.y).toBe(1);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-3);
      expect(coords.y).toBe(0);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-3);
      expect(coords.y).toBe(-1);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-3);
      expect(coords.y).toBe(-2);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-3);
      expect(coords.y).toBe(-3);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-3);
      expect(coords.y).toBe(-4);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-2);
      expect(coords.y).toBe(-4);

      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(4);
      expect(coords.y).toBe(-4);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(4);
      expect(coords.y).toBe(-3);

      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(4);
      expect(coords.y).toBe(4);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(3);
      expect(coords.y).toBe(4);

      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-4);
      expect(coords.y).toBe(4);

      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-4);
      expect(coords.y).toBe(-4);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-4);
      expect(coords.y).toBe(-5);

      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(-3);
      expect(coords.y).toBe(-5);

      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      playerProvider.addAI();
      coords = playerProvider._instance.getNextStartingRegionCoords();
      expect(coords.x).toBe(5);
      expect(coords.y).toBe(-5);

    });

  });

  describe('boundingNumber', function () {

    it('returns proper following values for start=0, direction=1, max=1, min=-1', function () {
      var start = 0;
      var startDirection = 1;
      var max = 1;
      var min = -1;
      var num = playerProvider._instance.boundingNumber(start, startDirection, max, min);
      expect(num.next).toBe(1);
      expect(num.direction).toBe(-1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(0);
      expect(num.direction).toBe(-1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(-1);
      expect(num.direction).toBe(1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(0);
      expect(num.direction).toBe(1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(1);
      expect(num.direction).toBe(-1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(0);
      expect(num.direction).toBe(-1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(-1);
      expect(num.direction).toBe(1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(0);
      expect(num.direction).toBe(1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(1);
      expect(num.direction).toBe(-1);
    });

    it('returns proper following values for start=0, direction=1, max=2, min=-2', function () {
      var start = 0;
      var startDirection = 1;
      var max = 2;
      var min = -2;
      var num = playerProvider._instance.boundingNumber(start, startDirection, max, min);
      expect(num.next).toBe(1);
      expect(num.direction).toBe(1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(2);
      expect(num.direction).toBe(-1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(1);
      expect(num.direction).toBe(-1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(0);
      expect(num.direction).toBe(-1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(-1);
      expect(num.direction).toBe(-1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(-2);
      expect(num.direction).toBe(1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(-1);
      expect(num.direction).toBe(1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(0);
      expect(num.direction).toBe(1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(1);
      expect(num.direction).toBe(1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(2);
      expect(num.direction).toBe(-1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(1);
      expect(num.direction).toBe(-1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(0);
      expect(num.direction).toBe(-1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(-1);
      expect(num.direction).toBe(-1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(-2);
      expect(num.direction).toBe(1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(-1);
      expect(num.direction).toBe(1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(0);
      expect(num.direction).toBe(1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(1);
      expect(num.direction).toBe(1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(2);
      expect(num.direction).toBe(-1);
    });

    it('returns proper following values for start=0, direction=1, max=1, min=-2', function () {
      var start = 0;
      var startDirection = 1;
      var max = 1;
      var min = -2;
      var num = playerProvider._instance.boundingNumber(start, startDirection, max, min);
      expect(num.next).toBe(1);
      expect(num.direction).toBe(-1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(0);
      expect(num.direction).toBe(-1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(-1);
      expect(num.direction).toBe(-1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(-2);
      expect(num.direction).toBe(1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(-1);
      expect(num.direction).toBe(1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(0);
      expect(num.direction).toBe(1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(1);
      expect(num.direction).toBe(-1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(0);
      expect(num.direction).toBe(-1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(-1);
      expect(num.direction).toBe(-1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(-2);
      expect(num.direction).toBe(1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(-1);
      expect(num.direction).toBe(1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(0);
      expect(num.direction).toBe(1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(1);
      expect(num.direction).toBe(-1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(0);
      expect(num.direction).toBe(-1);
    });

    it('returns proper following values for start=1, direction=-1, max=1, min=-1', function () {
      var start = 1;
      var startDirection = -1;
      var max = 1;
      var min = -1;
      var num = playerProvider._instance.boundingNumber(start, startDirection, max, min);
      expect(num.next).toBe(0);
      expect(num.direction).toBe(-1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(-1);
      expect(num.direction).toBe(1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(0);
      expect(num.direction).toBe(1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(1);
      expect(num.direction).toBe(-1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(0);
      expect(num.direction).toBe(-1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(-1);
      expect(num.direction).toBe(1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(0);
      expect(num.direction).toBe(1);
      num = playerProvider._instance.boundingNumber(num.next, num.direction, max, min);
      expect(num.next).toBe(1);
      expect(num.direction).toBe(-1);
    });

  });

  describe('addHuman', function () {

    beforeEach(function () {
      spyOn(HumanPlayer.prototype, 'setEventEmitter');
      spyOn(HumanPlayer.prototype, 'setNextDisclaimCooldown');
    });

    it('creates a new instance of HumanPlayer and ' +
       'assign it to `players.player-human` prop', function () {
      expect(playerProvider._instance.players['player-human'] instanceof HumanPlayer)
        .not.toBeTrue();
      playerProvider.addHuman();
      expect(playerProvider._instance.players['player-human'] instanceof HumanPlayer)
        .toBeTrue();
    });

    it('sets a new player\'s playerId to "player-human"', function () {
      playerProvider.addHuman();
      expect(playerProvider._instance.players['player-human'].playerId)
        .toBe('player-human');
    });

    it('calls setEventEmitter on a new HumanPlayer instance', function () {
      playerProvider.addHuman();
      expect(HumanPlayer.prototype.setEventEmitter).toHaveBeenCalledWith();
    });

    it('calls setNextDisclaimCooldown on a new HumanPlayer instance', function () {
      playerProvider.addHuman();
      expect(HumanPlayer.prototype.setNextDisclaimCooldown).toHaveBeenCalledWith();
    });

    it('returns a new HumanPlayer instance', function () {
      var result = playerProvider.addHuman();
      expect(result).toBe(playerProvider._instance.players['player-human']);
    });

  });

  describe('addAI', function () {

    beforeEach(function () {
      spyOn(AIPlayer.prototype, 'setEventEmitter');
      spyOn(AIPlayer.prototype, 'setNextDisclaimCooldown');
      spyOn(playerProvider._instance, 'getNextStartingRegionCoords');
    });

    it('creates a new instance of AIPlayer and ' +
       'assign it to `players.player-AI-0` prop if aiPlayersCounter is 0', function () {
      playerProvider._instance.aiPlayersCounter = 0;
      expect(playerProvider._instance.players['player-AI-0'] instanceof AIPlayer)
        .not.toBeTrue();
      playerProvider.addAI();
      expect(playerProvider._instance.players['player-AI-0'] instanceof AIPlayer)
        .toBeTrue();
    });

    it('creates a new instance of AIPlayer and ' +
       'assign it to `players.player-AI-7` prop if aiPlayersCounter is 7', function () {
      playerProvider._instance.aiPlayersCounter = 7;
      expect(playerProvider._instance.players['player-AI-7'] instanceof AIPlayer)
        .not.toBeTrue();
      playerProvider.addAI();
      expect(playerProvider._instance.players['player-AI-7'] instanceof AIPlayer)
        .toBeTrue();
    });

    it('sets a new player\'s playerId to "player-AI-0" if aiPlayersCounter is 0', function () {
      playerProvider._instance.aiPlayersCounter = 0;
      playerProvider.addAI();
      expect(playerProvider._instance.players['player-AI-0'].playerId)
        .toBe('player-AI-0');
    });

    it('sets a new player\'s playerId to "player-AI-6" if aiPlayersCounter is 6', function () {
      playerProvider._instance.aiPlayersCounter = 6;
      playerProvider.addAI();
      expect(playerProvider._instance.players['player-AI-6'].playerId)
        .toBe('player-AI-6');
    });

    it('calls getNextStartingRegionCoords', function () {
      playerProvider.addAI();
      expect(playerProvider._instance.getNextStartingRegionCoords)
        .toHaveBeenCalledWith();
    });

    it('calls setEventEmitter on a new addAI instance', function () {
      expect(AIPlayer.prototype.setEventEmitter).not.toHaveBeenCalled();
      playerProvider.addAI();
      expect(AIPlayer.prototype.setEventEmitter).toHaveBeenCalledWith();
    });

    it('calls setNextDisclaimCooldown on a new addAI instance', function () {
      expect(AIPlayer.prototype.setNextDisclaimCooldown).not.toHaveBeenCalled();
      playerProvider.addAI();
      expect(AIPlayer.prototype.setNextDisclaimCooldown).toHaveBeenCalledWith();
    });

    it('returns a new HumanPlayer instance', function () {
      var result = playerProvider.addAI();
      expect(result).toBe(playerProvider._instance.players['player-AI-0']);
    });

  });

  describe('nextPlayer', function () {

    it('returns a proper player for 2 players total', function () {
      var human = playerProvider.addHuman();
      var ai = playerProvider.addAI();
      expect(playerProvider.nextPlayer(human)).toBe(ai);
      expect(playerProvider.nextPlayer(ai)).toBe(human);
    });

    it('returns a proper player for 3 players total', function () {
      var human = playerProvider.addHuman();
      var ai1 = playerProvider.addAI();
      var ai2 = playerProvider.addAI();
      expect(playerProvider.nextPlayer(human)).toBe(ai1);
      expect(playerProvider.nextPlayer(ai1)).toBe(ai2);
      expect(playerProvider.nextPlayer(ai2)).toBe(human);
    });

    it('returns a proper player for 1 human player total', function () {
      var human = playerProvider.addHuman();
      expect(playerProvider.nextPlayer(human)).toBe(human);
    });

    it('returns a proper player for 1 ai player total', function () {
      var ai = playerProvider.addAI();
      expect(playerProvider.nextPlayer(ai)).toBe(ai);
    });

    it('returns a proper player for 2 ai players total', function () {
      var ai1 = playerProvider.addAI();
      var ai2 = playerProvider.addAI();
      expect(playerProvider.nextPlayer(ai1)).toBe(ai2);
      expect(playerProvider.nextPlayer(ai2)).toBe(ai1);
    });

    it('returns a proper player for 3 ai players total', function () {
      var ai1 = playerProvider.addAI();
      var ai2 = playerProvider.addAI();
      var ai3 = playerProvider.addAI();
      expect(playerProvider.nextPlayer(ai2)).toBe(ai3);
      expect(playerProvider.nextPlayer(ai1)).toBe(ai2);
      expect(playerProvider.nextPlayer(ai3)).toBe(ai1);
    });

    it('returns a proper player for 3 ai players total', function () {
      var ai1 = playerProvider.addAI();
      var ai2 = playerProvider.addAI();
      var ai3 = playerProvider.addAI();
      expect(playerProvider.nextPlayer(ai1)).toBe(ai2);
      expect(playerProvider.nextPlayer(ai2)).toBe(ai3);
      expect(playerProvider.nextPlayer(ai3)).toBe(ai1);
    });

    it('returns a proper player for 10 players total', function () {
      var human = playerProvider.addHuman();
      var ai1 = playerProvider.addAI();
      var ai2 = playerProvider.addAI();
      var ai3 = playerProvider.addAI();
      var ai4 = playerProvider.addAI();
      var ai5 = playerProvider.addAI();
      var ai6 = playerProvider.addAI();
      var ai7 = playerProvider.addAI();
      var ai8 = playerProvider.addAI();
      var ai9 = playerProvider.addAI();
      expect(playerProvider.nextPlayer(human)).toBe(ai1);
      expect(playerProvider.nextPlayer(ai1)).toBe(ai2);
      expect(playerProvider.nextPlayer(ai2)).toBe(ai3);
      expect(playerProvider.nextPlayer(ai3)).toBe(ai4);
      expect(playerProvider.nextPlayer(ai4)).toBe(ai5);
      expect(playerProvider.nextPlayer(ai5)).toBe(ai6);
      expect(playerProvider.nextPlayer(ai6)).toBe(ai7);
      expect(playerProvider.nextPlayer(ai7)).toBe(ai8);
      expect(playerProvider.nextPlayer(ai8)).toBe(ai9);
      expect(playerProvider.nextPlayer(ai9)).toBe(human);
      expect(playerProvider.nextPlayer(human)).toBe(ai1);
      expect(playerProvider.nextPlayer(ai5)).toBe(ai6);
      expect(playerProvider.nextPlayer(ai2)).toBe(ai3);
    });

    it('returns a proper player for simple_map_3x3_5x5 fixture loaded', function (done) {
      gameStateProvider.load(process.env.PWD + '/test_fixtures/simple_map_3x3_5x5.json', function () {
        var human = playerProvider.getHuman();
        var ai = playerProvider.getFirstAI();
        expect(playerProvider.nextPlayer(human)).toBe(ai);
        expect(playerProvider.nextPlayer(ai)).toBe(human);
        done();
      });
    });
  });

  describe('allPlayersDefeated', function () {

    var fakePlayers;

    beforeEach(function () {
      fakePlayers = {
        'player-id-1': {
          defeated: false
        },
        'player-id-2': {
          defeated: false
        },
        'player-id-3': {
          defeated: false
        }
      };
      playerProvider._instance.players = fakePlayers;
    });

    it('returns true if all players are defeated', function () {
      fakePlayers['player-id-1'].defeated = true;
      fakePlayers['player-id-2'].defeated = true;
      fakePlayers['player-id-3'].defeated = true;
      var result = playerProvider.allPlayersDefeated();
      expect(result).toBeTrue();
    });

    it('returns false if at least one player is not defeated', function () {
      fakePlayers['player-id-1'].defeated = true;
      fakePlayers['player-id-2'].defeated = false;
      fakePlayers['player-id-3'].defeated = true;
      var result = playerProvider.allPlayersDefeated();
      expect(result).toBeFalse();
    });

    it('returns false if all players are not defeated', function () {
      fakePlayers['player-id-1'].defeated = false;
      fakePlayers['player-id-2'].defeated = false;
      fakePlayers['player-id-3'].defeated = false;
      var result = playerProvider.allPlayersDefeated();
      expect(result).toBeFalse();
    });

  });

});

