'use strict';

var EventEmitter = require('events');

angular.module('square')

.factory('Player', [function () {

  function Player (playerId, startingRegionCoords) {
    this.playerId = playerId;
    this.startingRegionCoords = startingRegionCoords;
    this.squaresByLevel = {
      2: [],
      3: [],
      4: [],
      5: []
    };
    this.technologies = [];
  }

  Player.prototype.playerId = '';
  Player.prototype.startingRegionCoords = null;
  Player.prototype.defeated = false;
  Player.prototype.squaresByLevel = null;
  Player.prototype.technologies = null;
  Player.prototype.onMoveDone = null;
  Player.prototype.eventEmitter = null;
  Player.prototype.disclaimCooldown = 0;
  Player.prototype.baseDisclaimCooldown = 3;

  Player.prototype.setNextDisclaimCooldown = function () {
    //TODO: affected by buildings, char perks etc.
    this.disclaimCooldown = this.baseDisclaimCooldown;
    ++this.baseDisclaimCooldown;
    this.eventEmitter.emit('updatingDisclaimCooldown');
  };

  Player.prototype.nextMove = function (callback) {

  };

  Player.prototype.toString = function () {
    return this.constructor.name + ':' + this.playerId;
  };

  Player.prototype.setEventEmitter = function () {
    this.eventEmitter = new EventEmitter();
    this.eventEmitter.setMaxListeners(1 << 20);  // 2^20
    var that = this;
    this.eventEmitter.on('playerTurnBegin', function () {
      if (that.disclaimCooldown > 0) {
        --that.disclaimCooldown;
        that.eventEmitter.emit('updatingDisclaimCooldown');
      }
    });
  };

  return Player;
}])

.factory('AIPlayer', ['Player', 'AI', function (Player, AI) {

  function AIPlayer() {
    Player.apply(this, arguments);
    this.ai = new AI(this);  //TODO: remove from constructor
  }

  AIPlayer.prototype = Object.create(Player.prototype);
  AIPlayer.prototype.constructor = AIPlayer;

  AIPlayer.prototype.ai = null;

  AIPlayer.prototype.nextMove = function (callback) {
    this.ai.nextMove();
    callback();
  };

  return AIPlayer;
}])

.factory('HumanPlayer', ['Player', function (Player) {

  function HumanPlayer() {
    Player.apply(this, arguments);
  }

  HumanPlayer.prototype = Object.create(Player.prototype);
  HumanPlayer.prototype.constructor = HumanPlayer;

  HumanPlayer.prototype.nextMove = function (callback) {
    this.onMoveDone = function () { callback(); };
    this.eventEmitter.emit('humanTurnBegin', this);
  };

  HumanPlayer.prototype.moveDone = function () {
    var onMoveDone = this.onMoveDone;
    this.onMoveDone = null;
    onMoveDone();
  };

  return HumanPlayer;
}]);