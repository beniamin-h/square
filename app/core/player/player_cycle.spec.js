'use strict';

describe('PlayerCycle', function() {
  beforeEach(module('square'));

  var playerCycle, playerProvider, turnTicker;
  var timers;

  beforeEach(inject(function (_playerCycle_, _playerProvider_, _turnTicker_) {
    playerCycle = _playerCycle_;
    playerProvider = _playerProvider_;
    turnTicker = _turnTicker_;

    timers = require('timers');
  }));

  describe('emitPlayerTurnBeginsEvents', function () {

    var fakePlayer;

    beforeEach(function () {
      fakePlayer = jasmine.createSpyObj('player', ['eventEmitter']);
      fakePlayer.eventEmitter = jasmine.createSpyObj('eventEmitter', ['emit']);
    });

    it('emits a series of events', function () {
      playerCycle._instance.emitPlayerTurnBeginsEvents(fakePlayer);
      expect(fakePlayer.eventEmitter.emit.calls.allArgs()).toEqual([
        ['playerTurnBegin', fakePlayer],
        ['processingStorage', fakePlayer],
        ['proceedingConstructions', fakePlayer],
        ['resumingStoppedBuildings', fakePlayer],
        ['updatingBlocksAffordability', fakePlayer]
      ]);
    });

  });

  describe('onMoveDone', function () {

    var fakePlayer;

    beforeEach(function () {
      fakePlayer = jasmine.createSpy('player');
      spyOn(timers, 'setImmediate');
      spyOn(playerCycle, 'next');
    });

    it('calls setImmediate', function () {
      playerCycle._instance.onMoveDone(playerCycle, fakePlayer);
      expect(timers.setImmediate).toHaveBeenCalledWith(jasmine.any(Function));
    });

    it('calls next on playerCycle in setImmediate callback', function () {
      timers.setImmediate.and.callFake(function (callback) { callback(); });
      playerCycle._instance.onMoveDone(playerCycle, fakePlayer);
      expect(playerCycle.next).toHaveBeenCalledWith(fakePlayer);
    });

  });

  describe('start', function () {

    var fakePlayer;

    beforeEach(function () {
      fakePlayer = jasmine.createSpyObj('player', ['nextMove']);
      spyOn(playerProvider, 'getHuman').and.returnValue(fakePlayer);
      spyOn(playerProvider, 'getFirstAI').and.returnValue(fakePlayer);
      spyOn(playerCycle._instance, 'onMoveDone');
    });

    it('calls getHuman on playerProvider', function () {
      playerCycle.start();
      expect(playerProvider.getHuman).toHaveBeenCalledWith();
    });

    it('calls getFirstAI on playerProvider when getHuman returns falsy result', function () {
      playerProvider.getHuman.and.returnValue(undefined);
      playerCycle.start();
      expect(playerProvider.getFirstAI).toHaveBeenCalledWith();
    });

    it('calls nextMove on the player', function () {
      playerCycle.start();
      expect(fakePlayer.nextMove).toHaveBeenCalledWith(jasmine.any(Function));
    });

    it('calls onMoveDone in nextMove callback', function () {
      var nextMoveCallback = function () { };
      fakePlayer.nextMove.and.callFake(function (callback) {
        nextMoveCallback = callback;
      });
      playerCycle.start();
      nextMoveCallback();
      expect(playerCycle._instance.onMoveDone).toHaveBeenCalledWith(playerCycle, fakePlayer);
    });

  });

  describe('next', function () {

    var fakeNextPlayer;
    var fakePlayerDone;

    beforeEach(function () {
      fakeNextPlayer = jasmine.createSpyObj('player', ['nextMove', 'defeated']);
      fakePlayerDone = jasmine.createSpy('playerDone');
      spyOn(playerCycle._instance, 'emitPlayerTurnBeginsEvents');
      spyOn(playerCycle._instance, 'onMoveDone');
      playerCycle._instance.eventEmitter = jasmine.createSpyObj('eventEmitter', ['emit']);
      spyOn(playerProvider, 'nextPlayer').and.returnValue(fakeNextPlayer);
      spyOn(playerProvider, 'allPlayersDefeated');
      spyOn(turnTicker, 'tick');
    });

    it('calls nextPlayer on playerProvider', function () {
      playerCycle.next(fakePlayerDone);
      expect(playerProvider.nextPlayer).toHaveBeenCalledWith(fakePlayerDone);
    });

    it('calls emitPlayerTurnBeginsEvents if next player is not defeated', function () {
      fakeNextPlayer.defeated = false;
      playerCycle.next(fakePlayerDone);
      expect(playerCycle._instance.emitPlayerTurnBeginsEvents)
        .toHaveBeenCalledWith(fakeNextPlayer);
    });

    it('calls nextMove if next player is not defeated', function () {
      fakeNextPlayer.defeated = false;
      playerCycle.next(fakePlayerDone);
      expect(fakeNextPlayer.nextMove).toHaveBeenCalledWith(jasmine.any(Function));
    });

    it('calls onMoveDone in next player\'s nextMove callback ' +
       'if next player is not defeated', function () {
      var nextMoveCallback = function () { };
      fakeNextPlayer.defeated = false;
      fakeNextPlayer.nextMove.and.callFake(function (callback) {
        nextMoveCallback = callback;
      });
      playerCycle.next(fakePlayerDone);
      nextMoveCallback();
      expect(playerCycle._instance.onMoveDone).toHaveBeenCalledWith(playerCycle, fakeNextPlayer);
    });

    it('calls onMoveDone if next player is defeated but not all players are defeated', function () {
      fakeNextPlayer.defeated = true;
      playerProvider.allPlayersDefeated.and.returnValue(false);
      playerCycle.next(fakePlayerDone);
      expect(playerCycle._instance.onMoveDone).toHaveBeenCalledWith(playerCycle, fakeNextPlayer);
    });

    it('emits allPlayersDefeated if next player is defeated and all players are defeated', function () {
      fakeNextPlayer.defeated = true;
      playerProvider.allPlayersDefeated.and.returnValue(true);
      playerCycle.next(fakePlayerDone);
      expect(playerCycle._instance.eventEmitter.emit)
        .toHaveBeenCalledWith('allPlayersDefeated');
    });

    it('calls tick on turnTicker if nextPlayer is the first player', function () {
      playerCycle._instance.firstPlayer = fakeNextPlayer;
      playerCycle.next(fakePlayerDone);
      expect(turnTicker.tick).toHaveBeenCalledWith();
    });

  });

});
