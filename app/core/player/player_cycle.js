'use strict';

var timers = require('timers');
var EventEmitter = require('events');

angular.module('square').factory('playerCycle', ['playerProvider', 'turnTicker',
    function (playerProvider, turnTicker) {

  var that = this;
  that.eventEmitter = new EventEmitter();
  that.firstPlayer = null;

  that.emitPlayerTurnBeginsEvents = function (player) {
    player.eventEmitter.emit('playerTurnBegin', player);
    player.eventEmitter.emit('processingStorage', player);
    player.eventEmitter.emit('proceedingConstructions', player);
    player.eventEmitter.emit('resumingStoppedBuildings', player);
    player.eventEmitter.emit('updatingBlocksAffordability', player);
  };

  that.onMoveDone = function (playerCycle, player) {
    timers.setImmediate(function () {
      playerCycle.next(player);
    });
  };

  return {
    eventEmitter: that.eventEmitter,
    start: function () {
      var playerCycle = this;
      var player = playerProvider.getHuman() || playerProvider.getFirstAI();
      that.firstPlayer = player;
      player.nextMove(function () {
        that.onMoveDone(playerCycle, player);
      });
    },
    next: function (playerDone) {
      var playerCycle = this;
      var nextPlayer = playerProvider.nextPlayer(playerDone);
      if (nextPlayer === that.firstPlayer) {
        turnTicker.tick();
      }
      if (!nextPlayer.defeated) {
        that.emitPlayerTurnBeginsEvents(nextPlayer);
        nextPlayer.nextMove(function () {
          that.onMoveDone(playerCycle, nextPlayer);
        });
      } else if (!playerProvider.allPlayersDefeated()) {
        that.onMoveDone(playerCycle, nextPlayer);
      } else {
        that.eventEmitter.emit('allPlayersDefeated');
      }
    },
    _instance: this
  };
}]);
