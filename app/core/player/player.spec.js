'use strict';

describe('Player', function() {
  beforeEach(module('square'));

  var Player, AIPlayer, HumanPlayer, EventEmitter, testingUtils;

  beforeEach(inject(function (
      _Player_, _AIPlayer_, _HumanPlayer_, _testingUtils_) {
    Player = _Player_;
    AIPlayer = _AIPlayer_;
    HumanPlayer = _HumanPlayer_;
    EventEmitter = require('events');
    testingUtils = _testingUtils_;
  }));

  describe('Player', function () {

    var player;

    describe('constructor', function () {

      it('sets proper playerId', function () {
        var mockedPlayerId = jasmine.createSpy('playerId');
        player = new Player(mockedPlayerId, null);
        expect(player.playerId).toBe(mockedPlayerId, null);
      });

      it('sets proper startingRegionCoords', function () {
        var mockedStartingRegionCoords = jasmine.createSpy('startingRegionCoords');
        player = new Player(null, mockedStartingRegionCoords);
        expect(player.startingRegionCoords).toBe(mockedStartingRegionCoords, null);
      });

      it('sets proper squaresByLevel', function () {
        player = new Player();
        expect(player.squaresByLevel).toEqual({
          2: [],
          3: [],
          4: [],
          5: []
        });
      });

      it('sets proper technologies', function () {
        player = new Player();
        expect(player.technologies).toEqual([]);
      });

      it('does not set eventEmitter', function () {
        player = new Player();
        expect(player.eventEmitter).toBeNull();
      });

    });

    describe('setNextDisclaimCooldown', function () {

      beforeEach(function () {
        player = new Player();
        spyOn(player, 'disclaimCooldown');
        spyOn(player, 'baseDisclaimCooldown');
        player.eventEmitter = jasmine.createSpyObj('eventEmitter', ['emit']);
      });

      it('sets disclaimCooldown to baseDisclaimCooldown', function () {
        player.baseDisclaimCooldown = 8;
        expect(player.disclaimCooldown).not.toBe(8);
        player.setNextDisclaimCooldown();
        expect(player.disclaimCooldown).toBe(8);
      });

      it('increments baseDisclaimCooldown', function () {
        player.baseDisclaimCooldown = 78;
        player.setNextDisclaimCooldown();
        expect(player.baseDisclaimCooldown).toBe(79);
      });

      it('emits updatingDisclaimCooldown', function () {
        player.setNextDisclaimCooldown();
        expect(player.eventEmitter.emit)
          .toHaveBeenCalledWith('updatingDisclaimCooldown');
      });

    });

    describe('nextMove', function () {

      beforeEach(function () {
        player = new Player();
      });

      it('does not throw an error', function () {
        expect(player.nextMove).not.toThrowError();
      });

    });

    describe('toString', function () {

      beforeEach(function () {
        player = new Player();
      });

      it('returns Player:abc for playerId equals "abc"', function () {
        player.playerId = 'abc';
        var result = player.toString();
        expect(result).toBe('Player:abc');
      });

    });

    describe('setEventEmitter', function () {

      beforeEach(function () {
        player = new Player();
        spyOn(player, 'disclaimCooldown');
        spyOn(EventEmitter.prototype, 'setMaxListeners').and.callThrough();
        spyOn(EventEmitter.prototype, 'on').and.callThrough();
        spyOn(EventEmitter.prototype, 'emit').and.callThrough();
      });

      it('creates a new instance of EventEmitter and ' +
         'sets it to `eventEmitter` prop', function () {
        player.setEventEmitter();
        expect(player.eventEmitter instanceof EventEmitter).toBeTrue();
      });

      it('set eventEmitter max listeners to 2^20', function () {
        player.setEventEmitter();
        expect(player.eventEmitter.setMaxListeners)
          .toHaveBeenCalledWith(Math.pow(2, 20));
      });

      it('sets event listener on playerTurnBegin', function () {
        player.setEventEmitter();
        expect(player.eventEmitter.on)
          .toHaveBeenCalledWith('playerTurnBegin', jasmine.any(Function));
      });

      it('decrements disclaimCooldown on playerTurnBegin event ' +
         'if disclaimCooldown is greater than 0', function () {
        var callbacks = testingUtils.getEventListeners(
          EventEmitter.prototype, 'playerTurnBegin');
        player.disclaimCooldown = 11;
        player.setEventEmitter();
        expect(player.disclaimCooldown).not.toBe(10);
        callbacks[0]();
        expect(player.disclaimCooldown).toBe(10);
      });

      it('emits updatingDisclaimCooldown on playerTurnBegin event ' +
         'if disclaimCooldown is greater than 0', function () {
        var callbacks = testingUtils.getEventListeners(
          EventEmitter.prototype, 'playerTurnBegin');
        player.disclaimCooldown = 11;
        player.setEventEmitter();
        expect(EventEmitter.prototype.emit).not.toHaveBeenCalled();
        callbacks[0]();
        expect(EventEmitter.prototype.emit)
          .toHaveBeenCalledWith('updatingDisclaimCooldown');
      });

      it('does not change disclaimCooldown on playerTurnBegin event ' +
         'if disclaimCooldown is less or equal to 0', function () {
        var callbacks = testingUtils.getEventListeners(
          EventEmitter.prototype, 'playerTurnBegin');
        player.disclaimCooldown = 0;
        player.setEventEmitter();
        expect(player.disclaimCooldown).not.toBe(10);
        callbacks[0]();
        expect(player.disclaimCooldown).not.toBe(10);
      });

      it('does not emit updatingDisclaimCooldown on playerTurnBegin event ' +
         'if disclaimCooldown is less or equal to 0', function () {
        var callbacks = testingUtils.getEventListeners(
          EventEmitter.prototype, 'playerTurnBegin');
        player.disclaimCooldown = -5;
        player.setEventEmitter();
        expect(EventEmitter.prototype.emit).not.toHaveBeenCalled();
        callbacks[0]();
        expect(EventEmitter.prototype.emit).not.toHaveBeenCalled();
      });

    });

  });

  describe('AIPlayer', function () {

    var player;

    beforeEach(function () {
      player = new AIPlayer();
    });

    it('inherit from Player', function () {
      expect(player instanceof Player);
    });

    describe('nextMove', function () {

      var fakeCallback;

      beforeEach(function () {
        player.ai = jasmine.createSpyObj('ai', ['nextMove']);
        fakeCallback = jasmine.createSpy('callback');
      });

      it('calls nextMove on ai', function () {
        player.nextMove(fakeCallback);
        expect(player.ai.nextMove).toHaveBeenCalledWith();
      });

      it('calls the given callback', function () {
        player.nextMove(fakeCallback);
        expect(fakeCallback).toHaveBeenCalledWith();
      });

    });

  });

  describe('HumanPlayer', function () {

    var player;

    beforeEach(function () {
      player = new HumanPlayer();
      player.eventEmitter = jasmine.createSpyObj('eventEmitter', ['emit']);
    });

    it('inherit from Player', function () {
      expect(player instanceof Player);
    });

    describe('nextMove', function () {

      var fakeCallback;

      beforeEach(function () {
        fakeCallback = jasmine.createSpy('callback');
      });

      it('calls the given callback on calling onMoveDone', function () {
        player.nextMove(fakeCallback);
        expect(fakeCallback).not.toHaveBeenCalled();
        player.onMoveDone();
        expect(fakeCallback).toHaveBeenCalledWith();
      });

      it('emits humanTurnBegin', function () {
        player.nextMove(fakeCallback);
        expect(player.eventEmitter.emit)
          .toHaveBeenCalledWith('humanTurnBegin', player);
      });

    });

  });

});