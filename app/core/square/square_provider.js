'use strict';

angular.module('square').factory('squareProvider', ['SquaresMap', 'SquareFactory',
    function (SquaresMap, SquareFactory) {

  var that = this;
  that.squaresMap = null;
  that.squareFactory = null;

  return {
    initSquareProvider: function (players) {
      this._initSquareFactory();
      this._initSquaresMap(players);
    },
    _initSquareFactory: function () {
      that.squareFactory = new SquareFactory();
      return that.squareFactory;
    },
    _initSquaresMap: function (players) {
      var squaresMap = new SquaresMap(that.squareFactory);
      squaresMap.setPlayers(players);
      that.squareFactory.setSquaresMap(squaresMap);
      that.squaresMap = squaresMap;
      return that.squaresMap;
    },
    loadSquareFactory: function (squareFactory) {
      that.squareFactory = squareFactory;
    },
    loadSquaresMap: function (squaresMap) {
      that.squaresMap = squaresMap;
    },
    getSquareFactory: function () {
      return that.squareFactory;
    },
    getSquaresMap: function () {
      return that.squaresMap;
    },
    _instance: this
  };
}]);
