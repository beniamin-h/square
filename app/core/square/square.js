'use strict';

angular.module('square').factory('Square', ['Math', 'ArrayUtils',
    function (Math, ArrayUtils) {

  function Square(regionX, regionY, x, y, blocks, level, player) {
    this.regionX = regionX;
    this.regionY = regionY;
    this.x = x;
    this.y = y;
    this.blocks = blocks || [];
    this.level = level || 2;  // Base level for all squares
    this.player = player;
    this.buildingsByClass = {};
    this.buildings = [];
    this.blockResourceProduction = {};
    this.blockResourceUpkeep = {};
    this.blockResourceOutcome = {};
    this.buildingResourceProduction = {};
    this.buildingResourceUpkeep = {};
    this.buildingResourceOutcome = {};
    this.resourceOutcomeTotal = {};
  }

  Square.prototype.regionX = 0;
  Square.prototype.regionY = 0;
  Square.prototype.x = 0;
  Square.prototype.y = 0;
  Square.prototype.blocks = null;
  Square.prototype.level = 0;
  Square.prototype.player = null;
  Square.prototype.buildingsByClass = null;
  Square.prototype.buildings = null;
  Square.prototype.goldIncomeInternal = 0;
  Square.prototype.goldIncomeBlocks = 0;
  Square.prototype.goldIncomeBuildings = 0;
  Square.prototype.goldUpkeepBuildings = 0;
  Square.prototype.goldOutcomeTotal = 0;
  Square.prototype.blockResourceProduction = null;
  Square.prototype.blockResourceUpkeep = null;
  Square.prototype.blockResourceOutcome = null;
  Square.prototype.buildingResourceProduction = null;
  Square.prototype.buildingResourceUpkeep = null;
  Square.prototype.buildingResourceOutcome = null;
  Square.prototype.resourceOutcomeTotal = null;
  Square.goldIncomePerLevel = {
    2: 1,
    3: 6,
    4: 32,
    5: 160
  };

  Square.getWidth = function (level) {
    return Math.pow(2, level - 1);
  };

  Square.getHeight = function (level) {
    return Math.pow(2, level - 1);
  };

  Square.prototype.toString = function () {
    return 'Square-r' + this.regionX + 'x' + this.regionY + '_' +
      this.x + 'x' + this.y + 'L' + this.level;
  };

  Square.prototype.getWidth = function () {
    return Square.getWidth(this.level);
  };

  Square.prototype.getHeight = function () {
    return Square.getHeight(this.level);
  };

  Square.prototype.addBuilding = function (building) {
    this.buildingsByClass[building.constructor.name] = this.buildingsByClass[building.constructor.name] || [];
    this.buildingsByClass[building.constructor.name].push(building);
    this.buildings.push(building);
  };

  Square.prototype.removeBuilding = function (building) {
    ArrayUtils.removeElement(this.buildingsByClass[building.constructor.name], building);
    ArrayUtils.removeElement(this.buildings, building);
  };

  Square.prototype.getBuildingCountByClass = function (buildingClassName) {
    if (!buildingClassName) {
      throw new Error('Invalid buildingClassName given: ' + buildingClassName);
    }
    return this.buildingsByClass[buildingClassName] ? this.buildingsByClass[buildingClassName].length : 0;
  };

  Square.prototype.calcGoldIncomeInternal = function () {
    this.goldIncomeInternal = Square.goldIncomePerLevel[this.level];
    //TODO: technology impact, character perks impact etc.
  };

  Square.prototype._calcGoldIncomeBlocks = function () {
    this.goldIncomeBlocks = 0;
    this.blocks.forEach(function (block) {
      if (block.disabled) {
        return;
      }
      this.goldIncomeBlocks += block.kind.calcGoldGain(block.level, block.rich);
    }, this);
  };

  Square.prototype._calcGoldIncomeBuildings = function () {
    this.goldIncomeBuildings = 0;
    this.buildings.forEach(function (building) {
      this.goldIncomeBuildings += building.goldIncome;
    }, this);
  };

  Square.prototype._calcGoldUpkeepBuildings = function () {
    this.goldUpkeepBuildings = 0;
    this.buildings.forEach(function (building) {
      this.goldUpkeepBuildings += building.goldUpkeep;
    }, this);
  };

  Square.prototype.calcGoldOutcomeTotal = function () {
    this.calcGoldIncomeInternal();
    this._calcGoldIncomeBlocks();
    this._calcGoldIncomeBuildings();
    this._calcGoldUpkeepBuildings();
    this.goldOutcomeTotal = this.goldIncomeInternal + this.goldIncomeBlocks +
                            this.goldIncomeBuildings - this.goldUpkeepBuildings;
  };

  Square.prototype._calcBlockResourceProductionAndUpkeep = function () {
    this.blockResourceProduction = {};
    this.blockResourceUpkeep = {};
    this.blocks.forEach(function (block) {
      if (block.disabled) {
        return;
      }
      var blockGain = block.kind.calcResourceGain(block.level, block.rich);
      for (var resName in blockGain) {
        this.blockResourceProduction[resName] = this.blockResourceProduction[resName] || 0;
        this.blockResourceProduction[resName] += blockGain[resName];
      }
      var blockCost = block.kind.calcResourceCost();
      for (var costResName in blockCost) {
        this.blockResourceUpkeep[costResName] = this.blockResourceUpkeep[costResName] || 0;
        this.blockResourceUpkeep[costResName] += blockCost[costResName];
      }
    }, this);
  };

  Square.prototype._calcBlockResourceOutcome = function () {
    this._calcBlockResourceProductionAndUpkeep();
    this.blockResourceOutcome = {};
    for (var resName in this.blockResourceProduction) {
      this.blockResourceOutcome[resName] = this.blockResourceProduction[resName];
    }
    for (var upkeepResName in this.blockResourceUpkeep) {
      this.blockResourceOutcome[upkeepResName] = this.blockResourceOutcome[upkeepResName] || 0;
      this.blockResourceOutcome[upkeepResName] -= this.blockResourceUpkeep[upkeepResName];
    }
    Object.keys(this.blockResourceOutcome).forEach(function (outcomeResName) {
      if (this.blockResourceOutcome[outcomeResName] === 0) {
        delete this.blockResourceOutcome[outcomeResName];
      }
    }, this);
  };

  Square.prototype._calcBuildingResourceProductionAndUpkeep = function () {
    this.buildingResourceProduction = {};
    this.buildingResourceUpkeep = {};
    this.buildings.forEach(function (building) {
      for (var resName in building.resourceProduction) {
        this.buildingResourceProduction[resName] = this.buildingResourceProduction[resName] || 0;
        this.buildingResourceProduction[resName] += building.resourceProduction[resName];
      }
      for (var upkeepResName in building.resourceUpkeep) {
        this.buildingResourceUpkeep[upkeepResName] = this.buildingResourceUpkeep[upkeepResName] || 0;
        this.buildingResourceUpkeep[upkeepResName] += building.resourceUpkeep[upkeepResName];
      }
    }, this);
  };

  Square.prototype._calcBuildingResourceOutcome = function () {
    this._calcBuildingResourceProductionAndUpkeep();
    this.buildingResourceOutcome = {};
    for (var resName in this.buildingResourceProduction) {
      this.buildingResourceOutcome[resName] = this.buildingResourceProduction[resName];
    }
    for (var upkeepResName in this.buildingResourceUpkeep) {
      this.buildingResourceOutcome[upkeepResName] = this.buildingResourceOutcome[upkeepResName] || 0;
      this.buildingResourceOutcome[upkeepResName] -= this.buildingResourceUpkeep[upkeepResName];
    }
    Object.keys(this.buildingResourceOutcome).forEach(function (outcomeResName) {
      if (this.buildingResourceOutcome[outcomeResName] === 0) {
        delete this.buildingResourceOutcome[outcomeResName];
      }
    }, this);
  };

  Square.prototype.calcResourceOutcomeTotal = function () {
    this._calcBlockResourceOutcome();
    this._calcBuildingResourceOutcome();
    this.resourceOutcomeTotal = {};
    for (var resName in this.blockResourceOutcome) {
      this.resourceOutcomeTotal[resName] = this.blockResourceOutcome[resName];
    }
    for (var buildingResName in this.buildingResourceOutcome) {
      this.resourceOutcomeTotal[buildingResName] = this.resourceOutcomeTotal[buildingResName] || 0;
      this.resourceOutcomeTotal[buildingResName] += this.buildingResourceOutcome[buildingResName];
    }
  };

  Square.prototype.calcGoldAndResourceOutcomes = function () {
    this.calcGoldOutcomeTotal();
    this.calcResourceOutcomeTotal();
  };

  return Square;
}]);