'use strict';

describe('Square', function() {
  beforeEach(module('square'));

  var Square, Market, Tavern, Block,
    Water, Mountain, Forest, Plain, Hill,
    Coalfield, WildHorses, GoldDeposit, SilverDeposit, GemstoneDeposit;

  beforeEach(inject(function (_Square_, _Market_, _Tavern_, _Block_,
      _Water_, _Mountain_, _Forest_, _Plain_, _Hill_,
      _Coalfield_, _WildHorses_, _GoldDeposit_, _SilverDeposit_, _GemstoneDeposit_) {
    Square = _Square_;
    Market = _Market_;
    Tavern = _Tavern_;
    Block = _Block_;

    Water = _Water_;
    Mountain = _Mountain_;
    Forest = _Forest_;
    Plain = _Plain_;
    Hill = _Hill_;

    Coalfield = _Coalfield_;
    WildHorses = _WildHorses_;
    GoldDeposit = _GoldDeposit_;
    SilverDeposit = _SilverDeposit_;
    GemstoneDeposit = _GemstoneDeposit_;
  }));

  describe('getWidth', function () {

    it('returns 2 for a square level 2', function () {
      var square = new Square(0, 0, 0, 0, [], 2, null);
      expect(square.getWidth()).toBe(2);
    });

    it('returns 4 for a square level 3', function () {
      var square = new Square(0, 0, 0, 0, [], 3, null);
      expect(square.getWidth()).toBe(4);
    });

    it('returns 8 for a square level 4', function () {
      var square = new Square(0, 0, 0, 0, [], 4, null);
      expect(square.getWidth()).toBe(8);
    });

    it('returns 16 for a square level 5', function () {
      var square = new Square(0, 0, 0, 0, [], 5, null);
      expect(square.getWidth()).toBe(16);
    });

    it('returns 512 for a square level 10', function () {
      var square = new Square(0, 0, 0, 0, [], 10, null);
      expect(square.getWidth()).toBe(512);
    });

  });

  describe('getHeight', function () {

    it('returns the same results as getWidth', function () {
      var square = new Square(0, 0, 0, 0, [], 2, null);
      expect(square.getHeight()).toBe(square.getWidth());
      var square_lvl3 = new Square(0, 0, 0, 0, [], 3, null);
      expect(square_lvl3.getHeight()).toBe(square_lvl3.getWidth());
      var square_lvl4 = new Square(0, 0, 0, 0, [], 4, null);
      expect(square_lvl4.getHeight()).toBe(square_lvl4.getWidth());
      var square_lvl50 = new Square(0, 0, 0, 0, [], 50, null);
      expect(square_lvl50.getHeight()).toBe(square_lvl50.getWidth());
    });

  });

  describe('toString', function () {

    it('returns Square-r5x-3_4x2L2 for square in region x: 5, y: -3,' +
       'at block x: 4, y: 2, with level 2', function () {
      var square = new Square(5, -3, 4, 2, [], 2);
      var result = square.toString();
      expect(result).toBe('Square-r5x-3_4x2L2');
    });

  });

  describe('addBuilding', function () {

    it('adds the given building to buildingsByClass property under a proper index', function () {
      var square = new Square(0, 1);
      expect(square.buildingsByClass['FakeBuilding']).toBeUndefined();
      function FakeBuilding() { return null; }

      var fakeBuildingInstance = new FakeBuilding();
      square.addBuilding(fakeBuildingInstance);
      expect(square.buildingsByClass['FakeBuilding'].length).toBe(1);
      expect(square.buildingsByClass['FakeBuilding'][0]).toBe(fakeBuildingInstance);

      var fakeBuildingInstance2 = new FakeBuilding();
      square.addBuilding(fakeBuildingInstance2);
      expect(square.buildingsByClass['FakeBuilding'].length).toBe(2);
      expect(square.buildingsByClass['FakeBuilding'][1]).toBe(fakeBuildingInstance2);
    });

    it('adds the given building to buildings property', function () {
      var square = new Square(0, 1);
      expect(square.buildings.length).toBe(0);
      function FakeBuilding() { return null; }

      var fakeBuildingInstance = new FakeBuilding();
      square.addBuilding(fakeBuildingInstance);
      expect(square.buildings.length).toBe(1);
      expect(square.buildings[0]).toBe(fakeBuildingInstance);

      var fakeBuildingInstance2 = new FakeBuilding();
      square.addBuilding(fakeBuildingInstance2);
      expect(square.buildings.length).toBe(2);
      expect(square.buildings[1]).toBe(fakeBuildingInstance2);
    });

  });

  describe('removeBuilding', function () {

    it('removes the given building from buildingsByClass property from a proper index', function () {
      var square = new Square(0, 1);
      function FakeBuilding(id) { this.toString = function () { return id; }; }
      var fakeBuildingInstance = new FakeBuilding('fake-building-1');
      var fakeBuildingInstance2 = new FakeBuilding('fake-building-2');
      square.addBuilding(fakeBuildingInstance);
      square.addBuilding(fakeBuildingInstance2);
      expect(square.buildingsByClass['FakeBuilding'].length).toBe(2);

      square.removeBuilding(fakeBuildingInstance);
      expect(square.buildingsByClass['FakeBuilding'].length).toBe(1);
      expect(square.buildingsByClass['FakeBuilding'][0]).not.toBe(fakeBuildingInstance);
      expect(square.buildingsByClass['FakeBuilding'][0]).toBe(fakeBuildingInstance2);
    });

    it('removes the given building from buildings property', function () {
      var square = new Square(0, 1);
      function FakeBuilding(id) { this.toString = function () { return id; }; }
      var fakeBuildingInstance = new FakeBuilding('fake-building-1');
      var fakeBuildingInstance2 = new FakeBuilding('fake-building-2');
      square.addBuilding(fakeBuildingInstance);
      square.addBuilding(fakeBuildingInstance2);
      expect(square.buildings.length).toBe(2);

      square.removeBuilding(fakeBuildingInstance2);
      expect(square.buildings.length).toBe(1);
      expect(square.buildings[0]).not.toBe(fakeBuildingInstance2);
      expect(square.buildings[0]).toBe(fakeBuildingInstance);
    });

  });

  describe('getBuildingCountByClass', function () {

    it('returns number of building for the given class name', function () {
      var square = new Square(0, 1);
      function FakeBuilding(id) { this.toString = function () { return id; }; }
      var fakeBuildingInstance = new FakeBuilding('fake-building-1');
      var fakeBuildingInstance2 = new FakeBuilding('fake-building-2');
      square.addBuilding(fakeBuildingInstance);
      square.addBuilding(fakeBuildingInstance2);
      expect(square.buildingsByClass['FakeBuilding'].length).toBe(2);

      var result = square.getBuildingCountByClass('FakeBuilding');
      expect(result).toBe(2);
    });

    it('returns 0 if there are no buildings for a given class name', function () {
      var square = new Square(0, 1);
      function FakeBuilding(id) { this.toString = function () { return id; }; }
      var fakeBuildingInstance = new FakeBuilding('fake-building-1');
      var fakeBuildingInstance2 = new FakeBuilding('fake-building-2');
      square.addBuilding(fakeBuildingInstance);
      square.addBuilding(fakeBuildingInstance2);
      expect(square.buildingsByClass['FakeBuilding'].length).toBe(2);

      var result = square.getBuildingCountByClass('AnotherFakeBuilding');
      expect(result).toBe(0);
    });

    it('throws Error if given buildingClassName is falsy', function () {
      var square = new Square(0, 1);
      expect(function () { square.getBuildingCountByClass(); })
        .toThrow(new Error('Invalid buildingClassName given: undefined'));
      expect(function () { square.getBuildingCountByClass(''); })
        .toThrow(new Error('Invalid buildingClassName given: '));
      expect(function () { square.getBuildingCountByClass(false); })
        .toThrow(new Error('Invalid buildingClassName given: false'));
      expect(function () { square.getBuildingCountByClass(null); })
        .toThrow(new Error('Invalid buildingClassName given: null'));
      expect(function () { square.getBuildingCountByClass(0); })
        .toThrow(new Error('Invalid buildingClassName given: 0'));
    });

  });

  describe('calcGoldIncomeInternal', function () {

    var goldIncomeInternalSpy;

    function prepareSquare(squareLevel) {
      var square = new Square(0, 1, 1, 0, [], squareLevel);
      goldIncomeInternalSpy = jasmine.createSpy('goldIncomeInternal');
      square.goldIncomeInternal = goldIncomeInternalSpy;
      return square;
    }

    describe('sets goldIncomeInternal according to goldIncomePerLevel', function () {

      it('for the square of level 2', function () {
        var square = prepareSquare(2);
        square.calcGoldIncomeInternal();
        expect(square.goldIncomeInternal).toBe(Square.goldIncomePerLevel[2]);
      });

      it('for the square of level 3', function () {
        var square = prepareSquare(3);
        square.calcGoldIncomeInternal();
        expect(square.goldIncomeInternal).toBe(Square.goldIncomePerLevel[3]);
      });

      it('for the square of level 4', function () {
        var square = prepareSquare(4);
        square.calcGoldIncomeInternal();
        expect(square.goldIncomeInternal).toBe(Square.goldIncomePerLevel[4]);
      });

      it('for the square of level 5', function () {
        var square = prepareSquare(5);
        square.calcGoldIncomeInternal();
        expect(square.goldIncomeInternal).toBe(Square.goldIncomePerLevel[5]);
      });

    });

  });

  describe('_calcGoldIncomeBlocks', function () {

    var square;

    function createBlock(goldGain, level, rich, disabled) {
      var fakeKind = jasmine.createSpyObj('kind', ['calcGoldGain']);
      fakeKind.calcGoldGain.and.returnValue(goldGain);
      return {
        kind: fakeKind,
        level: level || 1,
        rich: rich || false,
        disabled: disabled || false
      };
    }

    beforeEach(function () {
      square = new Square(1, -1, 0, 0, [], 5);
      square.goldIncomeBlocks = jasmine.createSpy('goldIncomeBlocks');
    });

    describe('sets goldIncomeBlocks according to square blocks', function () {

      it('for 2 gold-producing blocks', function () {
        square.blocks = [createBlock(5), createBlock(18)];
        square._calcGoldIncomeBlocks();
        expect(square.goldIncomeBlocks).toBe(5 + 18);
      });

      it('for 3 gold-producing blocks, but one disabled', function () {
        square.blocks = [createBlock(19), createBlock(1),
                         createBlock(99, 1, false, true)];
        square._calcGoldIncomeBlocks();
        expect(square.goldIncomeBlocks).toBe(19 + 1);
      });

      it('for 2 gold-producing blocks with different level and richness', function () {
        var fakeLevel_block1 = jasmine.createSpy('fakeLevel_block1');
        var fakeLevel_block2 = jasmine.createSpy('fakeLevel_block2');
        var fakeRich_block1 = jasmine.createSpy('fakeRich_block1');
        var fakeRich_block2 = jasmine.createSpy('fakeRich_block2');
        square.blocks = [createBlock(4, fakeLevel_block1, fakeRich_block1),
                         createBlock(8, fakeLevel_block2, fakeRich_block2)];
        square._calcGoldIncomeBlocks();
        expect(square.blocks[0].kind.calcGoldGain).toHaveBeenCalledWith(fakeLevel_block1, fakeRich_block1);
        expect(square.blocks[1].kind.calcGoldGain).toHaveBeenCalledWith(fakeLevel_block2, fakeRich_block2);
        expect(square.goldIncomeBlocks).toBe(12);
      });

      it('for 5 blocks including 3 gold-producing blocks', function () {
        square.blocks = [createBlock(2), createBlock(0), createBlock(55),
                         createBlock(7), createBlock(0)];
        square._calcGoldIncomeBlocks();
        expect(square.goldIncomeBlocks).toBe(2 + 55 + 7);
      });

      it('for 7 blocks including none gold-producing blocks', function () {
        square.blocks = [createBlock(0), createBlock(0), createBlock(0), createBlock(0),
                         createBlock(0), createBlock(0), createBlock(0)];
        square._calcGoldIncomeBlocks();
        expect(square.goldIncomeBlocks).toBe(0);
      });

    });

  });

  describe('_calcGoldIncomeBuildings', function () {

    var square;

    beforeEach(function () {
      square = new Square(0, 1, 1, 0, []);
      square.goldIncomeBuildings = jasmine.createSpy('goldIncomeBuildings');
    });

    it('sets goldIncomeBuildings to 0 if there are no buildings in the square', function () {
      square._calcGoldIncomeBuildings();
      expect(square.goldIncomeBuildings).toBe(0);
    });

    describe('sets goldIncomeBuildings to sum of incomes from all buildings in the square', function () {

      it('for the list of buildings containing 1 building', function () {
        var market = new Market({}, square);
        market.goldIncome = 12;
        square.addBuilding(market);
        square._calcGoldIncomeBuildings();
        expect(square.goldIncomeBuildings).toBe(12);
      });

      it('for the list of buildings containing 5 the same class buildings', function () {
        var markets = [new Market({}, square), new Market({}, square),
                       new Market({}, square), new Market({}, square),
                       new Market({}, square)];
        markets.forEach(function (market) {
          market.goldIncome = 7;
          square.addBuilding(market);
        });
        square._calcGoldIncomeBuildings();
        expect(square.goldIncomeBuildings).toBe(5 * 7);
      });

      it('for the list of buildings containing 2 different buildings', function () {
        var market = new Market({}, square);
        market.goldIncome = 33;
        square.addBuilding(market);
        var tavern = new Tavern({}, square);
        tavern.goldIncome = 9;
        square.addBuilding(tavern);
        square._calcGoldIncomeBuildings();
        expect(square.goldIncomeBuildings).toBe(33 + 9);
      });

    });

  });

  describe('_calcGoldUpkeepBuildings', function () {

    var square;

    beforeEach(function () {
      square = new Square(0, 2, 1, 0, []);
      square.goldUpkeepBuildings = jasmine.createSpy('goldUpkeepBuildings');
    });

    it('sets goldUpkeepBuildings to 0 if there are no buildings in the square', function () {
      square._calcGoldUpkeepBuildings();
      expect(square.goldUpkeepBuildings).toBe(0);
    });

    describe('sets goldUpkeepBuildings to sum of incomes from all buildings in the square', function () {

      it('for the list of buildings containing 1 building', function () {
        var market = new Market({}, square);
        market.goldUpkeep = 2;
        square.addBuilding(market);
        square._calcGoldUpkeepBuildings();
        expect(square.goldUpkeepBuildings).toBe(2);
      });

      it('for the list of buildings containing 6 the same class buildings', function () {
        var markets = [new Market({}, square), new Market({}, square),
                       new Market({}, square), new Market({}, square),
                       new Market({}, square), new Market({}, square)];
        markets.forEach(function (market) {
          market.goldUpkeep = 3;
          square.addBuilding(market);
        });
        square._calcGoldUpkeepBuildings();
        expect(square.goldUpkeepBuildings).toBe(6 * 3);
      });

      it('for the list of buildings containing 2 different buildings', function () {
        var market = new Market({}, square);
        market.goldUpkeep = 7;
        square.addBuilding(market);
        var tavern = new Tavern({}, square);
        tavern.goldUpkeep = 11;
        square.addBuilding(tavern);
        square._calcGoldUpkeepBuildings();
        expect(square.goldUpkeepBuildings).toBe(7 + 11);
      });

    });

  });

  describe('calcGoldOutcomeTotal', function () {

    var square;

    beforeEach(function () {
      square = new Square(0, 2, 1, 0, []);
      spyOn(square, 'calcGoldIncomeInternal');
      spyOn(square, '_calcGoldIncomeBlocks');
      spyOn(square, '_calcGoldIncomeBuildings');
      spyOn(square, '_calcGoldUpkeepBuildings');
    });

    it('calls calcGoldIncomeInternal', function () {
      square.calcGoldOutcomeTotal();
      expect(square.calcGoldIncomeInternal).toHaveBeenCalled();
    });

    it('calls calcGoldIncomeInternal', function () {
      square.calcGoldOutcomeTotal();
      expect(square._calcGoldIncomeBlocks).toHaveBeenCalled();
    });

    it('calls _calcGoldIncomeBuildings', function () {
      square.calcGoldOutcomeTotal();
      expect(square._calcGoldIncomeBuildings).toHaveBeenCalled();
    });

    it('calls _calcGoldUpkeepBuildings', function () {
      square.calcGoldOutcomeTotal();
      expect(square._calcGoldUpkeepBuildings).toHaveBeenCalled();
    });

    it('sets goldOutcomeTotal based on goldIncomeInternal, goldIncomeBlocks, ' +
       'goldIncomeBuildings and goldUpkeepBuildings', function () {
      square.goldIncomeInternal = 500;
      square.goldIncomeBlocks = 2;
      square.goldIncomeBuildings = 21;
      square.goldUpkeepBuildings = 99;
      square.goldOutcomeTotal = jasmine.createSpy('goldOutcomeTotal');
      square.calcGoldOutcomeTotal();
      expect(square.goldOutcomeTotal).toBe(500 + 2 + 21 - 99);
    });

  });

  describe('_calcBlockResourceProductionAndUpkeep', function () {

    function createBlock(KindClass, rich, level, disabled) {
      var block = new Block(0, 0, jasmine.createSpy('region'), new KindClass(), rich || false);
      block.level = level || 1;
      block.disabled = disabled || false;
      return block;
    }

    it('sets blockResourceProduction to {} if none block produces any resource', function () {
      var square = new Square(0, 0, 1, 0, [createBlock(Water), createBlock(Water)]);
      square.blockResourceProduction = jasmine.createSpy('blockResourceProduction');
      square._calcBlockResourceProductionAndUpkeep();
      expect(square.blockResourceProduction).toEqual({});
    });

    it('sets blockResourceUpkeep to {} if none block requires any resource', function () {
      var square = new Square(0, 0, 1, 0, [createBlock(Water), createBlock(Water)]);
      square.blockResourceUpkeep = jasmine.createSpy('blockResourceUpkeep');
      square._calcBlockResourceProductionAndUpkeep();
      expect(square.blockResourceUpkeep).toEqual({});
    });

    it('sets blockResourceUpkeep to {} if one block requires a resource but it is disabled', function () {
      var square = new Square(0, 0, 1, 0,
        [createBlock(Mountain, false, 1, true), createBlock(Water)]);
      square.blockResourceUpkeep = jasmine.createSpy('blockResourceUpkeep');
      square._calcBlockResourceProductionAndUpkeep();
      expect(square.blockResourceUpkeep).toEqual({});
    });

    it('sets blockResourceProduction to {metal: 1} if square contains only one mountain block', function () {
      var square = new Square(0, 0, 1, 0, [createBlock(Mountain)]);
      square.blockResourceProduction = jasmine.createSpy('blockResourceProduction');
      square._calcBlockResourceProductionAndUpkeep();
      expect(square.blockResourceProduction).toEqual({metal: 1});
    });

    it('sets blockResourceUpkeep to {stone: 1} if square contains only one forest block', function () {
      var square = new Square(0, 0, 1, 0, [createBlock(Forest)]);
      square.blockResourceUpkeep = jasmine.createSpy('blockResourceUpkeep');
      square._calcBlockResourceProductionAndUpkeep();
      expect(square.blockResourceUpkeep).toEqual({stone: 1});
    });

    it('sets blockResourceProduction to {metal: 2} if square contains only one rich mountain block', function () {
      var square = new Square(0, 0, 1, 0, [createBlock(Mountain, true)]);
      square.blockResourceProduction = jasmine.createSpy('blockResourceProduction');
      square._calcBlockResourceProductionAndUpkeep();
      expect(square.blockResourceProduction).toEqual({metal: 2});
    });

    it('sets blockResourceUpkeep to {food: 1} if square contains only one rich mountain block', function () {
      var square = new Square(0, 0, 1, 0, [createBlock(Mountain, true)]);
      square.blockResourceUpkeep = jasmine.createSpy('blockResourceUpkeep');
      square._calcBlockResourceProductionAndUpkeep();
      expect(square.blockResourceUpkeep).toEqual({food: 1});
    });

    it('sets blockResourceProduction to {metal: 1, wood: 1} ' +
       'if square contains one mountain and one forest block', function () {
      var square = new Square(0, 0, 1, 0, [createBlock(Mountain), createBlock(Forest)]);
      square.blockResourceProduction = jasmine.createSpy('blockResourceProduction');
      square._calcBlockResourceProductionAndUpkeep();
      expect(square.blockResourceProduction).toEqual({metal: 1, wood: 1});
    });

    it('sets blockResourceProduction to {metal: 1} ' +
       'if square contains one mountain and one disabled forest block', function () {
      var square = new Square(0, 0, 1, 0, [
        createBlock(Mountain), createBlock(Forest, false, 1, true)]);
      square.blockResourceProduction = jasmine.createSpy('blockResourceProduction');
      square._calcBlockResourceProductionAndUpkeep();
      expect(square.blockResourceProduction).toEqual({metal: 1});
    });

    it('sets blockResourceUpkeep to {wood: 1, food: 1} ' +
       'if square contains one mountain and one plain block', function () {
      var square = new Square(0, 0, 1, 0, [createBlock(Mountain), createBlock(Plain)]);
      square.blockResourceUpkeep = jasmine.createSpy('blockResourceUpkeep');
      square._calcBlockResourceProductionAndUpkeep();
      expect(square.blockResourceUpkeep).toEqual({wood: 1, food: 1});
    });

    it('sets blockResourceProduction to {metal: 3, food: 1} ' +
       'if square contains one plain and three mountain blocks', function () {
      var square = new Square(0, 0, 1, 0, [createBlock(Mountain), createBlock(Mountain),
                                           createBlock(Plain), createBlock(Mountain)]);
      square.blockResourceProduction = jasmine.createSpy('blockResourceProduction');
      square._calcBlockResourceProductionAndUpkeep();
      expect(square.blockResourceProduction).toEqual({metal: 3, food: 1});
    });

    it('sets blockResourceUpkeep to {metal: 2, wood: 1, stone: 1} ' +
       'if square contains one forest, one plain and two hill blocks', function () {
      var square = new Square(0, 0, 1, 0, [createBlock(Forest), createBlock(Plain),
                                           createBlock(Hill), createBlock(Hill)]);
      square.blockResourceUpkeep = jasmine.createSpy('blockResourceUpkeep');
      square._calcBlockResourceProductionAndUpkeep();
      expect(square.blockResourceUpkeep).toEqual({metal: 2, wood: 1, stone: 1});
    });

    it('sets blockResourceProduction to {metal: 4, food: 1} ' +
       'if square contains one plain and three mountain blocks' +
       'and one of them is level: 2', function () {
      var square = new Square(0, 0, 1, 0, [createBlock(Mountain), createBlock(Mountain, false, 2),
                                           createBlock(Plain), createBlock(Mountain)]);
      square.blockResourceProduction = jasmine.createSpy('blockResourceProduction');
      square._calcBlockResourceProductionAndUpkeep();
      expect(square.blockResourceProduction).toEqual({metal: 4, food: 1});
    });

    it('sets blockResourceUpkeep to {food: 3, wood: 1} ' +
       'if square contains one plain and three mountain blocks' +
       'and two of them are level: 2', function () {
      var square = new Square(0, 0, 1, 0, [createBlock(Mountain), createBlock(Mountain, false, 2),
                                           createBlock(Plain), createBlock(Mountain, false, 2)]);
      square.blockResourceUpkeep = jasmine.createSpy('blockResourceUpkeep');
      square._calcBlockResourceProductionAndUpkeep();
      expect(square.blockResourceUpkeep).toEqual({food: 3, wood: 1});
    });

    it('sets blockResourceUpkeep to {food: 4} ' +
       'if square contains one coalfield and three mountain blocks', function () {
      var square = new Square(0, 0, 1, 0, [createBlock(Mountain), createBlock(Mountain),
                                           createBlock(Coalfield), createBlock(Mountain)]);
      square.blockResourceUpkeep = jasmine.createSpy('blockResourceUpkeep');
      square._calcBlockResourceProductionAndUpkeep();
      expect(square.blockResourceUpkeep).toEqual({food: 4});
    });

    it('sets blockResourceUpkeep to {food: 3} ' +
       'if square contains one wild horses and two mountain blocks', function () {
      var square = new Square(0, 0, 1, 0, [createBlock(WildHorses),
                                           createBlock(Mountain),
                                           createBlock(Mountain)]);
      square.blockResourceUpkeep = jasmine.createSpy('blockResourceUpkeep');
      square._calcBlockResourceProductionAndUpkeep();
      expect(square.blockResourceUpkeep).toEqual({food: 3});
    });

    it('sets blockResourceUpkeep to {food: 1} ' +
       'if square contains one gold deposit block', function () {
      var square = new Square(0, 0, 1, 0, [createBlock(GoldDeposit)]);
      square.blockResourceUpkeep = jasmine.createSpy('blockResourceUpkeep');
      square._calcBlockResourceProductionAndUpkeep();
      expect(square.blockResourceUpkeep).toEqual({food: 1});
    });

    it('sets blockResourceUpkeep to {food: 2} ' +
       'if square contains three silver deposit blocks, ' +
       'but one of them is disabled', function () {
      var square = new Square(0, 0, 1, 0, [createBlock(SilverDeposit, false, 1, true),
                                           createBlock(SilverDeposit),
                                           createBlock(SilverDeposit)]);
      square.blockResourceUpkeep = jasmine.createSpy('blockResourceUpkeep');
      square._calcBlockResourceProductionAndUpkeep();
      expect(square.blockResourceUpkeep).toEqual({food: 2});
    });

    it('sets blockResourceUpkeep to {food: 1, wood: 1} ' +
       'if square contains one gemstone deposit and one plain block', function () {
      var square = new Square(0, 0, 1, 0, [createBlock(GemstoneDeposit),
                                           createBlock(Plain)]);
      square.blockResourceUpkeep = jasmine.createSpy('blockResourceUpkeep');
      square._calcBlockResourceProductionAndUpkeep();
      expect(square.blockResourceUpkeep).toEqual({food: 1, wood: 1});
    });

  });

  describe('_calcBlockResourceOutcome', function () {

    var square;

    beforeEach(function () {
      square = new Square(0, 0, 1, 0, []);
      spyOn(square, '_calcBlockResourceProductionAndUpkeep');
      square.blockResourceOutcome = jasmine.createSpy('blockResourceOutcome');
    });

    it('calls _calcBlockResourceProductionAndUpkeep', function () {
      square._calcBlockResourceOutcome();
      expect(square._calcBlockResourceProductionAndUpkeep).toHaveBeenCalledWith();
    });

    it('sets blockResourceOutcome to {} ' +
       'if square blockResourceProduction is none and blockResourceUpkeep is none', function () {
      square.blockResourceProduction = {};
      square.blockResourceUpkeep = {};
      square._calcBlockResourceOutcome();
      expect(square.blockResourceOutcome).toEqual({});
    });

    it('sets blockResourceOutcome to {} ' +
       'if square blockResourceProduction is equal to blockResourceUpkeep', function () {
      square.blockResourceProduction = {stone: 1};
      square.blockResourceUpkeep = {stone: 1};
      square._calcBlockResourceOutcome();
      expect(square.blockResourceOutcome).toEqual({});

    });

    it('sets blockResourceOutcome to {wood: 1} ' +
       'if square blockResourceProduction is equal {wood: 1}', function () {
      square.blockResourceProduction = {wood: 1};
      square.blockResourceUpkeep = {};
      square._calcBlockResourceOutcome();
      expect(square.blockResourceOutcome).toEqual({wood: 1});

    });

    it('sets blockResourceOutcome to {wood: -1} ' +
       'if square blockResourceUpkeep is equal {wood: 1}', function () {
      square.blockResourceProduction = {};
      square.blockResourceUpkeep = {wood: 1};
      square._calcBlockResourceOutcome();
      expect(square.blockResourceOutcome).toEqual({wood: -1});

    });

    it('sets blockResourceOutcome to {food: 1, stone: -1} ' +
       'if square blockResourceProduction is equal {food: 1} ' +
       'and blockResourceUpkeep is equal {stone: 1}', function () {
      square.blockResourceProduction = {food: 1};
      square.blockResourceUpkeep = {stone: 1};
      square._calcBlockResourceOutcome();
      expect(square.blockResourceOutcome).toEqual({food: 1, stone: -1});
    });

    it('sets blockResourceOutcome to {metal: 1} ' +
       'if square blockResourceProduction is equal {metal: 2} ' +
       'and blockResourceUpkeep is equal {metal: 1}', function () {
      square.blockResourceProduction = {metal: 2};
      square.blockResourceUpkeep = {metal: 1};
      square._calcBlockResourceOutcome();
      expect(square.blockResourceOutcome).toEqual({metal: 1});
    });

    it('sets blockResourceOutcome to {stone: -1} ' +
       'if square blockResourceProduction is equal {stone: 3} ' +
       'and blockResourceUpkeep is equal {stone: 4}', function () {
      square.blockResourceProduction = {stone: 3};
      square.blockResourceUpkeep = {stone: 4};
      square._calcBlockResourceOutcome();
      expect(square.blockResourceOutcome).toEqual({stone: -1});
    });

    it('sets blockResourceOutcome to {wood: 1} ' +
       'if square blockResourceProduction is equal {wood: 1, food: 2} ' +
       'and blockResourceUpkeep is equal {food: 2}', function () {
      square.blockResourceProduction = {wood: 1, food: 2};
      square.blockResourceUpkeep = {food: 2};
      square._calcBlockResourceOutcome();
      expect(square.blockResourceOutcome).toEqual({wood: 1});
    });

  });

  describe('_calcBuildingResourceProductionAndUpkeep', function () {

    var square;

    function addBuilding(BuildingClass, resourceProduction, resourceUpkeep) {
      var building = new BuildingClass({}, square);
      building.resourceProduction = resourceProduction;
      building.resourceUpkeep = resourceUpkeep;
      square.addBuilding(building);
    }

    beforeEach(function () {
      square = new Square(0, 0, 1, 0, []);
      square.buildingResourceProduction = jasmine.createSpy('buildingResourceProduction');
      square.buildingResourceUpkeep = jasmine.createSpy('buildingResourceUpkeep');
    });

    it('sets buildingResourceProduction to {} if there are no buildings in the square', function () {
      expect(square.buildings).toEqual([]);
      square._calcBuildingResourceProductionAndUpkeep();
      expect(square.buildingResourceProduction).toEqual({});
    });

    it('sets buildingResourceUpkeep to {} if there are no buildings in the square', function () {
      expect(square.buildings).toEqual([]);
      square._calcBuildingResourceProductionAndUpkeep();
      expect(square.buildingResourceUpkeep).toEqual({});
    });

    it('sets buildingResourceProduction to {} if none building produces any resource in the square', function () {
      addBuilding(Market, {}, {});
      square._calcBuildingResourceProductionAndUpkeep();
      expect(square.buildingResourceProduction).toEqual({});
    });

    it('sets buildingResourceUpkeep to {} if none building requires any resources in the square', function () {
      addBuilding(Market, {}, {});
      square._calcBuildingResourceProductionAndUpkeep();
      expect(square.buildingResourceUpkeep).toEqual({});
    });

    it('sets buildingResourceProduction to {wood: 1} if there is one building producing 1 wood', function () {
      addBuilding(Market, {wood: 1}, {});
      square._calcBuildingResourceProductionAndUpkeep();
      expect(square.buildingResourceProduction).toEqual({wood: 1});
    });

    it('sets buildingResourceUpkeep to {stone: 1} if there is one building requiring 1 stone', function () {
      addBuilding(Market, {}, {stone: 1});
      square._calcBuildingResourceProductionAndUpkeep();
      expect(square.buildingResourceUpkeep).toEqual({stone: 1});
    });

    it('sets buildingResourceProduction to {metal: 5} if there are five buildings producing 1 metal', function () {
      for (var i = 0; i < 5; i++) {
        addBuilding(Market, {metal: 1}, {});
      }
      square._calcBuildingResourceProductionAndUpkeep();
      expect(square.buildingResourceProduction).toEqual({metal: 5});
    });

    it('sets buildingResourceUpkeep to {wood: 4} if there are four buildings requiring 1 wood', function () {
      for (var i = 0; i < 4; i++) {
        addBuilding(Market, {}, {wood: 1});
      }
      square._calcBuildingResourceProductionAndUpkeep();
      expect(square.buildingResourceUpkeep).toEqual({wood: 4});
    });

    it('sets buildingResourceProduction to {metal: 1, stone: 2} ' +
       'if there are two buildings producing 1 stone and one building producing 1 metal', function () {
      addBuilding(Market, {stone: 1}, {});
      addBuilding(Market, {stone: 1}, {});
      addBuilding(Market, {metal: 1}, {});
      square._calcBuildingResourceProductionAndUpkeep();
      expect(square.buildingResourceProduction).toEqual({metal: 1, stone: 2});
    });

    it('sets buildingResourceUpkeep to {wood: 2, food: 1} ' +
       'if there are two buildings requiring 2 wood and one building requiring 1 food', function () {
      addBuilding(Market, {}, {wood: 1});
      addBuilding(Market, {}, {wood: 1});
      addBuilding(Market, {}, {food: 1});
      square._calcBuildingResourceProductionAndUpkeep();
      expect(square.buildingResourceUpkeep).toEqual({wood: 2, food: 1});
    });

    it('sets buildingResourceProduction to {wood: 1, stone: 2} ' +
       'if there is one building producing 2 stone and one building producing 1 wood', function () {
      addBuilding(Market, {stone: 2}, {});
      addBuilding(Market, {wood: 1}, {});
      square._calcBuildingResourceProductionAndUpkeep();
      expect(square.buildingResourceProduction).toEqual({wood: 1, stone: 2});
    });

    it('sets buildingResourceUpkeep to {metal: 4, food: 5} ' +
       'if there is one building requiring 4 metal and one building requiring 5 food', function () {
      addBuilding(Market, {}, {metal: 4});
      addBuilding(Market, {}, {food: 5});
      square._calcBuildingResourceProductionAndUpkeep();
      expect(square.buildingResourceUpkeep).toEqual({metal: 4, food: 5});
    });

    it('sets buildingResourceProduction to {wood: 3, stone: 1} ' +
       'even if buildings require 99 wood and 1000 stone', function () {
      addBuilding(Market, {wood: 3}, {wood: 99});
      addBuilding(Market, {stone: 1}, {stone: 1000});
      square._calcBuildingResourceProductionAndUpkeep();
      expect(square.buildingResourceProduction).toEqual({wood: 3, stone: 1});
    });

    it('sets buildingResourceUpkeep to {food: 5, stone: 2} ' +
       'even if buildings produce 5 food and 991 stone', function () {
      addBuilding(Market, {stone: 991}, {food: 5});
      addBuilding(Market, {food: 5}, {stone: 2});
      square._calcBuildingResourceProductionAndUpkeep();
      expect(square.buildingResourceUpkeep).toEqual({food: 5, stone: 2});
    });

  });

  describe('_calcBuildingResourceOutcome', function () {

    var square;

    beforeEach(function () {
      square = new Square(1, 0, 0, 1, []);
      spyOn(square, '_calcBuildingResourceProductionAndUpkeep');
    });

    it('calls _calcBuildingResourceProductionAndUpkeep', function () {
      square._calcBuildingResourceOutcome();
      expect(square._calcBuildingResourceProductionAndUpkeep).toHaveBeenCalledWith();
    });

    it('sets buildingResourceOutcome to {} if square buildingResourceProduction is none ' +
       'and buildingResourceUpkeep is none', function () {
      square.buildingResourceProduction = {};
      square.buildingResourceUpkeep = {};
      square._calcBuildingResourceOutcome();
      expect(square.buildingResourceOutcome).toEqual({});
    });

    it('sets buildingResourceOutcome to {} ' +
       'if square buildingResourceProduction is equal to buildingResourceUpkeep', function () {
      square.buildingResourceProduction = {metal: 2, food: 3};
      square.buildingResourceUpkeep = {metal: 2, food: 3};
      square._calcBuildingResourceOutcome();
      expect(square.buildingResourceOutcome).toEqual({});
    });

    it('sets buildingResourceOutcome to {metal: 1} ' +
       'if square buildingResourceProduction is {metal: 1} and buildingResourceUpkeep is none', function () {
      square.buildingResourceProduction = {metal: 1};
      square.buildingResourceUpkeep = {};
      square._calcBuildingResourceOutcome();
      expect(square.buildingResourceOutcome).toEqual({metal: 1});
    });

    it('sets buildingResourceOutcome to {wood: -2} ' +
       'if square buildingResourceProduction is none and buildingResourceUpkeep is {wood: 2}', function () {
      square.buildingResourceProduction = {};
      square.buildingResourceUpkeep = {wood: 2};
      square._calcBuildingResourceOutcome();
      expect(square.buildingResourceOutcome).toEqual({wood: -2});
    });

    it('sets buildingResourceOutcome to {food: 1, metal: 2} ' +
       'if square buildingResourceProduction is {food: 1, metal: 2} and buildingResourceUpkeep is none', function () {
      square.buildingResourceProduction = {food: 1, metal: 2};
      square.buildingResourceUpkeep = {};
      square._calcBuildingResourceOutcome();
      expect(square.buildingResourceOutcome).toEqual({food: 1, metal: 2});
    });

    it('sets buildingResourceOutcome to {stone: -1, metal: -2} ' +
       'if square buildingResourceProduction is none and buildingResourceUpkeep is {stone: 1, metal: 2}', function () {
      square.buildingResourceProduction = {};
      square.buildingResourceUpkeep = {stone: 1, metal: 2};
      square._calcBuildingResourceOutcome();
      expect(square.buildingResourceOutcome).toEqual({stone: -1, metal: -2});
    });

    it('sets buildingResourceOutcome to {stone: 1, wood: -1} ' +
       'if square buildingResourceProduction is {stone: 1} and buildingResourceUpkeep is {wood: 1}', function () {
      square.buildingResourceProduction = {stone: 1};
      square.buildingResourceUpkeep = {wood: 1};
      square._calcBuildingResourceOutcome();
      expect(square.buildingResourceOutcome).toEqual({stone: 1, wood: -1});
    });

    it('sets buildingResourceOutcome to {metal: 2, wood: -1} ' +
       'if square buildingResourceProduction is {metal: 3} and buildingResourceUpkeep is {metal: 1, wood: 1}', function () {
      square.buildingResourceProduction = {metal: 3};
      square.buildingResourceUpkeep = {metal: 1, wood: 1};
      square._calcBuildingResourceOutcome();
      expect(square.buildingResourceOutcome).toEqual({metal: 2, wood: -1});
    });

    it('sets buildingResourceOutcome to {food: -1} ' +
       'if square buildingResourceProduction is {stone: 3} and buildingResourceUpkeep is {stone: 3, food: 1}', function () {
      square.buildingResourceProduction = {stone: 3};
      square.buildingResourceUpkeep = {stone: 3, food: 1};
      square._calcBuildingResourceOutcome();
      expect(square.buildingResourceOutcome).toEqual({food: -1});
    });

  });

  describe('calcResourceOutcomeTotal', function () {

    var square;

    beforeEach(function () {
      square = new Square(1, 0, 0, 1, []);
      spyOn(square, '_calcBlockResourceOutcome');
      spyOn(square, '_calcBuildingResourceOutcome');
    });

    it('calls _calcBlockResourceOutcome', function () {
      square.calcResourceOutcomeTotal();
      expect(square._calcBlockResourceOutcome).toHaveBeenCalledWith();
    });

    it('calls _calcBuildingResourceOutcome', function () {
      square.calcResourceOutcomeTotal();
      expect(square._calcBuildingResourceOutcome).toHaveBeenCalledWith();
    });

    it('sets resourceOutcomeTotal to {} if square blockResourceOutcome is none ' +
       'and buildingResourceOutcome is none', function () {
      square.blockResourceOutcome = {};
      square.buildingResourceOutcome = {};
      square.calcResourceOutcomeTotal();
      expect(square.resourceOutcomeTotal).toEqual({});
    });

    it('sets resourceOutcomeTotal to {metal: 10} ' +
       'if square blockResourceOutcome is {metal: 10} and buildingResourceOutcome is none', function () {
      square.blockResourceOutcome = {metal: 10};
      square.buildingResourceOutcome = {};
      square.calcResourceOutcomeTotal();
      expect(square.resourceOutcomeTotal).toEqual({metal: 10});
    });

    it('sets resourceOutcomeTotal to {wood: 2, food: 1} ' +
       'if square blockResourceOutcome is none ' +
       'and buildingResourceOutcome is {wood: 2, food: 1}', function () {
      square.blockResourceOutcome = {};
      square.buildingResourceOutcome = {wood: 2, food: 1};
      square.calcResourceOutcomeTotal();
      expect(square.resourceOutcomeTotal).toEqual({wood: 2, food: 1});
    });

    it('sets resourceOutcomeTotal to {wood: 2, food: 7, stone: 5} ' +
       'if square blockResourceOutcome is {food: 7, stone: 5} ' +
       'and buildingResourceOutcome is {wood: 2}', function () {
      square.blockResourceOutcome = {food: 7, stone: 5};
      square.buildingResourceOutcome = {wood: 2};
      square.calcResourceOutcomeTotal();
      expect(square.resourceOutcomeTotal).toEqual({wood: 2, food: 7, stone: 5});
    });

    it('sets resourceOutcomeTotal to {food: 11} ' +
       'if square blockResourceOutcome is {food: 5} ' +
       'and buildingResourceOutcome is {food: 6}', function () {
      square.blockResourceOutcome = {food: 5};
      square.buildingResourceOutcome = {food: 6};
      square.calcResourceOutcomeTotal();
      expect(square.resourceOutcomeTotal).toEqual({food: 11});

    });

    it('sets resourceOutcomeTotal to {food: 9, metal: 3, stone: 5} ' +
       'if square blockResourceOutcome is {food: 8, stone: 5} ' +
       'and buildingResourceOutcome is {food: 1, metal: 3}', function () {
      square.blockResourceOutcome = {food: 8, stone: 5};
      square.buildingResourceOutcome = {food: 1, metal: 3};
      square.calcResourceOutcomeTotal();
      expect(square.resourceOutcomeTotal).toEqual({food: 9, metal: 3, stone: 5});

    });

    it('sets resourceOutcomeTotal to {food: 9, metal: 3, stone: 5} ' +
       'if square blockResourceOutcome is {food: 8, stone: 5} ' +
       'and buildingResourceOutcome is {food: 1, metal: 3}', function () {
      square.blockResourceOutcome = {food: 8, stone: 5};
      square.buildingResourceOutcome = {food: 1, metal: 3};
      square.calcResourceOutcomeTotal();
      expect(square.resourceOutcomeTotal).toEqual({food: 9, metal: 3, stone: 5});

    });

    it('sets resourceOutcomeTotal to {metal: 5, stone: 5} ' +
       'if square blockResourceOutcome is {metal: 3, stone: 1} ' +
       'and buildingResourceOutcome is {stone: 4, metal: 2}', function () {
      square.blockResourceOutcome = {metal: 3, stone: 1};
      square.buildingResourceOutcome = {stone: 4, metal: 2};
      square.calcResourceOutcomeTotal();
      expect(square.resourceOutcomeTotal).toEqual({metal: 5, stone: 5});

    });

  });

  describe('calcGoldAndResourceOutcomes', function () {

    var square;

    beforeEach(function () {
      square = new Square(1, 0, 0, 1, []);
      spyOn(square, 'calcGoldOutcomeTotal');
      spyOn(square, 'calcResourceOutcomeTotal');
    });

    it('calls calcGoldOutcomeTotal on the given square', function () {
      square.calcGoldAndResourceOutcomes();
      expect(square.calcGoldOutcomeTotal).toHaveBeenCalledWith();
    });

    it('calls calcResourceOutcomeTotal on the given square', function () {
      square.calcGoldAndResourceOutcomes();
      expect(square.calcResourceOutcomeTotal).toHaveBeenCalledWith();
    });

  });

});