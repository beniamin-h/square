'use strict';

describe('SquareSchema', function() {
  beforeEach(module('square'));

  var Square, SquareFactory, SquareSchema, mapProvider,
    HumanPlayer, Block, Region, Joi, storageProvider,
    Market, Hill, playerProvider;

  beforeEach(inject(function (_Square_, _SquareFactory_, _SquareSchema_, _mapProvider_,
                              _HumanPlayer_, _Block_, _Region_, _Joi_, _storageProvider_,
                              _Market_, _Hill_, _playerProvider_) {
    Square = _Square_;
    SquareFactory = _SquareFactory_;
    SquareSchema = _SquareSchema_;
    mapProvider = _mapProvider_;

    HumanPlayer = _HumanPlayer_;
    Block = _Block_;
    Region = _Region_;
    Joi = _Joi_;
    storageProvider = _storageProvider_;

    Market = _Market_;
    Hill = _Hill_;
    playerProvider = _playerProvider_;
  }));

  describe('a new instance', function () {

    it('validates against the square schema', function (done) {
      var player = new HumanPlayer('human-player-mocked-id', {x: 2, y: 1});
      var square = new Square(0, -1, 5, 2,
        [new Block(5, 2, new Region(0, -1), new Hill())],
        2, player);
      Joi.validate(square, SquareSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

    it('created by SquareFactory validates against the square schema', function (done) {
      playerProvider.addHuman();
      spyOn(mapProvider, 'normalizeCoords').and.returnValue({
          regionX: -2,
          regionY: 0,
          blockX: 1,
          blockY: 33
        });

      spyOn(mapProvider, 'getBlocks').and.returnValue([]);
      spyOn(mapProvider, 'getBlock').and.returnValue(new Block(0, 2, new Region(0, 1), new Hill()));
      var squareFactory = new SquareFactory();
      squareFactory.squaresMap = jasmine.createSpyObj('squaresMap', ['addSquare']);
      storageProvider.init(playerProvider.getAllPlayers());
      var square = squareFactory.saveSquare(
        squareFactory.createSquare(-2, 0, 1, 33, 5, playerProvider.getHuman()));
      Joi.validate(square, SquareSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

    it('with 2 buildings validates against the square schema', function (done) {
      var player = new HumanPlayer('human-player-mocked-id', {x: 2, y: 1});
      var block = new Block(5, 2, new Region(0, -1), new Hill());
      var square = new Square(0, -1, 5, 2, [block], 2, player);
      square.addBuilding(new Market(block, square));
      square.addBuilding(new Market(block, square));
      Joi.validate(square, SquareSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

  });

});