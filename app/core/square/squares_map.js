'use strict';

angular.module('square').factory('SquaresMap', ['mapDirections', 'ArrayUtils', 'mapProvider', 'Square',
    function (mapDirections, ArrayUtils, mapProvider, Square) {

  function SquaresMap(squareFactory) {
    this.squareFactory = squareFactory;
    this.squares = {};
  }

  SquaresMap.prototype.squareFactory = null;
  SquaresMap.prototype.squares = null;
  SquaresMap.prototype.maxSquareLevel = 5;

  SquaresMap.prototype.squareMakingDirectionsSets = [
    ['right', 'rightDown', 'down'],
    ['down', 'leftDown', 'left'],
    ['left', 'leftUp', 'up'],
    ['up', 'rightUp', 'right']
  ];

  SquaresMap.prototype.setPlayers = function (players) {
    Object.keys(players).forEach(function (playerId) {
      this.squares[playerId] = {};
      if (players[playerId].ai) {
        players[playerId].ai.setSquaresMap(this);
      }
    }, this);
  };

  SquaresMap.prototype.addSquare = function (square) {
    this.squares[square.player.playerId][square.level] = this.squares[square.player.playerId][square.level] || {};
    this.squares[square.player.playerId][square.level][square.regionY] =
      this.squares[square.player.playerId][square.level][square.regionY] || {};
    this.squares[square.player.playerId][square.level][square.regionY][square.regionX] =
      this.squares[square.player.playerId][square.level][square.regionY][square.regionX] || {};
    this.squares[square.player.playerId][square.level][square.regionY][square.regionX][square.y] =
      this.squares[square.player.playerId][square.level][square.regionY][square.regionX][square.y] || {};
    this.squares[square.player.playerId][square.level][square.regionY][square.regionX][square.y][square.x] = square;
  };

  SquaresMap.prototype.removeSquare = function (square) {
    delete this.squares[square.player.playerId][square.level][square.regionY][square.regionX][square.y][square.x];
  };

  SquaresMap.prototype.getSquare = function (player, level, regionX, regionY, blockX, blockY) {
    var squareRegions = this.squares[player.playerId][level];
    var coords = mapProvider.normalizeCoords(regionX, regionY, blockX, blockY);
    var square = squareRegions &&
      squareRegions[coords.regionY] &&
      squareRegions[coords.regionY][coords.regionX] &&
      squareRegions[coords.regionY][coords.regionX][coords.blockY] &&
      squareRegions[coords.regionY][coords.regionX][coords.blockY][coords.blockX];
    return square || null;
  };

  SquaresMap.prototype.checkUpperLevelSquare = function (square) {
    return square.level < this.maxSquareLevel ?
           this.checkSquares(square) :
           false;
  };

  SquaresMap.prototype._getDirectionsWithSquare = function (square) {
    var directionsWithSquare = new Set();
    var adjacentSquaresMap = {};
    mapDirections.directions8.forEach(function (direction) {
      var adjacentSquare = this.getSquare(
        square.player, square.level, square.regionX, square.regionY,
        square.x + square.getWidth() * mapDirections.directionToCoord[direction].x,
        square.y + square.getHeight() * mapDirections.directionToCoord[direction].y
      );
      if (adjacentSquare) {
        directionsWithSquare.add(direction);
        adjacentSquaresMap[direction] = adjacentSquare;
      }
    }, this);
    return {
      directions: directionsWithSquare,
      adjacentSquaresMap: adjacentSquaresMap
    };
  };

  SquaresMap.prototype._allDirectionsHaveSquare = function (directionsToCheck, directionsWithSquare) {
    var haveSquare = true;
    directionsToCheck.forEach(function (direction) {
      if (!directionsWithSquare.has(direction)) {
        haveSquare = false;
      }
    });
    return haveSquare;
  };

  SquaresMap.prototype._prepareSquaresForNewGreaterSquare = function (oldSquare, directions, adjacentSquaresMap) {
    directions.map(function (direction) {
      this.squareFactory.deleteSquare(adjacentSquaresMap[direction]);
    }, this);
    this.squareFactory.deleteSquare(oldSquare);
  };

  SquaresMap.prototype._getNewSquareCoordsShift = function (directionsSetIndex) {
    var directions = this.squareMakingDirectionsSets[directionsSetIndex];
    return directionsSetIndex > 0 ?
           mapDirections.directionToCoord[directions[3 - directionsSetIndex]] :
           mapDirections.noneDirectionCoord;
  };

  SquaresMap.prototype._createNewGreaterSquare = function (square, directionsSetIndex) {
    var squareCoordsShift = this._getNewSquareCoordsShift(directionsSetIndex);
    return this.squareFactory.createSquare(
      square.regionX, square.regionY,
      square.x + square.getWidth() * squareCoordsShift.x,
      square.y + square.getHeight() * squareCoordsShift.y,
      square.level + 1, square.player);
  };

  SquaresMap.prototype.checkSquares = function (square) {
    var directionsWithSquareObj = this._getDirectionsWithSquare(square);
    var directionsWithSquare = directionsWithSquareObj.directions;
    var adjacentSquaresMap = directionsWithSquareObj.adjacentSquaresMap;

    for (var i = 0; i < this.squareMakingDirectionsSets.length; i++) {
      var directions = this.squareMakingDirectionsSets[i];
      if (this._allDirectionsHaveSquare(directions, directionsWithSquare)) {
        this._prepareSquaresForNewGreaterSquare(square, directions, adjacentSquaresMap);
        var greaterSquare = this._createNewGreaterSquare(square, i);
        this.squareFactory.saveSquare(greaterSquare);
        return this.checkUpperLevelSquare(greaterSquare) || greaterSquare;
      }
    }
    return false;
  };

  SquaresMap.prototype._checkSquareMakingNeighbors = function (
      squareMakingDirections, neighborsByPosition, player) {
    var squareMakingNeighborsImproved = true;
    squareMakingDirections.forEach(function (direction) {
      var neighbor = neighborsByPosition[direction];
      if (!neighbor || neighbor.square || neighbor.level !== 1 || neighbor.ownedBy !== player) {
        squareMakingNeighborsImproved = false;
      }
    });
    return squareMakingNeighborsImproved;
  };

  SquaresMap.prototype._createNewSmallestSquare = function (selectedBlock, directionsSetIndex, player) {
    var squareCoordsShift = this._getNewSquareCoordsShift(directionsSetIndex);
    return this.squareFactory.createSquare(
      selectedBlock.region.x, selectedBlock.region.y,
      selectedBlock.x + squareCoordsShift.x,
      selectedBlock.y + squareCoordsShift.y,
      2, player);
  };

  SquaresMap.prototype.checkSmallestSquare = function (selectedBlock, player) {
    for (var i = 0; i < this.squareMakingDirectionsSets.length; i++) {
      if (this._checkSquareMakingNeighbors(
          this.squareMakingDirectionsSets[i], selectedBlock.neighborsByPosition, player)) {
        var newSquare = this._createNewSmallestSquare(selectedBlock, i, player);
        this.squareFactory.saveSquare(newSquare);
        newSquare = this.checkUpperLevelSquare(newSquare) || newSquare;
        selectedBlock.level = newSquare.level;
        newSquare.calcGoldAndResourceOutcomes();
        if (player.ai) {
          player.ai.aiSquaring.notifyOfNewSquare(newSquare);
        }
        return newSquare;
      }
    }
    return false;
  };

  SquaresMap.prototype.checkSmallestSquareWithAdditionalBlocks = function (selectedBlock, player, additionalBlocks) {
    var squareOtherBlocks = false;
    var right = selectedBlock.neighborsByPosition.right;
    var left = selectedBlock.neighborsByPosition.left;
    var up = selectedBlock.neighborsByPosition.up;
    var down = selectedBlock.neighborsByPosition.down;
    var rightUp = selectedBlock.neighborsByPosition.rightUp;
    var rightDown = selectedBlock.neighborsByPosition.rightDown;
    var leftUp = selectedBlock.neighborsByPosition.leftUp;
    var leftDown = selectedBlock.neighborsByPosition.leftDown;

    if (right && rightDown && down &&
        (additionalBlocks.indexOf(right) > -1 ||
          !right.square && right.level === 1 && right.ownedBy === player) &&
        (additionalBlocks.indexOf(rightDown) > -1 ||
          !rightDown.square && rightDown.level === 1 && rightDown.ownedBy === player) &&
        (additionalBlocks.indexOf(down) > -1 ||
          !down.square && down.level === 1 && down.ownedBy === player)) {
      squareOtherBlocks = [right, rightDown, down, -1];
    } else if (down && leftDown && left &&
        (additionalBlocks.indexOf(down) > -1 ||
          !down.square && down.level === 1 && down.ownedBy === player) &&
        (additionalBlocks.indexOf(leftDown) > -1 ||
          !leftDown.square && leftDown.level === 1 && leftDown.ownedBy === player) &&
        (additionalBlocks.indexOf(left) > -1 ||
          !left.square && left.level === 1 && left.ownedBy === player)) {
      squareOtherBlocks = [left, down, leftDown, 0];
    } else if (left && leftUp && up &&
        (additionalBlocks.indexOf(left) > -1 ||
          !left.square && left.level === 1 && left.ownedBy === player) &&
        (additionalBlocks.indexOf(leftUp) > -1 ||
          !leftUp.square && leftUp.level === 1 && leftUp.ownedBy === player) &&
        (additionalBlocks.indexOf(up) > -1 ||
          !up.square && up.level === 1 && up.ownedBy === player)) {
      squareOtherBlocks = [leftUp, up, left, 0];
    } else if (up && rightUp && right &&
        (additionalBlocks.indexOf(up) > -1 ||
          !up.square && up.level === 1 && up.ownedBy === player) &&
        (additionalBlocks.indexOf(rightUp) > -1 ||
          !rightUp.square && rightUp.level === 1 && rightUp.ownedBy === player) &&
        (additionalBlocks.indexOf(right) > -1 ||
          !right.square && right.level === 1 && right.ownedBy === player)) {
      squareOtherBlocks = [up, rightUp, right, 0];
    }
    return squareOtherBlocks;
  };

  SquaresMap.prototype.removeBlockFromSquare = function (block) {
    var square = block.square;
    this.squareFactory.deleteSquare(block.square);
    this._deconstructSquare(square, block);
  };

  SquaresMap.prototype._deconstructSquare = function (square, subtractedBlock) {
    if (square.level === 2) {
      this._deconstructSmallestSquare(square, subtractedBlock);
      return;
    }
    var smallerSquareToDeconstruct;
    for (let smallerSquare of this._splitSquare(square)) {
      if (smallerSquare.blocks.indexOf(subtractedBlock) === -1) {
        this._saveSmallerSquare(smallerSquare);
      } else {
        smallerSquareToDeconstruct = smallerSquare;
      }
    }
    if (smallerSquareToDeconstruct) {
      this._deconstructSquare(smallerSquareToDeconstruct, subtractedBlock);
    }
  };

  SquaresMap.prototype._deconstructSmallestSquare = function (square, subtractedBlock) {
    for (const block of square.blocks) {
      if (block !== subtractedBlock) {
        mapProvider.getMap().changeOwnedBlockLevel(block, 1);
      }
    }
  };

  SquaresMap.prototype._splitSquare = function* (square) {
    var directionsCoords = [mapDirections.noneDirectionCoord,
                            mapDirections.directionToCoord.right,
                            mapDirections.directionToCoord.down,
                            mapDirections.directionToCoord.rightDown];
    for (var coord of directionsCoords) {
      yield this.squareFactory.createSquare(
        square.regionX, square.regionY,
        square.x + Square.getWidth(square.level - 1) * coord.x,
        square.y + Square.getHeight(square.level - 1) * coord.y,
        square.level - 1, square.player);
    }
  };

  SquaresMap.prototype._saveSmallerSquare = function (square) {
    this.squareFactory.saveSquare(square);
    square = this.checkUpperLevelSquare(square) || square;
    for (const block of square.blocks) {
      if (block.level > square.level) {
        mapProvider.getMap().changeOwnedBlockLevel(block, square.level);
      }
    }
    square.calcGoldAndResourceOutcomes();
  };

  return SquaresMap;
}]);