'use strict';

describe('SquaresMap', function() {
  beforeEach(module('square'));

  var SquaresMap, gameStateProvider, mapProvider, playerProvider,
    squareProvider, Square, storageProvider, testingUtils, ArrayUtils;

  beforeEach(inject(function (
      _SquaresMap_, _gameStateProvider_, _mapProvider_, _playerProvider_,
      _squareProvider_, _Square_, _storageProvider_, _testingUtils_, _ArrayUtils_) {
    SquaresMap = _SquaresMap_;
    gameStateProvider = _gameStateProvider_;
    mapProvider = _mapProvider_;
    playerProvider = _playerProvider_;

    squareProvider = _squareProvider_;
    Square = _Square_;
    storageProvider = _storageProvider_;
    testingUtils = _testingUtils_;
    ArrayUtils = _ArrayUtils_;
  }));

  var map;

  function loadFixture(fixtureName, done) {
    gameStateProvider.load(process.env.PWD +
        '/test_fixtures/' + fixtureName + '.json', function (err) {
      if (err) {
        done.fail(err);
        return;
      }
      map = mapProvider.getMap();
      done();
    });
  }

  describe('with easy_map_11x11_5x5_with_multiple_squares', function () {

    beforeEach(function (done) {
      loadFixture('easy_map_11x11_5x5_with_multiple_squares', done);
    });

    describe('removeBlockFromSquare', function () {

      var humanPlayer;
      var storage;
      var block;
      var bigSquare;
      var squaresMap;

      function checkSquaresResourceOutcome() {
        var prevResources = testingUtils.copyObject(storage.resources);
        Object.values(humanPlayer.squaresByLevel).forEach(function (squares) {
          squares.forEach(function (square) {
            storage.take(square.blockResourceOutcome, true);
          });
        });
        storage.take({ wood: 1 }, true); // initial wood
        expect(ArrayUtils.elementsAreEqual(
          [0, ...Object.values(storage.resources)]
        )).toBeTrue();
        storage.put(prevResources);
      }

      describe('for block Block-r1x-1_1x4L0h', function () {

        beforeEach(function () {
          block = map.getBlock(1, -1, 1, 4);
          bigSquare = block.square;
          humanPlayer = playerProvider.getHuman();
          squaresMap = squareProvider.getSquaresMap();
          storage = storageProvider.getStorage(humanPlayer);
        });

        it('deletes its square', function () {
          expect(squaresMap.getSquare(bigSquare.player, bigSquare.level,
            bigSquare.regionX, bigSquare.regionY, bigSquare.x, bigSquare.y)).toBe(bigSquare);
          expect(humanPlayer.squaresByLevel[bigSquare.level]).toContain(bigSquare);
          squaresMap.removeBlockFromSquare(block);
          expect(squaresMap.getSquare(bigSquare.player, bigSquare.level,
            bigSquare.regionX, bigSquare.regionY, bigSquare.x, bigSquare.y)).not.toBe(bigSquare);
          expect(humanPlayer.squaresByLevel[bigSquare.level]).not.toContain(bigSquare);
        });

        it('deconstructs its square properly', function () {
          spyOn(storage, 'put').and.callThrough();
          expect(map.getBlock(1, 0, 2, 0).level).toBe(2);
          expect(map.getBlock(1, 0, 2, 0).square).toBe(bigSquare);
          expect(map.getBlock(1, 0, 4, 0).level).toBe(2);
          expect(map.getBlock(1, 0, 4, 2).level).toBe(4);
          expect(map.getBlock(1, 0, 2, 4).level).toBe(2);
          expect(map.getBlock(1, 0, 0, 2).level).toBe(3);
          squaresMap.removeBlockFromSquare(block);
          [map.getBlock(1, -1, 2, 4),
           map.getBlock(1, 0, 1, 0),
           map.getBlock(1, 0, 2, 0)]
            .forEach(function () {
              expect(block.level).toBe(1);
              expect(block.square).toBeNull();
            });
          expect(storage.put.calls.allArgs()).toContain([{food: -1}]);
          expect(map.getBlock(1, 0, 4, 0).level).toBe(2);
          expect(map.getBlock(1, 0, 4, 0).square).toBeObject();
          expect(map.getBlock(1, 0, 4, 0).square).toBe(
            squaresMap.getSquare(humanPlayer, 2,
              1, -1, 3, 4));
          expect(map.getBlock(1, 0, 4, 2).level).toBe(3);
          expect(map.getBlock(1, 0, 4, 2).square).toBeObject();
          expect(map.getBlock(1, 0, 4, 2).square).toBe(
            squaresMap.getSquare(humanPlayer, 3,
              1, 0, 1, 1));
          expect(storage.put.calls.allArgs()).toContain([{wood: -4}]);
          expect(map.getBlock(1, 0, 2, 4).level).toBe(2);
          expect(map.getBlock(1, 0, 2, 4).square).toBeObject();
          expect(map.getBlock(1, 0, 2, 4).square).toBe(
            squaresMap.getSquare(humanPlayer, 3,
              1, 0, 1, 1));
          expect(map.getBlock(1, 0, 0, 2).level).toBe(3);
          expect(map.getBlock(1, 0, 0, 2).square).toBeObject();
          expect(map.getBlock(1, 0, 0, 2).square).toBe(
            squaresMap.getSquare(humanPlayer, 3,
              0, -1, 2, 4));
          expect(storage.put.calls.count()).toBe(2);
        });

        it('recalculates square outcomes properly', function () {
          var prevResources = testingUtils.copyObject(storage.resources);
          checkSquaresResourceOutcome();
          squaresMap.removeBlockFromSquare(block);
          storage.take({ wood: 2, food: 1 }, true); // level-1 blocks production
          storage.take({ stone: 1 }, true); // level-0 block production
          storage.put({ stone: 2, wood: 1 }); // level-1 blocks consumption
          storage.put({ metal: 1 }); // level-0 block consumption
          checkSquaresResourceOutcome();
          storage.put({ wood: 4, food: 1 }); // higher level blocks production
          storage.put({ wood: 2, food: 1 }); // level-1 blocks production
          storage.put({ stone: 1 }); // level-0 block production
          storage.take({ stone: 2, wood: 1 }, true); // level-1 blocks consumption
          storage.take({ metal: 1 }, true); // level-0 block consumption
          expect(storage.resources).toEqual(prevResources);
        });

      });

      describe('for block Block-r1x-1_0x4L0h', function () {

        beforeEach(function () {
          block = map.getBlock(1, -1, 0, 4);
          bigSquare = block.square;
          humanPlayer = playerProvider.getHuman();
          squaresMap = squareProvider.getSquaresMap();
          storage = storageProvider.getStorage(humanPlayer);
        });

        it('deletes its square', function () {
          expect(squaresMap.getSquare(bigSquare.player, bigSquare.level,
            bigSquare.regionX, bigSquare.regionY, bigSquare.x, bigSquare.y)).toBe(bigSquare);
          expect(humanPlayer.squaresByLevel[bigSquare.level].indexOf(bigSquare)).not.toBe(-1);
          squaresMap.removeBlockFromSquare(block);
          expect(squaresMap.getSquare(bigSquare.player, bigSquare.level,
            bigSquare.regionX, bigSquare.regionY, bigSquare.x, bigSquare.y)).not.toBe(bigSquare);
          expect(humanPlayer.squaresByLevel[bigSquare.level].indexOf(bigSquare)).toBe(-1);
        });

        it('deconstructs its square properly', function () {
          spyOn(storage, 'put').and.callThrough();
          squaresMap.removeBlockFromSquare(block);
          [map.getBlock(0, -1, 4, 4),
           map.getBlock(0, 0, 4, 0),
           map.getBlock(1, 0, 0, 0)]
            .forEach(function () {
              expect(block.level).toBe(1);
              expect(block.square).toBeNull();
            });
          expect(storage.put.calls.allArgs()).toContain([{stone: -1}]);
          expect(storage.put.calls.allArgs()).toContain([{wood: -2}]);
          expect(map.getBlock(0, 0, 3, 0).level).toBe(2);
          expect(map.getBlock(0, 0, 3, 0).square).toBeObject();
          expect(map.getBlock(0, 0, 3, 0).square).toBe(
            squaresMap.getSquare(humanPlayer, 2,
              0, -1, 2, 4));
          expect(map.getBlock(0, 0, 3, 2).level).toBe(2);
          expect(map.getBlock(0, 0, 3, 2).square).toBeObject();
          expect(map.getBlock(0, 0, 3, 2).square).toBe(
            squaresMap.getSquare(humanPlayer, 3,
              0, 0, 0, 1));
          expect(storage.put.calls.count()).toBe(2);
        });

        it('recalculates square outcomes properly', function () {
          var prevResources = testingUtils.copyObject(storage.resources);
          checkSquaresResourceOutcome();
          squaresMap.removeBlockFromSquare(block);
          storage.take({ stone: 2, wood: 1 }, true); // level-1 blocks production
          storage.take({ stone: 1 }, true); // level-0 block production
          storage.put({ metal: 2, stone: 1 }); // level-1 blocks consumption
          storage.put({ metal: 1 }); // level-0 block consumption
          checkSquaresResourceOutcome();
          storage.put({ wood: 2, stone: 1 }); // higher level blocks production
          storage.put({ stone: 2, wood: 1 }); // level-1 blocks production
          storage.put({ stone: 1 }); // level-0 block production
          storage.take({ metal: 2, stone: 1 }, true); // level-1 blocks consumption
          storage.take({ metal: 1 }, true); // level-0 block consumption
          expect(storage.resources).toEqual(prevResources);
        });

      });

      describe('for block Block-r2x0_0x4L1m', function () {

        var captureBlock = block =>
          expect(map.tryToCaptureBlock(block, humanPlayer)).toBeTrue();

        beforeEach(function () {
          block = map.getBlock(2, 0, 0, 4);
          humanPlayer = playerProvider.getHuman();
          squaresMap = squareProvider.getSquaresMap();
        });

        it('after capturing new blocks', function () {
          var block_1 = map.getBlock(2, 0, 2, 3);
          var block_2 = map.getBlock(2, 0, 2, 4);
          captureBlock(block_1);
          captureBlock(block_2);
          squaresMap.removeBlockFromSquare(block);
          expect(block_1.level).toBe(1);
          expect(block_2.level).toBe(1);
        });

        it('before capturing new blocks', function () {
          var block_1 = map.getBlock(2, 0, 2, 3);
          var block_2 = map.getBlock(2, 0, 2, 4);
          squaresMap.removeBlockFromSquare(block);
          captureBlock(block_1);
          captureBlock(block_2);
          expect(block_1.level).toBe(1);
          expect(block_2.level).toBe(2);
        });

      });

    });

  });

  describe('with simple_map_3x3_5x5', function () {

    beforeEach(function (done) {
      loadFixture('simple_map_3x3_5x5', done);
    });

    describe('getSquare', function () {

      it('returns a proper square', function () {
        var regionX = -1;
        var regionY = -1;
        var x = 3;
        var y = 4;
        var level = 4;
        var player = playerProvider.getHuman();
        var square1 = squareProvider.getSquareFactory().saveSquare(
          squareProvider.getSquareFactory().createSquare(regionX, regionY, x, y, level, player));
        squareProvider.getSquareFactory().saveSquare(
          squareProvider.getSquareFactory().createSquare(regionX, regionY, x + 1, y, level, player));
        var returnedSquare = squareProvider.getSquaresMap().getSquare(player, level, regionX, regionY, x, y);
        expect(returnedSquare).toBe(square1);
        var returnedSquareOfDenormalizedCoords = squareProvider.getSquaresMap().getSquare(
          player, level, regionX - 1, regionY, x + 5, y);
        expect(returnedSquareOfDenormalizedCoords).toBe(square1);
      });

    });

    describe('checkSquares', function () {

      it('returns a new upper level square if all 4 lower level squares match', function () {
        var regionX = 1;
        var regionY = 1;
        var x = 0;
        var y = 0;
        var level = 2;
        var player = playerProvider.getHuman();
        var square1 = squareProvider.getSquareFactory().createSquare(regionX, regionY, x, y, level, player);
        var square2 = squareProvider.getSquareFactory().createSquare(regionX, regionY, x + 2, y, level, player);
        var square3 = squareProvider.getSquareFactory().createSquare(regionX, regionY, x, y + 2, level, player);
        var square4 = squareProvider.getSquareFactory().createSquare(regionX, regionY, x + 2, y + 2, level, player);
        squareProvider.getSquareFactory().saveSquare(square1);
        squareProvider.getSquareFactory().saveSquare(square2);
        squareProvider.getSquareFactory().saveSquare(square3);
        expect(squareProvider.getSquaresMap().squares[player.playerId][level][regionY][regionX][y][x]).toBe(square1);
        expect(squareProvider.getSquaresMap().squares[player.playerId][level][regionY][regionX][y][x + 2]).toBe(square2);
        expect(squareProvider.getSquaresMap().squares[player.playerId][level][regionY][regionX][y + 2][x]).toBe(square3);
        var newSquare = squareProvider.getSquaresMap().checkSquares(square4, player);
        expect(newSquare.level).toBe(level + 1);
        expect(newSquare.x).toBe(x);
        expect(newSquare.y).toBe(y);
        expect(newSquare.regionX).toBe(regionX);
        expect(newSquare.regionY).toBe(regionY);
        expect(Array.isArray(newSquare.blocks)).toBeTrue();
        expect(newSquare.blocks.length).toBe(16);
      });

      it('returns a new 2-levels-up square if all 4 the same level squares and 3 upper level squares match', function () {
        var regionX = -1;
        var regionY = -1;
        var x = 0;
        var y = 0;
        var level = 2;
        var player = playerProvider.getHuman();
        var square1 = squareProvider.getSquareFactory().createSquare(regionX, regionY, x, y, level, player);
        var square2 = squareProvider.getSquareFactory().createSquare(regionX, regionY, x + 2, y, level, player);
        var square3 = squareProvider.getSquareFactory().createSquare(regionX, regionY, x, y + 2, level, player);
        var square4 = squareProvider.getSquareFactory().createSquare(regionX, regionY, x + 2, y + 2, level, player);
        squareProvider.getSquareFactory().saveSquare(square1);
        squareProvider.getSquareFactory().saveSquare(square2);
        squareProvider.getSquareFactory().saveSquare(square3);
        var squareLvl3_1 = squareProvider.getSquareFactory().createSquare(regionX, regionY, x + 4, y, level + 1, player);
        var squareLvl3_2 = squareProvider.getSquareFactory().createSquare(regionX, regionY, x, y + 4, level + 1, player);
        var squareLvl3_3 = squareProvider.getSquareFactory().createSquare(regionX, regionY, x + 4, y + 4, level + 1, player);
        squareProvider.getSquareFactory().saveSquare(squareLvl3_1);
        squareProvider.getSquareFactory().saveSquare(squareLvl3_2);
        squareProvider.getSquareFactory().saveSquare(squareLvl3_3);
        expect(squareProvider.getSquaresMap().squares[player.playerId][level][regionY][regionX][y][x]).toBe(square1);
        expect(squareProvider.getSquaresMap().squares[player.playerId][level][regionY][regionX][y][x + 2]).toBe(square2);
        expect(squareProvider.getSquaresMap().squares[player.playerId][level][regionY][regionX][y + 2][x]).toBe(square3);
        expect(squareProvider.getSquaresMap().squares[player.playerId][level + 1][regionY][regionX][y][x + 4]).toBe(squareLvl3_1);
        expect(squareProvider.getSquaresMap().squares[player.playerId][level + 1][regionY][regionX][y + 4][x]).toBe(squareLvl3_2);
        expect(squareProvider.getSquaresMap().squares[player.playerId][level + 1][regionY][regionX][y + 4][x + 4]).toBe(squareLvl3_3);
        var newSquare = squareProvider.getSquaresMap().checkSquares(square4, player);
        expect(newSquare.level).toBe(level + 2);
        expect(newSquare.x).toBe(x);
        expect(newSquare.y).toBe(y);
        expect(newSquare.regionX).toBe(regionX);
        expect(newSquare.regionY).toBe(regionY);
        expect(Array.isArray(newSquare.blocks)).toBeTrue();
        expect(newSquare.blocks.length).toBe(64);
        expect(squareProvider.getSquaresMap().squares[player.playerId][level + 2][regionY][regionX][y][x]).toBe(newSquare);
      });

      it('returns false if not all 4 lower level squares match', function () {
        var regionX = -1;
        var regionY = 1;
        var x = 0;
        var y = 0;
        var level = 2;
        var player = playerProvider.getHuman();
        var square1 = squareProvider.getSquareFactory().createSquare(regionX, regionY, x, y, level, player);
        var square2 = squareProvider.getSquareFactory().createSquare(regionX, regionY, x + 2, y, level, player);
        var square3 = squareProvider.getSquareFactory().createSquare(regionX, regionY, x, y + 2 + 1, level, player);
        var square4 = squareProvider.getSquareFactory().createSquare(regionX, regionY, x + 2, y + 2, level, player);
        squareProvider.getSquareFactory().saveSquare(square1);
        squareProvider.getSquareFactory().saveSquare(square2);
        squareProvider.getSquareFactory().saveSquare(square3);
        expect(squareProvider.getSquaresMap().squares[player.playerId][level][regionY][regionX][y][x]).toBe(square1);
        expect(squareProvider.getSquaresMap().squares[player.playerId][level][regionY][regionX][y][x + 2]).toBe(square2);
        expect(squareProvider.getSquaresMap().squares[player.playerId][level][regionY][regionX][y + 2 + 1][x]).toBe(square3);
        expect(squareProvider.getSquaresMap().squares[player.playerId][level][regionY][regionX][y + 2]).toBeUndefined();
        var newSquare = squareProvider.getSquaresMap().checkSquares(square4, player);
        expect(newSquare).toBeFalse();
      });

      it('returns false if any of 4 lower level squares belongs to another player', function () {
        var regionX = 0;
        var regionY = 1;
        var x = 0;
        var y = 0;
        var level = 2;
        var player = playerProvider.getHuman();
        var anotherPlayer = playerProvider.getFirstAI();
        var square1 = squareProvider.getSquareFactory().createSquare(regionX, regionY, x, y, level, player);
        var square2 = squareProvider.getSquareFactory().createSquare(regionX, regionY, x + 2, y, level, player);
        var square3 = squareProvider.getSquareFactory().createSquare(regionX, regionY, x, y + 2, level, anotherPlayer);
        var square4 = squareProvider.getSquareFactory().createSquare(regionX, regionY, x + 2, y + 2, level, player);
        squareProvider.getSquareFactory().saveSquare(square1);
        squareProvider.getSquareFactory().saveSquare(square2);
        squareProvider.getSquareFactory().saveSquare(square3);
        expect(squareProvider.getSquaresMap().squares[player.playerId][level][regionY][regionX][y][x]).toBe(square1);
        expect(squareProvider.getSquaresMap().squares[player.playerId][level][regionY][regionX][y][x + 2]).toBe(square2);
        expect(squareProvider.getSquaresMap().squares[anotherPlayer.playerId][level][regionY][regionX][y + 2][x]).toBe(square3);
        expect(squareProvider.getSquaresMap().squares[player.playerId][level][regionY][regionX][y + 2]).toBeUndefined();
        var newSquare = squareProvider.getSquaresMap().checkSquares(square4, player);
        expect(newSquare).toBeFalse();
      });

    });

    describe('checkSmallestSquare', function () {

      it('adds square if 4 blocks match', function () {
        var regionX = -1;
        var regionY = 0;
        var x = 4;
        var y = 0;
        var player = playerProvider.getHuman();
        var block1 = mapProvider.getBlock(regionX, regionY, x, y);
        var block2 = mapProvider.getBlock(regionX, regionY, x + 1, y);
        var block3 = mapProvider.getBlock(regionX, regionY, x + 1, y + 1);
        block1.level = block2.level = block3.level = 1;
        block1.ownedBy = block2.ownedBy = block3.ownedBy = player;
        var lastBlock = mapProvider.getBlock(regionX, regionY, x, y + 1);
        lastBlock.level = 1;
        var square = squareProvider.getSquaresMap().checkSmallestSquare(lastBlock, player);
        expect(square instanceof Square).toBeTrue();
        expect(squareProvider.getSquaresMap().squares[player.playerId][2][regionY][regionX][y][x] instanceof Square).toBeTrue();
      });

      it('does not add square if 4 blocks do not match', function () {
        var regionX = -1;
        var regionY = 0;
        var x = 4;
        var y = 0;
        var player = playerProvider.getHuman();
        var block1 = mapProvider.getBlock(regionX, regionY, x, y);
        var block2 = mapProvider.getBlock(regionX, regionY, x + 1, y);
        var block3 = mapProvider.getBlock(regionX, regionY, x + 2, y + 1);
        block1.level = block2.level = block3.level = 1;
        block1.ownedBy = block2.ownedBy = block3.ownedBy = player;
        var lastBlock = mapProvider.getBlock(regionX, regionY, x, y + 1);
        lastBlock.level = 1;
        var square = squareProvider.getSquaresMap().checkSmallestSquare(lastBlock, player);
        expect(square).toBeFalse();
        expect(squareProvider.getSquaresMap().squares[player.playerId]).toBeEmptyObject();
      });

      it('does not add square if any of 4 blocks belongs to another player', function () {
        var regionX = -1;
        var regionY = 0;
        var x = 4;
        var y = 0;
        var player = playerProvider.getHuman();
        var anotherPlayer = playerProvider.getFirstAI();
        var block1 = mapProvider.getBlock(regionX, regionY, x, y);
        var block2 = mapProvider.getBlock(regionX, regionY, x + 1, y);
        var block3 = mapProvider.getBlock(regionX, regionY, x + 1, y + 1);
        block1.level = block2.level = block3.level = 1;
        block1.ownedBy = block2.ownedBy = player;
        block3.ownedBy = anotherPlayer;
        var lastBlock = mapProvider.getBlock(regionX, regionY, x, y + 1);
        lastBlock.level = 1;
        var square = squareProvider.getSquaresMap().checkSmallestSquare(lastBlock, player);
        expect(square).toBeFalse();
        expect(squareProvider.getSquaresMap().squares[player.playerId]).toBeEmptyObject();
      });

      it('does not add square if there is another square already', function () {
        var regionX = -1;
        var regionY = 0;
        var x = 4;
        var y = 0;
        var player = playerProvider.getHuman();
        var existingSquareBlocks = mapProvider.getBlocks(regionX, regionY, x - 1, y - 1, 2, 2);
        existingSquareBlocks.forEach(function (block) { block.level = 1; block.ownedBy = player; });
        var existingSquare = squareProvider.getSquaresMap().checkSmallestSquare(existingSquareBlocks[3], player);
        expect(existingSquare instanceof Square).toBeTrue();
        expect(Object.keys(squareProvider.getSquaresMap().squares[player.playerId]).length).toBe(1);
        mapProvider.getBlock(regionX, regionY, x + 1, y).level = 1;
        mapProvider.getBlock(regionX, regionY, x + 1, y + 1).level = 1;
        var lastBlock = mapProvider.getBlock(regionX, regionY, x, y + 1);
        lastBlock.level = 1;
        var square = squareProvider.getSquaresMap().checkSmallestSquare(lastBlock, player);
        expect(square).toBeFalse();
        expect(Object.keys(squareProvider.getSquaresMap().squares[player.playerId]).length).toBe(1);
      });

      it('sets proper squarePos of proper blocks', function () {
        var regionX = 1;
        var regionY = -1;
        var x = 0;
        var y = 1;
        var player = playerProvider.getHuman();
        var block1 = mapProvider.getBlock(regionX, regionY, x, y);
        var block2 = mapProvider.getBlock(regionX, regionY, x + 1, y);
        var block3 = mapProvider.getBlock(regionX, regionY, x + 1, y + 1);
        block1.level = block2.level = block3.level = 1;
        block1.ownedBy = block2.ownedBy = block3.ownedBy = player;
        var lastBlock = mapProvider.getBlock(regionX, regionY, x, y + 1);
        lastBlock.level = 1;
        lastBlock.ownedBy = player;
        var square = squareProvider.getSquaresMap().checkSmallestSquare(lastBlock, player);
        expect(square instanceof Square).toBeTrue();
        expect(mapProvider.getBlock(regionX, regionY, x, y).squarePos).toBe('left-top');
        expect(mapProvider.getBlock(regionX, regionY, x + 1, y).squarePos).toBe('right-top');
        expect(mapProvider.getBlock(regionX, regionY, x + 1, y + 1).squarePos).toBe('right-bottom');
        expect(mapProvider.getBlock(regionX, regionY, x, y + 1).squarePos).toBe('left-bottom');
      });

      it('does not throw error on a fixed size map border', function () {
        var regionX = 1;
        var regionY = 0;
        var x = 4;
        var y = 0;
        var player = playerProvider.getHuman();
        var block1 = mapProvider.getBlock(regionX, regionY, x, y);
        var blockBeyondBorders = mapProvider.getBlock(regionX, regionY, x + 1, y);
        expect(blockBeyondBorders).toBeNull();
        block1.level = 1;
        block1.ownedBy = player;
        var anotherBlock = mapProvider.getBlock(regionX, regionY, x, y + 1);
        anotherBlock.level = 1;
        var square = squareProvider.getSquaresMap().checkSmallestSquare(anotherBlock, player);
        expect(square).toBeFalse();
        expect(squareProvider.getSquaresMap().squares[player.playerId]).toBeEmptyObject();
      });

    });

    describe('checkSmallestSquareWithAdditionalBlocks', function () {

      it('returns a new square if selectedBlock matches to other 3 improved blocks', function () {
        var regionX = -1;
        var regionY = 0;
        var x = 4;
        var y = 0;
        var player = playerProvider.getHuman();
        var block1 = map.getBlock(regionX, regionY, x, y);
        var block2 = map.getBlock(regionX, regionY, x + 1, y);
        var block3 = map.getBlock(regionX, regionY, x + 1, y + 1);
        block1.level = block2.level = block3.level = 1;
        block1.ownedBy = block2.ownedBy = block3.ownedBy = player;
        var selectedBlock = map.getBlock(regionX, regionY, x, y + 1);
        selectedBlock.level = 1;
        map.setNeighborhoodImprovedNeighbor(block1);
        map.setNeighborhoodImprovedNeighbor(block2);
        map.setNeighborhoodImprovedNeighbor(block3);
        map.setNeighborhoodImprovedNeighbor(selectedBlock);
        var square = squareProvider.getSquaresMap().checkSmallestSquareWithAdditionalBlocks(selectedBlock, player, []);
        expect(square).toBeTruthy();
      });

      it('returns a new square if selectedBlock matches to other 2 improved blocks and 1 passed as additionalBlock', function () {
        var regionX = -1;
        var regionY = 0;
        var x = 4;
        var y = 0;
        var player = playerProvider.getHuman();
        var block1 = map.getBlock(regionX, regionY, x, y);
        var block2 = map.getBlock(regionX, regionY, x + 1, y);
        var block3 = map.getBlock(regionX, regionY, x + 1, y + 1);
        block1.level = block2.level = 1;
        block1.ownedBy = block2.ownedBy = player;
        var selectedBlock = map.getBlock(regionX, regionY, x, y + 1);
        selectedBlock.level = 1;
        map.setNeighborhoodImprovedNeighbor(block1);
        map.setNeighborhoodImprovedNeighbor(block2);
        map.setNeighborhoodImprovedNeighbor(selectedBlock);
        var notSquare = squareProvider.getSquaresMap().checkSmallestSquareWithAdditionalBlocks(selectedBlock, player, []);
        expect(notSquare).toBeFalse();
        var square = squareProvider.getSquaresMap().checkSmallestSquareWithAdditionalBlocks(selectedBlock, player, [block3]);
        expect(square).toBeTruthy();
      });

      it('returns a new square if selectedBlock matches to 3 other not improved blocks passed as additionalBlocks', function () {
        var regionX = -1;
        var regionY = 0;
        var x = 4;
        var y = 0;
        var player = playerProvider.getHuman();
        var block1 = map.getBlock(regionX, regionY, x, y);
        var block2 = map.getBlock(regionX, regionY, x + 1, y);
        var block3 = map.getBlock(regionX, regionY, x + 1, y + 1);
        var selectedBlock = map.getBlock(regionX, regionY, x, y + 1);
        selectedBlock.level = 1;
        map.setNeighborhoodImprovedNeighbor(selectedBlock);
        var square = squareProvider.getSquaresMap().checkSmallestSquareWithAdditionalBlocks(selectedBlock, player, [block1, block2, block3]);
        expect(square).toBeTruthy();
      });

      it('returns a new square if selectedBlock matches to 3 other not improved blocks passed as additionalBlocks #2', function () {
        var regionX = 1;
        var regionY = 1;
        var x = 1;
        var y = 2;
        var player = playerProvider.getHuman();
        var block1 = map.getBlock(regionX, regionY, x, y + 1);
        var block2 = map.getBlock(regionX, regionY, x + 1, y);
        var block3 = map.getBlock(regionX, regionY, x + 1, y + 1);
        var selectedBlock = map.getBlock(regionX, regionY, x, y);
        selectedBlock.level = 1;
        map.setNeighborhoodImprovedNeighbor(selectedBlock);
        var notSquare = squareProvider.getSquaresMap().checkSmallestSquareWithAdditionalBlocks(selectedBlock, player, []);
        expect(notSquare).toBeFalse();
        var square = squareProvider.getSquaresMap().checkSmallestSquareWithAdditionalBlocks(selectedBlock, player, [block1, block2, block3]);
        expect(square).toBeTruthy();
      });

      it('returns a new square if selectedBlock matches to 1 another improved block and 2 other passed as additionalBlocks', function () {
        var regionX = 1;
        var regionY = 1;
        var x = 1;
        var y = 2;
        var player = playerProvider.getHuman();
        var block1 = map.getBlock(regionX, regionY, x, y + 1);
        var block2 = map.getBlock(regionX, regionY, x + 1, y);
        var block3 = map.getBlock(regionX, regionY, x, y);
        block1.level = 1;
        block1.ownedBy = player;
        var selectedBlock = map.getBlock(regionX, regionY, x + 1, y + 1);
        selectedBlock.level = 1;
        map.setNeighborhoodImprovedNeighbor(block1);
        map.setNeighborhoodImprovedNeighbor(selectedBlock);
        var square = squareProvider.getSquaresMap().checkSmallestSquareWithAdditionalBlocks(selectedBlock, player, [block2, block3]);
        expect(square).toBeTruthy();
      });

      it('returns a new square if selectedBlock matches to 1 another improved block and 2 other passed as additionalBlocks #2', function () {
        var regionX = 1;
        var regionY = 1;
        var x = 1;
        var y = 2;
        var player = playerProvider.getHuman();
        var block1 = map.getBlock(regionX, regionY, x, y + 1);
        var block2 = map.getBlock(regionX, regionY, x + 1, y);
        var block3 = map.getBlock(regionX, regionY, x, y);
        block3.level = 1;
        block3.ownedBy = player;
        var selectedBlock = map.getBlock(regionX, regionY, x + 1, y + 1);
        selectedBlock.level = 1;
        map.setNeighborhoodImprovedNeighbor(block3);
        map.setNeighborhoodImprovedNeighbor(selectedBlock);
        var square = squareProvider.getSquaresMap().checkSmallestSquareWithAdditionalBlocks(selectedBlock, player, [block1, block2]);
        expect(square).toBeTruthy();
      });

      it('does not return a new square if selectedBlock does not match to 3 other not improved blocks passed as additionalBlocks', function () {
        var regionX = 1;
        var regionY = 1;
        var x = 1;
        var y = 2;
        var player = playerProvider.getHuman();
        var block1 = map.getBlock(regionX, regionY, x, y + 1);
        var block2 = map.getBlock(regionX, regionY, x + 1, y - 1);
        var block3 = map.getBlock(regionX, regionY, x + 1, y + 1);
        var selectedBlock = map.getBlock(regionX, regionY, x, y);
        selectedBlock.level = 1;
        map.setNeighborhoodImprovedNeighbor(selectedBlock);
        var notSquare = squareProvider.getSquaresMap().checkSmallestSquareWithAdditionalBlocks(selectedBlock, player, [block1, block2, block3]);
        expect(notSquare).toBeFalse();
      });

      it('does not return a new square if selectedBlock does not match to 1 another improved block and 2 other passed as additionalBlocks', function () {
        var regionX = 1;
        var regionY = 1;
        var x = 1;
        var y = 2;
        var player = playerProvider.getHuman();
        var block1 = map.getBlock(regionX, regionY, x, y + 1);
        var block2 = map.getBlock(regionX, regionY, x + 1, y);
        var block3 = map.getBlock(regionX, regionY, x, y - 1);
        block1.level = 1;
        block1.ownedBy = player;
        var selectedBlock = map.getBlock(regionX, regionY, x + 1, y + 1);
        selectedBlock.level = 1;
        map.setNeighborhoodImprovedNeighbor(block1);
        map.setNeighborhoodImprovedNeighbor(selectedBlock);
        var notSquare = squareProvider.getSquaresMap().checkSmallestSquareWithAdditionalBlocks(selectedBlock, player, [block2, block3]);
        expect(notSquare).toBeFalse();
      });

    });

  });

});