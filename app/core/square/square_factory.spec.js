'use strict';

describe('SquareFactory', function() {
  beforeEach(module('square'));

  var SquareFactory, Square, storageProvider, mapProvider;

  beforeEach(inject(function (_SquareFactory_, _Square_, _storageProvider_, _mapProvider_) {
    SquareFactory = _SquareFactory_;
    Square = _Square_;
    storageProvider = _storageProvider_;
    mapProvider = _mapProvider_;
  }));

  describe('setSquaresMap', function () {

    it('sets squaresMap', function () {
      var squaresMap = jasmine.createSpy('squaresMap');
      var squareFactory = new SquareFactory();
      squareFactory.setSquaresMap(squaresMap);
      expect(squareFactory.squaresMap).toBe(squaresMap);
    });

  });

  describe('_saveSquareToBlocks', function () {

    var squareFactory;
    var cornerBlocks;

    beforeEach(function () {
      squareFactory = new SquareFactory();
      spyOn(squareFactory, '_getSquareBorderBlocks').and.returnValue({
        top: [],
        right: [],
        bottom: [],
        left: []
      });
      cornerBlocks = {
        leftTop: jasmine.createSpyObj('cornerBlock-leftTop', ['squarePos']),
        rightTop: jasmine.createSpyObj('cornerBlock-rightTop', ['squarePos']),
        rightBottom: jasmine.createSpyObj('cornerBlock-rightBottom', ['squarePos']),
        leftBottom: jasmine.createSpyObj('cornerBlock-leftBottom', ['squarePos'])
      };
      spyOn(squareFactory, '_getSquareCornerBlocks').and.returnValue(cornerBlocks);
    });

    it('sets square for each block in the square', function () {
      var square = {
        blocks: [
          jasmine.createSpyObj('block1', ['square', 'squarePos']),
          jasmine.createSpyObj('block2', ['square', 'squarePos']),
          jasmine.createSpyObj('block3', ['square', 'squarePos']),
          jasmine.createSpyObj('block4', ['square', 'squarePos']),
          jasmine.createSpyObj('block5', ['square', 'squarePos'])
        ],
        level: 3
      };
      square.blocks.forEach(function (block) {
        expect(block.square).not.toBe(square);
      });
      squareFactory._saveSquareToBlocks(square);
      square.blocks.forEach(function (block) {
        expect(block.square).toBe(square);
      });
    });

    it('sets squarePos=none for each block in the square', function () {
      var square = {
        blocks: [
          jasmine.createSpyObj('block1', ['square', 'squarePos']),
          jasmine.createSpyObj('block2', ['square', 'squarePos']),
          jasmine.createSpyObj('block3', ['square', 'squarePos'])
        ],
        level: 3
      };
      square.blocks.forEach(function (block) {
        expect(block.squarePos).not.toBe('none');
      });
      squareFactory._saveSquareToBlocks(square);
      square.blocks.forEach(function (block) {
        expect(block.squarePos).toBe('none');
      });
    });

    it('sets proper squarePos for border blocks in the square if square level is greater than 2', function () {
      var square = {
        blocks: [],
        level: 4
      };
      var borderBlocks = {
        top: [
          jasmine.createSpyObj('borderBlock-top-1', ['squarePos']),
          jasmine.createSpyObj('borderBlock-top-2', ['squarePos']),
          jasmine.createSpyObj('borderBlock-top-3', ['squarePos'])
        ],
        right: [
          jasmine.createSpyObj('borderBlock-right-1', ['squarePos']),
          jasmine.createSpyObj('borderBlock-right-2', ['squarePos'])
        ],
        bottom: [
          jasmine.createSpyObj('borderBlock-bottom-1', ['squarePos'])
        ],
        left: [
          jasmine.createSpyObj('borderBlock-left-1', ['squarePos']),
          jasmine.createSpyObj('borderBlock-left-2', ['squarePos']),
          jasmine.createSpyObj('borderBlock-left-3', ['squarePos']),
          jasmine.createSpyObj('borderBlock-left-4', ['squarePos'])
        ]
      };
      squareFactory._getSquareBorderBlocks.and.returnValue(borderBlocks);
      squareFactory._saveSquareToBlocks(square);
      Object.keys(borderBlocks).forEach(function (border) {
        borderBlocks[border].forEach(function (block) {
          expect(block.squarePos).toBe(border);
        });
      });
    });

    it('does not set squarePos for border blocks in the square if square level is equal 2', function () {
      var square = {
        blocks: [],
        level: 2
      };
      var borderBlocks = {
        top: [
          jasmine.createSpyObj('borderBlock-top-1', ['squarePos'])
        ],
        right: [
          jasmine.createSpyObj('borderBlock-right-1', ['squarePos']),
          jasmine.createSpyObj('borderBlock-right-2', ['squarePos'])
        ],
        bottom: [
          jasmine.createSpyObj('borderBlock-bottom-1', ['squarePos'])
        ],
        left: [
          jasmine.createSpyObj('borderBlock-left-1', ['squarePos']),
          jasmine.createSpyObj('borderBlock-left-2', ['squarePos']),
          jasmine.createSpyObj('borderBlock-left-3', ['squarePos'])
        ]
      };
      squareFactory._getSquareBorderBlocks.and.returnValue(borderBlocks);
      squareFactory._saveSquareToBlocks(square);
      Object.keys(borderBlocks).forEach(function (border) {
        borderBlocks[border].forEach(function (block) {
          expect(block.squarePos).not.toBe(border);
        });
      });
    });

    it('sets proper squarePos for corner blocks in the square', function () {
      var square = {
        blocks: [],
        level: 4
      };
      squareFactory._saveSquareToBlocks(square);
      expect(cornerBlocks.leftTop.squarePos).toBe('left-top');
      expect(cornerBlocks.rightTop.squarePos).toBe('right-top');
      expect(cornerBlocks.rightBottom.squarePos).toBe('right-bottom');
      expect(cornerBlocks.leftBottom.squarePos).toBe('left-bottom');
    });

  });

  describe('_getSquareCornerBlocks', function () {

    describe('returns obj with proper blocks', function () {

      it('for square level = 2', function () {
        var squareFactory = new SquareFactory();
        spyOn(mapProvider, 'getBlock').and.callFake(function (regionX, regionY, blockX, blockY) {
          if (blockX === 0 && blockY === 0) {
            return 'leftTopBlock';
          }
          if (blockX === 1 && blockY === 0) {
            return 'rightTopBlock';
          }
          if (blockX === 1 && blockY === 1) {
            return 'rightBottomBlock';
          }
          if (blockX === 0 && blockY === 1) {
            return 'leftBottomBlock';
          }
        });
        var square = new Square(0, 0, 0, 0, [], 2);
        var result = squareFactory._getSquareCornerBlocks(square);
        expect(result.leftTop).toBe('leftTopBlock');
        expect(result.rightTop).toBe('rightTopBlock');
        expect(result.rightBottom).toBe('rightBottomBlock');
        expect(result.leftBottom).toBe('leftBottomBlock');
      });

      it('for square level = 4', function () {
        var squareFactory = new SquareFactory();
        spyOn(mapProvider, 'getBlock').and.callFake(function (regionX, regionY, blockX, blockY) {
          if (blockX === 0 && blockY === 0) {
            return 'leftTopBlock';
          }
          if (blockX === 7 && blockY === 0) {
            return 'rightTopBlock';
          }
          if (blockX === 7 && blockY === 7) {
            return 'rightBottomBlock';
          }
          if (blockX === 0 && blockY === 7) {
            return 'leftBottomBlock';
          }
        });
        var square = new Square(0, 0, 0, 0, [], 4);
        var result = squareFactory._getSquareCornerBlocks(square);
        expect(result.leftTop).toBe('leftTopBlock');
        expect(result.rightTop).toBe('rightTopBlock');
        expect(result.rightBottom).toBe('rightBottomBlock');
        expect(result.leftBottom).toBe('leftBottomBlock');
      });

    });

  });

  describe('_getSquareBorderBlocks', function () {

    describe('returns obj with proper blocks', function () {

      var squareFactory;
      var square;

      function setupTest(squareLevel) {
        squareFactory = new SquareFactory();
        var squareWidth = Square.getWidth(squareLevel);
        var squareHeight = Square.getHeight(squareLevel);
        spyOn(mapProvider, 'getBlocks').and.callFake(function (regionX, regionY, blockX, blockY, width, height) {
          if (blockX === 0 && blockY === 0 && width === squareWidth) {
            return 'topBlocks';
          }
          if (blockX === 0 && blockY === 0 && height === squareHeight) {
            return 'leftBlocks';
          }
          if (blockX === squareWidth - 1 && blockY === 0 && height === squareHeight) {
            return 'rightBlocks';
          }
          if (blockX === 0 && blockY === squareHeight - 1 && width === squareWidth) {
            return 'bottomBlocks';
          }
          //else:
          throw new Error('Invalid coords given');
        });
        square = new Square(0, 0, 0, 0, [], squareLevel);
      }

      it('for square level = 3', function () {
        var squareLevel = 3;
        setupTest(squareLevel);
        var result = squareFactory._getSquareBorderBlocks(square);
        expect(result.top).toBe('topBlocks');
        expect(result.left).toBe('leftBlocks');
        expect(result.right).toBe('rightBlocks');
        expect(result.bottom).toBe('bottomBlocks');
      });

      it('for square level = 4', function () {
        var squareLevel = 4;
        setupTest(squareLevel);
        var result = squareFactory._getSquareBorderBlocks(square);
        expect(result.top).toBe('topBlocks');
        expect(result.left).toBe('leftBlocks');
        expect(result.right).toBe('rightBlocks');
        expect(result.bottom).toBe('bottomBlocks');
      });

      it('for square level = 5', function () {
        var squareLevel = 5;
        setupTest(squareLevel);
        var result = squareFactory._getSquareBorderBlocks(square);
        expect(result.top).toBe('topBlocks');
        expect(result.left).toBe('leftBlocks');
        expect(result.right).toBe('rightBlocks');
        expect(result.bottom).toBe('bottomBlocks');
      });

    });

  });

  describe('_saveSquareToPlayer', function () {

    describe('adds the given square to proper player (square owner) under the proper index (square level)', function () {

      it('for square level = 3', function () {
        var squareFactory = new SquareFactory();
        var player = {
          squaresByLevel: {
            2: [],
            3: [],
            4: []
          }
        };
        var square = {
          level: 3,
          player: player
        };
        squareFactory._saveSquareToPlayer(square);
        expect(player.squaresByLevel['3']).toEqual([square]);
      });

      it('for square level = 4, with other squares already added', function () {
        var squareFactory = new SquareFactory();
        var player = {
          squaresByLevel: {
            2: [jasmine.createSpy('square1'), jasmine.createSpy('square2')],
            3: [jasmine.createSpy('square3'), jasmine.createSpy('square4'), jasmine.createSpy('square5')],
            4: [jasmine.createSpy('square6')]
          }
        };
        var square = {
          level: 4,
          player: player
        };
        squareFactory._saveSquareToPlayer(square);
        expect(player.squaresByLevel['2'].length).toBe(2);
        expect(player.squaresByLevel['3'].length).toBe(3);
        expect(player.squaresByLevel['4'].length).toBe(2);
        expect(player.squaresByLevel['4']).toContain(square);
      });

    });

  });

  describe('_removeSquareFromBlocks', function () {

    var square;

    beforeEach(function () {
      square = {
        blocks: [
          jasmine.createSpyObj('block-1', ['squarePos', 'square']),
          jasmine.createSpyObj('block-1', ['squarePos', 'square']),
          jasmine.createSpyObj('block-1', ['squarePos', 'square'])
        ]
      };
    });

    it('sets square blocks "square" prop to null', function () {
      var squareFactory = new SquareFactory();
      square.blocks.forEach(function (block) {
        expect(block.square).not.toBe(null);
      });
      squareFactory._removeSquareFromBlocks(square);
      square.blocks.forEach(function (block) {
        expect(block.square).toBe(null);
      });
    });

    it('sets square blocks "squarePos" prop to "none"', function () {
      var squareFactory = new SquareFactory();
      square.blocks.forEach(function (block) {
        expect(block.squarePos).not.toBe('none');
      });
      squareFactory._removeSquareFromBlocks(square);
      square.blocks.forEach(function (block) {
        expect(block.squarePos).toBe('none');
      });
    });

  });

  describe('_removeSquareFromPlayer', function () {

    describe('removes the given square from proper player (square owner), ' +
             'from a proper index (square level)', function () {

      it('for square level = 3', function () {
        var squareFactory = new SquareFactory();
        var square = {
          level: 3
        };
        var player = {
          squaresByLevel: {
            2: [square],
            3: [square],
            4: [square]
          }
        };
        square.player = player;
        squareFactory._removeSquareFromPlayer(square);
        expect(player.squaresByLevel['2']).toEqual([square]);
        expect(player.squaresByLevel['3']).toEqual([]);
        expect(player.squaresByLevel['4']).toEqual([square]);
      });

      it('for square level = 2, with other squares already added', function () {
        var squareFactory = new SquareFactory();
        var square = {
          level: 2
        };
        var fakeSquare1 = jasmine.createSpy('square-1');
        var fakeSquare2 = jasmine.createSpy('square-2');
        var fakeSquare3 = jasmine.createSpy('square-3');
        var fakeSquare4 = jasmine.createSpy('square-4');
        var fakeSquare5 = jasmine.createSpy('square-5');
        var player = {
          squaresByLevel: {
            2: [fakeSquare1, square, fakeSquare2],
            3: [],
            4: [square, fakeSquare3, fakeSquare4],
            5: [fakeSquare5]
          }
        };
        square.player = player;
        squareFactory._removeSquareFromPlayer(square);
        expect(player.squaresByLevel['2']).toEqual([fakeSquare1, fakeSquare2]);
        expect(player.squaresByLevel['3']).toEqual([]);
        expect(player.squaresByLevel['4']).toEqual([square, fakeSquare3, fakeSquare4]);
        expect(player.squaresByLevel['5']).toEqual([fakeSquare5]);
      });

    });

  });

  describe('createSquare', function () {

    var squareFactory;

    beforeEach(function () {
      squareFactory = new SquareFactory();
      spyOn(mapProvider, 'getBlocks');
      spyOn(mapProvider, 'normalizeCoords').and.callFake(function (regionX, regionY, blockX, blockY) {
        return {
          regionX: regionX,
          regionY: regionY,
          blockX: blockX,
          blockY: blockY
        };
      });
    });

    it('returns a new square instance', function () {
      var square = squareFactory.createSquare();
      expect(square instanceof Square).toBeTrue();
    });

    it('returns a new square instance based on given arguments', function () {
      var regionX = jasmine.createSpy('regionX');
      var regionY = jasmine.createSpy('regionY');
      var blockX = jasmine.createSpy('blockX');
      var blockY = jasmine.createSpy('blockY');
      var level = jasmine.createSpy('level');
      var player = jasmine.createSpy('player');
      var square = squareFactory.createSquare(regionX, regionY, blockX, blockY, level, player);
      expect(square.regionX).toBe(regionX);
      expect(square.regionY).toBe(regionY);
      expect(square.x).toBe(blockX);
      expect(square.y).toBe(blockY);
      expect(square.level).toBe(level);
      expect(square.player).toBe(player);
    });

    it('calls normalizeCoords on map to set proper coords', function () {
      var regionX = jasmine.createSpy('regionX');
      var regionY = jasmine.createSpy('regionY');
      var blockX = jasmine.createSpy('blockX');
      var blockY = jasmine.createSpy('blockY');
      squareFactory.createSquare(regionX, regionY, blockX, blockY);
      expect(mapProvider.normalizeCoords).toHaveBeenCalledWith(
        regionX, regionY, blockX, blockY);
    });

    it('calls getBlocks on map to setup proper blocks for the square', function () {
      var regionX = jasmine.createSpy('regionX');
      var regionY = jasmine.createSpy('regionY');
      var blockX = jasmine.createSpy('blockX');
      var blockY = jasmine.createSpy('blockY');
      var properBlocks = jasmine.createSpy('blocks');
      mapProvider.getBlocks.and.returnValue(properBlocks);
      var square = squareFactory.createSquare(regionX, regionY, blockX, blockY);
      expect(mapProvider.getBlocks).toHaveBeenCalledWith(
        regionX, regionY, blockX, blockY, jasmine.any(Number), jasmine.any(Number));
      expect(square.blocks).toBe(properBlocks);
    });

  });

  describe('saveSquare', function () {

    var squareFactory;
    var fakeSquare;
    var fakeSquaresMap;
    var fakeStorage;

    beforeEach(function () {
      fakeSquare = jasmine.createSpyObj('square',
        ['calcGoldOutcomeTotal', 'calcResourceOutcomeTotal', 'goldIncomeInternal',
         'player', 'calcGoldIncomeInternal']);
      squareFactory = new SquareFactory();
      spyOn(squareFactory, '_saveSquareToBlocks');
      spyOn(squareFactory, '_saveSquareToPlayer');
      fakeStorage = {
        changeGoldIncome: jasmine.createSpy('storage.changeGoldIncome')
      };
      storageProvider.getStorage = jasmine.createSpy('getStorage')
        .and.returnValue(fakeStorage);
      fakeSquaresMap = jasmine.createSpyObj('squaresMap',['addSquare']);
      squareFactory.setSquaresMap(fakeSquaresMap);
    });

    it('returns given square', function () {
      var result = squareFactory.saveSquare(fakeSquare);
      expect(result).toBe(fakeSquare);
    });

    it('does not call calcGoldOutcomeTotal on square', function () {
      squareFactory.saveSquare(fakeSquare);
      expect(fakeSquare.calcGoldOutcomeTotal).not.toHaveBeenCalled();
    });

    it('does not call calcResourceOutcomeTotal on square', function () {
      squareFactory.saveSquare(fakeSquare);
      expect(fakeSquare.calcResourceOutcomeTotal).not.toHaveBeenCalled();
    });

    it('calls calcGoldIncomeInternal', function () {
      squareFactory.saveSquare(fakeSquare);
      expect(fakeSquare.calcGoldIncomeInternal).toHaveBeenCalledWith();
    });

    it('calls _saveSquareToBlocks', function () {
      squareFactory.saveSquare(fakeSquare);
      expect(squareFactory._saveSquareToBlocks).toHaveBeenCalledWith(fakeSquare);
    });

    it('calls _saveSquareToPlayer', function () {
      squareFactory.saveSquare(fakeSquare);
      expect(squareFactory._saveSquareToPlayer).toHaveBeenCalledWith(fakeSquare);
    });

    it('calls changeGoldIncome on storage', function () {
      squareFactory.saveSquare(fakeSquare);
      expect(storageProvider.getStorage).toHaveBeenCalledWith(fakeSquare.player);
      expect(fakeStorage.changeGoldIncome).toHaveBeenCalledWith(fakeSquare.goldIncomeInternal);
    });

    it('calls addSquare on squaresMap', function () {
      squareFactory.saveSquare(fakeSquare);
      expect(fakeSquaresMap.addSquare).toHaveBeenCalledWith(fakeSquare);
    });

  });

  describe('deleteSquare', function () {

    var squareFactory;
    var fakeSquaresMap;
    var fakeStorage;

    beforeEach(function () {
      squareFactory = new SquareFactory();
      spyOn(squareFactory, '_removeSquareFromBlocks');
      spyOn(squareFactory, '_removeSquareFromPlayer');
      fakeStorage = {
        changeGoldIncome: jasmine.createSpy('storage.changeGoldIncome')
      };
      storageProvider.getStorage = jasmine.createSpy('getStorage')
        .and.returnValue(fakeStorage);
      fakeSquaresMap = jasmine.createSpyObj('squaresMap',['removeSquare']);
      squareFactory.setSquaresMap(fakeSquaresMap);
    });

    it('calls _removeSquareFromBlocks', function () {
      var square = jasmine.createSpy('square');
      squareFactory.deleteSquare(square);
      expect(squareFactory._removeSquareFromBlocks).toHaveBeenCalledWith(square);
    });

    it('calls _removeSquareFromPlayer', function () {
      var square = jasmine.createSpy('square');
      squareFactory.deleteSquare(square);
      expect(squareFactory._removeSquareFromPlayer).toHaveBeenCalledWith(square);
    });

    it('calls changeGoldIncome on storage', function () {
      var player = jasmine.createSpy('player');
      var square = {
        goldIncomeInternal: 5,
        player: player
      };
      squareFactory.deleteSquare(square);
      expect(storageProvider.getStorage).toHaveBeenCalledWith(player);
      expect(fakeStorage.changeGoldIncome).toHaveBeenCalledWith(-5);
    });

    it('calls removeSquare on squaresMap', function () {
      var square = jasmine.createSpy('square');
      squareFactory.deleteSquare(square);
      expect(fakeSquaresMap.removeSquare).toHaveBeenCalledWith(square);
    });

  });

});