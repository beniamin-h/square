'use strict';

describe('SquaresMapSchema', function() {
  beforeEach(module('square'));

  var SquaresMapSchema, SquaresMap, SquareFactory, Square,
    HumanPlayer, AIPlayer, Joi;

  beforeEach(inject(function (_SquaresMapSchema_, _SquaresMap_, _SquareFactory_, _Square_,
                              _HumanPlayer_, _AIPlayer_, _Joi_) {
    SquaresMapSchema = _SquaresMapSchema_;
    SquaresMap = _SquaresMap_;
    SquareFactory = _SquareFactory_;
    Square = _Square_;

    HumanPlayer = _HumanPlayer_;
    AIPlayer = _AIPlayer_;
    Joi = _Joi_;
  }));

  describe('a new instance', function () {

    it('validates against the SquaresMap schema', function (done) {
      var squaresMap = new SquaresMap(new SquareFactory());
      squaresMap.squareFactory.setSquaresMap(squaresMap);
      Joi.validate(squaresMap, SquaresMapSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

    it('with players set validates against the SquaresMap schema', function (done) {
      var squaresMap = new SquaresMap(new SquareFactory());
      squaresMap.squareFactory.setSquaresMap(squaresMap);
      var player = new HumanPlayer('fake-id', {x: 1, y: 0});
      var anotherPlayer = new AIPlayer('ai-fake-id', {x: 1, y: 0});
      squaresMap.setPlayers({ 'fake-id': player, 'ai-fake-id': anotherPlayer });
      Joi.validate(squaresMap, SquaresMapSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

    it('with few squares added validates against the SquaresMap schema', function (done) {
      var squaresMap = new SquaresMap(new SquareFactory());
      squaresMap.squareFactory.setSquaresMap(squaresMap);
      var player = new HumanPlayer('fake-id', {x: 1, y: 0});
      squaresMap.setPlayers({ 'fake-id': player });
      squaresMap.addSquare(new Square(1, 2, 3, 4, [], 2, player));
      squaresMap.addSquare(new Square(0, 0, 3, 4, [], 3, player));
      squaresMap.addSquare(new Square(-1, -2, 0, 1, [], 5, player));
      Joi.validate(squaresMap, SquaresMapSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

  });

});