'use strict';

var _RegExp = require('nwglobal').RegExp;

angular.module('square')

.constant('buildingClassRegExp', new _RegExp(/^\w+$/))
.constant('squareLevelRegExp', new _RegExp(/^\d+$/))

.factory('SquareSchema', ['Joi', 'Square', 'PlayerSchema', 'BlockSchema', 'Building',
                          'buildingClassRegExp', 'StorageResourcesSchema',
    function (Joi, Square, PlayerSchema, BlockSchema, Building,
              buildingClassRegExp, StorageResourcesSchema) {

  return Joi.object().keys({
    x: Joi.number().integer(),
    y: Joi.number().integer(),
    regionX: Joi.number().integer(),
    regionY: Joi.number().integer(),
    blocks: Joi.array().items(
      BlockSchema
    ),
    level: Joi.number().positive(),
    player: PlayerSchema,
    buildingsByClass: Joi.object().pattern(
      buildingClassRegExp, Joi.array().items(
        Joi.object().type(Building)
      )
    ),
    buildings: Joi.array().items(
      Joi.object().type(Building)
    ),
    blockResourceProduction: StorageResourcesSchema,
    blockResourceUpkeep: StorageResourcesSchema,
    blockResourceOutcome: StorageResourcesSchema,
    buildingResourceProduction: StorageResourcesSchema,
    buildingResourceUpkeep: StorageResourcesSchema,
    buildingResourceOutcome: StorageResourcesSchema,
    resourceOutcomeTotal: StorageResourcesSchema,
    goldIncomeInternal: Joi.number().integer(),
    goldIncomeBlocks: Joi.number().integer(),
    goldIncomeBuildings: Joi.number().integer(),
    goldUpkeepBuildings: Joi.number().integer(),
    goldOutcomeTotal: Joi.number().integer()
  }).type(Square);

}]);