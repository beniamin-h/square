'use strict';

angular.module('square')

.factory('SquareFactorySchema', ['Joi', 'SquaresMap', 'SquareFactory',
    function (Joi, SquaresMap, SquareFactory) {

  return Joi.object().keys({
    squaresMap: Joi.object().type(SquaresMap)
  }).type(SquareFactory);

}]);