'use strict';

angular.module('square')

.factory('SquaresMapSchema', ['Joi', 'SquaresMap', 'SquareFactorySchema',
                              'playerIdRegExp', 'squareLevelRegExp', 'coordRegExp', 'SquareSchema',
    function (Joi, SquaresMap, SquareFactorySchema,
              playerIdRegExp, squareLevelRegExp, coordRegExp, SquareSchema) {

  return Joi.object().keys({
    squareFactory: SquareFactorySchema,
    squares: Joi.object({})
      .pattern(playerIdRegExp, Joi.object({})
        .pattern(squareLevelRegExp, Joi.object({})
          .pattern(coordRegExp, Joi.object({})
            .pattern(coordRegExp, Joi.object({})
              .pattern(coordRegExp, Joi.object({})
                .pattern(coordRegExp, SquareSchema)
              )
            )
          )
        )
      )
  }).type(SquaresMap);

}]);