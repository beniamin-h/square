'use strict';

describe('squareProvider', function() {
  beforeEach(module('square'));

  var squareProvider, SquareFactory, SquaresMap;
  var mockedPlayers;
  var mockedSquareFactory;
  var mockedSquaresMap;

  beforeEach(inject(function (_squareProvider_, _SquareFactory_, _SquaresMap_) {
    squareProvider = _squareProvider_;
    SquareFactory = _SquareFactory_;
    SquaresMap = _SquaresMap_;

    spyOn(squareProvider, 'initSquareProvider');
    spyOn(squareProvider, '_initSquareFactory');
    spyOn(squareProvider, '_initSquaresMap');
    spyOn(squareProvider, 'loadSquareFactory');
    spyOn(squareProvider, 'loadSquaresMap');
    spyOn(squareProvider, 'getSquareFactory');
    spyOn(squareProvider, 'getSquaresMap');
    mockedSquareFactory = jasmine.createSpyObj('squareFactory', ['setSquaresMap']);
    mockedSquaresMap = jasmine.createSpy('squaresMap');
    squareProvider._instance.squareFactory = mockedSquareFactory;
    squareProvider._instance.squaresMap = mockedSquaresMap;
    mockedPlayers = jasmine.createSpy('players');
    spyOn(SquaresMap.prototype, 'setPlayers');
  }));

  describe('initSquareProvider', function () {

    beforeEach(function () {
      squareProvider.initSquareProvider.and.callThrough();
    });

    it('calls _initSquareFactory and _initSquaresMap', function () {
      squareProvider.initSquareProvider(mockedPlayers);
      expect(squareProvider._initSquareFactory).toHaveBeenCalledWith();
      expect(squareProvider._initSquaresMap).toHaveBeenCalledWith(mockedPlayers);
    });

  });

  describe('_initSquareFactory', function () {

    beforeEach(function () {
      squareProvider._initSquareFactory.and.callThrough();
    });

    it('creates a new instance of SquareFactory and sets it to squareFactory prop', function () {
      squareProvider._initSquareFactory();
      expect(squareProvider._instance.squareFactory instanceof SquareFactory).toBeTrue();
    });

    it('returns squareFactory', function () {
      var result = squareProvider._initSquareFactory();
      expect(result).toBe(squareProvider._instance.squareFactory);
    });

  });

  describe('_initSquaresMap', function () {

    beforeEach(function () {
      squareProvider._initSquaresMap.and.callThrough();
    });

    it('creates a new instance of SquaresMap and sets it to squaresMap prop', function () {
      squareProvider._initSquaresMap();
      expect(squareProvider._instance.squaresMap instanceof SquaresMap).toBeTrue();
    });

    it('returns squaresMap', function () {
      var result = squareProvider._initSquaresMap();
      expect(result).toBe(squareProvider._instance.squaresMap);
    });

    it('calls setPlayers on the new squaresMap', function () {
      squareProvider._initSquaresMap(mockedPlayers);
      expect(SquaresMap.prototype.setPlayers).toHaveBeenCalledWith(mockedPlayers);
    });

    it('calls setSquaresMap on squareFactory', function () {
      squareProvider._initSquaresMap(mockedPlayers);
      expect(squareProvider._instance.squareFactory.setSquaresMap).toHaveBeenCalledWith(
        squareProvider._instance.squaresMap);
    });

  });

  describe('loadSquareFactory', function () {

    beforeEach(function () {
      squareProvider.loadSquareFactory.and.callThrough();
    });

    it('sets the given squareFactory to `squareFactory` prop', function () {
      var newSquareFactory = jasmine.createSpy('squareFactory');
      squareProvider.loadSquareFactory(newSquareFactory);
      expect(squareProvider._instance.squareFactory).toBe(newSquareFactory);
    });

  });

  describe('loadSquaresMap', function () {

    beforeEach(function () {
      squareProvider.loadSquaresMap.and.callThrough();
    });

    it('sets the given squaresMap to `squaresMap` prop', function () {
      var newSquaresMap = jasmine.createSpy('squaresMap');
      squareProvider.loadSquaresMap(newSquaresMap);
      expect(squareProvider._instance.squaresMap).toBe(newSquaresMap);
    });

  });

  describe('getSquareFactory', function () {

    beforeEach(function () {
      squareProvider.getSquareFactory.and.callThrough();
    });

    it('returns squareFactory prop', function () {
      var result = squareProvider.getSquareFactory();
      expect(result).toBe(mockedSquareFactory);
    });

  });

  describe('getSquaresMap', function () {

    beforeEach(function () {
      squareProvider.getSquaresMap.and.callThrough();
    });

    it('returns squaresMap prop', function () {
      var result = squareProvider.getSquaresMap();
      expect(result).toBe(mockedSquaresMap);
    });

  });

});

