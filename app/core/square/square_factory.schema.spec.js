'use strict';

describe('SquareFactorySchema', function() {
  beforeEach(module('square'));

  var SquareFactorySchema, SquaresMap, SquareFactory, Square,
    HumanPlayer, mapProvider, Joi, playerProvider,
    Block, Region, storageProvider, Mountain;

  beforeEach(inject(function (_SquareFactorySchema_, _SquaresMap_, _SquareFactory_, _Square_,
                              _HumanPlayer_, _mapProvider_, _Joi_, _playerProvider_,
                              _Block_, _Region_, _storageProvider_, _Mountain_) {
    SquareFactorySchema = _SquareFactorySchema_;
    SquaresMap = _SquaresMap_;
    SquareFactory = _SquareFactory_;
    Square = _Square_;

    HumanPlayer = _HumanPlayer_;
    mapProvider = _mapProvider_;
    Joi = _Joi_;
    playerProvider = _playerProvider_;

    Block = _Block_;
    Region = _Region_;
    storageProvider = _storageProvider_;
    Mountain = _Mountain_;
  }));

  describe('a new instance', function () {

    it('validates against the SquareFactory schema', function (done) {
      var squareFactory = new SquareFactory();
      squareFactory.setSquaresMap(new SquaresMap(squareFactory));
      Joi.validate(squareFactory, SquareFactorySchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

    it('with few squares added validates against the SquareFactory schema', function (done) {
      spyOn(mapProvider, 'getBlock').and.returnValue(new Block(0, 1, new Region(10, -1), new Mountain()));
      spyOn(mapProvider, 'getBlocks').and.returnValue([new Block(2, 0, new Region(0, 1), new Mountain())]);
      spyOn(mapProvider, 'normalizeCoords').and.callFake(function (regionX, regionY, blockX, blockY) {
        return {
          regionX: regionX,
          regionY: regionY,
          blockX: blockX,
          blockY: blockY
        };
      });
      var squareFactory = new SquareFactory();
      squareFactory.setSquaresMap(new SquaresMap(squareFactory));
      var player = playerProvider.addHuman();
      squareFactory.squaresMap.setPlayers(playerProvider.getAllPlayers());
      storageProvider.init(playerProvider.getAllPlayers());
      squareFactory.saveSquare(new Square(1, 2, 3, 4, [], 2, player));
      squareFactory.saveSquare(new Square(0, -1, 8, 8, [], 5, player));
      Joi.validate(squareFactory, SquareFactorySchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

  });

});
