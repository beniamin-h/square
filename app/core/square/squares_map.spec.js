'use strict';

describe('SquaresMap', function() {
  beforeEach(module('square'));

  var SquaresMap, mapProvider, mapDirections, Square;

  beforeEach(inject(function (
      _SquaresMap_, _mapProvider_, _mapDirections_, _Square_) {
    SquaresMap = _SquaresMap_;
    mapProvider = _mapProvider_;
    mapDirections = _mapDirections_;
    Square = _Square_;
  }));

  describe('setPlayers', function () {

    it('sets given player ids as `squares` prop keys', function () {
      var squaresMap = new SquaresMap();
      var fakePlayer = jasmine.createSpy('fakePlayer');
      squaresMap.setPlayers({
        'fake-player-id': fakePlayer
      });
      expect(squaresMap.squares['fake-player-id']).toEqual({});
    });

    it('calls setSquaresMap for each AI player', function () {
      var squaresMap = new SquaresMap();
      var fakePlayer = jasmine.createSpy('fakePlayer');
      var fakeAiPlayer = jasmine.createSpyObj('fakeAiPlayer', ['ai']);
      var anotherFakeAiPlayer = jasmine.createSpyObj('anotherFakeAiPlayer', ['ai']);
      fakeAiPlayer.ai.setSquaresMap = jasmine.createSpy('setSquaresMap1');
      anotherFakeAiPlayer.ai.setSquaresMap = jasmine.createSpy('setSquaresMap2');
      squaresMap.setPlayers({
        'fake-player-id': fakePlayer,
        'fake-ai-player-id': fakeAiPlayer,
        'another-fake-ai-player-id': anotherFakeAiPlayer
      });
      expect(fakeAiPlayer.ai.setSquaresMap).toHaveBeenCalledWith(squaresMap);
      expect(anotherFakeAiPlayer.ai.setSquaresMap).toHaveBeenCalledWith(squaresMap);
    });

  });

  describe('addSquare', function () {

    describe('adds the given square under the right index', function () {

      var squaresMap;
      var fakeHumanPlayer;
      var fakeAiPlayer;

      beforeEach(function () {
        fakeHumanPlayer = {
          playerId: 'human-fake-id'
        };
        fakeAiPlayer = {
          playerId: 'ai-fake-id'
        };
        squaresMap = new SquaresMap();
        squaresMap.squares['human-fake-id'] = {};
        squaresMap.squares['ai-fake-id'] = {};
      });

      it('for fake human square level 3 in region 5x-1 in block 4x2', function () {
        var fakeSquare = {
          player: fakeHumanPlayer,
          level: 3,
          regionX: 5,
          regionY: -1,
          x: 4,
          y: 2
        };
        squaresMap.addSquare(fakeSquare);
        expect(squaresMap.squares['human-fake-id']['3']['-1']['5']['2']['4']).toBe(fakeSquare);
        expect(Object.keys(squaresMap.squares['human-fake-id']).length).toBe(1);
        expect(Object.keys(squaresMap.squares['human-fake-id']['3']).length).toBe(1);
        expect(Object.keys(squaresMap.squares['human-fake-id']['3']['-1']).length).toBe(1);
        expect(Object.keys(squaresMap.squares['human-fake-id']['3']['-1']['5']).length).toBe(1);
        expect(Object.keys(squaresMap.squares['human-fake-id']['3']['-1']['5']['2']).length).toBe(1);
      });

      it('for fake AI player square level 5 in region -1x0 in block 0x55', function () {
        var fakeSquare = {
          player: fakeAiPlayer,
          level: 5,
          regionX: -1,
          regionY: 0,
          x: 0,
          y: 55
        };
        squaresMap.addSquare(fakeSquare);
        expect(squaresMap.squares['ai-fake-id']['5']['0']['-1']['55']['0']).toBe(fakeSquare);
        expect(Object.keys(squaresMap.squares['ai-fake-id']).length).toBe(1);
        expect(Object.keys(squaresMap.squares['ai-fake-id']['5']).length).toBe(1);
        expect(Object.keys(squaresMap.squares['ai-fake-id']['5']['0']).length).toBe(1);
        expect(Object.keys(squaresMap.squares['ai-fake-id']['5']['0']['-1']).length).toBe(1);
        expect(Object.keys(squaresMap.squares['ai-fake-id']['5']['0']['-1']['55']).length).toBe(1);
      });

      it('when there are already other squares', function () {
        var fakeSquare = {
          player: fakeHumanPlayer,
          level: 2,
          regionX: 0,
          regionY: 11,
          x: 2,
          y: 3
        };
        var anotherFakeSquare = jasmine.createSpy('anotherFakeSquare');
        squaresMap.squares['human-fake-id'] = {
          2: { 11: { 0: { 3: { 4: anotherFakeSquare } } } }
        };
        squaresMap.squares['ai-fake-id'] = {
          2: { 11: { 0: { 3: { 3: anotherFakeSquare } } } }
        };
        expect(squaresMap.squares['ai-fake-id']['2']['11']['0']['3']['3']).toBe(anotherFakeSquare);
        expect(squaresMap.squares['human-fake-id']['2']['11']['0']['3']['4']).toBe(anotherFakeSquare);
        squaresMap.addSquare(fakeSquare);
        expect(squaresMap.squares['ai-fake-id']['2']['11']['0']['3']['3']).toBe(anotherFakeSquare);
        expect(squaresMap.squares['human-fake-id']['2']['11']['0']['3']['4']).toBe(anotherFakeSquare);
        expect(squaresMap.squares['human-fake-id']['2']['11']['0']['3']['2']).toBe(fakeSquare);
      });

    });

  });

  describe('removeSquare', function () {

    describe('removes a square from squaresMap.squares', function () {

      var squaresMap;
      var fakeHumanPlayer;
      var fakeAiPlayer;
      var fakeSquare;
      var anotherAdjacentFakeSquare;
      var anotherLevelFakeSquare;
      var anotherPlayerFakeSquare;

      beforeEach(function () {
        fakeHumanPlayer = {
          playerId: 'human-fake-id'
        };
        fakeAiPlayer = {
          playerId: 'ai-fake-id'
        };
        fakeSquare = {
          player: fakeHumanPlayer,
          level: 3, regionX: -8, regionY: -2, x: 0, y: 0
        };
        anotherAdjacentFakeSquare = {
          player: fakeHumanPlayer,
          level: 3, regionX: -8, regionY: -2, x: 0, y: 2
        };
        anotherLevelFakeSquare = {
          player: fakeHumanPlayer,
          level: 4, regionX: 0, regionY: 1, x: 10, y: 1
        };
        anotherPlayerFakeSquare = {
          player: fakeAiPlayer,
          level: 3, regionX: -8, regionY: -2, x: 0, y: 0
        };
        squaresMap = new SquaresMap();
      });

      it('for fake human square level 3 in region -8x-2 in block 0x0', function () {
        squaresMap.squares['human-fake-id'] = {
          '3': { '-2': { '-8': { '0': {
            '0': fakeSquare
          } } } }
        };
        expect(squaresMap.squares['human-fake-id']['3']['-2']['-8']['0']['0']).toBe(fakeSquare);
        squaresMap.removeSquare(fakeSquare);
        expect(squaresMap.squares['human-fake-id']['3']['-2']['-8']['0']['0']).not.toBe(fakeSquare);
        expect(squaresMap.squares['human-fake-id']['3']['-2']['-8']['0']).toEqual({});
      });

      it('for fake human square level 4 in region 0x1 in block 10x1', function () {
        squaresMap.squares['human-fake-id'] = {
          '4': { '1': { '0': { '1': {
            '10': anotherLevelFakeSquare
          } } } }
        };
        expect(squaresMap.squares['human-fake-id']['4']['1']['0']['1']['10']).toBe(anotherLevelFakeSquare);
        squaresMap.removeSquare(anotherLevelFakeSquare);
        expect(squaresMap.squares['human-fake-id']['4']['1']['0']['1']['10']).not.toBe(anotherLevelFakeSquare);
        expect(squaresMap.squares['human-fake-id']['4']['1']['0']['1']).toEqual({});
      });

      it('for fake ai square level 3 in region -8x-2 in block 0x0', function () {
        squaresMap.squares['ai-fake-id'] = {
          '3': { '-2': { '-8': { '0': {
            '0': anotherPlayerFakeSquare
          } } } }
        };
        expect(squaresMap.squares['ai-fake-id']['3']['-2']['-8']['0']['0']).toBe(anotherPlayerFakeSquare);
        squaresMap.removeSquare(anotherPlayerFakeSquare);
        expect(squaresMap.squares['ai-fake-id']['3']['-2']['-8']['0']['0']).not.toBe(anotherPlayerFakeSquare);
        expect(squaresMap.squares['ai-fake-id']['3']['-2']['-8']['0']).toEqual({});
      });

      it('when there are already other squares', function () {
        squaresMap.squares['human-fake-id'] = {
          '3': { '-2': { '-8': { '0': {
            '0': fakeSquare,
            '2': anotherAdjacentFakeSquare
          } } } },
          '4': { '1': { '0': { '1': {
            '10': anotherLevelFakeSquare
          } } } }
        };
        squaresMap.squares['ai-fake-id'] = {
          '3': { '-2': { '-8': { '0': {
            '0': anotherPlayerFakeSquare
          } } } }
        };
        expect(squaresMap.squares['human-fake-id']['3']['-2']['-8']['0']['0']).toBe(fakeSquare);
        expect(squaresMap.squares['human-fake-id']['3']['-2']['-8']['0']['2']).toBe(anotherAdjacentFakeSquare);
        expect(squaresMap.squares['human-fake-id']['4']['1']['0']['1']['10']).toBe(anotherLevelFakeSquare);
        expect(squaresMap.squares['ai-fake-id']['3']['-2']['-8']['0']['0']).toBe(anotherPlayerFakeSquare);
        squaresMap.removeSquare(fakeSquare);
        expect(squaresMap.squares['human-fake-id']['3']['-2']['-8']['0']['2']).toBe(anotherAdjacentFakeSquare);
        expect(squaresMap.squares['human-fake-id']['4']['1']['0']['1']['10']).toBe(anotherLevelFakeSquare);
        expect(squaresMap.squares['ai-fake-id']['3']['-2']['-8']['0']['0']).toBe(anotherPlayerFakeSquare);
        expect(squaresMap.squares['human-fake-id']['3']['-2']['-8']['0']['0']).not.toBe(fakeSquare);
        expect(Object.keys(squaresMap.squares['human-fake-id']['3']['-2']['-8']['0'])).toEqual(['2']);
      });

    });

  });

  describe('getSquare', function () {

    var squaresMap;
    var fakeHumanPlayer;
    var fakeAiPlayer;

    beforeEach(function () {
      squaresMap = new SquaresMap();
      spyOn(mapProvider, 'normalizeCoords')
        .and.callFake(function (regionX, regionY, blockX, blockY) {
          return {
            regionX: regionX,
            regionY: regionY,
            blockX: blockX,
            blockY: blockY
          };
        });
      fakeHumanPlayer = {
        playerId: 'fake-human-player-id'
      };
      fakeAiPlayer = {
        playerId: 'fake-ai-player-id'
      };
    });

    describe('returns a proper square', function () {

      it('for human player, level: 3, region: 5x-5 and block: 0x2', function () {
        var fakeSquare = {
          player: fakeHumanPlayer,
          level: 3, regionX: 5, regionY: -5, x: 0, y: 2
        };
        squaresMap.squares['fake-human-player-id'] = {
          '3': { '-5': { '5': { '2': { '0': fakeSquare } } } }
        };
        var result = squaresMap.getSquare(fakeHumanPlayer, 3, 5, -5, 0, 2);
        expect(result).toBe(fakeSquare);
      });

    });

    describe('returns null', function () {

      it('if there are no squares', function () {
        squaresMap.squares['fake-ai-player-id'] = {};
        squaresMap.squares['fake-human-player-id'] = {};
        var result = squaresMap.getSquare(fakeAiPlayer, 3, 5, -5, 0, 2);
        expect(result).toBe(null);
      });

      it('for non existing square', function () {
        var fakeSquare = {
          player: fakeHumanPlayer,
          level: 3, regionX: 5, regionY: -5, x: 0, y: 2
        };
        squaresMap.squares['fake-ai-player-id'] = {};
        squaresMap.squares['fake-human-player-id'] = {
          '3': { '-5': { '5': { '2': { '0': fakeSquare } } } }
        };
        var invalidBlockXResult = squaresMap.getSquare(fakeHumanPlayer, 3, 5, -5, 0, 1);
        expect(invalidBlockXResult).toBe(null);
        var invalidBlockYResult = squaresMap.getSquare(fakeHumanPlayer, 3, 5, -5, -1, 2);
        expect(invalidBlockYResult).toBe(null);
        var invalidRegionXResult = squaresMap.getSquare(fakeHumanPlayer, 3, 5, 0, 0, 2);
        expect(invalidRegionXResult).toBe(null);
        var invalidRegionYResult = squaresMap.getSquare(fakeHumanPlayer, 3, -5, -5, 0, 2);
        expect(invalidRegionYResult).toBe(null);
        var invalidPlayerResult = squaresMap.getSquare(fakeAiPlayer, 3, 5, -5, 0, 2);
        expect(invalidPlayerResult).toBe(null);
      });

      it('if square has been removed', function () {
        squaresMap.squares['fake-human-player-id'] = {
          '3': { '-5': { '5': { '2': {  } } } }
        };
        var result = squaresMap.getSquare(fakeHumanPlayer, 3, 5, -5, 0, 2);
        expect(result).toBe(null);
      });

    });

    describe('calls normalizeCoords to get proper coords', function () {

      it('with given params', function () {
        squaresMap.squares['fake-human-player-id'] = {};
        squaresMap.getSquare(fakeHumanPlayer, 8, -5, 4, 20, 2);
        expect(mapProvider.normalizeCoords).toHaveBeenCalledWith(-5, 4, 20, 2);
      });

      it('with spied params', function () {
        squaresMap.squares['fake-human-player-id'] = {};
        var fakeLevel = jasmine.createSpy('fakeLevel');
        var fakeRegionX = jasmine.createSpy('fakeRegionX');
        var fakeRegionY = jasmine.createSpy('fakeRegionY');
        var fakeBlockX = jasmine.createSpy('fakeBlockX');
        var fakeBlockY = jasmine.createSpy('fakeBlockY');
        squaresMap.getSquare(fakeHumanPlayer, fakeLevel,
          fakeRegionX, fakeRegionY, fakeBlockX, fakeBlockY);
        expect(mapProvider.normalizeCoords).toHaveBeenCalledWith(fakeRegionX, fakeRegionY, fakeBlockX, fakeBlockY);
      });

    });

  });

  describe('checkUpperLevelSquare', function () {

    var squaresMap;
    var fakeSquare;
    var fakeCheckSquaresResult;

    beforeEach(function () {
      squaresMap = new SquaresMap();
      fakeCheckSquaresResult = jasmine.createSpy('fakeCheckSquaresResult');
      spyOn(squaresMap, 'checkSquares').and.returnValue(fakeCheckSquaresResult);
      fakeSquare = jasmine.createSpyObj('fakeSquare', ['level']);
    });

    it('returns false if the given square level ' +
       'is equal to squaresMap maxSquareLevel', function () {
      fakeSquare.level = 8;
      squaresMap.maxSquareLevel = 8;
      var result = squaresMap.checkUpperLevelSquare(fakeSquare);
      expect(result).toBeFalse();
    });

    it('returns false if the given square level ' +
       'is greater than squaresMap maxSquareLevel', function () {
      fakeSquare.level = 7;
      squaresMap.maxSquareLevel = 6;
      var result = squaresMap.checkUpperLevelSquare(fakeSquare);
      expect(result).toBeFalse();
    });

    it('calls squaresMap checkSquares if the given square level ' +
       'is lesser than squaresMap maxSquareLevel', function () {
      fakeSquare.level = 5;
      squaresMap.maxSquareLevel = 10;
      squaresMap.checkUpperLevelSquare(fakeSquare);
      expect(squaresMap.checkSquares).toHaveBeenCalledWith(fakeSquare);
    });

    it('returns squaresMap checkSquares result if the given square level ' +
       'is lesser than squaresMap maxSquareLevel', function () {
      fakeSquare.level = 8;
      squaresMap.maxSquareLevel = 9;
      var result = squaresMap.checkUpperLevelSquare(fakeSquare);
      expect(result).toBe(fakeCheckSquaresResult);
    });

  });

  describe('_getDirectionsWithSquare', function () {

    var squaresMap;
    var square;

    beforeEach(function () {
      squaresMap = new SquaresMap();
      spyOn(squaresMap, 'getSquare');
      square = jasmine.createSpyObj('square',
        ['player', 'level', 'regionX', 'regionY', 'getWidth', 'getHeight']);
      square.getWidth.and.returnValue(2);
      square.getHeight.and.returnValue(20);
      square.x = 5;
      square.y = 11;
    });

    it('returns all 8 directions if there are squares in each direction', function () {
      squaresMap.getSquare.and.returnValue({});
      var result = squaresMap._getDirectionsWithSquare(square);
      mapDirections.directions8.forEach(function (direction) {
        expect(result.directions.has(direction)).toBeTrue();
      });
      expect(result.directions.size).toBe(8);
    });

    it('returns only directions with squares', function () {
      squaresMap.getSquare.and.callFake(function (player, level, regionX, regionY, blockX, blockY) {
        return blockX === 5 || blockY === 11;
      });
      var result = squaresMap._getDirectionsWithSquare(square);
      expect(result.directions.has('up')).toBeTrue();
      expect(result.directions.has('right')).toBeTrue();
      expect(result.directions.has('down')).toBeTrue();
      expect(result.directions.has('left')).toBeTrue();
      expect(result.directions.size).toBe(4);
    });

    it('returns adjacent squares map containing only directions with a square', function () {
      squaresMap.getSquare.and.callFake(function (player, level, regionX, regionY, blockX, blockY) {
        if (blockY === 11) {
          return blockX + 'x' + blockY;
        }
        return null;
      });
      var result = squaresMap._getDirectionsWithSquare(square);
      expect(result.adjacentSquaresMap).toEqual({
        'left': '3x11',
        'right': '7x11'
      });
    });

    it('calls getSquare 8 times', function () {
      squaresMap._getDirectionsWithSquare(square);
      expect(squaresMap.getSquare.calls.count()).toBe(8);
      expect(squaresMap.getSquare.calls.allArgs()).toEqual([
        [square.player, square.level, square.regionX, square.regionY, 5, 11 - 20],
        [square.player, square.level, square.regionX, square.regionY, 5 + 2, 11 - 20],
        [square.player, square.level, square.regionX, square.regionY, 5 + 2, 11],
        [square.player, square.level, square.regionX, square.regionY, 5 + 2, 11 + 20],
        [square.player, square.level, square.regionX, square.regionY, 5, 11 + 20],
        [square.player, square.level, square.regionX, square.regionY, 5 - 2, 11 + 20],
        [square.player, square.level, square.regionX, square.regionY, 5 - 2, 11],
        [square.player, square.level, square.regionX, square.regionY, 5 - 2, 11 - 20]
      ]);
    });

  });

  describe('_allDirectionsHaveSquare', function () {

    var squaresMap;

    beforeEach(function () {
      squaresMap = new SquaresMap();
    });

    it('returns true if all given directions to check have a square', function () {
      var directionsToCheck = ['fakeDirection_1', 'fakeDirection_888'];
      var directionsWithSquare = new Set(['fakeDirection_1', 'fakeDirection_2', 'fakeDirection_888']);
      var result = squaresMap._allDirectionsHaveSquare(directionsToCheck, directionsWithSquare);
      expect(result).toBeTrue();
    });

    it('returns false if not all given directions have square', function () {
      var directionsToCheck = ['fakeDirection_1', 'fakeDirection_3', 'fakeDirection_888'];
      var directionsWithSquare = new Set(['fakeDirection_1', 'fakeDirection_2', 'fakeDirection_888']);
      var result = squaresMap._allDirectionsHaveSquare(directionsToCheck, directionsWithSquare);
      expect(result).toBeFalse();
    });

  });

  describe('_prepareSquaresForNewGreaterSquare', function () {

    var squaresMap;
    var squareFactory;

    beforeEach(function () {
      squareFactory = jasmine.createSpyObj('squareFactory', ['deleteSquare']);
      squaresMap = new SquaresMap(squareFactory);
    });

    it('calls deleteSquare on squareFactory for all adjacent squares ' +
       'in given directions', function () {
      var fakeSquare_1 = jasmine.createSpy('fakeSquare_1');
      var fakeSquare_2 = jasmine.createSpy('fakeSquare_2');
      var fakeSquare_3 = jasmine.createSpy('fakeSquare_3');
      squaresMap._prepareSquaresForNewGreaterSquare(
        jasmine.createSpy('oldSquare'),
        ['up', 'fakeDirection'],
        { 'up': fakeSquare_1, 'down': fakeSquare_2, 'fakeDirection': fakeSquare_3 }
      );
      expect(squareFactory.deleteSquare.calls.argsFor(0)).toEqual([fakeSquare_1]);
      expect(squareFactory.deleteSquare.calls.argsFor(1)).toEqual([fakeSquare_3]);
      expect(squareFactory.deleteSquare.calls.allArgs()).not.toContain([fakeSquare_2]);
    });

    it('calls deleteSquare on squareFactory for given old square', function () {
      var fakeOldSquare = jasmine.createSpy('oldSquare');
      squaresMap._prepareSquaresForNewGreaterSquare(
        fakeOldSquare,
        [],
        {}
      );
      expect(squareFactory.deleteSquare.calls.count()).toBe(1);
      expect(squareFactory.deleteSquare.calls.argsFor(0)).toEqual([fakeOldSquare]);
    });

  });

  describe('_getNewSquareCoordsShift', function () {

    var squaresMap;

    beforeEach(function () {
      squaresMap = new SquaresMap();
    });

    it('returns { x: 0, y: 0 } if given directionsSetIndex = 0', function () {
      var result = squaresMap._getNewSquareCoordsShift(0);
      expect(result).toEqual({ x: 0, y: 0 });
    });

    it('returns { x: -1, y: 0 } if given directionsSetIndex = 1', function () {
      var result = squaresMap._getNewSquareCoordsShift(1);
      expect(result).toEqual({ x: -1, y: 0 });
    });

    it('returns { x: -1, y: -1 } if given directionsSetIndex = 2', function () {
      var result = squaresMap._getNewSquareCoordsShift(2);
      expect(result).toEqual({ x: -1, y: -1 });
    });

    it('returns { x: 0, y: -1 } if given directionsSetIndex = 3', function () {
      var result = squaresMap._getNewSquareCoordsShift(3);
      expect(result).toEqual({ x: 0, y: -1 });
    });

  });

  describe('_createNewGreaterSquare', function () {

    var squaresMap;
    var squareFactory;
    var square;
    var directionsSetIndexSpy;

    beforeEach(function () {
      square = jasmine.createSpyObj('square', [
        'regionX', 'regionY', 'getWidth', 'getHeight', 'player']);
      square.x = 8;
      square.y = 4;
      square.level = 3;
      square.getWidth.and.returnValue(6);
      square.getHeight.and.returnValue(7);
      squareFactory = jasmine.createSpyObj('squareFactory', ['createSquare']);
      squaresMap = new SquaresMap(squareFactory);
      spyOn(squaresMap, '_getNewSquareCoordsShift').and.returnValue({
        x: 3,
        y: 2
      });
      directionsSetIndexSpy = jasmine.createSpy('directionsSetIndex');
    });

    it('calls _getNewSquareCoordsShift', function () {
      squaresMap._createNewGreaterSquare(square, directionsSetIndexSpy);
      expect(squaresMap._getNewSquareCoordsShift).toHaveBeenCalledWith(directionsSetIndexSpy);
    });

    it('calls createSquare on square factory', function () {
      squaresMap._createNewGreaterSquare(square, directionsSetIndexSpy);
      expect(squareFactory.createSquare).toHaveBeenCalledWith(
        square.regionX, square.regionY, 8 + 6 * 3, 4 + 7 * 2, 3 + 1, square.player);
    });

    it('returns createSquare result', function () {
      var createSquareResultSpy = jasmine.createSpy('createSquareResult');
      squareFactory.createSquare.and.returnValue(createSquareResultSpy);
      var result = squaresMap._createNewGreaterSquare(square, directionsSetIndexSpy);
      expect(result).toBe(createSquareResultSpy);
    });

  });

  describe('checkSquares', function () {

    var squaresMap;
    var squareFactory;
    var square;
    var directionsWithSquareSpy;
    var adjacentSquaresMapSpy;
    var newGreaterSquareSpy;

    beforeEach(function () {
      square = jasmine.createSpy('square');
      squareFactory = jasmine.createSpyObj('squareFactory', ['saveSquare']);
      squaresMap = new SquaresMap(squareFactory);
      directionsWithSquareSpy = jasmine.createSpy('directionsWithSquare');
      adjacentSquaresMapSpy = jasmine.createSpy('adjacentSquaresMap');
      newGreaterSquareSpy = jasmine.createSpy('newGreaterSquare');
      spyOn(squaresMap, '_getDirectionsWithSquare').and.returnValue({
        directions: directionsWithSquareSpy,
        adjacentSquaresMap: adjacentSquaresMapSpy
      });
      spyOn(squaresMap, '_allDirectionsHaveSquare').and.returnValue(true);
      spyOn(squaresMap, '_prepareSquaresForNewGreaterSquare');
      spyOn(squaresMap, '_createNewGreaterSquare').and.returnValue(newGreaterSquareSpy);
      spyOn(squaresMap, 'checkUpperLevelSquare');
    });

    it('calls _getDirectionsWithSquare', function () {
      squaresMap.checkSquares(square);
      expect(squaresMap._getDirectionsWithSquare).toHaveBeenCalledWith(square);
    });

    it('calls _allDirectionsHaveSquare for all square making dir sets ' +
       'if none of them have a square in all directions', function () {
      squaresMap._allDirectionsHaveSquare.and.returnValue(false);
      squaresMap.checkSquares(square);
      expect(squaresMap._allDirectionsHaveSquare.calls.allArgs()).toEqual([
        [squaresMap.squareMakingDirectionsSets[0], directionsWithSquareSpy],
        [squaresMap.squareMakingDirectionsSets[1], directionsWithSquareSpy],
        [squaresMap.squareMakingDirectionsSets[2], directionsWithSquareSpy],
        [squaresMap.squareMakingDirectionsSets[3], directionsWithSquareSpy]
      ]);
    });

    it('calls _prepareSquaresForNewGreaterSquare only if all directions have square', function () {
      squaresMap._allDirectionsHaveSquare.and.callFake(function (directions) {
        return directions === squaresMap.squareMakingDirectionsSets[2];
      });
      squaresMap.checkSquares(square);
      expect(squaresMap._prepareSquaresForNewGreaterSquare).toHaveBeenCalledWith(
        square, squaresMap.squareMakingDirectionsSets[2], adjacentSquaresMapSpy
      );
    });

    it('calls _prepareSquaresForNewGreaterSquare only once', function () {
      squaresMap._allDirectionsHaveSquare.and.callFake(function (directions) {
        return directions === squaresMap.squareMakingDirectionsSets[2] ||
          directions === squaresMap.squareMakingDirectionsSets[3];
      });
      squaresMap.checkSquares(square);
      expect(squaresMap._prepareSquaresForNewGreaterSquare.calls.count()).toBe(1);
    });

    it('calls _createNewGreaterSquare', function () {
      squaresMap._allDirectionsHaveSquare.and.callFake(function (directions) {
        return directions === squaresMap.squareMakingDirectionsSets[3];
      });
      squaresMap.checkSquares(square);
      expect(squaresMap._createNewGreaterSquare).toHaveBeenCalledWith(square, 3);
    });

    it('calls saveSquare on squareFactory', function () {
      squaresMap.checkSquares(square);
      expect(squareFactory.saveSquare).toHaveBeenCalledWith(newGreaterSquareSpy);
    });

    it('calls checkUpperLevelSquare', function () {
      squaresMap.checkSquares(square);
      expect(squaresMap.checkUpperLevelSquare).toHaveBeenCalledWith(newGreaterSquareSpy);
    });

    it('returns new greater square if checkUpperLevelSquare returns false', function () {
      squaresMap.checkUpperLevelSquare.and.returnValue(false);
      var result = squaresMap.checkSquares(square);
      expect(result).toBe(newGreaterSquareSpy);
    });

    it('returns checkUpperLevelSquare result if it is not falsy', function () {
      var checkUpperLevelSquareResult = jasmine.createSpy('checkUpperLevelSquareResult');
      squaresMap.checkUpperLevelSquare.and.returnValue(checkUpperLevelSquareResult);
      var result = squaresMap.checkSquares(square);
      expect(result).toBe(checkUpperLevelSquareResult);
    });

    it('returns false if none of square making direction sets ' +
       'have a square in all directions', function () {
      squaresMap._allDirectionsHaveSquare.and.returnValue(false);
      var result = squaresMap.checkSquares(square);
      expect(result).toBeFalse();
    });

  });

  describe('_checkSquareMakingNeighbors', function () {

    var squaresMap;
    var neighborWithSquare;
    var fakePlayer;
    var anotherFakePlayer;
    var neighborOfAnotherPlayer;
    var neighborWithInvalidLevel;
    var properNeighbor;
    var anotherProperNeighbor;

    beforeEach(function () {
      squaresMap = new SquaresMap();
      fakePlayer = jasmine.createSpy('fakePlayer');
      anotherFakePlayer = jasmine.createSpy('anotherFakePlayer');
      neighborWithSquare = {
        square: jasmine.createSpy('square'),
        level: 1,
        ownedBy: fakePlayer
      };
      neighborOfAnotherPlayer = {
        level: 1,
        ownedBy: anotherFakePlayer
      };
      neighborWithInvalidLevel = {
        level: 2,
        ownedBy: fakePlayer
      };
      properNeighbor = {
        level: 1,
        ownedBy: fakePlayer
      };
      anotherProperNeighbor = {
        level: 1,
        ownedBy: fakePlayer
      };
    });

    it('returns true if all square-making neighbors are properly improved', function () {
      var squareMakingDirections = ['fakeDirection', 'anotherDirection'];
      var neighborsByPosition = {
        fakeDirection: properNeighbor,
        anotherDirection: anotherProperNeighbor,
        irrelevantDirection: neighborWithSquare
      };
      var result = squaresMap._checkSquareMakingNeighbors(squareMakingDirections, neighborsByPosition, fakePlayer);
      expect(result).toBeTrue();
    });

    it('returns false if there are no neighbors', function () {
      var squareMakingDirections = ['fakeDirection', 'anotherDirection'];
      var neighborsByPosition = {};
      var result = squaresMap._checkSquareMakingNeighbors(squareMakingDirections, neighborsByPosition, fakePlayer);
      expect(result).toBeFalse();
    });

    it('returns false if at least one square-making neighbor has a square', function () {
      var squareMakingDirections = ['fakeDirection', 'anotherDirection'];
      var neighborsByPosition = {
        fakeDirection: neighborWithSquare,
        anotherDirection: properNeighbor,
        irrelevantDirection: anotherProperNeighbor
      };
      var result = squaresMap._checkSquareMakingNeighbors(squareMakingDirections, neighborsByPosition, fakePlayer);
      expect(result).toBeFalse();
    });

    it('returns false if at least one square-making neighbor belongs to another player', function () {
      var squareMakingDirections = ['fakeDirection', 'anotherDirection'];
      var neighborsByPosition = {
        fakeDirection: neighborOfAnotherPlayer,
        anotherDirection: properNeighbor,
        irrelevantDirection: anotherProperNeighbor
      };
      var result = squaresMap._checkSquareMakingNeighbors(squareMakingDirections, neighborsByPosition, fakePlayer);
      expect(result).toBeFalse();
    });

    it('returns false if at least one square-making neighbor has level greater than 1', function () {
      var squareMakingDirections = ['fakeDirection', 'anotherDirection'];
      var neighborsByPosition = {
        fakeDirection: neighborWithInvalidLevel,
        anotherDirection: properNeighbor,
        irrelevantDirection: anotherProperNeighbor
      };
      var result = squaresMap._checkSquareMakingNeighbors(squareMakingDirections, neighborsByPosition, fakePlayer);
      expect(result).toBeFalse();
    });

    it('returns false if all square-making neighbors are invalid', function () {
      var squareMakingDirections = ['fakeDirection', 'anotherDirection', 'fakeDirection_2'];
      var neighborsByPosition = {
        fakeDirection: neighborWithInvalidLevel,
        anotherDirection: neighborOfAnotherPlayer,
        fakeDirection_2: neighborWithSquare,
        irrelevantDirection: neighborOfAnotherPlayer
      };
      var result = squaresMap._checkSquareMakingNeighbors(squareMakingDirections, neighborsByPosition, fakePlayer);
      expect(result).toBeFalse();
    });

  });

  describe('_createNewSmallestSquare', function () {

    var squareFactory;
    var squaresMap;
    var selectedBlock;
    var playerSpy;
    var directionsSetIndexSpy;

    beforeEach(function () {
      squareFactory = jasmine.createSpyObj('squareFactory', ['createSquare']);
      squaresMap = new SquaresMap(squareFactory);
      playerSpy = jasmine.createSpy('player');
      selectedBlock = {
        region: jasmine.createSpyObj('region', ['x', 'y']),
        x: 5,
        y: 2,
        player: playerSpy
      };
      directionsSetIndexSpy = jasmine.createSpy('directionsSetIndexSpy');
      spyOn(squaresMap, '_getNewSquareCoordsShift').and.returnValue({
        x: 4,
        y: -3
      });
    });

    it('calls _getNewSquareCoordsShift', function () {
      squaresMap._createNewSmallestSquare(selectedBlock, directionsSetIndexSpy, playerSpy);
      expect(squaresMap._getNewSquareCoordsShift).toHaveBeenCalledWith(directionsSetIndexSpy);
    });

    it('calls createSquare on square factory', function () {
      squaresMap._createNewSmallestSquare(selectedBlock, directionsSetIndexSpy, playerSpy);
      expect(squareFactory.createSquare).toHaveBeenCalledWith(
        selectedBlock.region.x, selectedBlock.region.y,
        5 + 4, 2 - 3, 2, playerSpy);
    });

    it('returns createSquare result', function () {
      var createSquareResult = jasmine.createSpy('createSquareResult');
      squareFactory.createSquare.and.returnValue(createSquareResult);
      var result = squaresMap._createNewSmallestSquare(selectedBlock, directionsSetIndexSpy, playerSpy);
      expect(result).toBe(createSquareResult);
    });

  });

  describe('checkSmallestSquare', function () {

    var squareFactory;
    var squaresMap;
    var selectedBlock;
    var playerSpy;
    var newSquareSpy;
    var upperLevelSquare;

    beforeEach(function () {
      squareFactory = jasmine.createSpyObj('squareFactory',
        ['saveSquare']);
      squaresMap = new SquaresMap(squareFactory);
      playerSpy = jasmine.createSpy('player');
      selectedBlock = jasmine.createSpyObj('selectedBlock',
        ['neighborsByPosition', 'level']);
      newSquareSpy = jasmine.createSpyObj('newSquare',
        ['level', 'calcGoldAndResourceOutcomes']);
      spyOn(squaresMap, '_checkSquareMakingNeighbors');
      spyOn(squaresMap, '_createNewSmallestSquare').and.returnValue(newSquareSpy);
      spyOn(squaresMap, 'checkUpperLevelSquare');
      upperLevelSquare = jasmine.createSpyObj('upperLevelSquare',
        ['level', 'calcGoldAndResourceOutcomes']);
    });

    it('calls _checkSquareMakingNeighbors 4 times if it always returns false', function () {
      squaresMap._checkSquareMakingNeighbors.and.returnValue(false);
      squaresMap.checkSmallestSquare(selectedBlock, playerSpy);
      expect(squaresMap._checkSquareMakingNeighbors.calls.allArgs()).toEqual([
        [squaresMap.squareMakingDirectionsSets[0], selectedBlock.neighborsByPosition, playerSpy],
        [squaresMap.squareMakingDirectionsSets[1], selectedBlock.neighborsByPosition, playerSpy],
        [squaresMap.squareMakingDirectionsSets[2], selectedBlock.neighborsByPosition, playerSpy],
        [squaresMap.squareMakingDirectionsSets[3], selectedBlock.neighborsByPosition, playerSpy]
      ]);
    });

    it('calls _createNewSmallestSquare once when _checkSquareMakingNeighbors returns true', function () {
      squaresMap._checkSquareMakingNeighbors.and.callFake(function (directions) {
        return directions === squaresMap.squareMakingDirectionsSets[1];
      });
      squaresMap.checkSmallestSquare(selectedBlock, playerSpy);
      expect(squaresMap._createNewSmallestSquare).toHaveBeenCalledWith(selectedBlock, 1, playerSpy);
      expect(squaresMap._createNewSmallestSquare.calls.count()).toBe(1);
    });

    it('calls saveSquare on squareFactory once when _checkSquareMakingNeighbors returns true', function () {
      squaresMap._checkSquareMakingNeighbors.and.callFake(function (directions) {
        return directions === squaresMap.squareMakingDirectionsSets[0];
      });
      squaresMap.checkSmallestSquare(selectedBlock, playerSpy);
      expect(squareFactory.saveSquare).toHaveBeenCalledWith(newSquareSpy);
      expect(squareFactory.saveSquare.calls.count()).toBe(1);
    });

    it('calls calcGoldAndResourceOutcomes on newSquare when _checkSquareMakingNeighbors returns true', function () {
      squaresMap._checkSquareMakingNeighbors.and.callFake(function (directions) {
        return directions === squaresMap.squareMakingDirectionsSets[2];
      });
      squaresMap.checkSmallestSquare(selectedBlock, playerSpy);
      expect(newSquareSpy.calcGoldAndResourceOutcomes).toHaveBeenCalledWith();
    });

    it('calls checkUpperLevelSquare once when _checkSquareMakingNeighbors returns true', function () {
      squaresMap._checkSquareMakingNeighbors.and.callFake(function (directions) {
        return directions === squaresMap.squareMakingDirectionsSets[3];
      });
      squaresMap.checkSmallestSquare(selectedBlock, playerSpy);
      expect(squaresMap.checkUpperLevelSquare).toHaveBeenCalledWith(newSquareSpy);
      expect(squaresMap.checkUpperLevelSquare.calls.count()).toBe(1);
    });

    it('calls notifyOfNewSquare on aiSquaring once when _checkSquareMakingNeighbors returns true ' +
       'and the given player has an AI', function () {
      var player = jasmine.createSpyObj('player', ['ai']);
      player.ai.aiSquaring = jasmine.createSpyObj('aiSquaring', ['notifyOfNewSquare']);
      squaresMap._checkSquareMakingNeighbors.and.callFake(function (directions) {
        return directions === squaresMap.squareMakingDirectionsSets[2];
      });
      squaresMap.checkSmallestSquare(selectedBlock, player);
      expect(player.ai.aiSquaring.notifyOfNewSquare).toHaveBeenCalledWith(newSquareSpy);
      expect(player.ai.aiSquaring.notifyOfNewSquare.calls.count()).toBe(1);
    });

    it('returns upperLevelSquare when _checkSquareMakingNeighbors returns true ' +
       'and checkUpperLevelSquare does not return false', function () {
      squaresMap._checkSquareMakingNeighbors.and.callFake(function (directions) {
        return directions === squaresMap.squareMakingDirectionsSets[1];
      });
      squaresMap.checkUpperLevelSquare.and.returnValue(upperLevelSquare);
      var result = squaresMap.checkSmallestSquare(selectedBlock, playerSpy);
      expect(result).toBe(upperLevelSquare);
    });

    it('returns a new square if _checkSquareMakingNeighbors returns true ' +
       'and checkUpperLevelSquare returns false', function () {
      squaresMap._checkSquareMakingNeighbors.and.callFake(function (directions) {
        return directions === squaresMap.squareMakingDirectionsSets[0];
      });
      squaresMap.checkUpperLevelSquare.and.returnValue(false);
      var result = squaresMap.checkSmallestSquare(selectedBlock, playerSpy);
      expect(result).toBe(newSquareSpy);
    });

    it('returns false if _checkSquareMakingNeighbors always returns false', function () {
      squaresMap._checkSquareMakingNeighbors.and.returnValue(false);
      var result = squaresMap.checkSmallestSquare(selectedBlock, playerSpy);
      expect(result).toBeFalse();
    });

    it('sets selectedBlock level to newSquare level ' +
       'if checkUpperLevelSquare returns false and ' +
       '_checkSquareMakingNeighbors returns true', function () {
      squaresMap._checkSquareMakingNeighbors.and.returnValue(true);
      squaresMap.checkUpperLevelSquare.and.returnValue(false);
      expect(selectedBlock.level).not.toBe(newSquareSpy.level);
      squaresMap.checkSmallestSquare(selectedBlock, playerSpy);
      expect(selectedBlock.level).toBe(newSquareSpy.level);
    });

    it('sets selectedBlock level to upperLevelSquare level ' +
       'if checkUpperLevelSquare returns upperLevelSquare and ' +
       '_checkSquareMakingNeighbors returns true', function () {
      squaresMap._checkSquareMakingNeighbors.and.returnValue(true);
      squaresMap.checkUpperLevelSquare.and.returnValue(upperLevelSquare);
      expect(selectedBlock.level).not.toBe(upperLevelSquare.level);
      squaresMap.checkSmallestSquare(selectedBlock, playerSpy);
      expect(selectedBlock.level).not.toBe(newSquareSpy.level);
      expect(selectedBlock.level).toBe(upperLevelSquare.level);
    });

  });

  describe('checkSmallestSquareWithAdditionalBlocks', function () {

    var squaresMap;
    var selectedBlock;
    var player;
    var additionalBlocks;

    beforeEach(function () {
      squaresMap = new SquaresMap();
      player = jasmine.createSpy('player');
      selectedBlock = {
        neighborsByPosition: {}
      };
      additionalBlocks = [];
    });

    function createFakeNeighbor(level, player, square) {
      return {
        level: level,
        ownedBy: player,
        square: square
      };
    }

    it('returns an array of adjacent blocks: right, rightDown, down and -1 ' +
       'if they are properly improved', function () {
      selectedBlock.neighborsByPosition.right = createFakeNeighbor(1, player, null);
      selectedBlock.neighborsByPosition.rightDown = createFakeNeighbor(1, player, null);
      selectedBlock.neighborsByPosition.down = createFakeNeighbor(1, player, null);
      var result = squaresMap.checkSmallestSquareWithAdditionalBlocks(
        selectedBlock, player, additionalBlocks);
      expect(result).toEqual([
        selectedBlock.neighborsByPosition.right,
        selectedBlock.neighborsByPosition.rightDown,
        selectedBlock.neighborsByPosition.down,
        -1
      ]);
    });

    it('returns an array of adjacent blocks: left, down, leftDown and 0 ' +
       'if they are properly improved', function () {
      selectedBlock.neighborsByPosition.left = createFakeNeighbor(1, player, null);
      selectedBlock.neighborsByPosition.down = createFakeNeighbor(1, player, null);
      selectedBlock.neighborsByPosition.leftDown = createFakeNeighbor(1, player, null);
      var result = squaresMap.checkSmallestSquareWithAdditionalBlocks(
        selectedBlock, player, additionalBlocks);
      expect(result).toEqual([
        selectedBlock.neighborsByPosition.left,
        selectedBlock.neighborsByPosition.down,
        selectedBlock.neighborsByPosition.leftDown,
        0
      ]);
    });

    it('returns an array of adjacent blocks: leftUp, up, left and 0 ' +
       'if they are properly improved', function () {
      selectedBlock.neighborsByPosition.leftUp = createFakeNeighbor(1, player, null);
      selectedBlock.neighborsByPosition.up = createFakeNeighbor(1, player, null);
      selectedBlock.neighborsByPosition.left = createFakeNeighbor(1, player, null);
      var result = squaresMap.checkSmallestSquareWithAdditionalBlocks(
        selectedBlock, player, additionalBlocks);
      expect(result).toEqual([
        selectedBlock.neighborsByPosition.leftUp,
        selectedBlock.neighborsByPosition.up,
        selectedBlock.neighborsByPosition.left,
        0
      ]);
    });

    it('returns an array of adjacent blocks: up, rightUp, right and 0 ' +
       'if they are properly improved', function () {
      selectedBlock.neighborsByPosition.up = createFakeNeighbor(1, player, null);
      selectedBlock.neighborsByPosition.rightUp = createFakeNeighbor(1, player, null);
      selectedBlock.neighborsByPosition.right = createFakeNeighbor(1, player, null);
      var result = squaresMap.checkSmallestSquareWithAdditionalBlocks(
        selectedBlock, player, additionalBlocks);
      expect(result).toEqual([
        selectedBlock.neighborsByPosition.up,
        selectedBlock.neighborsByPosition.rightUp,
        selectedBlock.neighborsByPosition.right,
        0
      ]);
    });


    it('returns an array of adjacent blocks: right, rightDown, down and -1 ' +
       'if some of they are properly improved and the rest is passed as additionalBlocks', function () {
      selectedBlock.neighborsByPosition.right = createFakeNeighbor(1, player, null);
      selectedBlock.neighborsByPosition.rightDown = createFakeNeighbor(0, null, null);
      selectedBlock.neighborsByPosition.down = createFakeNeighbor(1, player, null);
      additionalBlocks.push(selectedBlock.neighborsByPosition.rightDown);
      var result = squaresMap.checkSmallestSquareWithAdditionalBlocks(
        selectedBlock, player, additionalBlocks);
      expect(result).toEqual([
        selectedBlock.neighborsByPosition.right,
        selectedBlock.neighborsByPosition.rightDown,
        selectedBlock.neighborsByPosition.down,
        -1
      ]);
    });

    it('returns an array of adjacent blocks: left, down, leftDown and 0 ' +
       'if some of they are properly improved and the rest is passed as additionalBlocks', function () {
      selectedBlock.neighborsByPosition.left = createFakeNeighbor(1, player, null);
      selectedBlock.neighborsByPosition.down = createFakeNeighbor(0, null, null);
      selectedBlock.neighborsByPosition.leftDown = createFakeNeighbor(0, null, null);
      additionalBlocks.push(selectedBlock.neighborsByPosition.down);
      additionalBlocks.push(selectedBlock.neighborsByPosition.leftDown);
      var result = squaresMap.checkSmallestSquareWithAdditionalBlocks(
        selectedBlock, player, additionalBlocks);
      expect(result).toEqual([
        selectedBlock.neighborsByPosition.left,
        selectedBlock.neighborsByPosition.down,
        selectedBlock.neighborsByPosition.leftDown,
        0
      ]);
    });

    it('returns an array of adjacent blocks: leftUp, up, left and 0 ' +
       'if some of they are properly improved and the rest is passed as additionalBlocks', function () {
      selectedBlock.neighborsByPosition.leftUp = createFakeNeighbor(1, {}, {});
      selectedBlock.neighborsByPosition.up = createFakeNeighbor(0, null, null);
      selectedBlock.neighborsByPosition.left = createFakeNeighbor(1, player, null);
      additionalBlocks.push(selectedBlock.neighborsByPosition.leftUp);
      additionalBlocks.push(selectedBlock.neighborsByPosition.up);
      var result = squaresMap.checkSmallestSquareWithAdditionalBlocks(
        selectedBlock, player, additionalBlocks);
      expect(result).toEqual([
        selectedBlock.neighborsByPosition.leftUp,
        selectedBlock.neighborsByPosition.up,
        selectedBlock.neighborsByPosition.left,
        0
      ]);
    });

    it('returns an array of adjacent blocks: up, rightUp, right and 0 ' +
       'if some of they are properly improved and the rest is passed as additionalBlocks', function () {
      selectedBlock.neighborsByPosition.up = createFakeNeighbor(1, {}, {});
      selectedBlock.neighborsByPosition.rightUp = createFakeNeighbor(1, player, null);
      selectedBlock.neighborsByPosition.right = createFakeNeighbor(1, player, null);
      additionalBlocks.push(selectedBlock.neighborsByPosition.up);
      var result = squaresMap.checkSmallestSquareWithAdditionalBlocks(
        selectedBlock, player, additionalBlocks);
      expect(result).toEqual([
        selectedBlock.neighborsByPosition.up,
        selectedBlock.neighborsByPosition.rightUp,
        selectedBlock.neighborsByPosition.right,
        0
      ]);
    });


    it('returns an array of adjacent blocks: right, rightDown, down and -1 ' +
       'if all of them are passed as additionalBlocks', function () {
      selectedBlock.neighborsByPosition.right = createFakeNeighbor(0, null, null);
      selectedBlock.neighborsByPosition.rightDown = createFakeNeighbor(0, null, null);
      selectedBlock.neighborsByPosition.down = createFakeNeighbor(0, null, null);
      additionalBlocks.push(selectedBlock.neighborsByPosition.right);
      additionalBlocks.push(selectedBlock.neighborsByPosition.rightDown);
      additionalBlocks.push(selectedBlock.neighborsByPosition.down);
      var result = squaresMap.checkSmallestSquareWithAdditionalBlocks(
        selectedBlock, player, additionalBlocks);
      expect(result).toEqual([
        selectedBlock.neighborsByPosition.right,
        selectedBlock.neighborsByPosition.rightDown,
        selectedBlock.neighborsByPosition.down,
        -1
      ]);
    });

    it('returns an array of adjacent blocks: left, down, leftDown and 0 ' +
       'if all of them are passed as additionalBlocks', function () {
      selectedBlock.neighborsByPosition.left = createFakeNeighbor(0, null, null);
      selectedBlock.neighborsByPosition.down = createFakeNeighbor(0, null, null);
      selectedBlock.neighborsByPosition.leftDown = createFakeNeighbor(0, null, null);
      additionalBlocks.push(selectedBlock.neighborsByPosition.down);
      additionalBlocks.push(selectedBlock.neighborsByPosition.leftDown);
      additionalBlocks.push(selectedBlock.neighborsByPosition.left);
      var result = squaresMap.checkSmallestSquareWithAdditionalBlocks(
        selectedBlock, player, additionalBlocks);
      expect(result).toEqual([
        selectedBlock.neighborsByPosition.left,
        selectedBlock.neighborsByPosition.down,
        selectedBlock.neighborsByPosition.leftDown,
        0
      ]);
    });

    it('returns an array of adjacent blocks: leftUp, up, left and 0 ' +
       'if all of them are passed as additionalBlocks', function () {
      selectedBlock.neighborsByPosition.leftUp = createFakeNeighbor(0, null, null);
      selectedBlock.neighborsByPosition.up = createFakeNeighbor(0, null, null);
      selectedBlock.neighborsByPosition.left = createFakeNeighbor(0, null, null);
      additionalBlocks.push(selectedBlock.neighborsByPosition.leftUp);
      additionalBlocks.push(selectedBlock.neighborsByPosition.left);
      additionalBlocks.push(selectedBlock.neighborsByPosition.up);
      var result = squaresMap.checkSmallestSquareWithAdditionalBlocks(
        selectedBlock, player, additionalBlocks);
      expect(result).toEqual([
        selectedBlock.neighborsByPosition.leftUp,
        selectedBlock.neighborsByPosition.up,
        selectedBlock.neighborsByPosition.left,
        0
      ]);
    });

    it('returns an array of adjacent blocks: up, rightUp, right and 0 ' +
       'if all of them are passed as additionalBlocks', function () {
      selectedBlock.neighborsByPosition.up = createFakeNeighbor(0, null, null);
      selectedBlock.neighborsByPosition.rightUp = createFakeNeighbor(0, null, null);
      selectedBlock.neighborsByPosition.right = createFakeNeighbor(0, null, null);
      additionalBlocks.push(selectedBlock.neighborsByPosition.up);
      additionalBlocks.push(selectedBlock.neighborsByPosition.rightUp);
      additionalBlocks.push(selectedBlock.neighborsByPosition.right);
      var result = squaresMap.checkSmallestSquareWithAdditionalBlocks(
        selectedBlock, player, additionalBlocks);
      expect(result).toEqual([
        selectedBlock.neighborsByPosition.up,
        selectedBlock.neighborsByPosition.rightUp,
        selectedBlock.neighborsByPosition.right,
        0
      ]);
    });


    it('returns false if not all square-making adjacent blocks are properly improved #1', function () {
      selectedBlock.neighborsByPosition.up = createFakeNeighbor(0, null, null);
      selectedBlock.neighborsByPosition.rightUp = createFakeNeighbor(1, player, null);
      selectedBlock.neighborsByPosition.right = createFakeNeighbor(1, player, null);
      var result = squaresMap.checkSmallestSquareWithAdditionalBlocks(
        selectedBlock, player, additionalBlocks);
      expect(result).toBeFalse();
    });

    it('returns false if not all square-making adjacent blocks are properly improved #2', function () {
      selectedBlock.neighborsByPosition.leftUp = createFakeNeighbor(0, null, null);
      selectedBlock.neighborsByPosition.up = createFakeNeighbor(1, {}, null);
      selectedBlock.neighborsByPosition.left = createFakeNeighbor(0, null, null);
      var result = squaresMap.checkSmallestSquareWithAdditionalBlocks(
        selectedBlock, player, additionalBlocks);
      expect(result).toBeFalse();
    });

    it('returns false if not all square-making adjacent blocks are properly improved #3', function () {
      selectedBlock.neighborsByPosition.left = createFakeNeighbor(1, player, null);
      selectedBlock.neighborsByPosition.down = createFakeNeighbor(1, player, null);
      selectedBlock.neighborsByPosition.leftDown = createFakeNeighbor(1, player, {});
      var result = squaresMap.checkSmallestSquareWithAdditionalBlocks(
        selectedBlock, player, additionalBlocks);
      expect(result).toBeFalse();
    });

    it('returns false if not all square-making adjacent blocks are properly improved #4', function () {
      selectedBlock.neighborsByPosition.right = createFakeNeighbor(0, null, null);
      selectedBlock.neighborsByPosition.rightDown = createFakeNeighbor(0, null, null);
      selectedBlock.neighborsByPosition.down = createFakeNeighbor(0, null, null);
      var result = squaresMap.checkSmallestSquareWithAdditionalBlocks(
        selectedBlock, player, additionalBlocks);
      expect(result).toBeFalse();
    });

  });

  describe('removeBlockFromSquare', function () {

    var squaresMap;
    var fakeBlock;

    beforeEach(function () {
      squaresMap = new SquaresMap();
      fakeBlock = jasmine.createSpyObj('block', ['square']);
      spyOn(squaresMap, '_deconstructSquare');
      squaresMap.squareFactory = jasmine.createSpyObj('squareFactory',
        ['deleteSquare']);
    });

    it('calls deleteSquare on squareFactory', function () {
      squaresMap.removeBlockFromSquare(fakeBlock);
      expect(squaresMap.squareFactory.deleteSquare)
        .toHaveBeenCalledWith(fakeBlock.square);
    });

    it('calls _deconstructSquare properly', function () {
      var square = fakeBlock.square;
      squaresMap.squareFactory.deleteSquare.and.callFake(function () {
        fakeBlock.square = null;
      });
      squaresMap.removeBlockFromSquare(fakeBlock);
      expect(squaresMap._deconstructSquare).toHaveBeenCalledWith(
        square, fakeBlock);
    });

  });

  describe('_deconstructSquare', function () {

    var squaresMap;
    var fakeSquare;
    var fakeSubtractedBlock;
    var _deconstructSquare;
    var _deconstructSquareCallCounter;

    function fakeDeconstructSquare() {
      if (_deconstructSquareCallCounter++ === 0) {
        _deconstructSquare.apply(squaresMap, Array.from(arguments));
      }
    }

    beforeEach(function () {
      squaresMap = new SquaresMap();
      spyOn(squaresMap, '_deconstructSmallestSquare');
      spyOn(squaresMap, '_splitSquare').and.returnValue([]);
      spyOn(squaresMap, '_saveSmallerSquare');
      _deconstructSquare = squaresMap._deconstructSquare;
      _deconstructSquareCallCounter = 0;
      spyOn(squaresMap, '_deconstructSquare').and.callFake(fakeDeconstructSquare);
      fakeSquare = jasmine.createSpyObj('square', ['level']);
      fakeSubtractedBlock = jasmine.createSpy('block');
    });

    it('calls _deconstructSmallestSquare if the given square level is 2', function () {
      fakeSquare.level = 2;
      squaresMap._deconstructSquare(fakeSquare, fakeSubtractedBlock);
      expect(squaresMap._deconstructSmallestSquare)
        .toHaveBeenCalledWith(fakeSquare, fakeSubtractedBlock);
    });

    describe('if the given square level is not 2', function () {

      function createSmallerSquare(withSubtractedBlock) {
        var smallerSquare = jasmine.createSpyObj('smallerSquare', ['blocks']);
        smallerSquare.blocks = jasmine.createSpyObj(
          'smallerSquare.blocks', ['indexOf']);
        smallerSquare.blocks.indexOf.and.returnValue(
          withSubtractedBlock ? 0 : -1
        );
        return smallerSquare;
      }

      beforeEach(function () {
        fakeSquare.level = 4;
      });

      it('calls _splitSquare', function () {
        squaresMap._deconstructSquare(fakeSquare, fakeSubtractedBlock);
        expect(squaresMap._splitSquare)
          .toHaveBeenCalledWith(fakeSquare);
      });

      it('calls _saveSmallerSquare for each smallerSquare ' +
         'if smallerSquare blocks does not contain the given subtractedBlock', function () {
        var smallerSquares = [createSmallerSquare(false),
                              createSmallerSquare(false),
                              createSmallerSquare(false)];
        squaresMap._splitSquare.and.returnValue(smallerSquares);
        squaresMap._deconstructSquare(fakeSquare, fakeSubtractedBlock);
        expect(squaresMap._saveSmallerSquare.calls.allArgs())
          .toEqual(smallerSquares.map(square => [square]));
      });

      it('does not call _saveSmallerSquare for smallerSquare blocks which ' +
         'contains the given subtractedBlock', function () {
        var smallerSquares = [createSmallerSquare(true),
                              createSmallerSquare(false),
                              createSmallerSquare(false)];
        squaresMap._splitSquare.and.returnValue(smallerSquares);
        squaresMap._deconstructSquare(fakeSquare, fakeSubtractedBlock);
        expect(squaresMap._saveSmallerSquare.calls.allArgs())
          .toEqual([[smallerSquares[1]], [smallerSquares[2]]]);
      });

      it('calls _deconstructSquare for smallerSquare ' +
         'which contains the given subtractedBlock', function () {
        var smallerSquares = [createSmallerSquare(false),
                              createSmallerSquare(true),
                              createSmallerSquare(false)];
        squaresMap._splitSquare.and.returnValue(smallerSquares);
        squaresMap._deconstructSquare(fakeSquare, fakeSubtractedBlock);
        expect(squaresMap._deconstructSquare.calls.argsFor(1))
          .toEqual([smallerSquares[1], fakeSubtractedBlock]);
      });

      it('calls _deconstructSquare after all _saveSmallerSquare calls', function () {
        var smallerSquares = [createSmallerSquare(false),
                              createSmallerSquare(true),
                              createSmallerSquare(false),
                              createSmallerSquare(false)];
        squaresMap._splitSquare.and.returnValue(smallerSquares);
        var _saveSmallerSquareCallCounter = 0;
        squaresMap._saveSmallerSquare.and.callFake(function () {
          _saveSmallerSquareCallCounter++;
        });
        squaresMap._deconstructSquare.and.callFake(function () {
          fakeDeconstructSquare(...Array.from(arguments));
          expect(_saveSmallerSquareCallCounter).toBe(3);
        });
        squaresMap._deconstructSquare(fakeSquare, fakeSubtractedBlock);
      });

    });

  });

  describe('_deconstructSmallestSquare', function () {

    var squaresMap;
    var fakeSquare;
    var fakeSubtractedBlock;
    var map;

    beforeEach(function () {
      squaresMap = new SquaresMap();
      fakeSubtractedBlock = jasmine.createSpy('subtractedBlock');
      fakeSquare = {
        blocks: [
          jasmine.createSpy('block 1'), jasmine.createSpy('block 2'),
          fakeSubtractedBlock, jasmine.createSpy('block 3'),
          jasmine.createSpy('block 4')
        ]
      };
      map = jasmine.createSpyObj('map', ['changeOwnedBlockLevel']);
      spyOn(mapProvider, 'getMap').and.returnValue(map);
    });

    it('calls changeOwnedBlockLevel on map for each the given square\'s block ' +
       'different than the given subtractedBlock', function () {
      squaresMap._deconstructSmallestSquare(fakeSquare, fakeSubtractedBlock);
      expect(map.changeOwnedBlockLevel.calls.allArgs()).toEqual([
        [fakeSquare.blocks[0], 1],
        [fakeSquare.blocks[1], 1],
        [fakeSquare.blocks[3], 1],
        [fakeSquare.blocks[4], 1]
      ]);
    });

  });

  describe('_splitSquare', function () {

    var squaresMap;
    var fakeSquare;

    beforeEach(function () {
      squaresMap = new SquaresMap();
      fakeSquare = {
        regionX: jasmine.createSpy('square.regionX'),
        regionY: jasmine.createSpy('square.regionY'),
        x: 8,
        y: 18,
        level: 5,
        player: jasmine.createSpy('square.player')
      };
      squaresMap.squareFactory = jasmine.createSpyObj(
        'squareFactory', ['createSquare']);
      spyOn(Square, 'getWidth').and.returnValue(5);
      spyOn(Square, 'getHeight').and.returnValue(4);
    });

    it('is a generator', function () {
      expect(squaresMap._splitSquare.constructor.name).toBe('GeneratorFunction');
    });

    it('calls createSquare on squareFactory for 4 directions', function () {
      var _splitSquareGen = squaresMap._splitSquare(fakeSquare);
      var result;
      do {
        result = _splitSquareGen.next();
      } while (!result.done);
      expect(squaresMap.squareFactory.createSquare.calls.allArgs()).toEqual([
        [fakeSquare.regionX, fakeSquare.regionY,
         8, 18, 5 - 1, fakeSquare.player],
        [fakeSquare.regionX, fakeSquare.regionY,
         8 + 5, 18, 5 - 1, fakeSquare.player],
        [fakeSquare.regionX, fakeSquare.regionY,
         8, 18 + 4, 5 - 1, fakeSquare.player],
        [fakeSquare.regionX, fakeSquare.regionY,
         8 + 5, 18 + 4, 5 - 1, fakeSquare.player]
      ]);
    });

    it('yields subsequent createSquare results', function () {
      var expectedResults = [];
      squaresMap.squareFactory.createSquare.and.callFake(function () {
        var result = jasmine.createSpy('createSquareResult');
        expectedResults.push(result);
        return result;
      });
      var _splitSquareGen = squaresMap._splitSquare(fakeSquare);
      var results = [];
      do {
        results.push(_splitSquareGen.next());
      } while (!results[results.length - 1].done);
      expect(expectedResults.length).toBeGreaterThan(0);
      expect(results.filter(result => !result.done)
        .map(result => result.value)).toEqual(expectedResults);
    });

  });

  describe('_saveSmallerSquare', function () {

    var squaresMap;
    var fakeSquare;
    var map;

    function createBlock(level) {
      return { level };
    }

    beforeEach(function () {
      squaresMap = new SquaresMap();
      fakeSquare = jasmine.createSpyObj('square',
        ['calcGoldAndResourceOutcomes']);
      fakeSquare.blocks = [createBlock(5), createBlock(2)];
      fakeSquare.level = 4;
      spyOn(squaresMap, 'checkUpperLevelSquare').and.returnValue(false);
      squaresMap.squareFactory = jasmine.createSpyObj('squareFactory',
        ['saveSquare']);
      map = jasmine.createSpyObj('map', ['changeOwnedBlockLevel']);
      spyOn(mapProvider, 'getMap').and.returnValue(map);
    });

    it('calls saveSquare on squareFactory', function () {
      squaresMap._saveSmallerSquare(fakeSquare);
      expect(squaresMap.squareFactory.saveSquare).toHaveBeenCalledWith(fakeSquare);
    });

    it('calls checkUpperLevelSquare', function () {
      squaresMap._saveSmallerSquare(fakeSquare);
      expect(squaresMap.checkUpperLevelSquare).toHaveBeenCalledWith(fakeSquare);
    });

    it('calls changeOwnedBlockLevel on map for each block ' +
       'with a level higher than its square level', function () {
      squaresMap._saveSmallerSquare(fakeSquare);
      expect(map.changeOwnedBlockLevel).toHaveBeenCalledWith(
        fakeSquare.blocks[0], fakeSquare.level);
    });

    it('calls calcGoldAndResourceOutcomes on the given square ' +
       'if checkUpperLevelSquare returns false', function () {
      squaresMap._saveSmallerSquare(fakeSquare);
      expect(fakeSquare.calcGoldAndResourceOutcomes).toHaveBeenCalledWith();
    });

    it('calls calcGoldAndResourceOutcomes on checkUpperLevelSquare result ' +
       'if checkUpperLevelSquare does not return false', function () {
      var anotherSquare = jasmine.createSpyObj('square',
        ['calcGoldAndResourceOutcomes']);
      anotherSquare.blocks = fakeSquare.blocks;
      squaresMap.checkUpperLevelSquare.and.returnValue(anotherSquare);
      squaresMap._saveSmallerSquare(fakeSquare);
      expect(anotherSquare.calcGoldAndResourceOutcomes).toHaveBeenCalledWith();
      expect(fakeSquare.calcGoldAndResourceOutcomes).not.toHaveBeenCalled();
    });

  });

});