'use strict';

angular.module('square').factory('SquareFactory', ['Square', 'ArrayUtils', 'storageProvider', 'mapProvider',
    function (Square, ArrayUtils, storageProvider, mapProvider) {

  function SquareFactory () {

  }

  SquareFactory.prototype.squaresMap = null;

  SquareFactory.prototype.setSquaresMap = function (squaresMap) {
    this.squaresMap = squaresMap;
  };

  SquareFactory.prototype._saveSquareToBlocks = function (square) {
    square.blocks.forEach(function (block) {
      block.squarePos = 'none';
      block.square = square;
    });
    if (square.level > 2) {
      var borders = this._getSquareBorderBlocks(square);
      borders.top.forEach(function (block) {
        block.squarePos = 'top';
      });
      borders.right.forEach(function (block) {
        block.squarePos = 'right';
      });
      borders.bottom.forEach(function (block) {
        block.squarePos = 'bottom';
      });
      borders.left.forEach(function (block) {
        block.squarePos = 'left';
      });
    }
    var corners = this._getSquareCornerBlocks(square);
    corners.leftTop.squarePos = 'left-top';
    corners.rightTop.squarePos = 'right-top';
    corners.rightBottom.squarePos = 'right-bottom';
    corners.leftBottom.squarePos = 'left-bottom';
  };

  SquareFactory.prototype._getSquareCornerBlocks = function (square) {
    return {
      leftTop: mapProvider.getBlock(square.regionX, square.regionY, square.x, square.y),
      rightTop: mapProvider.getBlock(square.regionX, square.regionY, square.x + square.getWidth() - 1, square.y),
      rightBottom: mapProvider.getBlock(square.regionX, square.regionY, square.x + square.getWidth() - 1, square.y + square.getHeight() - 1),
      leftBottom: mapProvider.getBlock(square.regionX, square.regionY, square.x, square.y + square.getHeight() - 1)
    };
  };

  SquareFactory.prototype._getSquareBorderBlocks = function (square) {
    return {
      top: mapProvider.getBlocks(square.regionX, square.regionY, square.x, square.y, square.getWidth(), 1),
      right: mapProvider.getBlocks(square.regionX, square.regionY, square.x + square.getWidth() - 1, square.y, 1, square.getHeight()),
      bottom: mapProvider.getBlocks(square.regionX, square.regionY, square.x, square.y + square.getHeight() - 1, square.getWidth(), 1),
      left: mapProvider.getBlocks(square.regionX, square.regionY, square.x, square.y, 1, square.getHeight())
    };
  };

  SquareFactory.prototype._saveSquareToPlayer = function (square) {
    square.player.squaresByLevel[square.level].push(square);
  };

  SquareFactory.prototype._removeSquareFromBlocks = function (square) {
    square.blocks.forEach(function (block) {
      block.squarePos = 'none';
      block.square = null;
      //TODO: what about block.level ?
    });
  };

  SquareFactory.prototype._removeSquareFromPlayer = function (square) {
    ArrayUtils.removeElement(square.player.squaresByLevel[square.level], square);
  };

  SquareFactory.prototype.createSquare = function (regionX, regionY, x, y, level, player) {
    var coords = mapProvider.normalizeCoords(regionX, regionY, x, y);
    return new Square(coords.regionX, coords.regionY, coords.blockX, coords.blockY,
      mapProvider.getBlocks(coords.regionX, coords.regionY, coords.blockX, coords.blockY,
        Square.getWidth(level), Square.getWidth(level)),
      level, player);
  };

  SquareFactory.prototype.saveSquare = function (square) {
    //TODO: check if there are already any buildings in blocks, if yes - assign them to the new square
    this._saveSquareToBlocks(square);
    this._saveSquareToPlayer(square);
    this.squaresMap.addSquare(square);
    square.calcGoldIncomeInternal();
    storageProvider.getStorage(square.player).changeGoldIncome(square.goldIncomeInternal);
    return square;
  };

  SquareFactory.prototype.deleteSquare = function (square) {
    //TODO: remove square from buildings
    this._removeSquareFromBlocks(square);
    this._removeSquareFromPlayer(square);
    this.squaresMap.removeSquare(square);
    storageProvider.getStorage(square.player).changeGoldIncome(-square.goldIncomeInternal);
  };

  return SquareFactory;
}]);