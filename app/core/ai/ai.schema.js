'use strict';

var _RegExp = require('nwglobal').RegExp;

angular.module('square')

.constant('aiMissionsRegExp', new _RegExp(/^(resourceCollecting|largeSquareMaking)$/))

.factory('AISchema', ['Joi', 'Player', 'Block', 'improvableBlockKindRegExp',
                      'AISquaring', 'Map', 'SquaresMap',
                      'aiMissionsRegExp', 'AI', 'storageResourcesRegExp',
    function (Joi, Player, Block, improvableBlockKindRegExp,
              AISquaring, Map, SquaresMap,
              aiMissionsRegExp, AI, storageResourcesRegExp) {

  return Joi.object().keys({
    player: Joi.object().type(Player),
    improvedBlocks: Joi.array().items(
      Joi.object().type(Block)
    ),
    improvedBlockNeighborsByKind: Joi.object({})
      .pattern(improvableBlockKindRegExp, Joi.array().items(
        Joi.object().type(Block)
      )).length(9),
    findingPathThresholds: Joi.array().items(
      Joi.number().positive()
    ),
    technologies: Joi.object(),
    missionValues: Joi.object({})
      .pattern(aiMissionsRegExp, [Joi.number().greater(0), 0])
      .length(2),
    aiSquaring: Joi.object().type(AISquaring),
    map: [Joi.object().type(Map), null],
    squaresMap: [Joi.object().type(SquaresMap), null],
    removedBlocksToCheckForSquaresIndexes: Joi.object().type(Int8Array),
    nextKindByResName: Joi.object({})
      .pattern(storageResourcesRegExp, Joi.string().regex(improvableBlockKindRegExp))
      .length(4),
    currentMission: Joi.string().regex(aiMissionsRegExp)
  }).type(AI);

}]);
