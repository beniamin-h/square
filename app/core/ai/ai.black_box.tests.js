'use strict';

var fs = require('fs');

jasmine.DEFAULT_TIMEOUT_INTERVAL = 120000;

describe('Ai', function() {
  beforeEach(module('square'));

  var AI, AIPlayer, Map,  playerProvider, gameStateProvider,
    mapProvider, storageProvider, squareProvider, Joi, initProvider;

  beforeEach(inject(function (
      _AI_, _AIPlayer_, _Map_, _playerProvider_, _gameStateProvider_,
      _mapProvider_, _storageProvider_, _squareProvider_, _Joi_, _initProvider_) {
    AI = _AI_;
    AIPlayer = _AIPlayer_;
    Map = _Map_;
    playerProvider = _playerProvider_;
    gameStateProvider = _gameStateProvider_;
    mapProvider = _mapProvider_;
    storageProvider = _storageProvider_;
    squareProvider = _squareProvider_;
    Joi = _Joi_;
    initProvider = _initProvider_;
  }));

  function makeMovesTimed(count) {
    var timer = new Date();
    var aiPlayer = playerProvider.getFirstAI();
    for (var i = 0; i < count; i++) {
      aiPlayer.ai.nextMove();
    }
    return new Date() - timer;
  }

  function loadMap(fixtureName, done, callback) {
    gameStateProvider.load(process.env.PWD + '/test_fixtures/' + fixtureName + '.json',
      function (err) {
        if (err) {
          done.fail(err);
          return;
        }
        try {
          callback();
        } catch (err) {
          done.fail(err);
          return;
        }
        done();
      }
    );
  }

  describe('achieves storage value', function () {

    var storageResourceValues = {
      food: 1,
      wood: 2,
      stone: 3,
      metal: 4,
      clay: 1,
      coal: 5,
      horses: 8,
      boards: 4,
      cutStone: 6,
      steel: 16,
      brick: 2
    };
    var storageGoldIncomeValue = 0.5;

    var moveLimit;
    var storageValue;
    var maxStorageValue;
    var moveCounter;
    var maxStorageValueIteration;

    function calcStorageValue(storage) {
      storageValue = 0;
      storageValue += Object.keys(storage.resources)
        .reduce(function (tmpValue, resName) {
          tmpValue += storageResourceValues[resName] * storage.resources[resName];
          return tmpValue;
        }, 0);
      storageValue += storage.gold * storageGoldIncomeValue;
      if (maxStorageValue < storageValue) {
        maxStorageValue = storageValue;
        maxStorageValueIteration = moveCounter;
      }
    }

    beforeEach(function () {
      moveLimit = 40  ;
      storageValue = 0;
      maxStorageValue = 0;
      moveCounter = 0;
      maxStorageValueIteration = null;
    });

    it('at least 75 on plenty_advanced_resources_map_11x11_5x5', function (done) {
      loadMap('plenty_advanced_resources_map_11x11_5x5', done, function () {
        var storage = storageProvider.getStorage(playerProvider.getFirstAI());
        while (storageValue < 100 && moveCounter++ < moveLimit) {
          playerProvider.getFirstAI().ai.nextMove();
          calcStorageValue(storage);
        }
        expect(maxStorageValue).toBeGreaterThanOrEqual(75);
      });
    });

    it('at least 26 on simple_map_3x3_5x5', function (done) {
      loadMap('simple_map_3x3_5x5', done, function () {
        var storage = storageProvider.getStorage(playerProvider.getFirstAI());
        while (storageValue < 100 && moveCounter++ < moveLimit) {
          playerProvider.getFirstAI().ai.nextMove();
          calcStorageValue(storage);
        }
        expect(maxStorageValue).toBeGreaterThanOrEqual(26);
      });
    });

  });

  describe('makes 50 steps in simple_map_3x3_5x5 fixture', function () {

    var execTime = 0;

    beforeEach(function (done) {
      loadMap('simple_map_3x3_5x5', done, function () {
        execTime = makeMovesTimed(50);
      });
    });

    it('and do it fast', function () {
      var aiPlayer = playerProvider.getFirstAI();
      expect(aiPlayer.defeated).toBeFalse();
      expect(execTime).toBeLessThan(100000);
    });

    /* afterAll(function () {
      console.log('AVG 0/17 (<150.0):', execTimeArray.reduce(function sum(a, b) {
        return a + b;
      }) / 5.0);
    }); */

  });

  describe('makes 30 steps in simple_map_5x5_10x10 fixture', function () {

    var execTime = 0;

    beforeEach(function (done) {
      loadMap('simple_map_5x5_10x10', done, function () {
        execTime = makeMovesTimed(30);
      });
    });

    it('and do it fast', function () {
      var aiPlayer = playerProvider.getFirstAI();
      expect(aiPlayer.defeated).toBeFalse();
      expect(execTime).toBeLessThan(60000);
    });

  });

  describe('makes 10 steps on simple_map_3x3_5x5_with_24_improved_blocks fixture', function () {

    var execTime = 0;

    beforeEach(function (done) {
      loadMap('simple_map_3x3_5x5_with_24_improved_blocks', done, function () {
        execTime = makeMovesTimed(10);
      });
    });

    it('and do it fast', function () {
      var aiPlayer = playerProvider.getFirstAI();
      expect(aiPlayer.defeated).toBeFalse();
      expect(execTime).toBeLessThan(20000);
    });

  });

  describe('makes 10 steps on simple_map_3x3_5x5_with_34_improved_blocks fixture', function () {

    var execTime = 0;

    beforeEach(function (done) {
      loadMap('simple_map_3x3_5x5_with_34_improved_blocks', done, function () {
        execTime = makeMovesTimed(10);
      });
    });

    it('and do it fast', function () {
      var aiPlayer = playerProvider.getFirstAI();
      expect(aiPlayer.defeated).toBeFalse();
      expect(execTime).toBeLessThan(20000);
    });
  });

  describe('makes 20 steps on random map', function () {

    var execTime = 0;

    function makeMovesOnRandomMap(done) {
      initProvider.initNewGame();
      var aiPlayer = playerProvider.getFirstAI();
      storageProvider.getStorage(aiPlayer).put({
        metal: 1, stone: 1, wood: 1, food: 1
      });

      var filePath = process.env.PWD + '/test_fixtures/test_failures/' +
        (new Date().toString().replace(/[\s:\(\)\+]/g, '_')) + '.json';
      gameStateProvider.save(filePath, function (err) {
        if (err) {
          done.fail('Cannot save random map: ' + err);
          return;
        }
        try {
          execTime = makeMovesTimed(20);
        } catch (err) {
          done.fail(err);
          return;
        }

        if (aiPlayer.defeated) {
          done.fail('AI player has been defeated');
        } else {
          expect(aiPlayer.defeated).toBeFalse();
          expect(execTime).toBeLessThan(40000);
          fs.unlink(filePath, function (err) {
            if (err) {
              done.fail('Cannot remove the map after the test: ' + err);
              return;
            }
            done();
          });
        }
      });
    }

    var numberOfTests = 5;

    for (var i = 0; i < numberOfTests; i++) {
      it('and do it fast #' + (i + 1), function (done) {
        makeMovesOnRandomMap(done);
      });
    }

  });

});