'use strict';

describe('Ai', function() {
  beforeEach(module('square'));

  var AI, AIPlayer, Map, playerProvider, gameStateProvider, mapProvider, storageProvider;

  beforeEach(inject(function (_AI_, _AIPlayer_,
      _Map_, _playerProvider_, _gameStateProvider_, _mapProvider_, _storageProvider_) {
    AI = _AI_;
    AIPlayer = _AIPlayer_;
    Map = _Map_;
    playerProvider = _playerProvider_;
    gameStateProvider = _gameStateProvider_;
    mapProvider = _mapProvider_;
    storageProvider = _storageProvider_;
  }));

  describe('findPathsRecursively', function () {

    describe('must not change improvedBlockNeighborsByKind', function () {

      function doTest () {
        var map = mapProvider.getMap();
        var aiPlayer = playerProvider.getFirstAI();

        aiPlayer.ai.updateImprovedBlockNeighborsByKind();
        var oldImprovedBlockNeighborsByKind = {};
        var kind;
        for (kind in aiPlayer.ai.improvedBlockNeighborsByKind) {
          oldImprovedBlockNeighborsByKind[kind] = aiPlayer.ai.improvedBlockNeighborsByKind[kind].slice();
        }
        aiPlayer.ai.resetRemovedBlocksToCheckForSquaresIndexes();
        aiPlayer.ai.setMissions();
        aiPlayer.ai.findPathsRecursively(map, map.getBlock(1, 0, 0, 2),
          aiPlayer.ai.improvedBlockNeighborsByKind, [], [],
          storageProvider.getStorage(aiPlayer), 0, 5);

        for (kind in aiPlayer.ai.improvedBlockNeighborsByKind) {
          expect(aiPlayer.ai.improvedBlockNeighborsByKind[kind]).toEqual(oldImprovedBlockNeighborsByKind[kind]);
        }
      }

      it('for an empty map', function (done) {
        gameStateProvider.load(process.env.PWD + '/test_fixtures/simple_map_3x3_5x5.json', function (err) {
          if (err) {
            done.fail(err);
            return;
          }

          doTest();
          done();
        });
      });

      it('for a map with 24 blocks improved', function (done) {
        gameStateProvider.load(process.env.PWD + '/test_fixtures/simple_map_3x3_5x5_with_24_improved_blocks.json', function (err) {
          if (err) {
            done.fail(err);
            return;
          }

          doTest();

          var improvedBlockNeighborsByKind = playerProvider.getFirstAI().ai.improvedBlockNeighborsByKind;
          var len = 0;
          for (var kind in improvedBlockNeighborsByKind) {
            len += improvedBlockNeighborsByKind[kind].length;
          }

          expect(len).toBeGreaterThan(0);

          done();
        });
      });
    });

    describe('must not change player storage', function () {

      function doTest () {
        var map = mapProvider.getMap();
        var aiPlayer = playerProvider.getFirstAI();

        aiPlayer.ai.updateImprovedBlockNeighborsByKind();
        var oldStorageResources = {};
        var playerStorage = storageProvider.getStorage(aiPlayer);
        var oldPlayerGold = playerStorage.gold;
        for (var kind in playerStorage.resources) {
          oldStorageResources[kind] = playerStorage.resources[kind];
        }
        aiPlayer.ai.resetRemovedBlocksToCheckForSquaresIndexes();
        aiPlayer.ai.setMissions();
        aiPlayer.ai.findPathsRecursively(map, map.getBlock(1, 0, 0, 2),
          aiPlayer.ai.improvedBlockNeighborsByKind, [], [],
          playerStorage, 0, 5);

        for (kind in playerStorage.resources) {
          expect(playerStorage.resources[kind]).toBe(oldStorageResources[kind]);
        }

        expect(oldPlayerGold).toBe(playerStorage.gold);
      }

      it('for an empty map', function (done) {
        gameStateProvider.load(process.env.PWD + '/test_fixtures/simple_map_3x3_5x5.json', function (err) {
          if (err) {
            done.fail(err);
            return;
          }

          doTest();
          done();
        });
      });

      it('for a map with 24 blocks improved', function (done) {
        gameStateProvider.load(process.env.PWD + '/test_fixtures/simple_map_3x3_5x5_with_24_improved_blocks.json', function (err) {
          if (err) {
            done.fail(err);
            return;
          }

          doTest();
          done();
        });
      });
    });

  });

  describe('_getNextKindByResName', function () {

    it('returns mountain if food passed', function () {
      var ai = new AI();
      expect(ai._getNextKindByResName('food')).toBe('mountain');
    });

    it('returns hill if metal passed', function () {
      var ai = new AI();
      expect(ai._getNextKindByResName('metal')).toBe('hill');
    });

    it('returns forest if stone passed', function () {
      var ai = new AI();
      expect(ai._getNextKindByResName('stone')).toBe('forest');
    });

    it('returns plain if wood passed', function () {
      var ai = new AI();
      expect(ai._getNextKindByResName('wood')).toBe('plain');
    });


    it('returns undefined if something else than above passed', function () {
      var ai = new AI();
      expect(ai._getNextKindByResName('forest a')).toBeUndefined();
      expect(ai._getNextKindByResName(null)).toBeUndefined();
      expect(ai._getNextKindByResName('water')).toBeUndefined();
      expect(ai._getNextKindByResName({})).toBeUndefined();
      expect(ai._getNextKindByResName()).toBeUndefined();
    });

  });

  describe('setMissions', function () {

    describe('sets currentMission', function () {

      it('to resourceCollecting if getResourceCollectingMissionValue returns the greatest value', function () {
        var ai = new AI();
        ai.currentMission = 'fakeMission';
        ai.missionValues['fakeMission'] = 0;
        spyOn(ai, 'getResourceCollectingMissionValue').and.returnValue(1000);
        spyOn(ai, 'getLargeSquareMakingMissionValue').and.returnValue(999);
        ai.setMissions();
        expect(ai.currentMission).toBe('resourceCollecting');
      });

      it('to largeSquareMaking if getLargeSquareMakingMissionValue returns the greatest value', function () {
        var ai = new AI();
        ai.currentMission = 'fakeMission';
        ai.missionValues['fakeMission'] = 0;
        spyOn(ai, 'getResourceCollectingMissionValue').and.returnValue(999);
        spyOn(ai, 'getLargeSquareMakingMissionValue').and.returnValue(1000);
        ai.setMissions();
        expect(ai.currentMission).toBe('largeSquareMaking');
      });

    });

    describe('sets mission values', function () {

      it('to values returned by getResourceCollectingMissionValue and getLargeSquareMakingMissionValue', function () {
        var ai = new AI();
        ai.currentMission = 'fakeMission';
        ai.missionValues['fakeMission'] = 0;
        spyOn(ai, 'getResourceCollectingMissionValue').and.returnValue(783);
        spyOn(ai, 'getLargeSquareMakingMissionValue').and.returnValue(111);
        ai.setMissions();
        expect(ai.missionValues.resourceCollecting).toBe(783);
        expect(ai.missionValues.largeSquareMaking).toBe(111);
      });

    });

  });

  describe('getResourceCollectingMissionValue', function () {

    it('returns higher value for emptier storage #1', function () {
      var getResourcesSpy = spyOn(storageProvider, 'getResources');
      getResourcesSpy.and.returnValue({
        metal: 10,
        stone: 10,
        wood: 10,
        food: 11
      });
      var ai = new AI();
      var fullStorageResult = ai.getResourceCollectingMissionValue();
      getResourcesSpy.and.returnValue({
        metal: 5,
        stone: 5,
        wood: 5,
        food: 6
      });
      var emptierStorageResult = ai.getResourceCollectingMissionValue();
      expect(emptierStorageResult).toBeGreaterThan(fullStorageResult);
    });

    it('returns higher value for emptier storage #2', function () {
      var getResourcesSpy = spyOn(storageProvider, 'getResources');
      getResourcesSpy.and.returnValue({
        metal: 3,
        stone: 5,
        wood: 5,
        food: 3
      });
      var ai = new AI();
      var fullStorageResult = ai.getResourceCollectingMissionValue();
      getResourcesSpy.and.returnValue({
        metal: 2,
        stone: 3,
        wood: 4,
        food: 2
      });
      var emptierStorageResult = ai.getResourceCollectingMissionValue();
      expect(emptierStorageResult).toBeGreaterThan(fullStorageResult);
    });

    it('returns higher value for emptier storage #3', function () {
      var getResourcesSpy = spyOn(storageProvider, 'getResources');
      getResourcesSpy.and.returnValue({
        metal: 3000,
        stone: 5000,
        wood: 5000,
        food: 3000
      });
      var ai = new AI();
      var fullStorageResult = ai.getResourceCollectingMissionValue();
      getResourcesSpy.and.returnValue({
        metal: 3000,
        stone: 5000,
        wood: 4999,
        food: 2999
      });
      var emptierStorageResult = ai.getResourceCollectingMissionValue();
      expect(emptierStorageResult).toBeGreaterThan(fullStorageResult);
    });

    it('returns 1000 if there is less than 2 of any resource', function () {
      var getResourcesSpy = spyOn(storageProvider, 'getResources');
      getResourcesSpy.and.returnValue({
        metal: 999,
        stone: 999,
        wood: 999,
        food: 1
      });
      var ai = new AI();
      var result1 = ai.getResourceCollectingMissionValue();
      expect(result1).toBe(1000);

      getResourcesSpy.and.returnValue({
        metal: 0,
        stone: 1,
        wood: 2,
        food: 1
      });
      var result2 = ai.getResourceCollectingMissionValue();
      expect(result2).toBe(1000);
    });

    it('returns 1000 if there are less than 9 resources total', function () {
      var getResourcesSpy = spyOn(storageProvider, 'getResources');
      getResourcesSpy.and.returnValue({
        metal: 2,
        stone: 2,
        wood: 2,
        food: 2
      });
      var ai = new AI();
      var result1 = ai.getResourceCollectingMissionValue();
      expect(result1).toBe(1000);

      getResourcesSpy.and.returnValue({
        metal: 7,
        stone: 0,
        wood: 0,
        food: 0
      });
      var result2 = ai.getResourceCollectingMissionValue();
      expect(result2).toBe(1000);
    });

    it('returns less than 1000 if there are 9 resources total', function () {
      var getResourcesSpy = spyOn(storageProvider, 'getResources');
      getResourcesSpy.and.returnValue({
        metal: 2,
        stone: 2,
        wood: 2,
        food: 3
      });
      var ai = new AI();
      var result1 = ai.getResourceCollectingMissionValue();
      expect(1000).toBeGreaterThan(result1);
    });

    it('returns less than 1000 if there are more than 9 resources total', function () {
      var getResourcesSpy = spyOn(storageProvider, 'getResources');
      getResourcesSpy.and.returnValue({
        metal: 200,
        stone: 2000,
        wood: 200,
        food: 3000
      });
      var ai = new AI();
      var result1 = ai.getResourceCollectingMissionValue();
      expect(1000).toBeGreaterThan(result1);
    });

    it('returns less than 1000 if there is 3 of each resource', function () {
      var getResourcesSpy = spyOn(storageProvider, 'getResources');
      getResourcesSpy.and.returnValue({
        metal: 3,
        stone: 3,
        wood: 3,
        food: 3
      });
      var ai = new AI();
      var result1 = ai.getResourceCollectingMissionValue();
      expect(1000).toBeGreaterThan(result1);
    });

    it('returns less than 1000 if there is more than 2 of each resource', function () {
      var getResourcesSpy = spyOn(storageProvider, 'getResources');
      getResourcesSpy.and.returnValue({
        metal: 300,
        stone: 3,
        wood: 3000,
        food: 4
      });
      var ai = new AI();
      var result1 = ai.getResourceCollectingMissionValue();
      expect(1000).toBeGreaterThan(result1);
    });

  });

  describe('getLargeSquareMakingMissionValue', function () {

    it('returns higher value if there are less larger squares', function () {
      var ai = new AI();
      ai.player = { squaresByLevel: {3: [1,1,1,1], 4: [1,1], 5: [1,1,1,1,1,1,1] } };
      var aLotOfSquaresResult = ai.getLargeSquareMakingMissionValue();
      ai.player = { squaresByLevel: {3: [1,1], 4: [1,1], 5: [1] } };
      var aFewSquaresResult = ai.getLargeSquareMakingMissionValue();
      expect(aFewSquaresResult).toBeGreaterThan(aLotOfSquaresResult);
    });

    it('returns 900 if there is no square', function () {
      var ai = new AI();
      ai.player = { squaresByLevel: {3: [], 4: [], 5: [] } };
      var result = ai.getLargeSquareMakingMissionValue();
      expect(result).toBe(900);
    });

    it('returns 700 if there is one level 5 square', function () {
      var ai = new AI();
      ai.player = { squaresByLevel: {3: [], 4: [], 5: [1] } };
      var result = ai.getLargeSquareMakingMissionValue();
      expect(result).toBe(700);
    });

    it('returns 775 if there is one level 4 square', function () {
      var ai = new AI();
      ai.player = { squaresByLevel: {3: [], 4: [1], 5: [] } };
      var result = ai.getLargeSquareMakingMissionValue();
      expect(result).toBe(775);
    });

    it('returns 825 if there is one level 3 square', function () {
      var ai = new AI();
      ai.player = { squaresByLevel: {3: [1], 4: [], 5: [] } };
      var result = ai.getLargeSquareMakingMissionValue();
      expect(result).toBe(825);
    });

    it('returns 900 even if there are 7 level 2 squares', function () {
      var ai = new AI();
      ai.player = { squaresByLevel: {2: [1,1,1,1,1,1,1], 3: [], 4: [], 5: [] } };
      var result = ai.getLargeSquareMakingMissionValue();
      expect(result).toBe(900);
    });

  });

  describe('getFirstMove', function () {

    var ai;
    var map;
    var fakePlainBlock;
    var fakeStorage;

    function createFakeBlock (kindString, level) {
      var fakeBlock = jasmine.createSpy('block_' + kindString);
      fakeBlock.kind = jasmine.createSpyObj('block.kind', ['toString']);
      fakeBlock.kind.toString.and.returnValue(kindString);
      fakeBlock.level = level || 0;
      return fakeBlock;
    }

    beforeEach(function () {
      ai = new AI({ startingRegionCoords: {} });
      fakeStorage =  jasmine.createSpy('fakeStorage');
      spyOn(storageProvider, 'getStorage').and.returnValue(fakeStorage);
      spyOn(ai, 'updateImprovedBlockNeighborsByKind');
      spyOn(ai, 'resetRemovedBlocksToCheckForSquaresIndexes');
      spyOn(ai, 'setMissions');
      fakePlainBlock = createFakeBlock('plain');
      map = jasmine.createSpyObj('map', ['getRegionByXY']);
      map.getRegionByXY.and.returnValue({ blocks: [ [ fakePlainBlock ] ] });
      spyOn(ai, 'findPathsRecursively').and.returnValue({ block: fakePlainBlock });
      ai.setMap(map);
    });

    it('returns move to the longest path', function () {
      var longestPathFakePlainBlock = createFakeBlock('plain');
      map.getRegionByXY.and.returnValue({ blocks: [ [ fakePlainBlock, longestPathFakePlainBlock ] ] });
      ai.findPathsRecursively.and.callFake(function (map, block) {
        return {
          pathLength: block === longestPathFakePlainBlock ? 5 : 4,
          pathValue: 1,
          block: block
        };
      });
      var result = ai.getFirstMove(map);
      expect(result).toBe(longestPathFakePlainBlock);
    });

    it('does not return move to the already improved block', function () {
      var longestPathFakePlainBlock = createFakeBlock('plain', 1);
      map.getRegionByXY.and.returnValue({ blocks: [ [ fakePlainBlock, longestPathFakePlainBlock ] ] });
      ai.findPathsRecursively.and.callFake(function (map, block) {
        return {
          pathLength: block === longestPathFakePlainBlock ? 5 : 4,
          pathValue: 1,
          block: block
        };
      });
      var result = ai.getFirstMove(map);
      expect(result).toBe(fakePlainBlock);
    });

    it('returns move to the path of the highest pathValue among the longest paths', function () {
      var anotherFakePlainBlock = createFakeBlock('plain'); // pathLength: 4, pathValue: 2
      var longestPathFakePlainBlock = createFakeBlock('plain'); // pathLength: 5, pathValue: 1
      var anotherLongestPathFakePlainBlock = createFakeBlock('plain'); // pathLength: 5, pathValue: 2
      map.getRegionByXY.and.returnValue({ blocks: [ [
        fakePlainBlock, longestPathFakePlainBlock, anotherLongestPathFakePlainBlock, anotherFakePlainBlock ] ] });
      ai.findPathsRecursively.and.callFake(function (map, block) {
        return {
          pathLength: block === longestPathFakePlainBlock || block === anotherLongestPathFakePlainBlock ? 5 : 4,
          pathValue: block === anotherLongestPathFakePlainBlock || block === anotherFakePlainBlock ? 2 : 1,
          block: block
        };
      });
      var result = ai.getFirstMove(map);
      expect(result).toBe(anotherLongestPathFakePlainBlock);
    });

    it('calls updateImprovedBlockNeighborsByKind', function () {
      ai.getFirstMove(map);
      expect(ai.updateImprovedBlockNeighborsByKind).toHaveBeenCalled();
    });

    it('calls resetRemovedBlocksToCheckForSquaresIndexes', function () {
      ai.getFirstMove(map);
      expect(ai.resetRemovedBlocksToCheckForSquaresIndexes).toHaveBeenCalled();
    });

    it('calls findPathsRecursively for each plain block in starting region', function () {
      var anotherFakePlainBlock = createFakeBlock('plain');
      var fakeHillBlock = createFakeBlock('hill');
      map.getRegionByXY.and.returnValue({ blocks: [ [ fakePlainBlock, anotherFakePlainBlock, fakeHillBlock ] ] });
      ai.getFirstMove(map);
      expect(ai.findPathsRecursively.calls.count()).toBe(2);
      expect(ai.findPathsRecursively).toHaveBeenCalledWith(
        map, fakePlainBlock, ai.improvedBlockNeighborsByKind, [], [], fakeStorage, 0, jasmine.any(Number));
      expect(ai.findPathsRecursively).toHaveBeenCalledWith(
        map, anotherFakePlainBlock, ai.improvedBlockNeighborsByKind, [], [], fakeStorage, 0, jasmine.any(Number));
    });

    it('does not change findingPathThresholds after execution', function () {
      var oldFindingPathThresholds = ai.findingPathThresholds.slice(0);
      ai.getFirstMove(map);
      expect(ai.findingPathThresholds).toEqual(oldFindingPathThresholds);
    });

    it('increases findingPathThresholds by 2 during its execution', function () {
      var findingPathThresholdsSnapshot;
      ai.findPathsRecursively.and.callFake(function () {
        findingPathThresholdsSnapshot = ai.findingPathThresholds.slice(0);
        return { block: fakePlainBlock };
      });
      ai.getFirstMove(map);
      ai.findingPathThresholds.forEach(function (threshold, index) {
        expect(findingPathThresholdsSnapshot[index]).toEqual(threshold + 2);
      });
    });

  });

  describe('updateImprovedBlockNeighborsByKind', function () {

    it('clears ai.improvedBlockNeighborsByKind to empty arrays', function () {
      var ai = new AI({ playerId: 'fakePlayerId' });
      ai.map = { improvementNeighbors: { 'fakePlayerId': [] } };
      ai.improvedBlockNeighborsByKind = {
        mountain: [2,3,45,5],
        hill: {},
        forest: jasmine.createSpy('fakeForest'),
        plain: 0,
        fakeKind: [],
        wildHorses: 1,
        silverDeposit: null,
        gemstoneDeposit: false
      };
      ai.updateImprovedBlockNeighborsByKind();
      expect(ai.improvedBlockNeighborsByKind['mountain']).toEqual([]);
      expect(ai.improvedBlockNeighborsByKind['hill']).toEqual([]);
      expect(ai.improvedBlockNeighborsByKind['forest']).toEqual([]);
      expect(ai.improvedBlockNeighborsByKind['plain']).toEqual([]);
      expect(ai.improvedBlockNeighborsByKind['coalfield']).toEqual([]);
      expect(ai.improvedBlockNeighborsByKind['wildHorses']).toEqual([]);
      expect(ai.improvedBlockNeighborsByKind['goldDeposit']).toEqual([]);
      expect(ai.improvedBlockNeighborsByKind['silverDeposit']).toEqual([]);
      expect(ai.improvedBlockNeighborsByKind['gemstoneDeposit']).toEqual([]);
      expect(ai.improvedBlockNeighborsByKind['fakeKind']).toBeUndefined();
    });

  });

  describe('setBlockImproved', function () {

    var ai;
    var fakeBlock;

    beforeEach(function () {
      ai = new AI();
      ai.improvedBlocks = ['1', 'a'];
      fakeBlock = jasmine.createSpy('block');
    });

    it('adds the given block to improvedBlocks', function () {
      ai.setBlockImproved(fakeBlock);
      expect(ai.improvedBlocks).toEqual(['1', 'a', fakeBlock]);
    });

  });

  describe('unsetBlockImproved', function () {

    var ai;
    var fakeBlock;

    beforeEach(function () {
      ai = new AI();
      fakeBlock = jasmine.createSpy('block');
      ai.improvedBlocks = ['1', fakeBlock, 'a'];
    });

    it('adds the given block to improvedBlocks', function () {
      ai.unsetBlockImproved(fakeBlock);
      expect(ai.improvedBlocks).toEqual(['1', 'a']);
    });

  });

});