'use strict';

angular.module('square')

.factory('AI', ['StringUtils', 'ArrayUtils', 'storageProvider', 'AISquaring',
    function (StringUtils, ArrayUtils, storageProvider, AISquaring) {

  function AI(player) {
    this.player = player;
    this.improvedBlocks = [];
    this.findingPathThresholds = [6, 4, 3, 3, 2, 2, 2, 2, 2, 2];
    this.technologies = {
      Drainage: false
    };
    this.missionValues = {
      resourceCollecting: 0,
      largeSquareMaking: 0
    };
    //TODO: remove AISquaring from constructor
    this.aiSquaring = new AISquaring(this.player, this.technologies);
    this.resetImprovedBlockNeighborsByKind();
    this.resetRemovedBlocksToCheckForSquaresIndexes();
  }

  AI.prototype.improvedBlocks = null;
  AI.prototype.improvedBlockNeighborsByKind = null;
  AI.prototype.map = null;
  AI.prototype.squaresMap = null;
  AI.prototype.removedBlocksToCheckForSquaresIndexes = null;

  AI.prototype.nextKindByResName = {
    food: 'mountain',
    metal: 'hill',
    stone: 'forest',
    wood: 'plain'
  };

  AI.prototype.advancedMapKinds = [
    'coalfield', 'wildHorses', 'goldDeposit', 'silverDeposit', 'gemstoneDeposit'
  ];

  AI.prototype.currentMission = 'resourceCollecting';
  AI.prototype.missionValues = null;

  AI.prototype.technologies = null;

  AI.prototype.findingPathThresholds = null;

  AI.prototype.setMissions = function () {
    // available missions: resourceCollecting, squareMaking

    this.missionValues.resourceCollecting = this.getResourceCollectingMissionValue();
    this.missionValues.largeSquareMaking = this.getLargeSquareMakingMissionValue();

    for(var mission in this.missionValues) {
      if (this.missionValues[mission] > this.missionValues[this.currentMission]) {
        this.currentMission = mission;
      }
    }
  };

  AI.prototype.getResourceCollectingMissionValue = function () {
    var resources = storageProvider.getResources(this.player);
    var minResAmount = +Infinity;
    var maxResAmount = -Infinity;
    var totalResAmount = 0;
    for (var resName in resources) {
      minResAmount = Math.min(resources[resName], minResAmount);
      maxResAmount = Math.max(resources[resName], maxResAmount);
      totalResAmount += resources[resName];
    }

    var baseMissionValue = 1000;

    if (minResAmount < 2 || totalResAmount < 9) {
      return baseMissionValue;
    }

    var minResAmountValue = baseMissionValue / Math.pow(minResAmount, 1.5) * 2.5;
    var totalResAmountValue = baseMissionValue / Math.pow(totalResAmount, 1.5) * 25;

    return Math.max(minResAmountValue, totalResAmountValue);
  };

  AI.prototype.getLargeSquareMakingMissionValue = function () {
    return 400 / (this.player.squaresByLevel['5'].length + 1) +
      250 / (this.player.squaresByLevel['4'].length + 1) +
      150 / (this.player.squaresByLevel['3'].length + 1) + 100;
  };

  AI.prototype.setMap = function (map) {
    this.map = map;
    this.aiSquaring.setMap(map);
  };

  AI.prototype.setSquaresMap = function (squaresMap) {
    this.squaresMap = squaresMap;
    this.aiSquaring.setSquaresMap(squaresMap);
  };

  AI.prototype.getFirstMove = function () {

    var startingRegion = this.map.getRegionByXY(this.player.startingRegionCoords.x,
      this.player.startingRegionCoords.y);

    var that = this;
    var bestPath = null;
    var playerStorage = storageProvider.getStorage(that.player);
    that.updateImprovedBlockNeighborsByKind();
    that.resetRemovedBlocksToCheckForSquaresIndexes();
    this.findingPathThresholds = this.findingPathThresholds.map(function (value) {
      return value + 2;
    });
    this.setMissions();
    startingRegion.blocks.forEach(function (row) {
      row.forEach(function (block) {
        if (block.kind.toString() === 'plain' && block.level === 0) {
          var path = that.findPathsRecursively(
            //TODO: limit depending on resources in the storage
            that.map, block, that.improvedBlockNeighborsByKind, [], [], playerStorage, 0, 5);
          if (bestPath === null || path.pathLength > bestPath.pathLength ||
              (path.pathLength === bestPath.pathLength && path.pathValue > bestPath.pathValue)) {
            bestPath = path;
          }
        }
      });
    });
    this.findingPathThresholds = this.findingPathThresholds.map(function (value) {
      return value - 2;
    });

    return bestPath.block;
  };

  AI.prototype.findPathsRecursively = function (map, block, improvedBlockNeighborsByKind, parents,
                                                blocksToCheckForSquares, expectedStorage, iteration, limit) {
    ++iteration;
    var currentBlockValue = block.aiValueForPlayer[this.player.playerId] * block.kind.mainResourceLevel;

    var improvementGain = {};
    for (var gainResName in block.kind.baseResourceGain) {
      var gain = (block.kind.baseResourceGain[gainResName] + (+block.rich));
      currentBlockValue *= gain / (expectedStorage.resources[gainResName] + 1) * 3;
      currentBlockValue *= expectedStorage.resources[gainResName] < 2 ?
                           1 + (2 - expectedStorage.resources[gainResName]) / iteration : 1;
      improvementGain[gainResName] = gain;
    }
    currentBlockValue *= this.missionValues.resourceCollecting / 1000 *
      (this.currentMission === 'resourceCollecting' ? 1 : 0.33);

    if (block.aiValuesForPlayerMissions[this.player.playerId]['largeSquareMaking']) {
      var blockLargeSquareMakingFactor = block.aiValuesForPlayerMissions[this.player.playerId]['largeSquareMaking'];
      currentBlockValue += blockLargeSquareMakingFactor * this.missionValues.largeSquareMaking / 1000 *
        (this.currentMission === 'largeSquareMaking' ? 1 : 0.33);
    }

    var squarePathValueBonus = 1;
    var squareCoordsFactor = 1;
    var squareOtherBlocks = this.squaresMap.checkSmallestSquareWithAdditionalBlocks(block, this.player, blocksToCheckForSquares);
    if (squareOtherBlocks) {
      var squarePosBlockIdx = squareOtherBlocks.pop();
      if (this.aiSquaring.nextSmallestSquareCoords) {
        var squarePosBlock = squarePosBlockIdx === -1 ? block : squareOtherBlocks[squarePosBlockIdx];
        var dist = this.map.getDistance(squarePosBlock, this.aiSquaring.nextSmallestSquareCoords.block);
        if (dist.x % 2 !== 0 || dist.y % 2 !== 0) {
          var distAbs = Math.abs(dist.x) + Math.abs(dist.y) + 60;
          squareCoordsFactor = 1 - 0.9375 / (distAbs / 64);  // value in range (0.04, 1)
          // soften squareCoordsFactor slightly by aiSquaring.nextSmallestSquareCoords.value
          squareCoordsFactor += (1 - squareCoordsFactor) / 16 / this.aiSquaring.nextSmallestSquareCoords.value;
          // and increase its impact
          squareCoordsFactor = Math.pow(squareCoordsFactor, 1.5); //TODO: 1.75
        }
      }
      squarePathValueBonus = 1 + Math.pow(block.kind.baseAiValue, 3 + (+block.rich) * 2) / 8;
      for (var i7 = 0; i7 <= 2; ++i7) {
        var idx = blocksToCheckForSquares.indexOf(squareOtherBlocks[i7]);
        if (idx > -1) {
          blocksToCheckForSquares.splice(idx, 1);
        }
        this.removedBlocksToCheckForSquaresIndexes[i7] = idx;
      }
    }

    var maxChildValue = 0;
    var findingPathThreshold = this.findingPathThresholds[iteration];
    var pathLength = iteration - 1;
    var childPathCount = 0;

    if (iteration <= limit) {
      expectedStorage.take(block.kind.baseResourceCost);
      expectedStorage.put(improvementGain);
      // how about gold ?

      parents.push(block);
      if (!squareOtherBlocks) {
        blocksToCheckForSquares.push(block);
      }

      var isLastIteration = iteration == limit;
      var addedImprovedBlockNeighborsByKindIndexes = {};

      // adding block neighbors to improvedBlockNeighborsByKind
      for (var kind in block.neighborsByKind) {
        addedImprovedBlockNeighborsByKindIndexes[kind] = [];
        var neighborBlocks = block.neighborsByKind[kind];
        for (var i1 = neighborBlocks.length - 1; i1 >= 0; --i1) {
          if (parents.indexOf(neighborBlocks[i1]) === -1 && neighborBlocks[i1].level === 0 &&
              improvedBlockNeighborsByKind[kind].indexOf(neighborBlocks[i1]) === -1) {
            var i3 = -1;
            var len = improvedBlockNeighborsByKind[kind].length;
            if (this.currentMission === 'largeSquareMaking') {
              while(++i3 < len && (!isLastIteration || i3 <= findingPathThreshold) &&
                improvedBlockNeighborsByKind[kind][i3].aiValueForPlayer[this.player.playerId] +
                (improvedBlockNeighborsByKind[kind][i3].aiValuesForPlayerMissions[
                  this.player.playerId]['largeSquareMaking'] || 0) >
                neighborBlocks[i1].aiValueForPlayer[this.player.playerId] +
                (neighborBlocks[i1].aiValuesForPlayerMissions[
                  this.player.playerId]['largeSquareMaking'] || 0)) { }  // sorted desc
            } else {
              while(++i3 < len && (!isLastIteration || i3 <= findingPathThreshold) &&
                improvedBlockNeighborsByKind[kind][i3].aiValueForPlayer[this.player.playerId] >
                  neighborBlocks[i1].aiValueForPlayer[this.player.playerId]) { }  // sorted desc
            }

            improvedBlockNeighborsByKind[kind].splice(i3, 0, neighborBlocks[i1]);  // insert
            addedImprovedBlockNeighborsByKindIndexes[kind].push(i3);  // remember inserted position
          }
        }
      }

      // checking further basic resource paths
      for (var i5 = 0; i5 < expectedStorage.basicResources.length; i5++) {
        var basicResName = expectedStorage.basicResources[i5];
        if (expectedStorage.resources[basicResName] > 0) {
          var nextKind = this.nextKindByResName[basicResName];
          var len2 = improvedBlockNeighborsByKind[nextKind].length - 1;
          childPathCount += improvedBlockNeighborsByKind[nextKind].length;
          var limit2 = len2 > findingPathThreshold ? len2 - findingPathThreshold : 0;  // 0; //
          var childPath;
          if (isLastIteration) {  // when it's one before last iteration
            for (var i2 = len2; i2 >= limit2; --i2) {
              childPath = this.findPathsRecursively(
                map, improvedBlockNeighborsByKind[nextKind][i2], null, parents,
                blocksToCheckForSquares, expectedStorage, iteration, limit);
              maxChildValue = maxChildValue > childPath.pathValue ? maxChildValue : childPath.pathValue;
              pathLength = pathLength >= childPath.pathLength ? pathLength : childPath.pathLength;
            }
          } else {
            for (var i4 = len2; i4 >= limit2; --i4) {
              var nextBlock = improvedBlockNeighborsByKind[nextKind][i4];
              improvedBlockNeighborsByKind[nextKind].splice(i4, 1);  // delete
              childPath = this.findPathsRecursively(
                map, nextBlock, improvedBlockNeighborsByKind, parents,
                blocksToCheckForSquares, expectedStorage, iteration, limit);
              maxChildValue = maxChildValue > childPath.pathValue ? maxChildValue : childPath.pathValue;
              pathLength = pathLength >= childPath.pathLength ? pathLength : childPath.pathLength;
              improvedBlockNeighborsByKind[nextKind].splice(i4, 0, nextBlock);  // insert
            }
          }
        }
      }

      // checking further advanced resource paths
      if (expectedStorage.resources.food > 0) {
        for (var i9 = this.advancedMapKinds.length - 1; i9 >= 0; --i9) {
          var advKind = this.advancedMapKinds[i9];
          var advKindNeighborsCount = improvedBlockNeighborsByKind[advKind].length;
          if (advKindNeighborsCount > 0) {
            childPathCount += advKindNeighborsCount;
            var advLimit = advKindNeighborsCount - 1 > findingPathThreshold ?
                           advKindNeighborsCount - 1 - findingPathThreshold : 0;  // 0; //
            var advChildPath;
            if (isLastIteration) {  // when it's one before last iteration
              for (var j1 = advKindNeighborsCount - 1; j1 >= advLimit; --j1) {
                advChildPath = this.findPathsRecursively(
                  map, improvedBlockNeighborsByKind[advKind][j1], null, parents,
                  blocksToCheckForSquares, expectedStorage, iteration, limit);
                maxChildValue = maxChildValue > advChildPath.pathValue ? maxChildValue : advChildPath.pathValue;
                pathLength = pathLength >= advChildPath.pathLength ? pathLength : advChildPath.pathLength;
              }
            } else {
              for (var j2 = advKindNeighborsCount - 1; j2 >= advLimit; --j2) {
                var nextAdvBlock = improvedBlockNeighborsByKind[advKind][j2];
                improvedBlockNeighborsByKind[advKind].splice(j2, 1);  // delete
                advChildPath = this.findPathsRecursively(
                  map, nextAdvBlock, improvedBlockNeighborsByKind, parents,
                  blocksToCheckForSquares, expectedStorage, iteration, limit);
                maxChildValue = maxChildValue > advChildPath.pathValue ? maxChildValue : advChildPath.pathValue;
                pathLength = pathLength >= advChildPath.pathLength ? pathLength : advChildPath.pathLength;
                improvedBlockNeighborsByKind[advKind].splice(j2, 0, nextAdvBlock);  // insert
              }
            }
          }
        }
      }

      for (var kind_ in addedImprovedBlockNeighborsByKindIndexes) {
        for (var i6 = addedImprovedBlockNeighborsByKindIndexes[kind_].length - 1; i6 >= 0; --i6) {
          improvedBlockNeighborsByKind[kind_].splice(addedImprovedBlockNeighborsByKindIndexes[kind_][i6], 1);  // delete
        }
      }

      if (!squareOtherBlocks) {
        blocksToCheckForSquares.pop(block);
      }
      parents.pop();

      expectedStorage.put(block.kind.baseResourceCost);
      expectedStorage.take(improvementGain);
    }

    if (squareOtherBlocks) {
      for (var i8 = 2; i8 >= 0; --i8) {
        if (this.removedBlocksToCheckForSquaresIndexes[i8] > -1) {
          blocksToCheckForSquares.splice(this.removedBlocksToCheckForSquaresIndexes[i8], 0, squareOtherBlocks[i8]);
        }
      }
    }

    var distanceFactor = (limit - iteration + 1) / limit;  /* (0.0, 1.0) */
    var basePathValue = currentBlockValue + (block.rich ? currentBlockValue * distanceFactor : 0);
    return {
      block: block,
      pathLength: pathLength,
      pathValue: (basePathValue + maxChildValue / Math.pow(this.improvedBlocks.length, 0.25) / iteration +
        (squareOtherBlocks ? basePathValue * distanceFactor * squarePathValueBonus : 0) +
        basePathValue * childPathCount / (20 + this.improvedBlocks.length * 2)) * squareCoordsFactor
    };
  };

  AI.prototype._getNextKindByResName = function (resName) {
    return this.nextKindByResName[resName];
  };

  AI.prototype.updateImprovedBlockNeighborsByKind = function () {
    var that = this;
    that.resetImprovedBlockNeighborsByKind();
    this.map.improvementNeighbors[this.player.playerId].forEach(function (block) {
      if (block.kind.improvable) {
        that.improvedBlockNeighborsByKind[block.kind.toString()].push(block);
      }
    });
    var kind;
    if (this.currentMission === 'resourceCollecting') {
      for (kind in that.improvedBlockNeighborsByKind) {
        that.improvedBlockNeighborsByKind[kind].sort(function descOrder(a, b) {
          return b.aiValueForPlayer[that.player.playerId] - a.aiValueForPlayer[that.player.playerId];
        });
      }
    } else if (this.currentMission === 'largeSquareMaking') {
      for (kind in that.improvedBlockNeighborsByKind) {
        that.improvedBlockNeighborsByKind[kind].sort(function descOrderWithLargeSquareMaking(a, b) {
          return b.aiValueForPlayer[that.player.playerId] +
              b.aiValuesForPlayerMissions[that.player.playerId]['largeSquareMaking'] -
            a.aiValueForPlayer[that.player.playerId] +
              a.aiValuesForPlayerMissions[that.player.playerId]['largeSquareMaking'];
        });
      }
    }
  };

  AI.prototype.resetRemovedBlocksToCheckForSquaresIndexes = function () {
    this.removedBlocksToCheckForSquaresIndexes = new Int8Array([-1, -1, -1]);
  };

  AI.prototype.resetImprovedBlockNeighborsByKind = function () {
    this.improvedBlockNeighborsByKind = {
      plain: [],
      forest: [],
      hill: [],
      mountain: [],
      coalfield: [],
      wildHorses: [],
      goldDeposit: [],
      silverDeposit: [],
      gemstoneDeposit: []
    };
  };

  AI.prototype.getNextBlockToCapture = function () {
    var nextBlock;
    if (this.improvedBlocks.length === 0) {
      nextBlock = this.getFirstMove();
    } else {
      if (this.improvedBlocks.length >= 30) {
        this.findingPathThresholds[3] = 2;
        this.findingPathThresholds[4] = 1;
        this.findingPathThresholds[5] = 1;
      }
      var bestPath = null;
      var that = this;
      that.updateImprovedBlockNeighborsByKind();
      that.resetRemovedBlocksToCheckForSquaresIndexes();
      var playerStorage = storageProvider.getStorage(that.player);
      this.setMissions();
      for (var kind in that.improvedBlockNeighborsByKind) {
        var pathCounter = 0;
        for (var i = 0; i < that.improvedBlockNeighborsByKind[kind].length; ++i) {
          var block = that.improvedBlockNeighborsByKind[kind][i];
          if (block.affordableForPlayer(that.player)) {
            //TODO: limit depending on resources in the storage, improvementNeighbors and others
            //TODO: findingPathThresholds depending on resources in the storage, improvementNeighbors and others
            that.improvedBlockNeighborsByKind[kind].splice(i, 1);  // delete
            var path = that.findPathsRecursively(that.map, block, that.improvedBlockNeighborsByKind,
              [], [], playerStorage, 0, 4);
            that.improvedBlockNeighborsByKind[kind].splice(i, 0, block);  // insert

            if (bestPath === null || path.pathLength > bestPath.pathLength ||
                (path.pathLength === bestPath.pathLength && path.pathValue > bestPath.pathValue)) {
              bestPath = path;
              nextBlock = bestPath.block;
            }

            if (++pathCounter > that.findingPathThresholds[0]) {
              break;
            }
          }
        }
      }
      that.resetRemovedBlocksToCheckForSquaresIndexes();
    }
    return nextBlock;
  };

  AI.prototype.tryToCaptureBlock = function (attemptNumber /*=0*/) {
    attemptNumber = attemptNumber === undefined ? 0 : attemptNumber;
    var nextBlock = this.getNextBlockToCapture();
    if (nextBlock) {
      if(!this.map.tryToCaptureBlock(nextBlock, this.player)) {
        if (attemptNumber === 0) {
          return this.tryToCaptureBlock(attemptNumber + 1);
        } else {
          return false;
        }
      }
      return true;
    } else {
      console.warn('No more moves for player', this.player.playerId);
      return false;
    }
  };

  AI.prototype.nextMove = function () {
    if (!this.tryToCaptureBlock()) {
      this.player.defeated = true;
    }
  };

  AI.prototype.setBlockImproved = function (block) {
    this.improvedBlocks.push(block);
  };

  AI.prototype.unsetBlockImproved = function (block) {
    ArrayUtils.removeElement(this.improvedBlocks, block);
  };

  return AI;

}]);