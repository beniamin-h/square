'use strict';

angular.module('square')

.factory('AISquaring', ['mapDirections', function (mapDirections) {

  function AISquaring (player, technologies) {
    this.player = player;
    this.technologies = technologies;
  }

  AISquaring.prototype.map = null;
  AISquaring.prototype.squaresMap = null;
  AISquaring.prototype.player = null;
  AISquaring.prototype.technologies = null;
  AISquaring.prototype.nextSmallestSquareCoords = null;

  AISquaring.prototype.setMap = function (map) {
    this.map = map;
  };

  AISquaring.prototype.setSquaresMap = function (squaresMap) {
    this.squaresMap = squaresMap;
  };

  AISquaring.prototype.thereIsAdjacentSquare = function (square, direction) {
    return !!this.squaresMap.getSquare(
      this.player, square.level, square.regionX, square.regionY,
      square.x + mapDirections.directionToCoord[direction].x * square.getWidth(),
      square.y + mapDirections.directionToCoord[direction].y * square.getHeight());
  };

  AISquaring.prototype.getAdjacentDirections = function (direction) {
    var result = [];
    if (mapDirections.isNonDiagonal(direction)) {
      result.push(  // eg. up => right
        mapDirections.getNextNonDiagonal(direction));
      result.push(  // eg. up => left
        mapDirections.getPrevNonDiagonal(direction));
      result.push(  // eg. up => rightUp
        mapDirections.getNextDiagonal(direction));
      result.push(  // eg. up => leftUp
        mapDirections.getPrevDiagonal(direction));
    } else {  // diagonal direction
      result.push(  // eg. rightDown => down
        mapDirections.getPrevNonDiagonal(direction));
      result.push(  // eg. rightDown => right
        mapDirections.getNextNonDiagonal(direction));
    }
    return result;
  };

  AISquaring.prototype.check8directions = function (square) {
    var prevDirectionHasSquare = false;
    var secondPrevDirectionHasSquare = false;
    var upHasSquare = false;
    var interestingDirections = new Set();
    var sameLevelNeighborSquaresCount = 0;
    var withoutSquareDirections = [];
    var largerSquareInterestingDirectionsTmp = [];
    mapDirections.directions8.forEach(function (direction) {
      if (this.thereIsAdjacentSquare(square, direction)) {
        if (prevDirectionHasSquare) {
          if (mapDirections.isNonDiagonal(direction)) {  // eg. rightUp, right => up
            interestingDirections.add(mapDirections.getPrevNonDiagonal(direction));
          } else {  // eg. right, rightDown => down
            interestingDirections.add(mapDirections.getNextNonDiagonal(direction));
          }
        }
        if (secondPrevDirectionHasSquare) {
          if (mapDirections.isNonDiagonal(direction)) {  // eg. down, left => leftDown
            interestingDirections.add(mapDirections.getPrevDiagonal(direction));
          }
        }
        // last + first direction
        if (direction === 'up') {
          upHasSquare = true;
        } else if (direction === 'left' && upHasSquare) {
          interestingDirections.add('leftUp');
        } else if (direction === 'leftUp' && upHasSquare) {
          interestingDirections.add('left');
        }
        if (square.level > 2) {  // for larger squares
          largerSquareInterestingDirectionsTmp = this.getAdjacentDirections(direction);
        }
        ++sameLevelNeighborSquaresCount;
        prevDirectionHasSquare = true;
      } else { // direction does not have a square
        secondPrevDirectionHasSquare = prevDirectionHasSquare;
        prevDirectionHasSquare = false;
        withoutSquareDirections.push(direction);
      }
    }, this);
    return {
      sameLevelNeighborSquaresCount: sameLevelNeighborSquaresCount,
      interestingDirections: interestingDirections,
      largerSquareInterestingDirectionsTmp: largerSquareInterestingDirectionsTmp,
      withoutSquareDirections: withoutSquareDirections
    };
  };

  AISquaring.prototype.getLargerSquareInterestingDirections = function (
      withoutSquareDirections, interestingDirectionsTmp) {
    var lessInteresting = new Set();
    var moreInteresting = new Set();

    withoutSquareDirections.forEach(function (direction) {
      if (interestingDirectionsTmp.indexOf(direction) > -1) {
        moreInteresting.add(direction);
      } else if (mapDirections.isNonDiagonal(direction)) {
        lessInteresting.add(direction);
      }
    }, this);

    return {
      lessInteresting: lessInteresting,
      moreInteresting: moreInteresting
    };
  };

  AISquaring.prototype.getSquareNeighborBlocks = function (square, direction) {
    return this.map.getBlocks(square.regionX, square.regionY,
      square.x + mapDirections.directionToCoord[direction].x * square.getWidth(),
      square.y + mapDirections.directionToCoord[direction].y * square.getHeight(),
      square.getWidth(), square.getHeight());
  };

  AISquaring.prototype.checkNeighborBlocks = function (square, direction, neighborBlocksMap) {
    var neighborBlocks = this.getSquareNeighborBlocks(square, direction);
    var allBlocksImprovable = true;
    var notFittingBlocksCount = 0;
    var nonImprovableNeighborBlocksCount = 0;
    neighborBlocks.forEach(function (block) {
      if (block.square && block.square.level === square.level) {
        ++notFittingBlocksCount;
      }
      //TODO: what if block.level > square.level ?
      //TODO: and what if block.level < square.level ?
      if (!block.kind.improvable) {
        allBlocksImprovable = false;
        ++nonImprovableNeighborBlocksCount;
      }
      if (block.ownedBy && block.ownedBy !== this.player) {
        //TODO: what if block is already captured ?
      }
    }, this);
    neighborBlocksMap[direction] = neighborBlocks;
    return {
      allBlocksImprovable: allBlocksImprovable,
      notFittingBlocksCount: notFittingBlocksCount,
      nonImprovableNeighborBlocksCount: nonImprovableNeighborBlocksCount
    };
  };

  AISquaring.prototype.removeInterestingDirection = function (interestingDirections, direction) {
    interestingDirections.delete(direction);
    if (mapDirections.isNonDiagonal(direction)) {
      // if non diagonal direction contains non-improvable block
      // we have to remove also prev and next directions (both will be diagonal) from interestingDirections
      interestingDirections.delete(mapDirections.getPrevDiagonal(direction));
      interestingDirections.delete(mapDirections.getNextDiagonal(direction));
    }
  };

  AISquaring.prototype.removeLargerSquareInterestingDirection = function (
      largerSquareInterestingDirections, withoutSquareDirections, direction) {
    largerSquareInterestingDirections.delete(direction);
    var opposite = mapDirections.directionOpposites[direction];
    if (mapDirections.isNonDiagonal(direction)) {  // eg. down
      if (withoutSquareDirections.indexOf(mapDirections.getNextDiagonal(direction)) > -1) {
        // eg. leftDown does not have a square
        largerSquareInterestingDirections.delete(mapDirections.getNextDiagonal(direction));  // eg. down => leftDown
      } else {
        // eg. leftDown has square
        if (withoutSquareDirections.indexOf(opposite) > -1 &&
            withoutSquareDirections.indexOf(mapDirections.getPrevDiagonal(opposite)) > -1) {
          // eg. and neither up or leftUp has square
          largerSquareInterestingDirections.delete(mapDirections.getNextNonDiagonal(direction));  // eg. down => left
        }
      }
      if (withoutSquareDirections.indexOf(mapDirections.getPrevDiagonal(direction)) > -1) {
        // eg. rightDown does not have a square
        largerSquareInterestingDirections.delete(mapDirections.getPrevDiagonal(direction));  // eg. down => rightDown
      } else {
        // eg. rightDown has square
        if (withoutSquareDirections.indexOf(opposite) > -1 &&
            withoutSquareDirections.indexOf(mapDirections.getNextDiagonal(opposite)) > -1) {
          // eg. and neither up or rightUp have square
          largerSquareInterestingDirections.delete(mapDirections.getPrevNonDiagonal(direction));  // eg. down => right
        }
      }
    } else {  // diagonal direction
      // eg. leftDown
      var nextDiagonalDirection = mapDirections.getNextDiagonal(direction);
      var nextSecondNonDiagonalDirection = mapDirections.getNextNonDiagonal(nextDiagonalDirection);
      if (withoutSquareDirections.indexOf(nextDiagonalDirection) > -1 &&
          withoutSquareDirections.indexOf(nextSecondNonDiagonalDirection) > -1) {
        // eg. neither leftUp or up have square
        largerSquareInterestingDirections.delete(mapDirections.getNextNonDiagonal(direction));  // leftDown => left
      }
      // eg. leftDown
      var prevDiagonalDirection_ = mapDirections.getPrevDiagonal(direction);
      var prevSecondNonDiagonalDirection = mapDirections.getPrevNonDiagonal(prevDiagonalDirection_);
      if (withoutSquareDirections.indexOf(prevDiagonalDirection_) > -1 &&
          withoutSquareDirections.indexOf(prevSecondNonDiagonalDirection) > -1) {
        // eg. neither rightDown or right have square
        largerSquareInterestingDirections.delete(mapDirections.getPrevNonDiagonal(direction));  // leftDown => down
      }
    }
  };

  AISquaring.prototype.checkWithoutSquareDirections = function (
      square, withoutSquareDirections, largerSquareInterestingDirections,
      interestingDirections, largerSquareLessInterestingDirections) {
    var notFittingBlocksCount = 0;
    var nonImprovableNeighborBlocksCount = 0;
    var neighborBlocksMap = {};

    withoutSquareDirections.forEach(function (direction) {

      var checkResult = this.checkNeighborBlocks(square, direction, neighborBlocksMap);
      var allBlocksImprovable = checkResult.allBlocksImprovable;
      notFittingBlocksCount += checkResult.notFittingBlocksCount;
      nonImprovableNeighborBlocksCount += checkResult.nonImprovableNeighborBlocksCount;

      if (interestingDirections.has(direction)) {
        if (!allBlocksImprovable) {
          this.removeInterestingDirection(interestingDirections, direction);
        }
      }
      if (largerSquareLessInterestingDirections.has(direction)) {
        if (allBlocksImprovable || this.technologies.Drainage) {
          //TODO: if !allBlocksImprovable add non improvable neighbor blocks to Drainage todo list
        } else {
          largerSquareLessInterestingDirections.delete(direction);
        }
      }
      if (largerSquareInterestingDirections.has(direction)) {
        if (allBlocksImprovable || this.technologies.Drainage) {
          //TODO: if !allBlocksImprovable add non improvable neighbor blocks to Drainage todo list
        } else {
          this.removeLargerSquareInterestingDirection(
            largerSquareInterestingDirections, withoutSquareDirections, direction);
        }
      }
    }, this);

    return {
      notFittingBlocksCount: notFittingBlocksCount,
      neighborBlocksMap: neighborBlocksMap,
      nonImprovableNeighborBlocksCount: nonImprovableNeighborBlocksCount
    };
  };

  AISquaring.prototype.updateNextSmallestSquareCoords = function (
      square, sameLevelNeighborSquaresCount, notFittingSquaresAvg, nonImprovableNeighborBlocksCount) {
    var oldValue = this.nextSmallestSquareCoords ? this.nextSmallestSquareCoords.value : 0;
    var newValue = (sameLevelNeighborSquaresCount - notFittingSquaresAvg) / 4 +
      sameLevelNeighborSquaresCount / (nonImprovableNeighborBlocksCount + 1) + 0.25;
    if (newValue >= oldValue) {
      this.nextSmallestSquareCoords = {
        block: this.map.getBlock(square.regionX, square.regionY, square.x, square.y),
        value: newValue  // between 0.25 and 7.75
      };
    }
  };

  AISquaring.prototype.increaseInterestingNeighborBlocksAiValue = function (
      interestingDirections, neighborBlocksMap) {
    interestingDirections.forEach(function (direction) {
      neighborBlocksMap[direction].forEach(function (block) {
        block.aiValueForPlayer[this.player.playerId] *= 1.5;
      }, this);
    }, this);
  };

  AISquaring.prototype.increaseLargerSquareInterestingNeighborBlocksAiValue = function (
      square, interestingDirections, neighborBlocksMap, factor) {
    interestingDirections.forEach(function (direction) {
      neighborBlocksMap[direction].forEach(function (block) {
        var aiValuesForPlayerMissions = block.aiValuesForPlayerMissions[this.player.playerId];
        aiValuesForPlayerMissions.largeSquareMaking = aiValuesForPlayerMissions.largeSquareMaking === undefined ?
          0 : aiValuesForPlayerMissions.largeSquareMaking;
        aiValuesForPlayerMissions.largeSquareMaking += square.level * factor;  // for factor 36 = 108, 144, 180
                                                                               // for factor 18 = 54, 72, 90
      }, this);
    }, this);
  };

  AISquaring.prototype.notifyOfNewSquare = function (square) {
    //var notFittingBlocksCount = 0;
    //var neighborBlocksMap = {};
    //var nonImprovableNeighborBlocksCount = 0;
    var largerSquareLessInterestingDirections = new Set();
    var largerSquareInterestingDirections = new Set();

    //TODO: remove aiValues for largeSquareMaking missions after the square growth

    var checkResult = this.check8directions(square);
    var sameLevelNeighborSquaresCount = checkResult.sameLevelNeighborSquaresCount;
    var interestingDirections = checkResult.interestingDirections;
    var largerSquareInterestingDirectionsTmp = checkResult.largerSquareInterestingDirectionsTmp;
    var withoutSquareDirections = checkResult.withoutSquareDirections;

    if (square.level > 2) {
      var largerSquareDirectionsResult = this.getLargerSquareInterestingDirections(
        withoutSquareDirections, largerSquareInterestingDirectionsTmp);
      largerSquareLessInterestingDirections = largerSquareDirectionsResult.lessInteresting;
      largerSquareInterestingDirections = largerSquareDirectionsResult.moreInteresting;
    }

    var checkWithoutSquareDirsResult = this.checkWithoutSquareDirections(
      square, withoutSquareDirections, largerSquareInterestingDirections,
      interestingDirections, largerSquareLessInterestingDirections);

    var notFittingBlocksCount = checkWithoutSquareDirsResult.notFittingBlocksCount;
    var neighborBlocksMap = checkWithoutSquareDirsResult.neighborBlocksMap;
    var nonImprovableNeighborBlocksCount = checkWithoutSquareDirsResult.nonImprovableNeighborBlocksCount;

    var notFittingSquaresAvg = notFittingBlocksCount / square.getWidth() / square.getHeight();
    if (sameLevelNeighborSquaresCount >= notFittingSquaresAvg) {
      if (square.level === 2) {
        this.updateNextSmallestSquareCoords(square, sameLevelNeighborSquaresCount,
          notFittingSquaresAvg, nonImprovableNeighborBlocksCount);
      }

      this.increaseInterestingNeighborBlocksAiValue(interestingDirections, neighborBlocksMap);

      this.increaseLargerSquareInterestingNeighborBlocksAiValue(
        square, largerSquareInterestingDirections, neighborBlocksMap, 36);

      this.increaseLargerSquareInterestingNeighborBlocksAiValue(
        square, largerSquareLessInterestingDirections, neighborBlocksMap, 18);

    }
  };

  return AISquaring;

}]);