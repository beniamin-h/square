'use strict';

describe('AiSchema', function() {
  beforeEach(module('square'));

  var AI, AISchema, AIPlayer, Joi;

  beforeEach(inject(function (_AI_, _AISchema_, _AIPlayer_, _Joi_) {
    AI = _AI_;
    AISchema = _AISchema_;
    AIPlayer = _AIPlayer_;
    Joi = _Joi_;
  }));

  describe('a new instance', function () {

    it('validates against AI schema', function (done) {
      var player = new AIPlayer('player-0', {x: 0, y: 0});
      var ai = new AI(player);
      Joi.validate(ai, AISchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });
  });

});