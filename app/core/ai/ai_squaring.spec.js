'use strict';

describe('AiSquaring', function() {
  beforeEach(module('square'));

  var AISquaring, Square, initProvider, mapDirections, Region, mapProvider,
    playerProvider, storageProvider, Plain, Water, squareProvider;

  beforeEach(inject(function (_AISquaring_, _Square_, _initProvider_, _mapDirections_, _Region_, _mapProvider_,
                              _playerProvider_, _storageProvider_, _Plain_, _Water_, _squareProvider_) {
    AISquaring = _AISquaring_;
    Square = _Square_;
    initProvider = _initProvider_;
    mapDirections = _mapDirections_;
    Region = _Region_;
    mapProvider = _mapProvider_;

    playerProvider = _playerProvider_;
    storageProvider = _storageProvider_;
    Plain = _Plain_;
    Water = _Water_;
    squareProvider = _squareProvider_;
  }));

  function addAdjacentSquare(regionX, regionY, blockX, blockY, squareLvl, aiPlayer, direction) {
    squareProvider.getSquareFactory().saveSquare(squareProvider.getSquareFactory().createSquare(regionX, regionY,
      blockX + mapDirections.directionToCoord[direction].x * Square.getWidth(squareLvl),
      blockY + mapDirections.directionToCoord[direction].y * Square.getHeight(squareLvl),
      squareLvl, aiPlayer));
  }

  describe('thereIsAdjacentSquare', function () {

    describe('calls getSquare with proper parameters', function () {

      function doTest(mockedPlayer, squareRegionX, squareRegionY, squareX, squareY, squareLevel, direction) {
        var mockedTechnologies = jasmine.createSpy('technologies');
        var aiSquaring = new AISquaring(mockedPlayer, mockedTechnologies);
        aiSquaring.setSquaresMap({ getSquare: jasmine.createSpy('getSquare') });
        var square = new Square(squareRegionX, squareRegionY, squareX, squareY, [], squareLevel, mockedPlayer);
        aiSquaring.thereIsAdjacentSquare(square, direction);
        return aiSquaring.squaresMap.getSquare;
      }

      it('for up direction and square level = 2', function () {
        var mockedPlayer = jasmine.createSpy('player');
        var squareLevel = 2;
        var getSquareSpy = doTest(mockedPlayer, 3, 1, 5, 9, squareLevel, 'up');
        expect(getSquareSpy).toHaveBeenCalledWith(mockedPlayer, squareLevel, 3, 1, 5, 9 - 2);
      });

      it('for left direction and square level = 2', function () {
        var mockedPlayer = jasmine.createSpy('player');
        var squareLevel = 2;
        var getSquareSpy = doTest(mockedPlayer, 6, 7, 5, 9, squareLevel, 'left');
        expect(getSquareSpy).toHaveBeenCalledWith(mockedPlayer, squareLevel, 6, 7, 5 - 2, 9);
      });

      it('for rightDown direction and square level = 2', function () {
        var mockedPlayer = jasmine.createSpy('player');
        var squareLevel = 2;
        var getSquareSpy = doTest(mockedPlayer, 3, 1, 6, 5, squareLevel, 'rightDown');
        expect(getSquareSpy).toHaveBeenCalledWith(mockedPlayer, squareLevel, 3, 1, 6 + 2, 5 + 2);
      });

      it('for down direction and square level = 3', function () {
        var mockedPlayer = jasmine.createSpy('player');
        var squareLevel = 3;
        var getSquareSpy = doTest(mockedPlayer, 3, 1, 5, 9, squareLevel, 'down');
        expect(getSquareSpy).toHaveBeenCalledWith(mockedPlayer, squareLevel, 3, 1, 5, 9 + 4);
      });

      it('for leftDown direction and square level = 3', function () {
        var mockedPlayer = jasmine.createSpy('player');
        var squareLevel = 3;
        var getSquareSpy = doTest(mockedPlayer, 8, 2, 5, 9, squareLevel, 'leftDown');
        expect(getSquareSpy).toHaveBeenCalledWith(mockedPlayer, squareLevel, 8, 2, 5 - 4, 9 + 4);
      });

      it('for rightUp direction and square level = 4', function () {
        var mockedPlayer = jasmine.createSpy('player');
        var squareLevel = 4;
        var getSquareSpy = doTest(mockedPlayer, 8, 2, 5, 9, squareLevel, 'rightUp');
        expect(getSquareSpy).toHaveBeenCalledWith(mockedPlayer, squareLevel, 8, 2, 5 + 8, 9 - 8);
      });

      it('for left direction and square level = 5', function () {
        var mockedPlayer = jasmine.createSpy('player');
        var squareLevel = 5;
        var getSquareSpy = doTest(mockedPlayer, 8, 2, 5, 9, squareLevel, 'left');
        expect(getSquareSpy).toHaveBeenCalledWith(mockedPlayer, squareLevel, 8, 2, 5 - 16, 9);
      });

    });

    describe('returns', function () {

      beforeEach(function () {
        initProvider.initNewGame({ mapSize: 7 });
        const aiPlayer = playerProvider.getFirstAI();
        squareProvider.getSquareFactory().saveSquare(
          squareProvider.getSquareFactory().createSquare(0, 0, 6, 2, 2, aiPlayer));
      });

      describe('true if there is adjacent square', function () {

        it('on the left', function () {
          var result = playerProvider.getFirstAI().ai.aiSquaring.thereIsAdjacentSquare(
            new Square(0, 0, 8, 2, [], 2, playerProvider.getFirstAI()),
            'left');
          expect(result).toBeTrue();
        });

        it('on the left up', function () {
          var result = playerProvider.getFirstAI().ai.aiSquaring.thereIsAdjacentSquare(
            new Square(0, 0, 8, 4, [], 2, playerProvider.getFirstAI()),
            'leftUp');
          expect(result).toBeTrue();
        });

        it('below', function () {
          var result = playerProvider.getFirstAI().ai.aiSquaring.thereIsAdjacentSquare(
            new Square(0, 0, 6, 0, [], 2, playerProvider.getFirstAI()),
            'down');
          expect(result).toBeTrue();
        });

      });

      describe('false if there is no adjacent square', function () {

        it('on the right', function () {
          var result = playerProvider.getFirstAI().ai.aiSquaring.thereIsAdjacentSquare(
            new Square(0, 0, 8, 2, [], 2, playerProvider.getFirstAI()),
            'right');
          expect(result).toBeFalse();
        });

        it('on the left down', function () {
          var result = playerProvider.getFirstAI().ai.aiSquaring.thereIsAdjacentSquare(
            new Square(0, 0, 8, 1, [], 2, playerProvider.getFirstAI()),
            'leftDown');
          expect(result).toBeFalse();
        });

        it('belonging to the same player', function () {
          squareProvider.getSquareFactory().saveSquare(squareProvider.getSquareFactory().createSquare(1, 1, 0, 0, 2, playerProvider.getHuman()));
          var result = playerProvider.getFirstAI().ai.aiSquaring.thereIsAdjacentSquare(
            new Square(1, 1, 0, 0, [], 2, playerProvider.getFirstAI()),
            'down');
          expect(result).toBeFalse();
        });

        it('of the same level', function () {
          squareProvider.getSquareFactory().saveSquare(squareProvider.getSquareFactory().createSquare(1, 1, 0, 2, 3, playerProvider.getFirstAI()));
          var result = playerProvider.getFirstAI().ai.aiSquaring.thereIsAdjacentSquare(
            new Square(1, 1, 0, 0, [], 2, playerProvider.getFirstAI()),
            'down');
          expect(result).toBeFalse();
        });

      });

    });


  });

  describe('getAdjacentDirections', function () {

      describe('returns adjacent directions', function () {

        function prepareAISquaring() {
          var mockedPlayer = jasmine.createSpy('player');
          var mockedTechnologies = jasmine.createSpy('technologies');
          return new AISquaring(mockedPlayer, mockedTechnologies);
        }

        it('for up direction', function () {
          var aiSquaring = prepareAISquaring();
          var result = aiSquaring.getAdjacentDirections('up');
          expect(result).toContain('leftUp');
          expect(result).toContain('rightUp');
          expect(result).toContain('left');
          expect(result).toContain('right');
        });

        it('for left direction', function () {
          var aiSquaring = prepareAISquaring();
          var result = aiSquaring.getAdjacentDirections('left');
          expect(result).toContain('leftUp');
          expect(result).toContain('leftDown');
          expect(result).toContain('up');
          expect(result).toContain('down');
        });

        it('for rightUp direction', function () {
          var aiSquaring = prepareAISquaring();
          var result = aiSquaring.getAdjacentDirections('rightUp');
          expect(result).toContain('up');
          expect(result).toContain('right');
        });

        it('for leftDown direction', function () {
          var aiSquaring = prepareAISquaring();
          var result = aiSquaring.getAdjacentDirections('leftDown');
          expect(result).toContain('left');
          expect(result).toContain('down');
        });

      });

  });

  describe('check8directions', function () {

    it('returns largerSquareInterestingDirectionsTmp only for square level > 2', function () {
      var aiPlayer = playerProvider.addAI();
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(aiPlayer, mockedTechnologies);
      aiSquaring.thereIsAdjacentSquare = jasmine.createSpy('thereIsAdjacentSquare')
        .and.callFake(function () {
          return true;
        });

      var squareLvl = 2;
      var result = aiSquaring.check8directions(new Square(0, 0, 6, 2, [], squareLvl, aiPlayer));
      expect(result.largerSquareInterestingDirectionsTmp.length).toBe(0);

      squareLvl = 3;
      result = aiSquaring.check8directions(new Square(0, 0, 6, 2, [], squareLvl, aiPlayer));
      expect(result.largerSquareInterestingDirectionsTmp.length).toBeGreaterThan(0);

      squareLvl = 4;
      result = aiSquaring.check8directions(new Square(0, 0, 6, 2, [], squareLvl, aiPlayer));
      expect(result.largerSquareInterestingDirectionsTmp.length).toBeGreaterThan(0);

      squareLvl = 5;
      result = aiSquaring.check8directions(new Square(0, 0, 6, 2, [], squareLvl, aiPlayer));
      expect(result.largerSquareInterestingDirectionsTmp.length).toBeGreaterThan(0);

      squareLvl = 2;
      result = aiSquaring.check8directions(new Square(0, 0, 6, 2, [], squareLvl, aiPlayer));
      expect(result.largerSquareInterestingDirectionsTmp.length).toBe(0);
    });

    describe('returns sameLevelNeighborSquaresCount', function () {

      var aiPlayer;
      var map;

      beforeEach(function () {
        initProvider.initNewGame({ mapSize: 11 });
        map = mapProvider.getMap();
        aiPlayer = playerProvider.getFirstAI();
      });

      it('equals to 3 when there are 3 fitting neighbor squares of the same level', function () {
        var squareLvl = 2;
        var baseSquare = new Square(1, 1, 4, 4, [], squareLvl, aiPlayer);

        addAdjacentSquare(1, 1, 4, 4, squareLvl, aiPlayer, 'left');
        addAdjacentSquare(1, 1, 4, 4, squareLvl, aiPlayer, 'right');
        addAdjacentSquare(1, 1, 4, 4, squareLvl, aiPlayer, 'down');

        var result = aiPlayer.ai.aiSquaring.check8directions(baseSquare);
        expect(result.sameLevelNeighborSquaresCount).toBe(3);
      });

      it('equals to 4 when there are 4 fitting neighbor squares of the same level also diagonal', function () {
        var squareLvl = 3;
        var baseSquare = new Square(0, -1, 5, 4, [], squareLvl, aiPlayer);

        addAdjacentSquare(0, -1, 5, 4, squareLvl, aiPlayer, 'left');
        addAdjacentSquare(0, -1, 5, 4, squareLvl, aiPlayer, 'leftUp');
        addAdjacentSquare(0, -1, 5, 4, squareLvl, aiPlayer, 'leftDown');
        addAdjacentSquare(0, -1, 5, 4, squareLvl, aiPlayer, 'rightDown');

        var result = aiPlayer.ai.aiSquaring.check8directions(baseSquare);
        expect(result.sameLevelNeighborSquaresCount).toBe(4);
      });

      it('equals to 3 when there are 4 neighbor squares of the same level but one does not fit', function () {
        var squareLvl = 3;
        var baseSquare = new Square(0, 1, 5, 4, [], squareLvl, aiPlayer);

        addAdjacentSquare(0, 1, 5, 4, squareLvl, aiPlayer, 'left');
        addAdjacentSquare(0, 1, 5, 4, squareLvl, aiPlayer, 'up');
        addAdjacentSquare(0, 1, 5, 4, squareLvl, aiPlayer, 'rightDown');

        // not fitting
        squareProvider.getSquareFactory().saveSquare(squareProvider.getSquareFactory().createSquare(0, 1, 5 + 1 - 4, 4 + 4, squareLvl, aiPlayer));

        var result = aiPlayer.ai.aiSquaring.check8directions(baseSquare);
        expect(result.sameLevelNeighborSquaresCount).toBe(3);
      });

      it('equals to 2 when there are 3 neighbor fitting squares but one is different level', function () {
        var squareLvl = 4;
        var baseSquare = new Square(0, -1, 1, 0, [], squareLvl, aiPlayer);

        addAdjacentSquare(0, -1, 1, 0, squareLvl, aiPlayer, 'right');
        addAdjacentSquare(0, -1, 1, 0, squareLvl, aiPlayer, 'up');
        addAdjacentSquare(0, -1, 1, 0, squareLvl + 1, aiPlayer, 'down');

        var result = aiPlayer.ai.aiSquaring.check8directions(baseSquare);
        expect(result.sameLevelNeighborSquaresCount).toBe(2);
      });

    });

    describe('returns interestingDirections', function () {

      var aiPlayer;
      var map;

      beforeEach(function () {
        initProvider.initNewGame({ mapSize: 7 });
        map = mapProvider.getMap();
        aiPlayer = playerProvider.getFirstAI();
      });

      it('containing rightUp when there are 2 neighbor squares: up and right', function () {
        var squareLvl = 3;
        var baseSquare = new Square(0, 1, 3, 4, [], squareLvl, aiPlayer);

        addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'up');
        addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'right');

        var result = aiPlayer.ai.aiSquaring.check8directions(baseSquare);
        expect(result.interestingDirections.has('rightUp')).toBeTrue();
      });

      it('containing leftUp when there are 2 neighbor squares: up and left', function () {
        var squareLvl = 3;
        var baseSquare = new Square(0, 1, 5, 4, [], squareLvl, aiPlayer);

        addAdjacentSquare(0, 1, 5, 4, squareLvl, aiPlayer, 'up');
        addAdjacentSquare(0, 1, 5, 4, squareLvl, aiPlayer, 'left');

        var result = aiPlayer.ai.aiSquaring.check8directions(baseSquare);
        expect(result.interestingDirections.has('leftUp')).toBeTrue();
      });

      it('containing left when there are 2 neighbor squares: up and leftUp', function () {
        var squareLvl = 3;
        var baseSquare = new Square(0, 1, 5, 4, [], squareLvl, aiPlayer);

        addAdjacentSquare(0, 1, 5, 4, squareLvl, aiPlayer, 'up');
        addAdjacentSquare(0, 1, 5, 4, squareLvl, aiPlayer, 'leftUp');

        var result = aiPlayer.ai.aiSquaring.check8directions(baseSquare);
        expect(result.interestingDirections.has('left')).toBeTrue();
      });

      it('containing rightUp, left and down when there are 4 neighbor squares: ' +
         'up, right, rightDown and leftUp', function () {
        var squareLvl = 2;
        var baseSquare = new Square(0, 1, 3, 4, [], squareLvl, aiPlayer);

        addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'up');
        addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'right');
        addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'rightDown');
        addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'leftUp');

        var result = aiPlayer.ai.aiSquaring.check8directions(baseSquare);
        expect(result.interestingDirections.has('rightUp')).toBeTrue();
        expect(result.interestingDirections.has('left')).toBeTrue();
        expect(result.interestingDirections.has('down')).toBeTrue();
      });

      it('containing rightUp, leftUp, leftDown and rightDown when there are 4 neighbor squares: ' +
         'up, right, down and left', function () {
        var squareLvl = 2;
          var baseSquare = new Square(0, 1, 3, 4, [], squareLvl, aiPlayer);

          addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'up');
          addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'right');
          addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'down');
          addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'left');

        var result = aiPlayer.ai.aiSquaring.check8directions(baseSquare);
        expect(result.interestingDirections.has('rightUp')).toBeTrue();
        expect(result.interestingDirections.has('leftUp')).toBeTrue();
        expect(result.interestingDirections.has('leftDown')).toBeTrue();
        expect(result.interestingDirections.has('rightDown')).toBeTrue();
      });

      it('not containing rightUp when there is already a square in rightUp direction', function () {
        var squareLvl = 2;
        var baseSquare = new Square(0, 1, 3, 4, [], squareLvl, aiPlayer);

        addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'right');
        addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'rightDown');
        addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'leftUp');
        addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'rightUp');

        var result = aiPlayer.ai.aiSquaring.check8directions(baseSquare);
        expect(result.interestingDirections.has('rightUp')).toBeFalse();
      });

      it('not containing leftDown when there are 4 neighbor squares: up, right, rightDown and leftUp', function () {
        var squareLvl = 2;
        var baseSquare = new Square(0, 1, 3, 4, [], squareLvl, aiPlayer);

        addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'up');
        addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'right');
        addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'rightDown');
        addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'leftUp');

        var result = aiPlayer.ai.aiSquaring.check8directions(baseSquare);
        expect(result.interestingDirections.has('leftDown')).toBeFalse();
      });

      it('not containing any elements when there are 2 neighbor squares: rightDown and leftDown', function () {
        var squareLvl = 2;
        var baseSquare = new Square(0, 1, 3, 4, [], squareLvl, aiPlayer);

        addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'rightDown');
        addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'leftDown');

        var result = aiPlayer.ai.aiSquaring.check8directions(baseSquare);
        expect(result.interestingDirections.size).toBe(0);
      });

      it('not containing any elements when there are 4 neighbor squares: ' +
         'rightUp, rightDown, leftDown and leftUp', function () {
        var squareLvl = 2;
        var baseSquare = new Square(0, 1, 3, 4, [], squareLvl, aiPlayer);

        addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'rightUp');
        addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'rightDown');
        addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'leftDown');
        addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'leftUp');

        var result = aiPlayer.ai.aiSquaring.check8directions(baseSquare);
        expect(result.interestingDirections.size).toBe(0);
      });

    });

    describe('returns withoutSquareDirections', function () {

      var aiPlayer;
      var map;

      beforeEach(function () {
        initProvider.initNewGame({ mapSize: 7 });
        map = mapProvider.getMap();
        aiPlayer = playerProvider.getFirstAI();
      });

      it('containing 6 distinct elements when there are 2 neighbor squares: up and right', function () {
        var squareLvl = 3;
        var baseSquare = new Square(0, 1, 3, 4, [], squareLvl, aiPlayer);

        addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'up');
        addAdjacentSquare(0, 1, 3, 4, squareLvl, aiPlayer, 'right');

        var result = aiPlayer.ai.aiSquaring.check8directions(baseSquare);
        expect(result.withoutSquareDirections.length).toBe(6);
        expect(new Set(result.withoutSquareDirections).size).toBe(6);
      });

      it('containing 8 distinct elements when there are no neighbor squares', function () {
        var squareLvl = 3;
        var baseSquare = new Square(0, 1, 3, 4, [], squareLvl, aiPlayer);

        var result = aiPlayer.ai.aiSquaring.check8directions(baseSquare);
        expect(result.withoutSquareDirections.length).toBe(8);
        expect(new Set(result.withoutSquareDirections).size).toBe(8);
      });

      it('containing only 4 non diagonal directions when there are 4 neighbor squares ' +
         'in all diagonal directions', function () {
        var squareLvl = 3;
        var baseSquare = new Square(0, 1, 5, 4, [], squareLvl, aiPlayer);

        addAdjacentSquare(0, 1, 5, 4, squareLvl, aiPlayer, 'rightUp');
        addAdjacentSquare(0, 1, 5, 4, squareLvl, aiPlayer, 'rightDown');
        addAdjacentSquare(0, 1, 5, 4, squareLvl, aiPlayer, 'leftDown');
        addAdjacentSquare(0, 1, 5, 4, squareLvl, aiPlayer, 'leftUp');

        var result = aiPlayer.ai.aiSquaring.check8directions(baseSquare);
        expect(result.withoutSquareDirections.length).toBe(4);
        expect(result.withoutSquareDirections).toContain('up');
        expect(result.withoutSquareDirections).toContain('right');
        expect(result.withoutSquareDirections).toContain('down');
        expect(result.withoutSquareDirections).toContain('left');
      });

    });

  });

  describe('getLargerSquareInterestingDirections', function () {

    it('returns only without square directions', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      var withoutSquareDirections = ['up', 'left', 'leftDown', 'rightUp'];
      var interestingDirections = ['up', 'rightUp', 'right', 'rightDown',
                                   'down', 'down', 'down', 'down', 'down'];
      var result = aiSquaring.getLargerSquareInterestingDirections(withoutSquareDirections, interestingDirections);
      expect(result.lessInteresting.size).toBeGreaterThan(0);
      result.lessInteresting.forEach(function (direction) {
        expect(withoutSquareDirections).toContain(direction);
      });
      expect(result.moreInteresting.size).toBeGreaterThan(0);
      result.moreInteresting.forEach(function (direction) {
        expect(withoutSquareDirections).toContain(direction);
      });
    });

    it('returns given interesting directions as more interesting', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      var withoutSquareDirections = ['up', 'left', 'leftDown', 'rightUp'];
      var interestingDirections = ['up', 'rightUp', 'right', 'rightDown', 'rightDown'];
      var result = aiSquaring.getLargerSquareInterestingDirections(withoutSquareDirections, interestingDirections);
      expect(result.moreInteresting.size).toBeGreaterThan(0);
      result.moreInteresting.forEach(function (direction) {
        expect(interestingDirections).toContain(direction);
      });
    });

    it('returns directions different than given interesting directions as less interesting ' +
       'if they are non diagonal', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      var withoutSquareDirections = ['up', 'left', 'leftDown', 'rightUp'];
      var interestingDirections = ['up', 'rightUp', 'right', 'rightDown', 'rightDown'];
      var result = aiSquaring.getLargerSquareInterestingDirections(withoutSquareDirections, interestingDirections);
      expect(result.lessInteresting.size).toBeGreaterThan(0);
      result.lessInteresting.forEach(function (direction) {
        expect(interestingDirections).not.toContain(direction);
        expect(mapDirections.isNonDiagonal(direction)).toBeTrue();
      });
    });

    describe('always returns an object with two keys: "lessInteresting" and "moreInteresting"', function () {

      it('for no interestingDirections', function () {
        var mockedAiPlayer = jasmine.createSpy('aiPlayer');
        var mockedTechnologies = jasmine.createSpy('technologies');
        var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
        var withoutSquareDirections = ['up', 'left', 'leftDown', 'rightUp'];
        var interestingDirections = [];
        var result = aiSquaring.getLargerSquareInterestingDirections(withoutSquareDirections, interestingDirections);
        expect(result.lessInteresting).toBeDefined();
        expect(result.moreInteresting).toBeDefined();
      });

      it('for all possible interestingDirections', function () {
        var mockedAiPlayer = jasmine.createSpy('aiPlayer');
        var mockedTechnologies = jasmine.createSpy('technologies');
        var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
        var withoutSquareDirections = ['up', 'left', 'leftDown', 'rightUp'];
        var interestingDirections = ['up', 'rightUp', 'right', 'rightDown',
                                     'down', 'leftDown', 'left', 'leftUp'];
        var result = aiSquaring.getLargerSquareInterestingDirections(withoutSquareDirections, interestingDirections);
        expect(result.lessInteresting).toBeDefined();
        expect(result.moreInteresting).toBeDefined();
      });

      it('for no withoutSquareDirections', function () {
        var mockedAiPlayer = jasmine.createSpy('aiPlayer');
        var mockedTechnologies = jasmine.createSpy('technologies');
        var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
        var withoutSquareDirections = [];
        var interestingDirections = ['up', 'rightUp', 'left', 'leftUp'];
        var result = aiSquaring.getLargerSquareInterestingDirections(withoutSquareDirections, interestingDirections);
        expect(result.lessInteresting).toBeDefined();
        expect(result.moreInteresting).toBeDefined();
      });

      it('for all possible withoutSquareDirections', function () {
        var mockedAiPlayer = jasmine.createSpy('aiPlayer');
        var mockedTechnologies = jasmine.createSpy('technologies');
        var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
        var withoutSquareDirections = ['up', 'rightUp', 'right', 'rightDown',
                                     'down', 'leftDown', 'left', 'leftUp'];
        var interestingDirections = ['up', 'rightUp', 'rightUp', 'rightUp'];
        var result = aiSquaring.getLargerSquareInterestingDirections(withoutSquareDirections, interestingDirections);
        expect(result.lessInteresting).toBeDefined();
        expect(result.moreInteresting).toBeDefined();
      });

    });

  });

  describe('getSquareNeighborBlocks', function () {

    function prepareMap() {
      initProvider.initNewGame({ mapSize: 3 });
      return mapProvider.getMap();
    }

    describe('returns proper blocks', function () {

      var aiSquaring;
      var map;

      beforeEach(function () {
        var mockedAiPlayer = jasmine.createSpy('aiPlayer');
        var mockedTechnologies = jasmine.createSpy('technologies');
        aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
        map = prepareMap();
        aiSquaring.setMap(map);
      });

      it('for up direction', function () {
        var returnedBlocks = aiSquaring.getSquareNeighborBlocks(
          new Square(0, -1, 0, 4, [], 2, playerProvider.getFirstAI()), 'up');
        var expectedBlocks = [map.getBlock(0, -1, 0, 2), map.getBlock(0, -1, 1, 2),
                              map.getBlock(0, -1, 0, 3), map.getBlock(0, -1, 1, 3)];
        expect(returnedBlocks.length).toBe(4);
        expectedBlocks.forEach(function (block) {
          expect(returnedBlocks.indexOf(block)).not.toBe(-1);
        });
      });

      it('for up direction in map leftTop corner', function () {
        var returnedBlocks = aiSquaring.getSquareNeighborBlocks(
          new Square(-1, -1, 0, 0, [], 2, playerProvider.getFirstAI()), 'up');
        expect(returnedBlocks).toEqual([]);
      });

      it('for rightUp direction', function () {
        var returnedBlocks = aiSquaring.getSquareNeighborBlocks(
          new Square(0, 0, 1, 0, [], 2, playerProvider.getFirstAI()), 'rightUp');
        var expectedBlocks = [map.getBlock(0, 0, 3, -2), map.getBlock(0, 0, 4, -2),
                              map.getBlock(0, 0, 3, -1), map.getBlock(0, 0, 3, -1)];
        expect(returnedBlocks.length).toBe(4);
        expectedBlocks.forEach(function (block) {
          expect(returnedBlocks.indexOf(block)).not.toBe(-1);
        });
      });

      it('for rightUp direction in map leftTop corner', function () {
        var returnedBlocks = aiSquaring.getSquareNeighborBlocks(
          new Square(-1, -1, 0, 0, [], 2, playerProvider.getFirstAI()), 'rightUp');
        expect(returnedBlocks).toEqual([]);
      });

      it('for a square level 3', function () {
        var returnedBlocks = aiSquaring.getSquareNeighborBlocks(
          new Square(0, -1, 1, 2, [], 3, playerProvider.getFirstAI()), 'down');
        var expectedBlocks = map.getBlocks(0, -1, 1, 6, 4, 4);
        expect(returnedBlocks.length).toBe(16);
        expectedBlocks.forEach(function (block) {
          expect(returnedBlocks.indexOf(block)).not.toBe(-1);
        });
      });

      it('for a square level 4', function () {
        var returnedBlocks = aiSquaring.getSquareNeighborBlocks(
          new Square(0, -1, 4, 3, [], 4, playerProvider.getFirstAI()), 'leftDown');
        var expectedBlocks = map.getBlocks(0, -1, 4 - 8, 3 + 8, 8, 8);
        expect(returnedBlocks.length).toBe(8 * (3 * Region.blocksHeight - (3 + 8)));
        expectedBlocks.forEach(function (block) {
          expect(returnedBlocks.indexOf(block)).not.toBe(-1);
        });
      });

    });

  });

  describe('checkNeighborBlocks', function () {

    var map;

    beforeEach(function () {
      initProvider.initNewGame({ mapSize: 3 });
      map = mapProvider.getMap();
      map.getBlocks(-1, -1, 0, 0, 15, 15).forEach(function setAllBlocksToBeImprovable(block) {
        if (!block.kind.improvable) {
          map.changeBlockKind(block, new Plain());
        }
      });
    });

    it('returns allBlocksImprovable: true if all blocks are improvable', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      aiSquaring.setMap(map);
      var result = aiSquaring.checkNeighborBlocks(new Square(0, 0, 0, 0, [], 3, mockedAiPlayer), 'leftDown', {});
      expect(result.allBlocksImprovable).toBeTrue();
      var result2 = aiSquaring.checkNeighborBlocks(new Square(0, 0, 0, 0, [], 3, mockedAiPlayer), 'right', {});
      expect(result2.allBlocksImprovable).toBeTrue();
    });

    it('returns allBlocksImprovable: false if at least one block is non improvable', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      aiSquaring.setMap(map);
      map.changeBlockKind(map.getBlock(0, 1, 0, 0), new Water());
      var result = aiSquaring.checkNeighborBlocks(new Square(0, 0, 0, 0, [], 3, mockedAiPlayer), 'down', {});
      expect(result.allBlocksImprovable).toBeFalse();
    });

    it('returns allBlocksImprovable: false if all blocks are non improvable', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      aiSquaring.setMap(map);
      map.getBlocks(0, -1, 0, 0, 5, 5).forEach(function setAllBlocksToBeNonImprovable(block) {
        map.changeBlockKind(block, new Water());
      });
      var result = aiSquaring.checkNeighborBlocks(new Square(0, 0, 0, 0, [], 3, mockedAiPlayer), 'up', {});
      expect(result.allBlocksImprovable).toBeFalse();
    });

    it('returns notFittingBlocksCount: 4 if there are 4 not fitting blocks of the same level in the given direction', function () {
      var aiPlayer = playerProvider.getFirstAI();
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(aiPlayer, mockedTechnologies);
      aiSquaring.setMap(map);
      squareProvider.getSquareFactory().saveSquare(squareProvider.getSquareFactory().createSquare(-1, -1, 3, 4, 3, aiPlayer));
      var result = aiSquaring.checkNeighborBlocks(new Square(-1, -1, 0, 0, [], 3, aiPlayer), 'down', {});
      expect(result.notFittingBlocksCount).toBe(4);
    });

    it('returns notFittingBlocksCount: 0 if there are no not fitting blocks', function () {
      var aiPlayer = playerProvider.getFirstAI();
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(aiPlayer, mockedTechnologies);
      aiSquaring.setMap(map);
      squareProvider.getSquareFactory().saveSquare(squareProvider.getSquareFactory().createSquare(0, 0, 4, 4, 3, aiPlayer));
      var result = aiSquaring.checkNeighborBlocks(new Square(-1, -1, 0, 0, [], 3, aiPlayer), 'down', {});
      expect(result.notFittingBlocksCount).toBe(0);
    });

    it('returns nonImprovableNeighborBlocksCount: 1 if there is 1 non-improvable neighbor block', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      aiSquaring.setMap(map);
      map.changeBlockKind(map.getBlock(0, -1, 3, 4), new Water());
      var result = aiSquaring.checkNeighborBlocks(new Square(0, 0, 0, 0, [], 3, mockedAiPlayer), 'up', {});
      expect(result.nonImprovableNeighborBlocksCount).toBe(1);
    });

    it('returns nonImprovableNeighborBlocksCount: 4 if there are 4 non-improvable neighbor blocks', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      aiSquaring.setMap(map);
      map.changeBlockKind(map.getBlock(0, 0, 4, 0), new Water());
      map.changeBlockKind(map.getBlock(0, 0, 4, 1), new Water());
      map.changeBlockKind(map.getBlock(1, 0, 0, 0), new Water());
      map.changeBlockKind(map.getBlock(1, 0, 2, 2), new Water());
      var result = aiSquaring.checkNeighborBlocks(new Square(0, 0, 0, 0, [], 3, mockedAiPlayer), 'right', {});
      expect(result.nonImprovableNeighborBlocksCount).toBe(4);
    });

    it('returns nonImprovableNeighborBlocksCount: 0 if there are no non-improvable neighbor blocks', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      aiSquaring.setMap(map);
      map.changeBlockKind(map.getBlock(0, 0, 4, 0), new Water());
      map.changeBlockKind(map.getBlock(0, 0, 4, 1), new Water());
      map.changeBlockKind(map.getBlock(0, 0, 0, 0), new Water());
      map.changeBlockKind(map.getBlock(0, 0, 2, 2), new Water());
      var result = aiSquaring.checkNeighborBlocks(new Square(1, 1, 0, 0, [], 3, mockedAiPlayer), 'down', {});
      expect(result.nonImprovableNeighborBlocksCount).toBe(0);
    });

    it('always returns an object with 3 keys: ' +
       '"allBlocksImprovable", "notFittingBlocksCount", "nonImprovableNeighborBlocksCount"', function () {
      var aiPlayer = playerProvider.getFirstAI();
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(aiPlayer, mockedTechnologies);
      aiSquaring.setMap(map);
      var result = aiSquaring.checkNeighborBlocks(new Square(1, 1, 4, 3, [], 5, aiPlayer), 'rightUp', {});
      expect(Object.keys(result)).toContain('allBlocksImprovable');
      expect(Object.keys(result)).toContain('notFittingBlocksCount');
      expect(Object.keys(result)).toContain('nonImprovableNeighborBlocksCount');
    });

    it('updates neighborBlocksMap with blocks of the given direction', function () {
      var aiPlayer = playerProvider.getFirstAI();
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(aiPlayer, mockedTechnologies);
      aiSquaring.setMap(map);
      var neighborBlocksMap = {'something': 'toBeHere'};
      aiSquaring.checkNeighborBlocks(new Square(0, -1, 1, 1, [], 3, aiPlayer), 'leftDown', neighborBlocksMap);
      expect(neighborBlocksMap.leftDown).toBeDefined();
      expect(neighborBlocksMap.something).toBe('toBeHere');
      map.getBlocks(-1, 0, 2, 0, 4, 4).forEach(function (block) {
        expect(neighborBlocksMap.leftDown.indexOf(block)).not.toBe(-1);
      });
    });

  });

  describe('removeInterestingDirection', function () {

    it('removes the given direction from the given interestingDirections', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      var interestingDirections = new Set(['up', 'right', 'rightDown', 'down', 'leftDown']);
      aiSquaring.removeInterestingDirection(interestingDirections, 'rightDown');
      expect(interestingDirections.has('rightDown')).toBeFalse();
      expect(interestingDirections.has('up')).toBeTrue();
      aiSquaring.removeInterestingDirection(interestingDirections, 'up');
      expect(interestingDirections.has('up')).toBeFalse();
    });

    it('removes adjacent directions if given direction is non diagonal', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      var interestingDirections = new Set(['up', 'right', 'rightDown', 'down', 'leftDown']);
      aiSquaring.removeInterestingDirection(interestingDirections, 'down');
      expect(interestingDirections.has('rightDown')).toBeFalse();
      expect(interestingDirections.has('leftDown')).toBeFalse();
      expect(interestingDirections.has('down')).toBeFalse();
      expect(interestingDirections.has('right')).toBeTrue();
    });

    it('does not remove adjacent directions if given direction is diagonal', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      var interestingDirections = new Set(['up', 'right', 'rightDown', 'down', 'leftDown']);
      aiSquaring.removeInterestingDirection(interestingDirections, 'rightDown');
      expect(interestingDirections.has('rightDown')).toBeFalse();
      expect(interestingDirections.has('down')).toBeTrue();
      expect(interestingDirections.has('right')).toBeTrue();
      expect(interestingDirections.has('leftDown')).toBeTrue();
    });

  });

  describe('removeLargerSquareInterestingDirection', function () {

    it('removes the given direction from the given largerSquareInterestingDirections', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      var largerSquareInterestingDirections = new Set(['up', 'right', 'rightDown', 'down', 'leftDown']);
      aiSquaring.removeLargerSquareInterestingDirection(largerSquareInterestingDirections, [], 'right');
      expect(largerSquareInterestingDirections.has('right')).toBeFalse();
    });

    it('removes empty (w/o square) adjacent directions if the given direction is non diagonal', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      var largerSquareInterestingDirections = new Set(['up', 'rightUp', 'right', 'rightDown', 'down', 'leftDown']);
      var withoutSquareDirections = ['rightUp', 'rightDown'];
      aiSquaring.removeLargerSquareInterestingDirection(
        largerSquareInterestingDirections, withoutSquareDirections, 'right');
      expect(largerSquareInterestingDirections.has('rightUp')).toBeFalse();
      expect(largerSquareInterestingDirections.has('rightDown')).toBeFalse();
    });

    it('removes next and prev non diagonal directions (up, down) ' +
       'if the given direction (right) is non diagonal ' +
       'and adjacent directions (rightUp, rightDown) has a square and ' +
       'there are no other squares interested in these directions', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      var largerSquareInterestingDirections = new Set(['up', 'rightUp', 'right', 'rightDown', 'down', 'leftDown']);
      var withoutSquareDirections = ['right', 'up', 'leftUp', 'left', 'leftDown', 'down'];
      // rightUp and rightDown has a square
      aiSquaring.removeLargerSquareInterestingDirection(
        largerSquareInterestingDirections, withoutSquareDirections, 'right');
      expect(largerSquareInterestingDirections.has('up')).toBeFalse();
      expect(largerSquareInterestingDirections.has('down')).toBeFalse();
    });

    it('does not remove next and prev non diagonal directions (up, down) ' +
       'if the given direction (right) is non diagonal ' +
       'and adjacent directions (rightUp, rightDown) has a square ' +
       'but there is another square (left) interested in these directions', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      var largerSquareInterestingDirections = new Set(['up', 'rightUp', 'right', 'rightDown', 'down', 'leftDown']);
      var withoutSquareDirections = ['right', 'up', 'leftUp', 'leftDown', 'down'];
      // left, rightUp and rightDown has a square
      aiSquaring.removeLargerSquareInterestingDirection(
        largerSquareInterestingDirections, withoutSquareDirections, 'right');
      expect(largerSquareInterestingDirections.has('up')).toBeTrue();
      expect(largerSquareInterestingDirections.has('down')).toBeTrue();
    });

    it('does not remove next and prev non diagonal directions (up, down) ' +
       'if the given direction (right) is non diagonal ' +
       'and adjacent directions (rightUp, rightDown) has a square ' +
       'but there are 2 other squares (leftUp, leftDown) interested in these directions', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      var largerSquareInterestingDirections = new Set(['up', 'right', 'down', 'left']);
      var withoutSquareDirections = ['right', 'up', 'left', 'down'];
      // leftUp, leftDown, rightUp and rightDown has a square
      aiSquaring.removeLargerSquareInterestingDirection(
        largerSquareInterestingDirections, withoutSquareDirections, 'right');
      expect(largerSquareInterestingDirections.has('up')).toBeTrue();
      expect(largerSquareInterestingDirections.has('down')).toBeTrue();
    });

    it('removes next non diagonal direction (down) ' +
       'if the given direction (right) is non diagonal ' +
       'and one of adjacent directions (rightDown) has a square ' +
       'but there are no other squares interested in this direction', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      var largerSquareInterestingDirections = new Set(['up', 'right', 'down', 'left']);
      var withoutSquareDirections = ['right', 'rightUp', 'up', 'leftUp', 'left', 'leftDown', 'down'];
      // rightDown has a square
      aiSquaring.removeLargerSquareInterestingDirection(
        largerSquareInterestingDirections, withoutSquareDirections, 'right');
      expect(largerSquareInterestingDirections.has('up')).toBeTrue();
      expect(largerSquareInterestingDirections.has('down')).toBeFalse();
    });

    it('removes adjacent directions (up, left) ' +
       'if the given direction (leftUp) is diagonal ' +
       'and there are no other squares', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      var largerSquareInterestingDirections = new Set(['up', 'right', 'down', 'left']);
      var withoutSquareDirections = ['right', 'rightUp', 'up', 'leftUp', 'left', 'leftDown', 'down', 'rightDown'];
      // no square
      aiSquaring.removeLargerSquareInterestingDirection(
        largerSquareInterestingDirections, withoutSquareDirections, 'leftUp');
      expect(largerSquareInterestingDirections.has('up')).toBeFalse();
      expect(largerSquareInterestingDirections.has('left')).toBeFalse();
    });

    it('removes one of adjacent directions (left) ' +
       'if the given direction (leftUp) is diagonal ' +
       'and there is a square in right direction', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      var largerSquareInterestingDirections = new Set(['up', 'right', 'down', 'left']);
      var withoutSquareDirections = ['rightUp', 'up', 'leftUp', 'left', 'leftDown', 'down', 'rightDown'];
      // right has a square
      aiSquaring.removeLargerSquareInterestingDirection(
        largerSquareInterestingDirections, withoutSquareDirections, 'leftUp');
      expect(largerSquareInterestingDirections.has('up')).toBeTrue();
      expect(largerSquareInterestingDirections.has('left')).toBeFalse();
    });

    it('removes one of adjacent directions (left) ' +
       'if the given direction (leftUp) is diagonal ' +
       'and there is a square in rightUp direction', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      var largerSquareInterestingDirections = new Set(['up', 'right', 'down', 'left']);
      var withoutSquareDirections = ['right', 'up', 'leftUp', 'left', 'leftDown', 'down', 'rightDown'];
      // rightUp has a square
      aiSquaring.removeLargerSquareInterestingDirection(
        largerSquareInterestingDirections, withoutSquareDirections, 'leftUp');
      expect(largerSquareInterestingDirections.has('up')).toBeTrue();
      expect(largerSquareInterestingDirections.has('left')).toBeFalse();
    });

    it('does not remove any direction ' +
       'if the given direction (leftUp) is diagonal ' +
       'and there are 2 squares in rightUp and down directions', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      var largerSquareInterestingDirections = new Set(['up', 'right', 'down', 'left']);
      var withoutSquareDirections = ['right', 'up', 'leftUp', 'left', 'leftDown', 'rightDown'];
      // rightUp and down has a square
      aiSquaring.removeLargerSquareInterestingDirection(
        largerSquareInterestingDirections, withoutSquareDirections, 'leftUp');
      expect(largerSquareInterestingDirections.has('up')).toBeTrue();
      expect(largerSquareInterestingDirections.has('left')).toBeTrue();
    });

    it('does not remove any direction ' +
       'if the given direction (leftUp) is diagonal ' +
       'and there are 2 squares in right and leftDown directions', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      var largerSquareInterestingDirections = new Set(['up', 'right', 'down', 'left']);
      var withoutSquareDirections = ['rightUp', 'up', 'leftUp', 'left', 'down', 'rightDown'];
      // right and leftDown has a square
      aiSquaring.removeLargerSquareInterestingDirection(
        largerSquareInterestingDirections, withoutSquareDirections, 'leftUp');
      expect(largerSquareInterestingDirections.has('up')).toBeTrue();
      expect(largerSquareInterestingDirections.has('left')).toBeTrue();
    });

  });

  describe('checkWithoutSquareDirections', function () {

    it('returns notFittingBlocksCount summed up from all checkNeighborBlocks call results', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      spyOn(aiSquaring, 'checkNeighborBlocks').and.returnValue({
        allBlocksImprovable: true,
        notFittingBlocksCount: 9,
        nonImprovableNeighborBlocksCount: 0
      });
      var result = aiSquaring.checkWithoutSquareDirections(
        jasmine.createSpy('square'), ['fakeDirection1', 'fakeDirection2', 'fakeDirection3'],
        new Set(), new Set(), new Set());  // 3 without square directions
      expect(result.notFittingBlocksCount).toBe(3 * 9);
    });

    it('returns nonImprovableNeighborBlocksCount summed up from all checkNeighborBlocks call results', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      spyOn(aiSquaring, 'checkNeighborBlocks').and.returnValue({
        allBlocksImprovable: true,
        notFittingBlocksCount: 0,
        nonImprovableNeighborBlocksCount: 7
      });
      var result = aiSquaring.checkWithoutSquareDirections(
        jasmine.createSpy('square'), ['fakeDirection1', 'fakeDirection2', 'fakeDirection3', 'fakeDirection4'],
        new Set(), new Set(), new Set());  // 4 without square directions
      expect(result.nonImprovableNeighborBlocksCount).toBe(4 * 7);
    });

    it('returns neighborBlocksMap filled by checkNeighborBlocks calls', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      var fakeMapValue = jasmine.createSpy('fakeMapValue');
      spyOn(aiSquaring, 'checkNeighborBlocks').and.callFake(function (square, direction, neighborBlocksMap) {
        neighborBlocksMap[direction] = fakeMapValue;
        return {};
      });
      var result = aiSquaring.checkWithoutSquareDirections(
        jasmine.createSpy('square'), ['fakeDirection1', 'fakeDirection2'],
        new Set(), new Set(), new Set());  // 4 without square directions
      expect(result.neighborBlocksMap).toEqual(jasmine.objectContaining({
        fakeDirection1: fakeMapValue,
        fakeDirection2: fakeMapValue
      }));
    });

    it('removes direction (fakeDirection2) from interestingDirections ' +
       'if not all its blocks are improvable', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      spyOn(aiSquaring, 'checkNeighborBlocks').and.callFake(function (square, direction) {
        return {
          allBlocksImprovable: direction !== 'fakeDirection2',
          notFittingBlocksCount: 0,
          nonImprovableNeighborBlocksCount: 0
        };
      });
      var interestingDirections = new Set(['fakeDirection1', 'fakeDirection2', 'fakeDirection3', 'fakeDirection4']);
      aiSquaring.checkWithoutSquareDirections(
        jasmine.createSpy('square'), ['fakeDirection1', 'fakeDirection2', 'fakeDirection3', 'fakeDirection4'],
        new Set(), interestingDirections, new Set());  // 4 without square directions
      expect(interestingDirections.has('fakeDirection2')).toBeFalse();
    });

    it('does not remove direction from interestingDirections if all its blocks are improvable', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      spyOn(aiSquaring, 'checkNeighborBlocks').and.callFake(function (square, direction) {
        return {
          allBlocksImprovable: direction !== 'fakeDirection2',
          notFittingBlocksCount: 0,
          nonImprovableNeighborBlocksCount: 0
        };
      });
      var interestingDirections = new Set(['fakeDirection1', 'fakeDirection2', 'fakeDirection3', 'fakeDirection4']);
      aiSquaring.checkWithoutSquareDirections(
        jasmine.createSpy('square'), ['fakeDirection1', 'fakeDirection2', 'fakeDirection3', 'fakeDirection4'],
        new Set(), interestingDirections, new Set());  // 4 without square directions
      expect(interestingDirections.has('fakeDirection1')).toBeTrue();
      expect(interestingDirections.has('fakeDirection3')).toBeTrue();
      expect(interestingDirections.has('fakeDirection4')).toBeTrue();
    });

    it('removes direction (fakeDirection3) from largerSquareLessInterestingDirections ' +
       'if not all its blocks are improvable', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      spyOn(aiSquaring, 'checkNeighborBlocks').and.callFake(function (square, direction) {
        return {
          allBlocksImprovable: direction !== 'fakeDirection3',
          notFittingBlocksCount: 0,
          nonImprovableNeighborBlocksCount: 0
        };
      });
      var largerSquareLessInterestingDirections = new Set([
        'fakeDirection1', 'fakeDirection2', 'fakeDirection3', 'fakeDirection4']);
      aiSquaring.checkWithoutSquareDirections(
        jasmine.createSpy('square'), ['fakeDirection1', 'fakeDirection2', 'fakeDirection3', 'fakeDirection4'],
        new Set(), new Set(), largerSquareLessInterestingDirections);  // 4 without square directions
      expect(largerSquareLessInterestingDirections.has('fakeDirection3')).toBeFalse();
    });

    it('does not remove direction from largerSquareLessInterestingDirections ' +
       'if all its blocks are improvable', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      spyOn(aiSquaring, 'checkNeighborBlocks').and.callFake(function (square, direction) {
        return {
          allBlocksImprovable: direction !== 'fakeDirection3',
          notFittingBlocksCount: 0,
          nonImprovableNeighborBlocksCount: 0
        };
      });
      var largerSquareLessInterestingDirections = new Set([
        'fakeDirection1', 'fakeDirection2', 'fakeDirection3', 'fakeDirection4']);
      aiSquaring.checkWithoutSquareDirections(
        jasmine.createSpy('square'), ['fakeDirection1', 'fakeDirection2', 'fakeDirection3', 'fakeDirection4'],
        new Set(), new Set(), largerSquareLessInterestingDirections);  // 4 without square directions
      expect(largerSquareLessInterestingDirections.has('fakeDirection1')).toBeTrue();
      expect(largerSquareLessInterestingDirections.has('fakeDirection2')).toBeTrue();
      expect(largerSquareLessInterestingDirections.has('fakeDirection4')).toBeTrue();
    });

    it('removes direction (fakeDirection1) from largerSquareInterestingDirections ' +
       'if not all its blocks are improvable', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      spyOn(aiSquaring, 'checkNeighborBlocks').and.callFake(function (square, direction) {
        return {
          allBlocksImprovable: direction !== 'fakeDirection1',
          notFittingBlocksCount: 0,
          nonImprovableNeighborBlocksCount: 0
        };
      });
      var largerSquareInterestingDirections = new Set([
        'fakeDirection1', 'fakeDirection2', 'fakeDirection3', 'fakeDirection4']);
      aiSquaring.checkWithoutSquareDirections(
        jasmine.createSpy('square'), ['fakeDirection1', 'fakeDirection2', 'fakeDirection3', 'fakeDirection4'],
        largerSquareInterestingDirections, new Set(), new Set());  // 4 without square directions
      expect(largerSquareInterestingDirections.has('fakeDirection1')).toBeFalse();
    });

    it('does not remove direction from largerSquareInterestingDirections ' +
       'if all its blocks are improvable', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      spyOn(aiSquaring, 'checkNeighborBlocks').and.callFake(function (square, direction) {
        return {
          allBlocksImprovable: direction !== 'fakeDirection1',
          notFittingBlocksCount: 0,
          nonImprovableNeighborBlocksCount: 0
        };
      });
      var largerSquareInterestingDirections = new Set([
        'fakeDirection1', 'fakeDirection2', 'fakeDirection3', 'fakeDirection4']);
      aiSquaring.checkWithoutSquareDirections(
        jasmine.createSpy('square'), ['fakeDirection1', 'fakeDirection2', 'fakeDirection3', 'fakeDirection4'],
        largerSquareInterestingDirections, new Set(), new Set());  // 4 without square directions
      expect(largerSquareInterestingDirections.has('fakeDirection2')).toBeTrue();
      expect(largerSquareInterestingDirections.has('fakeDirection3')).toBeTrue();
      expect(largerSquareInterestingDirections.has('fakeDirection4')).toBeTrue();
    });

    it('calls removeLargerSquareInterestingDirection for each larger square interesting direction ' +
       'which not all blocks are improvable', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      spyOn(aiSquaring, 'checkNeighborBlocks').and.callFake(function (square, direction) {
        return {
          allBlocksImprovable: direction !== 'fakeDirection1' && direction !== 'fakeDirection4',
          notFittingBlocksCount: 0,
          nonImprovableNeighborBlocksCount: 0
        };
      });
      spyOn(aiSquaring, 'removeLargerSquareInterestingDirection');
      var largerSquareInterestingDirections = new Set([
        'fakeDirection1', 'fakeDirection2', 'fakeDirection3', 'fakeDirection4']);
      var withoutSquareDirections = ['fakeDirection1', 'fakeDirection2', 'fakeDirection3', 'fakeDirection4'];
      aiSquaring.checkWithoutSquareDirections(
        jasmine.createSpy('square'), withoutSquareDirections,
        largerSquareInterestingDirections, new Set(), new Set());  // 4 without square directions
      expect(aiSquaring.removeLargerSquareInterestingDirection).toHaveBeenCalledWith(
        largerSquareInterestingDirections, withoutSquareDirections, 'fakeDirection1');
      expect(aiSquaring.removeLargerSquareInterestingDirection).toHaveBeenCalledWith(
        largerSquareInterestingDirections, withoutSquareDirections, 'fakeDirection4');
    });

  });

  describe('updateNextSmallestSquareCoords', function () {

    it('sets the new nextSmallestSquareCoords value when it is greater than old one', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      aiSquaring.setMap({ getBlock: jasmine.createSpy('getBlock') });
      var oldBlock = jasmine.createSpy('oldBlock');
      aiSquaring.nextSmallestSquareCoords = {
        block: oldBlock,
        value: 0.25
      };

      var sameLevelNeighborSquaresCount = 100;
      var notFittingSquaresAvg = 0;
      var nonImprovableNeighborBlocksCount = 0;
      aiSquaring.updateNextSmallestSquareCoords(new Square(0, -1, 1, 2),
        sameLevelNeighborSquaresCount, notFittingSquaresAvg, nonImprovableNeighborBlocksCount);
      expect(aiSquaring.nextSmallestSquareCoords.block).not.toBe(oldBlock);
      expect(aiSquaring.nextSmallestSquareCoords.value).toBeGreaterThan(0.25);
    });

    it('sets the new nextSmallestSquareCoords value when it is equal to old one', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      aiSquaring.setMap({ getBlock: jasmine.createSpy('getBlock') });
      var oldBlock = jasmine.createSpy('oldBlock');
      aiSquaring.nextSmallestSquareCoords = {
        block: oldBlock,
        value: 1.5
      };

      var sameLevelNeighborSquaresCount = 1;
      var notFittingSquaresAvg = 0;
      var nonImprovableNeighborBlocksCount = 0;
      aiSquaring.updateNextSmallestSquareCoords(new Square(0, -1, 1, 2),
        sameLevelNeighborSquaresCount, notFittingSquaresAvg, nonImprovableNeighborBlocksCount);
      expect(aiSquaring.nextSmallestSquareCoords.block).not.toBe(oldBlock);
      expect(aiSquaring.nextSmallestSquareCoords.value).toBe(1.5);
    });

    it('does not set the new nextSmallestSquareCoords value when it is lesser than old one', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      aiSquaring.setMap({ getBlock: jasmine.createSpy('getBlock') });
      var oldBlock = jasmine.createSpy('oldBlock');
      aiSquaring.nextSmallestSquareCoords = {
        block: oldBlock,
        value: 10.25
      };

      var sameLevelNeighborSquaresCount = 1;
      var notFittingSquaresAvg = 10;
      var nonImprovableNeighborBlocksCount = 10;
      aiSquaring.updateNextSmallestSquareCoords(new Square(0, -1, 1, 2),
        sameLevelNeighborSquaresCount, notFittingSquaresAvg, nonImprovableNeighborBlocksCount);
      expect(aiSquaring.nextSmallestSquareCoords.block).toBe(oldBlock);
      expect(aiSquaring.nextSmallestSquareCoords.value).toBe(10.25);
    });

    it('sets the new nextSmallestSquareCoords value to 7.75 ' +
       'when there are 6 same level neighbor squares (maximum)', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      aiSquaring.setMap({ getBlock: jasmine.createSpy('getBlock') });
      var oldBlock = jasmine.createSpy('oldBlock');
      aiSquaring.nextSmallestSquareCoords = {
        block: oldBlock,
        value: 0.25
      };

      var sameLevelNeighborSquaresCount = 6;
      var notFittingSquaresAvg = 0;
      var nonImprovableNeighborBlocksCount = 0;
      aiSquaring.updateNextSmallestSquareCoords(new Square(0, -1, 1, 2),
        sameLevelNeighborSquaresCount, notFittingSquaresAvg, nonImprovableNeighborBlocksCount);
      expect(aiSquaring.nextSmallestSquareCoords.block).not.toBe(oldBlock);
      expect(aiSquaring.nextSmallestSquareCoords.value).toBe(7.75);
    });

    it('sets the new nextSmallestSquareCoords value to 0.25 ' +
       'when there no same level neighbor squares (minimum)', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      aiSquaring.setMap({ getBlock: jasmine.createSpy('getBlock') });

      var sameLevelNeighborSquaresCount = 0;
      var notFittingSquaresAvg = 0;
      var nonImprovableNeighborBlocksCount = 0;
      aiSquaring.updateNextSmallestSquareCoords(new Square(0, -1, 1, 2),
        sameLevelNeighborSquaresCount, notFittingSquaresAvg, nonImprovableNeighborBlocksCount);
      expect(aiSquaring.nextSmallestSquareCoords.value).toBe(0.25);
    });

    it('does not set the new nextSmallestSquareCoords ' +
       'if there are only not fitting squares', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      aiSquaring.setMap({ getBlock: jasmine.createSpy('getBlock') });

      var sameLevelNeighborSquaresCount = 0;
      var notFittingSquaresAvg = 2;
      var nonImprovableNeighborBlocksCount = 0;
      aiSquaring.updateNextSmallestSquareCoords(new Square(0, -1, 1, 2),
        sameLevelNeighborSquaresCount, notFittingSquaresAvg, nonImprovableNeighborBlocksCount);
      expect(aiSquaring.nextSmallestSquareCoords).toBeNull();
    });

  });

  describe('increaseInterestingNeighborBlocksAiValue', function () {

    it('increases neighbor blocks aiValues only in interesting directions', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      mockedAiPlayer.playerId = 'mockedAiPlayerId';
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      var neighborBlocksMap = {
        'left': [{ aiValueForPlayer: { mockedAiPlayerId: 10 }}, { aiValueForPlayer: { mockedAiPlayerId: 20 }}],
        'up': [{ aiValueForPlayer: { mockedAiPlayerId: 30 }}, { aiValueForPlayer: { mockedAiPlayerId: 15 }}]
      };
      var interestingDirections = new Set(['up']);
      aiSquaring.increaseInterestingNeighborBlocksAiValue(interestingDirections, neighborBlocksMap);
      expect(neighborBlocksMap.left[0].aiValueForPlayer.mockedAiPlayerId).toBe(10);
      expect(neighborBlocksMap.left[1].aiValueForPlayer.mockedAiPlayerId).toBe(20);
      expect(neighborBlocksMap.up[0].aiValueForPlayer.mockedAiPlayerId).toBeGreaterThan(30);
      expect(neighborBlocksMap.up[1].aiValueForPlayer.mockedAiPlayerId).toBeGreaterThan(15);
    });

  });

  describe('increaseLargerSquareInterestingNeighborBlocksAiValue', function () {

    it('increases neighbor blocks aiValuesForPlayerMissions.largeSquareMaking ' +
       'only in interesting directions', function () {
      var mockedAiPlayer = jasmine.createSpy('aiPlayer');
      mockedAiPlayer.playerId = 'mockedAiPlayerId';
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      var neighborBlocksMap = {
        'leftDown': [{ aiValuesForPlayerMissions: { mockedAiPlayerId: {} }},
                     { aiValuesForPlayerMissions: { mockedAiPlayerId: { largeSquareMaking: 20 } }}],
        'down': [{ aiValuesForPlayerMissions: { mockedAiPlayerId: { largeSquareMaking: 10 } }},
                 { aiValuesForPlayerMissions: { mockedAiPlayerId: {} }}]
      };
      var largerSquareInterestingDirections = new Set(['down']);
      var square = { level: 2 };
      aiSquaring.increaseLargerSquareInterestingNeighborBlocksAiValue(
        square, largerSquareInterestingDirections, neighborBlocksMap, 18);
      expect(neighborBlocksMap.down[0].aiValuesForPlayerMissions.mockedAiPlayerId.largeSquareMaking).toBe(10 + 18 * 2);
      expect(neighborBlocksMap.down[1].aiValuesForPlayerMissions.mockedAiPlayerId.largeSquareMaking).toBe(18 * 2);
    });

    it('increases neighbor blocks aiValuesForPlayerMissions.largeSquareMaking ' +
       'only for previously set player', function () {
      var aiPlayer = playerProvider.addAI();
      var anotherAiPlayer = playerProvider.addAI();
      var mockedTechnologies = jasmine.createSpy('technologies');
      var aiSquaring = new AISquaring(aiPlayer, mockedTechnologies);
      var neighborBlocksMap = {
        'leftDown': [{ aiValuesForPlayerMissions: { }},
                     { aiValuesForPlayerMissions: { }}],
        'down': [{ aiValuesForPlayerMissions: { }},
                 { aiValuesForPlayerMissions: { }}],
        'up': [{ aiValuesForPlayerMissions: { }},
                 { aiValuesForPlayerMissions: { }}]
      };
      for (var direction in neighborBlocksMap) {
        neighborBlocksMap[direction].forEach(function (block) {
          block.aiValuesForPlayerMissions[aiPlayer.playerId] = {};
          block.aiValuesForPlayerMissions[anotherAiPlayer.playerId] = {};
        });
      }
      var largerSquareInterestingDirections = new Set(['down', 'leftDown']);
      var square = { level: 3 };
      aiSquaring.increaseLargerSquareInterestingNeighborBlocksAiValue(
        square, largerSquareInterestingDirections, neighborBlocksMap, 36);
      expect(neighborBlocksMap.down[0].aiValuesForPlayerMissions[aiPlayer.playerId].largeSquareMaking).toBe(36 * 3);
      expect(neighborBlocksMap.down[1].aiValuesForPlayerMissions[aiPlayer.playerId].largeSquareMaking).toBe(36 * 3);
      expect(neighborBlocksMap.leftDown[0].aiValuesForPlayerMissions[aiPlayer.playerId].largeSquareMaking).toBe(36 * 3);
      expect(neighborBlocksMap.leftDown[1].aiValuesForPlayerMissions[aiPlayer.playerId].largeSquareMaking).toBe(36 * 3);

      expect(neighborBlocksMap.down[0].aiValuesForPlayerMissions[anotherAiPlayer.playerId].largeSquareMaking)
        .toBeUndefined();
      expect(neighborBlocksMap.down[1].aiValuesForPlayerMissions[anotherAiPlayer.playerId].largeSquareMaking)
        .toBeUndefined();
      expect(neighborBlocksMap.leftDown[0].aiValuesForPlayerMissions[anotherAiPlayer.playerId].largeSquareMaking)
        .toBeUndefined();
      expect(neighborBlocksMap.leftDown[1].aiValuesForPlayerMissions[anotherAiPlayer.playerId].largeSquareMaking)
        .toBeUndefined();
    });

  });

  describe('notifyOfNewSquare', function () {

    var aiSquaring;
    var mockedAiPlayer;
    var check8directionsSpy;
    var getLargerSquareInterestingDirectionsSpy;
    var checkWithoutSquareDirectionsSpy;

    beforeEach(function () {
      mockedAiPlayer = jasmine.createSpy('aiPlayer');
      var mockedTechnologies = jasmine.createSpy('technologies');
      aiSquaring = new AISquaring(mockedAiPlayer, mockedTechnologies);
      check8directionsSpy = spyOn(aiSquaring, 'check8directions')
        .and.returnValue({});
      getLargerSquareInterestingDirectionsSpy = spyOn(aiSquaring, 'getLargerSquareInterestingDirections')
        .and.returnValue({});
      checkWithoutSquareDirectionsSpy = spyOn(aiSquaring, 'checkWithoutSquareDirections')
        .and.returnValue({});
      spyOn(aiSquaring, 'updateNextSmallestSquareCoords');
      spyOn(aiSquaring, 'increaseInterestingNeighborBlocksAiValue');
      spyOn(aiSquaring, 'increaseLargerSquareInterestingNeighborBlocksAiValue');
    });

    it('calls getLargerSquareInterestingDirections only for large squares', function () {
      var squareLevel = 3;
      aiSquaring.notifyOfNewSquare(new Square(0, 0, 1, 2, [], squareLevel, mockedAiPlayer));
      expect(aiSquaring.getLargerSquareInterestingDirections).toHaveBeenCalled();
    });

    it('does not call getLargerSquareInterestingDirections for smaller squares', function () {
      var squareLevel = 2;
      aiSquaring.notifyOfNewSquare(new Square(0, 0, 1, 2, [], squareLevel, mockedAiPlayer));
      expect(aiSquaring.getLargerSquareInterestingDirections).not.toHaveBeenCalled();
    });

    it('calls updateNextSmallestSquareCoords only for the smallest squares', function () {
      var squareLevel = 2;
      checkWithoutSquareDirectionsSpy.and.returnValue({
        notFittingBlocksCount: 0
      });
      check8directionsSpy.and.returnValue({
        sameLevelNeighborSquaresCount: 0
      });
      aiSquaring.notifyOfNewSquare(new Square(0, 0, 1, 2, [], squareLevel, mockedAiPlayer));
      expect(aiSquaring.updateNextSmallestSquareCoords).toHaveBeenCalled();
    });

    it('does not call updateNextSmallestSquareCoords for larger squares', function () {
      var squareLevel = 3;
      checkWithoutSquareDirectionsSpy.and.returnValue({
        notFittingBlocksCount: 0
      });
      check8directionsSpy.and.returnValue({
        sameLevelNeighborSquaresCount: 0
      });
      aiSquaring.notifyOfNewSquare(new Square(0, 0, 1, 2, [], squareLevel, mockedAiPlayer));
      expect(aiSquaring.updateNextSmallestSquareCoords).not.toHaveBeenCalled();
    });

    it('does not call updateNextSmallestSquareCoords if there is too many not fitting squares', function () {
      var squareLevel = 2;
      checkWithoutSquareDirectionsSpy.and.returnValue({
        notFittingBlocksCount: 1000
      });
      check8directionsSpy.and.returnValue({
        sameLevelNeighborSquaresCount: 1
      });
      aiSquaring.notifyOfNewSquare(new Square(0, 0, 1, 2, [], squareLevel, mockedAiPlayer));
      expect(aiSquaring.updateNextSmallestSquareCoords).not.toHaveBeenCalled();
    });

    it('calls increaseInterestingNeighborBlocksAiValue ' +
       'if number of the same level neighbor squares is equal ' +
       'than average not fitting squares number', function () {
      var squareLevel = 2;
      checkWithoutSquareDirectionsSpy.and.returnValue({
        notFittingBlocksCount: 40  // 40 / (2 * 2) === 10
      });
      check8directionsSpy.and.returnValue({
        sameLevelNeighborSquaresCount: 10
      });
      aiSquaring.notifyOfNewSquare(new Square(0, 0, 1, 2, [], squareLevel, mockedAiPlayer));
      expect(aiSquaring.increaseInterestingNeighborBlocksAiValue).toHaveBeenCalled();
    });

    it('calls increaseInterestingNeighborBlocksAiValue ' +
       'if number of the same level neighbor squares is greater ' +
       'than average not fitting squares number', function () {
      var squareLevel = 3;
      checkWithoutSquareDirectionsSpy.and.returnValue({
        notFittingBlocksCount: 50  // 50 / (3 * 3) < 10
      });
      check8directionsSpy.and.returnValue({
        sameLevelNeighborSquaresCount: 10
      });
      aiSquaring.notifyOfNewSquare(new Square(0, 0, 1, 2, [], squareLevel, mockedAiPlayer));
      expect(aiSquaring.increaseInterestingNeighborBlocksAiValue).toHaveBeenCalled();
    });

    it('calls increaseLargerSquareInterestingNeighborBlocksAiValue two times ' +
       'if number of the same level neighbor squares is greater ' +
       'than average not fitting squares number', function () {
      var squareLevel = 3;
      checkWithoutSquareDirectionsSpy.and.returnValue({
        notFittingBlocksCount: 80  // 80 / (3 * 3) < 10
      });
      check8directionsSpy.and.returnValue({
        sameLevelNeighborSquaresCount: 10
      });
      aiSquaring.notifyOfNewSquare(new Square(0, 0, 1, 2, [], squareLevel, mockedAiPlayer));
      expect(aiSquaring.increaseLargerSquareInterestingNeighborBlocksAiValue.calls.count()).toBe(2);
    });

  });

});