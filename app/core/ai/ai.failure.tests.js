'use strict';

var fs = require('fs');

jasmine.DEFAULT_TIMEOUT_INTERVAL = 60000;

describe('Ai', function() {
  beforeEach(module('square'));

  var AI, AIPlayer, Map,  playerProvider, gameStateProvider,
    mapProvider, storageProvider, squareProvider, Joi;

  beforeEach(inject(function (_AI_, _AIPlayer_, _Map_, _playerProvider_, _gameStateProvider_,
                              _mapProvider_, _storageProvider_, _squareProvider_, _Joi_) {
    AI = _AI_;
    AIPlayer = _AIPlayer_;
    Map = _Map_;
    playerProvider = _playerProvider_;
    gameStateProvider = _gameStateProvider_;
    mapProvider = _mapProvider_;
    storageProvider = _storageProvider_;
    squareProvider = _squareProvider_;
    Joi = _Joi_;
  }));

  function makeMoves(count) {
    for (var i = 0; i < count; i++) {
      playerProvider.getFirstAI().ai.nextMove();
    }
  }

  describe('makes 30 steps in test_failures', function () {

    var dirPath = process.env.PWD + '/test_fixtures/test_failures/';
    var files = fs.readdirSync(dirPath);

    for (var i = 0; i < files.length; i++) {
      (function (filename) {
        if (filename !== '.' && filename !== '..') {

          it('map `' + filename + '`', function (done) {
            gameStateProvider.load(dirPath + filename, function (err) {
              if (err) {
                done.fail(err);
                return;
              }
              var aiPlayer = playerProvider.getFirstAI();
              aiPlayer.defeated = false;
              storageProvider.getStorage(aiPlayer).put({metal: 1, stone: 1, wood: 1, food: 1});
              mapProvider.getMap().updateNeighborBlocksAffordability(aiPlayer);
              //console.log('Started processing:', filename);
              try {
                makeMoves(30);
              } catch (moveErr) {
                done.fail(moveErr);
                return;
              }
              //console.log('Finished processing:', filename);
              if (aiPlayer.defeated) {
                done.fail('AI player has been defeated');
                return;
              }
              done();
            });
          });

        }
      }(files[i]));
    }

  });

});
