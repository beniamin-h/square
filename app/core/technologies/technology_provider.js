'use strict';

angular.module('square').factory('technologyProvider', ['Technology',
    function (Technology) {

  var that = this;
  that.technologyRegistry = {};

  that.addTech = function (tech) {
    that.technologyRegistry[tech.name] = tech;
  };

  that.addTech(new Technology('Deforestation'));

  return {
    getTech: function (techName) {
      return that.technologyRegistry[techName];
    }
  };
}]);