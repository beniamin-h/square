'use strict';

angular.module('square').factory('Technology', [
    function () {

  function Technology(name) {
    this.name = name;
  }

  Technology.prototype.name = '';

  return Technology;
}]);