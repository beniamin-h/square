'use strict';

describe('Block', function() {
  beforeEach(module('square'));

  let Block, Plain, Forest, Hill, Mountain, Water, Rand;

  beforeEach(inject(function (
      _Block_, _Plain_, _Forest_, _Hill_, _Mountain_, _Water_, _Rand_) {
    Block = _Block_;
    Plain = _Plain_;
    Forest = _Forest_;
    Hill = _Hill_;
    Mountain = _Mountain_;
    Water = _Water_;
    Rand = _Rand_;
  }));

  describe('generateBlockKind', function () {

    describe('generates block kind and richness with a proper randomness', function () {

      let sampleSize;

      beforeEach(function () {
        sampleSize = 5000;
      });

      function makeTest(kindProbabilities, richProbability, advKindProbabilityFactor, region) {
        const randGenerator = new Rand(
          'Map.blockKindAndRichness', Rand.getUnpredictableInt31Unsigned())
            .getGenerator('blockKindAndRichness');
        let blocks = [];
        for (let i = 0; i < sampleSize; i++) {
          const block = new Block(0, 0, region);
          block.generateBlockKind(randGenerator, kindProbabilities, advKindProbabilityFactor);
          block.generateBlockRichness(randGenerator, richProbability);
          blocks.push(block);
        }
        const blocksByKind = {};
        let riches = 0;
        blocks.forEach(function (block) {
          const kind = block.kind.toString();
          blocksByKind[kind] = blocksByKind[kind] || 0;
          blocksByKind[kind]++;
          riches += block.rich ? 1 : 0;
        });
        return {
          blocksByKind: blocksByKind,
          riches: riches
        };
      }

      describe('for base probabilities', function () {

        let kindProbabilities;
        let richProbability;
        let advKindProbabilityFactor;

        beforeEach(function () {
          kindProbabilities = Block.baseKindProbabilities;
          richProbability = Block.baseRichProbability;
          advKindProbabilityFactor = Block.baseAdvancedKindProbabilityFactor;
        });

        it('for region coords: { x: 0, y: 0 }', function () {
          const result = makeTest(kindProbabilities, richProbability,
            advKindProbabilityFactor, { x: 0, y: 0 });
          expect(Object.keys(result.blocksByKind).length).toBe(5); // 5 block kinds
          expect(result.blocksByKind['plain'] / sampleSize).toBeWithinRange(0.40, 0.50);  // 45%
          expect(result.blocksByKind['forest'] / sampleSize).toBeWithinRange(0.20, 0.30);  // 25%
          expect(result.blocksByKind['hill'] / sampleSize).toBeWithinRange(0.15, 0.25);  // 20%
          expect(result.blocksByKind['mountain'] / sampleSize).toBeWithinRange(0.04, 0.13);  // 8.5%
          expect(result.blocksByKind['water'] / sampleSize).toBeWithinRange(0.001, 0.05);  // 2.5%
          expect(result.riches / sampleSize).toBeWithinRange(0.0, 0.04);  // 2%
        });

        it('for region coords: { x: 0, y: -8 }', function () {
          const result = makeTest(kindProbabilities, richProbability,
            advKindProbabilityFactor, { x: 0, y: -8 });
          expect(Object.keys(result.blocksByKind).length).toBeGreaterThan(6); // more than 6 block kinds
          expect(result.blocksByKind['plain'] / sampleSize).toBeWithinRange(0.35, 0.45);  // 40%
          expect(result.blocksByKind['forest'] / sampleSize).toBeWithinRange(0.20, 0.30);  // 25%
          expect(result.blocksByKind['hill'] / sampleSize).toBeWithinRange(0.15, 0.25);  // 20%
          expect(result.blocksByKind['mountain'] / sampleSize).toBeWithinRange(0.04, 0.12);  // 8%
          expect(result.blocksByKind['water'] / sampleSize).toBeWithinRange(0.001, 0.05);  // 3% + 2% - 3.2% = 1.8%
          expect(result.blocksByKind['wildHorses'] / sampleSize).toBeWithinRange(0.0001, 0.03);  // 3.2% * 40% = 1.28%
          expect(result.blocksByKind['coalfield'] / sampleSize).toBeWithinRange(0.0001, 0.02);  // 3.2% * 30% = 0.96%
          const others = (result.blocksByKind['silverDeposit'] || 0) +
            (result.blocksByKind['goldDeposit'] || 0) +
            (result.blocksByKind['gemstoneDeposit'] || 0);
          expect(others / sampleSize).toBeWithinRange(0.0001, 0.02);  // 3.2% * 30% = 0.96%
          expect(result.riches / sampleSize).toBeWithinRange(0.0, 0.04);  // 2%
        });

      });

      describe('for custom probabilities', function () {

        let kindProbabilities;
        let richProbability;
        let advKindProbabilityFactor;

        beforeEach(function () {
          kindProbabilities = [
            {
              0.9: Plain  // 10%
            }, {
              0.6: Forest  // 30%
            }, {
              0.55: Hill  // 5%
            }, {
              0.11: Mountain  // 44%
            }, {
              0.0: Water  // 11%
            }
          ];
          richProbability = 0.1;
          advKindProbabilityFactor = 0.008;
        });

        it('for region coords: { x: 0, y: 0 }', function () {
          const result = makeTest(kindProbabilities, richProbability,
            advKindProbabilityFactor, { x: 0, y: 0 });
          expect(Object.keys(result.blocksByKind).length).toBe(5); // 5 block kinds
          expect(result.blocksByKind['plain'] / sampleSize).toBeWithinRange(0.06, 0.14);  // 10%
          expect(result.blocksByKind['forest'] / sampleSize).toBeWithinRange(0.25, 0.35);  // 30%
          expect(result.blocksByKind['hill'] / sampleSize).toBeWithinRange(0.02, 0.08);  // 5%
          expect(result.blocksByKind['mountain'] / sampleSize).toBeWithinRange(0.39, 0.49);  // 44%
          expect(result.blocksByKind['water'] / sampleSize).toBeWithinRange(0.06, 0.15);  // 11%
          expect(result.riches / sampleSize).toBeWithinRange(0.05, 0.15);  // 10%
        });

        it('for region coords: { x: 8, y: 0 }', function () {
          const result = makeTest(kindProbabilities, richProbability,
            advKindProbabilityFactor, { x: 8, y: 0 });
          expect(Object.keys(result.blocksByKind).length).toBeGreaterThan(6); // more than 6 block kinds
          expect(result.blocksByKind['plain'] / sampleSize).toBeWithinRange(0.06, 0.14);  // 10%
          expect(result.blocksByKind['forest'] / sampleSize).toBeWithinRange(0.25, 0.35);  // 30%
          expect(result.blocksByKind['hill'] / sampleSize).toBeWithinRange(0.02, 0.08);  // 5%
          expect(result.blocksByKind['mountain'] / sampleSize).toBeWithinRange(0.39, 0.49);  // 44%
          expect(result.blocksByKind['water'] / sampleSize).toBeWithinRange(0.02, 0.08);  // 11% - 6.4% = 4.6%
          expect(result.blocksByKind['wildHorses'] / sampleSize).toBeWithinRange(0.001, 0.05);  // 6.4% * 40% = 2.56%
          expect(result.blocksByKind['coalfield'] / sampleSize).toBeWithinRange(0.001, 0.04);  // 6.4% * 30% = 1.92%
          const others = (result.blocksByKind['silverDeposit'] || 0) +
            (result.blocksByKind['goldDeposit'] || 0) +
            (result.blocksByKind['gemstoneDeposit'] || 0);
          expect(others / sampleSize).toBeWithinRange(0.001, 0.04);  // 6.4% * 30% = 1.92%
          expect(result.riches / sampleSize).toBeWithinRange(0.05, 0.15);  // 10%
        });

      });

    });

    it('does not set rich for water blocks', function () {
      const sampleSize = 1000;
      const randGenerator = new Rand(
        'Map.blockKindAndRichness', Rand.getUnpredictableInt31Unsigned())
          .getGenerator('blockKindAndRichness');

      let blocks = [];
      for (let i = 0; i < sampleSize; i++) {
        const block = new Block(0, 0, jasmine.createSpy('region'));
        block.generateBlockKind(randGenerator, Block.baseKindProbabilities, Block.baseAdvancedKindProbabilityFactor);
        block.generateBlockRichness(randGenerator, Block.baseRichProbability);
        blocks.push(block);
      }

      let waterFound = false;
      blocks.forEach(function (block) {
        if (block.kind.toString() === 'water') {
          expect(block.rich).toBeFalse();
          waterFound = true;
        }
      });

      expect(waterFound).toBeTrue();
    });

  });

  describe('getAdvancedResourceKindClass', function () {

    it('returns advanced block kind with a proper randomness', function () {
      const sampleSize = 5000;
      const resultsByKind = {
        wildHorses: 0,
        coalfield: 0,
        silverDeposit: 0,
        goldDeposit: 0,
        gemstoneDeposit: 0
      };
      const block = new Block(0, -1, null, {});
      const randGenerator = new Rand(
        'Map.blockKindAndRichness', Rand.getUnpredictableInt31Unsigned())
          .getGenerator('blockKindAndRichness');

      for (let i = 0; i < sampleSize; i++) {
        resultsByKind[(new (block.getAdvancedResourceKindClass(randGenerator))()).toString()]++;
      }

      expect(Object.keys(resultsByKind).length).toBe(5);
      expect(resultsByKind['wildHorses'] / sampleSize).toBeWithinRange(0.35, 0.45);  // 40%
      expect(resultsByKind['coalfield'] / sampleSize).toBeWithinRange(0.25, 0.35);  // 30%
      expect(resultsByKind['silverDeposit'] / sampleSize).toBeWithinRange(0.15, 0.25);  // 20%
      expect(resultsByKind['goldDeposit'] / sampleSize).toBeWithinRange(0.03, 0.10);  // 6.5%
      expect(resultsByKind['gemstoneDeposit'] / sampleSize).toBeWithinRange(0.0, 0.07);  // 3.5%
    });

  });

});