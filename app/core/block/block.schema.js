'use strict';

var _RegExp = require('nwglobal').RegExp;

angular.module('square')

.constant('blockAiValueMissionRegExp', new _RegExp(/^(largeSquareMaking)$/))

.constant('improvableBlockKindRegExp',
  new _RegExp(/^(plain|forest|hill|mountain|coalfield|wildHorses|goldDeposit|silverDeposit|gemstoneDeposit)$/))

.factory('BlockSchema', ['Joi', 'Region', 'Block', 'BlockKindSchema', 'PlayerSchema', 'improvableBlockKindRegExp',
                         'playerIdRegExp', 'Building', 'buildingClassRegExp', 'Square', 'map8DirectionRegExp',
                         'blockAiValueMissionRegExp',
    function (Joi, Region, Block, BlockKindSchema, PlayerSchema, improvableBlockKindRegExp,
              playerIdRegExp, Building, buildingClassRegExp, Square, map8DirectionRegExp,
              blockAiValueMissionRegExp) {

  return Joi.object().keys({
    kind: BlockKindSchema,
    level: Joi.number().natural(),
    x: Joi.number().integer(),
    y: Joi.number().integer(),
    region: Joi.object().type(Region),
    squarePos: Joi.string(), //TODO: regexp
    _hasImprovedNeighborOfPlayers: Joi.array(), //TODO
    _affordableForPlayers: Joi.array(), //TODO
    exhausted: Joi.boolean(),
    rich: Joi.boolean(),
    ownedBy: [PlayerSchema, null],
    neighborsByKind: Joi.object({})
      .pattern(improvableBlockKindRegExp, Joi.array().items(
        Joi.object().type(Block)
      )).length(9),
    neighborsByPosition: Joi.object({})
      .pattern(map8DirectionRegExp, Joi.object().type(Block)),
    improvedNeighbors: Joi.object({})
      .pattern(map8DirectionRegExp, Joi.object().type(Block)),  //TODO: remove ?
    baseAiValue: Joi.number().integer(),
    tmpAiValue: Joi.number().integer(),
    aiValueForPlayer: Joi.object({})
      .pattern(playerIdRegExp, Joi.number().integer()),
    aiValuesForPlayerMissions: Joi.object({})
      .pattern(playerIdRegExp, Joi.object({})
        .pattern(blockAiValueMissionRegExp, Joi.number().integer())),
    buildingsByClass: Joi.object().pattern(
      buildingClassRegExp, Joi.array().items(
        Joi.object().type(Building)
      )
    ),
    buildings: Joi.array().items(
      Joi.object().type(Building)
    ),
    square: [Joi.object().type(Square), null],
    isMutating: Joi.boolean(),
    mutated: Joi.boolean(),
    disabled: Joi.boolean()
  }).type(Block);

}]);