'use strict';

describe('Block', function() {
  beforeEach(module('square'));

  var Block, BlockKind,
    Plain, Forest, Hill, Mountain, Water,
    WildHorses, Coalfield, SilverDeposit, GoldDeposit, GemstoneDeposit;

  beforeEach(inject(function (_Block_, _BlockKind_,
      _Plain_, _Forest_, _Hill_, _Mountain_, _Water_,
      _WildHorses_, _Coalfield_, _SilverDeposit_, _GoldDeposit_, _GemstoneDeposit_) {
    Block = _Block_;
    BlockKind = _BlockKind_;

    Plain = _Plain_;
    Forest = _Forest_;
    Hill = _Hill_;
    Mountain = _Mountain_;
    Water = _Water_;

    WildHorses = _WildHorses_;
    Coalfield = _Coalfield_;
    SilverDeposit = _SilverDeposit_;
    GoldDeposit = _GoldDeposit_;
    GemstoneDeposit = _GemstoneDeposit_;
  }));

  describe('a new instance', function () {

    var fakeX;
    var fakeY;
    var fakeRegion;
    var fakeKind;
    var fakeRich;

    beforeEach(function () {
      fakeX = jasmine.createSpy('x');
      fakeY = jasmine.createSpy('y');
      fakeRegion = jasmine.createSpy('region');
      fakeKind = jasmine.createSpy('kind');
      fakeRich = true;
      spyOn(Block.prototype, 'resetNeighborsByKind');
    });

    it('sets given params to block props', function () {
      var block = new Block(fakeX, fakeY, fakeRegion, fakeKind, fakeRich);
      expect(block.x).toBe(fakeX);
      expect(block.y).toBe(fakeY);
      expect(block.region).toBe(fakeRegion);
      expect(block.kind).toBe(fakeKind);
      expect(block.rich).toBe(fakeRich);
    });

    it('calls resetNeighborsByKind', function () {
      var block = new Block(fakeX, fakeY, fakeRegion, fakeKind, fakeRich);
      expect(block.resetNeighborsByKind).toHaveBeenCalledWith();
    });

  });

  describe('kindMutations', function () {

    it('is a dict with 4 basic block kinds', function () {
      expect(Block.kindMutations).toBeObject();
      expect(Block.kindMutations.plain).toBeDefined();
      expect(Block.kindMutations.forest).toBeDefined();
      expect(Block.kindMutations.hill).toBeDefined();
      expect(Block.kindMutations.mountain).toBeDefined();
    });

    describe('each kind', function () {

      ['plain', 'forest', 'hill', 'mountain'].forEach(function (kind) {

        it('(' + kind + ') is a non-empty array', function () {
          expect(Block.kindMutations[kind]).toBeArray();
          expect(Block.kindMutations[kind].length).toBeGreaterThan(0);
        });

        it('(' + kind + ') is an array of one-element objects ' +
           'where key is a float and value is BlockKind class', function () {
          Block.kindMutations[kind].forEach(function (obj) {
            expect(Object.keys(obj).length).toBe(1);
            var key = Object.keys(obj)[0];
            expect(parseFloat(key)).toBeWithinRange(0, 1);
            expect(parseFloat(key)).not.toBe(1);
            expect(obj[key].prototype instanceof BlockKind).toBeTrue();
          });
        });

      });

    });

  });

  describe('calcKindProbabilities', function () {

    it('returns obj similar to Block.baseKindProbabilities', function () {
      var result = Block.calcKindProbabilities(0.5);
      expect(result).toBeArray();
      expect(result.length).toBe(Block.baseKindProbabilities.length);
      result.forEach(function (resultItem) {
        expect(resultItem).toBeObject();
        expect(Object.keys(resultItem).length).toBe(1);
        var key = Object.keys(resultItem)[0];
        expect(parseFloat(key)).toBeWithinRange(0, 1);
        expect(resultItem[key].prototype instanceof BlockKind).toBeTrue();
      });
    });

    describe('returns probabilities:', function () {

      function calcProbabilities(result) {
        var probabilities = {};
        var rangeLeft = 1.0;
        result.forEach(function (resultItem) {
          var probabilityThreshold = Object.keys(resultItem)[0];
          var kindStr = resultItem[probabilityThreshold].prototype.toString();
          probabilities[kindStr] = probabilities[kindStr] || 0;
          probabilities[kindStr] += rangeLeft - parseFloat(probabilityThreshold);
          rangeLeft -= probabilities[kindStr];
          probabilities[kindStr] = parseFloat((probabilities[kindStr]).toFixed(6));
        });
        return probabilities;
      }

      it('{ plain: 0.45, forest: 0.25, hill: 0.2, mountain: 0.085, water: 0.015 } ' +
         'for mapDifficulty = 1.0', function () {
        var result = Block.calcKindProbabilities(1.0);
        expect(calcProbabilities(result)).toEqual(
          { plain: 0.45, forest: 0.25, hill: 0.2, mountain: 0.085, water: 0.015 }
        );
      });

      it('{ plain: 0.2925, forest: 0.24, hill: 0.24, mountain: 0.225, water: 0.0025 } ' +
         'for mapDifficulty = 0.0', function () {
        var result = Block.calcKindProbabilities(0.0);
        expect(calcProbabilities(result)).toEqual(
          { plain: 0.2925, forest: 0.24, hill: 0.24, mountain: 0.225, water: 0.0025 }
        );
      });

      it('{ plain: 0.37125, forest: 0.245, hill: 0.22, mountain: 0.155, water: 0.00875 } ' +
         'for mapDifficulty = 0.5', function () {
        var result = Block.calcKindProbabilities(0.5);
        expect(calcProbabilities(result)).toEqual(
          { plain: 0.37125, forest: 0.245, hill: 0.22, mountain: 0.155, water: 0.00875 }
        );
      });

    });

  });

  describe('calcRichProbability', function () {

    it('returns 0.02 for baseRichProbability = 0.02 and ' +
       'mapDifficulty = 0.5', function () {
      Block.baseRichProbability = 0.02;
      var result = Block.calcRichProbability(0.5);
      expect(result).toBe(0.02);
    });

    it('returns 0.01 for baseRichProbability = 0.02 and ' +
       'mapDifficulty = 1.0', function () {
      Block.baseRichProbability = 0.02;
      var result = Block.calcRichProbability(1.0);
      expect(result).toBe(0.01);
    });

    it('returns 0.03 for baseRichProbability = 0.02 and ' +
       'mapDifficulty = 0.0', function () {
      Block.baseRichProbability = 0.02;
      var result = Block.calcRichProbability(0.0);
      expect(result).toBe(0.03);
    });

  });

  describe('calcAdvancedKindProbabilityFactor', function () {

    it('returns 0.004 for baseAdvancedKindProbabilityFactor = 0.004 and ' +
       'mapDifficulty = 1.0', function () {
      Block.baseAdvancedKindProbabilityFactor = 0.004;
      var result = Block.calcAdvancedKindProbabilityFactor(1.0);
      expect(result).toBe(0.004);
    });

    it('returns 0.008 for baseAdvancedKindProbabilityFactor = 0.004 and ' +
       'mapDifficulty = 0.0', function () {
      Block.baseAdvancedKindProbabilityFactor = 0.004;
      var result = Block.calcAdvancedKindProbabilityFactor(0.0);
      expect(result).toBe(0.008);
    });

    it('returns 0.006 for baseAdvancedKindProbabilityFactor = 0.004 and ' +
       'mapDifficulty = 0.5', function () {
      Block.baseAdvancedKindProbabilityFactor = 0.004;
      var result = Block.calcAdvancedKindProbabilityFactor(0.5);
      expect(result).toBe(0.006);
    });

    it('returns 0.0125 for baseAdvancedKindProbabilityFactor = 0.01 and ' +
       'mapDifficulty = 0.75', function () {
      Block.baseAdvancedKindProbabilityFactor = 0.01;
      var result = Block.calcAdvancedKindProbabilityFactor(0.75);
      expect(result).toBe(0.0125);
    });

  });

  describe('generateBlockKind', function () {

    let fakeRandGenerator;

    beforeEach(function () {
      fakeRandGenerator = jasmine.createSpyObj('randGenerator', ['getFloat']);
    });

    it('calls getFloat on the given randGenerator', function () {
      const block = new Block(0, 0, null, {});
      block.generateBlockKind(fakeRandGenerator,
                              Block.baseKindProbabilities,
                              Block.baseAdvancedKindProbabilityFactor);
      expect(fakeRandGenerator.getFloat).toHaveBeenCalledWith();
    });

    describe('does not call getAdvancedResourceProbability', function () {

      let block;

      beforeEach(function () {
        block = new Block(0, 0, null, {});
        spyOn(block, 'getAdvancedResourceProbability');
      });

      describe('for base probabilities', function () {

        it('when randGenerator.getFloat returns 0.09', function () {
          fakeRandGenerator.getFloat.and.returnValue(0.09);
          block.generateBlockKind(fakeRandGenerator,
                                  Block.baseKindProbabilities,
                                  Block.baseAdvancedKindProbabilityFactor);
          expect(block.getAdvancedResourceProbability).not.toHaveBeenCalled();
        });

        it('when randGenerator.getFloat returns 0.051', function () {
          fakeRandGenerator.getFloat.and.returnValue(0.051);
          block.generateBlockKind(fakeRandGenerator,
                                  Block.baseKindProbabilities,
                                  Block.baseAdvancedKindProbabilityFactor);
          expect(block.getAdvancedResourceProbability).not.toHaveBeenCalled();
        });

      });

      describe('for advKindProbabilityFactor = 0.02 and ' +
               'advancedKindMaxProbabilityMultiplier = 10', function () {

        beforeEach(function () {
          Block.advancedKindMaxProbabilityMultiplier = 10;
        });

        it('when randGenerator.getFloat returns 0.21', function () {
          fakeRandGenerator.getFloat.and.returnValue(0.21);
          block.generateBlockKind(fakeRandGenerator,
                                  Block.baseKindProbabilities, 0.02);
          expect(block.getAdvancedResourceProbability).not.toHaveBeenCalled();
        });

        it('when randGenerator.getFloat returns 0.8', function () {
          fakeRandGenerator.getFloat.and.returnValue(0.8);
          block.generateBlockKind(fakeRandGenerator,
                                  Block.baseKindProbabilities, 0.02);
          expect(block.getAdvancedResourceProbability).not.toHaveBeenCalled();
        });

      });

    });

    describe('calls getAdvancedResourceProbability', function () {

      let block;

      beforeEach(function () {
        block = new Block(0, 0, null, {});
        spyOn(block, 'getAdvancedResourceProbability');
        spyOn(block, 'getAdvancedResourceKindClass')
          .and.returnValue(function FakeAdvancedResClass() { });
      });

      describe('for base probabilities', function () {

        it('when randGenerator.getFloat returns 0.029', function () {
          fakeRandGenerator.getFloat.and.returnValue(0.029);
          block.generateBlockKind(fakeRandGenerator,
                                  Block.baseKindProbabilities,
                                  Block.baseAdvancedKindProbabilityFactor);
          expect(block.getAdvancedResourceProbability).toHaveBeenCalledWith(
            Block.advancedKindMaxProbabilityMultiplier * Block.baseAdvancedKindProbabilityFactor,
            Block.baseAdvancedKindProbabilityFactor
          );
        });

        it('when randGenerator.getFloat returns 0.025', function () {
          fakeRandGenerator.getFloat.and.returnValue(0.025);
          block.generateBlockKind(fakeRandGenerator,
                                  Block.baseKindProbabilities,
                                  Block.baseAdvancedKindProbabilityFactor);
          expect(block.getAdvancedResourceProbability).toHaveBeenCalledWith(
            Block.advancedKindMaxProbabilityMultiplier * Block.baseAdvancedKindProbabilityFactor,
            Block.baseAdvancedKindProbabilityFactor
          );
        });

        it('when randGenerator.getFloat returns 0.01', function () {
          fakeRandGenerator.getFloat.and.returnValue(0.01);
          block.generateBlockKind(fakeRandGenerator,
                                  Block.baseKindProbabilities,
                                  Block.baseAdvancedKindProbabilityFactor);
          expect(block.getAdvancedResourceProbability).toHaveBeenCalledWith(
            Block.advancedKindMaxProbabilityMultiplier * Block.baseAdvancedKindProbabilityFactor,
            Block.baseAdvancedKindProbabilityFactor
          );
        });

        it('when randGenerator.getFloat returns 0.00001', function () {
          fakeRandGenerator.getFloat.and.returnValue(0.00001);
          block.generateBlockKind(fakeRandGenerator,
                                  Block.baseKindProbabilities,
                                  Block.baseAdvancedKindProbabilityFactor);
          expect(block.getAdvancedResourceProbability).toHaveBeenCalledWith(
            Block.advancedKindMaxProbabilityMultiplier * Block.baseAdvancedKindProbabilityFactor,
            Block.baseAdvancedKindProbabilityFactor
          );
        });

        it('when randGenerator.getFloat returns 0', function () {
          fakeRandGenerator.getFloat.and.returnValue(0);
          block.generateBlockKind(fakeRandGenerator,
                                  Block.baseKindProbabilities,
                                  Block.baseAdvancedKindProbabilityFactor);
          expect(block.getAdvancedResourceProbability).toHaveBeenCalledWith(
            Block.advancedKindMaxProbabilityMultiplier * Block.baseAdvancedKindProbabilityFactor,
            Block.baseAdvancedKindProbabilityFactor
          );
        });

      });

      describe('for advKindProbabilityFactor = 0.001 and ' +
               'advancedKindMaxProbabilityMultiplier = 6', function () {

        beforeEach(function () {
          Block.advancedKindMaxProbabilityMultiplier = 6;
        });

        it('when randGenerator.getFloat returns 0.0000001', function () {
          fakeRandGenerator.getFloat.and.returnValue(0.0000001);
          block.generateBlockKind(fakeRandGenerator,
                                  Block.baseKindProbabilities, 0.001);
          expect(block.getAdvancedResourceProbability).toHaveBeenCalledWith(
            6 * 0.001, 0.001);
        });

        it('when randGenerator.getFloat returns 0.0059', function () {
          fakeRandGenerator.getFloat.and.returnValue(0.0059);
          block.generateBlockKind(fakeRandGenerator,
                                  Block.baseKindProbabilities, 0.001);
          expect(block.getAdvancedResourceProbability).toHaveBeenCalledWith(
            6 * 0.001, 0.001);
        });

      });

    });

    describe('calls getAdvancedResourceKindClass', function () {

      let block;

      beforeEach(function () {
        block = new Block(0, 0, null, {});
        spyOn(block, 'getAdvancedResourceProbability');
        spyOn(block, 'getAdvancedResourceKindClass')
          .and.returnValue(function FakeAdvancedResClass() { });
      });

      describe('for base probabilities', function () {

        it('when randGenerator.getFloat returns 0.0149 and ' +
           'getAdvancedResourceProbability returns 0.015', function () {
          fakeRandGenerator.getFloat.and.returnValue(0.0149);
          block.getAdvancedResourceProbability.and.returnValue(0.015);
          block.generateBlockKind(fakeRandGenerator,
                                  Block.baseKindProbabilities,
                                  Block.baseAdvancedKindProbabilityFactor);
          expect(block.getAdvancedResourceKindClass)
            .toHaveBeenCalledWith(fakeRandGenerator);
        });

        it('when randGenerator.getFloat returns 0.01 and ' +
           'getAdvancedResourceProbability returns 0.015', function () {
          fakeRandGenerator.getFloat.and.returnValue(0.01);
          block.getAdvancedResourceProbability.and.returnValue(0.015);
          block.generateBlockKind(fakeRandGenerator,
                                  Block.baseKindProbabilities,
                                  Block.baseAdvancedKindProbabilityFactor);
          expect(block.getAdvancedResourceKindClass)
            .toHaveBeenCalledWith(fakeRandGenerator);
        });

        it('when randGenerator.getFloat returns 0.005 and ' +
           'getAdvancedResourceProbability returns 0.092', function () {
          fakeRandGenerator.getFloat.and.returnValue(0.005);
          block.getAdvancedResourceProbability.and.returnValue(0.092);
          block.generateBlockKind(fakeRandGenerator,
                                  Block.baseKindProbabilities,
                                  Block.baseAdvancedKindProbabilityFactor);
          expect(block.getAdvancedResourceKindClass)
            .toHaveBeenCalledWith(fakeRandGenerator);
        });

        it('when randGenerator.getFloat returns 0.00001 and ' +
           'getAdvancedResourceProbability returns 0.00002', function () {
          fakeRandGenerator.getFloat.and.returnValue(0.00001);
          block.getAdvancedResourceProbability.and.returnValue(0.00002);
          block.generateBlockKind(fakeRandGenerator,
                                  Block.baseKindProbabilities,
                                  Block.baseAdvancedKindProbabilityFactor);
          expect(block.getAdvancedResourceKindClass)
            .toHaveBeenCalledWith(fakeRandGenerator);
        });

        it('when randGenerator.getFloat returns 0 and ' +
           'getAdvancedResourceProbability returns 0.00001', function () {
          fakeRandGenerator.getFloat.and.returnValue(0);
          block.getAdvancedResourceProbability.and.returnValue(0.00001);
          block.generateBlockKind(fakeRandGenerator,
                                  Block.baseKindProbabilities,
                                  Block.baseAdvancedKindProbabilityFactor);
          expect(block.getAdvancedResourceKindClass)
            .toHaveBeenCalledWith(fakeRandGenerator);
        });

      });

    });

    describe('does not call getAdvancedResourceKindClass', function () {

      let block;

      beforeEach(function () {
        block = new Block(0, 0, null, {});
        spyOn(block, 'getAdvancedResourceProbability');
        spyOn(block, 'getAdvancedResourceKindClass').and.returnValue({});
      });

      describe('for base probabilities', function () {

        it('when randGenerator.getFloat returns 0.015 and ' +
           'getAdvancedResourceProbability returns 0.015', function () {
          fakeRandGenerator.getFloat.and.returnValue(0.015);
          block.getAdvancedResourceProbability.and.returnValue(0.015);
          block.generateBlockKind(fakeRandGenerator,
                                  Block.baseKindProbabilities,
                                  Block.baseAdvancedKindProbabilityFactor);
          expect(block.getAdvancedResourceKindClass).not.toHaveBeenCalled();
        });

        it('when randGenerator.getFloat returns 0.025 and ' +
           'getAdvancedResourceProbability returns 0.015', function () {
          fakeRandGenerator.getFloat.and.returnValue(0.025);
          block.getAdvancedResourceProbability.and.returnValue(0.015);
          block.generateBlockKind(fakeRandGenerator,
                                  Block.baseKindProbabilities,
                                  Block.baseAdvancedKindProbabilityFactor);
          expect(block.getAdvancedResourceKindClass).not.toHaveBeenCalled();
        });

        it('when randGenerator.getFloat returns 0.01 and ' +
           'getAdvancedResourceProbability returns 0.009', function () {
          fakeRandGenerator.getFloat.and.returnValue(0.01);
          block.getAdvancedResourceProbability.and.returnValue(0.009);
          block.generateBlockKind(fakeRandGenerator,
                                  Block.baseKindProbabilities,
                                  Block.baseAdvancedKindProbabilityFactor);
          expect(block.getAdvancedResourceKindClass).not.toHaveBeenCalled();
        });

        it('when randGenerator.getFloat returns 0.001 and ' +
           'getAdvancedResourceProbability returns 0.001', function () {
          fakeRandGenerator.getFloat.and.returnValue(0.001);
          block.getAdvancedResourceProbability.and.returnValue(0.001);
          block.generateBlockKind(fakeRandGenerator,
                                  Block.baseKindProbabilities,
                                  Block.baseAdvancedKindProbabilityFactor);
          expect(block.getAdvancedResourceKindClass).not.toHaveBeenCalled();
        });

        it('when randGenerator.getFloat returns 0.00001 and ' +
           'getAdvancedResourceProbability returns 0', function () {
          fakeRandGenerator.getFloat.and.returnValue(0.00001);
          block.getAdvancedResourceProbability.and.returnValue(0);
          block.generateBlockKind(fakeRandGenerator,
                                  Block.baseKindProbabilities,
                                  Block.baseAdvancedKindProbabilityFactor);
          expect(block.getAdvancedResourceKindClass).not.toHaveBeenCalled();
        });

        it('when randGenerator.getFloat returns 0 and ' +
           'getAdvancedResourceProbability returns 0', function () {
          fakeRandGenerator.getFloat.and.returnValue(0);
          block.getAdvancedResourceProbability.and.returnValue(0);
          block.generateBlockKind(fakeRandGenerator,
                                  Block.baseKindProbabilities,
                                  Block.baseAdvancedKindProbabilityFactor);
          expect(block.getAdvancedResourceKindClass).not.toHaveBeenCalled();
        });

        it('when randGenerator.getFloat returns 0.027 and ' +
           'getAdvancedResourceProbability returns 0', function () {
          fakeRandGenerator.getFloat.and.returnValue(0.027);
          block.getAdvancedResourceProbability.and.returnValue(0);
          block.generateBlockKind(fakeRandGenerator,
                                  Block.baseKindProbabilities,
                                  Block.baseAdvancedKindProbabilityFactor);
          expect(block.getAdvancedResourceKindClass).not.toHaveBeenCalled();
        });

      });

    });

    describe('using base probabilities sets block kind to', function () {

      let block;
      let fakeAdvancedResClass;

      beforeEach(function () {
        block = new Block();
        spyOn(block, 'getAdvancedResourceProbability');
        fakeAdvancedResClass = function () { };
        spyOn(block, 'getAdvancedResourceKindClass')
          .and.returnValue(fakeAdvancedResClass);
      });

      it('a new Plain instance if random returns 0.6', function () {
        fakeRandGenerator.getFloat.and.returnValue(0.6);
        block.generateBlockKind(fakeRandGenerator,
                                Block.baseKindProbabilities,
                                Block.baseAdvancedKindProbabilityFactor);
        expect(block.kind instanceof Plain).toBeTrue();
      });

      it('a new Forest instance if random returns 0.35', function () {
        fakeRandGenerator.getFloat.and.returnValue(0.35);
        block.generateBlockKind(fakeRandGenerator,
                                Block.baseKindProbabilities,
                                Block.baseAdvancedKindProbabilityFactor);
        expect(block.kind instanceof Forest).toBeTrue();
      });

      it('a new Hill instance if random returns 0.15', function () {
        fakeRandGenerator.getFloat.and.returnValue(0.15);
        block.generateBlockKind(fakeRandGenerator,
                                Block.baseKindProbabilities,
                                Block.baseAdvancedKindProbabilityFactor);
        expect(block.kind instanceof Hill).toBeTrue();
      });

      it('a new Mountain instance if random returns 0.065', function () {
        fakeRandGenerator.getFloat.and.returnValue(0.065);
        block.generateBlockKind(fakeRandGenerator,
                                Block.baseKindProbabilities,
                                Block.baseAdvancedKindProbabilityFactor);
        expect(block.kind instanceof Mountain).toBeTrue();
      });

      it('getAdvancedResourceKindClass result if random returns 0.025 ' +
         'and getAdvancedResourceProbability returns 0.03', function () {
        fakeRandGenerator.getFloat.and.returnValue(0.025);
        block.getAdvancedResourceProbability.and.returnValue(0.03);
        block.generateBlockKind(fakeRandGenerator,
                                Block.baseKindProbabilities,
                                Block.baseAdvancedKindProbabilityFactor);
        expect(block.kind instanceof fakeAdvancedResClass).toBeTrue();
      });

      it('a new Water instance if random returns 0.05 ' +
         'and getAdvancedResourceProbability returns 0.045', function () {
        fakeRandGenerator.getFloat.and.returnValue(0.05);
        block.getAdvancedResourceProbability.and.returnValue(0.045);
        block.generateBlockKind(fakeRandGenerator,
                                Block.baseKindProbabilities,
                                Block.baseAdvancedKindProbabilityFactor);
        expect(block.kind instanceof Water).toBeTrue();
      });

      it('a new Water instance if random returns 0.064 ' +
         'and getAdvancedResourceProbability returns 0.06', function () {
        fakeRandGenerator.getFloat.and.returnValue(0.064);
        block.getAdvancedResourceProbability.and.returnValue(0.06);
        block.generateBlockKind(fakeRandGenerator,
                                Block.baseKindProbabilities,
                                Block.baseAdvancedKindProbabilityFactor);
        expect(block.kind instanceof Water).toBeTrue();
      });

      it('a new Plain instance if random returns 0.01 ' +
         'and getAdvancedResourceProbability returns 0.005', function () {
        fakeRandGenerator.getFloat.and.returnValue(0.01);
        block.getAdvancedResourceProbability.and.returnValue(0.005);
        block.generateBlockKind(fakeRandGenerator,
                                Block.baseKindProbabilities,
                                Block.baseAdvancedKindProbabilityFactor);
        expect(block.kind instanceof Plain).toBeTrue();
      });

    });

    describe('using given kind probabilities sets block kind to', function () {

      let block;
      let fakeAdvancedResClass;

      beforeEach(function () {
        block = new Block();
        spyOn(block, 'getAdvancedResourceProbability');
        fakeAdvancedResClass = function () { };
        spyOn(block, 'getAdvancedResourceKindClass').and.returnValue(fakeAdvancedResClass);
      });

      it('a new Plain instance if random returns 0.2 and ' +
         'plain probability threshold is 0.2', function () {
        fakeRandGenerator.getFloat.and.returnValue(0.2);
        block.generateBlockKind(fakeRandGenerator, [
          { 0.2: Plain }, { 0.0: Forest }
        ], Block.baseAdvancedKindProbabilityFactor);
        expect(block.kind instanceof Plain).toBeTrue();
      });

      it('a new Forest instance if random returns 0.9 and ' +
         'forest probability threshold is 0.85', function () {
        fakeRandGenerator.getFloat.and.returnValue(0.9);
        block.generateBlockKind(fakeRandGenerator, [
          { 0.95: Plain }, { 0.85: Forest }, { 0.0: Hill }
        ], Block.baseAdvancedKindProbabilityFactor);
        expect(block.kind instanceof Forest).toBeTrue();
      });

      it('a new Mountain instance if random returns 0.1 and ' +
         'mountain probability threshold is 0.0', function () {
        fakeRandGenerator.getFloat.and.returnValue(0.1);
        block.generateBlockKind(fakeRandGenerator, [
          { 0.5: Plain }, { 0.25: Forest }, { 0.0: Mountain }
        ], Block.baseAdvancedKindProbabilityFactor);
        expect(block.kind instanceof Mountain).toBeTrue();
      });

    });

  });

  describe('generateBlockRichness', function () {

    let block;
    let fakeRandGenerator;

    beforeEach(function () {
      block = new Block();
      spyOn(block, 'rich');
      spyOn(block, 'canBeRich');
      fakeRandGenerator = jasmine.createSpyObj('randGenerator', ['getFloat']);
    });

    it('calls getFloat on the given randGenerator ' +
       'if the block can be rich', function () {
      block.canBeRich.and.returnValue(true);
      block.generateBlockRichness(fakeRandGenerator, Block.baseRichProbability);
      expect(fakeRandGenerator.getFloat).toHaveBeenCalledWith();
    });

    it('does not call getFloat on the given randGenerator ' +
       'if the block can be rich', function () {
      block.canBeRich.and.returnValue(false);
      block.generateBlockRichness(fakeRandGenerator, Block.baseRichProbability);
      expect(fakeRandGenerator.getFloat).not.toHaveBeenCalled();
    });

    describe('for baseRichProbability', function () {

      it('sets rich to true if canBeRich returns true and ' +
         'random returns 0.019', function () {
        block.canBeRich.and.returnValue(true);
        fakeRandGenerator.getFloat.and.returnValue(0.019);
        block.generateBlockRichness(fakeRandGenerator, Block.baseRichProbability);
        expect(block.rich).toBeTrue();
      });

      it('sets rich to false if canBeRich returns true but ' +
         'random returns 0.979', function () {
        block.canBeRich.and.returnValue(true);
        fakeRandGenerator.getFloat.and.returnValue(0.021);
        block.generateBlockRichness(fakeRandGenerator, Block.baseRichProbability);
        expect(block.rich).toBeFalse();
      });

      it('sets rich to false if canBeRich returns false', function () {
        block.canBeRich.and.returnValue(false);
        block.generateBlockRichness(fakeRandGenerator, Block.baseRichProbability);
        expect(block.rich).toBeFalse();
      });

      it('calls canBeRich', function () {
        block.generateBlockRichness(fakeRandGenerator, Block.baseRichProbability);
        expect(block.canBeRich).toHaveBeenCalledWith();
      });

    });

    describe('for a custom rich probability', function () {

      it('sets rich to true if canBeRich returns true, ' +
         'random returns 0.049 and richProbability is 0.5', function () {
        block.canBeRich.and.returnValue(true);
        fakeRandGenerator.getFloat.and.returnValue(0.049);
        block.generateBlockRichness(fakeRandGenerator, 0.5);
        expect(block.rich).toBeTrue();
      });

      it('sets rich to true if canBeRich returns true, ' +
         'random returns 0.0001 and richProbability is 0.0002', function () {
        block.canBeRich.and.returnValue(true);
        fakeRandGenerator.getFloat.and.returnValue(0.0001);
        block.generateBlockRichness(fakeRandGenerator, 0.0002);
        expect(block.rich).toBeTrue();
      });

      it('sets rich to false if canBeRich returns true, ' +
         'random returns 0.5 and richProbability is 0.4', function () {
        block.canBeRich.and.returnValue(true);
        fakeRandGenerator.getFloat.and.returnValue(0.5);
        block.generateBlockRichness(fakeRandGenerator, 0.4);
        expect(block.rich).toBeFalse();
      });

      it('sets rich to false if canBeRich returns true, ' +
         'random returns 0.0001 and richProbability is 0.00009', function () {
        block.canBeRich.and.returnValue(true);
        fakeRandGenerator.getFloat.and.returnValue(0.0001);
        block.generateBlockRichness(fakeRandGenerator, 0.00009);
        expect(block.rich).toBeFalse();
      });

    });

  });

  describe('getAdvancedResourceProbability', function () {

    var block;
    var advKindMaxProbability;
    var advKindProbabilityFactor;

    beforeEach(function () {
      block = new Block(0, 1);
    });

    describe('using base probabilities', function () {

      beforeEach(function () {
        advKindMaxProbability = Block.advancedKindMaxProbabilityMultiplier *
                                Block.baseAdvancedKindProbabilityFactor;
        advKindProbabilityFactor = Block.baseAdvancedKindProbabilityFactor;
      });

      it('returns 0 if block region is falsy', function () {
        var result = block.getAdvancedResourceProbability(
          advKindMaxProbability, advKindProbabilityFactor);
        expect(result).toBe(0);
      });

      it('returns 0 for block region = { x: 0, y: 0 }', function () {
        block.region = { x: 0, y: 0 };
        var result = block.getAdvancedResourceProbability(
          advKindMaxProbability, advKindProbabilityFactor);
        expect(result).toBe(0);
      });

      it('returns 0 for block region = { x: 0, y: 1 }', function () {
        block.region = { x: 0, y: 1 };
        var result = block.getAdvancedResourceProbability(
          advKindMaxProbability, advKindProbabilityFactor);
        expect(result).toBe(0);
      });

      it('returns 0 for block region = { x: 1, y: 1 }', function () {
        block.region = { x: 1, y: 1 };
        var result = block.getAdvancedResourceProbability(
          advKindMaxProbability, advKindProbabilityFactor);
        expect(result).toBe(0);
      });

      it('returns 0 for block region = { x: -1, y: 0 }', function () {
        block.region = { x: -1, y: 0 };
        var result = block.getAdvancedResourceProbability(
          advKindMaxProbability, advKindProbabilityFactor);
        expect(result).toBe(0);
      });

      it('returns 0 for block region = { x: 0, y: -1 }', function () {
        block.region = { x: 0, y: -1 };
        var result = block.getAdvancedResourceProbability(
          advKindMaxProbability, advKindProbabilityFactor);
        expect(result).toBe(0);
      });

      it('returns 0 for block region = { x: 1, y: -1 }', function () {
        block.region = { x: 1, y: -1 };
        var result = block.getAdvancedResourceProbability(
          advKindMaxProbability, advKindProbabilityFactor);
        expect(result).toBe(0);
      });

      it('returns 0.008 for block region = { x: -1, y: 2 }', function () {
        block.region = { x: -1, y: 2 };
        var result = block.getAdvancedResourceProbability(
          advKindMaxProbability, advKindProbabilityFactor);
        expect(result).toBe(0.008);
      });

      it('returns 0.008 for block region = { x: -2, y: 1 }', function () {
        block.region = { x: -2, y: 1 };
        var result = block.getAdvancedResourceProbability(
          advKindMaxProbability, advKindProbabilityFactor);
        expect(result).toBe(0.008);
      });

      it('returns 0.012 for block region = { x: -3, y: 3 }', function () {
        block.region = { x: -3, y: 3 };
        var result = block.getAdvancedResourceProbability(
          advKindMaxProbability, advKindProbabilityFactor);
        expect(result).toBe(0.012);
      });

      it('returns 0.028 for block region = { x: -8, y: 6 }', function () {
        block.region = { x: -8, y: 6 };
        var result = block.getAdvancedResourceProbability(
          advKindMaxProbability, advKindProbabilityFactor);
        expect(result).toBe(0.032);
      });

      it('returns 0.03 for block region = { x: -9, y: 6 }', function () {
        block.region = { x: -9, y: 6 };
        var result = block.getAdvancedResourceProbability(
          advKindMaxProbability, advKindProbabilityFactor);
        expect(result).toBe(0.032);
      });

      it('returns 0.03 for block region = { x: 9, y: 9 }', function () {
        block.region = { x: 9, y: 9 };
        var result = block.getAdvancedResourceProbability(
          advKindMaxProbability, advKindProbabilityFactor);
        expect(result).toBe(0.032);
      });

      it('returns 0.03 for block region = { x: 9, y: -2 }', function () {
        block.region = { x: 9, y: -2 };
        var result = block.getAdvancedResourceProbability(
          advKindMaxProbability, advKindProbabilityFactor);
        expect(result).toBe(0.032);
      });

      it('returns 0.03 for block region = { x: 0, y: 9 }', function () {
        block.region = { x: 0, y: 9 };
        var result = block.getAdvancedResourceProbability(
          advKindMaxProbability, advKindProbabilityFactor);
        expect(result).toBe(0.032);
      });

      it('returns 0.032 for block region = { x: -1000, y: 20000 }', function () {
        block.region = { x: -1000, y: 20000 };
        var result = block.getAdvancedResourceProbability(
          advKindMaxProbability, advKindProbabilityFactor);
        expect(result).toBe(0.032);
      });

    });

    describe('using advKindMaxProbability = 0.5 and ' +
             'advKindProbabilityFactor = 0.1', function () {

      beforeEach(function () {
        advKindMaxProbability = 0.5;
        advKindProbabilityFactor = 0.1;
      });

      it('returns 0 for block region = { x: -1, y: 0 }', function () {
        block.region = { x: -1, y: 0 };
        var result = block.getAdvancedResourceProbability(
          advKindMaxProbability, advKindProbabilityFactor);
        expect(result).toBe(0);
      });

      it('returns 0.02 for block region = { x: -2, y: 0 }', function () {
        block.region = { x: -2, y: 0 };
        var result = block.getAdvancedResourceProbability(
          advKindMaxProbability, advKindProbabilityFactor);
        expect(result).toBe(0.2);
      });

      it('returns 0.05 for block region = { x: 2, y: 11 }', function () {
        block.region = { x: 2, y: 11 };
        var result = block.getAdvancedResourceProbability(
          advKindMaxProbability, advKindProbabilityFactor);
        expect(result).toBe(0.5);
      });

    });

  });

  describe('getAdvancedResourceKindClass', function () {

    let block;
    let fakeRandGenerator;

    beforeEach(function () {
      block = new Block(0, -1, null, {});
      fakeRandGenerator = jasmine.createSpyObj('randGenerator', ['getFloat']);
    });

    it('calls getFloat on the given randGenerator', function () {
      block.getAdvancedResourceKindClass(fakeRandGenerator);
      expect(fakeRandGenerator.getFloat).toHaveBeenCalledWith();
    });

    it('returns WildHorses when random returns 0.99', function () {
      fakeRandGenerator.getFloat.and.returnValue(0.99);
      const result = block.getAdvancedResourceKindClass(fakeRandGenerator);
      expect(result).toBe(WildHorses);
    });

    it('returns WildHorses when random returns 0.601', function () {
      fakeRandGenerator.getFloat.and.returnValue(0.601);
      const result = block.getAdvancedResourceKindClass(fakeRandGenerator);
      expect(result).toBe(WildHorses);
    });

    it('returns Coalfield when random returns 0.6', function () {
      fakeRandGenerator.getFloat.and.returnValue(0.6);
      const result = block.getAdvancedResourceKindClass(fakeRandGenerator);
      expect(result).toBe(Coalfield);
    });

    it('returns Coalfield when random returns 0.30005', function () {
      fakeRandGenerator.getFloat.and.returnValue(0.30005);
      const result = block.getAdvancedResourceKindClass(fakeRandGenerator);
      expect(result).toBe(Coalfield);
    });

    it('returns SilverDeposit when random returns 0.29', function () {
      fakeRandGenerator.getFloat.and.returnValue(0.29);
      const result = block.getAdvancedResourceKindClass(fakeRandGenerator);
      expect(result).toBe(SilverDeposit);
    });

    it('returns SilverDeposit when random returns 0.11', function () {
      fakeRandGenerator.getFloat.and.returnValue(0.11);
      const result = block.getAdvancedResourceKindClass(fakeRandGenerator);
      expect(result).toBe(SilverDeposit);
    });

    it('returns GoldDeposit when random returns 0.1', function () {
      fakeRandGenerator.getFloat.and.returnValue(0.1);
      const result = block.getAdvancedResourceKindClass(fakeRandGenerator);
      expect(result).toBe(GoldDeposit);
    });

    it('returns GoldDeposit when random returns 0.0351', function () {
      fakeRandGenerator.getFloat.and.returnValue(0.0351);
      const result = block.getAdvancedResourceKindClass(fakeRandGenerator);
      expect(result).toBe(GoldDeposit);
    });

    it('returns GemstoneDeposit when random returns 0.035', function () {
      fakeRandGenerator.getFloat.and.returnValue(0.035);
      const result = block.getAdvancedResourceKindClass(fakeRandGenerator);
      expect(result).toBe(GemstoneDeposit);
    });

    it('returns GemstoneDeposit when random returns 0.01', function () {
      fakeRandGenerator.getFloat.and.returnValue(0.01);
      const result = block.getAdvancedResourceKindClass(fakeRandGenerator);
      expect(result).toBe(GemstoneDeposit);
    });

    it('returns GemstoneDeposit when random returns 0', function () {
      fakeRandGenerator.getFloat.and.returnValue(0);
      const result = block.getAdvancedResourceKindClass(fakeRandGenerator);
      expect(result).toBe(GemstoneDeposit);
    });

  });

  describe('resetNeighborsByKind', function () {

    it('sets neighborsByKind to object with 9 blocks kinds as keys and empty arrays as values', function () {
      var block = new Block(0, 1, {});
      block.neighborsByKind = jasmine.createSpy('neighborsByKind');
      block.resetNeighborsByKind();
      expect(typeof block.neighborsByKind).toBe('object');
      expect(Object.keys(block.neighborsByKind)).toContain('plain');
      expect(Object.keys(block.neighborsByKind)).toContain('forest');
      expect(Object.keys(block.neighborsByKind)).toContain('hill');
      expect(Object.keys(block.neighborsByKind)).toContain('mountain');
      expect(Object.keys(block.neighborsByKind)).toContain('coalfield');
      expect(Object.keys(block.neighborsByKind)).toContain('wildHorses');
      expect(Object.keys(block.neighborsByKind)).toContain('goldDeposit');
      expect(Object.keys(block.neighborsByKind)).toContain('silverDeposit');
      expect(Object.keys(block.neighborsByKind)).toContain('gemstoneDeposit');
      expect(Object.keys(block.neighborsByKind).length).toBe(9);
      expect(block.neighborsByKind['plain']).toEqual([]);
      expect(block.neighborsByKind['forest']).toEqual([]);
      expect(block.neighborsByKind['hill']).toEqual([]);
      expect(block.neighborsByKind['mountain']).toEqual([]);
      expect(block.neighborsByKind['coalfield']).toEqual([]);
      expect(block.neighborsByKind['wildHorses']).toEqual([]);
      expect(block.neighborsByKind['goldDeposit']).toEqual([]);
      expect(block.neighborsByKind['silverDeposit']).toEqual([]);
      expect(block.neighborsByKind['gemstoneDeposit']).toEqual([]);
    });

  });

  describe('changeKind', function () {

    it('sets the given kind to the block', function () {
      var oldKind = jasmine.createSpy('old kind');
      var newKind = jasmine.createSpy('new kind');
      var block = new Block(2, 1, {}, oldKind);
      expect(block.kind).toBe(oldKind);
      block.changeKind(newKind);
      expect(block.kind).toBe(newKind);
    });

    it('sets block to rich if rich=true passed', function () {
      var block = new Block(0, -1, {}, jasmine.createSpy('old kind'), false);
      block.changeKind(jasmine.createSpy('new kind'), true);
      expect(block.rich).toBeTrue();
    });

  });

  describe('addBuilding', function () {

    it('adds the given building to buildingsByClass property under a proper index', function () {
      var block = new Block(0, 1);
      expect(block.buildingsByClass['FakeBuilding']).toBeUndefined();
      function FakeBuilding() { return null; }

      var fakeBuildingInstance = new FakeBuilding();
      block.addBuilding(fakeBuildingInstance);
      expect(block.buildingsByClass['FakeBuilding'].length).toBe(1);
      expect(block.buildingsByClass['FakeBuilding'][0]).toBe(fakeBuildingInstance);

      var fakeBuildingInstance2 = new FakeBuilding();
      block.addBuilding(fakeBuildingInstance2);
      expect(block.buildingsByClass['FakeBuilding'].length).toBe(2);
      expect(block.buildingsByClass['FakeBuilding'][1]).toBe(fakeBuildingInstance2);
    });

    it('adds the given building to buildings property', function () {
      var block = new Block(0, 1);
      expect(block.buildings.length).toBe(0);
      function FakeBuilding() { return null; }

      var fakeBuildingInstance = new FakeBuilding();
      block.addBuilding(fakeBuildingInstance);
      expect(block.buildings.length).toBe(1);
      expect(block.buildings[0]).toBe(fakeBuildingInstance);

      var fakeBuildingInstance2 = new FakeBuilding();
      block.addBuilding(fakeBuildingInstance2);
      expect(block.buildings.length).toBe(2);
      expect(block.buildings[1]).toBe(fakeBuildingInstance2);
    });

  });

  describe('removeBuilding', function () {

    it('removes the given building from buildingsByClass property from a proper index', function () {
      var block = new Block(0, 1);
      function FakeBuilding(id) { this.toString = function () { return id; }; }
      var fakeBuildingInstance = new FakeBuilding('fake-building-1');
      var fakeBuildingInstance2 = new FakeBuilding('fake-building-2');
      block.addBuilding(fakeBuildingInstance);
      block.addBuilding(fakeBuildingInstance2);
      expect(block.buildingsByClass['FakeBuilding'].length).toBe(2);

      block.removeBuilding(fakeBuildingInstance);
      expect(block.buildingsByClass['FakeBuilding'].length).toBe(1);
      expect(block.buildingsByClass['FakeBuilding'][0]).not.toBe(fakeBuildingInstance);
      expect(block.buildingsByClass['FakeBuilding'][0]).toBe(fakeBuildingInstance2);
    });

    it('removes the given building from buildings property', function () {
      var block = new Block(0, 1);
      function FakeBuilding(id) { this.toString = function () { return id; }; }
      var fakeBuildingInstance = new FakeBuilding('fake-building-1');
      var fakeBuildingInstance2 = new FakeBuilding('fake-building-2');
      block.addBuilding(fakeBuildingInstance);
      block.addBuilding(fakeBuildingInstance2);
      expect(block.buildings.length).toBe(2);

      block.removeBuilding(fakeBuildingInstance2);
      expect(block.buildings.length).toBe(1);
      expect(block.buildings[0]).not.toBe(fakeBuildingInstance2);
      expect(block.buildings[0]).toBe(fakeBuildingInstance);
    });

  });

  describe('getBuildingCountByClass', function () {

    var block;

    beforeEach(function () {
      block = new Block(0, 1);
      function FakeBuilding(id) {
        this.toString = function () { return id; };
      }
      block.addBuilding(new FakeBuilding('fake-building-1'));
      block.addBuilding(new FakeBuilding('fake-building-2'));
      expect(block.buildingsByClass.FakeBuilding.length).toBe(2);
    });

    it('returns number of building for the given class name', function () {
      var result = block.getBuildingCountByClass('FakeBuilding');
      expect(result).toBe(2);
    });

    it('returns 0 if there are no buildings for a given class name', function () {
      var result = block.getBuildingCountByClass('AnotherFakeBuilding');
      expect(result).toBe(0);
    });

    it('throws an Error if the given class name is falsy', function () {
      expect(function() {
        block.getBuildingCountByClass(0);
      }).toThrow(new Error('Invalid buildingClassName given: 0'));
    });

  });

  describe('isMutable', function () {

    var block;

    beforeEach(function () {
      block = new Block();
    });

    it('returns true if kind mainResourceLevel is 1 ' +
       'and block is not rich and it has not mutated', function () {
      block.kind = { mainResourceLevel: 1 };
      block.rich = false;
      block.mutated = false;
      var result = block.isMutable();
      expect(result).toBeTrue();
    });

    it('returns false if kind mainResourceLevel is 0 ' +
       'and block is not rich and it has not mutated', function () {
      block.kind = { mainResourceLevel: 0 };
      block.rich = false;
      block.mutated = false;
      var result = block.isMutable();
      expect(result).toBeFalse();
    });

    it('returns false if kind mainResourceLevel is 2 ' +
       'and block is not rich and it has not mutated', function () {
      block.kind = { mainResourceLevel: 2 };
      block.rich = false;
      block.mutated = false;
      var result = block.isMutable();
      expect(result).toBeFalse();
    });

    it('returns false if kind mainResourceLevel is 1 ' +
       'and block is not rich but it has mutated', function () {
      block.kind = { mainResourceLevel: 1 };
      block.rich = false;
      block.mutated = true;
      var result = block.isMutable();
      expect(result).toBeFalse();
    });

    it('returns false if kind mainResourceLevel is 1 ' +
       'and block has not mutated but it is rich', function () {
      block.kind = { mainResourceLevel: 1 };
      block.rich = true;
      block.mutated = false;
      var result = block.isMutable();
      expect(result).toBeFalse();
    });

    it('returns false if kind mainResourceLevel is 1 ' +
       'but block is rich and it has mutated', function () {
      block.kind = { mainResourceLevel: 1 };
      block.rich = true;
      block.mutated = true;
      var result = block.isMutable();
      expect(result).toBeFalse();
    });

    it('returns false if kind mainResourceLevel is 3, ' +
       'block is rich and it has mutated', function () {
      block.kind = { mainResourceLevel: 3 };
      block.rich = true;
      block.mutated = true;
      var result = block.isMutable();
      expect(result).toBeFalse();
    });

  });

  describe('canBeRich', function () {

    var block;

    beforeEach(function () {
      block = new Block();
    });

    it('returns true if kind is improvable ' +
       'and mainResourceLevel is 1', function () {
      block.kind = {
        improvable: true,
        mainResourceLevel: 1
      };
      var result = block.canBeRich();
      expect(result).toBeTrue();
    });

    it('returns false if kind is improvable ' +
       'but mainResourceLevel is 2', function () {
      block.kind = {
        improvable: true,
        mainResourceLevel: 2
      };
      var result = block.canBeRich();
      expect(result).toBeFalse();
    });

    it('returns false if kind is improvable ' +
       'but mainResourceLevel is 0', function () {
      block.kind = {
        improvable: true,
        mainResourceLevel: 0
      };
      var result = block.canBeRich();
      expect(result).toBeFalse();
    });

    it('returns false if kind is not improvable ' +
       'and mainResourceLevel is 1', function () {
      block.kind = {
        improvable: false,
        mainResourceLevel: 1
      };
      var result = block.canBeRich();
      expect(result).toBeFalse();
    });

    it('returns false if kind is not improvable ' +
       'and mainResourceLevel is 3', function () {
      block.kind = {
        improvable: false,
        mainResourceLevel: 3
      };
      var result = block.canBeRich();
      expect(result).toBeFalse();
    });

  });

  describe('createMutatedKind', function () {

    let block;
    let fakeKind;
    let anotherFakeKind;
    let yetAnotherFakeKind;
    let fakeRandGenerator;

    beforeEach(function () {
      block = new Block();
      block.kind = jasmine.createSpyObj('kind', ['toString']);
      block.kind.toString.and.returnValue('oldKind');
      fakeKind = function () { };
      anotherFakeKind = function () { };
      yetAnotherFakeKind = function () { };
      fakeRandGenerator = jasmine.createSpyObj('randGenerator', ['getFloat']);
      fakeRandGenerator.getFloat.and.returnValue(0.5);
    });

    it('calls getFloat on the given fakeRandGenerator', function () {
      Block.kindMutations = { oldKind: [ { 0: fakeKind } ] };
      block.createMutatedKind(fakeRandGenerator);
      expect(fakeRandGenerator.getFloat).toHaveBeenCalledWith();
    });

    it('returns a new instance of fakeKind' +
       'for kindMutations: { oldKind: [ { 0: fakeKind } ] }', function () {
      Block.kindMutations = { oldKind: [ { 0: fakeKind } ] };
      const result = block.createMutatedKind(fakeRandGenerator);
      expect(result instanceof fakeKind).toBeTrue();
    });

    it('returns a new instance of fakeKind when random returns 0.5 ' +
       'for kindMutations: { oldKind: [ { 0.6: anotherFakeKind }, { 0: fakeKind } ] }', function () {
      Block.kindMutations = { oldKind: [ { 0.6: anotherFakeKind }, { 0: fakeKind } ] };
      const result = block.createMutatedKind(fakeRandGenerator);
      expect(result instanceof fakeKind).toBeTrue();
    });

    it('returns a new instance of anotherFakeKind when random returns 0.7 ' +
       'for kindMutations: { oldKind: [ { 0.6: anotherFakeKind }, { 0: fakeKind } ] }', function () {
      fakeRandGenerator.getFloat.and.returnValue(0.7);
      Block.kindMutations = { oldKind: [
        { 0.6: anotherFakeKind }, { 0: fakeKind }
      ]};
      const result = block.createMutatedKind(fakeRandGenerator);
      expect(result instanceof anotherFakeKind).toBeTrue();
    });

    it('returns a new instance of yetAnotherFakeKind when random returns 0.4 ' +
       'for kindMutations: { oldKind: [ { 0.6: anotherFakeKind}, ' +
                                       '{ 0.3: yetAnotherFakeKind}, ' +
                                       '{ 0: fakeKind } ] }', function () {
      fakeRandGenerator.getFloat.and.returnValue(0.4);
      Block.kindMutations = { oldKind: [
        { 0.6: anotherFakeKind }, { 0.3: yetAnotherFakeKind }, { 0: fakeKind }
      ]};
      const result = block.createMutatedKind(fakeRandGenerator);
      expect(result instanceof yetAnotherFakeKind).toBeTrue();
    });

    it('returns a new instance of fakeKind when random returns 0 ' +
       'for kindMutations: { oldKind: [ { 0.6: anotherFakeKind}, {0: fakeKind } ] }', function () {
      fakeRandGenerator.getFloat.and.returnValue(0);
      Block.kindMutations = { oldKind: [
        { 0.6: anotherFakeKind }, { 0: fakeKind }
      ]};
      const result = block.createMutatedKind(fakeRandGenerator);
      expect(result instanceof fakeKind).toBeTrue();
    });

  });

});