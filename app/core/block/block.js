'use strict';

angular.module('square').factory('Block', [
    'Forest', 'Hill', 'Mountain', 'Plain',
    'Coalfield', 'WildHorses', 'GoldDeposit', 'SilverDeposit', 'GemstoneDeposit',
    'Water', 'Math', 'ArrayUtils',
    function (Forest, Hill, Mountain, Plain,
              Coalfield, WildHorses, GoldDeposit, SilverDeposit, GemstoneDeposit,
              Water, Math, ArrayUtils) {

  function Block(x, y, region, kind, rich) {
    this.x = x;
    this.y = y;
    this.region = region;
    if (kind !== undefined) {
      this.kind = kind;
    }
    if (rich !== undefined) {
      this.rich = !!rich;
    }
    this._hasImprovedNeighborOfPlayers = [];  //change into Set
    this._affordableForPlayers = [];  //change into Set
    this.neighborsByPosition = {};
    this.improvedNeighbors = {};
    this.aiValueForPlayer = {};
    this.aiValuesForPlayerMissions = {};
    this.buildingsByClass = {};
    this.buildings = [];
    this.resetNeighborsByKind();
  }

  Block.advancedKindMaxProbabilityMultiplier = 8;
  Block.baseAdvancedKindProbabilityFactor = 0.004;
  Block.baseRichProbability = 0.02;

  Block.baseKindProbabilities = [
    {
      0.6: Plain  // 40%
    }, {
      0.35: Forest  // 25%
    }, {
      0.15: Hill  // 20%
    }, {
      0.065: Mountain  // 8.5%
    }, {
      0.05: Water  // 1.5%
    }, {
      0.0: Plain  // 5%
    }
  ];

  Block.kindMutations = {
    plain: [
      {
        0.6: Forest  // 40%
      }, {
        0.35: Hill  // 25%
      }, {
        0.2: WildHorses  // 15%
      }, {
        0.05: Coalfield  // 15%
      }, {
        0: Water // 5%
      }
    ],
    forest: [
      {
        0.65: Plain  // 35%
      }, {
        0.3: Hill  // 35%
      }, {
        0.15: Coalfield  // 15%
      }, {
        0: WildHorses  // 15%
      }
    ],
    hill: [
      {
        0.65: Plain  // 35%
      }, {
        0.35: Mountain  // 30%
      }, {
        0.10: Forest  // 25%
      }, {
        0: Coalfield  // 10%
      }
    ],
    mountain: [
      {
        0.7: SilverDeposit  // 30%
      }, {
        0.4: GoldDeposit  // 30%
      }, {
        0.15: GemstoneDeposit  // 25%
      }, {
        0: Hill  // 15%
      }
    ]
  };

  Block.calcKindProbabilities = function (mapDifficulty /* 0.0 - 1.0 */) {
    var mapEasiness = 1.0 - mapDifficulty;
    var probabilityModifiers = [0.16, 0.17, 0.13, -0.01, 0.0025, 0.0];
    return Block.baseKindProbabilities.map(function (baseProbabilityObj, idx) {
      var newProbabilityObj = {};
      var baseProbability = Object.keys(baseProbabilityObj)[0];
      var newProbability = parseFloat(baseProbability) + probabilityModifiers[idx] * mapEasiness;
      newProbabilityObj[newProbability] = baseProbabilityObj[baseProbability];
      return newProbabilityObj;
    });
  };

  Block.calcRichProbability = function (mapDifficulty /* 0.0 - 1.0 */) {
    return Block.baseRichProbability * (1 + (0.5 - mapDifficulty));
  };

  Block.calcAdvancedKindProbabilityFactor = function (mapDifficulty /* 0.0 - 1.0 */) {
    return Block.baseAdvancedKindProbabilityFactor * (2 - mapDifficulty);
  };

  Block.prototype.generateBlockKind = function (randGenerator, kindProbabilities, advKindProbabilityFactor) {
    var rand = randGenerator.getFloat();
    var i = -1;
    var probability;
    var KindClass;
    var advKindMaxProbability = Block.advancedKindMaxProbabilityMultiplier * advKindProbabilityFactor;
    if (rand < advKindMaxProbability &&
        rand < this.getAdvancedResourceProbability(advKindMaxProbability, advKindProbabilityFactor)) {
      KindClass = this.getAdvancedResourceKindClass(randGenerator);
    } else {
      do {
        probability = Object.keys(kindProbabilities[++i])[0];
        KindClass = kindProbabilities[i][probability];
      } while (rand < parseFloat(probability));
    }
    this.kind = new KindClass();
  };

  Block.prototype.generateBlockRichness = function (randGenerator,
                                                    richProbability /* 0.01 - 0.03 */) {
    this.rich = this.canBeRich() && randGenerator.getFloat() < richProbability;
  };

  Block.prototype.getAdvancedResourceProbability = function (
      advKindMaxProbability, advKindProbabilityFactor /* 0.004 - 0.008 */) {
    if (!this.region) {
      return 0;
    }
    var absX = Math.abs(this.region.x);
    var absY = Math.abs(this.region.y);
    return Math.min(advKindMaxProbability, Math.max(
      advKindProbabilityFactor * (absX > 1 ? absX : 0),
      advKindProbabilityFactor * (absY > 1 ? absY : 0)
    ));
  };

  Block.prototype.getAdvancedResourceKindClass = function (randGenerator) {
    var rand = randGenerator.getFloat();
    return rand > 0.6 ? WildHorses :
           (rand > 0.3 ? Coalfield :
             (rand > 0.1 ? SilverDeposit :
               (rand > 0.035 ? GoldDeposit : GemstoneDeposit)));
  };

  Block.prototype.resetNeighborsByKind = function () {
    this.neighborsByKind = {
      plain: [],
      forest: [],
      hill: [],
      mountain: [],
      coalfield: [],
      wildHorses: [],
      goldDeposit: [],
      silverDeposit: [],
      gemstoneDeposit: []
    };
  };

  Block.prototype.changeKind = function (newKind, rich) {
    this.kind = newKind;
    if (rich !== undefined) {
      this.rich = rich;
    }
  };

  Block.prototype.setHasImprovedNeighbor = function (player) {
    if (!this.hasImprovedNeighbor(player)) {
      this._hasImprovedNeighborOfPlayers.push(player);
    }
  };

  Block.prototype.unsetHasImprovedNeighbor = function (player) {
    ArrayUtils.removeElement(this._hasImprovedNeighborOfPlayers, player);
  };

  Block.prototype.hasImprovedNeighbor = function (player) {
    return this._hasImprovedNeighborOfPlayers.indexOf(player) > -1;
  };

  Block.prototype.setAffordableForPlayer = function (player) {
    if (!this.affordableForPlayer(player)) {
      this._affordableForPlayers.push(player);
    }
  };

  Block.prototype.unsetAffordableForPlayer = function (player) {
    ArrayUtils.removeElement(this._affordableForPlayers, player);
  };

  Block.prototype.affordableForPlayer = function (player) {
    return this._affordableForPlayers.indexOf(player) > -1;
  };

  Block.prototype.toString = function () {
    return 'Block-r' + (this.region ? this.region.x + 'x' + this.region.y : '') + '_' +
      this.x + 'x' + this.y + 'L' + this.level +
      (this.kind ? this.kind.toString()[0] : '') + (this.rich ? 'R' : '');
  };

  Block.prototype.addBuilding = function (building) {
    this.buildingsByClass[building.constructor.name] = this.buildingsByClass[building.constructor.name] || [];
    this.buildingsByClass[building.constructor.name].push(building);
    this.buildings.push(building);
  };

  Block.prototype.removeBuilding = function (building) {
    var byClassIdx = this.buildingsByClass[building.constructor.name].indexOf(building);
    this.buildingsByClass[building.constructor.name].splice(byClassIdx, 1);
    var idx = this.buildings.indexOf(building);
    this.buildings.splice(idx, 1);
  };

  Block.prototype.getBuildingCountByClass = function (buildingClassName) {
    if (!buildingClassName) {
      throw new Error('Invalid buildingClassName given: ' + buildingClassName);
    }
    return this.buildingsByClass[buildingClassName] ? this.buildingsByClass[buildingClassName].length : 0;
  };

  Block.prototype.isMutable = function () {
    return this.kind.mainResourceLevel === 1 && !this.rich && !this.mutated;
  };

  Block.prototype.canBeRich = function () {
    return this.kind.improvable && this.kind.mainResourceLevel === 1;
  };

  Block.prototype.createMutatedKind = function (randGenerator) {
    var mutationProbabilities = Block.kindMutations[this.kind.toString()];
    var rand = randGenerator.getFloat();
    var i = -1;
    var probability;
    var NewKindClass;
    do {
      probability = Object.keys(mutationProbabilities[++i])[0];
      NewKindClass = mutationProbabilities[i][probability];
    } while (rand < parseFloat(probability));
    return new NewKindClass();
  };

  Block.prototype.kind = null;
  Block.prototype.level = 0;
  Block.prototype.x = 0;
  Block.prototype.y = 0;
  Block.prototype.region = null;
  Block.prototype.squarePos = 'none';
  Block.prototype._hasImprovedNeighborOfPlayers = null;
  Block.prototype._affordableForPlayers = null;
  Block.prototype.exhausted = false;
  Block.prototype.rich = false;
  Block.prototype.ownedBy = null;
  Block.prototype.neighborsByKind = null;
  Block.prototype.neighborsByPosition = null;
  Block.prototype.improvedNeighbors = null;
  Block.prototype.baseAiValue = 0;
  Block.prototype.tmpAiValue = 0;
  Block.prototype.aiValueForPlayer = null;
  Block.prototype.aiValuesForPlayerMissions = null;
  Block.prototype.buildingsByClass = null;
  Block.prototype.buildings = null;
  Block.prototype.square = null;
  Block.prototype.isMutating = false;
  Block.prototype.mutated = false;
  Block.prototype.disabled = false;

  return Block;
}]);