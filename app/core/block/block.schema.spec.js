'use strict';

describe('BlockSchema', function() {
  beforeEach(module('square'));

  var Block, BlockSchema, Region, HumanPlayer, Joi, Forest,
    Square, Market;

  beforeEach(inject(function (_Block_, _BlockSchema_, _Region_, _HumanPlayer_, _Joi_, _Forest_,
                              _Square_, _Market_) {
    Block = _Block_;
    BlockSchema = _BlockSchema_;
    Region = _Region_;
    HumanPlayer = _HumanPlayer_;
    Joi = _Joi_;
    Forest = _Forest_;

    Square = _Square_;
    Market = _Market_;
  }));

  describe('a new instance', function () {

    it('validates against the block schema', function (done) {
      var block = new Block(4, 0, new Region(1, -10), new Forest());
      Joi.validate(block, BlockSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

    it('of owned block validates against the block schema', function (done) {
      var block = new Block(4, 0, new Region(1, -10), new Forest());
      block.ownedBy = new HumanPlayer('mocked-player-id-999', {x: 2, y: 1});
      Joi.validate(block, BlockSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

    it('of owned with given kind and richness validates against the block schema', function (done) {
      var block = new Block(4, 0, new Region(1, -1), new Forest(), true);
      block.ownedBy = new HumanPlayer('mocked-player-id-999', {x: 2, y: 1});
      Joi.validate(block, BlockSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

    it('with 1 building validates against the block schema', function (done) {
      var block = new Block(4, 0, new Region(1, -10), new Forest());
      var square = new Square(-1, -1, 0, 2, [block], 2, new HumanPlayer('mocked-player-id-999', {x: 2, y: 1}));
      block.addBuilding(new Market(block, square));
      Joi.validate(block, BlockSchema, {presence: 'required', convert: false}, function (validation_error) {
        if (validation_error) {
          done.fail(validation_error);
        } else {
          done();
        }
      });
    });

  });

});