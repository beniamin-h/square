'use strict';

angular.module('square').factory('Region', ['Block',
    function (Block) {

  function Region(x, y) {
    this.blocks = [];
    this.x = x;
    this.y = y;
  }

  Region.prototype.populateWithBlocks = function (randGenerator, mapDifficulty) {
    var blockKindProbabilities = Block.calcKindProbabilities(mapDifficulty);
    var richProbability = Block.calcRichProbability(mapDifficulty);
    var advKindProbabilityFactor = Block.calcAdvancedKindProbabilityFactor(mapDifficulty);
    for (var y = 0; y < Region.blocksHeight; y++) {
      this.blocks[y] = [];
      for (var x = 0; x < Region.blocksWidth; x++) {
        var block = new Block(x, y, this);
        block.generateBlockKind(
          randGenerator, blockKindProbabilities, advKindProbabilityFactor);
        block.generateBlockRichness(
          randGenerator, richProbability);
        this.blocks[y][x] = block;
      }
    }
  };

  Region.prototype.getBlockByXY = function (x, y) {
    return this.blocks[y][x];
  };

  Region.prototype.x = 0;
  Region.prototype.y = 0;

  Region.blocksWidth = 5;
  Region.blocksHeight = 5;

  return Region;
}]);