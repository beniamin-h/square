'use strict';

angular.module('square').factory('RegionSchema', ['Joi', 'Region', 'BlockSchema',
    function (Joi, Region, BlockSchema) {

  return Joi.object().keys({
    x: Joi.number().integer(),
    y: Joi.number().integer(),
    blocks: Joi.array().items(
      Joi.array().items(
        BlockSchema
      )
    )
  }).type(Region);
}]);