'use strict';

describe('Region', function() {
  beforeEach(module('square'));

  var Region, Block, Rand;

  beforeEach(inject(function (_Region_, _Block_, _Rand_) {
    Region = _Region_;
    Block = _Block_;
    Rand = _Rand_;
  }));

  describe('populateWithBlocks', function () {

    var region;

    beforeEach(function () {
      region = new Region(2, 5);
    });

    it('populates region with two dimensions of blocks', function () {
      const randGen = new Rand('fake-ns', Rand.getUnpredictableInt31Unsigned())
        .getGenerator('populateWithBlocks');
      region.populateWithBlocks(randGen, 1.0);
      expect(region.blocks).toBeArrayOfSize(Region.blocksHeight);
      expect(region.blocks[0]).toBeArrayOfSize(Region.blocksWidth);
      expect(region.blocks[0][0] instanceof Block).toBeTrue();
    });

  });

  describe('getBlockByXY', function () {

    it('returns a proper block', function () {
      Region.blocksWidth = 10;
      Region.blocksHeight = 10;
      var region = new Region(7, 1);
      const randGen = new Rand('fake-ns', Rand.getUnpredictableInt31Unsigned())
        .getGenerator('populateWithBlocks');
      region.populateWithBlocks(randGen, 1.0);
      var block = region.getBlockByXY(4, 8);
      expect(block.x).toBe(4);
      expect(block.y).toBe(8);
    });

  });

});