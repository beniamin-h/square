'use strict';

describe('Region', function() {
  beforeEach(module('square'));

  let Region, Block;

  function spyOnConstructor($provide, constructorName) {
    $provide.decorator(constructorName, ['$delegate', function ($delegate) {
      const mock = jasmine.createSpy(constructorName + 'Constructor');
      for (const propertyName of Object.keys($delegate)) {
        mock[propertyName] = $delegate[propertyName];
      }
      for (const prototypePropertyName of Object.keys($delegate.prototype)) {
        mock.prototype[prototypePropertyName] =
          $delegate.prototype[prototypePropertyName];
      }
      return mock;
    }]);
  }

  beforeEach(function () {
    module(function ($provide) {
      spyOnConstructor($provide, 'Block');
    });
  });

  beforeEach(inject(function (_Region_, _Block_) {
    Region = _Region_;
    Block = _Block_;
  }));

  let region;
  let fakeRegionX;
  let fakeRegionY;

  beforeEach(function () {
    fakeRegionX = jasmine.createSpy('regionX');
    fakeRegionY = jasmine.createSpy('regionY');
    region = new Region(fakeRegionX, fakeRegionY);
  });

  describe('populateWithBlocks', function () {

    let fakeRandGen;
    let fakeMapDifficulty;
    let calcKindProbabilitiesResult;
    let calcRichProbabilityResult;
    let calcAdvancedKindProbabilityFactorResult;

    beforeEach(function () {
      fakeRandGen = jasmine.createSpy('randGen');
      fakeMapDifficulty = jasmine.createSpy('mapDifficulty');
      calcKindProbabilitiesResult = jasmine.createSpy('calcKindProbabilitiesResult');
      calcRichProbabilityResult = jasmine.createSpy('calcRichProbabilityResult');
      calcAdvancedKindProbabilityFactorResult =
        jasmine.createSpy('calcAdvancedKindProbabilityFactorResult');
      spyOn(Block, 'calcKindProbabilities')
        .and.returnValue(calcKindProbabilitiesResult);
      spyOn(Block, 'calcRichProbability')
        .and.returnValue(calcRichProbabilityResult);
      spyOn(Block, 'calcAdvancedKindProbabilityFactor')
        .and.returnValue(calcAdvancedKindProbabilityFactorResult);
      spyOn(Block.prototype, 'generateBlockKind');
      spyOn(Block.prototype, 'generateBlockRichness');
    });

    it('calls Block.calcKindProbabilities', function () {
      region.populateWithBlocks(fakeRandGen, fakeMapDifficulty);
      expect(Block.calcKindProbabilities).toHaveBeenCalledWith(fakeMapDifficulty);
    });

    it('calls Block.calcRichProbability', function () {
      region.populateWithBlocks(fakeRandGen, fakeMapDifficulty);
      expect(Block.calcRichProbability).toHaveBeenCalledWith(fakeMapDifficulty);
    });

    it('calls Block.calcAdvancedKindProbabilityFactor', function () {
      region.populateWithBlocks(fakeRandGen, fakeMapDifficulty);
      expect(Block.calcAdvancedKindProbabilityFactor)
        .toHaveBeenCalledWith(fakeMapDifficulty);
    });

    it('creates 55 new Blocks instances for ' +
       'Region.blocksHeight: 11 and Region.blocksWidth: 5', function () {
      Region.blocksHeight = 11;
      Region.blocksWidth = 5;
      region.populateWithBlocks(fakeRandGen, fakeMapDifficulty);
      expect(Block.calls.count()).toBe(55);
    });

    it('assigns new Blocks instances to `blocks` property respectively', function () {
      Region.blocksHeight = 7;
      Region.blocksWidth = 8;
      expect(region.blocks).toEqual([]);
      region.populateWithBlocks(fakeRandGen, fakeMapDifficulty);
      expect(region.blocks.length).toBe(7);
      expect(region.blocks[0].length).toBe(8);
      expect(region.blocks[6].length).toBe(8);
      expect(region.blocks[6][0] instanceof Block).toBeTrue();
      expect(region.blocks[6][7] instanceof Block).toBeTrue();
      expect(region.blocks[0][0] instanceof Block).toBeTrue();
      expect(region.blocks[4][2] instanceof Block).toBeTrue();
    });

    it('calls generateBlockKind on each new block', function () {
      region.populateWithBlocks(fakeRandGen, fakeMapDifficulty);
      expect(Block.prototype.generateBlockKind)
        .toHaveBeenCalledWith(fakeRandGen, calcKindProbabilitiesResult,
          calcAdvancedKindProbabilityFactorResult);
      expect(Block.prototype.generateBlockKind.calls.count())
        .toBe(Region.blocksHeight * Region.blocksWidth);
    });

    it('calls generateBlockRichness on each new block', function () {
      region.populateWithBlocks(fakeRandGen, fakeMapDifficulty);
      expect(Block.prototype.generateBlockRichness)
        .toHaveBeenCalledWith(fakeRandGen, calcRichProbabilityResult);
      expect(Block.prototype.generateBlockRichness.calls.count())
        .toBe(Region.blocksHeight * Region.blocksWidth);
    });

  });

  describe('getBlockByXY', function () {

    function createBlock(id) {
      return jasmine.createSpy(id);
    }

    it('returns a proper block', function () {
      const properBlock = createBlock('properBlock');
      region.blocks = [
        [createBlock('anotherBlock'), createBlock('anotherBlock')],
        [createBlock('anotherBlock')],
        [properBlock, createBlock('anotherBlock')],
      ];
      const result = region.getBlockByXY(0, 2);
      expect(result).toBe(properBlock);
    });

  });

});