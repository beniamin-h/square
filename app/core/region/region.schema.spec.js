'use strict';

describe('RegionSchema', function() {
  beforeEach(module('square'));

  let Region, RegionSchema, Joi, Rand, testingUtils;

  beforeEach(inject(function (
      _Region_, _RegionSchema_, _Joi_, _Rand_, _testingUtils_) {
    Region = _Region_;
    RegionSchema = _RegionSchema_;
    Joi = _Joi_;
    Rand = _Rand_;
    testingUtils = _testingUtils_;
  }));

  describe('a new instance', function () {

    it('validates against the region schema', function (done) {
      const region = new Region(4, 8);
      testingUtils.validateAgainstSchema(region, RegionSchema, done);
    });

  });

  describe('a new instance with blocks', function () {

    it('validates against the region schema', function (done) {
      const region = new Region(4, 8);
      const randGenerator = new Rand(
        'Map.blockKindAndRichness', Rand.getUnpredictableInt31Unsigned())
          .getGenerator('blockKindAndRichness');
      region.populateWithBlocks(randGenerator, 1.0);
      testingUtils.validateAgainstSchema(region, RegionSchema, done);
    });

  });

});