'use strict';

angular.module('square').factory('TickerSchema', ['Joi', 'Ticker',
    function (Joi, Ticker) {

  return Joi.object().keys({
    tickStamp: Joi.number().natural()
  }).type(Ticker);

}]);
