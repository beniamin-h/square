'use strict';

angular.module('square').factory('Ticker', function () {

  var Ticker = function () {
    this.tickStamp = 0;
  };

  Ticker.prototype.tickStamp = 0;

  Ticker.prototype.tick = function () {
    this.tickStamp++;
  };

  Ticker.prototype.nowTickStamp = function () {
    return this.tickStamp;
  };

  return Ticker;
});
