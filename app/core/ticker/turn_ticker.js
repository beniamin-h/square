'use strict';

angular.module('square').factory('turnTicker', ['Ticker',
    function (Ticker) {

  var that = this;

  return {
    start: function () {
      that.ticker = new Ticker();
    },
    load: function (ticker) {
      that.ticker = ticker;
    },
    tick: function () {
      that.ticker.tick();
    },
    getTicker: function () {
      return that.ticker;
    },
    now: function () {
      return that.ticker.nowTickStamp();
    },
    _instance: this
  };
}]);
