'use strict';

describe('Ticker', function() {
  beforeEach(module('square'));

  let Ticker;

  beforeEach(inject(function (_Ticker_) {
    Ticker = _Ticker_;
  }));

  let ticker;

  beforeEach(function () {
    ticker = new Ticker();
  });

  describe('constructor', function () {

    it('sets tickStamp to 0', function () {
      expect(ticker.tickStamp).toBe(0);
    });

  });

  describe('tick', function () {

    it('increments tickStamp', function () {
      ticker.tickStamp = 8;
      ticker.tick();
      expect(ticker.tickStamp).toBe(9);
    });

  });

  describe('nowTickStamp', function () {

    it('returns tickStamp', function () {
      ticker.tickStamp = jasmine.createSpy('tickStamp');
      const result = ticker.nowTickStamp();
      expect(result).toBe(ticker.tickStamp);
    });

  });

});