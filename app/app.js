'use strict';

// Declare app level module which depends on views, and components
angular.module('square', [
  'serialijse',
  'ngRoute'
])
.run(['$rootScope', '$document', '$timeout',
      function($rootScope, $document, $timeout) {

  $rootScope.activeControlPanel = null;

  $document.bind('keydown', function (key) {
    if (key.code === 'Escape') {
      $timeout(() => {
        $rootScope.activeControlPanel = null;
      });
    }
  });

}]);
