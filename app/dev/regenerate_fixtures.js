'use strict';

var beautify = require("json-beautify");
var fs = require('fs');
var async = require("async");

jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;

describe('TestFixturesRegenerator', function() {
  beforeEach(module('square'));

  var gameStateProvider, playerProvider, mapProvider, playerCycle,
    storageProvider, Rand, turnTicker;

  beforeEach(inject(function (
      _gameStateProvider_, _playerProvider_, _mapProvider_, _playerCycle_,
      _storageProvider_, _Rand_, _turnTicker_) {
    gameStateProvider = _gameStateProvider_;
    playerProvider = _playerProvider_;
    mapProvider = _mapProvider_;
    playerCycle = _playerCycle_;

    storageProvider = _storageProvider_;
    Rand = _Rand_;
    turnTicker = _turnTicker_;
  }));

  function regenerateFixture(inFilename, outFilename, additionalTasks, done) {

    async.waterfall([
      function loadOldFixture(callback) {
        console.log('loadOldFixture', inFilename, '(' + outFilename + ')');
        gameStateProvider.load(process.env.PWD + '/test_fixtures/' + inFilename + '.json', callback);
      },
      async.asyncify(function doAdditionalTasks() {
        console.log('doAdditionalTasks', outFilename);
        if (additionalTasks) {
          additionalTasks();
        }
      }),
      function saveNewFixtureAsTmp(result, callback) {
        console.log('saveNewFixtureAsTmp', outFilename);
        gameStateProvider.save(process.env.PWD + '/test_fixtures/' + outFilename + '.tmp.json', callback);
      },
      function readTmpFixture(callback) {
        console.log('readTmpFixture', outFilename);
        fs.readFile(process.env.PWD + '/test_fixtures/' + outFilename + '.tmp.json', callback);
      },
      function beautifyFixture(fixture, callback) {
        console.log('beautifyFixture', outFilename);
        var beautifulFixture = beautify(JSON.parse(fixture), null, 2, 15);
        callback(null, beautifulFixture);
      },
      function saveFixtureAsNew(fixture, callback) {
        console.log('saveFixtureAsNew', outFilename);
        fs.writeFile(process.env.PWD + '/test_fixtures/' + outFilename + '.new.json', fixture, callback);
      },
      function removeTmp(callback) {
        console.log('removeTmp', outFilename);
        fs.unlink(process.env.PWD + '/test_fixtures/' + outFilename + '.tmp.json', callback);
      }
    ], function (err) {
      if (err) {
        done.fail(err);
        return;
      }

      done();
    });

  }

  function addAdvancedResourcesToStorages() {
    var storages = storageProvider.getAllStorages();
    for (var playerId in storages) {
      storageProvider.advancedResources.forEach(function (resName) {
        storages[playerId].resources[resName] = 0;
      });
    }
  }

  function renameHorsesInStorages() {
    var storages = storageProvider.getAllStorages();
    for (var playerId in storages) {
      delete storages[playerId].resources.horse;
      storages[playerId].resources.horses = 0;
    }
  }

  function renameBoardsInStorages() {
    var storages = storageProvider.getAllStorages();
    for (var playerId in storages) {
      delete storages[playerId].resources.board;
      storages[playerId].resources.boards = 0;
    }
  }

  function updateAiImprovedBlockNeighborsByKind() {
    var players = playerProvider.getAllPlayers();
    for (var playerId in players) {
      if (players[playerId].ai) {
        players[playerId].ai.updateImprovedBlockNeighborsByKind();
      }
    }
  }

  function setBaseBlockMutationProbabilityToZero() {
    var map = mapProvider.getMap();
    map.baseBlockMutationProbability = 0;
  }

  function setMapDifficultyToOne() {
    var map = mapProvider.getMap();
    map.mapDifficulty = 1.0;
  }

  function setMapBlocksProp() {
    mapProvider.getMap().setMapBlocks(playerProvider.getAllPlayers());
  }

  function setResourceShortageHandlers() {
    var storages = storageProvider.getAllStorages();
    for (var playerId in storages) {
      storages[playerId].setResourceShortageHandler();
    }
  }

  function setPlayersDisclaimCooldownToZero() {
    for (const player of Object.values(playerProvider.getAllPlayers())) {
      player.disclaimCooldown = 0;
    }
  }

  function setPlayersNextDisclaimCooldown() {
    for (const player of Object.values(playerProvider.getAllPlayers())) {
      player.setNextDisclaimCooldown();
    }
  }

  function setRandomness() {
    mapProvider.getMap().setRandomness(
      Rand.getUnpredictableInt31Unsigned(), null);
  }

  function setPredictableRandomness() {
    const map = mapProvider.getMap();
    map.setRandomness(
      map.gameSeed, map.mapSeed);
  }

  function startTurnTicker() {
    turnTicker.start();
  }

  function reset() {
    mapProvider.getMap().resetNeighborsByKindForAllBlocks();
    mapProvider.getMap().resetAllBlocksAiValueForPlayer();
    mapProvider.getMap().initAllBlocksNeighborsAndTmpAiValues();
    mapProvider.getMap().initAllBlocksBaseAiValues();
    mapProvider.getMap().setPlayers(playerProvider.getAllPlayers());
    //addAdvancedResourcesToStorages();
    //renameHorsesInStorages();
    //updateAiImprovedBlockNeighborsByKind();
    //renameBoardsInStorages();
    //setBaseBlockMutationProbabilityToZero();
    //setMapDifficultyToOne();
    //setMapBlocksProp();
    //setResourceShortageHandlers();
    //setPlayersNextDisclaimCooldown();
    setPredictableRandomness();
    startTurnTicker();
  }

  if (Array.isArray(__karma__.config.args)) {
    if (__karma__.config.args.indexOf('--failures') > -1) {
      var dirPath = process.env.PWD + '/test_fixtures/test_failures/';
      fs.readdirSync(dirPath).forEach(function (filename) {
        if (filename !== '.' && filename !== '..' &&
            filename.indexOf('.new.json') === -1 &&
            filename.indexOf('.tmp.json') === -1) {
          var fixtureName = filename.replace('.json', '');
          it('regenerate test_failure `' + fixtureName + '`', function (done) {
            regenerateFixture('test_failures/' + fixtureName, 'test_failures/' + fixtureName, function () {
              reset();
            }, done);
          });
        }
      });
      return;
    }
  }

  it('regenerate simple_map_3x3_5x5', function (done) {
    regenerateFixture('simple_map_3x3_5x5', 'simple_map_3x3_5x5', function () {
      reset();
    }, done);
  });

  it('regenerate simple_map_3x3_5x5_with_24_improved_blocks', function (done) {
    regenerateFixture('simple_map_3x3_5x5.new', 'simple_map_3x3_5x5_with_24_improved_blocks', function () {
      for(var i = 0; i < 24; i++) {
        playerProvider.getFirstAI().ai.nextMove();
        turnTicker.tick();
      }
      expect(playerProvider.getFirstAI().defeated).toBeFalse();
      setPlayersDisclaimCooldownToZero();
    }, done);
  });

  it('regenerate simple_map_3x3_5x5_with_34_improved_blocks', function (done) {
    regenerateFixture('simple_map_3x3_5x5.new', 'simple_map_3x3_5x5_with_34_improved_blocks', function () {
      for(var i = 0; i < 34; i++) {
        playerProvider.getFirstAI().ai.nextMove();
        turnTicker.tick();
      }
      expect(playerProvider.getFirstAI().defeated).toBeFalse();
      setPlayersDisclaimCooldownToZero();
    }, done);
  });

  it('regenerate simple_map_5x5_10x10', function (done) {
    regenerateFixture('simple_map_5x5_10x10', 'simple_map_5x5_10x10', function () {
      reset();
    }, done);
  });

  it('regenerate simple_map_3x3_5x5_with_2_human_squares', function (done) {
    regenerateFixture('simple_map_3x3_5x5.new', 'simple_map_3x3_5x5_with_2_human_squares', function () {
      var blocks = [[0, 1, 0, 0],
                    [0, 0, 0, 4],
                    [0, 0, 1, 4],
                    [0, 1, 1, 0],
                    [0, 1, 0, 1],
                    [-1, 1, 4, 1],
                    [-1, 1, 4, 2],
                    [0, 1, 0, 2]];
      var map = mapProvider.getMap();
      blocks.forEach(function (blockCoords) {
        expect(map.tryToCaptureBlock(
          map.getBlock(...blockCoords),
          playerProvider.getHuman()
        )).toBeTrue();
        playerProvider.getHuman().eventEmitter.emit('humanActionDone');
        playerProvider.getFirstAI().nextMove(function () { });
        turnTicker.tick();
      });
      expect(playerProvider.getFirstAI().defeated).toBeFalse();
      expect(playerProvider.getHuman().defeated).toBeFalse();
    }, done);
  });

  it('regenerate plenty_advanced_resources_map_11x11_5x5', function (done) {
    regenerateFixture('plenty_advanced_resources_map_11x11_5x5',
      'plenty_advanced_resources_map_11x11_5x5', function () {
        reset();
    }, done);
  });

  it('regenerate easy_map_11x11_5x5', function (done) {
    regenerateFixture('easy_map_11x11_5x5', 'easy_map_11x11_5x5', function () {
        reset();
    }, done);
  });

  it('regenerate easy_map_11x11_5x5_with_multiple_squares', function (done) {
    regenerateFixture('easy_map_11x11_5x5.new',
      'easy_map_11x11_5x5_with_multiple_squares', function () {
        setPlayersDisclaimCooldownToZero();
        var ai = playerProvider.getFirstAI();
        ai.startingRegionCoords = {x: 0, y: 1};
        var humanPlayer = playerProvider.getHuman();
        var captureBlock = block =>
          expect(mapProvider.getMap().tryToCaptureBlock(block, humanPlayer)).toBeTrue();
        var storage = storageProvider.getStorage(humanPlayer);
        storage.put({ metal: 1000, stone: 1000, wood: 1000, food: 1000 });
        mapProvider.getBlocks(0, -1, 2, 0, 8, 8).forEach(captureBlock);
        mapProvider.getBlocks(0, -2, 0, 3, 10, 2).reverse().forEach(captureBlock);
        mapProvider.getBlocks(2, -2, 0, 3, 2, 2).forEach(captureBlock);
        mapProvider.getBlocks(0, -1, 0, 0, 2, 10).forEach(captureBlock);
        mapProvider.getBlocks(0, 0, 2, 3, 10, 2).forEach(captureBlock);
        mapProvider.getBlocks(2, -1, 0, 0, 4, 8).forEach(captureBlock);
        expect(mapProvider.getMap().improvedBlocksCounter[humanPlayer.playerId])
          .toBe(160);
        storage.take({ metal: 1000, stone: 1000, wood: 1000, food: 1000 }, true);
    }, done);
  });

});