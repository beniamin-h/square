'use strict';

angular.module('square').controller('DevShortcuts', [
    '$document', function ($document) {

  $document.bind('keydown', function (key) {
    if (key.code === 'F5') {
      location.reload();
    }
  });

}]);