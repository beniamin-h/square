'use strict';

angular.module('square')

.controller('InitCtrl', ['initProvider', function(initProvider) {

  initProvider.initGenericProviders();

}]);
