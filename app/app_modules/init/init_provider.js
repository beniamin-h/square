'use strict';

var EventEmitter = require('events');

angular.module('square').factory('initProvider', ['$interval', 'fs',
      'mapProvider', 'gameStateProvider',
      'playerProvider', 'storageProvider', 'playerCycle',
      'squareProvider', 'turnTicker', 'Rand',
    function($interval, fs,
             mapProvider, gameStateProvider,
             playerProvider, storageProvider, playerCycle,
             squareProvider, turnTicker, Rand) {

  var that = this;
  that.eventEmitter = new EventEmitter();
  that.eventEmitter.setMaxListeners(1 << 20);  // 2^20

  function declareGlobalDebugHelpers() {
    var moveCounter = 0;
    window.mapProvider = mapProvider;
    window.storageProvider = storageProvider;
    window.playerProvider = playerProvider;

    window.blockElementToObj = function (elem) {
      return mapProvider.getBlock(
        elem.getAttribute('data-rx'),
        elem.getAttribute('data-ry'),
        elem.getAttribute('data-x'),
        elem.getAttribute('data-y'));
    };

    window.getBlockCoordsStr = function (block) {
      return block.region.x + ', ' + block.region.y + ', ' +
        block.x + ', ' + block.y;
    };

    window.autoAI = function () {
      playerProvider.getHuman().moveDone();
    };

    window.startAutoAI = function (moveCounterLimit) {
      moveCounterLimit = moveCounterLimit || 35;
      moveCounterLimit += moveCounter;

      var callback = function (human, firstRun) {
        if (moveCounter < moveCounterLimit) {
          moveCounter++;
          if (!firstRun) {
            console.timeEnd('autoAI');
          }
          human.moveDone();
          console.time('autoAI');
        } else {
          console.timeEnd('autoAI');
          playerProvider.getHuman().eventEmitter.removeListener(
            'humanTurnBegin', callback);
          console.log('Done', moveCounterLimit);
        }
      };

      playerProvider.getHuman().eventEmitter.on('humanTurnBegin', callback);
      callback(playerProvider.getHuman(), true);
    };

    window.saveMap = function (filename) {
      filename = filename === undefined ? 'simple_map_3x3_5x5a' : filename;
      console.time('Saved');
      gameStateProvider.save('test_fixtures/' + filename + '.json', function (err) {
        if (err) {
          console.error(err);
        } else {
          console.timeEnd('Saved');
        }
      });
    };

    window.readFixture = function (filename, into) {
      filename = filename === undefined ? 'simple_map_3x3_5x5' : filename;
      console.time('Saved');
      fs.readFile('test_fixtures/' + filename + '.json', 'utf8', function (err, data) {
        if (err) {
          console.error(err);
        } else {
          into.data = JSON.parse(data);
          console.log('Loaded');
        }
      });
    };
  }

  function initNewGame({
      numberOfPlayers=18,  //current max: 18
      mapDifficulty=0.5,
      mapSeed=null,
      gameSeed=Rand.getUnpredictableInt31Unsigned(),
      mapSize=11
    } = {}) {

    turnTicker.start();
    playerProvider.addHuman();
    for (let i = numberOfPlayers - 1; i > 0; --i) {
      playerProvider.addAI();
    }
    storageProvider.init(playerProvider.getAllPlayers());
    //MapId: mapSeed + '-' + mapDifficulty + '-' + mapSize
    mapProvider.initMap(
      gameSeed, mapSeed, playerProvider.getAllPlayers(), mapDifficulty, mapSize);
    squareProvider.initSquareProvider(playerProvider.getAllPlayers());
    mapProvider.setSquaresMap(squareProvider.getSquaresMap());
  }

  function startNewGame() {
    console.time('New game');
    initNewGame({ gameSeed: 9 });
    that.eventEmitter.emit('gameInited');
    playerCycle.start();
    console.timeEnd('New game');
  }

  function loadExistingGame(filename, onLoaded) {
    console.time('Loaded');
    gameStateProvider.load(filename, function (err) {
      if (err) {
        console.error(err.stack);
        return;
      }
      that.eventEmitter.emit('gameLoaded');
      console.timeEnd('Loaded');
      playerCycle.start();
      onLoaded();
      //mapProvider.getMap().setPlayers(playerProvider.getAllPlayers());
      //mapProvider.getMap().resetNeighborsByKindForAllBlocks();
      //mapProvider.getMap().initAllBlocksNeighborsAndTmpAiValues();
      //mapProvider.getMap().setBlocksAiValuesForAllAiPlayers(playerProvider.getAllPlayers());
    });
  }

  return {
    eventEmitter: that.eventEmitter,
    initNewGame: initNewGame,
    initGenericProviders: function () {

      declareGlobalDebugHelpers();

      var newGame = true;

      if (newGame) {
        startNewGame();
      } else {
        loadExistingGame('test_fixtures/easy_map_11x11_5x5_with_multiple_squares.json',
          function () {
            //const map = mapProvider.getMap();
            //map.baseBlockMutationProbability = 0.5;
            //map.maxBlockMutationProbability = 10.5;
          });
      }
    },
    _instance: this
  };
}]);
