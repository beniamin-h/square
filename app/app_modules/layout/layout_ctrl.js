'use strict';

angular.module('square')

.controller('LayoutCtrl', ['$scope', function($scope) {

  $scope.storagePanelHeight = 145;
  $scope.controlPanelHeight = 600;
  $scope.controlPanelTop = 183;

  $scope.$on('resourcePanelHeightIncreased', function () {
    $scope.storagePanelHeight += 28;
    $scope.controlPanelHeight -= 28;
    $scope.controlPanelTop += 28;
  });

}]);
