'use strict';

describe('MapCtrl', function() {
  beforeEach(module('square'));

  var $controller;
  var mapProvider, $rootScope, $timeout, playerProvider,
    initProvider, mapDirections, testingUtils;

  beforeEach(inject(function (
      _$controller_, _mapProvider_, _$rootScope_, _$timeout_, _playerProvider_,
      _initProvider_, _mapDirections_, _testingUtils_) {
    $controller = _$controller_;
    mapProvider = _mapProvider_;
    $rootScope = _$rootScope_;
    $timeout = _$timeout_;
    playerProvider = _playerProvider_;

    initProvider = _initProvider_;
    mapDirections = _mapDirections_;
    testingUtils = _testingUtils_;
  }));

  var $scope;
  var mapCtrl;
  var fakeBlock;
  var checkScrollingBoundariesSpy;
  var tryToCaptureBlockSpy;
  var tryToDisclaimBlockSpy;
  var isFirstImprovementSpy;
  var fakeHuman;
  var aiPlayer1;
  var aiPlayer2;
  var fakeMapEventEmitter;

  function createFakePlayer() {
    return {
      eventEmitter: jasmine.createSpyObj('eventEmitter', ['on', 'emit']),
      moveDone: jasmine.createSpy('moveDone')
    };
  }

  beforeEach(function () {
    $scope = {};
    mapCtrl = $controller('MapCtrl', { $scope: $scope });
    spyOn($rootScope, '$on');
    spyOn($rootScope, '$emit');
    spyOn($rootScope, 'activeControlPanel');
    spyOn(mapProvider, 'getVisibleSquares');
    spyOn(mapProvider, 'updateVisibleRegions');
    fakeBlock = jasmine.createSpy('block');
    checkScrollingBoundariesSpy = jasmine.createSpy('checkScrollingBoundaries');
    tryToCaptureBlockSpy = jasmine.createSpy('tryToCaptureBlock');
    tryToDisclaimBlockSpy = jasmine.createSpy('tryToDisclaimBlock');
    isFirstImprovementSpy = jasmine.createSpy('isFirstImprovement');
    fakeMapEventEmitter = jasmine.createSpyObj('mapEventEmitter', ['on']);
    spyOn(mapProvider, 'getMap').and.returnValue({
      checkScrollingBoundaries: checkScrollingBoundariesSpy,
      tryToCaptureBlock: tryToCaptureBlockSpy,
      tryToDisclaimBlock: tryToDisclaimBlockSpy,
      isFirstImprovement: isFirstImprovementSpy,
      eventEmitter: fakeMapEventEmitter
    });
    spyOn(mapProvider, 'getRegionWidth');
    spyOn(mapProvider, 'getRegionHeight');
    spyOn(mapProvider, 'getBlockWidth');
    spyOn(mapProvider, 'getBlockHeight');
    fakeHuman = createFakePlayer();
    aiPlayer1 = createFakePlayer();
    aiPlayer2 = createFakePlayer();
    spyOn(playerProvider, 'getAllPlayers').and.returnValue({
      'fake-human-id': fakeHuman,
      'fake-ai-player-1': aiPlayer1,
      'fake-ai-player-2': aiPlayer2,
    });
    spyOn(playerProvider, 'getHuman').and.returnValue(fakeHuman);
    initProvider.eventEmitter = jasmine.createSpyObj('initEventEmitter', ['on']);
    spyOn(mapCtrl, 'leftTopCornerRegionX');
    spyOn(mapCtrl, 'leftTopCornerRegionY');
    spyOn(mapCtrl, 'updateVisibleSquares');
    spyOn(mapCtrl, 'updateVisibleRegions');
    spyOn(mapCtrl, 'updateLeftTopCornerRegionCoords');
    spyOn(mapCtrl, 'captureBlock');
    spyOn(mapCtrl, 'disclaimBlock');
    Object.keys(mapCtrl.scrollFuncs).forEach(function (direction) {
      spyOn(mapCtrl.scrollFuncs, direction);
    });
    spyOn(mapCtrl, 'init');
    spyOn($scope, 'scroll');
    spyOn($scope, 'stopScrolling');
    $scope.regionWidth = jasmine.createSpy('regionWidth');
    $scope.regionHeight = jasmine.createSpy('regionHeight');
    $scope.currentPlayer = jasmine.createSpy('currentPlayer');
    $scope.blockWidth = jasmine.createSpy('blockWidth');
    $scope.blockHeight = jasmine.createSpy('blockHeight');
    $scope.readOnly = false;
    $scope.selectionMode = jasmine.createSpy('selectionMode');
  });

  describe('init', function () {

    beforeEach(function () {
      mapCtrl.init.and.callThrough();
    });

    it('calls getRegionWidth and sets its result into $scope.regionWidth', function () {
      var getRegionWidthResult = jasmine.createSpy('getRegionWidth');
      mapProvider.getRegionWidth.and.returnValue(getRegionWidthResult);
      mapCtrl.init();
      expect(mapProvider.getRegionWidth).toHaveBeenCalledWith();
      expect($scope.regionWidth).toBe(getRegionWidthResult);
    });

    it('calls getRegionHeight and sets its result into $scope.regionHeight', function () {
      var getRegionHeightResult = jasmine.createSpy('getRegionHeight');
      mapProvider.getRegionHeight.and.returnValue(getRegionHeightResult);
      mapCtrl.init();
      expect(mapProvider.getRegionHeight).toHaveBeenCalledWith();
      expect($scope.regionHeight).toBe(getRegionHeightResult);
    });

    it('calls getHuman and sets its result into $scope.currentPlayer', function () {
      mapCtrl.init();
      expect(playerProvider.getHuman).toHaveBeenCalledWith();
      expect($scope.currentPlayer).toBe(fakeHuman);
    });

    it('calls getBlockWidth and sets its result into $scope.blockWidth', function () {
      var getBlockWidthResult = jasmine.createSpy('getBlockWidth');
      mapProvider.getBlockWidth.and.returnValue(getBlockWidthResult);
      mapCtrl.init();
      expect(mapProvider.getBlockWidth).toHaveBeenCalledWith();
      expect($scope.blockWidth).toBe(getBlockWidthResult);
    });

    it('calls getBlockHeight and sets its result into $scope.blockHeight', function () {
      var getBlockHeightResult = jasmine.createSpy('getBlockHeight');
      mapProvider.getBlockHeight.and.returnValue(getBlockHeightResult);
      mapCtrl.init();
      expect(mapProvider.getBlockHeight).toHaveBeenCalledWith();
      expect($scope.blockHeight).toBe(getBlockHeightResult);
    });

    it('calls isFirstImprovement on map', function () {
      mapCtrl.init();
      expect(isFirstImprovementSpy).toHaveBeenCalledWith(fakeHuman);
    });

    it('sets $scope.anyBlockImproved to false if isFirstImprovement returns true', function () {
      isFirstImprovementSpy.and.returnValue(true);
      mapCtrl.init();
      expect($scope.anyBlockImproved).toBeFalse();
    });

    it('sets $scope.anyBlockImproved to true if isFirstImprovement returns false', function () {
      isFirstImprovementSpy.and.returnValue(false);
      mapCtrl.init();
      expect($scope.anyBlockImproved).toBeTrue();
    });

    it('calls updateVisibleRegions', function () {
      mapCtrl.init();
      expect(mapCtrl.updateVisibleRegions).toHaveBeenCalledWith();
    });

    it('calls updateVisibleSquares', function () {
      mapCtrl.init();
      expect(mapCtrl.updateVisibleSquares).toHaveBeenCalledWith();
    });

    it('adds event listener on playerTurnBegin for each player', function () {
      mapCtrl.init();
      expect(fakeHuman.eventEmitter.on).toHaveBeenCalledWith(
        'playerTurnBegin', jasmine.any(Function));
      expect(aiPlayer1.eventEmitter.on).toHaveBeenCalledWith(
        'playerTurnBegin', jasmine.any(Function));
      expect(aiPlayer2.eventEmitter.on).toHaveBeenCalledWith(
        'playerTurnBegin', jasmine.any(Function));
    });

    it('calls updateVisibleSquares on playerTurnBegin ' +
       'for each player after timeout', function () {
      const humanCallbacks = testingUtils.getEventListeners(
        fakeHuman.eventEmitter, 'playerTurnBegin');
      const aiPlayer1Callbacks = testingUtils.getEventListeners(
        aiPlayer1.eventEmitter, 'playerTurnBegin');
      mapCtrl.init();
      mapCtrl.updateVisibleSquares.calls.reset();
      humanCallbacks[0]();
      expect(mapCtrl.updateVisibleSquares).not.toHaveBeenCalled();
      $timeout.flush();
      expect(mapCtrl.updateVisibleSquares).toHaveBeenCalledWith();
      aiPlayer1Callbacks[0]();
      $timeout.flush();
      expect(mapCtrl.updateVisibleSquares).toHaveBeenCalledWith();
      expect(mapCtrl.updateVisibleSquares.calls.count()).toBe(2);
    });

    it('sets $scope.readOnly to false on humanTurnBegin event', function () {
      const humanCallbacks = testingUtils.getEventListeners(
        fakeHuman.eventEmitter, 'humanTurnBegin');
      spyOn($scope, 'readOnly');
      expect($scope.readOnly).not.toBeFalse();
      mapCtrl.init();
      humanCallbacks[0]();
      expect($scope.readOnly).toBeFalse();
    });

    it('adds event listener on humanActionDone', function () {
      mapCtrl.init();
      expect(fakeHuman.eventEmitter.on)
        .toHaveBeenCalledWith('humanActionDone', jasmine.any(Function));
    });

    it('sets $scope.readOnly to true on humanActionDone event', function () {
      const humanCallbacks = testingUtils.getEventListeners(
        fakeHuman.eventEmitter, 'humanActionDone');
      expect($scope.readOnly).not.toBeTrue();
      mapCtrl.init();
      humanCallbacks[0]();
      expect($scope.readOnly).toBeTrue();
    });

    it('calls updateVisibleSquares after timeout on human player ' +
       'on humanActionDone event', function () {
      const humanCallbacks = testingUtils.getEventListeners(
        fakeHuman.eventEmitter, 'humanActionDone');
      mapCtrl.init();
      mapCtrl.updateVisibleSquares.calls.reset();
      humanCallbacks[0]();
      expect(mapCtrl.updateVisibleSquares).not.toHaveBeenCalled();
      $timeout.flush();
      expect(mapCtrl.updateVisibleSquares).toHaveBeenCalledWith();
    });

    it('calls moveDone after timeout on human player ' +
       'on humanActionDone event', function () {
      const humanCallbacks = testingUtils.getEventListeners(
        fakeHuman.eventEmitter, 'humanActionDone');
      mapCtrl.init();
      humanCallbacks[0]();
      expect(fakeHuman.moveDone).not.toHaveBeenCalled();
      $timeout.flush();
      expect(fakeHuman.moveDone).toHaveBeenCalledWith();
    });

    it('adds event listener on blockMutates', function () {
      mapCtrl.init();
      expect(fakeMapEventEmitter.on).toHaveBeenCalledWith('blockMutates', jasmine.any(Function));
    });

    it('sets block isMutating to true on blockMutates', function () {
      var fakeBlock = jasmine.createSpyObj('block', ['isMutating']);
      fakeMapEventEmitter.on.and.callFake(function (eventName, callback) {
        if (eventName === 'blockMutates') {
          callback(fakeBlock);
        }
      });
      expect(fakeBlock.isMutating).not.toBeTrue();
      mapCtrl.init();
      expect(fakeBlock.isMutating).toBeTrue();
    });

    it('sets block isMutating to false after timeout on blockMutates', function () {
      var fakeBlock = jasmine.createSpyObj('block', ['isMutating']);
      fakeMapEventEmitter.on.and.callFake(function (eventName, callback) {
        if (eventName === 'blockMutates') {
          callback(fakeBlock);
        }
      });
      mapCtrl.init();
      expect(fakeBlock.isMutating).not.toBeFalse();
      $timeout.flush();
      expect(fakeBlock.isMutating).toBeFalse();
    });

  });

  describe('addListeners', function () {

    it('adds event listener on gameLoaded on initProvider', function () {
      mapCtrl.addListeners();
      expect(initProvider.eventEmitter.on).toHaveBeenCalledWith('gameInited', jasmine.any(Function));
    });

    it('adds event listener on gameInited on initProvider', function () {
      mapCtrl.addListeners();
      expect(initProvider.eventEmitter.on).toHaveBeenCalledWith('gameLoaded', jasmine.any(Function));
    });

    it('adds event listener on mapSelectionModeChanged on $rootScope', function () {
      mapCtrl.addListeners();
      expect($rootScope.$on).toHaveBeenCalledWith(
        'mapSelectionModeChanged', jasmine.any(Function));
    });

    it('calls init on gameInited', function () {
      initProvider.eventEmitter.on.and.callFake(function (eventName, callback) {
        if (eventName === 'gameInited') {
          callback();
        }
      });
      mapCtrl.addListeners();
      expect(mapCtrl.init).toHaveBeenCalledWith();
    });

    it('calls init on gameLoaded', function () {
      initProvider.eventEmitter.on.and.callFake(function (eventName, callback) {
        if (eventName === 'gameLoaded') {
          callback();
        }
      });
      mapCtrl.addListeners();
      expect(mapCtrl.init).toHaveBeenCalledWith();
    });

    it('sets $scope.selectionMode on mapSelectionModeChanged', function () {
      var fakeSelectionMode = jasmine.createSpy('fake selectionMode');
      $rootScope.$on.and.callFake(function (eventName, callback) {
        if (eventName === 'mapSelectionModeChanged') {
          callback({}, fakeSelectionMode);
        }
      });
      mapCtrl.addListeners();
      expect($scope.selectionMode).toBe(fakeSelectionMode);
    });

  });

  describe('updateVisibleSquares', function () {

    var getVisibleSquaresResult;

    beforeEach(function () {
      $scope.squares = jasmine.createSpy('$scope.squares');
      getVisibleSquaresResult = jasmine.createSpy('getVisibleSquares');
      mapProvider.getVisibleSquares.and.returnValue(getVisibleSquaresResult);
      mapCtrl.updateVisibleSquares.and.callThrough();
    });

    it('calls getVisibleSquares and sets its result as $scope.squares', function () {
      mapCtrl.updateVisibleSquares();
      expect(mapProvider.getVisibleSquares).toHaveBeenCalledWith();
      expect($scope.squares).toBe(getVisibleSquaresResult);
    });

  });

  describe('updateVisibleRegions', function () {

    beforeEach(function () {
      $scope.regions = jasmine.createSpy('$scope.regions');
      mapCtrl.updateVisibleRegions.and.callThrough();
    });

    it('calls updateVisibleRegions on mapProvider', function () {
      mapCtrl.updateVisibleRegions();
      expect(mapProvider.updateVisibleRegions).toHaveBeenCalledWith(
        $scope.regions, mapCtrl.leftTopCornerRegionX, mapCtrl.leftTopCornerRegionY
      );
    });

  });

  describe('updateLeftTopCornerRegionCoords', function () {

    beforeEach(function () {
      $scope.mapScroll = {
        left: 0,
        top: 0
      };
      $scope.regionWidth = 1;
      $scope.regionHeight = 1;
      mapCtrl.updateLeftTopCornerRegionCoords.and.callThrough();
    });

    describe('sets leftTopCornerRegionX', function () {

      it('to 5 if $scope.mapScroll.left = -500 and $scope.regionWidth = 100', function () {
        $scope.mapScroll.left = -500;
        $scope.regionWidth = 100;
        expect(mapCtrl.leftTopCornerRegionX).not.toBe(5);
        mapCtrl.updateLeftTopCornerRegionCoords();
        expect(mapCtrl.leftTopCornerRegionX).toBe(5);
      });

      it('to 0 if $scope.mapScroll.left = 0 and $scope.regionWidth = 100', function () {
        $scope.mapScroll.left = 0;
        $scope.regionWidth = 100;
        expect(mapCtrl.leftTopCornerRegionX).not.toBe(0);
        mapCtrl.updateLeftTopCornerRegionCoords();
        expect(mapCtrl.leftTopCornerRegionX).toBe(0);
      });

      it('to -1 if $scope.mapScroll.left = 1 and $scope.regionWidth = 100', function () {
        $scope.mapScroll.left = 1;
        $scope.regionWidth = 100;
        expect(mapCtrl.leftTopCornerRegionX).not.toBe(-1);
        mapCtrl.updateLeftTopCornerRegionCoords();
        expect(mapCtrl.leftTopCornerRegionX).toBe(-1);
      });

      it('to -1 if $scope.mapScroll.left = 100 and $scope.regionWidth = 100', function () {
        $scope.mapScroll.left = 100;
        $scope.regionWidth = 100;
        expect(mapCtrl.leftTopCornerRegionX).not.toBe(-1);
        mapCtrl.updateLeftTopCornerRegionCoords();
        expect(mapCtrl.leftTopCornerRegionX).toBe(-1);
      });

      it('to -2 if $scope.mapScroll.left = 101 and $scope.regionWidth = 100', function () {
        $scope.mapScroll.left = 101;
        $scope.regionWidth = 100;
        expect(mapCtrl.leftTopCornerRegionX).not.toBe(-2);
        mapCtrl.updateLeftTopCornerRegionCoords();
        expect(mapCtrl.leftTopCornerRegionX).toBe(-2);
      });

      it('to 0 if $scope.mapScroll.left = -1 and $scope.regionWidth = 100', function () {
        $scope.mapScroll.left = -1;
        $scope.regionWidth = 100;
        expect(mapCtrl.leftTopCornerRegionX).not.toBe(0);
        mapCtrl.updateLeftTopCornerRegionCoords();
        expect(mapCtrl.leftTopCornerRegionX).toBe(0);
      });

      it('to 0 if $scope.mapScroll.left = -99 and $scope.regionWidth = 100', function () {
        $scope.mapScroll.left = -99;
        $scope.regionWidth = 100;
        expect(mapCtrl.leftTopCornerRegionX).not.toBe(0);
        mapCtrl.updateLeftTopCornerRegionCoords();
        expect(mapCtrl.leftTopCornerRegionX).toBe(0);
      });

      it('to 1 if $scope.mapScroll.left = -100 and $scope.regionWidth = 100', function () {
        $scope.mapScroll.left = -100;
        $scope.regionWidth = 100;
        expect(mapCtrl.leftTopCornerRegionX).not.toBe(1);
        mapCtrl.updateLeftTopCornerRegionCoords();
        expect(mapCtrl.leftTopCornerRegionX).toBe(1);
      });

      it('y to 5 if $scope.mapScroll.top = -500 and $scope.regionHeight = 100', function () {
        $scope.mapScroll.top = -500;
        $scope.regionHeight = 100;
        expect(mapCtrl.leftTopCornerRegionY).not.toBe(5);
        mapCtrl.updateLeftTopCornerRegionCoords();
        expect(mapCtrl.leftTopCornerRegionY).toBe(5);
      });

    });

    describe('sets leftTopCornerRegionY', function () {

      it('to 0 if $scope.mapScroll.top = 0 and $scope.regionHeight = 100', function () {
        $scope.mapScroll.top = 0;
        $scope.regionHeight = 100;
        expect(mapCtrl.leftTopCornerRegionY).not.toBe(0);
        mapCtrl.updateLeftTopCornerRegionCoords();
        expect(mapCtrl.leftTopCornerRegionY).toBe(0);
      });

      it('to -1 if $scope.mapScroll.top = 1 and $scope.regionHeight = 100', function () {
        $scope.mapScroll.top = 1;
        $scope.regionHeight = 100;
        expect(mapCtrl.leftTopCornerRegionY).not.toBe(-1);
        mapCtrl.updateLeftTopCornerRegionCoords();
        expect(mapCtrl.leftTopCornerRegionY).toBe(-1);
      });

      it('to -1 if $scope.mapScroll.top = 100 and $scope.regionHeight = 100', function () {
        $scope.mapScroll.top = 100;
        $scope.regionHeight = 100;
        expect(mapCtrl.leftTopCornerRegionY).not.toBe(-1);
        mapCtrl.updateLeftTopCornerRegionCoords();
        expect(mapCtrl.leftTopCornerRegionY).toBe(-1);
      });

      it('to 0 if $scope.mapScroll.top = -1 and $scope.regionHeight = 100', function () {
        $scope.mapScroll.top = -1;
        $scope.regionHeight = 100;
        expect(mapCtrl.leftTopCornerRegionY).not.toBe(0);
        mapCtrl.updateLeftTopCornerRegionCoords();
        expect(mapCtrl.leftTopCornerRegionY).toBe(0);
      });

      it('to 1 if $scope.mapScroll.top = -100 and $scope.regionHeight = 100', function () {
        $scope.mapScroll.top = -100;
        $scope.regionHeight = 100;
        expect(mapCtrl.leftTopCornerRegionY).not.toBe(1);
        mapCtrl.updateLeftTopCornerRegionCoords();
        expect(mapCtrl.leftTopCornerRegionY).toBe(1);
      });

    });

    describe('calls updateVisibleRegions', function () {

      it('when leftTopCornerRegionX value changes', function () {
        mapCtrl.leftTopCornerRegionX = 1000;
        mapCtrl.leftTopCornerRegionY = 0;
        mapCtrl.updateLeftTopCornerRegionCoords();
        expect(mapCtrl.leftTopCornerRegionX).not.toBe(1000);
        expect(mapCtrl.leftTopCornerRegionY ).toBe(0);
        expect(mapCtrl.updateVisibleRegions).toHaveBeenCalledWith();
      });

      it('when leftTopCornerRegionY value changes', function () {
        mapCtrl.leftTopCornerRegionX = 0;
        mapCtrl.leftTopCornerRegionY = 1000;
        mapCtrl.updateLeftTopCornerRegionCoords();
        expect(mapCtrl.leftTopCornerRegionX).toBe(0);
        expect(mapCtrl.leftTopCornerRegionY).not.toBe(1000);
        expect(mapCtrl.updateVisibleRegions).toHaveBeenCalledWith();
      });

    });

    it('does not call updateVisibleRegions ' +
       'if leftTopCornerRegionX and leftTopCornerRegionY has not been changed',
      function () {
        mapCtrl.leftTopCornerRegionX = 0;
        mapCtrl.leftTopCornerRegionY = 0;
        mapCtrl.updateLeftTopCornerRegionCoords();
        expect(mapCtrl.leftTopCornerRegionX).toBe(0);
        expect(mapCtrl.leftTopCornerRegionY).toBe(0);
        expect(mapCtrl.updateVisibleRegions).not.toHaveBeenCalled();
      });

  });

  describe('scrollFuncs', function () {

    var mapScrollLeftSpy;
    var mapScrollTopSpy;

    beforeEach(function () {
      Object.keys(mapCtrl.scrollFuncs).forEach(function (direction) {
        mapCtrl.scrollFuncs[direction].and.callThrough();
      });
      checkScrollingBoundariesSpy.and.returnValue(true);
      $scope.blockWidth = 50;
      $scope.blockHeight = 50;
      mapScrollLeftSpy = jasmine.createSpy('mapScrollLeft');
      mapScrollTopSpy = jasmine.createSpy('mapScrollTop');
      $scope.mapScroll = {
        left: mapScrollLeftSpy,
        top: mapScrollTopSpy
      };
    });

    it('is an object with 4 non-diagonal directions', function () {
      mapDirections.nonDiagonal.forEach(function (direction) {
        expect(mapCtrl.scrollFuncs[direction]).toEqual(jasmine.any(Function));
      });
    });

    describe('each func', function () {

      ['up', 'right', 'down', 'left'].forEach(function (direction) {

        it('calls getMap on mapProvider', function () {
          mapCtrl.scrollFuncs[direction]();
          expect(mapProvider.getMap).toHaveBeenCalledWith();
        });

        it('calls checkScrollingBoundaries on map', function () {
          mapCtrl.scrollFuncs[direction]();
          expect(checkScrollingBoundariesSpy).toHaveBeenCalledWith(
            mapScrollLeftSpy, mapScrollTopSpy, direction
          );
        });

        it('calls updateLeftTopCornerRegionCoords', function () {
          mapCtrl.scrollFuncs[direction]();
          expect(mapCtrl.updateLeftTopCornerRegionCoords).toHaveBeenCalledWith();
        });

        it('does not call updateLeftTopCornerRegionCoords ' +
           'if checkScrollingBoundaries returns false', function () {
          checkScrollingBoundariesSpy.and.returnValue(false);
          mapCtrl.scrollFuncs[direction]();
          expect(mapCtrl.updateLeftTopCornerRegionCoords).not.toHaveBeenCalled();
        });

        it('calls stopScrolling if checkScrollingBoundaries returns false', function () {
          checkScrollingBoundariesSpy.and.returnValue(false);
          mapCtrl.scrollFuncs[direction]();
          expect($scope.stopScrolling).toHaveBeenCalledWith();
        });

        it('does not call stopScrolling ' +
           'if checkScrollingBoundaries returns true', function () {
          mapCtrl.scrollFuncs[direction]();
          expect($scope.stopScrolling).not.toHaveBeenCalled();
        });

        it('returns true if checkScrollingBoundaries returns true', function () {
          var result = mapCtrl.scrollFuncs[direction]();
          expect(result).toBeTrue();
        });

        it('returns false if checkScrollingBoundaries returns false', function () {
          checkScrollingBoundariesSpy.and.returnValue(false);
          var result = mapCtrl.scrollFuncs[direction]();
          expect(result).toBeFalse();
        });

      });

    });

    describe('up', function () {

      it('changes $scope.mapScroll.top by +50 when blockHeight is 50', function () {
        $scope.mapScroll.left = 100;
        $scope.mapScroll.top = 100;
        mapCtrl.scrollFuncs.up();
        expect($scope.mapScroll.top).toBe(150);
        expect($scope.mapScroll.left).toBe(100);
      });

      it('changes $scope.mapScroll.top by +99 when blockHeight is 99', function () {
        $scope.mapScroll.left = 0;
        $scope.mapScroll.top = 0;
        $scope.blockHeight = 99;
        mapCtrl.scrollFuncs.up();
        expect($scope.mapScroll.top).toBe(99);
        expect($scope.mapScroll.left).toBe(0);
      });

    });

    describe('right', function () {

      it('changes $scope.mapScroll.left by -50 when blockWidth is 50', function () {
        $scope.mapScroll.left = 200;
        $scope.mapScroll.top = 200;
        mapCtrl.scrollFuncs.right();
        expect($scope.mapScroll.top).toBe(200);
        expect($scope.mapScroll.left).toBe(150);
      });

      it('changes $scope.mapScroll.left by -1 when blockWidth is 1', function () {
        $scope.mapScroll.left = 1;
        $scope.mapScroll.top = 1;
        $scope.blockWidth = 1;
        mapCtrl.scrollFuncs.right();
        expect($scope.mapScroll.top).toBe(1);
        expect($scope.mapScroll.left).toBe(0);
      });

    });

    describe('down', function () {

      it('changes $scope.mapScroll.top by -50 when blockHeight is 50', function () {
        $scope.mapScroll.left = 1000;
        $scope.mapScroll.top = 1000;
        mapCtrl.scrollFuncs.down();
        expect($scope.mapScroll.top).toBe(950);
        expect($scope.mapScroll.left).toBe(1000);
      });

      it('changes $scope.mapScroll.top by -100 when blockHeight is 100', function () {
        $scope.mapScroll.left = -100;
        $scope.mapScroll.top = -100;
        $scope.blockHeight = 100;
        mapCtrl.scrollFuncs.down();
        expect($scope.mapScroll.top).toBe(-200);
        expect($scope.mapScroll.left).toBe(-100);
      });

    });

    describe('left', function () {

      it('changes $scope.mapScroll.top by 50 when blockWidth is 50', function () {
        $scope.mapScroll.left = 0;
        $scope.mapScroll.top = 0;
        mapCtrl.scrollFuncs.left();
        expect($scope.mapScroll.top).toBe(0);
        expect($scope.mapScroll.left).toBe(50);
      });

      it('changes $scope.mapScroll.top by 1 when blockWidth is 1', function () {
        $scope.mapScroll.left = -1;
        $scope.mapScroll.top = -1;
        $scope.blockWidth = 1;
        mapCtrl.scrollFuncs.left();
        expect($scope.mapScroll.top).toBe(-1);
        expect($scope.mapScroll.left).toBe(0);
      });

    });

  });

  describe('captureBlock', function () {

    beforeEach(function () {
      mapCtrl.captureBlock.and.callThrough();
    });

    it('calls getMap on mapProvider', function () {
      mapCtrl.captureBlock(fakeBlock);
      expect(mapProvider.getMap).toHaveBeenCalledWith();
    });

    it('calls tryToCaptureBlock on map', function () {
      mapCtrl.captureBlock(fakeBlock);
      expect(tryToCaptureBlockSpy).toHaveBeenCalledWith(fakeBlock, fakeHuman);
    });

    it('calls getHuman on playerProvider', function () {
      mapCtrl.captureBlock(fakeBlock);
      expect(playerProvider.getHuman).toHaveBeenCalledWith();
    });

    it('calls updateVisibleSquares if tryToCaptureBlock returns true', function () {
      tryToCaptureBlockSpy.and.returnValue(true);
      mapCtrl.captureBlock(fakeBlock);
      expect(mapCtrl.updateVisibleSquares).toHaveBeenCalledWith();
    });

    it('does not call updateVisibleSquares if tryToCaptureBlock returns false', function () {
      tryToCaptureBlockSpy.and.returnValue(false);
      mapCtrl.captureBlock(fakeBlock);
      expect(mapCtrl.updateVisibleSquares).not.toHaveBeenCalled();
    });

    it('calls updateVisibleRegions if tryToCaptureBlock returns true', function () {
      tryToCaptureBlockSpy.and.returnValue(true);
      mapCtrl.captureBlock(fakeBlock);
      expect(mapCtrl.updateVisibleRegions).toHaveBeenCalledWith();
    });

    it('does not call updateVisibleRegions if tryToCaptureBlock returns false', function () {
      tryToCaptureBlockSpy.and.returnValue(false);
      mapCtrl.captureBlock(fakeBlock);
      expect(mapCtrl.updateVisibleRegions).not.toHaveBeenCalled();
    });

    it('calls emit(humanActionDone) if tryToCaptureBlock returns true', function () {
      tryToCaptureBlockSpy.and.returnValue(true);
      mapCtrl.captureBlock(fakeBlock);
      expect(fakeHuman.eventEmitter.emit).toHaveBeenCalledWith('humanActionDone');
    });

    it('calls emit(humanActionDone) if tryToCaptureBlock returns false', function () {
      tryToCaptureBlockSpy.and.returnValue(false);
      mapCtrl.captureBlock(fakeBlock);
      expect(fakeHuman.eventEmitter.emit).toHaveBeenCalledWith('humanActionDone');
    });

  });

  describe('disclaimBlock', function () {

    beforeEach(function () {
      mapCtrl.disclaimBlock.and.callThrough();
    });

    it('calls getMap on mapProvider', function () {
      mapCtrl.disclaimBlock(fakeBlock);
      expect(mapProvider.getMap).toHaveBeenCalledWith();
    });

    it('calls tryToDisclaimBlock on map', function () {
      mapCtrl.disclaimBlock(fakeBlock);
      expect(tryToDisclaimBlockSpy).toHaveBeenCalledWith(fakeBlock, fakeHuman);
    });

    it('calls getHuman on playerProvider', function () {
      mapCtrl.disclaimBlock(fakeBlock);
      expect(playerProvider.getHuman).toHaveBeenCalledWith();
    });

    it('calls updateVisibleSquares if tryToDisclaimBlock returns true', function () {
      tryToDisclaimBlockSpy.and.returnValue(true);
      mapCtrl.disclaimBlock(fakeBlock);
      expect(mapCtrl.updateVisibleSquares).toHaveBeenCalledWith();
    });

    it('does not call updateVisibleSquares if tryToDisclaimBlock returns false', function () {
      tryToDisclaimBlockSpy.and.returnValue(false);
      mapCtrl.disclaimBlock(fakeBlock);
      expect(mapCtrl.updateVisibleSquares).not.toHaveBeenCalled();
    });

    it('calls updateVisibleRegions if tryToDisclaimBlock returns true', function () {
      tryToDisclaimBlockSpy.and.returnValue(true);
      mapCtrl.disclaimBlock(fakeBlock);
      expect(mapCtrl.updateVisibleRegions).toHaveBeenCalledWith();
    });

    it('does not call updateVisibleRegions if tryToDisclaimBlock returns false', function () {
      tryToDisclaimBlockSpy.and.returnValue(false);
      mapCtrl.disclaimBlock(fakeBlock);
      expect(mapCtrl.updateVisibleRegions).not.toHaveBeenCalled();
    });

    it('sets $scope.selectionMode to captureLand if tryToDisclaimBlock returns true', function () {
      expect($scope.selectionMode).not.toBe('captureLand');
      tryToDisclaimBlockSpy.and.returnValue(true);
      mapCtrl.disclaimBlock(fakeBlock);
      expect($scope.selectionMode).toBe('captureLand');
    });

    it('calls emit(humanActionDone) if tryToDisclaimBlock returns true', function () {
      tryToDisclaimBlockSpy.and.returnValue(true);
      mapCtrl.disclaimBlock(fakeBlock);
      expect(fakeHuman.eventEmitter.emit).toHaveBeenCalledWith('humanActionDone');
    });

    it('does not call emit(humanActionDone) if tryToDisclaimBlock returns false', function () {
      tryToDisclaimBlockSpy.and.returnValue(false);
      mapCtrl.disclaimBlock(fakeBlock);
      expect(fakeHuman.eventEmitter.emit).not.toHaveBeenCalled();
    });

  });

  describe('selectBlock', function () {

    it('does not call captureBlock nor disclaimBlock if $scope.readOnly is true', function () {
      $scope.readOnly = true;
      $scope.selectBlock(fakeBlock);
      expect(mapCtrl.captureBlock).not.toHaveBeenCalled();
      expect(mapCtrl.disclaimBlock).not.toHaveBeenCalled();
    });

    it('calls captureBlock if $scope.readOnly is false and ' +
       '$scope.selectionMode is captureLand', function () {
      $scope.readOnly = false;
      $scope.selectionMode = 'captureLand';
      $scope.selectBlock(fakeBlock);
      expect(mapCtrl.captureBlock).toHaveBeenCalledWith(fakeBlock);
    });

    it('calls disclaimBlock if $scope.readOnly is false and ' +
       '$scope.selectionMode is disclaimLand', function () {
      $scope.readOnly = false;
      $scope.selectionMode = 'disclaimLand';
      $scope.selectBlock(fakeBlock);
      expect(mapCtrl.disclaimBlock).toHaveBeenCalledWith(fakeBlock);
    });

  });

  describe('selectBlockInSquare', function () {

    it('does not call disclaimBlock if $scope.readOnly is true', function () {
      $scope.readOnly = true;
      $scope.selectBlockInSquare(fakeBlock);
      expect(mapCtrl.disclaimBlock).not.toHaveBeenCalled();
    });

    it('calls disclaimBlock if $scope.readOnly is false and ' +
       '$scope.selectionMode is disclaimLand', function () {
      $scope.readOnly = false;
      $scope.selectionMode = 'disclaimLand';
      $scope.selectBlockInSquare(fakeBlock);
      expect(mapCtrl.disclaimBlock).toHaveBeenCalledWith(fakeBlock);
    });

  });

  describe('selectSquare', function () {

    var fakeSquare;

    beforeEach(function () {
      fakeSquare = jasmine.createSpy('square');
    });

    it('sets the given square to $scope.selectedSquare ' +
       'if $scope.selectionMode is not disclaimLand', function () {
      $scope.selectSquare(fakeSquare);
      expect($scope.selectedSquare).toBe(fakeSquare);
    });

    it('calls $emit(squareSelected) on $rootScope with the given square ' +
       'if $scope.selectionMode is not disclaimLand', function () {
      $scope.selectSquare(fakeSquare);
      expect($rootScope.$emit).toHaveBeenCalledWith('squareSelected', fakeSquare);
    });

    it('sets $rootScope.activeControlPanel to "square_panel" ' +
       'if $scope.selectionMode is not disclaimLand', function () {
      $scope.selectSquare(fakeSquare);
      expect($rootScope.activeControlPanel).toBe('square_panel');
    });

    it('do nothing if $scope.selectionMode is "disclaimLand"', function () {
      $scope.selectionMode = 'disclaimLand';
      $scope.selectSquare(fakeSquare);
      expect($scope.selectedSquare).not.toBe(fakeSquare);
      expect($rootScope.$emit).not.toHaveBeenCalled();
      expect($rootScope.activeControlPanel).not.toBe('square_panel');
    });

  });

  describe('scroll', function () {

    var scrollTimeoutPromiseSpy;

    beforeEach(function () {
      scrollTimeoutPromiseSpy = jasmine.createSpy('scrollTimeoutPromise');
      mapCtrl.scrollTimeoutPromise = scrollTimeoutPromiseSpy;
      mapCtrl.scrollFuncs.fakeDirection = jasmine.createSpy('scroll func');
      $scope.scroll.and.callThrough();
    });

    it('sets scrollTimeoutPromise', function () {
      $scope.scroll('fakeDirection');
      expect(mapCtrl.scrollTimeoutPromise).not.toBe(scrollTimeoutPromiseSpy);
      expect(mapCtrl.scrollTimeoutPromise.constructor.name).toBe('Promise');
    });

    it('calls proper scrollFuncs after timeout', function () {
      $scope.scroll('fakeDirection');
      expect(mapCtrl.scrollFuncs.fakeDirection).not.toHaveBeenCalled();
      $timeout.flush();
      expect(mapCtrl.scrollFuncs.fakeDirection).toHaveBeenCalledWith();
    });

    it('calls scroll after timeout if scroll func returns true', function () {
      mapCtrl.scrollFuncs.fakeDirection.and.returnValue(true);
      $scope.scroll('fakeDirection');
      expect($scope.scroll).toHaveBeenCalledWith('fakeDirection');
      $scope.scroll.calls.reset();
      $timeout.flush();
      expect($scope.scroll).toHaveBeenCalledWith('fakeDirection', 1.05);
    });

    it('calls recursively scroll after timeout if scroll func returns true ' +
       'and stop calling when scroll func returns false', function () {
      mapCtrl.scrollFuncs.fakeDirection.and.returnValue(true);
      $scope.scroll('fakeDirection');
      expect($scope.scroll).toHaveBeenCalledWith('fakeDirection');
      $scope.scroll.calls.reset();
      $timeout.flush();
      expect($scope.scroll).toHaveBeenCalledWith('fakeDirection', 1.05);
      $scope.scroll.calls.reset();
      $timeout.flush();
      expect($scope.scroll).toHaveBeenCalledWith('fakeDirection', 1.05 + 0.1);
      $scope.scroll.calls.reset();
      $timeout.flush();
      expect($scope.scroll).toHaveBeenCalledWith('fakeDirection', 1.05 + 0.1 + 0.1);
      mapCtrl.scrollFuncs.fakeDirection.and.returnValue(false);
      $scope.scroll.calls.reset();
      $timeout.flush();
      expect($scope.scroll).not.toHaveBeenCalled();
    });

    it('does not call scroll if scroll func returns false', function () {
      mapCtrl.scrollFuncs.fakeDirection.and.returnValue(false);
      $scope.scroll('fakeDirection');
      expect($scope.scroll).toHaveBeenCalledWith('fakeDirection');
      $scope.scroll.calls.reset();
      $timeout.flush();
      expect($scope.scroll).not.toHaveBeenCalled();
    });

    it('sets scrollTimeoutDelay to 200 / 1.05 after timeout if scroll func returns true ' +
       'and scrollTimeoutDelay = 200', function () {
      mapCtrl.scrollTimeoutDelay = 200;
      mapCtrl.scrollFuncs.fakeDirection.and.returnValue(true);
      $scope.scroll('fakeDirection');
      expect(mapCtrl.scrollTimeoutDelay).toBe(200);
      $timeout.flush();
      expect(mapCtrl.scrollTimeoutDelay).toBeCloseTo(200 / 1.05, 6);
    });

    it('sets scrollTimeoutDelay to 200 / 1.15 after timeout if scroll func returns true ' +
       'and the given scrollTimeoutDelayDivider = 1.05', function () {
      mapCtrl.scrollTimeoutDelay = 200;
      mapCtrl.scrollFuncs.fakeDirection.and.returnValue(true);
      $scope.scroll('fakeDirection', 1.05);
      expect(mapCtrl.scrollTimeoutDelay).toBe(200);
      $timeout.flush();
      expect(mapCtrl.scrollTimeoutDelay).toBeCloseTo(200 / 1.15, 6);
    });

    it('does not change scrollTimeoutDelay if scroll func returns true ' +
       'and scrollTimeoutDelay = 98', function () {
      mapCtrl.scrollTimeoutDelay = 200;
      mapCtrl.scrollFuncs.fakeDirection.and.returnValue(false);
      $scope.scroll('fakeDirection');
      expect(mapCtrl.scrollTimeoutDelay).toBe(200);
      $timeout.flush();
      expect(mapCtrl.scrollTimeoutDelay).toBe(200);
    });

    it('changes scrollTimeoutDelay 5 times if scroll func returns true', function () {
      mapCtrl.scrollTimeoutDelay = 200;
      mapCtrl.scrollFuncs.fakeDirection.and.returnValue(true);
      $scope.scroll('fakeDirection');
      expect(mapCtrl.scrollTimeoutDelay).toBe(200);
      $timeout.flush();
      expect(mapCtrl.scrollTimeoutDelay).toBeCloseTo(200 / 1.05, 6);
      $timeout.flush();
      expect(mapCtrl.scrollTimeoutDelay).toBeCloseTo(200 / 1.05 / 1.15, 6);
      $timeout.flush();
      expect(mapCtrl.scrollTimeoutDelay).toBeCloseTo(200 / 1.05 / 1.15 / 1.25, 6);
      $timeout.flush();
      expect(mapCtrl.scrollTimeoutDelay).toBeCloseTo(200 / 1.05 / 1.15 / 1.25 / 1.35, 6);
      $timeout.flush();
      expect(mapCtrl.scrollTimeoutDelay).toBeCloseTo(200 / 1.05 / 1.15 / 1.25 / 1.35, 6);
    });

  });

  describe('stopScrolling', function () {

    beforeEach(function () {
      spyOn($timeout, 'cancel');
      mapCtrl.scrollTimeoutPromise = jasmine.createSpy('scrollTimeoutPromise');
      mapCtrl.scrollTimeoutDelay = jasmine.createSpy('scrollTimeoutDelay');
      $scope.stopScrolling.and.callThrough();
    });

    it('calls $timeout.cancel on scrollTimeoutPromise', function () {
      $scope.stopScrolling();
      expect($timeout.cancel).toHaveBeenCalledWith(mapCtrl.scrollTimeoutPromise);
    });

    it('sets scrollTimeoutDelay to 200', function () {
      $scope.stopScrolling();
      expect(mapCtrl.scrollTimeoutDelay).toBe(200);
    });

  });

});