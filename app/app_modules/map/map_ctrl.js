'use strict';

angular.module('square')

.controller('MapCtrl', ['$scope', '$rootScope', 'mapProvider', '$timeout',
                        'playerProvider', 'initProvider', 'mapDirections',
    function($scope, $rootScope, mapProvider, $timeout,
             playerProvider, initProvider, mapDirections) {

  var that = this;

  that.scrollTimeoutPromise = null;
  that.scrollTimeoutDelay = 200;
  that.leftTopCornerRegionX = 0;
  that.leftTopCornerRegionY = 0;

  $scope.mapScroll = { left: 0, top: 0 };
  $scope.gameEnd = false;
  $scope.anyBlockImproved = false;
  $scope.selectedSquare = false;
  $scope.regions = [];
  $scope.readOnly = true;
  $scope.selectionMode = 'captureLand';

  // ------------------- INIT -------------------

  that.init = function () {
    $scope.regionWidth = mapProvider.getRegionWidth();
    $scope.regionHeight = mapProvider.getRegionHeight();
    $scope.currentPlayer = playerProvider.getHuman();
    $scope.blockWidth = mapProvider.getBlockWidth();
    $scope.blockHeight = mapProvider.getBlockHeight();
    $scope.anyBlockImproved = !mapProvider.getMap().isFirstImprovement(playerProvider.getHuman());
    that.updateVisibleRegions();
    that.updateVisibleSquares();

    Object.values(playerProvider.getAllPlayers()).map(function (player) {
      player.eventEmitter.on('playerTurnBegin', function () {
        $timeout(function() {
          that.updateVisibleSquares();
        });
      });
    });

    playerProvider.getHuman().eventEmitter.on('humanTurnBegin', function () {
      $scope.readOnly = false;
    });

    playerProvider.getHuman().eventEmitter.on('humanActionDone', function () {
      //TODO: check Administration Points and turn auto-ending
      $scope.readOnly = true;
      $timeout(function() {
        that.updateVisibleSquares();
      });
      $timeout(function () {
        playerProvider.getHuman().moveDone();
      }, 150);
    });

    mapProvider.getMap().eventEmitter.on('blockMutates', function (block) {
      block.isMutating = true;
      $timeout(function () {
        block.isMutating = false;
      }, 2000);
    });

    for (var i = 0; i < 7; i++) {
      that.scrollFuncs.up();
    }
    for (i = 0; i < 10; i++) {
      that.scrollFuncs.left();
    }
  };

  that.addListeners = function () {
    initProvider.eventEmitter.on('gameInited', function () {
      that.init();
    });
    initProvider.eventEmitter.on('gameLoaded', function () {
      that.init();
    });
    $rootScope.$on('mapSelectionModeChanged', function (event, mode) {
      $scope.selectionMode = mode;
    });
  };

  that.addListeners();

  // ------------------- PRIVATE METHODS -------------------

  that.updateVisibleSquares = function () {
    $scope.squares = mapProvider.getVisibleSquares();
  };

  that.updateVisibleRegions = function () {
    mapProvider.updateVisibleRegions(
      //TODO: pass each corner separately (do not calculate other corner coords)
      $scope.regions, that.leftTopCornerRegionX, that.leftTopCornerRegionY);
  };

  that.updateLeftTopCornerRegionCoords = function () {
    var prevLeftTopCornerRegionX = that.leftTopCornerRegionX;
    var prevLeftTopCornerRegionY = that.leftTopCornerRegionY;
    that.leftTopCornerRegionX = Math.floor(-$scope.mapScroll.left / $scope.regionWidth);
    that.leftTopCornerRegionY = Math.floor(-$scope.mapScroll.top / $scope.regionHeight);
    if (prevLeftTopCornerRegionX !== that.leftTopCornerRegionX ||
        prevLeftTopCornerRegionY !== that.leftTopCornerRegionY) {
      //TODO: updating visible regions selectively every one scroll
      that.updateVisibleRegions();
    }
  };

  that.scrollFuncs = mapDirections.nonDiagonal.reduce(function (scrollFuncs, direction) {
    scrollFuncs[direction] = function () {
      if (mapProvider.getMap().checkScrollingBoundaries(
          $scope.mapScroll.left, $scope.mapScroll.top, direction)) {
        var scrollCoords = mapDirections.directionToCoord[direction];
        $scope.mapScroll.left -= scrollCoords.x * $scope.blockWidth;
        $scope.mapScroll.top -= scrollCoords.y * $scope.blockHeight;
        that.updateLeftTopCornerRegionCoords();
        return true;
      }
      $scope.stopScrolling();
      return false;
    };
    return scrollFuncs;
  }, {});

  that.captureBlock = function (block) {
    if (mapProvider.getMap().tryToCaptureBlock(block, playerProvider.getHuman())) {
      $scope.anyBlockImproved = true;
      that.updateVisibleSquares();
      that.updateVisibleRegions();
      //TODO: decrease AP
    }
    playerProvider.getHuman().eventEmitter.emit('humanActionDone');
  };

  that.disclaimBlock = function (block) {
    if (mapProvider.getMap().tryToDisclaimBlock(block, playerProvider.getHuman())) {
      that.updateVisibleSquares();
      that.updateVisibleRegions();
      $scope.selectionMode = 'captureLand';
      playerProvider.getHuman().eventEmitter.emit('humanActionDone');
      //TODO: decrease AP
    }
  };

  // ------------------- SCOPE METHODS -------------------

  $scope.selectBlock = function (block) {
    if ($scope.readOnly) {
      return;
    }
    if ($scope.selectionMode === 'captureLand') {
      that.captureBlock(block);
      return;
    }
    if ($scope.selectionMode === 'disclaimLand') {
      that.disclaimBlock(block);
    }
  };

  $scope.selectBlockInSquare = function (block) {
    if ($scope.readOnly) {
      return;
    }
    if ($scope.selectionMode === 'disclaimLand') {
      that.disclaimBlock(block);
    }
  };

  $scope.selectSquare = function (square) {
    if ($scope.selectionMode === 'disclaimLand') {
      return;
    }
    $scope.selectedSquare = square;
    $rootScope.$emit('squareSelected', square);
    $rootScope.activeControlPanel = 'square_panel';
  };

  $scope.scroll = function (direction, scrollTimeoutDelayDivider) {
    scrollTimeoutDelayDivider = scrollTimeoutDelayDivider === undefined ?
                                1.05 :
                                scrollTimeoutDelayDivider + 0.1;
      that.scrollTimeoutPromise = $timeout(function () {
      if (that.scrollFuncs[direction]()) {
        that.scrollTimeoutDelay = that.scrollTimeoutDelay > 100 ?
                                  that.scrollTimeoutDelay / scrollTimeoutDelayDivider :
                                  that.scrollTimeoutDelay;
        $scope.scroll(direction, scrollTimeoutDelayDivider);
      }
    }, that.scrollTimeoutDelay);
  };

  $scope.stopScrolling = function () {
    $timeout.cancel(that.scrollTimeoutPromise);
    that.scrollTimeoutDelay = 200;
  };

}]);
