'use strict';

describe('KingdomActionsPanelCtrl', function() {
  beforeEach(module('square'));

  var $controller, $rootScope, playerProvider, testingUtils, initProvider;

  beforeEach(inject(function (
      _$controller_, _$rootScope_, _playerProvider_, _testingUtils_, _initProvider_) {
    $controller = _$controller_;
    $rootScope = _$rootScope_;
    playerProvider = _playerProvider_;
    testingUtils = _testingUtils_;
    initProvider = _initProvider_;
  }));

  var $scope;
  var kingdomCtrl;
  var playerProviderGetHumanResult;

  beforeEach(function () {
    $scope = {};
    kingdomCtrl = $controller('KingdomActionsPanelCtrl', { $scope: $scope });
    spyOn($rootScope, '$emit');
    spyOn($scope, 'selectAction');
    spyOn(kingdomCtrl, 'updateDisclaimCooldown');
    spyOn(kingdomCtrl, 'addListeners');
    spyOn(kingdomCtrl, 'init');
    playerProviderGetHumanResult = {
      disclaimCooldown: jasmine.createSpy('disclaimCooldown'),
      eventEmitter: jasmine.createSpyObj('human.eventEmitter', ['on'])
    };
    spyOn(playerProvider, 'getHuman').and.returnValue(playerProviderGetHumanResult);
    initProvider.eventEmitter = jasmine.createSpyObj('initProvider.eventEmitter', ['on']);
  });

  describe('selectAction', function () {

    beforeEach(function () {
      $scope.selectAction.and.callThrough();
    });

    it('calls getHuman on playerProvider if the given action is disclaimLand', function () {
      $scope.selectAction('disclaimLand');
      expect(playerProvider.getHuman).toHaveBeenCalledWith();
    });

    it('emits mapSelectionModeChanged with disclaimLand on $rootScope ' +
       'if the given action is disclaimLand and human disclaimCooldown is 0', function () {
      playerProviderGetHumanResult.disclaimCooldown = 0;
      $scope.selectAction('disclaimLand');
      expect($rootScope.$emit).toHaveBeenCalledWith('mapSelectionModeChanged', 'disclaimLand');
    });

    it('does not emit mapSelectionModeChanged with disclaimLand on $rootScope ' +
       'if the given action is disclaimLand but human disclaimCooldown is not 0', function () {
      playerProviderGetHumanResult.disclaimCooldown = 1;
      $scope.selectAction('disclaimLand');
      expect($rootScope.$emit).not.toHaveBeenCalled();
    });

    it('emits mapSelectionModeChanged with captureLand on $rootScope ' +
       'if the given action is captureLand', function () {
      $scope.selectAction('captureLand');
      expect($rootScope.$emit).toHaveBeenCalledWith('mapSelectionModeChanged', 'captureLand');
    });

    it('throws an error if the given action is not disclaimLand nor captureLand', function () {
      expect(function () {
        $scope.selectAction('z');
      }).toThrow(new Error('Invalid action: z'));
    });

  });

  describe('updateDisclaimCooldown', function () {

    beforeEach(function () {
      kingdomCtrl.updateDisclaimCooldown.and.callThrough();
    });

    it('sets $scope.disclaimCooldown to human\'s disclaimCooldown', function () {
      kingdomCtrl.updateDisclaimCooldown();
      expect($scope.disclaimCooldown).toBe(playerProviderGetHumanResult.disclaimCooldown);
    });

    it('calls getHuman', function () {
      kingdomCtrl.updateDisclaimCooldown();
      expect(playerProvider.getHuman).toHaveBeenCalledWith();
    });

  });

  describe('init', function () {

    beforeEach(function () {
      kingdomCtrl.init.and.callThrough();
    });

    it('calls getHuman', function () {
      kingdomCtrl.init();
      expect(playerProvider.getHuman).toHaveBeenCalledWith();
    });

    it('sets an event listener on updatingDisclaimCooldown event', function () {
      kingdomCtrl.init();
      expect(playerProviderGetHumanResult.eventEmitter.on)
        .toHaveBeenCalledWith('updatingDisclaimCooldown', jasmine.any(Function));
    });

    it('calls updateDisclaimCooldown', function () {
      kingdomCtrl.init();
      expect(kingdomCtrl.updateDisclaimCooldown).toHaveBeenCalledWith();
    });

    it('calls updateDisclaimCooldown on updatingDisclaimCooldown event', function () {
      var callbacks = testingUtils.getEventListeners(
        playerProviderGetHumanResult.eventEmitter, 'updatingDisclaimCooldown');
      kingdomCtrl.init();
      kingdomCtrl.updateDisclaimCooldown.calls.reset();
      callbacks[0]();
      expect(kingdomCtrl.updateDisclaimCooldown).toHaveBeenCalledWith();
    });

  });

  describe('addListeners', function () {

    beforeEach(function () {
      kingdomCtrl.addListeners.and.callThrough();
    });

    it('adds event listener on gameLoaded on initProvider', function () {
      kingdomCtrl.addListeners();
      expect(initProvider.eventEmitter.on).toHaveBeenCalledWith(
        'gameInited', jasmine.any(Function));
    });

    it('adds event listener on gameInited on initProvider', function () {
      kingdomCtrl.addListeners();
      expect(initProvider.eventEmitter.on).toHaveBeenCalledWith(
        'gameLoaded', jasmine.any(Function));
    });

    it('calls init on gameInited', function () {
      var callbacks = testingUtils.getEventListeners(
        initProvider.eventEmitter, 'gameInited');
      kingdomCtrl.addListeners();
      callbacks[0]();
      expect(kingdomCtrl.init).toHaveBeenCalledWith();
    });

    it('calls init on gameLoaded', function () {
      var callbacks = testingUtils.getEventListeners(
        initProvider.eventEmitter, 'gameLoaded');
      kingdomCtrl.addListeners();
      callbacks[0]();
      expect(kingdomCtrl.init).toHaveBeenCalledWith();
    });

  });

});