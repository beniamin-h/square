'use strict';

angular.module('square')

.controller('KingdomActionsPanelCtrl', [
    '$scope', '$rootScope', 'playerProvider', 'initProvider',
    function($scope, $rootScope, playerProvider, initProvider) {

  var that = this;

  $scope.disclaimCooldown = Infinity;

  $scope.selectAction = function (action) {
    if (action === 'disclaimLand') {
      if (playerProvider.getHuman().disclaimCooldown === 0) {
        $rootScope.$emit('mapSelectionModeChanged', action);
      }
      return;
    }
    if (action === 'captureLand') {
      $rootScope.$emit('mapSelectionModeChanged', action);
      return;
    }
    throw new Error('Invalid action: ' + action);
  };

  that.updateDisclaimCooldown = function () {
    $scope.disclaimCooldown = playerProvider.getHuman().disclaimCooldown;
  };

  that.init = function () {
    playerProvider.getHuman().eventEmitter.on('updatingDisclaimCooldown', function () {
      that.updateDisclaimCooldown();
    });
    that.updateDisclaimCooldown();
  };

  that.addListeners = function () {
    initProvider.eventEmitter.on('gameInited', function () {
      that.init();
    });
    initProvider.eventEmitter.on('gameLoaded', function () {
      that.init();
    });
  };

  that.addListeners();

}]);
