'use strict';

angular.module('square')

.controller('SquarePanelCtrl', ['$scope', '$rootScope', 'buildingsBuilder',
                                'playerProvider', 'initProvider',
    function($scope, $rootScope, buildingsBuilder,
             playerProvider, initProvider) {

  $scope.selectedSquare = false;
  $scope.selectedPanel = null;
  $scope.availableBuildings = [];

  function updateAvailableBuildings() {
    if ($scope.selectedSquare && $scope.selectedSquare.player === playerProvider.getHuman()) {
      $scope.availableBuildings = buildingsBuilder.getAvailableBuildingClasses($scope.selectedSquare);
    } else {
      $scope.availableBuildings = [];
    }
  }

  // ------------------- INIT -------------------

  function init() {
    playerProvider.getHuman().eventEmitter.on('humanPlayerMoveBegin', function () {
      updateAvailableBuildings();
    });

    playerProvider.getHuman().eventEmitter.on('humanActionDone', function () {
      updateAvailableBuildings();
    });
  }

  initProvider.eventEmitter.on('gameInited', function () {
    init();
  });

  initProvider.eventEmitter.on('gameLoaded', function () {
    init();
  });

  $rootScope.$on('squareSelected', function (event, square) {
    $scope.selectedSquare = square;
    updateAvailableBuildings();
  });

  // ------------------- SCOPE METHODS -------------------

  $scope.openBuildPanel = function () {
    $scope.selectedPanel = 'build';
  };

  $scope.build = function (buildingClass) {
    console.log(buildingsBuilder.build($scope.selectedSquare, buildingClass));
  };

  $scope.openMainPanel = function () {
    $scope.selectedPanel = null;
  };

}]);
