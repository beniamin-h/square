'use strict';

angular.module('square')

.controller('StorageCtrl', ['$scope', 'storageProvider', 'playerProvider', 'initProvider',
    function($scope, storageProvider, playerProvider, initProvider) {

  $scope.resDisplayNames = {
    wood: 'Wood',
    stone: 'Stone',
    metal: 'Metal',
    food: 'Food',
    clay: 'Clay',
    coal: 'Coal',
    horses: 'Horses',
    boards: 'Boards',
    cutStone: 'Cut stone',
    steel: 'Steel',
    brick: 'Brick'
  };

  $scope.advancedResourcesAcquired = [];

  // ------------------- INIT -------------------

  function init() {
    $scope.basicResources = storageProvider.basicResources;
    $scope.resources = storageProvider.getResources(playerProvider.getHuman());
    $scope.getGoldIncome = function () {
      var income = storageProvider.getGoldIncome(playerProvider.getHuman());
      income = (income >= 0 ? '+' : '-') + income;
      return income;
    };

    $scope.getGold = function () {
      return storageProvider.getGold(playerProvider.getHuman());
    };

    storageProvider.getStorage(playerProvider.getHuman()).eventEmitter.on('put', function (resources) {
      Object.keys(resources).forEach(function (resName) {
        if (resources[resName] !== 0 &&
            storageProvider.advancedResources.indexOf(resName) > -1 &&
            $scope.advancedResourcesAcquired.indexOf(resName) === -1) {
          $scope.advancedResourcesAcquired.push(resName);
          $scope.$emit('resourcePanelHeightIncreased');
        }
      });
    });
  }

  initProvider.eventEmitter.on('gameInited', function () {
    init();
  });

  initProvider.eventEmitter.on('gameLoaded', function () {
    init();
  });


}]);
