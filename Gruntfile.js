"use strict";

module.exports = function(grunt) {

  function getSelectedFixtures() {
    var selectedFixtures = [];
    var fixtures = [
      'simple_map_3x3_5x5',
      'simple_map_3x3_5x5_with_24_improved_blocks',
      'simple_map_3x3_5x5_with_34_improved_blocks',
      'simple_map_5x5_10x10',
      'simple_map_3x3_5x5_with_2_human_squares',
      'plenty_advanced_resources_map_11x11_5x5',
      'easy_map_11x11_5x5',
      'easy_map_11x11_5x5_with_multiple_squares'
    ];

    if (grunt.option('fixture')) {
      if (Array.isArray(grunt.option('fixture'))) {
        grunt.option('fixture').forEach(function (fixture) {
          if (fixtures.indexOf(fixture) > -1) {
            selectedFixtures.push(fixture);
          } else {
            throw new Error('Invalid fixture: ' + fixture);
          }
        });
      } else {
        if (fixtures.indexOf(grunt.option('fixture')) > -1) {
          selectedFixtures.push(grunt.option('fixture'));
        } else {
          throw new Error('Invalid fixture: ' + grunt.option('fixture'));
        }
      }
    } else {
      selectedFixtures = fixtures;
    }

    return selectedFixtures;
  }

  var scope = grunt.option('scope');

  var baseTestFiles = [
    'app/utils/exception.catch.js',
    'app/bower_components/angular/angular.js',
    'app/bower_components/angular-route/angular-route.js',
    'app/bower_components/angular-bootstrap/ui-bootstrap.js',
    'app/bower_components/angular-mocks/angular-mocks.js',
    'app/bower_components/serialijse/dist/serialijse.js',
    'node_modules/karma-read-json/karma-read-json.js',
    {pattern: 'test_fixtures/**/*.json', included: false},
    'app/app.js',
    'app/app_modules/**/!(*.spec|*.schema|*.tests).js',
    'app/utils/**/!(*.spec|*.schema|*.tests).js',
    'app/core/**/!(*.spec|*.schema|*.tests).js'
  ];

  var unitTestFiles = baseTestFiles.concat(
    'app/app_modules/**/!(*.schema).spec.js',
    'app/utils/**/!(*.schema).spec.js',
    'app/core/**/!(*.schema).spec.js'
  );

  var coverageTestFiles;
  var coveragePreprocessorFiles = {};
  var blackBoxTestFiles;

  if (scope) {
    if (scope === 'utils') {
      coverageTestFiles = baseTestFiles.concat(
        'app/utils/**/!(*.schema).spec.js'
      );
      coveragePreprocessorFiles['app/utils/**/!(*.spec|*.schema|*.tests).js'] = ['coverage'];
      blackBoxTestFiles = baseTestFiles.concat(
        'app/utils/**/*.black_box.tests.js'
      );
    } else {
      coverageTestFiles = baseTestFiles.concat(
        'app/app_modules/' + scope + '/!(*.schema).spec.js',
        'app/core/' + scope + '/!(*.schema).spec.js'
      );
      coveragePreprocessorFiles['app/app_modules/' + scope + '/!(*.spec|*.schema|*.tests).js'] = ['coverage'];
      coveragePreprocessorFiles['app/core/' + scope + '/!(*.spec|*.schema|*.tests).js'] = ['coverage'];
      blackBoxTestFiles = baseTestFiles.concat(
        'app/core/' + scope + '/**/*.black_box.tests.js'
      );
    }
  } else {
    coverageTestFiles = unitTestFiles;
    coveragePreprocessorFiles = {
      'app/core/**/!(*spec|*schema|*tests).js': ['coverage'],
      'app/utils/**/!(*spec|*schema|*tests).js': ['coverage'],
      'app/app_modules/**/!(*spec|*schema|*tests).js': ['coverage']
    };
    blackBoxTestFiles = baseTestFiles.concat(
      'app/core/**/*.black_box.tests.js',
      'app/utils/**/*.black_box.tests.js'
    );
  }

  var failureFixtures = [
    'Mon_Dec_26_2016_01_04_30_GMT_0100__CET_',
    'Mon_Jan_02_2017_13_42_53_GMT_0100__CET_',
    'Mon_Jan_09_2017_13_01_49_GMT_0100__CET_',
    'Sat_Dec_31_2016_20_12_18_GMT_0100__CET_',
    'Sun_Dec_25_2016_21_01_44_GMT_0100__CET_',
    'Sun_Dec_25_2016_22_06_06_GMT_0000___',
    'Thu_Jan_12_2017_14_44_15_GMT_0100__CET_',
    'Thu_Jan_12_2017_14_44_21_GMT_0100__CET_',
    'Wed_Dec_28_2016_13_23_57_GMT_0100__CET_',
    'Wed_Dec_28_2016_13_23_59_GMT_0100__CET_',
    'Fri_Jan_20_2017_16_56_18_GMT_0100__CET_',
    'Sun_Jan_22_2017_16_12_43_GMT_0100__CET_',
    'Sun_Jan_22_2017_17_10_21_GMT_0100__CET_'
  ];

  grunt.initConfig({
    karma: {
      options: {
        configFile: 'karma.conf.js',
        singleRun: true
      },
      unit: {
        options: {
          files: unitTestFiles
        }
      },
      schema: {
        options: {
          reportSlowerThan: 1000,
          browserNoActivityTimeout: 60000, // 1 minute
          files: baseTestFiles.concat(
            'app/core/**/*.schema.js',
            'app/core/**/*.schema.spec.js',
            'app/utils/**/*.schema.js',
            'app/utils/**/*.schema.spec.js'
          ),
          reporters: ['progress', 'verbose']
        }
      },
      test_failures: {
        options: {
          browserNoActivityTimeout: 120000, // 2 minutes
          reportSlowerThan: 60000, // 60 sec
          files: baseTestFiles.concat(
            'app/core/ai/ai.failure.tests.js'
          ),
          reporters: ['progress', 'verbose']
        }
      },
      black_box: {
        options: {
          browserNoActivityTimeout: 120000, // 2 minutes
          reportSlowerThan: 60000, // 60 sec
          files: blackBoxTestFiles,
          reporters: ['progress', 'verbose']
        }
      },
      test_changed: {
        options: {
          singleRun: false,
          files: unitTestFiles,
          browsers: ['NodeWebkitChangedTester'],
          autoWatch: false,
          port: 9870
        }
      },
      test_changed_schemas: {
        options: {
          browserNoActivityTimeout: 20000, // 20 sec
          reportSlowerThan: 500,
          singleRun: false,
          files: baseTestFiles.concat(
            'app/core/**/*.schema.js',
            'app/core/**/*.schema.spec.js',
            'app/utils/**/*.schema.js',
            'app/utils/**/*.schema.spec.js'
          ),
          browsers: ['NodeWebkitChangedSchemasTester'],
          autoWatch: false,
          port: 9872
        }
      },
      test_changed_black_box: {
        options: {
          reportSlowerThan: 500,
          singleRun: false,
          files: blackBoxTestFiles,
          browsers: ['NodeWebkitChangedBlackBoxTester'],
          autoWatch: false,
          port: 9871
        }
      },
      coverage: {
        options: {
          files: coverageTestFiles,
          reporters: ['dots', 'coverage'],
          reportSlowerThan: 10000,
          preprocessors: coveragePreprocessorFiles
        }
      },
      regen_fixtures: {
        client: {
          args: ['--grep', grunt.option('grep')]
        },
        options: {
          files: baseTestFiles.concat(
            'app/dev/regenerate_fixtures.js'
          ),
          reporters: ['progress', 'verbose'],
          reportSlowerThan: 10000
        }
      }
    },
    rename: {
      confirm_regenerated_fixtures: {
        files: getSelectedFixtures().map(function (fixture) {
          return {
            src: process.env.PWD + '/test_fixtures/' + fixture + '.new.json',
            dest: process.env.PWD + '/test_fixtures/' + fixture + '.json'
          };
        })
      },
      confirm_regenerated_failure_fixtures: {
        files: failureFixtures.map(function (fixture) {
          return {
            src: process.env.PWD + '/test_fixtures/test_failures/' + fixture + '.new.json',
            dest: process.env.PWD + '/test_fixtures/test_failures/' + fixture + '.json'
          };
        })
      }
    }
  });

  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-contrib-rename');

  grunt.registerTask('test', ['karma:unit', 'karma:schema', 'karma:black_box']);
  grunt.registerTask('confirm_regenerated_fixtures', ['rename:confirm_regenerated_fixtures']);
};